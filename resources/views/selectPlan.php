<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
    <link href="css/roundslider.min.css" rel="stylesheet">
    <script src="js/roundslider.min.js"></script>
</head>


<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">
                        <style>
                            .rs-handle  {
                                background-color: #fff;
                                border: 1px solid #000;
                            }
                            .rs-tooltip-text{
                                font-size: 20px;
                                font-family: FontAwesome;
                            }
                            .rs-control .rs-range-color {
                                background-color: #fc6558;
                            }
                            .rs-control .rs-path-color {
                                background-color: #ffffff;
                            }
                            .rs-control .rs-handle:after {
                                background-color: #fc6558;
                            }
                            .rs-control .rs-bg-color {
                                background-color: #ffffff;
                            }
                        </style>

                        <br><br>
                        <h3 class="text-center">Choose Your Plan</h3>

                        <div class="container-fluid">

                                <section id="optionA" class="meter">
                                    <div class="panel panel-default">
                                        <div class="panel-body" style="padding:0 15px;">
                                            <div class="row">
                                                <div class="col-md-8 col-sm-12 col-xs-12" style="padding-top:1em;">
                                                    <h5>Payroll</h5>
                                                    <h4 class="text-center">How many employees do you have?</h4>
                                                    <br>
                                                    
                                                    <div class="col-md-5 col-sm-3 col-xs-12"><div id="optionASlider" class="center-block"></div><div class="clearfix"></div></div>
                                                    <div class="col-md-3 col-sm-4 col-xs-12 text-center">
                                                        <br><br>
                                                        <span><i class="fa fa-inr"></i> </span>
                                                        <span class="h2 total">
                                                            124 
                                                        </span>
                                                        <br>
                                                        <span class="h6">PER MONTH</span> 
                                                    </div>
                                                    <div class="col-md-4 col-sm-5 col-xs-12 text-center">
                                                        <br><br>
                                                        <span class="h4">Here&rsquo;s the Math</span>
                                                        <br>
                                                        <span class="h5 noOfEmployee">1 employee at <i class="fa fa-inr"></i>  25</span>
                                                        <br>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <br><br>
                                                </div>
                                                <div class="col-md-4 col-sm-12 col-xs-12 bg-primary" style="height: 22em;padding:1em 10px;">
                                                    <h4 class="text-center">General Pricing</h4>
                                                    <form action="" method="POST">
                                                        <input type="hidden" name="plan" value="payroll">
                                                        <br>
                                                        <table class="table">
                                                            <tr>
                                                                <th>ITEM</th>
                                                                <th>PRICE PER MONTH</th>
                                                            </tr>
                                                            <tr>
                                                                <td>Base Fee</td>
                                                                <td><i class="fa fa-inr"></i>  99</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Employees</td>
                                                                <td><i class="fa fa-inr"></i>  25 per Employee</td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <input type="submit" value="Select" class="btn btn-raised btn-default btn-block">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </form>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </section>

                                <section id="optionB" class="meter">
                                    <div class="panel panel-default">
                                        <div class="panel-body" style="padding:0 15px;">
                                            <div class="row">
                                                <div class="col-md-8 col-sm-12 col-xs-12" style="padding-top:1em;">
                                                    <h5>Payroll + Benefits</h5>
                                                    <h4 class="text-center">How many employees do you have?</h4>
                                                    <br>
                                                    
                                                    <div class="col-md-5 col-sm-3 col-xs-12"><div id="optionBSlider" class="center-block"></div><div class="clearfix"></div></div>
                                                    <div class="col-md-3 col-sm-4 col-xs-12 text-center">
                                                        <br><br>
                                                        <span><i class="fa fa-inr"></i> </span>
                                                        <span class="h2 total">
                                                            0
                                                        </span>
                                                        <br>
                                                        <span class="h6">PER MONTH</span> 
                                                    </div>
                                                    <div class="col-md-4 col-sm-5 col-xs-12 text-center">
                                                        <br><br>
                                                        <span class="h4">Here&rsquo;s the Math</span>
                                                        <br>
                                                        <span class="h5 noOfEmployee">1 employee at <i class="fa fa-inr"></i>  0</span>
                                                        <br>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <br><br>
                                                </div>
                                                <div class="col-md-4 col-sm-12 col-xs-12 bg-primary" style="height: 22em;padding:1em 10px;">
                                                    <h4 class="text-center">General Pricing</h4>
                                                    <form action="" method="POST">
                                                        <input type="hidden" name="plan" value="benefits">
                                                        <br>
                                                        <table class="table">
                                                            <tr>
                                                                <th>ITEM</th>
                                                                <th>PRICE PER MONTH</th>
                                                            </tr>
                                                            <tr>
                                                                <td>Base Fee</td>
                                                                <td><i class="fa fa-inr"></i>  0</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Employees</td>
                                                                <td><i class="fa fa-inr"></i>  0 per Employee</td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <input type="submit" value="Select" class="btn btn-raised btn-default btn-block">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>

                                <section id="optionC" class="meter">
                                    <div class="panel panel-default">
                                        <div class="panel-body" style="padding:0 15px;">
                                            <div class="row">
                                                <div class="col-md-8 col-sm-12 col-xs-12" style="padding-top:1em;">
                                                    <h5>Payroll + Benefits + Manager</h5>
                                                    <h4 class="text-center">How many employees do you have?</h4>
                                                    <br>
                                                    
                                                    <div class="col-md-5 col-sm-3 col-xs-12"><div id="optionCSlider" class="center-block"></div><div class="clearfix"></div></div>
                                                    <div class="col-md-3 col-sm-4 col-xs-12 text-center">
                                                        <br><br>
                                                        <span><i class="fa fa-inr"></i> </span>
                                                        <span class="h2 total">
                                                              3025
                                                        </span>
                                                        <br>
                                                        <span class="h6">PER MONTH</span> 
                                                    </div>
                                                    <div class="col-md-4 col-sm-5 col-xs-12 text-center">
                                                        <br><br>
                                                        <span class="h4">Here&rsquo;s the Math</span>
                                                        <br>
                                                        <span class="h5 noOfEmployee">1 employee at <i class="fa fa-inr"></i>  25</span>
                                                        <br>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <br><br>
                                                </div>
                                                <div class="col-md-4 col-sm-12 col-xs-12 bg-primary" style="height: 22em;padding:1em 10px;">
                                                    <h4 class="text-center">General Pricing</h4>
                                                    <form action="" method="POST">
                                                        <input type="hidden" name="plan" value="manager">
                                                        <br>
                                                        <table class="table">
                                                            <tr>
                                                                <th>ITEM</th>
                                                                <th>PRICE PER MONTH</th>
                                                            </tr>
                                                            <tr>
                                                                <td>Manager Fee</td>
                                                                <td><i class="fa fa-inr"></i>  3000 per 100 employee</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Employees</td>
                                                                <td><i class="fa fa-inr"></i>  25 per Employee</td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <input type="submit" value="Select" class="btn btn-raised btn-default btn-block">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>

                            <div class="clearfix"> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>

    $(function() {
        $.material.init();

        $("nav .nav.navbar.nav-stacked li").addClass('disabled');
        $("nav .nav.navbar.nav-stacked li").css({
            webkitFilter: 'blur(3px)',
            filter: 'blur(3px)'
        });

        $("nav .nav.navbar.nav-stacked li a").attr('href', 'javascript:void(0)');

        $("#optionASlider, #optionBSlider, #optionCSlider").each(function(index, el) {

            var $global = $(this).parentsUntil(".meter").last().parent();
            
            $(el).roundSlider({
                radius: 70,
                width: 15,
                handleShape: "square",
                sliderType: "min-range",
                circleShape: "pie",
                startAngle: 315,
                value: 1,
                max: 500,
                min: 1,
                mouseScrollAction: true,

                baseRate : 0,
                employeeRate : 25,
                hasManager : false,
                managerRate : 3000,

                drag: function (args) {
                    handleInput.call($($global), args, $(el).roundSlider("option"));
                },
                change: function (args) {
                    handleInput.call($($global), args, $(el).roundSlider("option"));
                }
            });
        });

        function handleInput (args, slider) {
            var noOfEmployee = args.value;

            var employeeRate = slider.options.employeeRate;
            var managerRate = slider.options.managerRate;
            var hasManager = slider.options.hasManager;
            var noOfManager = 0;

            if (hasManager) {
                noOfManager = Math.floor(noOfEmployee / 101) + 1;
            }
            var baseRate = slider.options.baseRate;

            var total = baseRate + employeeRate * noOfEmployee + noOfManager * managerRate;

            $(this).find(".noOfEmployee").html(' ' + noOfEmployee +  ' employee at <i class="fa fa-inr"></i> ' + employeeRate);

            $(this).find(".total").text(total);

            return true;
        }

        $("#optionASlider").roundSlider("option", "baseRate", 99 );
        $("#optionBSlider").roundSlider("option", "baseRate", 0 );
        $("#optionBSlider").roundSlider("option", "employeeRate", 0 );
        $("#optionCSlider").roundSlider("option", "baseRate", 0 );
        $("#optionCSlider").roundSlider("option", "hasManager", true );
    });
</script>