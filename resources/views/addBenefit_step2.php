<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>


<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">
                        
                        <div class="breadcrumb flat text-center row">
                            <span class="col-md-4 col-sm-4 hidden-xs">Add Existing Benefits</span>
                            <span class="active col-md-4 col-sm-4 col-xs-12">Choose Benefits Plans</span>
                            <span class="col-md-4 col-sm-4 hidden-xs">Add Employees To Benefit</span>
                        </div>

                        <br>
                         <?php if (!empty($result)) 
                            { ?>
                        <br>
                        <div class="row text-center" style="border-bottom: 1px solid #ddd;">
                            <div class="col-md-3 col-sm-3 col-xs-4"><h6>Plan Name</h6> </div>
                            <div class="col-md-2 col-sm-3 col-xs-4"><h6>Cover</h6></div>
                            <div class="col-md-3 hidden-sm hidden-xs"><h6>Factors</h6></div>
                            <div class="col-md-2 col-sm-3 col-xs-4"><h6>Total Sum Assured</h6></div>
                            <div class="col-md-2 col-sm-3 hidden-xs"></div>
                        </div>
                        <?php 
                                foreach ($result as $key => $value) 
                                { ?>
                        <div class="planContainer row">
                            <div class="plan">
                                <div class="summary">
                                    <div class="col-md-3 col-sm-3 col-xs-4">
                                        <br>
                                        <h5><?php echo $value->planCompany;?></h5>
                                        <h6><?php echo $value->planName;?></h6>
                                    </div>
                                    <div class="col-md-2 col-sm-3 col-xs-4">
                                        <br>
                                        <h6>Amount - &nbsp;<i class="fa fa-inr"></i><?php echo $value->sumAssured;?></h6>
                                        <h6>Group Size - &nbsp;2</h6>
                                    </div>
                                    <div class="col-md-3 hidden-sm hidden-xs">
                                        <br>
                                        <h6><i class="fa fa-users"></i> &nbsp; FAMILY DEFINITION</h6>
                                        <h6><i class="fa fa-medkit"></i> &nbsp; PRE-EXISTING DISEASES</h6>
                                        <h6><i class="fa fa-money"></i> &nbsp; WAIVER</h6>
                                    </div>
                                    <div class="col-md-2 text-center col-sm-3 col-xs-4"><br><br><h6><i class="fa fa-inr"></i><?php echo $value->premium;?></h6></div>
                                    <div class="col-md-2 text-center col-sm-3 col-xs-12">
                                        <br>
                                        <span data-id="<?php echo $value->planCompany;?>" class="select btn btn-raised btn-sm"><i class="fa fa-cart-plus"></i> SELECT</span>
                                        <h6>Cost Per Employee</h6>
                                        <h6><b><i class="fa fa-inr"></i><?php echo round($value->premium/365);?></b></h6>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="body">
                                    <div class="col-md-6">
                                        <div class="detail">        
                                            <h6>
                                                <b> &nbsp;&nbsp;<u>Inclusions</u></b>
                                                <div class="clearfix"></div>
                                            </h6><?php if ($value->familyFloater=='true'){ ?>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-8 col-sm-8 col-xs-8"><h6>Family Floater</h6></div>
                                                <div class="col-md-4 col-sm-4 col-xs-4"><h6>Waived</h6></div>
                                            </div>
                                            <?php } ?>
                                            <?php if ($value->waitingPeriod=='true'){ ?>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-8 col-sm-8 col-xs-8"><h6>30 days Waiting Period</h6></div>
                                                <div class="col-md-4 col-sm-4 col-xs-4"><h6>Waived</h6></div>
                                            </div>
                                            <?php } ?><?php if ($value->firstYearExclusion=='true'){ ?>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-8 col-sm-8 col-xs-8"><h6>First Year Exclusion</h6></div>
                                                <div class="col-md-4 col-sm-4 col-xs-4"><h6>Waived</h6></div>
                                            </div>
                                            <?php } ?>
                                            <?php if ($value->twoYearExclusion=='true'){ ?>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-8 col-sm-8 col-xs-8"><h6>First Two Year Exclusion</h6></div>
                                                <div class="col-md-4 col-sm-4 col-xs-4"><h6>Waived</h6></div>
                                            </div>
                                            <?php } ?>
                                            <?php if ($value->diseaseCover=='true'){ ?>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-8 col-sm-8 col-xs-8"><h6>Pre Existing Disease Cover</h6></div>
                                                <div class="col-md-4 col-sm-4 col-xs-4"><h6>Waived</h6></div>
                                            </div>
                                            <?php } ?>
                                            <?php if ($value->maternityBenefit=='true'){ ?>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-8 col-sm-8 col-xs-8"><h6>Maternity Benefits</h6></div>
                                                <div class="col-md-4 col-sm-4 col-xs-4"><h6>Waived</h6></div>
                                            </div>
                                            <?php } ?>
                                            <?php if ($value->childCover=='true'){ ?>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-8 col-sm-8 col-xs-8"><h6>Child Cover From Day One</h6></div>
                                                <div class="col-md-4 col-sm-4 col-xs-4"><h6>Waived</h6></div>
                                            </div>
                                            <?php } ?>
                                            <?php if ($value->copay=='true'){ ?>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-8 col-sm-8 col-xs-8"><h6>Co - Pay</h6></div>
                                                <div class="col-md-4 col-sm-4 col-xs-4"><h6>Waived</h6></div>
                                            </div>
                                            <?php } ?>
                                            <?php if ($value->corporateBuffer=='true'){ ?>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-8 col-sm-8 col-xs-8"><h6>Corporate Buffer</h6></div>
                                                <div class="col-md-4 col-sm-4 col-xs-4"><h6>Waived</h6></div>
                                            </div>
                                            <?php } ?>
                                            <?php if ($value->allDayCare=='true'){ ?>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-8 col-sm-8 col-xs-8"><h6>All Day Care</h6></div>
                                                <div class="col-md-4 col-sm-4 col-xs-4"><h6>Waived</h6></div>
                                            </div>
                                            <?php } ?>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="detail">
                                            <h6>
                                                <b> &nbsp;&nbsp; <u>Exclusions </u></b>
                                                <div class="clearfix"></div>
                                            </h6>
                                            <?php if ($value->familyFloater=='false'){ ?>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-8 col-sm-8 col-xs-8"><h6>Family Floater</h6></div>
                                                <div class="col-md-4 col-sm-4 col-xs-4"><h6>Not Waived</h6></div>
                                            </div>
                                            <?php } ?>
                                            <?php if ($value->waitingPeriod=='false'){ ?>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-8 col-sm-8 col-xs-8"><h6>30 days Waiting Period</h6></div>
                                                <div class="col-md-4 col-sm-4 col-xs-4"><h6>Not Waived</h6></div>
                                            </div>
                                            <?php } ?><?php if ($value->firstYearExclusion=='false'){ ?>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-8 col-sm-8 col-xs-8"><h6>First Year Exclusion</h6></div>
                                                <div class="col-md-4 col-sm-4 col-xs-4"><h6>Not Waived</h6></div>
                                            </div>
                                            <?php } ?>
                                            <?php if ($value->twoYearExclusion=='false'){ ?>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-8 col-sm-8 col-xs-8"><h6>First Two Year Exclusion</h6></div>
                                                <div class="col-md-4 col-sm-4 col-xs-4"><h6>Not Waived</h6></div>
                                            </div>
                                            <?php } ?>
                                            <?php if ($value->diseaseCover=='false'){ ?>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-8 col-sm-8 col-xs-8"><h6>Pre Existing Disease Cover</h6></div>
                                                <div class="col-md-4 col-sm-4 col-xs-4"><h6>Not Waived</h6></div>
                                            </div>
                                            <?php } ?>
                                            <?php if ($value->maternityBenefit=='false'){ ?>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-8 col-sm-8 col-xs-8"><h6>Maternity Benefits</h6></div>
                                                <div class="col-md-4 col-sm-4 col-xs-4"><h6>Not Waived</h6></div>
                                            </div>
                                            <?php } ?>
                                            <?php if ($value->childCover=='false'){ ?>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-8 col-sm-8 col-xs-8"><h6>Child Cover From Day One</h6></div>
                                                <div class="col-md-4 col-sm-4 col-xs-4"><h6>Not Waived</h6></div>
                                            </div>
                                            <?php } ?>
                                            <?php if ($value->copay=='false'){ ?>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-8 col-sm-8 col-xs-8"><h6>Co - Pay</h6></div>
                                                <div class="col-md-4 col-sm-4 col-xs-4"><h6>Not Waived</h6></div>
                                            </div>
                                            <?php } ?>
                                            <?php if ($value->corporateBuffer=='false'){ ?>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-8 col-sm-8 col-xs-8"><h6>Corporate Buffer</h6></div>
                                                <div class="col-md-4 col-sm-4 col-xs-4"><h6>Not Waived</h6></div>
                                            </div>
                                            <?php } ?>
                                            <?php if ($value->allDayCare=='false'){ ?>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-8 col-sm-8 col-xs-8"><h6>All Day Care</h6></div>
                                                <div class="col-md-4 col-sm-4 col-xs-4"><h6>Not Waived</h6></div>
                                            </div>
                                            <?php } ?>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    
                                    <div class="col-md-6">
                                        <div class="detail">   
                                            <h6>
                                                <b> &nbsp;&nbsp; <u>Other Factors</u></b>
                                                <div class="clearfix"></div>
                                            </h6>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-7 col-sm-7 col-xs-7"><h6>All Day care Procedures</h6></div>
                                                <div class="col-md-5 col-sm-5 col-xs-5"><h6>Covered / Not Covered</h6></div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-7 col-sm-7 col-xs-7"><h6>Co - Pay</h6></div>
                                                <div class="col-md-5 col-sm-5 col-xs-5"><h6>Yes / No</h6></div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-7 col-sm-7 col-xs-7"><h6>Pre hospitalisation</h6></div>
                                                <div class="col-md-5 col-sm-5 col-xs-5"><h6>30 Days</h6></div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-7 col-sm-7 col-xs-7"><h6>Post hospitalisation</h6></div>
                                                <div class="col-md-5 col-sm-5 col-xs-5"><h6>60 Daus</h6></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="detail">
                                    
                                            <h6>
                                                <b> &nbsp;&nbsp; <u>Hospitals and TPA</u></b>
                                                <div class="clearfix"></div>
                                            </h6>

                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-7 col-sm-7 col-xs-7"><h6>TPA</h6></div>
                                                <div class="col-md-5 col-sm-5 col-xs-5"><h6>Self</h6></div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-7 col-sm-7 col-xs-7"><h6>Claim intimation</h6></div>
                                                <div class="col-md-5 col-sm-5 col-xs-5"><h6>Yes / No</h6></div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-7 col-sm-7 col-xs-7"><h6>Mid term addition deletion of employees </h6></div>
                                                <div class="col-md-5 col-sm-5 col-xs-5"><h6>Yes / No</h6></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>

                                    <br>

                                    <!-- <h4>Limitations : </h4>
                                    <ul>
                                        <li></li>
                                        <li></li>
                                    </ul> -->

                                    <br>

                                    <h4>Downloads</h4>
                                    <a href="#">Product Brochure</a><br>
                                    <a href="#">Policy Wording</a>
                                </div>
                            </div>
                        </div>
                                                        <?php } } else { ?>
                                        <p class="clearfix">You don't have any benefits as of now.</p>
                                  <?php } ?>
                        <style>
                            .planContainer{
                                padding: 0 10px;
                            }
                            .plan {
                                /*perspective:1200px;*/
                                margin: 20px 0;
                            }
                            .plan > .summary{
                                transition:all 0.3s ease-in-out;
                                cursor: pointer;
                                background: #f6f6f6;
                                border-bottom: 1px solid #ddd;
                            }
                            .plan > .summary:hover{
                                box-shadow: 0 0 3px #aaa;
                            }

                            .plan >.summary > div{
                                border:1px solid #ddd;
                                border-left: none;
                                border-bottom: none;
                                box-sizing:border-box;
                                min-height: 8em;
                                padding:0 10px;
                            }
                            .plan >.summary > div:first-child{
                                border-left: 1px solid #ddd;
                            }
                            .plan > .summary .btn{
                                margin: 0;
                            }
                            .plan .body{
                                padding: 15px;
                                border: 1px solid #ddd;
                                box-sizing:border-box;
                                padding-top: 30px;
                                transform-style:preserve-3d;
                                transform-origin:top;
                                backface-visibility: hidden;
                            }
                            .detail{
                                box-shadow: 0 0 2px #888;
                                padding: 15px;
                                margin-top: 15px;
                                overflow-x: overlay;
                            }

                            .selected {
                                background: #03A9F4 !important;
                                color: #fff;
                            }
                            .btn.select.btn-raised, .btn.select.btn-raised:hover, .btn.select.btn-raised:active {
                                background: #cf3d3d;
                                color: #fff;
                            }
                        </style>

                    </div>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>

    function displayError() {
        swal({
            title : "Error !",
            text : 'Try Again!',
            type : 'error',
            confirmButtonClass : 'bg-theme',
            animation : false,
            width : 400
        });
    };

    function closeAlert() {
        $(".sweet-alert button").first().trigger('click');
    }

    $(function() {

        // add active class to corresponding link in menu 
        $(".nav li.active").removeClass('active');
        $(".nav.nav-stacked li").filter(function(index) {
            return $(this).data('url') == 'benefit';
        }).addClass('active');

        $.material.init();

        $(".plan .summary").click(function(event) {
            if (event.target.tagName!='SPAN'){
                $(".plan .body").not($(this).siblings('.body')).slideUp();
                $(this).siblings('.body').slideToggle();
            }
        });

        $(".select").click(function(event) {
            event.preventDefault();
            var id = $(this).data('id');
            var $this = $(this);
            
            $.ajax({
                url: location.href + '/' + id,
                type: 'POST',
                data: {id: id},
            })
            .done(function(data) {

                if (data.status == 200) {
  
                    getAndInsert(data.url);
                    history.pushState(null, null, data.url);
                }
                else 
                    displayError();
            })
            .fail(function() {
                displayError();
            })
        });

        $(".plan .body").hide();
    })
</script>
