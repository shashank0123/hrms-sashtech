<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>

<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <style>
                        .contractor{
                            -webkit-box-shadow: none;
                            box-shadow: none;
                            position: relative;
                        }
                        .contractor .panel-body > div::after,.contractor .panel-body > div::before {
                            content: '';
                            position: absolute;
                            width: 15px;
                            height: 18px;
                            background: #9A8B8B;
                            top: -11px;
                        }
                        .contractor .panel-body > div::before{
                            left: 0;
                            transform:skewY(40deg);
                        }
                        .contractor .panel-body > div::after{
                            right: 0;
                            transform:skewY(-40deg);
                        }
                        .contractor .panel-heading{
                            z-index: 10;
                            cursor: pointer;
                        }
                        .contractor .panel-body {
                            position: relative;
                            padding-top: 0;
                            padding-bottom: 0;
                        }

                        .contractor .panel-body > div{
                            background: #f5f5f5;
                            padding-top: 15px;
                            margin: 0 15px;
                        }

                    </style>
                    <div class="panel-body" style="min-height:36em;">

                        <div class="breadcrumb flat text-center row">
                            <span class="active col-md-6 col-sm-6 col-xs-12">Edit Payment</span>
                            <span class="col-md-6 col-sm-6 col-xs-6 hidden-xs">Review And Pay </span>
                        </div>

                        <br>
                        <p class="clearfix">Please complete your payroll by 00:00 on <b><?php echo date('d M', strtotime($contractors[0]->runPayroll)); ?></b>. Your contractors will be paid on <b><?php echo date('d M', strtotime($contractors[0]->payday)); ?></b>.</p>

                        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                            <div class="col-md-2 col-sm-2 col-xs-2"><b>Contractor Id</b></div>
                            <div class="col-md-5 col-sm-5 col-xs-5"><b>Name</b></div>
                            <div class="col-md-2 col-sm-2 col-xs-2"><b></b></div>
                            <div class="col-md-3 col-sm-3 col-xs-3"><b>Gross Payment</b></div>
                        </div>
                        
                        <div class="clearfix"></div>
                        <br>

                        <!-- ======================  contractor ============================= -->
                          <?php foreach ($contractors as $key => $value):?>   
                        <div class="contractor small panel panel-info">
                            <div class="panel-heading  text-center col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-2 col-sm-2 col-xs-3"><h4><?php echo $value->id; ?></h4></div>
                                <div class="col-md-6 col-sm-6 col-xs-7"><h4><?php echo $value->firstName . ' ' . $value->lastName; ?></h4></div>
                                <div class="col-md-2 col-sm-2 col-xs-2"></div>
                                <div class="col-md-2 col-sm-2 hidden-xs"><h5 class="grossPayHead"><i class="fa fa-inr"></i> <span><?php echo $value->grossPay; ?></span></h5><i class="fa fa-caret-down"></i></div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="panel-body col-md-12 col-sm-12 col-xs-12">
                                <div>
                                    <form class="form-horizontal">
                                        <input type="hidden" class="userId" name="userId" value="<?php echo $value->id;?>">
                                           
                                        <div class="clearfix"></div>
                                        <div class="col-md-3">
                                            <h6>
                                                <div class="form-group form-group-sm label-floating">
                                                    <label for="typeOfPayment" class="control-label">Type of Payment</label>

                                                    <select name="typeOfPayment" class="form-control typeOfPayment" disabled>
                                                        <?php 
                                                        if (strtolower($value->earning) == "hour") {
                                                            echo '<option value="hour">Hourly Rate</option>';
                                                        }
                                                        elseif (strtolower($value->earning) == "monthly") {
                                                            echo '<option value="month">Monthly</option>';   
                                                        }
                                                        elseif (strtolower($value->earning) == "project") {
                                                            echo '<option value="project">Project</option>';
                                                        }
                                                     ?>
                                                    </select>
                                                </div>
                                            </h6>
                                            <h6>
                                                <div class="form-group form-group-sm label-floating">
                                                    <label class="control-label" for="amount"><span>Amount</span></label>
                                                        <input class="form-control amount" name="amount" type="text" style="cursor: auto;" placeholder="" disabled value="<?php 
                                                    if (isset($value->ctc)) {
                                                        echo $value->ctc;
                                                    } else {
                                                        echo '0';
                                                    }
                                                     ?>">
                                                        <br>
                                                        <span class="amount-paid">
                                                         <?php if ((strtolower($value->earning) == 'project')&&(isset($value->paid))) {
                                                            echo "Amount Paid: " . $value->paid;
                                                     } 
                                                    ?>
                                                        </span>
                                                </div>
                                            </h6>
                                            <h6>
                                            <?php 
                                            if (($value->modeOfPayment == 'direct deposit')||($value->modeOfPayment == 'direct')) {
                                                echo "Direct Deposit";
                                            }
                                            elseif ($value->modeOfPayment == 'cash / cheque') {
                                                echo "Cash / Cheque";
                                            }
                                            elseif ($value->modeOfPayment == 'cash') {
                                                echo "Cash";
                                            }
                                            elseif ($value->modeOfPayment == 'cheque') {
                                                echo "Cheque";
                                            }
                                        ?>
                                            </h6>
                                        </div>
                                        <div class="col-md-3">
                                            <h6>
                                                <div class="typeHandle form-group form-group-sm label-floating is-empty">
                                                    <label class="control-label" for="noOfHour"><span>No Of Hours</span></label>
                                                        <input class="form-control noOfHour" name="noOfHour" type="number" min="0" style="cursor: auto;" placeholder="">
                                                </div>

                                                <div class="typeHandle form-group form-group-sm label-floating is-empty hidden">
                                                    <label class="control-label" for="noOfLeave"><span>No Of Leave</span></label>
                                                        <input class="form-control noOfLeave" name="noOfLeave" type="number" min="0" style="cursor: auto;" placeholder="">
                                                </div>

                                                <div class="typeHandle form-group form-group-sm label-floating is-empty hidden">
                                                    <label class="control-label" for="shareOfAmount"><span>Share Of Amount</span></label>
                                                        <input class="form-control shareOfAmount" name="shareOfAmount" type="number" min="0" style="cursor: auto;" placeholder="">
                                                </div>
                                            </h6>
                                            <h6>
                                                <div class="form-group form-group-sm label-floating is-empty">
                                                    <label class="control-label" for="overtime"><span><i class="fa fa-plus"></i> Overtime</span></label>
                                                    <input class="form-control overtime" name="overtime" type="number" min="0" style="cursor: auto;" placeholder="">
                                                </div>
                                            </h6>
                                        </div>
                                        <div class="col-md-3">
                                            <h6>
                                                <div class="form-group form-group-sm label-floating is-empty">
                                                    <label class="control-label" for="bonus"><span><i class="fa fa-plus"></i> Bonus</span></label>
                                                    <input class="form-control bonus" name="bonus" type="number" style="cursor: auto;" placeholder="">
                                                </div>
                                            </h6>
                                            <h6>
                                                <div class="form-group form-group-sm label-floating is-empty">
                                                    <label class="control-label" for="commission"><span><i class="fa fa-plus"></i> Commission</span></label>
                                                    <input class="form-control commission" name="commission" type="number" style="cursor: auto;" placeholder="">
                                                </div>
                                            </h6>
                                            <h6 >
                                                <div class="form-group form-group-sm label-floating is-empty">
                                                    <label class="control-label" for="otherEarning"><span><i class="fa fa-plus"></i> Other Earnings</span></label>
                                                    <input class="form-control otherEarning" name="otherEarning" type="number" style="cursor: auto;" placeholder="">
                                                </div>
                                            </h6>
                                        </div>
                                        <div class="col-md-3">
                                            <h6>
                                                <div class="form-group form-group-sm label-floating is-empty">
                                                    <label class="control-label" for="grossPay"><span><i class="fa fa-inr"></i> Gross Pay</span></label>
                                                    <input class="form-control grossPay" name="grossPay" type="number" style="cursor: auto;" placeholder="" disabled>
                                                </div>
                                            </h6>
                                            <h6>
                                                <div class="form-group form-group-sm label-floating is-empty">
                                                    <label class="control-label" for="reimbursement"><span><i class="fa fa-plus"></i> Reimbursement</span></label>
                                                    <input class="form-control reimbursement" name="reimbursement" type="number" style="cursor: auto;" placeholder="">
                                                </div>
                                            </h6>
                                            <h6>
                                                <div class="form-group form-group-sm label-floating is-empty">
                                                    <label class="control-label" for="deduction"><span><i class="fa fa-minus"></i> Deduction</span></label>
                                                    <input class="form-control deduction" name="deduction" type="number" style="cursor: auto;" placeholder="">
                                                </div>
                                            </h6>
                                        </div>
                                        <div class="clearfix"></div>
                                    </form>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                         <?php endforeach; ?>
                        <!-- ============================================================= -->
                        <div class="clearfix"></div>
                        <br>
                        <br>
                        <!-- <ul class="pagination">
                            <li class="disabled"><a href="javascript:void(0)"><i class="fa fa-arrow-left"></i></a></li>
                            <li class="active"><a href="javascript:void(0)">1</a></li>
                            <li><a href="javascript:void(0)">2</a></li>
                            <li><a href="javascript:void(0)">3</a></li>
                            <li><a href="javascript:void(0)">4</a></li>
                            <li><a href="javascript:void(0)">5</a></li>
                            <li><a href="javascript:void(0)"><i class="fa fa-arrow-right"></i></a></li>
                        </ul> -->
                        <div class="clearfix"></div>
                        <br>
                        <br>
                    </div>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>
$(function() {
    
    // add active class to corresponding link in menu 
    $(".nav li.active").removeClass('active');
    $(".nav.nav-stacked li").filter(function(index) {
        return $(this).data('url') == 'payContractor';
    }).addClass('active');
    
    $.material.init();

    var contractor_target = $(".contractor").first(); // last changed form
    var current_target = $(document);  // clicked target form or document

    $(".contractor .panel-body").hide();

    $(".contractor .panel-heading").click(function(event) {
        $(this).siblings('.panel-body').slideToggle();
        var $arrow = $(this).find('i.fa').toggleClass('fa-caret-down fa-caret-up');
    });

    $(".typeOfPayment").change(function(event) {
        event.preventDefault();
        var val = $(this).val();
        var $contractor = $(this).parents(".contractor");

        $contractor.find(".typeHandle").addClass('hidden');
        var $noOfHour = $contractor.find(".noOfHour");
        var $noOfLeave = $contractor.find(".noOfLeave");
        var $shareOfAmount = $contractor.find(".shareOfAmount");

        $noOfHour.val(0);
        $noOfLeave.val(0);
        $shareOfAmount.val(0);

        switch (val) {
            case 'hour':
                $noOfHour.parents(".form-group").removeClass('hidden');
                $noOfHour.trigger('change');
                break; 
            case 'month':
                $noOfLeave.parents(".form-group").removeClass('hidden');
                $noOfLeave.trigger('change');
                break; 
            case 'project':
                $shareOfAmount.parents(".form-group").removeClass('hidden');
                $shareOfAmount.trigger('change');
                break;
            default :
                console.log("not Possible");
                break; 
        }
    });

    var lastMonth = "2016-3";

    $(".noOfLeave").change(function(event) {
        event.preventDefault();
        var noOfDays = new Date(lastMonth[0], lastMonth[1], 0).getDate();

        var val = $(this).val()*1;
        $(this).val( Math.min(Math.max( val, 0), noOfDays));
    });

    $(".contractor .form-horizontal input").change(function(event) {
        $(this).parents(".form-horizontal").data('changed', true);
        contractor_target = $(event.target).parents('.contractor') || current_target;
    });

    $(document).click(function(event) {
        current_target = $(event.target).parents('.contractor');
        if (current_target.data('id') != contractor_target.data('id'))
            sendData(contractor_target);
    });

    $(".contractor .form-horizontal input").change(function(event) {
        $(this).parents(".form-horizontal").data('changed', true);
    });

    function sendData($contractor) {
        if (!contractor_target) return false;
        var $form = $contractor.find('.form-horizontal');
        if ($form.data('changed')) {

            $contractor.find('.panel-heading').css('background-color', '#cd3d3d');

            $.ajax({
                url: location.href,
                type: 'POST',
                data: $form.serialize(),
            })
            .done(function(data) {
                if (data) {
                    $contractor.find('.grossPayHead span').text(data.grossPay);
                    $form.find('.grossPay').val(data.grossPay).trigger('change');
                    $form.find('.deduction').val(data.deduction).trigger('change');
                    $contractor.find('.panel-heading').css('background-color', '#5cb85c');
                    $form.data('changed', false);
                    contractor_target = [];
                }
            })
            .fail(function() {
                console.log("error");
            });
        }
    }
})
</script>
