<!DOCTYPE html>
<html>
<head>
     <title>Sashtechs - India’s best HR Platform, Payroll Software, Health Benefits Insurance Company</title>

    <meta name="description" content="Sashtechs provides payroll, benefits and employer health insurance to small businesses in the India. Founded in 2016 in Delhi, we currently serve over thousands of clients ranging from startups from small businesses" />

    <meta name="keywords" content="Paycheck, Paystub, payslip management software, form16 generation, payroll software, group Health Employees, Group Health Insurance, Time attendance tracking, Expense reporting" />

    <!--Import Google Icon Font-->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
    <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet"></link>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Material Design -->
    <link href="css/bootstrap-material-design.css" rel="stylesheet">
    <link href="css/ripples.min.css" rel="stylesheet">
    <link href="css/roundslider.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">

    <script src="/js/jquery-1.12.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ripples.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/roundslider.min.js"></script>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <style>
        button#submit[disabled]{
            color: #fff;
        }
    </style>
</head>

<body>
<?php require_once("staticNavBar.php");?>

<!-- ================  Header  ===================== -->

<header class="parallax" style="background-image: url(images/aboutUs.jpg);background-position: center">
    <div class="panel panel-default" style="  ">
        <div class="panel-body">
            <div class="text-center">
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <h5>ABOUT US</h5>
                <h2>Simple, reliable and effective payroll, benefits and more.</h2>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
            </div>
        </div>
    </div>
</header>

<section style="background:#fff;margin-top: -20px">
    <div class="container-fluid">
        <div class="row">
            <div class="container">
                <br>
                <br>
                <br>
                    <blockquote class="text-muted">
                        Sashtechs provides small and medium-sized companies with intuitive, cloud-based onboarding, payroll, benefits, expenses and time-tracking application with the goal of enabling businesses to be more efficient and to make smarter decisions about their people and processes.
                    </blockquote>

                    <br>
                    <br>

                    <blockquote class="text-muted">
                        We at Sashtechs, we believe in simplification of the process of payroll & Benefits. We are a team of Payroll experts and professionals and a very responsive customer services which serves as an icing on the cake. We help the users through every step of the setting up payroll, setup taxes. Our teams consists of highly qualified professionals including CA’s, Payroll experts, tax experts, tax return preparation, accountants and innovative IT professionals who work day in and day out to make the process of payroll management simpler and hassle free. We aim for sky high standards and diligently work at grass root level to provide the best services to the users.
                    </blockquote>

                    <br><br>

                    <blockquote class="text-muted">
                        Sashtechs the right mix of young and dynamic professionals from the fields of finance and technology, Our team of professionals have been in this profession since 1975 and helping people to manage thier company payroll and benefits in India. We aim for standards as high as sky and we diligently work to provide you the best customer service. Our motto is Maximum Return in Minimum Time.
                    </blockquote>
                <br>
            </div>
        </div>
        <br><hr><br>
    </div>
</section>

<div class="clearfix"> </div>

<section class="parallax" style="background-image: url(images/try.jpg);">
    <div class="panel panel-default" style="  ">
        <div class="panel-body">
            <div class="text-center">
                <br>
                <br>
                <br>
                <br>
                <h5><b>TRY Sashtechs</b></h5>
                <h3 class="text-uppercase">Start your one month free trial </h3>
                <br>

                <div class="panel text-left" id="panel-text-left">
                    <?php require 'demoForm.php'; ?>
                </div>
                <br>
                    <div class="clearfix"></div>
                    <br><br>
                    <br><br>
            </div>
        </div>
    </div>
</section>

<?php require_once('staticFooter.php');?>

</body>
</html>

<script>
    $(function() {
        $.material.init();
    });
</script>
