<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>


<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">
                        
                        <br>
                        <h3>One Time Payment</h3>
                        <br>
                        <?php if ((isset($payments)) and (count($payments)>0)): ?>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>Team</th>
                                    <th>User Id</th>
                                    <th>Type</th>
                                    <th>Amount</th>
                                </tr>
                                 <?php foreach ($payments as $key => $value):?>
                                <tr>
                                    <td><?php echo $value->team; ?></td>
                                    <td><?php echo $value->userId; ?></td>
                                    <td><?php echo $value->type; ?></td>
                                    <td><?php echo $value->amount; ?></td>
                                </tr>
                                <?php endforeach; ?>
                            </table>
                        </div>
                        <br>
                        <div class="clearfix"></div>
                        <br>
                         <?php endif; ?>

                        <form action="#" class="form-horizontal">
                            
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-6 col-sm-10 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label for="team" class="control-label">Team</label>

                                        <select id="team" name="team" class="form-control">
                                        <?php if (isset($teams)): ?>
                                            <option>Please select a team</option>
                                            <?php foreach ($teams as $key => $value):?>
                                                <option><?php echo $value->team; ?></option>
                                            <?php endforeach; ?>
                                        <?php else: ?>
                                            <option disabled>You don't have any teams yet</option>
                                        <?php endif; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-6 col-sm-10 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label for="type" class="control-label">Type</label>

                                        <select id="type" name="type" class="form-control">
                                            <option>Choose type of payment</option>
                                            <option>Expenses</option>
                                            <option>Bonus</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-6 col-sm-10 col-xs-12">
                                    <div class="form-group label-floating is-empty ">
                                        <label class="control-label" for="amount"><span>Amount</span></label>
                                        <input class="form-control" id="amount" name="amount" type="number" style="cursor: auto;">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <div class="radio radio-primary">
                                            <label>
                                                <input type="radio" id="split" name="distribute" value="split" checked="">
                                                    Split equally among all employee 
                                            </label>
                                        </div>
                                        <div class="radio radio-primary">
                                            <label>
                                                <input type="radio" id="equal" name="distribute" value="equal">
                                                Per Head
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="clearfix"></div>
                            <br><br>

                            <div class="table-responsive">
                                <table class="table table-bordered" id="employeeTable">
                                    <tr>
                                        <th>Employee Id</th>
                                        <th>Employee Name</th>
                                        <th>Amount</th>
                                        <th></th>
                                    </tr>
                            <?php if(isset($details)): ?>
                                <?php foreach ($details as $key => $value) : ?>
                                    <tr>
                                        <td><?php echo $value->id; ?></td>
                                        <td><?php echo $value->firstName . ' ' . $value->lastName; ?></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                                </table>
                            </div>

                            <br>
                            <br>

                            <span class="btn btn-default add"><i class="fa fa-plus"></i> Add Another Employee</span>
                            <span class="btn btn-raised bg-theme submit"></i> Submit</span>
                        </form>
                    </div>

                    <style>
                        .form-horizontal .form-group{
                            margin: 0;
                        }
                        .table tr td.delete {
                            padding-top: 25px;
                        }
                    </style>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>

    function displayError() {
        swal({
            title : "Error !",
            text : 'Try Again!',
            type : 'error',
            confirmButtonClass : 'bg-theme',
            animation : false,
            width : 400
        });
    };

    function closeAlert() {
        $(".sweet-alert button").first().trigger('click');
    }

    $(function() {
        $.material.init();

        var e_count = 0;
        
        function addEmployee(id, firstName, lastName) {
            var id = id == undefined ? " " : id;
            var name = firstName == undefined ? " " : firstName + ' ' + lastName;
            $html = $('<tr>'+
                            '<td>'+
                                '<div class="form-group">'+
                                   '<input class="form-control employeeId"  id= "employeeId'+e_count+'" type="text" name="employeeId'+e_count+'">'+
                                '</div>'+
                            '</td>'+
                            '<td>'+
                                '<div class="form-group">'+
                                    '<input class="form-control employeeName"  id= "employeeName'+e_count+'" type="text" name="employeeName'+e_count+'">'+
                                '</div>'+
                            '</td>'+
                            '<td>'+
                                '<div class="form-group">'+
                                    '<input class="form-control employeeAmount" id= "employeeAmount'+e_count+'" type="number" name="employeeAmount'+e_count+'">'+
                                '</div>'+
                            '</td>'+
                            '<td class="delete"><i class="fa fa-times"></i></td>'+
                        '</tr>');

            $("#employeeTable").append($html);

            $html.find('.employeeId').val(id);
            $html.find('.employeeName').val(name);

            $html.find('.delete').click(function(event) {
                event.preventDefault();
                deleteEmployee.call(this);
            });
            e_count++;
        }

        function distribute() {
            var amount = 1*$("#amount").val() | 0;
            var equal = $("#equal").is(':checked');
            var payAmount = 0;
            var noOfEmployee = $("#employeeTable tr").length - 1;

            if (equal) payAmount = amount;
            else payAmount = Math.floor( amount/noOfEmployee);

            $(".employeeAmount").each(function(index, el) {
                $(el).val(payAmount);
            });
        }

        $("#amount, .radio input").change(function(event) {
            distribute();
        });

        $(".form-horizontal .add").click(function(event) {
            event.preventDefault();
            addEmployee();
        });

        function deleteEmployee() {
            $(this).parent().remove();
            distribute();
        }

        $(".form-horizontal .submit").click(function(event) {
            event.preventDefault();

            swal({
                title : 'Saving',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 400
            });

            var count = $('#employeeTable.table tr').length-1;

            $.ajax({
                url: location.href,
                type: 'POST',
                data: $('.main form').serialize()+'&varcount='+count+'',
            })
            .done(function(data) {
                console.log(data);
                if (data.status == 200) {
                    swal({
                        title : "Success !",
                        text : data.message,
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });
                }
                else {
                    displayError();
                }
            })
            .fail(function() {
                console.log("error");
                displayError();
            })
        });

        $("#team").change(function(event) {
            // clear old fields
            $('.table.table-bordered tr').not(':first').remove();
            // reset counter
            e_count = 0;
            
            var val = $(this).val();
            swal({
                title : 'Fetching data',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 400
            });
            $.ajax({
                url: location.href+'/'+val,
                type: 'POST',
                data: {team: val},
            })
            .done(function(data) {
                for (var i = 0; i < data.length; i++) {
                    // console.log(data[i].id, data[i].firstName, data[i].lastName);
                    addEmployee(data[i].id, data[i].firstName, data[i].lastName);
                }
                distribute();
                closeAlert();
            })
            .fail(function() {
                console.log("error");
                displayError();
            })            
        });
    })
</script>
