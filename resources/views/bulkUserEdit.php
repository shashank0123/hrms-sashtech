<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>


<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">
                        
                        <br>
                        <form action="#" enctype="multipart/form-data" class="form-horizontal col-md-8 col-sm-12 col-xs-12">
                            <legend>Edit Multiple User at a time</legend>
                            <br>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <a href="/userEdit.xlsx" class="btn btn-info btn-raised" download>download Template</a>
                            </div>

                            <div class="clearfix"></div>
                            <br>
                            <br>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group is-empty is-fileinput">
                                    <label for="uploadFile" class="col-md-3 control-label">File</label>

                                    <div class="col-md-8">
                                        <input type="text" readonly="" class="form-control" placeholder="upload your excel or csv file">
                                        <input type="file" id="uploadFile" multiple="">
                                    </div>
                            </div>

                            <div class="clearfix"></div>
                            <br><br>
                            <span id="upload" class="btn btn-raised bg-theme">Upload</span>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>
    $(function() {

        // add active class to corresponding link in menu 
        $(".nav li.active").removeClass('active');
        $(".nav.nav-stacked li").filter(function(index) {
            return $(this).data('url') == 'bulkFeature';
        }).addClass('active');

        $.material.init();
        
        $("#upload").click(function(event) {
            event.preventDefault();

            var $files = $("#uploadFile")[0].files;
            if ($files.length) {
                var formData = new FormData();
                formData.append('uploadFile', $files[0]);

                $.ajax({
                    url: location.href,
                    type: 'POST',
                    data: formData,
                    enctype: 'multipart/form-data',
                    processData: false,
                    contentType: false 
                })
                .done(function(data) {
                    console.log("success", typeof(data), data);
                })
                .fail(function() {
                    console.log("error");
                });                      
            }
        });
    });
</script>
