<!DOCTYPE html>
<html>
<head>
    <title>HR Software, Payroll, Health Benefits, Time tracking, Expense reporting India</title>
    <meta name="description" content="Sashtechs is free online HR Software that gives your team a single place to manage all your human resource needs - payroll, benefits, time, expense and more." />

    <meta name="keywords" content="hr software, hr platform, hr management system, payroll software, Benefits, Group Health Insurance, Time tracking, Expense reporting, hrms software, payroll software free, leave & attendance management system" />

    <!--Import Google Icon Font-->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">

    <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet"></link>


    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Material Design -->
    <link href="css/bootstrap-material-design.css" rel="stylesheet">
    <link href="css/ripples.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">

    <script src="js/jquery-1.12.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ripples.min.js"></script>
    <script src="js/material.min.js"></script>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    <link rel="stylesheet" type="text/css" href="css/slick.css"/>
    <link rel="stylesheet" type="text/css" href="css/slick-theme.css"/>
    <style>
        .companyContent {
            padding: 0;
            padding-top: 20px;
            padding-bottom: 30px;
            margin-top: -20px;
            margin-bottom: -40px;
            background: #FFFFFF;
        }
        .companyQuote{
            background: #EEE;
            padding: 0px 0px 10px 0px;
            margin-bottom: 0;
        }
        .companyQuote img{
            margin: 0 auto;
            padding: 10px;
            transition: all 0.3s ease;
            height: 5em;
        }
        .companyQuote img:hover{
            transform: scale(1.2);
            transform-origin: center;
        }
        .arrow-up{
            width: 4em;
            height: 4em;
            z-index: -10;
            background: #EEE;
            transform: translateY(30px) rotate(45deg);
        }
        .cards {
            height: 18em;
            transition: all 0.3s ease-in-out;
            border: 0 solid #ddd;
            border-left: 1px dotted #ddd;
            border-top: 1px dotted #ddd;
            padding: 1em;
            box-sizing: border-box;
            position: relative;
            color: #333;
        }
        .cards:nth-of-type(3n) {
            border-right: 1px dotted #ddd;
        }
        .cards:last-child {
            border-bottom: 1px dotted #ddd;
        }
        .cards:nth-of-type(4), .cards:nth-of-type(5), .cards:nth-of-type(6) {
            border-bottom: 1px dotted #ddd;
        }
        .cards:hover {
            text-decoration: none;
            background: #f8f8f8;
        }
        .cards span i {
            border: 1px solid #CF3D3D;
            border-radius: 50%;
            margin: 10px;
            color: #CF3D3D;
            width: 70px;
            height: 70px;
            line-height: 65px;
        }
        .cards .center-block {
            padding: 10px;
            width: 10em;
            border: 1px solid #CF3D3D;
            color: #CF3D3D;
            font-size: 11px;
            border-radius: 2px;
            letter-spacing: 1px;
            transition: all 0.3s ease-in-out;
        }
        .cards .center-block:hover {
            background: #CF3D3D;
            color: #fff;
        }
        .cards h6:last-child {
            position: absolute;
            bottom: 0;
            width: 100%;
            left: 0;
        }

        .videoContainer {
            padding: 2em;
            background: rgba(0, 0, 0, 0.67);
            color: #fff;
        }
        .videoContainer .play {
            position: relative;
            width: 150px;
            height: 150px;
            display: block;
            border: 5px solid;
            text-align: center;
            border-radius: 75px;
            box-sizing: border-box;
            padding: 30px 48px;
            text-shadow: 0 0 8px #fff;
            box-shadow: 0 0 4px #fff;
            cursor: pointer;
            font-size: 20px;
            transition: all 0.3s ease-in-out;
        }
        .play:hover {
            text-shadow: 0 0 15px #fff;
        }

        #demo_video {
            position: fixed;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            padding-top: 15%;
            z-index: 1000;
            display: none;
            opacity: 0;
            transition: all 1s ease-in-out;
        }

        #demo_video.visible {
            display: block;
            opacity: 1;
        }

        #demo_video .overlay{
            position: absolute;
            width: 100%;
            height: 100%;
            left: 0;
            top: 0;
            z-index: -1;
            background-color: rgba(0 , 0, 0, 0.6);
        }

        button#submit[disabled]{
            color: #fff;
        }

        @media screen and (max-width: 768px) {
            .cards:nth-of-type(n) {
                border-right: 1px dotted #ddd;
            }
            .cards:nth-of-type(4), .cards:nth-of-type(5) {
                border-bottom: none;
            }

            #demo_video {
                padding-top: 10em;
            }
            #demo_video iframe {
                padding: 0;
                width: 100%;
            }
        }
        @media screen and (min-width: 700px) {
            .companyQuote img{
                max-width: 10em;
            }
        }
    </style>
</head>

<body>
<?php require_once("staticNavBar.php");?>

<!-- ================  Header  ===================== -->

<style>
    #parallax1 {background-image: url(images/home.jpg);}
</style>

<section class="parallax" id="parallax1">
    <div class="panel panel-default" style="  ">
        <div class="panel-body">
            <div class="text-center">
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <h1>Payroll and Benefits which loves Employees</h1>
                <br>
                <h4 class="text-uppercase">Payroll, Benefits, time and more. Connected and on auto-pilot. You’ll simply love it. </h4>
                <br>

                <?php if (!empty($result)): ?>
                <pre>
                    <?php var_dump($result); ?>
                </pre>
                <?php endif ?>

                <div class="panel text-left" id="panel-text-left">
                    <form action="#" class="form-horizontal" method="POST">
                        <div class="col-md-12">
                            <div class="form-group label-floating is-empty">
                                <label class="control-label" for="EmployerName">Full Name</label>
                                <input class="form-control" id="EmployerName" name="EmployerName" required type="text" id="cursor-auto">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group label-floating is-empty">
                                <label class="control-label" for="supportmail">Email (work email) </label>
                                <input class="form-control" id="supportmail" name="supportmail" required type="email" id="cursor-auto">
                            </div>
                        </div>
                        
                        <div class="toggle">
                            <div class="col-md-12">
                                <div class="form-group label-floating ">
                                    <label class="control-label" for="password"><span>Password</span></label>
                                    <input class="form-control" id="password" type="password" required name="password" style="cursor: auto;" autocomplete="off">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group label-floating ">
                                    <label class="control-label" for="confpassword"><span>Confirm Password</span></label>
                                    <input class="form-control" id="confpassword" type="password" required name="confpassword" style="cursor: auto;" autocomplete="off">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group label-floating ">
                                    <label class="control-label" for="companyName"><span>Company Name</span></label>
                                    <input class="form-control" id="companyName" type="text" required name="companyName" style="cursor: auto;" autocomplete="off">
                                </div>
                            </div>
                        </div>

                        <button id="submit" class="btn bg-theme btn-raised btn-block">TRY 1 MONTH FREE</button>
                        <h6 class="text-center text-primary">Start a free trial today. Cancel any time, no commitments.</h6>
                    </form>
                </div>
                <br>
                <div class="clearfix"></div>
                <br><br>
                <br><br>
            </div>
        </div>
    </div>
</section>

<section class="companyContent text-center">
    <br>
    <h3>Small Business Finance SaaS Provider Sashtechs Launches Payroll & Benefits Service .</h3>
    <br>
</section>

<div class="center-block arrow-up"></div>

<div class="companyQuote">
    <div class="text-center">
        <img class="img-responsive" alt="PR-Newswire" src="images/pr-newswire.jpg">
        <div class="content hidden">
            <h3>Sashtechs gives Startups & Businesses A One-Stop Shop For Finding And Managing Employee Payroll & Benefits</h3>
        </div>
    </div>
    <div class="text-center">
        <img class="img-responsive" alt="India-TV" src="images/India-TV.png">
        <div class="content hidden">
            <h3>Sashtechs Launches Cloud-Based Payroll & Benefits Service to aim startups</h3>
        </div>
    </div>
    <div class="text-center">
        <img class="img-responsive" alt="P8" src="images/p8.jpg">
        <div class="content hidden">
            <h3>Small Business Finance SaaS Provider Sashtechs Launches Payroll & Benefits Service .</h3>
        </div>
    </div>
    <div class="text-center">
        <img class="img-responsive" alt="PR-Newswire" src="images/pr-newswire.jpg">
        <div class="content hidden">
            <h3>Sashtechs gives Smartups & Businesses A One-Stop Shop For Finding And Managing Employee Payroll & Benefits</h3>
        </div>
    </div>
    <div class="text-center">
        <img class="img-responsive" alt="India-TV" src="images/India-TV.png">
        <div class="content hidden">
            <h3>Sashtechs Launches Cloud-Based Payroll & Benefits Service to aim startups</h3>
        </div>
    </div>
    <div class="text-center">
        <img class="img-responsive" alt="P8" src="images/p8.jpg">
        <div class="content hidden">
            <h3>Small Business Finance SaaS Provider Sashtechs Launches Payroll & Benefits Service .</h3>
        </div>
    </div>
</div>

<style>
    #section-white-3em {background: #fff;padding-top: 3em;}
</style>

<section id="section-white-3em">

    <div class="container">

        <br>
        <a href="/payroll" class="col-md-4 col-sm-4 col-xs-12 cards text-center">
            <span><i class="fa fa-users fa-2x"></i></span>
            <h4>Payroll</h4>
            <h6>Simplify your payroll. Paying your employees has never been easier</h6>
            <h6><span class="center-block">LEARN MORE</span></h6>
        </a>
        <a href="/health-insurance-benefit" class="col-md-4 col-sm-4 col-xs-12 cards text-center">
            <span><i class="fa fa-heart fa-2x"></i></span>
            <h4>Benefits</h4>
            <h6>Setup a new health benefits or keep your existing plans and carriers in minutes</h6>
            <h6><span class="center-block">LEARN MORE</span></h6>
        </a>
        <a href="/expense" class="col-md-4 col-sm-4 col-xs-12 cards text-center">
            <span><i class="fa fa-inr fa-2x"></i></span>
            <h4>Expenses</h4>
            <h6>Expense reporting that rocks. Use our mobile app to upload your receipts</h6>
            <h6><span class="center-block">LEARN MORE</span></h6>
        </a>
        <a href="timeSheet" class="col-md-4 col-sm-4 col-xs-12 cards text-center">
            <span><i class="fa fa-clock-o fa-2x"></i></span>
            <h4>Time tracking</h4>
            <h6>The Smart way to manage time for your employees & fully integrated with payroll.</h6>
            <h6><span class="center-block">LEARN MORE</span></h6>
        </a>
        <a href="#" class="col-md-4 col-sm-4 col-xs-12 cards text-center">
            <span><i class="fa fa-list-alt fa-2x"></i></span>
            <h4>Business Intelligence</h4>
            <h6>Dashboard that provide real-time insight into your organization.</h6>
            <h6><span class="center-block">LEARN MORE</span></h6>
        </a>
        <a href="/tools" class="col-md-4 col-sm-4 col-xs-12 cards text-center">
            <span><i class="fa fa-cog fa-2x"></i></span>
            <h4>Tools</h4>
            <h6>Our comprehensive calculators and tools can help your employees make smarter, more-informed financial plan</h6>
            <h6><span class="center-block">LEARN MORE</span></h6>
        </a>
    </div>
    <div class="clearfix"></div>
    <br>
    <br>
    <br>
    <style>
    #div-gradient-white {background: linear-gradient( 14deg, #CF3D3D 44%, rgb(191, 52, 52) 20%);color: #fff;}
    </style>
    <div id="div-gradient-white" >
        <div class="container">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <h2>Smart companies deserve Smart HR Platform</h2>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <style>
                #letter-spacing-1 {letter-spacing: 1px;}
                </style>
                <p id="letter-spacing-1" >
                    <br>
                    Our complete HR platform includes payroll, benefits, time and expense management which replaces the complex, process driven world of HR Management with one product that simplifies and improves every aspect of how companies take care of their people.
                    <br>
                    <br>
                    <a href="/payroll" class="btn btn-raised">Learn More</a>
                </p>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="parallax" id="parallax1">
        <div class="videoContainer">
            <br>
            <br>
            <div class="container">
                <br>
                <div class="col-md-7 col-sm-6 col-xs-12 text-center">
                    <h1>Sashtechs Overview </h1>
                    <br>
                    <h5>Everything you need to know about Sashtechs in 2 minute.</h5>

                    <br>
                    <span class="btn btn-raised bg-theme hidden-xs watch">Watch Now</span>
                </div>

                <div class="col-md-5 col-sm-6 col-xs-12">
                    <br>
                        <span class="play center-block">
                                <i class="fa fa-play fa-4x"></i>
                        </span>
                    <br>
                </div>
                <div class="clearfix"></div>
            </div>
            <br>
            <br>
        </div>
    </div>

    <div class="clearfix"></div>
    <br>
</section>

<div id="demo_video">
    <iframe id="popupVideo" class="col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-12" width="560" height="315" data-src="https://www.youtube.com/embed/tlV8SaY4dXI?rel=0?enablejsapi=1" frameborder="0" allowfullscreen allowscriptaccess="always" src=""></iframe>
    <div class="overlay"></div>
</div>

<div class="clearfix"></div>


<?php require_once('staticFooter.php');?>
<script type="text/javascript" src="js/slick.min.js"></script>
</body>
</html>

<script>

    $(function() {
        $.material.init();

        $('.companyQuote').slick({
            autoplay : true,
            autoplaySpeed : 3000,
            arrows : false,
            centerMode : true,
            dots : false,
            infinite : true,
            pauseOnHover : false,
            adaptiveHeight: false,
            slidesToShow : 3,
            draggable : false,
            responsive: [
                {
                    breakpoint: 500,
                    settings: {
                        slidesToShow: 1,
                    }
                },
            ],
        });

        $('.companyQuote').on('afterChange', function(event, slick, currentSlide){
            $(".companyContent").html("<br />" + $($(".content").get(currentSlide)).html() + "<br />");
        });

        var popupVideo = $("#popupVideo");

        $(".videoContainer .play, .videoContainer .watch").click(function(event) {
            popupVideo.attr('src', popupVideo.data('src'));
            $("#demo_video").toggleClass('visible');
        });

        $("#demo_video .overlay").click(function(event) {
            popupVideo.attr('src', '');
            $("#demo_video").toggleClass('visible');
        });

        $(".toggle").hide();

        $("#supportmail").on('keyup change', function(event) {

            if (!$(".toggle").is(':visible')) {
                $(".toggle").slideDown();
            }

            var val = $(this).val().split('@');
            if (val.length<2) return false;
            val = val.pop();
            $("#companyName").val(val.split('.')[0].toUpperCase());
            $("#companyName").trigger('change');
        });

        function passwordVerification() {
            // remove all error messages
            $(".help-block").remove();
            $(".has-error").removeClass('has-error');
            
            var response = true;
            $("#password, #confpassword").each(function(index, el) {
                if (!$(this).val()) {
                    $(this).parents(".label-floating").addClass('has-error');
                    $(this).parent().append('<p class="help-block"> password required</p>');
                    response = false;
                }
                else if ($(this).val().length < 8) {
                    $(this).parents(".label-floating").addClass('has-error');
                    $(this).parent().append('<p class="help-block"> password is less than 8 characters</p>');
                    response = false;
                }
            });

            if (!response) return response;

            if ($("#password").val()!=$("#confpassword").val()) {
                $("#confpassword").parents(".label-floating").addClass('has-error');
                $("#confpassword").parent().append('<p class="help-block"> passwords did not match</p>');                
                return false
            };

            return true;
        }

        function displayError() {
            $("form.form-horizontal .text-primary").text("Please Try Again");
        }

        function clearError () {
            $("form.form-horizontal #submit").text("TRY 1 MONTH FREE"); 
            $('form.form-horizontal #submit').prop('disabled', false);
            $("form.form-horizontal .text-primary").text("");
        } 

        $("form.form-horizontal").submit(function(event) {
            $(this).find('.text-primary').text("");
            $(this).find('#submit').text("Please Wait ...");

            if ($(this).find('#submit').prop('disabled')) return false;

            event.preventDefault();

            if (!passwordVerification()) {
                displayError();
                return false;
            }
            // remove all error messages
            $(".help-block").remove();
            $(".has-error").removeClass('has-error');
            
            $(this).find('#submit').prop('disabled', true);

            $.ajax({
                url: '/signup',
                type: 'POST',
                data: $('.form-horizontal').serialize(), 
            })
            .done(function(data) {
                if (data.status == 200) {
                    clearError();
                    $("form.form-horizontal .text-primary").text(data.message);
                    $('form.form-horizontal').trigger('reset');
                    $('form.form-horizontal .label-floating').addClass('is-empty');
                    $(".toggle").hide();
                }
                else if(data.status == 208) {  //  email already Exist
                    clearError();
                    var $email = $("#supportmail");
                    $email.parent().append('<p class="help-block">'+ data.message +'</p>');
                    $email.focus();
                    $email.parents('.form-group').addClass('has-error');
                }
                else if(data.status == 209) {  //  email already Exist
                    clearError();
                    var $companyName = $("#companyName");
                    $companyName.parent().append('<p class="help-block">'+ data.message +'</p>');
                    $companyName.focus();
                    $companyName.parents('.form-group').addClass('has-error');
                }
                else if(data.status == 406) {  //  email already Exist
                    clearError();
                    var $email = $("#supportmail");
                    $email.parent().append('<p class="help-block">'+ data.message +'</p>');
                    $email.focus();
                    $email.parents('.form-group').addClass('has-error');
                }
                else{  // Validation Error
                    clearError();
                    displayError();
                    // show Errors
                    for(var fields in data) {
                        $('#'+fields).parents('.form-group').addClass('has-error');
                        $('#'+fields).parent().append('<p class="help-block">'+ data[fields][0] +'</p>');
                    }
                }
            })
            .error(function() {
                clearError();
                displayError();
            });
        });

    });
</script>
