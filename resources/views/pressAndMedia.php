<!DOCTYPE html>
<html>
<head>
    <title>Press Center & Media Resources for Payroll & Health Insurance | Sashtechs</title>

    <meta name="description" content="Get the latest press info on Sashtechs including our most recent press coverage, press releases, and press resources. Article includes HR Software, Payroll, Health Benefits, Health Insurance, Time tracking, TDS filing for Companies, Expense reporting India" />

    <meta name="keywords" content="hr mgt platform, payslip generation, form16 generation, payroll software, Group Health Employees, Group Health Insurance Employer, Time attendance tracking, Expense reporting" />

    <!--Import Google Icon Font-->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
    <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet"></link>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Material Design -->
    <link href="css/bootstrap-material-design.css" rel="stylesheet">
    <link href="css/ripples.min.css" rel="stylesheet">
    <link href="css/roundslider.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">

    <script src="/js/jquery-1.12.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ripples.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/roundslider.min.js"></script>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

</head>

<style>

    #timeline{
        position: relative;
        padding-top: 1px;
        margin-bottom: 3em;
    }
    #line{
        position: absolute;
        border: 2px solid #555;
        height: 10%;
        top: 0;
        left: 50%;
        border-radius: 5px;
    }
    .message-container{
        min-height: 6em;
        padding: 5px;
        margin: 2em;
    }
    .date{
        text-align: right;
        padding-right: 4em;
        padding-top: 10px;
        font-family: FontAwesome;
        font-size: 12px;
        transition:all 0.3s cubic-bezier(0, 1.1, 1, 1.1);
    }
    .message-container .circle{
        position: absolute;
        width: 3em;
        height: 3em;
        background: #fff;
        border-radius: 3em;
        color: red;
        padding:7px;
        box-shadow: 0 0 4px;
        text-align: center;
        transition:all 0.3s cubic-bezier(0, 1.1, 1, 1.1);
    }
    .message-container .tooltip{
        padding: 10px 4em;
        background-color: #fff;
        color: #000;
        border-radius: 5px;
        z-index: 100;
        transition:all 0.3s cubic-bezier(0, 1.1, 1, 1.1);
    }

    .message-container:nth-child(odd) .circle{
        left: -18px;
    }
    .message-container:nth-child(odd) .tooltip{
        left: 5em;
    }
    .message-container:nth-child(odd) .tooltip > i{
        position: absolute;
        left: -10px;
        color: white;
        top: 25%;
    }

    .message-container:nth-child(even) .date{
        float: right;
        text-align: left;
        padding-left: 4em;
        padding-top: 10px;
    }

    .message-container:nth-child(even) .tooltip{
        right: 5em;
    }

    .message-container:nth-child(even) .tooltip > i{
        position: absolute;
        right: -10px;
        color: white;
        top: 25%;
    }
    .message-container:nth-child(even) .circle{
        right: -23px;
    }
    
    .message-container.is-hidden .date{
        opacity: 0;
        visibility: hidden;
    }
    .message-container.is-hidden .circle{
        transform:scale(0.5);
        opacity: 0;
        visibility: hidden;
    }
    .message-container.is-hidden:nth-child(even) .tooltip{
        transform:translateX(-4em);
        opacity: 0;
        visibility: hidden;
    }
    .message-container.is-hidden:nth-child(odd) .tooltip{
        transform:translateX(4em);
        opacity: 0;
        visibility: hidden;
    }

    @media screen and (max-width: 768px) {
        #timeline .date{
            text-align: left;
            margin: 10px 0;
            padding-left: 15px;
        }
        .message-container{
            margin: 2em 1.5em;
        }
        .message {
            margin: 15px 0;
        }
        #timeline #line{
            left: 1.5em;
        }
        #timeline .tooltip{
            left: 4em;
            position: relative;
            padding: 10px 1em;
        }
        #timeline .circle{
            left: -23px;
            top: 0;
        }
        #timeline .tooltip > i{
            left: -10px;
            right: initial;
        }
        #timeline .message-container.is-hidden .tooltip{
            transform:translateX(4em);
            opacity: 0;
            visibility: hidden;
        }
    }

    .img-thumbnail{
        height: 6em;
        width: 7em;
        margin: 15px;
    }

    @media screen and (max-width: 520px) {
        section.container > div >img.img-thumbnail{
            padding: 15px 20px;
        }        
    }

</style>

<body>
<?php require_once("staticNavBar.php");?>

<!-- ================  Header  ===================== -->

<header class="parallax" style="background-image: url(images/startup.jpg);">
    <div class="panel panel-default" style="  ">
        <div class="panel-body">
            <div class="text-center">
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <h5>Press & Media</h5>
                <h3>See what's new with Sashtechs</h3>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <i class="fa fa-arrow-down"></i>
                <br>
                <br>
            </div>
        </div>
    </div>
</header>

<div class="clearfix"> </div>

<section class="container-fluid" style="background:#fff;margin-top: -20px;">
    <div class="container">
        <div class="text-center">

            <br>
            <h3 class="text-uppercase">See what people are saying about Sashtechs</h3>
            <hr>
            <br><br>

            <p>
                <a href="http://www.apsense.com/article/payroll-and-benefits-which-loves-employees.html" class="text-info"><h3 >Sashtechs Launches Cloud-Based Payroll & Benefits Service to aim startups</h3></a>
                <span><i class="fa fa-check"></i>  &nbsp; APSense </span>
            </p>
            <p>
                <a href="https://medium.com/@sashtechs.solutions/payroll-and-benefits-which-loves-employees-d8727fc3779e#.mwyt5vxd2" class="text-info"><h3 >Sashtechs Launches Cloud-Based Payroll & Benefits Service to aim startups</h3></a>
                <span><i class="fa fa-check"></i>  &nbsp; Medium.com </span>
            </p>
            <p>
                <a href="http://www.imfaceplate.com/sashtechs/payroll-and-benefits-which-loves-employees" class="text-info"><h3 >Sashtechs gives Startups A One-Stop Shop For Finding And Managing Employee Payroll</h3></a>
                <span><i class="fa fa-check"></i>  &nbsp; IM faceplate </span>
            </p>
            <p>
                <a href="http://sashtechs-blogs.blogspot.in/2016/04/payroll-and-benefits-which-loves.html" class="text-info"><h3 >Payroll and Benefits which loves Employees</h3></a>
                <span><i class="fa fa-check"></i>  &nbsp; sashtechs-blogs.blogspot.in </span>
            </p>

            <br>
        </div>
    </div>
</section>

<section class="bg-primary" id="milestones">
    <div class="container">
        <h1 class="text-center">Milestones</h1>
        <hr>

        <br>

        <div id="timeline">
            <div id="line"></div>

            <div class="message-container is-hidden">
                <div class="col-md-6 col-sm-6 col-xs-12 date"><b>January , 2016</b></div>
                <div class="col-md-6 col-sm-6 col-xs-12 message">
                    <div class="tooltip">
                        <i class="fa fa-caret-right fa-2x hidden-xs"></i>
                        <i class="fa fa-caret-left fa-2x visible-xs-inline"></i>
                        <h5>Sashtechs founded in Delhi.</h5>
                    </div>
                    <div class="circle"><i class="fa fa-flag fa-2x"></i></div>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="message-container is-hidden">
                <div class="col-md-6 col-sm-6 col-xs-12 date"><b>March , 2016</b></div>
                <div class="col-md-6 col-sm-6 col-xs-12 message">
                    <div class="tooltip">
                        <i class="fa fa-caret-left fa-2x"></i>
                        <h5>Sashtechs released in beta.</h5>
                    </div>
                    <div class="circle"><i class="fa fa-user fa-2x"></i></div>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="message-container is-hidden">
                <div class="col-md-6 col-sm-6 col-xs-12 date"><b>April , 2016</b></div>
                <div class="col-md-6 col-sm-6 col-xs-12 message">
                    <div class="tooltip">
                        <i class="fa fa-caret-right fa-2x hidden-xs"></i>
                        <i class="fa fa-caret-left fa-2x visible-xs-inline"></i>
                        <h5>Sashtechs comes out of beta.</h5>
                    </div>
                    <div class="circle"><i class="fa fa-user fa-2x"></i></div>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="message-container is-hidden">
                <div class="col-md-6 col-sm-6 col-xs-12 date"><b>May , 2016</b></div>
                <div class="col-md-6 col-sm-6 col-xs-12 message">
                    <div class="tooltip">
                        <i class="fa fa-caret-left fa-2x"></i>
                        <h5>going further</h5>
                    </div>
                    <div class="circle"><i class="fa fa-user fa-2x"></i></div>
                </div>
                <div class="clearfix"></div>
            </div>

        </div>
    </div>
</section>

<br>
<br>

<section class="text-center container">
    <h5>Our Customer</h5>
    <br>
    <div class="col-md-4 col-sm-4 col-xs-6"><img src="images/mifa.jpg" alt="logo" class="img-thumbnail"></div>
    <div class="col-md-4 col-sm-4 col-xs-6"><img src="images/avantika.png" alt="logo" class="img-thumbnail"></div>
    <div class="col-md-4 col-sm-4 col-xs-6"><img src="images/sticon-1.png" alt="logo" class="img-thumbnail"></div>
    <div class="col-md-4 col-sm-4 col-xs-6"><img src="images/wembleypaints.png" alt="logo" class="img-thumbnail"></div>
    <!-- <div class="col-md-4 col-sm-4 col-xs-6"><img src="images/auto-logo.png" alt="logo" class="img-thumbnail"></div> -->
    <div class="col-md-4 col-sm-4 col-xs-6"><img src="images/bsb.jpg" alt="logo" class="img-thumbnail"></div>
</section>

<br>
<br>

<section class="parallax" style="background-image: url(images/try.jpg);">
    <div class="panel panel-default" style="  ">
        <div class="panel-body">
            <div class="text-center">
                <br>
                <br>
                <br>
                <br>
                <h5><b>TRY Sashtechs</b></h5>
                <h3 class="text-uppercase">Start your one month free trial </h3>
                <br>

                <div class="panel text-left" id="panel-text-left">
                    <?php require 'demoForm.php'; ?>
                </div>
                <br>
                    <div class="clearfix"></div>
                    <br><br>
                    <br><br>
            </div>
        </div>
    </div>
</section>

<?php require_once('staticFooter.php');?>

</body>
</html>

<script>

    $(function() {
        $.material.init();

        $('body').animate({
            scrollTop: $("#milestones").offset().top
        }, 1000);

        var $totalTime = $(".message-container").length * 600 + 800;

        $("#line").animate({height:"100%"}, $totalTime, "swing");

        $(".message-container").each(function(index, el) {
            $(el).delay(index*600+1000).queue(function() {
                $(this).removeClass('is-hidden');
            });
        });
    });
</script>
