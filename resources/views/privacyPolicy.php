<!DOCTYPE html>
<html>
<head>
    <title> Payroll System for Small Enterprise, Businesses & Startups India</title>

    <meta name="description" content="Enjoy autopilot payroll runs, automatic tax filing, and automatic onboarding of new employees with our platform and mobile app." />

    <meta name="keywords" content="hr software, hr platform, hr management system, payroll software, Benefits, Enterprise payroll, Group Health Insurance, Time tracking, Expense reporting, hrms software, payroll software free, leave & attendance management system" />

    <!--Import Google Icon Font-->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
    <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet"></link>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Material Design -->
    <link href="css/bootstrap-material-design.css" rel="stylesheet">
    <link href="css/ripples.min.css" rel="stylesheet">
    <link href="css/roundslider.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">

    <script src="/js/jquery-1.12.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ripples.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/roundslider.min.js"></script>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

</head>

<body>
<?php require_once("staticNavBar.php");?>

<!-- ================  Header  ===================== -->
<header style="padding: 4em;background: #cf3d3d;"></header>
<section style="background:#fff;padding-top: 2em;">
    <div class="container-fluid">
        <div class="row">
            <div class="container">
                <br>
                <h3>Privacy Policy</h3>
                <hr>
                <p>This Privacy Policy governs the manner in which Sashtechs collects, uses, maintains and discloses information collected from users of the Sashtechs (Zippy Tax Solution Pvt. Ltd) website (“Site”). This privacy policy applies to the Site and all products and services offered by Sashtechs.</p>
                <br>

                <h3>Personal identification information</h3>
                <br>
                <p>We may collect personal identification information from Users in a variety of ways, including, but not limited to, when Users visit our site, register on the site, fill out a form, and in connection with other activities, services, features or resources we make available on our Site. <br>Users may be asked for, as appropriate, name, email address, mailing address, phone number, credit card information and PAN Number. Users may, however, visit our Site anonymously. We will collect personal identification information from Users only if they voluntarily submit such information to us. Users can always refuse to supply personally identification information, except that it may prevent them from engaging in certain Site related activities.</p>
                
                <br>

                <h3>Non-personal identification information</h3>
                <br>
                <p>We may collect non-personal identification information about Users whenever they interact with our Site. Non-personal identification information may include the browser name, the type of computer and technical information about Users means of connection to our Site, such as the operating system and the Internet service providers utilized and other similar information.</p>

                <h3>Web browser cookies</h3>
                <br>
                <p>Our Site may use “cookies” to enhance User experience. The User’s web browser places cookies on their hard drive for record-keeping purposes and sometimes to track information about them. User may choose to set their web browser to refuse cookies, or to alert you when cookies are being sent. If they do so, note that some parts of the Site may not function properly.</p>

                <br>

                <h3>How we use collected information</h3>
                <br>
                <p>Sashtechs collects and uses Users personal information for the following purposes:</p>
                <ul>
                    <li><b>To personalize user experience</b> <br> <p>We may use information in the aggregate to understand how our Users as a group use the services and resources provided on our Site.</p></li>

                    <li><b>To process transactions (New Letters)</b> <br> <p>We may use the information Users provide about themselves when placing an order only to provide service to that order. We do not share this information with outside parties except to the extent necessary to provide the service.</p></li>

                    <li><b>To send periodic emails</b> <br> <p>The email address Users provide for order processing will only be used to send them information and updates pertaining to their order. It may also be used to respond to their inquiries, and/or other requests or questions. If the User decides to opt-in to our mailing list, they will receive emails that may include company news, updates, related product or service information, etc. If at any time the User would like to unsubscribe from receiving future emails, we include detailed unsubscribe instructions at the bottom of each email or User may contact us via our Site.</p></li>

                </ul>

                <br>

                <h3>How we protect your information</h3>
                <br>
                <p>We adopt appropriate data collection, storage and processing practices and security measures to protect against unauthorized access, alteration, disclosure or destruction of your personal information, username, password, transaction information and data stored on our Site. <br>
                Sensitive and private data exchange between the Site and its Users happens over a SSL secured communication channel and is encrypted and protected with digital signatures.</p>

                <br>

                <h3>Sharing your personal information</h3>
                <br>
                <p>We do not sell, trade, or rent Users personal identification information to others. We may share generic aggregated demographic information not linked to any personal identification information regarding visitors and users with our business partners, trusted affiliates and advertisers for the purposes outlined above.</p>

                <br>

                <h3>Changes to this privacy policy</h3>
                <br>
                <p>Sashtechs has the discretion to update this privacy policy at any time. When we do, we will post a notification on the main page of our Site, revise the updated date at the bottom of this page and send you an email. <br> We encourage Users to frequently check this page for any changes to stay informed about how we are helping to protect the personal information we collect. You acknowledge and agree that it is your responsibility to review this privacy policy periodically and become aware of modifications.</p>

                <br>

                <h3>Your acceptance of these terms</h3>
                <br>    
                <p>By using this Site, you signify your acceptance of this policy. If you do not agree to this policy, please do not use our website. Your continued use of the Site following the posting of changes to this policy will be deemed your acceptance of those changes.</p>

                <br>

                <h3>Contacting us</h3>
                <br>
                <p>If you have any questions about this Privacy Policy, the practices of this site, or your dealings with this site, please contact us at: <a href="mailto:support@Sashtechs.com">support@Sashtechs.com</a>.</p>
            </div>
        </div>
        <br><hr><br>
    </div>
</section>

<div class="clearfix"> </div>

<?php require_once('staticFooter.php');?>

</body>
</html>

<script>
    $(function() {
        $.material.init();
    });
</script>
