<!DOCTYPE html>
<html>
<head>
    <title>Payslip</title>
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
    <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet"></link>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Material Design -->
    <link href="css/bootstrap-material-design.css" rel="stylesheet">
    <link href="css/ripples.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">

    <script src="js/jquery-1.12.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ripples.min.js"></script>
    <script src="js/material.min.js"></script>


    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

</head>

<style>
    .section{
        width:55em;
        padding: 1em;
        margin: 0 auto;
    }

    .table{
        border-collapse: collapse;
        border: 1px solid #000;
    }
    .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th{
        border-color:#000;
        padding-left: 10px;
    }

    .table tr td:nth-child(odd){
        font-weight: 400;
    }

    #last tr td{
        font-weight: 100;
    }

    #last tr td:nth-child(3n+1){
        font-weight: 400;
    }

    .bg-theme1{
        background: #FC6558;
        color: #fff;
    }

    #logo > div:first-child{
        border-top: 1px solid #aaa;
    }

    .no-border{
        border: none !important;
    }

    @media print {
        .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th{
            border-color:#000 !important;
        }

        table{
            border-collapse: collapse !important;
        }

        table td.bg-theme1,table th.bg-theme1,table tr.bg-theme1{
            background: #FC6558 !important;
            color: #fff !important;
        }
        button.btn{
            display: none !important;
        }
        i.fa-heart{
            color: #f44336 !important;
        }
        .table-bordered>tbody>tr>td.no-border{
            border: none !important;
        }
    }

</style>
<body  style="margin-top:1em;background:#fff">
<?php        
    $ctc = array(
        'Basic' => round(($result2->basic/12)), 
        'House Rent Allowance' => round(($result2->hra/12)), 
        'Conveyance' => round(($result2->conveyance/12)), 
        'Medical Allowance' => round(($result2->medicalAllowance/12)), 
        'medical Insurance' => round(($result2->medicalInsurance/12)), 
        'Telephone Allowance' => round(($result2->telephone/12)), 
        'Leave Travel Allowance' => round(($result2->leaveTravel/12)), 
        'Uniform Allowance' => round(($result2->uniform/12)), 
        'Gratuity Allowance' => round(($result2->gratuity/12)), 
        'Super Annuation Allowance' => round(($result2->superAnnuation/12)), 
        'Annual Bonus Allowance' => round(($result2->annualBonus/12)), 
        'Festival Bonus Allowance' => round(($result2->festivalBonus/12)), 
        'Incentives ' => round(($result2->incentives/12)), 
        'Other ' => round(($result2->others/12)), 
        'Leave Encashment Allowance' => round(($result2->leaveEncashment/12)), 
        );
    $basicSum=round(($result2->sum/12));

    if (!empty($result4) and isset($result4)) 
    {
        $deduction = array(
            '0' => array(
                'key' => 'EPF',
                'value' => $result4->epf,
                ),
            '1' => array(
                'key' => 'ESI',
                'value' => $result4->esi,
                ),
            '2' => array(
                'key' => 'TDS',
                'value' => $result4->tds,
                ),
            '3' => array(
                'key' => 'Professional Tax',
                'value' => $result4->pt,
                ),
            'total' => $result4->epf + $result4->esi + $result4->tds + $result4->pt,
            );
    }
    else 
    {
        $deduction = 0;
    }
?>
    <div class="section">
        <span id="logo" class="fa btn btn-lg" >
         <div>
             <img src="<?php echo $_SERVER['HTTP_HOST'] . '/' . $result5->logoUrl; ?>" alt="Company Logo">
         </div>
         </span>
        
        <br>

        <table class="table table-bordered table-condensed" cols="4">
            
            <tr >
                <th class="bg-theme1" colspan="4"><?php echo $result5->companyName; ?></th>
            </tr>
            
            <tr><th class="text-center" colspan="4">Payslip For <?php echo isset($result4->month) ? $result4->month : ''; ?> <?php echo isset($result4->financialYear) ? $result4->financialYear : '' ; ?></th></tr>

            <tr>
                <td class="bg-theme1" width="150">Name</td>
                <td> <?php echo $result1->firstName . ' ' . $result1->lastName; ?> </td>
                <td class="bg-theme1" width="150">PAN</td>
                <td><?php echo $result->PAN; ?></td>
            </tr>
            <tr>
                <td class="bg-theme1">Employee code</td>
                <td> <?php echo $result1->id; ?> </td>
                <td class="bg-theme1">PF Number</td>
                <td><?php echo $result->PFAcNo; ?></td>
            </tr>
            <tr>
                <td class="bg-theme1">Designation</td>
                <td><?php echo $result1->title; ?></td>
                <td class="bg-theme1">No of Days</td>
                <td>
                    <?php 
                        if (isset($result4) and !empty($result4)) 
                        {
                            $totalDays = cal_days_in_month(CAL_GREGORIAN, intval(date('m', strtotime($result4->month))), date('Y'));

                            echo ($totalDays - $result4->noOfLeave);
                        }
                     ?>
                </td>
            </tr>
            <tr>
                <td class="bg-theme1">D.O.J</td>
                <td><?php echo date('d/m/y', strtotime($result1->joinDate)); ?></td>
                <td class="bg-theme1">Leave</td>
                <td><?php echo isset($result4->noOfLeave) ? $result4->noOfLeave : '' ; ?></td>
            </tr>
            <tr>
                <td class="bg-theme1">Location</td>
                <td><?php echo $result5->city; ?></td>
                <td class="bg-theme1">EL / CL</td>
                <td>(what goes here?)</td>
            </tr>
        </table>

        <br>

        <table class="table table-bordered table-condensed" cols="4">
            <tr ><th class="text-center bg-theme1" colspan="4">Transfer Summary</th></tr>
            <tr>
                <th width="200">Earning</th>
                <th>Arrears</th>
                <th>Deductions</th>
                <th width="200">Take Home Amount</th>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>Cheque / Transfer / Paytm</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>

        <table id="last" class="table table-bordered table-condensed" cols="5">
            <tr>
                <th class="bg-theme1">Earnings</th>
                <th class="bg-theme1">Amount (<i class="fa fa-inr"></i>) </th>
                <th class="bg-theme1">Arrears</th>
                <th class="bg-theme1">Deductions</th>
                <th class="bg-theme1">Amount (<i class="fa fa-inr"></i>)</th>
            </tr>
            <?php $c = 0; ?>
            <?php foreach($ctc as $key => $value): ?>
            <tr>
                <td class="no-border"><?php echo $key; ?></td>
                <td class="no-border"><?php echo $value; ?></td>
                <td></td>
                <?php if($c < (count($deduction)-1)): ?>
                <td class="no-border"><?php echo $deduction[$c]['key']; ?></td>
                <td class="no-border"><?php echo $deduction[$c]['value']; ?></td>
                <?php else: ?>
                <td class="no-border"></td>
                <td class="no-border"></td>
                <?php endif; ?>
                <?php $c++; ?>
            </tr>
            <?php endforeach; ?>
            <tr>
                <td class="bg-theme1">Earnings Total</td>
                <td><?php echo $basicSum; ?></td>
                <td></td>
                <td class="bg-theme1">Deductions Total</td>
                <td><?php echo $deduction['total']; ?></td>
            </tr>
        </table>
        <div class="heart-beat">Designed with <i class="fa fa-heart text-danger"></i> Sashtechs</div>
        <br>
        <button class="btn btn-raised bg-theme" onclick="print()">PRINT</button>
    </div>


</body>
</html>

<script>
    $.material.init();
</script>
