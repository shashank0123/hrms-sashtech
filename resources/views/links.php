 <!--Import Google Icon Font-->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
    <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet"></link>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Material Design -->
    <link href="css/bootstrap-material-datetimepicker.css" rel="stylesheet">
    <link href="css/bootstrap-material-design.css" rel="stylesheet">
    <link href="css/ripples.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.6.0/fullcalendar.min.css">
    <link rel="stylesheet" href="css/sweetalert2.css">
    <link rel="stylesheet" href="css/jquery-ui.min.css">
    <link href="css/admin.css" rel="stylesheet">

    <script src="js/jquery-1.12.0.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ripples.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/moment-with-locales.min.js"></script>
    <script src="js/bootstrap-material-datetimepicker.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.6.0/fullcalendar.min.js"></script>
    <script src="js/sweetalert2.min.js"></script>


    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    


<style>
    .nav.navbar li>a{
        text-transform: uppercase;
        font-size: 11px;
        color: #3E3E3E;
        font-weight: 400;
        padding-left: 0;
        text-align: left;
    }

    .heart-beat:hover .fa-heart{
        -webkit-animation: beat infinite 0.7s ease-in-out;
        -o-animation: beat infinite 0.7s ease-in-out;
        animation: beat infinite 0.7s ease-in-out;
    }

    .fa-lg{
        width: 25px;
        margin-left: 5px;
    }

    #logo{
        padding-top: 15px;
        padding-bottom: 15px;
        background: #fff;
        margin-top: 0;
    }
    #logo > div:first-child{
        border-bottom: 1px solid;
        margin:0 auto;
        width: 6em;
    }

    @-webkit-keyframes beat {
        0% {transform:scale(1);}
        50% {transform:scale(1.25);}
        100% { transform:scale(1);}
    }
    @-o-keyframes beat {
        0% {transform:scale(1);}
        50% {transform:scale(1.25);}
        100% { transform:scale(1);}
    }
    @-moz-keyframes beat {
        0% {transform:scale(1);}
        50% {transform:scale(1.25);}
        100% { transform:scale(1);}
    }
    @keyframes beat {
        0%{transform:scale(1);}
        50% {transform:scale(1.25);}
        100% { transform:scale(1);}
    }

    .form-group{
        margin-top: 15px;
    }

    @media screen and (max-width: 768px) {
        .nav.navbar li>a{
            text-align: center;
        }
        .fa-lg{
            float: left;
        }
        #logo{
            display: block;
        }
    }
    
    .showbox {
        width: 100%;
        height: 100%;
        position: absolute;
        left: 0;
        top: 40%;
    }

    .loader {
        position: relative;
        margin: 0px auto;
        width: 100px;
    }

    .loader:before {
        content: '';
        display: block;
        padding-top: 100%;
    }

    .circular {
        -webkit-animation: rotate 2s linear infinite;
        animation: rotate 2s linear infinite;
        height: 100%;
        -webkit-transform-origin: center center;
        transform-origin: center center;
        width: 100%;
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
    }

    .path {
        stroke-dasharray: 1,200;
        stroke-dashoffset: 0;
        -webkit-animation: dash 1.5s ease-in-out infinite, color 6s ease-in-out infinite;
        animation: dash 1.5s ease-in-out infinite, color 6s ease-in-out infinite;
        stroke-linecap: round;
    }

    @-webkit-keyframes rotate {
        100% {
            -webkit-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }

    @keyframes rotate {
        100% {
            -webkit-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }

    @-webkit-keyframes dash {
        0% {
            stroke-dasharray: 1,200;
            stroke-dashoffset: 0;
        }
        50% {
            stroke-dasharray: 89,200;
            stroke-dashoffset: -35px;
        }
        100% {
            stroke-dasharray: 89,200;
            stroke-dashoffset: -124px;
        }
    }

    @keyframes dash {
        0% {
            stroke-dasharray: 1,200;
            stroke-dashoffset: 0;
        }
        50% {
            stroke-dasharray: 89,200;
            stroke-dashoffset: -35px;
        }
        100% {
            stroke-dasharray: 89,200;
            stroke-dashoffset: -124px;
        }
    }

    @-webkit-keyframes color {
        100%, 0% {
            stroke: #d62d20;
        }
        40% {
            stroke: #0057e7;
        }
        66% {
            stroke: #008744;
        }
        80%, 90% {
            stroke: #ffa700;
        }
    }

    @keyframes color {
        100%, 0% {
            stroke: #d62d20;
        }
        40% {
            stroke: #0057e7;
        }
        66% {
            stroke: #008744;
        }
        80%, 90% {
            stroke: #ffa700;
        }
    }

</style>

<script>

    function getAndInsert (href) {

        var loader = '<div class="panel-body" style="min-height:36em;"><div class="showbox"><div class="loader"><svg class="circular" viewBox="25 25 50 50"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg></div></div></div>';

        $(".main").html(loader);
        $.get(location.origin + href, function(data) {

            $(".main").html($(data).find(".main").html());

            $('.main').find("a.ajaxify").on('click', function(event) {

                var href = $(this).attr('href');
                
                if (!href || href=="#" || $(this).parent().hasClass('disabled')) return false;
                
                $(".nav li.active").removeClass('active');
                $(this).parent().addClass('active');

                event.preventDefault();

                var loader = '<div class="panel-body" style="min-height:36em;"><div class="showbox"><div class="loader"><svg class="circular" viewBox="25 25 50 50"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg></div></div></div>';

                $(".main").html(loader);

                getAndInsert(href);

                history.pushState(null, null, href);
            });
            eval( data.substring( data.lastIndexOf("<script>")+8, data.lastIndexOf("</"+"script>") ));
        }).error(function() {
            errorOccured();
        });
    }

    function errorOccured(){
        var loader = '<div class="panel-body" style="min-height:36em;"><h4 class="text-center"> <i class="fa fa-times"></i>  &nbsp;&nbsp; Something not right</div><div class="showbox"><div class="loader"><svg class="circular" viewBox="25 25 50 50"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg></div></div></div>';
        $(".main").html(loader);
    }



    $(function() {

        $("a.ajaxify").on('click', function(event) {

            var href = $(this).attr('href');
            
            if (!href || href=="#" || $(this).parent().hasClass('disabled')) return false;

            $(".nav li.active").removeClass('active');
            $(this).parent().addClass('active');

            event.preventDefault();

            var loader = '<div class="panel-body" style="min-height:36em;"><div class="showbox"><div class="loader"><svg class="circular" viewBox="25 25 50 50"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg></div></div></div>';

            $(".main").html(loader);

            getAndInsert(href);

            history.pushState(null, null, href);
        });

        // go back and forward
        $(window).on('popstate', function() {

            var loader = '<div class="panel-body" style="min-height:36em;"><div class="showbox"><div class="loader"><svg class="circular" viewBox="25 25 50 50"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg></div></div></div>';

            $(".main").html(loader);

            getAndInsert(location.pathname);
        });
    });
</script>
