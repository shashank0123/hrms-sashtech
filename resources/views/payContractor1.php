<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>
<style>
    hr {
        border-color: #B1A9A9;
    }
</style>

<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">
                         <h2>Pay Contractor</h2><hr>
                <br>
                <center>  
                    <i class="fa fa-cog fa-5x"></i>
                </center>
                <h5 class="text-center">Looks like sommething needs your attention</h5>
                <?php 
                $complete = session()->get('complete');
                if($complete == 'on'): ?>
                    
                    <div class="text-center col-md-8 col-md-offset-2" style="font-size:11px">Before you can continue to pay your contractors, please go to manage contractors page and add at least one contractor. Don't worry, we don't mind waiting a few minutes for you to return. <br> <hr></div>
                
                    <center><a href="/manageContractors" class="ajaxify btn btn-primary btn-raised bg-theme"><i class="fa fa-signal"></i> &nbsp; Manage Contractors</a></center><br>
                
                <?php else: ?>
                    
                    <div class="text-center col-md-8 col-md-offset-2" style="font-size:11px">Before you can continue to pay your contractors, please go to your dashboard and complete the to-do items there. Don't worry, we don't mind waiting a few minutes for you to return. <br> <hr></div>

                    <center><a href="/getStarted" class="ajaxify btn btn-primary btn-raised bg-theme"><i class="fa fa-signal"></i> &nbsp; Return to Dashboard</a></center><br>
                <?php endif; ?>

                    </div>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>
    $(function() {

        // add active class to corresponding link in menu 
        $(".nav li.active").removeClass('active');
        $(".nav.nav-stacked li").filter(function(index) {
            return $(this).data('url') == 'payContractor';
        }).addClass('active');

        $.material.init();
    })
</script>
