<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>

<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;padding-bottom: 1em;position:relative;">
                        <span id="prevYear" class="custom-btn btn bg-theme"><i class="fa fa-backward"></i></span>
                        <span id="prev" class="custom-btn btn bg-theme"><i class="fa fa-chevron-left"></i></span>
                        <span id="today" class="custom-btn btn bg-theme">today</span>
                        <span id="next" class="custom-btn btn bg-theme"><i class="fa fa-chevron-right"></i></span>
                        <span id="nextYear" class="custom-btn btn bg-theme"><i class="fa fa-forward"></i></span>
                        <div id="calendar">
                            <span class="btn bg-theme btn-raised col-md-3 col-sm-4 col-xs-6 save">Save</span>
                        </div>
                    </div>
                    <style>
                        #calendar {
                            position: relative;
                        }
                        .custom-btn.btn{
                            padding: 8px 15px;
                        }
                        .event-btn.btn{
                            padding: 5px 13px;
                            font-size: 16px;
                        }
                        .eventPopup{
                            position: absolute;
                            top: 0;
                            left: 0;
                            z-index: 1000;
                            height: 100%;
                            width: 100%;
                        }
                        .eventPopup > .edit{
                            box-shadow: 0 0 10px #aaa;
                            position: relative;
                            z-index: 1;
                            padding: 1px 10px;
                            width: 20em;
                            margin: 0 auto;
                            top: 40%;
                            background: #fff;
                        }
                        .eventPopup > .overlay{
                            background: rgba(255, 255, 255, 0.75);
                            z-index: 1;
                            position: absolute;
                            top: 0;
                            left:0;
                            width: 100%;
                            height: 100%;
                        }
                        .fc-event { 
                            padding: 1px 5px;
                        }
                    </style>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>


    function closeAlert() {
        $(".sweet-alert button").first().trigger('click');
    }

    function displayError(mesg) {
        swal({
            title : "Error !",
            text : mesg,
            type : 'error',
            confirmButtonClass : 'bg-theme',
            animation : false,
            width : 400
        });
    };

    $(function() {

        // add active class to corresponding link in menu 
        $(".nav li.active").removeClass('active');
        $(".nav.nav-stacked li").filter(function(index) {
            return $(this).data('url') == 'calendar';
        }).addClass('active');

        //  ===================================================================================
        var deleteHoliday = function(id) {
            var events = HOLIDAY.events;
            var index = events.findIndex(function(index) {
                return index.id == id;
            });

            var done = HOLIDAY.events.splice(index, 1);
            if (done) return true;
            else return false;
        }
        // ===================================================================================
        var addHoliday = function(event) {
            delete event.backgroundColor;
            HOLIDAY.events.push(event);
        }
        // =====================================================================================
        var updateEvent = function(changedEvent, title) {
            var index = HOLIDAY.events.findIndex(function(index) {
                return index.id == changedEvent.id;
            });

            HOLIDAY.events[index].start = changedEvent.start.format();
            HOLIDAY.events[index].end = changedEvent.end.format();
        }
        // =====================================================================================

        var prepareFinalData = function() {
            HOLIDAY.editable = false;
        }

        // =====================================================================================

        
        $.material.init();

        var $calendar = $('#calendar');
        var currentEvent = null;
        var $eventPopup = null;

        var EVENTS = null;
        var HOLIDAY = [];
        var WEEKLEAVE = [];
        var PAYDAY = [];


        function initializeCalendar() {

            $calendar.fullCalendar({

                editable: true,
                selectable:true,
                selectHelper:true,
                eventLimit: true,
                dragScroll:true,

                header: {
                    left: '',
                    center: 'title',
                    right: ''
                },

                dayRender : function(date, cell) {
                    if (WEEKLEAVE && WEEKLEAVE.indexOf(date.day()) != -1) {
                        cell.css('backgroundColor', '#f5f5f5');
                    }
                },
                
                eventClick: function(event, jsEvent, view) {
                    var currentEvent = event;

                    if (event.editable == false || event.source.editable == false) return false;
                    else {
                        $eventPopup = createPopup('remove');

                        // assigning default values and id of event
                        $eventPopup.find('#id').val(currentEvent.id);
                        $eventPopup.find('.title').text(currentEvent.title);
                        
                        $eventPopup.find('.overlay').click(function() { $eventPopup.remove(); });
                        // close popup if cancel is fired
                        $eventPopup.find('.cancel').click(function() { $eventPopup.remove(); });
                        // delete event
                        $eventPopup.find('.delete').click(function() { 
                            $eventPopup.remove();
                            deleteHoliday(currentEvent.id);
                            $calendar.fullCalendar('removeEvents', currentEvent.id);
                        });
                    }
                },

                select : function(start, end) {
            
                    $eventPopup = createPopup('custom');

                    // assigning default values and id of event
                    var newEvent = new Object();
                    newEvent.id = Date.now();
                    newEvent.title = "";
                    newEvent.start = start;
                    newEvent.end = end;

                    $eventPopup.find(".add").click(function(e) {
                        if (!$eventPopup.find('.newTitle').val()) {
                            $eventPopup.find('.cancel').trigger('click');
                            return false;
                        }
                        newEvent.title = $eventPopup.find('.newTitle').val();
                        newEvent.backgroundColor = '#FC6558';
                        $calendar.fullCalendar('renderEvent', newEvent, true);
                        addHoliday(newEvent);
                        $eventPopup.remove();
                    });
                    // close event Popup
                    $eventPopup.find('.overlay').click(function() { $eventPopup.remove(); delete newEvent});
                    $eventPopup.find('.cancel').click(function() { $eventPopup.remove(); delete newEvent});
                },

                eventDrop: function(event, delta, revertFunc) {
                    currentEvent = event;
                    currentEvent.backgroundColor = '#FC6558';
                    updateEvent(currentEvent);
                },

                eventResize : function(event, jsEvent, ui, view) {
                    currentEvent = event;
                    currentEvent.backgroundColor = '#FC6558';
                    updateEvent(currentEvent);
                },

                windowResize: function(view) {
                    if ($(window).width() < 400){
                        $calendar.fullCalendar('option', 'aspectRatio', 0.75);
                    }
                }
            });
        }

        // Responsive initialization
        if ($(window).width() < 400){
            $calendar.fullCalendar('option', 'aspectRatio', 0.75);
        }

        $("#next").click(function(event) {
            $("#calendar").fullCalendar('next');
        });
        $("#prev").click(function(event) {
            $("#calendar").fullCalendar('prev');
        });
        $("#today").click(function(event) {
            $("#calendar").fullCalendar('today');
        });
        $("#prevYear").click(function(event) {
            $("#calendar").fullCalendar('prevYear');
        });
        $("#nextYear").click(function(event) {
            $("#calendar").fullCalendar('nextYear');
        });

        function createPopup(operation) {

            var $html = "";
            if (operation == 'custom') {
                var $html = '<div class="eventPopup">'+
                    '<div class="overlay"></div>'+
                    '<div class="edit">'+
                        '<form>'+
                            '<div class="h6">New Event<span class="cancel pull-right"><i class="fa fa-times"></i></span></div>'+
                            '<input type="hidden" class="id">'+
                            '<input type="text" class="newTitle"/> <br />'+
                            '<span class="event-btn btn btn-xs bg-theme  pull-right change add"><i class="fa fa-check"></i></span>'+
                            '<span class="event-btn btn btn-xs bg-theme cancel"><i class="fa fa-times"></i></span>'+
                        '</form>'+
                    '</div>'+
                '</div>'
            }
            else {
                $html = '<div class="eventPopup">'+
                    '<div class="overlay"></div>'+
                    '<div class="edit">'+
                        '<form>'+
                            '<div class="h6">Event : <span class="title"></span><span class="cancel pull-right"><i class="fa fa-times"></i></span></div>'+
                            '<input type="hidden" class="id">'+
                            '<h5>Sure Delete this Event ?</h5>'+
                            '<span class="event-btn btn btn-xs bg-theme  pull-right delete"><i class="fa fa-check"></i></span>'+
                            '<span class="event-btn btn btn-xs bg-theme cancel"><i class="fa fa-times"></i></span>'+
                        '</form>'+
                    '</div>'+
                '</div>'
            }
            // adding popup
            var eventForm = $($html)
            $calendar.append(eventForm);
            $.material.init();
            return eventForm;
        };

        $calendar.find('.save').click(function(event) {
            event.preventDefault();
            swal({
                title : 'Saving Events',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 400
            });

            prepareFinalData();

            $.ajax({
                url: '/calendardata',
                type: 'POST',
                dataType: 'json',
                data: {events : JSON.stringify(EVENTS)},
            })
            .done(function(data) {
                if (data == 1) {
                    swal({
                        title : "Success !",
                        text : "Events Successfully Saved",
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });
                }
                else {
                    displayError('Try Again !');
                }
            })
            .fail(function() {
                displayError('Try Again!');
            })                                
        });

        function fetchEvents() {
            swal({
                title : 'Loading Events',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 400
            });
            $.get('/companyCalendar', function(data) {
                EVENTS = JSON.parse(data);

                WEEKLEAVE = EVENTS.weekleave;
                HOLIDAY = EVENTS.holiday;
                PAYDAY = EVENTS.payday;

                initializeCalendar();

                if (HOLIDAY) {
                    HOLIDAY.editable = true;
                    $calendar.fullCalendar( 'addEventSource', HOLIDAY);
                }
                if (PAYDAY) {
                    $calendar.fullCalendar( 'addEventSource', PAYDAY);
                }
                console.log("Events loaded Successfully");
                closeAlert();
            }).error(function() {
                displayError('Try Again!');
                initializeCalendar();
            });
        }

        fetchEvents();
    });
</script>
