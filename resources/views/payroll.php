<!DOCTYPE html>
<html>
<head>
     <title>Free Payroll Software for Small Businesses & Startups India</title>

     <meta name="description" content="Enjoy autopilot payroll runs, automatic tax filing, and automatic onboarding of new employees with our platform and mobile app." />


    <meta name="keywords" content="hr software, hr platform, hr management system, payroll software, Benefits, Group Health Insurance, Time tracking, Expense reporting, hrms software, payroll software free, leave & attendance management system" />

    <!--Import Google Icon Font-->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
    <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet"></link>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Material Design -->
    <link href="css/bootstrap-material-design.css" rel="stylesheet">
    <link href="css/ripples.min.css" rel="stylesheet">
    <link href="css/roundslider.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">

    <script src="/js/jquery-1.12.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ripples.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/roundslider.min.js"></script>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    
    <style>
        .feature {
            margin: 1em;
            border: 1px solid #ddd;
            padding: 1em;
            height: 17em;
            box-shadow: 0 0 3px #ddd;
        }
    </style>

</head>

<body>
<?php require_once("staticNavBar.php");?>

<!-- ================  Header  ===================== -->

<header class="parallax" style="background-image: url(images/payroll.jpg);background-position: center">
    <div class="panel panel-default" style="  ">
        <div class="panel-body">
            <div class="text-center">
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <h5>PAYROLL</h5>
                <h2>Run payroll in minutes.</h2>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
            </div>
        </div>
    </div>
</header>

<section style="background:#fff;margin-top: -20px">
    <div class="container-fluid">
        <div class="row">
            <div class="container">
                <br>
                <br>
                <h1 class="text-center text-muted">Run payroll and file taxes in just a few clicks.</h1>
                <br>
                <p class="text-center h4 text-muted">Benefits, Expenses, time-tracking, and more—to your run payroll seamlessly. <br>Every change is instantly updated, so sit back, relax, and watch payroll happen.</p>
                
                <br>
                <hr>
                <br>

                <h1 class="text-center">Why Sashtechs</h1>
                <h3 class="text-center">We built a suite of services to simplify your payroll.</h3>
                <br>
                <br>

                <div class="clearfix"></div>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="center-block feature text-center">
                        <i class="fa fa-users fa-3x"></i>
                        <br>
                        <h4>Payroll for everyone</h4>
                        <br>
                        <p>We give employers a total control over their payroll through an intuitive, friendly online interface & mobile application. It provide helpful tips in every step.</p>
                        <br>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="center-block feature text-center">
                        <i class="fa fa-clock-o fa-3x"></i>
                        <br>
                        <h4>Fully integrated timecards</h4>
                        <br>
                        <p>Employee time management is taken care by Sashtechs app. Days/Hours are imported automatically so you can easily manage payroll online.</p>
                        <br>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="center-block feature text-center">
                        <i class="fa fa-calculator fa-3x"></i>
                        <br>
                        <h4>Takes care of taxes</h4>
                        <br>
                        <p>Sashtechs payroll services handle payroll taxes including withholdings, payments, and filings.</p>
                        <br>
                    </div>
                </div>
                
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="center-block feature text-center">
                        <i class="fa fa-thumbs-up fa-3x"></i>
                        <br>
                        <h4>Employee self-onboarding</h4>
                        <br>
                        <p>No more voided checks—employees can set up bank details and Personal Details and access their payslip online.</p>
                        <br>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="center-block feature text-center">
                        <i class="fa fa-inr fa-3x"></i>
                        <br>
                        <h4>Simple pricing, no hidden fees</h4>
                        <br>
                        <p>All payroll services, tax filings, withholdings, and payments are included in a simple monthly price.</p>
                        <br>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="center-block feature text-center">
                        <i class="fa fa-smile-o fa-3x"></i>
                        <br>
                        <h4>World-class customer care</h4>
                        <br>
                        <p>Our team is composed of expert, friendly payroll professionals that will help you to setup your account. Give them a call at xxxxxxxxxx and say Hi.</p>
                        <br>
                    </div>
                </div>
            </div>

            <br><hr><br>
        </div>
    </div>
</section>

<div class="clearfix"> </div>

<section class="parallax" style="background-image: url(images/try.jpg);">
    <div class="panel panel-default" style="  ">
        <div class="panel-body">
            <div class="text-center">
                <br>
                <br>
                <br>
                <br>
                <h5><b>TRY Sashtechs</b></h5>
                <h3 class="text-uppercase">Start your one month free trial </h3>
                <br>

                <div class="panel text-left" id="panel-text-left">
                    <?php require 'demoForm.php'; ?>
                </div>
                <br>
                    <div class="clearfix"></div>
                    <br><br>
                    <br><br>
            </div>
        </div>
    </div>
</section>

<?php require_once('staticFooter.php');?>

</body>
</html>

<script>
    $(function() {
        $.material.init();
    });
</script>
