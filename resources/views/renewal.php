<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>

<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">
                        <br>
                        <div class="col-md-8 col-sm-12 col-xs-12">
                            <form class="form-horizontal" method="post">
                                <fieldset>
                                    <legend>Renewal</legend>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating is-empty ">
                                                <label class="control-label" for="detailExpiringPolicy"><span>Details of Expiring Policy</span></label>
                                                <input class="form-control" id="detailExpiringPolicy" type="text" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="renewalDate" class="col-md-5 control-label">Renewal Date</label>

                                            <div class="col-md-7">
                                                <input type="text" name="renewalDate" id="renewalDate" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating is-empty ">
                                                <label class="control-label" for="premiumExpiringPolicy"><span>Premium under expiring policy</span></label>
                                                <input class="form-control" id="premiumExpiringPolicy" type="text" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating is-empty ">
                                                <label class="control-label" for="scopeUnderExpiry"><span>Scope of cover under the expiring policy</span></label>
                                                <input class="form-control" id="scopeUnderExpiry" type="text" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating is-empty ">
                                                <label class="control-label" for="noOfEmployeeUnderExpiry"><span>No. of Employees Covered under Expiring Policy</span></label>
                                                <input class="form-control" id="noOfEmployeeUnderExpiry" type="number" min="0" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating is-empty ">
                                                <label class="control-label" for="noOfDependentUnderExpiry"><span>No. of Dependents Covered under Expiring Policy.</span></label>
                                                <input class="form-control" id="noOfDependentUnderExpiry" type="number" min="0" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating is-empty ">
                                                <label class="control-label" for="claimPaid"><span>Claims Paid/ Incurred under expiring policy</span></label>
                                                <input class="form-control" id="claimPaid" type="number" min="0" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating is-empty ">
                                                <label class="control-label" for="claimExperienceUnderExpiry"><span>Claims experience  % under expiring policy</span></label>
                                                <input class="form-control" id="claimExperienceUnderExpiry" type="number" min="0" max="100" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating is-empty ">
                                                <label class="control-label" for="claimExperienceForLast3Year"><span>Claims Experience % for the last 3 years </span></label>
                                                <input class="form-control" id="claimExperienceForLast3Year" type="number" min="0" max="100" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating is-empty ">
                                                <label class="control-label" for="expectedPremium"><span>Expected Premium / Competitive Premium </span></label>
                                                <input class="form-control" id="expectedPremium" type="number" min="0" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating is-empty ">
                                                <label class="control-label" for="nameOfAgent"><span>Name of the Agent / Broker / Intermediary </span></label>
                                                <input class="form-control" id="nameOfAgent" type="text" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating is-empty ">
                                                <label class="control-label" for="recommendationsOfBC"><span>Recommendations of BC / AO/ ZM </span></label>
                                                <input class="form-control" id="recommendationsOfBC" type="text" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <div class="col-md-8 col-sm-8 col-xs-8 col-md-offset-4 col-sm-offset-3 col-xs-offset-4">
                                                <span class="btn btn-default btn-raised cancel">Cancel</span>
                                                <span class="btn bg-theme btn-raised next">Submit</span>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>
    $(function() {
        $.material.init();

        $('#renewalDate').bootstrapMaterialDatePicker({ weekStart : 0, time: false }).on('dateSelected', function(event) {
           $('#'+$(this).data('dtp')).find('.dtp-btn-ok').trigger('click');
        });
    });
</script>
