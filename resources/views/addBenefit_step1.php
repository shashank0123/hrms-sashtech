<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>


<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">

                        <div class="breadcrumb flat text-center row">
                            <span class="active col-md-6 col-sm-6 col-xs-12">Add Benefits</span>
                            <span class="col-md-6 col-sm-6 hidden-xs">Add Employee To Benefit</span>
                        </div>
                       
                       <form action="" method="POST" enctype="multipart/form-data" class="form-horizontal col-md-12">

                            <br>

                            <div id="preview">
                                <label for="planLogo">CHOOSE LOGO</label>
                                <input type="file" accept="image/*" class="planLogo" name="planLogo" id="planLogo" style="display:none;">
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="form-group label-floating is-empty ">
                                        <label class="control-label" for="planCompany"><span>Company Name</span></label>
                                        <input class="form-control planCompany" id="planCompany" value="<?php if (!empty($benefit->planCompany)) {
                                            echo $benefit->planCompany;
                                        } ?>" name="planCompany" type="text" style="cursor: auto;">
                                    </div>
                                </div>
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p>Details about the Health insurance provider</p>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="form-group label-floating is-empty ">
                                        <label class="control-label" for="planName"><span>Plan Name</span></label>
                                        <input class="form-control planName" id="planName" value="<?php if (!empty($benefit->planName)) {
                                            echo $benefit->planName;
                                        } ?>" name="planName" type="text" style="cursor: auto;">
                                    </div>
                                </div>
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p>Benefit names are displayed on employee paycheck and are for your records</p>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="form-group label-floating is-empty ">
                                        <label class="control-label" for="coverage"><span>Coverage</span></label>
                                        <input class="form-control coverage" id="coverage" value="<?php if (!empty($benefit->coverage)) {
                                            echo $benefit->coverage;
                                        } ?>" name="coverage" type="text" style="cursor: auto;">
                                    </div>
                                </div>
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p>This amount is for which employee is entitled too</p>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="form-group label-floating is-empty ">
                                        <label class="control-label" for="premium"><span>Premium</span></label>
                                        <input class="form-control premium" id="premium" value="<?php if (!empty($benefit->premium)) {
                                            echo $benefit->premium;
                                        } ?>" name="premium" type="text" style="cursor: auto;">
                                    </div>
                                </div>
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p>This amount is deducted from the employee each pay period to pay for this benefit</p>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="form-group label-floating is-empty ">
                                        <label class="control-label" for="sumAssured"><span>Sum Assured</span></label>
                                        <input class="form-control sumAssured" id="sumAssured" value="<?php if (!empty($benefit->sumAssured)) {
                                            echo $benefit->sumAssured;
                                        } ?>" name="sumAssured" type="text" style="cursor: auto;">
                                    </div>
                                </div>
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p>This amount is for employee sum assured</p>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12">
                                    <div class="togglebutton form-group">
                                        <label>
                                            Family Floater Y/N
                                            &nbsp;&nbsp;&nbsp; 
                                            <input type="checkbox"  class="familyFloater" checked="<?php if (!empty($benefit->waitingPeriod)) {
                                            echo $benefit->waitingPeriod;
                                        } ?>" name="familyFloater">
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12">

                                    <div class="form-group" id="familyFloaterOption">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="radio radio-primary col-md-8 col-sm-8 col-xs-12" data-hint="true">
                                                <label>
                                                    <input type="radio" class="familyFloaterOption"  name="familyFloaterOption" value="2" 
                                                     <?php 
                                                            if (empty($benefit->familyFloaterOption) or (intval($benefit->familyFloaterOption) == 2)) 
                                                            {
                                                                echo 'checked';
                                                            }
                                                         ?>

                                                     > 1+1
                                                </label>
                                            </div>
                                            <div class="hint-text col-md-8 text-muted hidden-sm hidden-xs">
                                                <p>self / spouse</p>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="radio radio-primary col-md-8 col-sm-8 col-xs-12" data-hint="true">
                                                <label>
                                                    <input type="radio" class="familyFloaterOption"  name="familyFloaterOption" value="4" 
                                                     <?php 
                                                            if (empty($benefit->familyFloaterOption) or (intval($benefit->familyFloaterOption) == 4)) 
                                                            {
                                                                echo 'checked';
                                                            }
                                                         ?>

                                                    > 1+3
                                                </label>
                                            </div>
                                            <div class="hint-text col-md-8 text-muted hidden-sm hidden-xs">
                                                <p>self spouse two children</p>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="radio radio-primary col-md-8 col-sm-8 col-xs-12" data-hint="true">
                                                <label>
                                                    <input type="radio" class="familyFloaterOption" name="familyFloaterOption" value="6" 
                                                     <?php 
                                                            if (empty($benefit->familyFloaterOption) or (intval($benefit->familyFloaterOption) == 6)) 
                                                            {
                                                                echo 'checked';
                                                            }
                                                         ?>

                                                    > 1+5
                                                </label>
                                            </div>
                                            <div class="hint-text col-md-8 text-muted hidden-sm hidden-xs">
                                                <p>self spouse children parents</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12">
                                    <div class="togglebutton form-group">
                                        <label>
                                            Waiver of 30 days Waiting Period
                                            &nbsp;&nbsp;&nbsp; 
                                            <input type="checkbox" checked="<?php if (!empty($benefit->waitingPeriod)) {
                                            echo $benefit->waitingPeriod;
                                        } ?>" class="waitingPeriod" name="waitingPeriod">
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12">
                                    <div class="togglebutton form-group">
                                        <label>
                                            Waiver of First Year Exclusion
                                            &nbsp;&nbsp;&nbsp; 
                                            <input type="checkbox" name="firstYearExclusion" checked="<?php if (!empty($benefit->firstYearExclusion)) {
                                            echo $benefit->firstYearExclusion;
                                        } ?>" class="firstYearExclusion">
                                        </label>
                                    </div>
                                </div>
                            </div> 

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12">
                                    <div class="togglebutton form-group">
                                        <label>
                                            Waiver of First Two Year Exclusion
                                            &nbsp;&nbsp;&nbsp; 
                                            <input type="checkbox" checked="<?php if (!empty($benefit->twoYearExclusion)) {
                                            echo $benefit->twoYearExclusion;
                                        } ?>" name="twoYearExclusion" class="twoYearExclusion">
                                        </label>
                                    </div>
                                </div>
                            </div>  

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12">
                                    <div class="togglebutton form-group">
                                        <label>
                                            Pre Existing Disease Cover
                                            &nbsp;&nbsp;&nbsp; 
                                            <input type="checkbox" checked="<?php if (!empty($benefit->diseaseCover)) {
                                            echo $benefit->diseaseCover;
                                        } ?>" name="DiseaseCover" class="DiseaseCover">
                                        </label>
                                    </div>
                                </div>
                            </div> 

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12">
                                    <div class="togglebutton form-group">
                                        <label>
                                            Maternity Benefit
                                            &nbsp;&nbsp;&nbsp; 
                                            <input type="checkbox" checked="<?php if (!empty($benefit->maternityBenefit)) {
                                            echo $benefit->maternityBenefit;
                                        } ?>" name="maternityBenefit" class="maternityBenefit" id="maternityBenefit">
                                        </label>
                                    </div>
                                </div>
                            </div>     

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating is-empty ">
                                        <label class="control-label" for="maternityLimit"><span>Maternity Limit</span></label>
                                        <input class="form-control maternityLimit" id="maternityLimit" value="<?php if (!empty($benefit->maternityLimit)) {
                                            echo $benefit->maternityLimit;
                                        } ?>" type="number" min="0" style="cursor: auto;">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12">
                                    <div class="togglebutton form-group">
                                        <label>
                                            Extension of 9 months
                                            &nbsp;&nbsp;&nbsp; 
                                            <input type="checkbox" checked="<?php if (!empty($benefit->extensionNineMonth)) {
                                            echo $benefit->extensionNineMonth;
                                        } ?>" name="extensionNineMonth" class="extensionNineMonth" id="extensionNineMonth">
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12">
                                    <div class="togglebutton form-group">
                                        <label>
                                            Waiver of per-post natal expenses
                                            &nbsp;&nbsp;&nbsp; 
                                            <input type="checkbox" checked="<?php if (!empty($benefit->waiverExpense)) {
                                            echo $benefit->waiverExpense;
                                        } ?>" name="waiverExpense" class="waiverExpense" id="waiverExpense">
                                        </label>
                                    </div>

                                    <div class="form-group label-floating is-empty ">
                                        <label class="control-label" for="waiverAmount"><span>Waiver Amount</span></label>
                                        <input class="form-control waiverAmount" id="waiverAmount" value="<?php if (!empty($benefit->waiverAmount)) {
                                            echo $benefit->waiverAmount;
                                        } ?>" type="number" min="0" style="cursor: auto;">
                                    </div>
                                </div>
                            </div>     

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12">
                                    <div class="togglebutton form-group">
                                        <label>
                                            Child Cover From Day One
                                            &nbsp;&nbsp;&nbsp; 
                                            <input type="checkbox" checked="<?php if (!empty($benefit->childCover)) {
                                            echo $benefit->childCover;
                                        } ?>" name="childCover" class="childCover">
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12">
                                    <div class="togglebutton form-group">
                                        <label>
                                            Co - Pay
                                            &nbsp;&nbsp;&nbsp; 
                                            <input type="checkbox" checked="<?php if (!empty($benefit->copay)) {
                                            echo $benefit->copay;
                                        } ?>" name="copay" class="copay" id="copay">
                                        </label>
                                    </div>
                                </div>
                            </div>     

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating is-empty ">
                                        <label class="control-label" for="copayAmount"><span>Co-Pay Amount</span></label>
                                        <input class="form-control copayAmount" id="copayAmount" value="<?php if (!empty($benefit->copayAmount)) {
                                            echo $benefit->copayAmount;
                                        } ?>" type="number" min="0" style="cursor: auto;">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12">
                                    <div class="togglebutton form-group">
                                        <label>
                                            Corporate Buffer
                                            &nbsp;&nbsp;&nbsp; 
                                            <input type="checkbox" checked="<?php if (!empty($benefit->corporateBuffer)) {
                                            echo $benefit->corporateBuffer;
                                        } ?>" name="corporateBuffer" class="corporateBuffer">
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12">
                                    <div class="togglebutton form-group">
                                        <label>
                                            All day care procedures
                                            &nbsp;&nbsp;&nbsp; 
                                            <input type="checkbox" checked="<?php if (!empty($benefit->allDayCare)) {
                                            echo $benefit->allDayCare;
                                        } ?>" name="allDayCare" class="allDayCare">
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating is-empty ">
                                        <label class="control-label" for="tpa"><span>TPA</span></label>
                                        <input class="form-control tpa" id="tpa" value="<?php if (!empty($benefit->tpa)) {
                                            echo $benefit->tpa;
                                        } ?>" type="text" style="cursor: auto;">
                                    </div>
                                </div>
                            </div>                        

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="col-md-8 col-sm-8 col-xs-8 col-md-offset-4 col-sm-offset-3 col-xs-offset-4">
                                        <span class="btn btn-default btn-raised cancel" onclick="history.back();">Cancel</span>
                                        <input type="submit" class="next btn bg-theme next btn-raised" value="Submit">
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>

                    <style>
                    #preview{
                        background: #fff;
                        position: relative;
                        height: 10em;
                        width: 10em;
                        margin: 10px auto;
                        border: 1px solid #aaa;
                    }

                    #preview > label{
                        margin: 0 auto;
                        display: block;
                        width: 10em;
                        height: 100%;
                        text-align: center;
                        line-height: 9em;
                        background-size: cover;
                    }

                    .togglebutton > label{
                        display: block;
                        text-align: left;
                    }

                    .toggle {
                        float: right;
                    }
                    </style>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>
$(function() {

    // add active class to corresponding link in menu 
    $(".nav li.active").removeClass('active');
    $(".nav.nav-stacked li").filter(function(index) {
        return $(this).data('url') == 'benefit';
    }).addClass('active');
    
    $.material.init();


     $("#planLogo").on('change', function(event) {
        if (event.target.files.length) {
            // $("#preview > label").css('background-image', 'url('+URL.createObjectURL(event.target.files[0])+')');
            // $("#preview").css('border', 'none');
            var file = event.target.files[0];
            if (file && file.size > (500*1024)) {// under 500kb file is allowed
                swal({
                    title : "Error !",
                    text : 'File Size is more than 500 KB',
                    type : 'error',
                    confirmButtonClass : 'bg-theme',
                    animation : false,
                    width : 400
                });
                return false;                    
            }

            if (file && file.type != 'image/png' && file.type != 'image/jpeg') {
                swal({
                    title : "Error !",
                    text : 'only png and jpeg images are allowed',
                    type : 'error',
                    confirmButtonClass : 'bg-theme',
                    animation : false,
                    width : 400
                });
                return false; 
            }
            
            $("#preview > label").css({
                backgroundImage: 'url('+URL.createObjectURL(file)+')',
                color: 'transparent',
                'border' : '1px dotted #ddd'
            });
            $("#preview").css('border', 'none');
        }
        else{
            // $("#preview > label").css('background-image', 'url()');
            // $("#preview").css('border', '1px solid #aaa');
            $("#preview > label").css({
                backgroundImage : 'url()',
                color : '#bdbdbd',
                border : 'none'
            });
            $("#preview").css({
                border: '1px solid #aaa',
            });
        }

    });

                     
    $(".familyFloater").change(function(event) {
        if ($(this).is(':checked')) {
            $("#familyFloaterOption").show();
            $("#familyFloaterOption").find('.familyFloaterOption:eq(0)').prop('checked', true).trigger('change');
        }
        else{
            $("#familyFloaterOption").hide();
            $("#familyFloaterOption").find('.familyFloaterOption:eq(0)').prop('checked', true).trigger('change');
        }
    });
    $("#familyFloaterOption").hide();

    $("#maternityBenefit").change(function(event) {
        if ($(this).is(':checked')) {
            $("#maternityLimit, #extensionNineMonth, #waiverExpense").parents(".form-group").show();
        }
        else {
            $("#maternityLimit, #extensionNineMonth, #waiverExpense").parents(".form-group").hide();
            $("#maternityLimit").val(0).trigger('change');
            $("#extensionNineMonth, #waiverExpense").prop('checked', false).trigger('change');
        }
    });

    $("#waiverExpense").change(function(event) {
        if ($(this).is(':checked')) {
            $("#waiverAmount").parents(".form-group").show();
        }
        else {
            $("#waiverAmount").parents(".form-group").hide();
            $("#waiverAmount").val(0).trigger('change');
        }
    });

    $("#waiverAmount").parents(".form-group").hide();
    

    $("#copay").change(function(event) {
        if ($(this).is(':checked')) {
            $("#copayAmount").parents(".form-group").show();
        }
        else {
            $("#copayAmount").parents(".form-group").hide();
            $("#copayAmount").val(0).trigger('change');
        }
    });

    $("#copayAmount").parents(".form-group").hide();


    $("#maternityLimit, #extensionNineMonth, #waiverExpense").parents(".form-group").hide();

    function getFormData() {
        var formData = new FormData();
        // formData.append('planLogo', $('.planLogo')[0].files[0]);
        var file = $("#planLogo")[0].files[0];
        if (file && file.size <= (500*1024) && (file.type == 'image/png' || file.type == 'image/jpeg'))
            formData.append('planLogo', file);
        else
            formData.append('planLogo', "");
        formData.append('planCompany', $('.planCompany').val());
        formData.append('planName', $('.planName').val());
        formData.append('coverage', $('.coverage').val());
        formData.append('premium', $('.premium').val());
        formData.append('sumAssured', $('.sumAssured').val());
        formData.append('familyFloater', $('.familyFloater').is(':checked'));
        formData.append('familyFloaterOption', $('.familyFloaterOption:checked').val());
        formData.append('waitingPeriod', $('.waitingPeriod').is(':checked'));
        formData.append('firstYearExclusion', $('.firstYearExclusion').is(':checked'));
        formData.append('twoYearExclusion', $('.twoYearExclusion').is(':checked'));
        formData.append('diseaseCover', $('.diseaseCover').is(':checked'));
        formData.append('maternityBenefit', $('.maternityBenefit').is(':checked'));
        formData.append('maternityLimit', $('.maternityLimit').val());
        formData.append('extensionNineMonth', $('.extensionNineMonth').val());
        formData.append('waiverExpense', $('.waiverExpense').is(':checked'));
        formData.append('waiverAmount', $('.waiverAmount').val());
        formData.append('copay', $('.copay').is(':checked'));
        formData.append('copayAmount', $('.copayAmount').val());
        formData.append('childCover', $('.childCover').is(':checked'));
        formData.append('corporateBuffer', $('.corporateBuffer').is(':checked'));
        formData.append('allDayCare', $('.allDayCare').is(':checked'));
        formData.append('tpa', $('.tpa').val());

        return formData;
    }

    function displayError() {
        swal({
            title : "Error !",
            text : 'Try Again!',
            type : 'error',
            confirmButtonClass : 'bg-theme',
            animation : false,
            width : 400
        });
    };

    function closeAlert() {
        $(".sweet-alert button").first().trigger('click');
    }

    $("form.form-horizontal").submit(function(event) {
        event.preventDefault();

        // remove all error messages
        $(".help-block").remove();
        $(".has-error").removeClass('has-error');
        
        swal({
            title : 'Saving',
            html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
            allowOutsideClick : false,
            showConfirmButton : false,
            animation : false,
            width : 400
        });

        $.ajax({
            url: location.href,
            type: 'POST',
            data: getFormData(),
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false 
        })
        .done(function(data) {
            if (data.status == 200) {
                swal({
                    title : "Success !",
                    text : data.message,
                    type : 'success',
                    confirmButtonClass : 'bg-theme',
                    animation : false,
                    width : 400
                });   
                getAndInsert(data.url);
                history.pushState(null, null, data.url);
            }
            else if (data.status == 400) {
                swal({
                    title : "Error !",
                    text : data.message,
                    type : 'error',
                    confirmButtonClass : 'bg-theme',
                    animation : false,
                    width : 400
                });
            }
            else{  // Validation Error
                displayError();
                // show Errors
                for(var fields in data) {
                    $('#'+fields).parents('.form-group').addClass('has-error');
                    $('#'+fields).parent().append('<p class="help-block">'+ data[fields][0] +'</p>');
                }
            }
        })
        .error(function() {
            displayError();
        });
    });

    $(".main .form-horizontal input").trigger('change');
});


</script>
