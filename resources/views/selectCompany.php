<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>

<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">
                        <br>
                        <h3>Companies</h3>
                        <br>

                        <div id="companies">
                            <div class="company col-md-4 col-sm-6 col-xs-12">
                                <div class="content">
                                    <img src="images/testLogo.png" alt="testLogo" class="img-thumbnail center-block">
                                    <br>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et tempore eius velit, ipsam      </p>
                                    <a href="#" class="btn btn-block bg-theme">Select</a>
                                </div>
                            </div>
                            <div class="company col-md-4 col-sm-6 col-xs-12">
                                <div class="content">
                                    <img src="images/testLogo.png" alt="testLogo" class="img-thumbnail center-block">
                                    <br>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et tempore eius velit, ipsam      </p>
                                    <a href="#" class="btn btn-block bg-theme">Select</a>
                                </div>
                            </div><div class="company col-md-4 col-sm-6 col-xs-12">
                                <div class="content">
                                    <img src="images/testLogo.png" alt="testLogo" class="img-thumbnail center-block">
                                    <br>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et tempore eius velit, ipsam      </p>
                                    <a href="#" class="btn btn-block bg-theme">Select</a>
                                </div>
                            </div><div class="company col-md-4 col-sm-6 col-xs-12">
                                <div class="content">
                                    <img src="images/testLogo.png" alt="testLogo" class="img-thumbnail center-block">
                                    <br>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et tempore eius velit, ipsam      </p>
                                    <a href="#" class="btn btn-block bg-theme">Select</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <style>
                        .company .content{
                            padding: 10px;
                            margin: 15px;
                            border: 1px solid #ddd;
                            box-shadow: 0px 0px 5px #ddd;
                        }
                        .company .content > img {
                            border: none;
                            width: 10em;
                        }

                        @media screen and (max-width: 450px) {
                            .company .content{
                                padding: 0 5px;
                                margin: 15px 0px;
                            }
                        }
                    </style>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>
    $(function() {
        $.material.init();

        $("nav .nav.navbar li").addClass('disabled');
        $("nav .navbar").css({
            webkitFilter: 'blur(2px)',
            filter: 'blur(2px)'
        });
    })

</script>
