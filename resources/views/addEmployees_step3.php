<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>

<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">

                <div class="breadcrumb flat text-center row">
                    <?php if(session()->get('plan') == 'payroll'): ?>
                        <span class="col-xs-6 col-sm-3 col-md-3 hidden-xs">The Basics</span>
                        <span class="col-xs-6 col-sm-3 col-md-3 hidden-xs">Personal Details</span>
                        <span class="col-xs-12 col-sm-3 col-md-3 active">Tax Details</span>
                        <span class="col-xs-6 col-sm-3 col-md-3 hidden-xs">Payment Details</span>
                    <?php elseif((session()->get('plan') == 'benefits')or(session()->get('plan') == 'manager')): ?>
                        <span class="col-xs-6 col-sm-2 col-md-2 hidden-xs">Basics</span>
                        <span class="col-xs-6 col-sm-2 col-md-2 hidden-xs">Personal</span>
                        <span class="col-xs-6 col-sm-2 col-md-2 hidden-xs">Family</span>
                        <span class="col-xs-12 col-sm-3 col-md-3 active">Tax Details</span>
                        <span class="col-xs-6 col-sm-3 col-md-3 hidden-xs">Payment</span>
                    <?php endif; ?>
                </div>


                <br>
                <br>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <form class="form-horizontal">
                        <fieldset>
                            <legend>Tax Details / Information</legend>
                            
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="togglebutton form-group">
                                        <label>
                                            Professional Tax Applicable Y/N
                                            &nbsp;&nbsp;&nbsp; 
                                            <input type="checkbox" name="taxApplicable">
                                        </label>
                                    </div>
                                </div>
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p> Any earning an income from salary or anyone practicing a profession such as chartered accountant, lawyer, doctor etc. are required to pay this professional tax</p>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="form-group label-floating is-empty ">
                                        <label class="control-label" for="PFAcNo"><span>Provident Fund A/c Number</span></label>
                                        <input class="form-control" id="PFAcNo" name="PFAcNo" type="text" style="cursor: auto;">
                                    </div>
                                </div>
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p> Provident fund is a term for pension fund, enter your account details</p>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="form-group label-floating is-empty ">
                                        <label class="control-label" for="esiNumber"><span>ESI Number</span></label>
                                        <input class="form-control" id="esiNumber" name="esiNumber" type="text" style="cursor: auto;">
                                    </div>
                                </div>
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p> ESI is Employee State Insurance and it is used for the benefit of the employee</p>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="form-group label-floating is-empty ">
                                        <label class="control-label" for="uan"><span>UAN ( Universal account number )</span></label>
                                        <input class="form-control" id="uan" name="uan" type="number" min="0" style="cursor: auto;">
                                    </div>
                                </div>
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p> Enter details of universal account number</p>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="col-md-8 col-sm-8 col-xs-8 col-md-offset-4 col-sm-offset-3 col-xs-offset-4">
                                        <span class="btn btn-default btn-raised cancel" onclick="history.back();">Cancel</span>
                                        <input type="submit" class="next btn bg-theme next btn-raised" value="next">
                                    </div>
                                </div>
                            </div>


                        </fieldset>
                    </form>
                </div>
                    </div>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>
    $(function() {

        $.material.init();

        function displayError() {
            swal({
                title : "Error !",
                text : 'Try Again!',
                type : 'error',
                confirmButtonClass : 'bg-theme',
                animation : false,
                width : 400
            });
        };

        function closeAlert() {
            $(".sweet-alert button").first().trigger('click');
        }

        // $('.cancel').click(function(event) {
        //     $(".help-block").remove();
        //     $(".has-error").removeClass('has-error');
        //     event.preventDefault();
        //     var $form = $(this).parents('form')[0];
        //     $form.reset();
        //     $($form).find('.form-group.label-floating').addClass('is-empty');
        // });

        $("form.form-horizontal").submit(function(event) {

            event.preventDefault();

            // remove all error messages
            $(".help-block").remove();
            $(".has-error").removeClass('has-error');
            
            swal({
                title : 'Saving',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 400
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: $('.main form').serialize(), 
            })
            .done(function(data) {
                if (data.status == 200) {
                    swal({
                        title : "Success !",
                        text : data.message,
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });   
                    getAndInsert(data.url);
                    history.pushState(null, null, data.url);
                }
                else if (data.status == 400) {
                    swal({
                        title : "Error !",
                        text : data.message,
                        type : 'error',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });
                }
                else{  // Validation Error
                    displayError();
                    // show Errors
                    for(var fields in data) {
                        $('#'+fields).parents('.form-group').addClass('has-error');
                        $('#'+fields).parent().append('<p class="help-block">'+ data[fields][0] +'</p>');
                    }
                }
            })
            .error(function() {
                displayError();
            });
        });

        $(".main .form-horizontal input").trigger('change');
    })
</script>
