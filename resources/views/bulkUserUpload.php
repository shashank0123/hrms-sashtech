<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>


<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">
                        
                        <br>
                        <form action="#" enctype="multipart/form-data" class="form-horizontal col-md-8 col-sm-12 col-xs-12">
                            <legend>Add Multiple User at a time</legend>
                            <br>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <a href="/userUpload.xlsx" class="btn btn-info btn-raised" download>download Template</a>
                            </div>

                            <div class="clearfix"></div>
                            <br>
                            <br>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group is-empty is-fileinput">
                                    <label for="uploadFile" class="col-md-3 control-label">File</label>

                                    <div class="col-md-8">
                                        <input type="text" readonly="" class="form-control" placeholder="upload your excel or csv file">
                                        <input type="file" id="uploadFile" name="uploadFile" multiple="">
                                    </div>
                            </div>

                            <div class="clearfix"></div>
                            <br><br>
                            <span id="upload" class="btn btn-raised bg-theme">Upload</span>
                        </form>
                    </div>
                    
                    <div class="clearfix"></div>
                    <br>

                    <div id="error">

                    </div>
                </div>
            </div>
        </div>
    </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>
    $(function() {

        // add active class to corresponding link in menu 
        $(".nav li.active").removeClass('active');
        $(".nav.nav-stacked li").filter(function(index) {
            return $(this).data('url') == 'bulkFeature';
        }).addClass('active');

        $.material.init();

        function displayError() {
            swal({
                title : "Error !",
                text : 'Try Again!',
                type : 'error',
                confirmButtonClass : 'bg-theme',
                animation : false,
                width : 400
            });
        };

        function closeAlert() {
            $(".sweet-alert button").first().trigger('click');
        }
        
        $("#upload").click(function(event) {
            event.preventDefault();

            $files = $("#uploadFile")[0].files[0];
            if ($files) {

                swal({
                    title : 'Please wait...',
                    html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                    allowOutsideClick : false,
                    showConfirmButton : false,
                    animation : false,
                    width : 400
                });

                var formData = new FormData();
                formData.append('uploadFile', $files);

                $.ajax({
                    url: location.href,
                    type: 'POST',
                    data: formData,
                    enctype: 'multipart/form-data',
                    processData: false,
                    contentType: false 
                })
                .done(function(data) {
                    d = data;
                    if (data.status == 200) {
                        swal({
                            title : "Success !",
                            text : "Users were uploaded successfully.",
                            type : 'success',
                            confirmButtonClass : 'bg-theme',
                            animation : false,
                            width : 400
                        });
                    }
                    else if (data.status == 400) {
                        $("#error").children().remove();
                        var $html = $("<table class='table table-condensed table-bordered'></ul>");
                        $html.append('<tr><th>Row</th><th>Column</th><th>Error Type</th></tr>');

                        for(var row in data) {
                            var user = data[row];

                            for(var column in user) {
                                var $errorList = $("<tr></tr>");
                                $errorList.append('<td>'+row+'</td><td>'+column+'</td><td>'+ user[column][0] +'</td>');
                                $html.append($errorList);
                            }
                        }

                        $("#error").append('<h4>Error</h4><br><br>');
                        $("#error").append($html);
                        closeAlert();
                    }
                    else
                    {
                        displayError();
                    }
                })
                .fail(function() {
                    console.log("error");
                    displayError();
                });
            }
            else {
                swal({
                    title : "Error !",
                    text : 'Please upload a file first',
                    type : 'error',
                    confirmButtonClass : 'bg-theme',
                    animation : false,
                    width : 400
                });
            }
        });
    });
</script>
