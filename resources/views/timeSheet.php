<!DOCTYPE html>
<html>
<head>
    <title>Time Tracking, Online Timesheets , Mobile Time Clock</title>

    <meta name="description" content="Sashtechs is an easy to use time tracker and online timesheet solution. Automatic collection of time cards, custom permission settings, location based reminders, to simplify payroll processing."/>

    <meta name="keywords" content="Employee time tracking software with GPS, Timesheets with payroll, reporting & invoicing, Time cards, time table tracking"/>

    <!--Import Google Icon Font-->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
    <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet"></link>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Material Design -->
    <link href="css/bootstrap-material-design.css" rel="stylesheet">
    <link href="css/ripples.min.css" rel="stylesheet">
    <link href="css/roundslider.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">

    <script src="/js/jquery-1.12.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ripples.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/roundslider.min.js"></script>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
</head>

<style>
    #time{
        display: block;
        font-size: 5em;
        padding: .5em;
    }

    .feature {
        margin: 1em;
        border: 1px solid #ddd;
        padding: 1em;
        height: 17em;
        box-shadow: 0 0 3px #ddd;
    }
</style>

<body>
<?php require_once("staticNavBar.php");?>

<!-- ================  Header  ===================== -->

<header class="parallax">
    <div class="panel panel-default" style="background-color: rgba(0,0,0,0.7);color: #fff;">
        <div class="panel-body">
            <div class="text-center">
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <h5>EMPLOYEE TIME-TRACKING</h5>
                <h3>Time Tracking Your Employees Will Love</h3>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <h6><b></b></h6><i class="fa fa-arrow-down"></i>
                <br>
                <br>
            </div>
        </div>
    </div>
</header>

<div class="clearfix"> </div>
<section style="background:#fff;margin-top: -20px">
    <div class="container-fluid">
        <div class="row">
            <div class="container">
                <br>
                <div class="clearfix"></div>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="center-block feature text-center">
                        <i class="fa fa-clock-o fa-3x"></i>
                        <br>
                        <h4>Fully integrated timecards</h4>
                        <br>
                        <p>Employee time management is taken care by Sashtechs app. Days/Hours are imported automatically so you can easily manage payroll online.</p>
                        <br>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="center-block feature text-center">
                        <i class="fa fa-cog fa-3x"></i>
                        <br>
                        <h4>Easy setup</h4>
                        <br>
                        <p> Setup your holidays, run payroll, reminders with ease. Keep it simple and steady!</p>
                        <br>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="center-block feature text-center">
                        <i class="fa fa-thumbs-up fa-3x"></i>
                        <br>
                        <h4>Easy to Use</h4>
                        <br>
                        <p> Employees can track timesheets, vacations, sick days using mobile app or the web.</p>
                        <br>
                    </div>
                </div>
                
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="center-block feature text-center">
                        <i class="fa fa-lock fa-3x"></i>
                        <br>
                        <h4>256-BIT SSL ENCRYPTION</h4>
                        <br>
                        <p>All information travelling between your browser to our servers is protected with 256-bit SSL encryption.</p>
                        <br>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="center-block feature text-center">
                        <i class="fa fa-smile-o fa-3x"></i>
                        <br>
                        <h4>World-class customer care</h4>
                        <br>
                        <p>Our team is composed of expert, friendly payroll professionals that will help you to setup your account. Give them a call at xxxxxxxxxx and say Hi.</p>
                        <br>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="center-block feature text-center">
                        <i class="fa fa-signal fa-3x"></i>
                        <br>
                        <h4>Sexy Reports</h4>
                        <br>
                        <p>Get an instant overview of your billable time and team progress. Export timesheets.</p>
                        <br>
                    </div>
                </div>
            </div>

            <br><hr><br>
        </div>
    </div>
</section>

<div class="clearfix"></div>    
<section class="container">
    <br>
    <br>
    <h3 class="text-center">Online Time Tracking with Geo Location</h3>
    <br>
    <h4 class="text-center">Easily track time and location for your mobile employees.</h4>
    <br>
    <p class="text-center h4">Looking for easy-to-use time tracking with Geo Location for your mobile or remote employees? <br><br> By using our mobile apps your employees geo-location are automatically attached to the timesheet when they clock in or out. </p>
    <br>
    <br>
    <br>
    <br>
</section>
<div class="clearfix"></div>

<section class="container-fluid" style="background: linear-gradient( 14deg, #CF3D3D 44%, rgb(191, 52, 52) 20%);color: #fff;">
    <div class="container">
        <br>
        <h3 class="text-center">Employee Alerts and Reminders</h3>
        <br>
        <h4 class="text-center">See how easily you can remind employees about the activities. Simply and tactfully remind your employees </h4>
        <br>
        <div id="time" class="text-center"></div>
        <br>
    </div>
</section>

<section class="parallax" style="background-image: url(images/try.jpg);">
    <div class="panel panel-default" style="  ">
        <div class="panel-body">
            <div class="text-center">
                <br>
                <br>
                <br>
                <br>
                <h5><b>TRY Sashtechs</b></h5>
                <h3 class="text-uppercase">Start your one month free trial </h3>
                <br>

                <div class="panel text-left" id="panel-text-left">
                    <?php require 'demoForm.php'; ?>
                </div>
                <br>
                    <div class="clearfix"></div>
                    <br><br>
                    <br><br>
            </div>
        </div>
    </div>
</section>


<?php require_once('staticFooter.php');?>
</body>
</html>

<script>
$(function() {
    $.material.init();

    var $time = $("#time");

    setInterval(function(){ 
        var now = new Date();
        var hms = [now.getHours(), now.getMinutes(), now.getSeconds()];
        $time.text(hms.join(' : '));
    }, 1000);
});
</script>
