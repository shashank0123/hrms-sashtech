<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>

<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">

                    <div class="breadcrumb flat text-center row">
                    <?php if(session()->get('plan') == 'payroll'): ?>
                        <span class="col-xs-12 col-sm-3 col-md-3  active">The Basics</span>
                        <span class="col-xs-6 col-sm-3 col-md-3  hidden-xs">Personal Details</span>
                        <span class="col-xs-6 col-sm-3 col-md-3  hidden-xs">Tax Details</span>
                        <span class="col-xs-6 col-sm-3 col-md-3  hidden-xs">Payment Details</span>
                    <?php elseif((session()->get('plan') == 'benefits')or(session()->get('plan') == 'manager')): ?>
                        <span class="col-xs-12 col-sm-3 col-md-3 active">The Basics</span>
                        <span class="col-xs-6 col-sm-2 col-md-2 hidden-xs">Personal</span>
                        <span class="col-xs-6 col-sm-2 col-md-2 hidden-xs">Family</span>
                        <span class="col-xs-6 col-sm-2 col-md-2 hidden-xs">Tax</span>
                        <span class="col-xs-6 col-sm-3 col-md-3 hidden-xs">Payment</span>
                    <?php endif; ?>
                    </div>

                <br>
                <br>

                <form class="form-horizontal" id="addEmployees_step1">
                    <fieldset>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <legend>Let's bring your employees on board! <h4 class="pull-right"><a class="ajaxify" href="/bulkUserUpload">Bulk User Upload</a></h4></legend>


                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating is-empty ">
                                        <label class="control-label" for="firstName"><span>First Name</span></label>
                                        <input class="form-control" id="firstName" name="firstName" type="text" style="cursor: auto;">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" >
                                    <div class="form-group label-floating is-empty ">
                                        <label class="control-label" for="lastName"><span>Last Name</span></label>
                                        <input class="form-control" id="lastName" name="lastName" type="text" style="cursor: auto;">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="form-group label-floating is-empty ">
                                        <label class="control-label" for="title"><span>Job Title</span></label>
                                        <input class="form-control" id="title" name="title" type="text" style="cursor: auto;">
                                    </div>
                                </div>
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p>This is name of the job. It will be shown in the employee profile, payslip and reports</p>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="form-group label-floating is-empty ">
                                        <label class="control-label" for="team"><span>Team</span></label>
                                        <input class="form-control" id="team" name="team" type="text" style="cursor: auto;">
                                    </div>
                                </div>
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p>You can create small teams within your company and assign employees within the team.</p>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="form-group label-floating is-empty">
                                        <label for="workAddress" class="control-label">Local Address</label>
                                        <div class="">
                                            <textarea class="form-control" rows="2" name="localAddress" id="workAddress"></textarea>
                                        </div>
                                    </div>                                
                                </div>
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p>Employee address. It can be anywhere in the India</p>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="form-group label-floating is-empty">
                                        <label for="email" class="control-label">Email</label>
                                        <input type="email" name="email" class="form-control" id="email">
                                    </div>
                                </div>   
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p>Employee email address,  this will be your employee username to login in Sashtechs</p>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <br>

                            <legend>Pay Package</legend>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="form-group label-floating">
                                        <label for="joinDate" class="control-label">Date of Joining</label>
                                        <input type="text" name="joinDate" id="joinDate" class="form-control">
                                    </div>
                                </div>
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p>First work day of this employee in your company</p>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="form-group label-floating ">
                                        <label for="ctc" class="control-label">CTC</label>
                                        <select id="ctc" name="ctc" class="form-control">
                                            <option value=" ">Select CTC </option>
                                            <option value="other">Other</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p>The salary you will pay to Employees</p>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div id="otherCTC">
                            <div id="salaryComponents" class="hidden">
                                <div class="clearfix"></div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="CTC"><span>CTC Amount</span></label>
                                            <input class="form-control" id="CTC" name="CTC" type="number" min="0" style="cursor: auto;">
                                        </div>
                                    </div>
                                    <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                        <p>Gross pay to be paid to Employees</p>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <br>
                                <legend>Salary Components</legend>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="basic"><span>Basic</span></label>
                                            <input class="form-control" id="basic" name="basic" type="number" min="0" style="cursor: auto;">
                                        </div>
                                    </div>
                                    <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                        <p>Basic salary is the core salary and it is the fixed part of the compensation package</p>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="hra"><span>HRA</span></label>
                                            <input class="form-control" id="hra" name="hra" type="number" min="0" style="cursor: auto;">
                                        </div>
                                    </div>
                                    <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                        <p>This amount is for allowance for housing</p>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="conveyance"><span>Conveyance</span></label>
                                            <input class="form-control" id="conveyance" name="conveyance" type="number" min="0" style="cursor: auto;" max="19200">
                                        </div>
                                    </div>
                                    <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                        <p>Enter details to complete CTC</p>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="medicalAllowance"><span>Medical Allowance</span></label>
                                            <input class="form-control" id="medicalAllowance" name="medicalAllowance" type="number" min="0" max="15000" style="cursor: auto;">
                                        </div>
                                    </div>
                                    <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                        <p>Enter details to complete CTC</p>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="others"><span>Others </span></label>
                                            <input class="form-control" id="others" name="others" type="number" min="0" style="cursor: auto;">
                                        </div>
                                    </div>
                                    <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                        <p>Enter details to complete CTC</p>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="telephone"><span>Telephone Allowance</span></label>
                                            <input class="form-control" id="telephone" name="telephone" type="number" min="0" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="leaveTravel"><span>Leave Travel Allowance</span></label>
                                            <input class="form-control" id="leaveTravel" name="leaveTravel" type="number" min="0" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="medicalInsurance"><span>Medical Insurance</span></label>
                                            <input class="form-control" id="medicalInsurance" name="medicalInsurance" type="number" min="0" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="uniform"><span>Uniform Allowance</span></label>
                                            <input class="form-control" id="uniform" name="uniform" type="number" min="0" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="gratuity"><span>Gratuity</span></label>
                                            <input class="form-control" id="gratuity" name="gratuity" type="number" min="0" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="superAnnuation"><span>Super Annuation</span></label>
                                            <input class="form-control" id="superAnnuation" name="superAnnuation" type="number" min="0" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="annualBonus"><span>Annual Bonus</span></label>
                                            <input class="form-control" id="annualBonus" name="annualBonus" type="number" min="0" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="festivalBonus"><span>Festival Bonus</span></label>
                                            <input class="form-control" id="festivalBonus" name="festivalBonus" type="number" min="0" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="incentives"><span>Incentives</span></label>
                                            <input class="form-control" id="incentives" name="incentives" type="number" min="0" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="leaveEncashment"><span>Leave Encashment</span></label>
                                            <input class="form-control" id="leaveEncashment" name="leaveEncashment" type="number" min="0" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="pfContribution"><span>PF Contribution</span></label>
                                            <input class="form-control" id="pfContribution" name="pfContribution" type="number" min="0" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="esiContribution"><span>ESI Contribution</span></label>
                                            <input class="form-control" id="esiContribution" name="esiContribution" type="number" min="0" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>
    
                                
                                <div class="col-md-12 addMoreButton">
                                    <div class="btn-group  pull-left">
                                        <a href="#" data-target="#" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
                                            Add more Fields
                                            <span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a href="javascript:void(0)" data-id="telephone" class="addMore">Telephone Allowance</a></li>
                                            <li><a href="javascript:void(0)" data-id="leaveTravel" class="addMore">Leave Travel Allowance</a></li>
                                            <li><a href="javascript:void(0)" data-id="uniform" class="addMore">Uniform Allowance</a></li>
                                            <li><a href="javascript:void(0)" data-id="gratuity" class="addMore">Gratuity</a></li>
                                            <li><a href="javascript:void(0)" data-id="superAnnuation" class="addMore">Super Annuation</a></li>
                                            <li><a href="javascript:void(0)" data-id="annualBonus" class="addMore">Annual Bonus</a></li>
                                            <li><a href="javascript:void(0)" data-id="festivalBonus" class="addMore">Festival Bonus</a></li>
                                            <li><a href="javascript:void(0)" data-id="incentives" class="addMore">Incentives</a></li>
                                            <li><a href="javascript:void(0)" data-id="leaveEncashment" class="addMore">Leave Encashment</a></li>
                                            <li><a href="javascript:void(0)" data-id="pfContribution" class="addMore">PF Contribution</a></li>
                                            <li><a href="javascript:void(0)" data-id="esiContribution" class="addMore">ESI Contribution</a></li>
                                            <li><a href="javascript:void(0)" data-id="medicalInsurance" class="addMore">Medical Insurance</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <br>
                            </div>
                        </div>

                            <div class="clearfix"></div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="togglebutton form-group">
                                        <label>Employee Self On-Boarding Y/N
                                            &nbsp;&nbsp;&nbsp; 
                                            <input type="checkbox" id="employeeSelfOnBoarding" name="employeeSelfOnBoarding">
                                        </label>
                                    </div>
                                </div>
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p>The employee will enter their Personal details, tax details and Band and bank information on Sashtechs</p>
                                </div>
                            </div>
                            
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="col-md-8 col-sm-8 col-xs-8 col-md-offset-4 col-sm-offset-3 ">
                                        <span class="btn btn-default btn-raised cancel" onclick="history.back();">Cancel</span>
                                        <input type="submit" class="next btn bg-theme next btn-raised" value="next">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </fieldset>
                </form>

                    </div>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>

    Array.prototype.unique = function() {
        var unique = [];
        for (var i = 0; i < this.length; i++) {
            var current = this[i];
            if (unique.indexOf(current) < 0) unique.push(current);
        }
        return unique;
    }

    $(function() {
        $.material.init();

        $('#joinDate').bootstrapMaterialDatePicker({ weekStart : 0, time: false }).on('dateSelected', function(event) {
            $('#'+$(this).data('dtp')).find('.dtp-btn-ok').trigger('click');
        });
        $('#joinDate').bootstrapMaterialDatePicker('setDate', moment());
        
        var CTC_LIST = [];

        var availableTitle = [
            "Chief Product Officer",
            "CEO",
            "Associate Vice President",
            "QA Manager",
            "Manager",
            "Technical Manager",
            "Senior QA Engineer",
            "Business Analyst",
            "Chief Finance Officer",
            "Senior Vice President - Marketing",
            "Data Scientist",
            "Talent Acquisition",
            "Senior Recruiter",
            "Director of Engineering",
            "Software Developer",
            "Assistant Manager Payroll",
            "Associate Payroll",
            "Associate HR",
            "Assistant Manager Taxation",
            "Team Manager",
            "Sales Manager",
            "Sales Executive",
            "Office Assistant",
            "Computer Operator",
            "Director",
            "Managing Director",
            "Accounts Executive",
            "Accounts Manager",
            "HR Manager",
            <?php 
            if (!empty($titles)) 
                foreach ($titles as $key => $value) 
                {
                    echo '"'.$value->titles.'",';
                }
            ?>
            "Team Lead"

        ].sort().unique();

        var availableTeam = [
        <?php 
        if (!empty($teams)) 
            foreach ($teams as $key => $value) 
            {
                echo '"'.$value->team.'",';
            }
        ?>
        ].sort().unique();

        $( "#team" ).autocomplete({
          source: availableTeam,
        });

        $("#title").autocomplete({
            source : availableTitle,
        })

        $( "#title" ).on( "autocompletechange", function( event, ui ) {
            var val = $(this).val();

            swal({
                title : 'Fetching data',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 300
            });

            $.ajax({
                url: location.href + '/' + val,
                type: 'POST',
                data: {worker: val},
            })
            .done(function(data) {
                CTC_LIST = data;
                var ctc = $('#ctc');
                ctc.empty();
                                                            
                ctc.append('<option value="">Select CTC </option>')
                for (var i = 0; i < data.length; i++) {
                    ctc.append('<option value="'+data[i].ctc+'">'+data[i].ctc+'</option>');
                }
                ctc.append('<option value="other">Other</option>');
                
                closeAlert();
            })
            .fail(function() {
                console.log("error");
            })
            
        } );


        function displayError() {
            swal({
                title : "Try again !",
                type : 'error',
                confirmButtonClass : 'bg-theme',
                animation : false,
                width : 300
            });
        };

        function closeAlert() {
            $(".sweet-alert button").first().trigger('click');
        }

        // $('.cancel').click(function(event) {
        //     $(".help-block").remove();
        //     $(".has-error").removeClass('has-error');
        //     event.preventDefault();
        //     var $form = $(this).parents('form')[0];
        //     $form.reset();
        //     $($form).find('.form-group.label-floating').addClass('is-empty');
        // });

        $("form.form-horizontal").submit(function(event) {
            event.preventDefault();

            $("#ctc").parents(".form-group").removeClass('has-error');
            // handle ctc from template ctc and entered CTC
            var $CTC = 1*$("#ctc").val() || 1*$("#CTC").val() || 0 ;

            // salary components  ===============================================================

            var input = $("#salaryComponents .form-group input");
            var ctc = 0;

            // checking sum of salary components w.r.t ctc
            input.each(function(index, el) {
                ctc += $(el).val() ? 1*$(el).val(): 0;
            });
            ctc -= $("#CTC").val() || 0; // remove ctc that added

            if (!$CTC) {
                $("#CTC").val(ctc).parents(".label-floating").removeClass('is-empty');
            }

            if($CTC < ctc){
                swal({
                    title : 'Adjust Salary Components',
                    text : "Sum of your salary components can not be greater than CTC",
                    type : 'error',
                    animation : false,
                    confirmButtonClass : 'bg-theme',
                },function() {
                    $("#CTC").parents(".form-group").addClass('has-error');
                    $("#CTC").focus();
                });
            }
            else if ($CTC > ctc) {
                swal({
                    title : 'Adjust CTC',
                    text : "CTC is greater than sum of salary components",
                    type : 'warning',
                    confirmButtonClass : 'bg-theme',
                    animation : false,
                    confirmButtonText : 'Change'
                }, function() {
                    $("#CTC").parents(".form-group").addClass('has-error');
                    $("#CTC").focus();
                });
            }
            else {

                // =============================================================================
                // remove all error messages
                $(".help-block").remove();
                $(".has-error").removeClass('has-error');
                
                swal({
                    title : 'Saving',
                    html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                    allowOutsideClick : false,
                    showConfirmButton : false,
                    animation : false,
                    width : 300
                });

                $.ajax({
                    url: location.href,
                    type: 'POST',
                    data: $('.main form').serialize(), 
                })
                .done(function(data) {
                    if (data.status == 200) 
                    {
                        swal({
                            title : "Added!",
                            // text : data.message,
                            type : 'success',
                            confirmButtonClass : 'bg-theme',
                            showConfirmButton : false,
                            animation : false,
                            width : 300,
                            timer: 1500
                        });
                        if ($('#employeeSelfOnBoarding').is(':checked')) 
                        {
                            swal({
                                title : 'Another Employee',
                                text : "Do you want to add another employee?",
                                type : 'info',
                                confirmButtonClass : 'bg-theme',
                                animation : false,
                                showCancelButton: true,
                                confirmButtonText: 'Yes',
                                cancelButtonText: 'No',
                                cancelButtonClass: 'bg-theme',
                                closeOnConfirm: true,
                                closeOnCancel: true
                            },
                            function(isConfirm) 
                            {
                                if (isConfirm === true) 
                                {
                                    data.url = location.pathname;
                                    getAndInsert(data.url);
                                    history.pushState(null, null, data.url);
                                }
                                else 
                                {
                                    getAndInsert(data.url);
                                    history.pushState(null, null, data.url);
                                }
                            })
                        }
                        else 
                        {
                            getAndInsert(data.url);
                            history.pushState(null, null, data.url);
                        }
                    }
                    else if(data.status == 208) {  //  email already Exist
                        closeAlert();
                        var $email = $("#email");
                        $email.parent().append('<p class="help-block">'+ data.message +'</p>');
                        $email.focus();
                        $email.parents('.form-group').addClass('has-error');
                    }
                    else{  // Validation Error
                        displayError();
                        // show Errors
                        for(var fields in data) {
                            $('#'+fields).parents('.form-group').addClass('has-error');
                            $('#'+fields).parent().append('<p class="help-block">'+ data[fields][0] +'</p>');
                        }
                    }
                })
                .error(function() {
                    displayError();
                });
            }
        });
        
        $("#ctc").change(function(event) {

            $(this).siblings('.help-block').remove();
            // reset salary component and add more drop down 
            // =============================
            $(".addMoreButton").show();
            $(".addMoreButton ul li a").addClass('hidden');

            var $salaryComponents = $("#salaryComponents");
            $salaryComponents.find('.hidden').removeClass('hidden');
            $salaryComponents.find('.form-group input').val('');
            $salaryComponents.find('.form-group').addClass('is-empty');
            $(".addMoreButton ul li a").each(function(index, el) {
                $('#'+$(this).data('id')).parent().parent().addClass('hidden');
            });
            // =============================

            var val = $(this).val();
            if (!val) return false;
            if (val == "other") {
                $salaryComponents.removeClass('hidden');
                $("#CTC").parent().show();
            }
            else{
                $salaryComponents.removeClass('hidden');
                    //console.log("success", data);

                    for (var i = 0; i < CTC_LIST.length; i++) {
                        if (1*val == CTC_LIST[i].ctc) { // get ctc components corresponding to ctc value
                            var data = CTC_LIST[i];
                            for(var fields in data) {
                                var el = $('#'+fields);
                                var parent = el.parents(".form-group");
                                // if data fill the fields
                                if (data[fields]){
                                    el.val(1*data[fields]);
                                    parent.removeClass('is-empty');
                                }
                                else{
                                    el.val('');
                                    parent.addClass('is-empty');
                                }
                                var superParent = parent.parent().parent();
                                if (data[fields] && superParent.hasClass('hidden')) {
                                    superParent.removeClass('hidden');
                                    $(".addMoreButton ul li a").filter(function(index) {
                                        return ($(this).data('id') == fields);
                                    }).addClass('hidden');// do not use hide(), need to count hidden element below
                                }
                            }
                            break;
                        }
                        else
                            console.log("ctc component not found");
                    }
                
                $("#CTC").parent().hide();
            }
        });

        //   salary components  ======================================================================

        $("#CTC").change(function(event) {
            //event.preventDefault();
            var $val = 1*$(this).val();
            var used = 0;

            if (!(1*$("#basic").val())){
                $("#basic").val(Math.round($val * 0.4));
            }
            used += 1*$("#basic").val();

            if (!(1*$("#hra").val())) {
                $("#hra").val(Math.round($val * 0.2));
            }
            used += 1*$("#hra").val();

            if (!(1*$("#conveyance").val())) {
                $("#conveyance").val(Math.max(Math.min(19200, $val-used), 0));
            }
            used += 1*$("#conveyance").val();

            if (!(1*$("#medicalAllowance").val())) {
                $("#medicalAllowance").val(Math.max(Math.min(15000, $val-used), 0));
            }
            used += 1*$("#medicalAllowance").val();

            if (!(1*$("#others").val())) 
                $("#others").val(Math.max(0, $val-used));

            $("#basic, #hra, #conveyance, #medicalAllowance, #others").trigger('change');
        });

        // handle conveyance max value
        // $("#conveyance").change(function(event) {
        //     event.preventDefault();
        //     $(this).val( Math.min($(this).val(), 19200) );
        // });
        //  // handle conveyance max value
        // $("#medicalAllowance").change(function(event) {
        //     event.preventDefault();
        //     $(this).val( Math.min($(this).val(), 15000) );
        // });

        $(".addMore").click(function(event) {
            event.preventDefault();
            $("#" + $(this).data('id')).parents(".hidden").removeClass('hidden');
            $(this).addClass('hidden');

            if ($(".addMore.hidden").length == $('.addMore').length) {$(".addMoreButton").hide()};
        });

        $('.main .form-horizontal input').trigger('change');
    });
</script>
