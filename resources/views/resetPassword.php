<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>

<style>
    .panel{
        width:320px;
        margin: 0 auto;
        padding: 5px;
    }
</style>

<body  style="margin-top:4em;">

        <div class="panel panel-default">
            <div class="panel-body">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <form class="form-horizontal" method="POST" autocomplete="off">
                        <input style="display:none">
                        <input type="password" style="display:none">
                        <fieldset>
                            <legend><a id="logo" class="fa btn btn-block" href="javascript:void(0)" >Sashtechs <div></div></a></legend>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating is-empty ">
                                        <label class="control-label" for="newPass"><span>New Password</span></label>
                                        <input class="form-control" id="newPass" type="password" name="newPass" style="cursor: auto;" autocomplete="off">
                                        <p class="help-block">
                                        <?php 
                                        if (isset($errors)) 
                                        {
                                            if (!is_object($errors)) 
                                            {
                                                $error = json_decode($errors);
                                                if (isset($error->newPass)) 
                                                {
                                                    echo $error->newPass[0];
                                                }
                                            }
                                        } 
                                        ?>
                                        </p>
                                    </div>
                                </div>
                            </div>                            

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating is-empty ">
                                        <label class="control-label" for="confirmPass"><span>Confirm Password</span></label>
                                        <input class="form-control" id="confirmPass" type="password" name="confirmPass" style="cursor: auto;" autocomplete="off">
                                        <p class="help-block">
                                        <?php 
                                        if (isset($errors)) 
                                        {
                                            if (!is_object($errors)) 
                                            {
                                                $error = json_decode($errors);
                                                if (!empty($error->confirmPass[0]))
                                                echo $error->confirmPass[0];
                                            }
                                        } 
                                        ?>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <br>
                            <a href="/forgotPassword" style="color: red">
                            <?php 
                                if (isset($result->message)) {
                                    echo $result->message;
                                }
                             ?>
                             </a>
                            <div class="clearfix"></div>
                            <br>

                            <input type="hidden" id="apiKey" name="apiKey" value="<?php echo Input::get('apiKey'); ?>">

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <button style="margin:0 auto;" type="submit" class="btn bg-theme btn-block btn-raised">Submit</button>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </fieldset>
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>

                    </div>
                </div>
            </div>
        </div>

    <br><br>
</body>
</html>

<script>
    $.material.init();
</script>
