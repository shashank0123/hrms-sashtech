<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>


<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">
                       <br>
                <br>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <form method="POST" class="form-horizontal">
                        <fieldset>
                            <legend>Bank Details</legend>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="form-group label-floating <?php echo (isset($result->bankName)) ? '' : 'is-empty' ?>">
                                        <label class="control-label" for="bankName"><span>Name of Bank</span></label>
                                        <input class="form-control" id="bankName" name="bankName" type="text" style="cursor: auto;" value="<?php echo (isset($result->bankName)) ? $result->bankName : ''; ?>">
                                    </div>
                                </div>
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p>Name & details of the bank where you have company account</p>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="form-group label-floating <?php echo (isset($result->accountNumber)) ? '' : 'is-empty' ?>">
                                        <label class="control-label" for="accountNumber"><span>Account Number</span></label>
                                        <input class="form-control" id="accountNumber" name="accountNumber" type="text" style="cursor: auto;" value="<?php echo (isset($result->accountNumber)) ? $result->accountNumber : ''; ?>">
                                    </div>
                                </div>
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p>Name & details of the bank where you have company account</p>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12" >
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="form-group label-floating">
                                        <label for="accountType" class="control-label">Type of Account</label>

                                        <select id="accountType" name="accountType" class="form-control">
                                            <option value="saving" <?php 
                                                if (isset($result->accountType)) {
                                                    echo ($result->accountType == "saving") ? 'selected' : '';
                                                }
                                             ?>>Saving</option>
                                            <option value="current" <?php 
                                                if (isset($result->accountType)) {
                                                    echo ($result->accountType == "current") ? 'selected' : '';
                                                }
                                             ?>>Current</option>
                                            <option value="overDraft" <?php 
                                                if (isset($result->accountType)) {
                                                    echo ($result->accountType == "overDraft") ? 'selected' : '';
                                                }
                                             ?>>OverDraft</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p>Name & details of the bank where you have company account</p>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="form-group label-floating <?php echo (isset($result->ifsc)) ? '' : 'is-empty' ?>">
                                        <label class="control-label" for="ifsc"><span>IFSC</span></label>
                                        <input class="form-control" id="ifsc" name="ifsc" type="text" style="cursor: auto;" value="<?php echo (isset($result->ifsc)) ? $result->ifsc : ''; ?>">
                                    </div>
                                </div>
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p>IFSC Code is Indian Financial System Code, which is an eleven character code assigned by RBI to identify every bank branches uniquely</p>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="form-group label-floating <?php echo (isset($result->micr)) ? '' : 'is-empty' ?>">
                                        <label class="control-label" for="micr"><span>MICR</span></label>
                                        <input class="form-control" id="micr" name="micr" type="text" style="cursor: auto;" value="<?php echo (isset($result->micr)) ? $result->micr : ''; ?>">
                                    </div>
                                </div>
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p>MICR (Magnetic Ink Character Recognition), as name suggest it is a character recognition technology which helps in faster processing of cheques</p>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="form-group label-floating <?php echo (isset($result->loginid)) ? '' : 'is-empty' ?>">
                                        <label class="control-label" for="loginid"><span>Login ID</span></label>
                                        <input class="form-control" id="loginid" name="loginid" type="text" style="cursor: auto;" value="<?php echo (isset($result->loginid)) ? $result->loginid : ''; ?>">
                                    </div>
                                </div>
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p>Name & details of the bank where you have company account</p>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="form-group label-floating <?php echo (isset($result->bankURL)) ? '' : 'is-empty' ?>">
                                        <label class="control-label" for="bankURL"><span>Bank URL</span></label>
                                        <input class="form-control" id="bankURL" type="text" name="bankURL" style="cursor: auto;" value="<?php echo (isset($result->bankURL)) ? $result->bankURL : ''; ?>">
                                    </div>
                                </div>
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p>Name & details of the bank where you have company account</p>
                                </div>
                            </div>

                            <div class="col-md-8 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="col-md-8 col-sm-8 col-xs-8 col-md-offset-4 col-sm-offset-3 col-xs-offset-4">
                                        <span class="btn btn-default btn-raised cancel" onclick="history.back();">Cancel</span>
                                        <input type="submit" class="add btn bg-theme btn-raised next" value="Submit">
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
                    </div>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>
    $(function() {
        $.material.init();

        function displayError() {
            swal({
                title : "Error !",
                text : 'Try Again!',
                type : 'error',
                confirmButtonClass : 'bg-theme',
                animation : false,
                width : 400
            });
        };

        function closeAlert() {
            $(".sweet-alert button").first().trigger('click');
        }

        $('.cancel').click(function(event) {
            $(".help-block").remove();
            $(".has-error").removeClass('has-error');
            event.preventDefault();
            var $form = $(this).parents('form')[0];
            $form.reset();
            
            $($form).find('.form-group.label-floating').addClass('is-empty').find('input').trigger('change');
        });

        $("form.form-horizontal").submit(function(event) {

            event.preventDefault();

            // remove all error messages
            $(".help-block").remove();
            $(".has-error").removeClass('has-error');
            
            swal({
                title : 'Saving',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 400
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: $('.main form').serialize(), 
            })
            .done(function(data) {
                if (data.status == 200) {
                    swal({
                        title : "Success !",
                        text : data.message,
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });   
                    getAndInsert(data.url);
                    history.pushState(null, null, data.url);
                }
                else{  // Validation Error
                    displayError();
                    // show Errors
                    for(var fields in data) {
                        $('#'+fields).parents('.form-group').addClass('has-error');
                        $('#'+fields).parent().append('<p class="help-block">'+ data[fields][0] +'</p>');
                    }
                }
            })
            .error(function() {
                displayError();
            });
        });

        $('.main .form-horizontal input').trigger('change');
    });
</script>
