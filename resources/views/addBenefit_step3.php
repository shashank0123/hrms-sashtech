<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>


<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">

                        <div class="breadcrumb flat text-center row">
                            <span class="col-md-6 col-sm-6 hidden-xs">Add Benefits</span>
                            <span class="active col-md-6 col-sm-6 col-xs-12">Add Employee To Benefit</span>
                        </div>

                        <br>
                        <br>
                        <h3>Add Employees To Benefit</h3>
                        <hr>
                        
                        <h6>Select Employee and assign benefits plan</h6>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <br>
                            <form class="form-horizontal" method="POST">
                                <div class="checkbox">
                                     <?php if (!empty($result)) { 
                                                foreach ($result as $key => $value) { ?>
                                    <label>
                                        <input type="checkbox" name="id<?php echo $key.$value->id?>"> &nbsp;&nbsp;&nbsp;&nbsp; <?php echo $value->firstName." ".$value->lastName;?> 
                                    </label><br> <?php } } else { ?>
                                        You dont have any Benefits
                                  <?php } ?>
                                   
                                </div>

                                <br><br>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="employerContribution"><span>Employer's contribution</span></label>
                                            <input class="form-control" id="employerContribution" name="employerContribution" type="number" max="100" min="0" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="employeeContribution"><span>Employee's contribution</span></label>
                                            <input class="form-control" id="employeeContribution" name="employeeContribution" type="number" max="100" min="0" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <div class="col-md-8 col-sm-8 col-xs-8 col-md-offset-4 col-sm-offset-3 col-xs-offset-4">
                                            <span class="btn btn-default btn-raised cancel" onclick="history.back();">Cancel</span>
                                            <input type="submit" class="next btn bg-theme next btn-raised" value="Save">
                                        </div>
                                    </div>
                                </div>
                                
                            </form>
                        </div>

                        <br>


                    </div>
                    <style>
                        .checkbox label{
                            padding: 10px;
                        }
                    </style>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>
    function displayError() {
        swal({
            title : "Error !",
            text : 'Try Again!',
            type : 'error',
            confirmButtonClass : 'bg-theme',
            animation : false,
            width : 400
        });
    };

    function closeAlert() {
        $(".sweet-alert button").first().trigger('click');
    }

    $(function() {

        // add active class to corresponding link in menu 
        $(".nav li.active").removeClass('active');
        $(".nav.nav-stacked li").filter(function(index) {
            return $(this).data('url') == 'benefit';
        }).addClass('active');
        
        $.material.init();

        $('form.form-horizontal').submit(function(event) {

            event.preventDefault();

            // remove all error messages
            $(".help-block").remove();
            $(".has-error").removeClass('has-error');
            
            swal({
                title : 'Saving',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 400
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: $('.main form').serialize(), 
            })
            .done(function(data) {
                // console.log(data);
                if (data.status == 200) {
                    swal({
                        title : "Success !",
                        text : data.message,
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });   
                    getAndInsert(data.url);
                    history.pushState(null, null, data.url);
                }
                else if(data.status == 208) {  //  email already Exist
                    closeAlert();
                    var $email = $("#employeOthereMail");
                    $email.parent().append('<p class="help-block">'+ data.message +'</p>');
                    $email.focus();
                    $email.parents('.form-group').addClass('has-error');
                }
                else{  // Validation Error
                    displayError();
                    // show Errors
                    for(var fields in data) {
                        $('#'+fields).parents('.form-group').addClass('has-error');
                        $('#'+fields).parent().append('<p class="help-block">'+ data[fields][0] +'</p>');
                    }
                }
            })
            .error(function() {
                displayError();
            });
        });

        $(".main .form-horizontal input").trigger('change');
    });
</script>
