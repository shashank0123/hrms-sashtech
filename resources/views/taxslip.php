<!DOCTYPE html>
<html>
<head>
    <title>Taxslip</title>
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
    <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet"></link>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Material Design -->
    <link href="css/bootstrap-material-design.css" rel="stylesheet">
    <link href="css/ripples.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">

    <script src="js/jquery-1.12.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ripples.min.js"></script>
    <script src="js/material.min.js"></script>


    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

</head>

<style>
    .section{
        width:70em;
        margin: 0 auto;
    }

    .table{
        border-collapse: collapse;
        border: 1px solid #000;
    }
    .table-bordered {
        border: 1px solid #000;
    }
    .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th{
        border-color:#000;
        padding-left: 10px;
    }

    .table tr td:nth-child(odd){
        font-weight: 400;
    }

    #last tr td{
        font-weight: 100;
    }

    #last tr td:nth-child(3n+1){
        font-weight: 400;
    }

    .bg-theme1{
        background: #FC6558 !important;
        color: #fff;
    }
    .bg-theme2{
        background: lightseagreen !important;
        color: #fff;
    }
    .bg-theme3{
        background: #E2C4C2 !important;
        color: #fff;
    }

    #logo > div:first-child{
        border-top: 1px solid #aaa;
    }

    .no-border{
        border: none !important;
    }

    .center-block {
        display: flex;
        justify-content: center;
    }

    #table3 {
        border-left: none;
    }
    #table3 tr > th:first-child {
        border-left: none;
    }

    @media print {
        .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th{
            border-color:#000 !important;
        }

        table{
            border-collapse: collapse !important;
        }
        button.btn{
            display: none !important;
        }
        i.fa-heart{
            color: #f44336 !important;
        }
        
        .table-bordered>tbody>tr>td.no-border{
            border: none !important;
        }
        table td.bg-theme1,table th.bg-theme1,table tr.bg-theme1{
            background: #FC6558 !important;
            color: #fff !important;
        }
        table td.bg-theme2,table th.bg-theme2,table tr.bg-theme2{
            background: lightseagreen !important;
            color: #fff;
        }
        table td.bg-theme3,table th.bg-theme3,table tr.bg-theme3{
            background: #E2C4C2 !important;
            color: #fff;
        }
        table tr td b{
            color:#fff !important;
        }

        #table3 {
            border-left: none ;
        }
        #table3 tr > th:first-child {
            border-left: none !important;
        }
    }

</style>
<body  style="margin-top:1em;background:#fff">
<?php        $ctc = array(
            'Basic' => $select8->basic, 
            'House Rent Allowance' => $select8->hra, 
            'Conveyance' => $select8->conveyance, 
            'Medical Allowance' => $select8->medicalAllowance, 
            'medical Insurance' => $select8->medicalInsurance, 
            'Telephone Allowance' => $select8->telephone, 
            'Leave Travel Allowance' => $select8->leaveTravel, 
            'Uniform Allowance' => $select8->uniform, 
            'Gratuity Allowance' => $select8->gratuity, 
            'Super Annuation Allowance' => $select8->superAnnuation, 
            'Annual Bonus Allowance' => $select8->annualBonus, 
            'Festival Bonus Allowance' => $select8->festivalBonus, 
            'Incentives ' => $select8->incentives, 
            'Other ' => $select8->others, 
            'Leave Encashment Allowance' => $select8->leaveEncashment, 
            );
        $basicSum=$select8->sum;

?>
    <div class="section">
        <span id="logo" class="fa btn btn-lg" >Sashtechs <div></div></span>
        
        <br>

        <table class="table table-bordered table-condensed" cols="4">
            
            <tr >
                <th class="bg-theme1" colspan="4">Sashtechs</th>
            </tr>
            
            <tr><th class="text-center" colspan="4">Taxslip upto the month of - 2016</th></tr>

            <tr>
                <td class="bg-theme1" width="200">Name</td>
                <td width="280"><?php if (isset($select6->firstName)) echo $select6->firstName; echo " "; if (isset($select6->firstName)) echo $select6->lastName?></td>
                <td class="bg-theme1" width="200">PAN</td>
                <td width="300"><?php if (isset($select7)) echo $select7->PAN?></td>
            </tr>
            <tr>
                <td class="bg-theme1">Employee code</td>
                <td><?php if (isset($select6)) echo $select6->id?></td>
                <td class="bg-theme1">PF Number</td>
                <td><?php if (isset($select7)) echo $select7->PFAcNo?></td>
            </tr>
            <tr>
                <td class="bg-theme1">Designation</td>
                <td><?php if (isset($select6)) echo $select6->title?></td>
                <td class="bg-theme1">ESI Number</td>
                <td><?php if (isset($select7)) echo $select7->esiNumber?></td>
            </tr>
            <tr>
                <td class="bg-theme1">D.O.J</td>
                <td><?php if (isset($select7)) echo $select6->joinDate?></td>
                <td class="bg-theme1"></td>
                <td></td>
            </tr>
            <tr>
                <td class="bg-theme1">Location</td>
                <td>Delhi</td>
                <td class="bg-theme1"></td>
                <td></td>
            </tr>
        </table>

        <br>

        <!-- <table id="last" class="table table-bordered table-condensed" cols="6">
            <tr>
                <th width="400" class="bg-theme1 text-center" colspan="3">Earning Details (Annual)</th>
                <th class="bg-theme1 text-center" colspan="3">Chapter VI - Deduction Details</th>
            </tr>
            <tr>
                <th width="200" class="bg-theme2">Particulars</th>
                <th class="bg-theme2">Exempt Amount</th>
                <th class="bg-theme2">Taxable Amount</th>
                <th width="200" class="bg-theme2">Particulars</th>
                <th class="bg-theme2">Claimed Amount</th>
                <th class="bg-theme2">Approved Amount</th>
            </tr>
                <?php //if (isset($select8->basic)) {?>
            <tr>
                <td class="no-border">Basic</td>
                <td class="no-border">0</td>
                <td><?php //echo $select8->basic;}?></td>
                
                <td class="no-border">PF Company</td>
                <td class="no-border">0</td>
                <td>0</td>
            </tr>
            <tr>
                <td class="no-border">House Rent Allowance</td>
                <td class="no-border">0</td>
                <td>0</td>
                <td class="no-border">PF Employee</td>
                <td class="no-border">0</td>
                <td>0</td>
            </tr>
            <tr>
                <td class="no-border">Bonus</td>
                <td class="no-border">0</td>
                <td>0</td>
                <td class="no-border">VPF</td>
                <td class="no-border">0</td>
                <td>0</td>
            </tr>
            <tr>
                <td class="no-border">Allowance</td>
                <td class="no-border">0</td>
                <td>0</td>
                <td class="no-border">LIC</td>
                <td class="no-border">0</td>
                <td>0</td>
            </tr>
            <tr>
                <td class="no-border">...</td>
                <td class="no-border">0</td>
                <td>0</td>
                <td class="no-border">ULIP</td>
                <td class="no-border">0</td>
                <td>0</td>
            </tr>
            <tr>
                <td class="no-border">...</td>
                <td class="no-border">0</td>
                <td>0</td>
                <td class="no-border">ELSS</td>
                <td class="no-border">0</td>
                <td>0</td>
            </tr>
            <tr>
                <td class="no-border">...</td>
                <td class="no-border">0</td>
                <td>0</td>
                <td class="no-border">PPF</td>
                <td class="no-border">0</td>
                <td>0</td>
            </tr>
            <tr>
                <td class="no-border">...</td>
                <td class="no-border">0</td>
                <td>0</td>
                <td class="no-border">NSC</td>
                <td class="no-border">0</td>
                <td>0</td>
            </tr>
            <tr>
                <td class="no-border">...</td>
                <td class="no-border">0</td>
                <td>0</td>
                <td class="no-border">MUTUAL FUNDS</td>
                <td class="no-border">0</td>
                <td>0</td>
            </tr>
            <tr>
                <td class="no-border bg-theme1"><b>Gross Salary</b></td>
                <td class="no-border bg-theme1"></td>
                <td class="bg-theme1"></td>
                <td class="no-border">FIXED DEPOSIT</td>
                <td class="no-border">0</td>
                <td>0</td>
            </tr>
            <tr>
                <td class="no-border">Exemption u/s 10</td>
                <td class="no-border">0</td>
                <td>0</td>
                <td class="no-border">Repayment of Housing Loan</td>
                <td class="no-border">0</td>
                <td>0</td>
            </tr>
            <tr>
                <td class="no-border">Taxable Salary</td>
                <td class="no-border">0</td>
                <td>0</td>
                <td class="no-border bg-theme2">Total u/s 80-C</td>
                <td class="no-border bg-theme3"></td>
                <td>0</td>
            </tr>
            <tr>
                <td class="no-border">Other Income</td>
                <td class="no-border">0</td>
                <td>0</td>
                <td class="no-border">Pension Fund u/s 80-CCC</td>
                <td class="no-border">0</td>
                <td>0</td>
            </tr>
            <tr>
                <td class="no-border">Aggregate of Chapter VI</td>
                <td class="no-border">0</td>
                <td>0</td>
                <td class="no-border bg-theme2">Total U/s 80 CCE</td>
                <td class="no-border bg-theme3"></td>
                <td>0</td>
            </tr>
            <tr>
                <td class="no-border">Total Income</td>
                <td class="no-border">0</td>
                <td>0</td>
                <td class="no-border">80D</td>
                <td class="no-border">0</td>
                <td>0</td>
            </tr>
            <tr>
                <td class="no-border">Tax on Total Income</td>
                <td class="no-border">0</td>
                <td>0</td>
                <td class="no-border">80DD</td>
                <td class="no-border">0</td>
                <td>0</td>
            </tr>
            <tr>
                <td class="no-border">Rebate</td>
                <td class="no-border">0</td>
                <td>0</td>
                <td class="no-border">80DDB</td>
                <td class="no-border">0</td>
                <td>0</td>
            </tr>
            <tr>
                <td class="no-border">Surcharge/EC/HEC</td>
                <td class="no-border">0</td>
                <td>0</td>
                <td class="no-border">80E</td>
                <td class="no-border">0</td>
                <td>0</td>
            </tr>
            <tr>
                <td class="no-border">Total Tax Payable</td>
                <td class="no-border">0</td>
                <td>0</td>
                <td class="no-border">80EE</td>
                <td class="no-border">0</td>
                <td>0</td>
            </tr>
            <tr>
                <td class="no-border">Tax deducted so far</td>
                <td class="no-border">0</td>
                <td>0</td>
                <td class="no-border">80U</td>
                <td class="no-border">0</td>
                <td>0</td>
            </tr>
            <tr>
                <td class="no-border">Tax for the month</td>
                <td class="no-border">0</td>
                <td>0</td>
                <td class="no-border bg-theme1">Total Deduction under Chapter VI</td>
                <td class="no-border bg-theme3"></td>
                <td class="bg-theme1">0</td>
            </tr>
        </table> -->

        <div class="center-block">
        
            <table id="table2" class="table-bordered table-condensed" cols="3" >
                <tr>
                    <th class="bg-theme1 text-center" colspan="3">Earning Details (Annual)</th>
                </tr>
                <tr>
                    <th width="240" class="bg-theme2">Particulars</th>
                    <th class="bg-theme2">Exempt Amount</th>
                    <th class="bg-theme2">Taxable Amount</th>
                </tr>
                <?php foreach ($ctc as $key => $value) { 
                    if ($ctc[$key]>0){ ?>
                    <tr>
                        <td class="no-border"><?php echo $key; ?></td>
                        <td class="no-border"><?php echo $ctc[$key]; ?></td>
                        <td>0</td>
                     </tr>    
                <?php } else 
                        continue;
                    }
                ?>
                <!-- <tr>
                    <td class="no-border">Basic</td>
                    <td class="no-border">0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="no-border">House Rent Allowance</td>
                    <td class="no-border">0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="no-border">Bonus</td>
                    <td class="no-border">0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="no-border">Allowance</td>
                    <td class="no-border">0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="no-border">...</td>
                    <td class="no-border">0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="no-border">...</td>
                    <td class="no-border">0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="no-border">...</td>
                    <td class="no-border">0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="no-border">...</td>
                    <td class="no-border">0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="no-border">...</td>
                    <td class="no-border">0</td>
                    <td>0</td>
                </tr> -->
                <tr>
                    <td class="no-border bg-theme1"><b>Gross Salary</b></td>
                    <td class="no-border bg-theme1">0</td>
                    <td class="bg-theme1">0</td>
                </tr>
                <tr>
                    <td class="no-border">Exemption u/s 10</td>
                    <td class="no-border">0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="no-border">Taxable Salary</td>
                    <td class="no-border">0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="no-border">Other Income</td>
                    <td class="no-border">0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="no-border">Aggregate of Chapter VI</td>
                    <td class="no-border">0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="no-border">Total Income</td>
                    <td class="no-border">0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="no-border">Tax on Total Income</td>
                    <td class="no-border">0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="no-border">Rebate</td>
                    <td class="no-border">0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="no-border">Surcharge/EC/HEC</td>
                    <td class="no-border">0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="no-border">Total Tax Payable</td>
                    <td class="no-border">0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="no-border">Tax deducted so far</td>
                    <td class="no-border">0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="no-border">Tax for the month</td>
                    <td class="no-border">0</td>
                    <td>0</td>
                </tr>
            </table>

            <table id="table3" class="table-bordered table-condensed" cols="3">
                <tr>
                    <th class="bg-theme1 text-center" colspan="3">Chapter VI - Deduction Details</th>
                </tr>
                <tr>
                    <th width="250" class="bg-theme2">Particulars</th>
                    <th class="bg-theme2">Claimed Amount</th>
                    <th class="bg-theme2">Approved Amount</th>
                </tr>

                <tr>
                    <td class="no-border">PF Company</td>
                    <td class="no-border">0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="no-border">PF Employee</td>
                    <td class="no-border">0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="no-border">VPF</td>
                    <td class="no-border">0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="no-border">LIC</td>
                    <td class="no-border">0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="no-border">ULIP</td>
                    <td class="no-border">0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="no-border">ELSS</td>
                    <td class="no-border">0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="no-border">PPF</td>
                    <td class="no-border">0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="no-border">NSC</td>
                    <td class="no-border">0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="no-border">MUTUAL FUNDS</td>
                    <td class="no-border">0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="no-border">FIXED DEPOSIT</td>
                    <td class="no-border">0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="no-border">Repayment of Housing Loan</td>
                    <td class="no-border">0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="no-border bg-theme2">Total u/s 80-C</td>
                    <td class="no-border bg-theme3"></td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="no-border">Pension Fund u/s 80-CCC</td>
                    <td class="no-border">0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="no-border bg-theme2">Total U/s 80 CCE</td>
                    <td class="no-border bg-theme3"></td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="no-border">80D</td>
                    <td class="no-border">0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="no-border">80DD</td>
                    <td class="no-border">0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="no-border">80DDB</td>
                    <td class="no-border">0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="no-border">80E</td>
                    <td class="no-border">0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="no-border">80EE</td>
                    <td class="no-border">0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="no-border">80U</td>
                    <td class="no-border">0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td class="no-border bg-theme1">Total Deduction under Chapter VI</td>
                    <td class="no-border bg-theme3"></td>
                    <td class="bg-theme1">0</td>
                </tr>

            </table>

        </div>
        <div class="clearfix"></div>
        <br>
        <div class="heart-beat">Designed with <i class="fa fa-heart text-danger"></i> Sashtechs</div>
        <br>
        <button class="btn btn-raised bg-theme" onclick="print()">PRINT</button>
    </div>
</body>
</html>

<script>
    $.material.init();
</script>
