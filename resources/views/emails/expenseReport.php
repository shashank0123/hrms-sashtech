<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Expense Report - Sashtechs</title>
<style>
/* -------------------------------------
		GLOBAL
------------------------------------- */
* {
	margin: 0;
	padding: 0;
	font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
	font-size: 100%;
	line-height: 1.6;
}
img {
	max-width: 100%;
}
body {
	-webkit-font-smoothing: antialiased;
	-webkit-text-size-adjust: none;
	width: 100%!important;
	height: 100%;
}
/* -------------------------------------
		ELEMENTS
------------------------------------- */
a {
	color: #fd6e2a;
}
.btn-primary {
	text-decoration: none;
	color: #FFF;
	background-color: #cf3d3d;
	border: 1px solid #cf3d3d;
	padding: 5px 20px;
	text-align: center;
	cursor: pointer;
	font-size: 14px;
	font-weight: 500;
	display: inline-block;
	border-radius: 3px;
	text-transform: uppercase;
	outline: none;
	box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12);
	transition: box-shadow 0.2s cubic-bezier(0.4, 0, 1, 1);
}
.btn-primary:hover {
	box-shadow: 0 4px 4px 0 rgba(0, 0, 0, 0.14), 0 4px 4px -1px rgba(0, 0, 0, 0.18), 0 4px 10px 0 rgba(0, 0, 0, 0.12);
}
.btn-secondary {
	text-decoration: none;
	color: #FFF;
	background-color: #aaa;
	border: solid #aaa;
	border-width: 10px 20px;
	line-height: 2;
	font-weight: bold;
	margin-right: 10px;
	text-align: center;
	cursor: pointer;
	display: inline-block;
	border-radius: 25px;
}
.last {
	margin-bottom: 0;
}
.first {
	margin-top: 0;
}
.padding {
	padding: 10px 0;
}
/* -------------------------------------
		BODY
------------------------------------- */
table.body-wrap {
	width: 100%;
	padding: 20px;
}
table.body-wrap .container {
	border: 1px solid #f0f0f0;
}
/* -------------------------------------
		FOOTER
------------------------------------- */
table.footer-wrap {
	width: 100%;	
	clear: both!important;
}
.footer-wrap .container p {
	font-size: 12px;
	color: #666;
	
}
table.footer-wrap a {
	color: #999;
}
/* -------------------------------------
		TYPOGRAPHY
------------------------------------- */
h1, h2, h3 {
	font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
	color: #000;
	margin: 20px 0 10px;
	line-height: 1.2;
	font-weight: 200;
}
h1 {
	font-size: 36px;
}
h2 {
	font-size: 28px;
}
h3 {
	font-size: 22px;
}
p, ul, ol {
	margin-bottom: 10px;
	font-weight: normal;
	font-size: 14px;
}
ul li, ol li {
	margin-left: 5px;
	list-style-position: inside;
	list-style-type: none;
}
/* ---------------------------------------------------
		RESPONSIVENESS
		Nuke it from orbit. It's the only way to be sure.
------------------------------------------------------ */
/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
.container {
	display: block!important;
	max-width: 600px!important;
	margin: 0 auto!important; /* makes it centered */
	clear: both!important;
}
/* Set the padding on the td rather than the div for Outlook compatibility */
.body-wrap .container {
	padding: 20px;
}
/* This should also be a block element, so that it will fill 100% of the .container */
.content {
	max-width: 600px;
	margin: 0 auto;
	display: block;
}
/* Let's make sure tables in the content area are 100% wide */
.content table {
	width: 100%;
}

.footer-wrap a img {
	height: 4em;
}
</style>
</head>

<body bgcolor="#f6f6f6">

<!-- body -->
<table class="body-wrap" bgcolor="#f6f6f6">
	<tr>
		<td></td>
		<td class="container" bgcolor="#FFFFFF">

			<!-- content -->
			<div class="content">
			<table>
				<tr>
					<td>
					<!-- <img src="https://sashtechs.com/blog/wp-content/uploads/2015/08/email.jpg" alt="Welcome To sashtechs" style="width:650px;height:296px;"> -->
					
						<h3 style='font-family: sans-serif;text-decoration: underline;font-weight: 500'>Sashtechs</h3>
						<br />
						<br />

 						<div style="margin-top:8px;">	<p style="font-weight: 100;">Hi <?php echo $firstName . ' ' . $lastName . ','; ?></p><br/>
						<p>
							Your expense report in the amount of Rs XXXX  has been successfully submitted
						</p>
						<br>
						<p> Sincerely,</p>
						<p> Sashtechs  &nbsp;&nbsp; Customer Experience</p> 
						<a href="mailto:support@Sashtechs.com">Support @ Sashtechs</a>

						<p>Please note: This e-mail was sent from an auto-notification system that cannot accept incoming e-mail. Please do not reply to this message.</p>

					</td>
				</tr>
			</table>
			</div>
			<!-- /content -->
			
		</td>
		<td></td>
	</tr>
</table>
<!-- /body -->

<!-- footer -->
<table class="footer-wrap">
	<tr>
		<td></td>
		<td class="container">
			
			<!-- content -->
			<div class="content">
				<table>
					<tr>
						<td align="center">
							<p>Made with Love Sashtechs</p>
							<a href="https://Sashtechs.com/">Sashtechs.com</a> | Awesome payroll, benefits & timetracking app. <br />
							<p style="position: relative;"> 
								<br />
								<span>Download mobile app </span><br />
								<a href="#"><img src="google_play.png" alt="google store" /></a>&nbsp;&nbsp;&nbsp; <a href="#"><img src="apple_App_Store.png" alt="apple store" /></a>&nbsp;&nbsp;&nbsp; 
								<a href="#"><img src="fb.png" alt="facebook" /></a>&nbsp;&nbsp;&nbsp; 
								<a href="#"><img src="linkedin.png" alt="linkedin" /></a>&nbsp;&nbsp;&nbsp; 
								<a href="#"><img src="twitter.png" alt="twitter" /></a>
							</p>
						</td>
					</tr>
				</table>
			</div>
			<!-- /content -->
			
		</td>
		<td></td>
	</tr>
</table>
<!-- /footer -->

</body>
</html>
