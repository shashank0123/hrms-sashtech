<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>


<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">
                       
                        <form action="#" method="POST" class="form-horizontal">

                            <br>
                            <legend>Insurance Requirement</legend>
                            <br>
                            <div class="col-md-12 col-sm-12 col-xs-12">

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="sumAssured"><span>Sum Assured</span></label>
                                            <input class="form-control sumAssured" id="sumAssured" name="sumAssured" type="text" style="cursor: auto;"
                                            <?php echo (!empty($requirements->sumAssured)) ? 'value="' . $requirements->sumAssured .'"' : '' ; ?>
                                            >
                                        </div>
                                    </div>
                                    <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                        <p>This amount is for employee sum assured</p>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="togglebutton form-group">
                                            <label>
                                                Family Floater Y/N
                                                &nbsp;&nbsp;&nbsp; 
                                                <input type="checkbox" class="familyFloater" name="familyFloater"
                                                <?php echo (!empty($requirements->familyFloater)) ? 'checked' : ''; ?>
                                                >
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12">

                                        <div class="form-group" id="familyFloaterOption">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="radio radio-primary col-md-8 col-sm-8" data-hint="true">
                                                    <label>
                                                        <input type="radio" class="familyFloaterOption" name="familyFloaterOption" value="2" 
                                                        <?php 
                                                            if (empty($requirements->familyFloaterOption) or (intval($requirements->familyFloaterOption) == 2)) 
                                                            {
                                                                echo 'checked=""';
                                                            }
                                                         ?>
                                                        > 
                                                        1 + 1
                                                    </label>
                                                </div>
                                                <div class="hint-text col-md-8 text-muted hidden-sm hidden-xs">
                                                    <p>self / spouse</p>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="radio radio-primary col-md-8 col-sm-8" data-hint="true">
                                                    <label>
                                                        <input type="radio" class="familyFloaterOption" name="familyFloaterOption" value="4"
                                                        <?php 
                                                            if (!empty($requirements->familyFloaterOption))
                                                            if (intval($requirements->familyFloaterOption) == 4) 
                                                            {
                                                                echo 'checked=""';
                                                            }
                                                         ?>
                                                        >
                                                        1 + 3
                                                    </label>
                                                </div>
                                                <div class="hint-text col-md-8 text-muted hidden-sm hidden-xs">
                                                    <p>self spouse two children</p>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="radio radio-primary col-md-8 col-sm-8" data-hint="true">
                                                    <label>
                                                        <input type="radio" class="familyFloaterOption" name="familyFloaterOption" value="6" 
                                                        <?php 
                                                            if (!empty($requirements->familyFloaterOption) )
                                                            if (intval($requirements->familyFloaterOption) == 6) 
                                                            {
                                                                echo 'checked=""';
                                                            }
                                                         ?>
                                                         > 
                                                         1 + 5
                                                    </label>
                                                </div>
                                                <div class="hint-text col-md-8 text-muted hidden-sm hidden-xs">
                                                    <p>self spouse children parents</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="togglebutton form-group">
                                            <label>
                                                Waiver of 30 days Waiting Period
                                                &nbsp;&nbsp;&nbsp; 
                                                <input type="checkbox" class="waitingPeriod" name="waitingPeriod"
                                                <?php echo (!empty($requirements->waitingPeriod)) ? 'checked' : ''; ?>
                                                >
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="togglebutton form-group">
                                            <label>
                                                Waiver of First Year Exclusion
                                                &nbsp;&nbsp;&nbsp; 
                                                <input type="checkbox" name="firstYearExclusion" class="firstYearExclusion" 
                                                <?php echo (!empty($requirements->firstYearExclusion)) ? 'checked' : ''; ?>
                                                >
                                            </label>
                                        </div>
                                    </div>
                                </div> 

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="togglebutton form-group">
                                            <label>
                                                Waiver of First Two Year Exclusion
                                                &nbsp;&nbsp;&nbsp; 
                                                <input type="checkbox" name="twoYearExclusion" class="twoYearExclusion"
                                                <?php echo (!empty($requirements->twoYearExclusion)) ? 'checked' : ''; ?>
                                                >
                                            </label>
                                        </div>
                                    </div>
                                </div>  

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="togglebutton form-group">
                                            <label>
                                                Pre Existing Disease Cover
                                                &nbsp;&nbsp;&nbsp; 
                                                <input type="checkbox" name="DiseaseCover" class="DiseaseCover"
                                                <?php echo (!empty($requirements->DiseaseCover)) ? 'checked' : ''; ?>
                                                >
                                            </label>
                                        </div>
                                    </div>
                                </div> 

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="togglebutton form-group">
                                            <label>
                                                Maternity Benefit
                                                &nbsp;&nbsp;&nbsp; 
                                                <input type="checkbox" name="maternityBenefit" class="maternityBenefit" id="maternityBenefit"
                                                <?php echo (!empty($requirements->maternityBenefit)) ? 'checked' : ''; ?>
                                                >
                                            </label>
                                        </div>
                                    </div>
                                </div>     

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="maternityLimit"><span>Maternity Limit</span></label>
                                            <input class="form-control maternityLimit" id="maternityLimit" name="maternityLimit" type="number" min="0" style="cursor: auto;"
                                            value="<?php echo (!empty($requirements->maternityLimit)) ? $requirements->maternityLimit  : '0'; ?>" 
                                            >
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="togglebutton form-group">
                                            <label>
                                                Extension of 9 months
                                                &nbsp;&nbsp;&nbsp; 
                                                <input type="checkbox" name="extensionNineMonth" class="extensionNineMonth" id="extensionNineMonth"
                                                <?php echo (!empty($requirements->extensionNineMonth)) ? 'checked' : ''; ?>
                                                >
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="togglebutton form-group">
                                            <label>
                                                Waiver of per-post natal expenses
                                                &nbsp;&nbsp;&nbsp; 
                                                <input type="checkbox" name="waiverExpense" class="waiverExpense" id="waiverExpense"
                                                <?php echo (!empty($requirements->waiverExpense)) ? 'checked' : ''; ?>
                                                >
                                            </label>
                                        </div>

                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="waiverAmount"><span>Waiver Amount</span></label>
                                            <input class="form-control waiverAmount" id="waiverAmount" name="waiverAmount" type="number" min="0" style="cursor: auto;"
                                            value="<?php echo (!empty($requirements->waiverAmount)) ? $requirements->waiverAmount  : '0'; ?>" 
                                            >
                                        </div>
                                    </div>
                                </div>     

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="togglebutton form-group">
                                            <label>
                                                Child Cover From Day One
                                                &nbsp;&nbsp;&nbsp; 
                                                <input type="checkbox" name="childCover" class="childCover"
                                                <?php echo (!empty($requirements->childCover)) ? 'checked' : ''; ?>
                                                >
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="togglebutton form-group">
                                            <label>
                                                Co - Pay
                                                &nbsp;&nbsp;&nbsp; 
                                                <input type="checkbox" name="copay" class="copay" id="copay"
                                                <?php echo (!empty($requirements->copay)) ? 'checked' : ''; ?>
                                                >
                                            </label>
                                        </div>
                                    </div>
                                </div>     

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="copayAmount"><span>Co-Pay Amount</span></label>
                                            <input class="form-control copayAmount" id="copayAmount" type="number" min="0" style="cursor: auto;"
                                            value="<?php echo (!empty($requirements->copayAmount)) ? $requirements->copayAmount  : '0'; ?>" 
                                            >
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="togglebutton form-group">
                                            <label>
                                                Corporate Buffer
                                                &nbsp;&nbsp;&nbsp; 
                                                <input type="checkbox" name="corporateBuffer" class="corporateBuffer" 
                                                <?php echo (!empty($requirements->corporateBuffer)) ? 'checked' : ''; ?>
                                                >
                                            </label>
                                        </div>
                                    </div>
                                </div>                           


                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <div class="col-md-8 col-sm-8 col-xs-8 col-md-offset-4 col-sm-offset-3 col-xs-offset-4">
                                            <span class="btn btn-default btn-raised cancel" onclick="history.back();">Cancel</span>
                                            <input type="submit" class="btn bg-theme btn-raised next" value="Submit">
                                        </div>
                                    </div>
                                </div>

                           </div>
                        </form>
                    </div>

                    <style>

                    .togglebutton > label{
                        display: block;
                        text-align: left;
                    }

                    .toggle {
                        float: right;
                    }
                    </style>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>
$(function() {
    $.material.init();
                     
    $(".familyFloater").change(function(event) {
        if ($(this).is(':checked')) {
            $("#familyFloaterOption").show();
            $("#familyFloaterOption").find('.familyFloaterOption:eq(0)').prop('checked', true).trigger('change');
        }
        else{
            $("#familyFloaterOption").hide();
            $("#familyFloaterOption").find('.familyFloaterOption:eq(0)').prop('checked', true).trigger('change');
        }
    });
    $("#familyFloaterOption").hide();

    $("#maternityBenefit").change(function(event) {
        if ($(this).is(':checked')) {
            $("#maternityLimit, #extensionNineMonth, #waiverExpense").parents(".form-group").show();
        }
        else {
            $("#maternityLimit, #extensionNineMonth, #waiverExpense").parents(".form-group").hide();
            $("#extensionNineMonth, #waiverExpense").prop('checked', false).trigger('change');
            $("#maternityLimit").val(0).trigger('change');
        }
    });

    $("#waiverExpense").change(function(event) {
        if ($(this).is(':checked')) {
            $("#waiverAmount").parents(".form-group").show();
        }
        else {
            $("#waiverAmount").parents(".form-group").hide();
            $("#waiverAmount").val(0).trigger('change');
        }
    });

    $("#waiverAmount").parents(".form-group").hide();
    

    $("#copay").change(function(event) {
        if ($(this).is(':checked')) {
            $("#copayAmount").parents(".form-group").show();
        }
        else {
            $("#copayAmount").parents(".form-group").hide();
            $("#copayAmount").val(0).trigger('change');
        }
    });

    $("#copayAmount").parents(".form-group").hide();


    $("#maternityLimit, #extensionNineMonth, #waiverExpense").parents(".form-group").hide();

    function displayError() {
        swal({
            title : "Error !",
            text : 'Try Again!',
            type : 'error',
            confirmButtonClass : 'bg-theme',
            animation : false,
            width : 400
        });
    };

    function closeAlert() {
        $(".sweet-alert button").first().trigger('click');
    }

    $("form.form-horizontal").submit(function(event) {

        event.preventDefault();

        // remove all error messages
        $(".help-block").remove();
        $(".has-error").removeClass('has-error');
        
        swal({
            title : 'Saving',
            html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
            allowOutsideClick : false,
            showConfirmButton : false,
            animation : false,
            width : 400
        });

        $.ajax({
            url: location.href,
            type: 'POST',
            data: $('.main form').serialize(), 
        })
        .done(function(data) {
            if (data.status == 200) {
                swal({
                    title : "Success !",
                    text : data.message,
                    type : 'success',
                    confirmButtonClass : 'bg-theme',
                    animation : false,
                    width : 400
                });
                getAndInsert(data.url);
                history.pushState(null, null, data.url);
            }
            else if (data.status == 400) {
                swal({
                    title : "Error !",
                    text : data.message,
                    type : 'error',
                    confirmButtonClass : 'bg-theme',
                    animation : false,
                    width : 400
                });
            }
            else{  // Validation Error
                displayError();
                // show Errors
                for(var fields in data) {
                    $('#'+fields).parents('.form-group').addClass('has-error');
                    $('#'+fields).parent().append('<p class="help-block">'+ data[fields][0] +'</p>');
                }
            }
        })
        .error(function() {
            displayError();
        });
    });

    $(".main .form-horizontal input").trigger('change');
});


</script>
