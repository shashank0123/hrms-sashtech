<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>


<body  style="margin-top:2em;">
<?php 
    foreach ($result2 as $key => $value) 
    {
        if (empty($value->DateOfBirth)) 
        {
            continue;
        }

        $year = date('Y');

        $today = date('Y-m-d');
        $month = date('m');
        $currentDay = date('d');

        // Following values were used for testing
        // $today = '28-12-2016';
        // $month = 12;
        // $currentDay = 28;

        if ($month == 12) 
        {
            if ($currentDay > 24)
            {
                $bdayMonth = date('m', strtotime($value->DateOfBirth));
                if ($bdayMonth == 1)
                {
                    $bdayDate = date('d', strtotime($value->DateOfBirth));

                    $next = date_create(date('d-m-', strtotime($value->DateOfBirth)) . ($year+1));

                    $diff = date_diff($next, date_create($today));

                    if ($diff->days < 8)
                    {
                        $year +=1;
                    }
                }
            }
        }

        $next =  $year . date('-m-d', strtotime($value->DateOfBirth));
        
        $diff = date_diff(date_create($next), date_create($today));

        // var_dump($next);
        
        if ((($diff->days < 8) and ($diff->invert == 1)) or ($diff->days == 0))
        {
            foreach ($result3 as $key2 => $value2) 
            {
                if ($value->userid == $value2->id) 
                {
                    $name = $value2->firstName . ' ' . $value2->lastName;
                }
            }

            $bDay[] = array(
                'id' => $value->userid, 
                'name' => $name,
                'on' => $next,
                );
        }
    }

    if (isset($bDay)) 
    {
        for ($i=0; $i < count($bDay) ; $i++) 
        {
            for ($j=0; $j < count($bDay); $j++) 
            {
                if ($bDay[$i] == $bDay[$j]) 
                {
                    continue;
                }

                $datetime1  = new DateTime($bDay[$j]['on']);
                $datetime2  = new DateTime($bDay[$i]['on']);

                $interval   = $datetime1->diff($datetime2);

                $invert     = $interval->invert;

                if ($invert == 1)
                {
                    $temp       = $bDay[$i];
                    $bDay[$i]   = $bDay[$j];
                    $bDay[$j]   = $temp;
                }
            }
        }
    }
?>
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">

                        <br><br>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <div class="tweet">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <form action="" method="POST" class="form-horizontal">
                                         <div class="form-group">
                                            <textarea class="textarea" rows="3" id="textArea" name="message" placeholder="Tweet to your employees"></textarea>
                                        </div>
                                        <input type="submit" class="btn btn-raised bg-theme pull-right" value="send">
                                    </form>
                                </div>
                                <div class="clearfix"></div>
                                <br>
                            </div>
                        </div>

                        <?php if (session()->get('plan') == 'manager'): ?>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="extra text-center notice">
                                <div class="box">
                                    <img src="images/Pin.png" class="boardPin" alt="pin">
                                    <h4>Your Manager</h4>
                                    <div class="profilePic"></div>
                                    <br>
                                    <h6><?php if (!empty($result8->firstName)): ?>
                                    <?php echo $result8->firstName . ' ' . $result8->lastName; ?>
                                    <?php else: ?>
                                        Manager
                                    <?php endif ?>
                                    </h6>
                                    <h6>
                                        <?php if (!empty($result7->phone)): ?>
                                            <?php echo $result7->phone; ?>
                                        <?php else: ?>
                                            0123456789
                                        <?php endif ?>
                                    </h6>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>

                        <div class="clearfix"></div>
                        <br><br>

                        <div id="wrapper">

                            <div id="page-wrapper">

                                <div class="container-fluid row">
                                    <!-- /.row -->

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h3 class="panel-title"><i class="fa fa-clock-o fa-fw"></i> Notifications</h3>
                                            </div>
                                            <div class="panel-body">
                                                <div class="list-group">
                                                    <h5>Quote of the Day</h5>
                                                    <span class="list-group-item">
                                                        <i class="fa fa-fw fa-bullhorn"></i> 
                                                        "<?php echo $result->quote; ?>" - <?php echo $result->author; ?>
                                                    </span>
                                                </div>

                                                <?php if (!empty($bDay) and isset($bDay)): ?>
                                                <h5>Upcoming Birthdays</h5>
                                                <div class="list-group">
                                                    <?php foreach ($bDay as $key => $value): ?>
                                                        <?php 
                                                            if ($value['on'] < date('Y-m-d')) 
                                                            {
                                                                continue;
                                                            }
                                                         ?>
                                                        <?php if(session()->get('employeeId') == $value['id']): ?>
                                                            <?php if (date_diff(date_create(date('d-m-Y', strtotime($value['on']))), date_create(date('d-m-Y')))->days == 0): ?>
                                                            <span class="list-group-item">
                                                                <i class="fa fa-fw fa-birthday-cake"></i> Happy Birthday! :D
                                                            </span>
                                                            <?php else: ?>
                                                            <span class="list-group-item">
                                                                <span class="badge"><?php echo date('d-M', strtotime($value['on'])); ?></span>
                                                                <i class="fa fa-fw fa-birthday-cake"></i> Your birthday is coming on
                                                            </span>
                                                            <?php endif; ?>
                                                        <?php else: ?>
                                                            <?php if (date_diff(date_create(date('d-m-Y', strtotime($value['on']))), date_create(date('d-m-Y')))->days == 0): ?>
                                                            <span class="list-group-item">
                                                                <span class="badge">Today!</span>
                                                                <i class="fa fa-fw fa-birthday-cake"></i> It's <?php echo $value['name'] . "'s"; ?> birthday today!
                                                            </span>
                                                            <?php else: ?>
                                                            <span class="list-group-item">
                                                                <span class="badge"><?php echo date('d-M', strtotime($value['on'])); ?></span>
                                                                <i class="fa fa-fw fa-birthday-cake"></i> Upcoming birthday of <?php echo $value['name']; ?> on
                                                            </span>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                </div>
                                                <?php endif; ?>

                                                <?php if (!empty($result4)) : ?>
                                                <h5>Tweets</h5>
                                                <div class="list-group">
                                                    <?php foreach($result4 as $key => $value): ?>
                                                        <?php 
                                                            $datetime1  = new DateTime($value->created_at);
                                                            $datetime2  = new DateTime(date('d-m-Y h:i:sa'));

                                                            $interval   = $datetime1->diff($datetime2);

                                                            if ($interval->y < 1) 
                                                            {
                                                                if ($interval->m == 0) 
                                                                {
                                                                    if ($interval->d == 0) 
                                                                    {
                                                                        if ($interval->h == 0) 
                                                                        {
                                                                            if ($interval->i == 0) { $time = $interval->s . ' seconds ago' ; }
                                                                            else { $time = $interval->i . ' mins ago' ; }
                                                                        }
                                                                        else { $time = $interval->h . ' hours ago' ; }
                                                                    }
                                                                    else { $time = $interval->d . ' days ago' ;
                                                                    } }
                                                                elseif ($interval->m == 1) 
                                                                {
                                                                    $time = $interval->m . ' month and ' . $interval->d . ' days ago' ;
                                                                }
                                                                else { $time = $interval->m . ' months and ' . $interval->d . ' days ago' ; }
                                                            }
                                                            else { $time = 'Older than a year'; }
                                                         ?>

                                                    <span class="list-group-item">
                                                        <span class="badge"><?php echo $time ?></span>
                                                        <i class="fa fa-fw fa-comment"></i>
                                                        <?php echo htmlspecialchars_decode($value->messageContent); ?>
                                                    </span>
                                                    <?php endforeach; ?>
                                                </div>
                                                <?php endif; ?>

                                                <?php if(!empty($result6)): ?>
                                                <h5>Company Policy Updates</h5>
                                                <div class="list-group">
                                                    <?php foreach ($result6 as $key => $value) : ?>
                                                        <?php 
                                                            $datetime1  = new DateTime($value->created_at);
                                                            $datetime2  = new DateTime(date('d-m-Y h:i:sa'));

                                                            $interval   = $datetime1->diff($datetime2);

                                                            if ($interval->y < 1) 
                                                            {
                                                                if ($interval->m == 0) 
                                                                {
                                                                    if ($interval->d == 0) 
                                                                    {
                                                                        if ($interval->h == 0) 
                                                                        {
                                                                            if ($interval->i == 0) { $time = $interval->s . ' seconds ago' ; }
                                                                            else { $time = $interval->i . ' mins ago' ; }
                                                                        }
                                                                        else { $time = $interval->h . ' hours ago' ; }
                                                                    }
                                                                    else { $time = $interval->d . ' days ago' ;
                                                                    } }
                                                                elseif ($interval->m == 1) 
                                                                {
                                                                    $time = $interval->m . ' month and ' . $interval->d . ' days ago' ;
                                                                }
                                                                else { $time = $interval->m . ' months and ' . $interval->d . ' days ago' ; }
                                                            }
                                                            else { $time = 'Older than a year'; }
                                                         ?>
                                                    <span class="list-group-item">
                                                        <span class="badge"><?php echo $time; $time = NULL; ?></span>
                                                        <i class="fa fa-fw fa-bullhorn"></i> Your employer added a new company policy - "<?php echo $value->policyName; ?>". Please check policies for more info.
                                                    </span>
                                                    <?php endforeach; ?>
                                                </div>
                                                <?php endif; ?>
                                                <!-- <div class="text-right">
                                                    <a href="#">View All Activity <i class="fa fa-arrow-circle-right"></i></a>
                                                </div> -->
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <!-- /.container-fluid -->

                            </div>
                            <!-- /#page-wrapper -->

                        </div>

                        <style>
                            .tweet .textarea {
                                width: 100%;
                                border-radius: 3px;
                                padding: 10px;
                                border: 1px solid #ddd;
                            }

                            .profilePic {
                                margin: 0 auto;
                                display: block;
                                width: 8em;
                                height: 8em;
                                text-align: center;
                                line-height: 9em;
                                border-radius: 50%;
                                <?php if (!empty($result7->dp)): ?>
                                background-image: url(<?php echo $result7->dp; ?>);
                                <?php else:; ?>
                                background-image: url(images/testLogo.png);
                                <?php endif ?>
                                background-size: contain;
                                background-repeat: no-repeat;
                                background-position: center;
                                border: 1px dotted;
                            }
                        </style>

                    </div>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>
    $(function() {

        // add active class to corresponding link in menu
        $(".nav li.active").removeClass('active');
        $(".nav.nav-stacked li").filter(function(index) {
            return $(this).data('url') == 'employerDashboard';
        }).addClass('active');

        $.material.init();
    })
</script>
