<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>


<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">

                    <br>
                    <br>
                    <form enctype="multipart/form-data" method="POST" id="addCompanyLocation" class="form-horizontal col-md-9 col-sm-12 col-sm-12" action="">
                        <fieldset>
                            <input type="hidden" name="type" value="branchOffice"></input> 
                            
                            <legend>Add Branch Location</legend>
                            <p>Company locations can be used in a variety of ways, including as your company government filing address, company government mailing address, or as a work location for your employees.</p>
                            
                            <br><br>
                            <div class="clearfix"></div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating is-empty ">
                                        <label class="control-label" for="address"><span>Address</span></label>
                                        <input class="form-control" id="address" name="address" type="text" style="cursor: auto;" placeholder=" ">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating is-empty ">
                                        <label class="control-label" for="propertyNo"><span>Property No. / Description</span></label>
                                        <input class="form-control" id="propertyNo" name="propertyNo" type="text" style="cursor: auto;">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating is-empty ">
                                        <label class="control-label" for="street"><span>Street</span></label>
                                        <input class="form-control" id="street" name="street" type="text" style="cursor: auto;">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating is-empty ">
                                        <label class="control-label" for="city"><span>City</span></label>
                                        <input class="form-control" id="city" name="city" type="text" style="cursor: auto;">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="state" class="col-md-2 control-label">State</label>

                                    <div class="col-md-10">
                                        <select id="state" name="state" class="form-control">
                                            <option value="0">Select State</option>
                                            <option value="AN">Andaman and Nicobar Islands</option>
                                            <option value="AP">Andhra Pradesh</option>
                                            <option value="AR">Arunachal Pradesh</option>
                                            <option value="AS">Assam</option>
                                            <option value="BR">Bihar</option>
                                            <option value="CH">Chandigarh</option>
                                            <option value="CT">Chhattisgarh</option>
                                            <option value="DN">Dadra and Nagar Haveli</option>
                                            <option value="DD">Daman and Diu</option>
                                            <option value="DL">Delhi</option>
                                            <option value="GA">GOA</option>
                                            <option value="GJ">Gujarat</option>
                                            <option value="HR">Haryana</option>
                                            <option value="HP">Himachal Pradesh</option>
                                            <option value="JK">Jammu and Kashmir</option>
                                            <option value="JH">Jharkhand</option>
                                            <option value="KA">Karnataka</option>
                                            <option value="KL">Kerala</option>
                                            <option value="LD">Lakshadweep</option>
                                            <option value="MP">Madhya Pradesh</option>
                                            <option value="MH">Maharashtra</option>
                                            <option value="MN">Manipur</option>
                                            <option value="ML">Meghalaya</option>
                                            <option value="MZ">Mizoram</option>
                                            <option value="NL">Nagaland</option>
                                            <option value="OR">Odisha</option>
                                            <option value="PY">Puducherry</option>
                                            <option value="PB">Punjab</option>
                                            <option value="RJ">Rajasthan</option>
                                            <option value="SK">Sikkim</option>
                                            <option value="TN">Tamil Nadu</option>
                                            <option value="TG">Telangana</option>
                                            <option value="TR">Tripura</option>
                                            <option value="UT">Uttarakhand</option>
                                            <option value="UP">Uttar Pradesh</option>
                                            <option value="WB">West Bengal</option>
                                        </select>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating is-empty ">
                                        <label class="control-label" for="pin"><span>Pin</span></label>
                                        <input class="form-control" id="pin" name="pin" type="text" style="cursor: auto;">
                                    </div>
                                </div>
                            </div>
        
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="col-md-12 col-sm-8 col-xs-8">
                                        <a href="/addCompanyLocation" class="ajaxify cancel btn btn-default btn-raised">Cancel</a>
                                        <span class="btn next btn-primary btn-raised bg-theme">next</span>
                                        <span class="add btn btn-primary btn-raised bg-theme pull-right">Add Another Branch</span>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                    

                </div>
            </div>
        </div>
    </div>
    <br><br>
    <?php require_once('footer.php');?>

</body>
</html>

<script>
    var placeSearch, autocomplete;

    function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('address')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();
        var componentForm = {
            street_number: 'propertyNo',
            route: 'street',
            // locality: 'street',
            administrative_area_level_1: 'state',
            administrative_area_level_2: 'city',
            // country: 'country',
            postal_code: 'pin'
        };

        for (var component in componentForm) {
            document.getElementById(componentForm[component]).value = '';
            document.getElementById(componentForm[component]).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
                var val = place.address_components[i]['long_name'];
                if (addressType=="administrative_area_level_1") {val = place.address_components[i]['short_name'];}
                document.getElementById(componentForm[addressType]).value = val;
                $("#"+componentForm[addressType]).trigger('change')
            }
        }
    }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
    function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }

    function displayError() {
        swal({
            title : "Error !",
            animation: false,
            text : 'Try Again!',
            type : 'error',
            width: 400,
        });
    }

    function closeAlert() {
            $(".sweet-alert button").first().trigger('click');
        }

    $(function() {
        $.material.init();

        $('form.form-horizontal').submit(function(event) {
            event.preventDefault();

            return false;
        });

        $('.add, .next').unbind('click').on('click', function(event) {
            event.preventDefault();
            swal({
                title : 'Saving',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                width: 300
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: $('#addCompanyLocation').serialize() +'&target='+ ($(event.target).hasClass('add') ? 'add' : 'next'), 
            })
            .done(function(data) {
                if (data.message) {
                    swal({
                        title : "Saved",
                        type : 'success',
                        width : 300,
                        animation : false,
                        confirmButtonClass : 'bg-theme',
                        timer : 1000,
                        showConfirmButton: false
                    });
                    getAndInsert(data.url); 
                    history.pushState(null, null, data.url);
                }
                else{
                    displayError();
                    for(var fields in data) {
                        $('#'+fields).parents('.form-group').addClass('has-error');
                        $('#'+fields).parent().append('<p class="help-block">'+ data[fields][0] +'</p>');
                    }
                }
            })
            .fail(function() {
                displayError();
            });
        });

        $("#address").focus(function() {
            geolocate();
        });

        (function () {
            if (typeof(google)=="undefined") {
                var script = document.createElement("script");
                script.type = "text/javascript";
                script.src = "https://maps.googleapis.com/maps/api/js?signed_in=false&libraries=places&components=country:IN";
                document.body.appendChild(script);
            }
            var done = function() {
                if (typeof(google)=="object") {
                    initAutocomplete();
                    clearTimeout(done);
                }
                else
                    setTimeout(done, 100);
            }
            done();
            // setTimeout(initAutocomplete, 500);
        })();

    });
</script>
