<!DOCTYPE html>
<html>
<head>
    <title>Monthly Slip</title>
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
    <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet"></link>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Material Design -->
    <link href="css/bootstrap-material-design.css" rel="stylesheet">
    <link href="css/ripples.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">

    <script src="js/jquery-1.12.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ripples.min.js"></script>
    <script src="js/material.min.js"></script>


    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

</head>

<style>
    .section{
        width:70em;
        padding: 1em;
        margin: 0 auto;
    }

    .table{
        border-collapse: collapse;
        border: 1px solid #000;
    }
    .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th{
        border-color:#000;
        padding-left: 10px;
    }

    .bg-theme1{
        background: #FC6558 !important;
        color: #fff;
    }

    #logo > div:first-child{
        border-top: 1px solid #aaa;
    }

    @media print {
        .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th{
            border-color:#000 !important;
        }

        table{
            border-collapse: collapse !important;
        }
        button.btn{
            display: none !important;
        }
        i.fa-heart{
            color: #f44336 !important;
        }
        
        table td.bg-theme1,table th.bg-theme1,table tr.bg-theme1{
            background: #FC6558 !important;
            color: #fff !important;
        }
        table tr td b{
            color:#fff !important;
        }
    }

</style>
<body  style="margin-top:1em;background:#fff">
    <div class="section">
        <span id="logo" class="fa btn btn-lg" >Sashtechs <div></div></span>
        
        <br>
            <h4 class="text-center">Monthly Slip for Month - 2016</h4>
        <br>

        <table class="table table-bordered " cols="10">

            <tr>
                <th class="bg-theme1">Id</th>
                <th class="bg-theme1" width="200">Name</th>
                <th class="bg-theme1">Base Pay</th>
                <th class="bg-theme1">Bonus</th>
                <th class="bg-theme1">Commision</th>
                <th class="bg-theme1">Other Earnings</th>
                <th class="bg-theme1">Reimbursements</th>
                <th class="bg-theme1">Gross Payment</th>
                <th class="bg-theme1">Deductions</th>
                <th class="bg-theme1">Net payments</th>
            </tr>
            <?php if(isset($employees)): ?>
            <?php foreach ($employees as $key => $value): ?>
            <tr>
                <td><?php echo $value->id; ?></td>
                <td><?php echo $value->firstName . ' ' . $value->lastName;?></td>
                <td><?php (isset($value->basePay)) ? $value->basePay : ''; ?></td>
                <td><?php echo $value->bonus; ?></td>
                <td><?php echo $value->commission; ?></td>
                <td><?php echo $value->otherEarning; ?></td>
                <td><?php echo $value->reimbursement; ?></td>
                <td><?php (isset($value->gross)) ? $value->gross : ''; ?></td>
                <td><?php echo $value->deduction; ?></td>
                <td><?php echo $value->grossPay; ?></td>
            </tr>
            <?php endforeach; ?>
            <?php else: ?>
                <tr>
                    <th>1</th>
                    <th>Odin</th>
                    <th>&#x221e;</th>
                    <th>&#x221e;</th>
                    <th>&#x221e;</th>
                    <th>&#x221e;</th>
                    <th>&#x221e;</th>
                    <th>&#x221e;</th>
                    <th>0</th>
                    <th>&#x221e;</th>
                </tr>
            <?php endif; ?>
        </table>

        <div class="heart-beat">Designed with <i class="fa fa-heart text-danger"></i> Sashtechs</div>
        <?php if (!isset($employees)) {
            echo "PS - This is only dummy data. March month has some real data.";
        } ?>
        <br>    
        <button class="btn btn-raised bg-theme" onclick="print()">PRINT</button>
    </div>
</body>
</html>

<script>
    $.material.init();
</script>
