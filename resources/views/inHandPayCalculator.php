<!DOCTYPE html>
<html>
<head>
     <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <!--Import Google Icon Font-->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">

    <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet"></link>


    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Material Design -->
    <link href="css/bootstrap-material-design.css" rel="stylesheet">
    <link href="css/ripples.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <link href="css/bootstrap-material-datetimepicker.css" rel="stylesheet">
    <link href="css/morris.css" rel="stylesheet">

    <script src="/js/jquery-1.12.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ripples.min.js"></script>
    <script src="js/material.min.js"></script>

    <script src="js/raphael.min.js"></script>
    <script src="js/morris.min.js"></script>

    <script src="js/moment-with-locales.min.js"></script>
    <script src="js/bootstrap-material-datetimepicker.js"></script>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <style>
        
        .noUi-tooltip{
            display: block;
            position: absolute;
            border: 1px solid #D9D9D9;
            border-radius: 3px;
            background: #fff;
            padding: 0 5px ;
            text-align: center;
        }
        .noUi-base {
            height: 1000%;
        }
        .noUi-handle.noUi-handle-lower{
            left:-6px;
        }
        .center-block {
            position: relative;
            padding-top: 20px;
            margin-bottom: -30px;
        }
        .form-group {
            padding-bottom: 7px;
            margin-top: 20px;
        }
        .control-slider::before {
            position: absolute;
            content: attr(data-min);
            left: -2px;
            top: 48px;
            font-size: 10px;
        }
        .control-slider::after {
            position: absolute;
            content: attr(data-max);
            right: -2px;
            top: 48px;
            font-size: 10px;
        }

        .toggle-box {
            border: 1px solid #ddd;
            box-shadow: 0 0 3px #ddd;
            padding: 10px;
            margin: 5px;
            width: 8em;
            height: 7em;
            font-size: 12px;
            text-align: center;
            border-radius: 3px;
            cursor: pointer;
            transition: all 0.1s ease-in;
        }
        .toggle-box:hover {
            box-shadow: 0 0 8px #ddd;
        }
        .report {
            margin-top: 0em;
            border-left: 1px solid #ddd;
        }

        .radio.radio-primary label{
            font-size: 12px;
            padding-top: 3px;
        }

        .main-box:not(.font-small) .form-group.label-floating label.control-label {
            font-size: 15px !important;
        }
    </style>
</head>

<body>
<?php require_once("staticNavBar.php");?>

<header style="padding: 4em;background: #cf3d3d;"></header>

<section>
    <br>
    <br>
    <br>
    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="text-center" style="font-weight: 400">Enter your Information below to estimate your In Hand Pay</h3>
            </div>
            <div class="panel-body">
                <form action="#" class="form-horizontal">
                    <!-- control -->
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <div class="main-box font-small col-md-12 col-sm-12 col-xs-12">
                            <h3>Personal Information</h3>
                            <br>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group label-floating">
                                    <label for="dateOfBirth" class="col-md-3 control-label">D.O.B</label>

                                    <div class="col-md-9">
                                        <input type="text" name="dateOfBirth" id="dateOfBirth" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group label-floating">
                                    <label for="age" class="col-md-3 control-label">Age</label>
                                    <div class="col-md-9">
                                        <div class="center-block">
                                            <div class="slider" id="age"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <br>
                            <br>
                            <br>
                            
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-9 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating is-empty ">
                                        <label class="control-label" for="name"><span>Name</span></label>
                                        <input class="form-control" id="name" name="name" type="text" style="cursor: auto;">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-9 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating is-empty ">
                                        <label class="control-label" for="phone"><span>Phone</span></label>
                                        <input class="form-control" id="phone" name="phone" type="number" min="0" style="cursor: auto;">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-9 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating is-empty ">
                                        <label class="control-label" for="email"><span>Email</span></label>
                                        <input class="form-control" id="email" name="email" type="email" style="cursor: auto;">
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="clearfix"></div>

                        <!--  ==================================  Salary Components =========================================-->


                        <div class="main-box col-md-12 col-sm-12 col-xs-12">
                            <h3 class="pull-left">Salary Components</h3>
                            <div class="clearfix"></div>
                            <span class="small text-center text-muted pull-left">
                                Put all input values on yearly basis
                            </span>
                            <div class="clearfix"></div>
                            <hr>
                            <br>
                            <div class="toggle-box pull-left" data-id="basicComponent">Basic Component</div>
                            <div class="toggle-box pull-left" data-id="allowances">Allowances</div>
                            <div class="pull-right btn btn-raised bg-theme continue">Continue</div>
                            <div class="clearfix"></div>
                            <br>
                            <div id="basicComponent" class="toggle-area">
                                <h4>Basic Component</h4>
                                <br>

                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label for="basic-slider" class="col-md-12 control-label">Basic</label>
                                        <div class="col-md-12">
                                            <div class="center-block">
                                                <div class="slider control-slider" id="basic-slider" data-min="0" data-max="0" data-input="basic-input" data-var="basic"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label for="basic-input" class="control-label col-md-2 col-sm-2"><i class="fa fa-inr"></i></label>
                                        <div class="col-md-10 col-sm-9">
                                            <input class="form-control control-input" id="basic-input" data-slider="basic-slider"  step="1" type="number" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <br>

                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label for="hra-slider" class="col-md-12 control-label">HRA</label>
                                        <div class="col-md-12">
                                            <div class="center-block">
                                                <div class="slider control-slider" id="hra-slider" data-min="0" data-max="0" data-input="hra-input" data-var="hra"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label for="hra-input" class="control-label col-md-2 col-sm-2"><i class="fa fa-inr"></i></label>
                                        <div class="col-md-10 col-sm-9">
                                            <input class="form-control control-input" id="hra-input" data-slider="hra-slider"  step="1" type="number" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <br>

                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label for="rent-slider" class="col-md-12 control-label">Rent if any</label>
                                        <div class="col-md-12">
                                            <div class="center-block">
                                                <div class="slider control-slider" id="rent-slider" data-min="0" data-max="0" data-input="rent-input" data-var="rent"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label for="rent-input" class="control-label col-md-2 col-sm-2"><i class="fa fa-inr"></i></label>
                                        <div class="col-md-10 col-sm-9">
                                            <input class="form-control control-input" id="rent-input" data-slider="rent-slider"  step="1" type="number" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <br>

                                <div class="col-md-7 col-md-offset-1 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <div class="radio radio-primary col-md-6">
                                            <label>
                                                <input type="radio" name="city" id="city1" value="Metro" checked="">
                                                    Metro City
                                            </label>
                                        </div>
                                        <div class="radio radio-primary col-md-6">
                                            <label>
                                                <input type="radio" name="city" id="city2" value="NonMetro">
                                                Non Metro City
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label for="travel-slider" class="col-md-12 control-label">Travel allowance</label>
                                        <div class="col-md-12">
                                            <div class="center-block">
                                                <div class="slider control-slider" id="travel-slider" data-min="0" data-max="0" data-input="travel-input" data-var="travel"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label for="travel-input" class="control-label col-md-2 col-sm-2"><i class="fa fa-inr"></i></label>
                                        <div class="col-md-10 col-sm-9">
                                            <input class="form-control control-input" id="travel-input" data-slider="travel-slider"  step="1" type="number" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div id="allowances" class="toggle-area">
                                <h4>Allowances</h4>
                                <br>

                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label for="uniform-slider" class="col-md-12 control-label">Uniform Allowance</label>
                                        <div class="col-md-12">
                                            <div class="center-block">
                                                <div class="slider control-slider" id="uniform-slider" data-min="0" data-max="0" data-input="uniform-input" data-var="uniform"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label for="uniform-input" class="control-label col-md-2 col-sm-2"><i class="fa fa-inr"></i></label>
                                        <div class="col-md-10 col-sm-9">
                                            <input class="form-control control-input" id="uniform-input" data-slider="uniform-slider"  step="1" type="number" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                <div class="col-md-7 col-md-offset-1 col-sm-8 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label for="uniform-expense-slider" class="col-md-12 control-label">Expense</label>
                                        <div class="col-md-12">
                                            <div class="center-block">
                                                <div class="slider control-slider not-extendable" id="uniform-expense-slider" data-min="0" data-max="0" data-input="uniform-expense-input" data-var="uniform-expense"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label for="uniform-expense-input" class="control-label col-md-2 col-sm-2"><i class="fa fa-inr"></i></label>
                                        <div class="col-md-10 col-sm-9">
                                            <input class="form-control control-input" id="uniform-expense-input" data-slider="uniform-expense-slider"  step="1" type="number" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <br>  

                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label for="medical-slider" class="col-md-12 control-label">Medical Allowance</label>
                                        <div class="col-md-12">
                                            <div class="center-block">
                                                <div class="slider control-slider" id="medical-slider" data-min="0" data-max="0" data-input="medical-input" data-var="medical"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label for="medical-input" class="control-label col-md-2 col-sm-2"><i class="fa fa-inr"></i></label>
                                        <div class="col-md-10 col-sm-9">
                                            <input class="form-control control-input" id="medical-input" data-slider="medical-slider"  step="1" type="number" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                <div class="col-md-7 col-md-offset-1 col-sm-8 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label for="medical-expense-slider" class="col-md-12 control-label">Expense</label>
                                        <div class="col-md-12">
                                            <div class="center-block">
                                                <div class="slider control-slider not-extendable" id="medical-expense-slider" data-min="0" data-max="0" data-input="medical-expense-input" data-var="medical-expense"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label for="medical-expense-input" class="control-label col-md-2 col-sm-2"><i class="fa fa-inr"></i></label>
                                        <div class="col-md-10 col-sm-9">
                                            <input class="form-control control-input" id="medical-expense-input" data-slider="medical-expense-slider"  step="1" type="number" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <br>  

                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label for="driver-slider" class="col-md-12 control-label">Driver Allowance</label>
                                        <div class="col-md-12">
                                            <div class="center-block">
                                                <div class="slider control-slider" id="driver-slider" data-min="0" data-max="0" data-input="driver-input" data-var="driver"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label for="driver-input" class="control-label col-md-2 col-sm-2"><i class="fa fa-inr"></i></label>
                                        <div class="col-md-10 col-sm-9">
                                            <input class="form-control control-input" id="driver-input" data-slider="driver-slider"  step="1" type="number" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                <div class="col-md-7 col-md-offset-1 col-sm-8 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label for="driver-expense-slider" class="col-md-12 control-label">Expense</label>
                                        <div class="col-md-12">
                                            <div class="center-block">
                                                <div class="slider control-slider not-extendable" id="driver-expense-slider" data-min="0" data-max="0" data-input="driver-expense-input" data-var="driver-expense"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label for="driver-expense-input" class="control-label col-md-2 col-sm-2"><i class="fa fa-inr"></i></label>
                                        <div class="col-md-10 col-sm-9">
                                            <input class="form-control control-input" id="driver-expense-input" data-slider="driver-expense-slider"  step="1" type="number" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <br>  

                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label for="any-other-taxable-slider" class="col-md-12 control-label">Any Other Taxable Allowance</label>
                                        <div class="col-md-12">
                                            <div class="center-block">
                                                <div class="slider control-slider" id="any-other-taxable-slider" data-min="0" data-max="0" data-input="any-other-taxable-input" data-var="any-other-taxable"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label for="any-other-taxable-input" class="control-label col-md-2 col-sm-2"><i class="fa fa-inr"></i></label>
                                        <div class="col-md-10 col-sm-9">
                                            <input class="form-control control-input" id="any-other-taxable-input" data-slider="any-other-taxable-slider"  step="1" type="number" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>                          
                            </div>
                        </div>
                        
                        <div class="clearfix"></div>

                        <!-- ===================================  Deduction ==================================== -->


                        <div class="main-box col-md-12 col-sm-12 col-xs-12">
                            <h3 class="pull-left">Deduction</h3>
                            <div class="clearfix"></div>
                            <span class="small text-center text-muted pull-left">
                                Put all input values on yearly basis
                            </span>
                            <div class="clearfix"></div>
                            <hr>
                            <br>
                            <div class="toggle-box pull-left" data-id="investement">Investement</div>
                            <div class="toggle-box pull-left" data-id="medical">Medical</div>
                            <div class="toggle-box pull-left" data-id="disability">Disability</div>
                            <div class="toggle-box pull-left" data-id="others">Others</div>
                            <div class="pull-right btn btn-raised bg-theme continue">Continue</div>
                            <div class="clearfix"></div>
                            <br>
                            <div id="investement" class="toggle-area">
                                <h4>Investement </h4>
                                <br>

                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label for="80CCD1A-slider" class="col-md-12 control-label">Investment under section 80-C, 80-CCC, 80-CCD(1A) </label>
                                        <div class="col-md-12">
                                            <div class="center-block">
                                                <div class="slider control-slider" id="80CCD1A-slider" data-min="0" data-max="0" data-input="80CCD1A-input" data-var="80CCD1A"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label for="80CCD1A-input" class="control-label col-md-2 col-sm-2"><i class="fa fa-inr"></i></label>
                                        <div class="col-md-10 col-sm-9">
                                            <input class="form-control control-input" id="80CCD1A-input" data-slider="80CCD1A-slider"  step="1" type="number" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <br>

                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label for="80CCD1B-slider" class="col-md-12 control-label">Investment under section 80-CCD(1B)</label>
                                        <div class="col-md-12">
                                            <div class="center-block">
                                                <div class="slider control-slider" id="80CCD1B-slider" data-min="0" data-max="0" data-input="80CCD1B-input" data-var="80CCD1B"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label for="80CCD1B-input" class="control-label col-md-2 col-sm-2"><i class="fa fa-inr"></i></label>
                                        <div class="col-md-10 col-sm-9">
                                            <input class="form-control control-input" id="80CCD1B-input" data-slider="80CCD1B-slider"  step="1" type="number" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <br>

                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label for="80GGC-slider" class="col-md-12 control-label">Investment under section 80-GGC</label>
                                        <div class="col-md-12">
                                            <div class="center-block">
                                                <div class="slider control-slider" id="80GGC-slider" data-min="0" data-max="0" data-input="80GGC-input" data-var="80GGCC"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label for="80GGC-input" class="control-label col-md-2 col-sm-2"><i class="fa fa-inr"></i></label>
                                        <div class="col-md-10 col-sm-9">
                                            <input class="form-control control-input" id="80GGC-input" data-slider="80GGC-slider"  step="1" type="number" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <br>

                            </div>
                            
                            <div id="medical" class="toggle-area">
                                <h4>Medical Deduction</h4>
                                <br>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="togglebutton form-group">
                                            <label>
                                                include Parents ?
                                                &nbsp;&nbsp;&nbsp; 
                                                <input type="checkbox" id="includeParent">
                                            </label>
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                Senior Citizens ?
                                                &nbsp;&nbsp;&nbsp; 
                                                <input type="checkbox" id="seniorCitizen">
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label for="80D-slider" class="col-md-12 control-label">Deduction under section 80-D </label>
                                        <div class="col-md-12">
                                            <div class="center-block">
                                                <div class="slider control-slider not-extendable" id="80D-slider" data-min="0" data-max="0" data-input="80D-input" data-var="80D"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label for="80D-input" class="control-label col-md-2 col-sm-2"><i class="fa fa-inr"></i></label>
                                        <div class="col-md-10 col-sm-9">
                                            <input class="form-control control-input" id="80D-input" data-slider="80D-slider"  step="1" type="number" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <br>
                                <h4>Treatment for Disability</h4>
                                <br>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="togglebutton form-group">
                                            <label>
                                                Whether any Senior Member ?
                                                &nbsp;&nbsp;&nbsp; 
                                                <input type="checkbox" id="anySeniorMember">
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label for="80DDB-slider" class="col-md-12 control-label">Deduction under section 80-DDB</label>
                                        <div class="col-md-12">
                                            <div class="center-block">
                                                <div class="slider control-slider not-extendable" id="80DDB-slider" data-min="0" data-max="0" data-input="80DDB-input" data-var="80DDB"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label for="80DDB-input" class="control-label col-md-2 col-sm-2"><i class="fa fa-inr"></i></label>
                                        <div class="col-md-10 col-sm-9">
                                            <input class="form-control control-input" id="80DDB-input" data-slider="80DDB-slider"  step="1" type="number" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <br>
                            </div>

                            <div id="disability" class="toggle-area">
                                <h4>Disability</h4>
                                <br>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="togglebutton form-group">
                                            <label>
                                                Have Certificate ?
                                                &nbsp;&nbsp;&nbsp; 
                                                <input type="checkbox" id="haveCertificate">
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="togglebutton form-group">
                                            <label>
                                                Severe ?
                                                &nbsp;&nbsp;&nbsp; 
                                                <input type="checkbox" id="severeDamage">
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <br>
                            </div>

                            <div id="others" class="toggle-area">
                                <h4>Others Deduction</h4>
                                <br>

                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label for="education-slider" class="col-md-12 control-label">Education Loan</label>
                                        <div class="col-md-12">
                                            <div class="center-block">
                                                <div class="slider control-slider" id="education-slider" data-min="0" data-max="0" data-input="education-input" data-var="education"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label for="education-input" class="control-label col-md-2 col-sm-2"><i class="fa fa-inr"></i></label>
                                        <div class="col-md-10 col-sm-9">
                                            <input class="form-control control-input" id="education-input" data-slider="education-slider"  step="1" type="number" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <br>

                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label for="home-slider" class="col-md-12 control-label">Interest On Home Loan</label>
                                        <div class="col-md-12">
                                            <div class="center-block">
                                                <div class="slider control-slider" id="home-slider" data-min="0" data-max="0" data-input="home-input" data-var="home"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label for="home-input" class="control-label col-md-2 col-sm-2"><i class="fa fa-inr"></i></label>
                                        <div class="col-md-10 col-sm-9">
                                            <input class="form-control control-input" id="home-input" data-slider="home-slider"  step="1" type="number" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>


                                <div class="clearfix"></div>
                                <br>
                            </div>

                        </div>


                    </div>
                    <!-- report -->
                    <div class="col-md-4 col-sm-4 col-xs-12 report">
                        
                        <div class="col-md-12 col-sm-12 col-xs-12 meter">
                            <div id="donut-chart"></div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <hr>
                            <h6 class="small text-muted">These values are on yearly basis</h6>
                            <h5>Total CTC &nbsp; <span class="pull-right"><i class="fa fa-inr"></i> &nbsp; &nbsp; <span id="totalCTC">0</span></span></h5>
                            <h5>Deduction Under Chapter VI A &nbsp; <span class="pull-right"><i class="fa fa-inr"></i> &nbsp; &nbsp; <span id="deductionUnderChapterVIA">0</span></span></h5>
                            <hr>
                            <h5 class="small text-muted">These values are on monthly basis</h5>
                            <h5>Total Monthly Pay &nbsp; <span class="pull-right"><i class="fa fa-inr"></i> &nbsp; &nbsp; <span id="totalMonthlyPay">0</span></span></h5>
                            <h5>Exempt allowances &nbsp; <span class="pull-right"><i class="fa fa-inr"></i> &nbsp; &nbsp; <span id="exemptAllowances">0</span></span></h5>
                            <h5>Taxable Salary &nbsp; <span class="pull-right"><i class="fa fa-inr"></i> &nbsp; &nbsp; <span id="taxableSalary">0</span></span></h5>
                            <h5>Deductions (ESI and EPF) &nbsp; <span class="pull-right"><i class="fa fa-inr"></i> &nbsp; &nbsp; <span id="deductionESI_EPF">0</span></span></h5>
                            <h5>Deduction Of Taxes &nbsp; <span class="pull-right"><i class="fa fa-inr"></i> &nbsp; &nbsp; <span id="deductionTax">0</span></span></h5>
                            <hr>
                            <h5>Take Home Pay <span class="small">(Monthly)</span>&nbsp; <span class="pull-right"><i class="fa fa-inr"></i> &nbsp; &nbsp; <span id="takeHomePay">0</span></span></h5>
                        </div>

                    </div>

                    <div class="clearfix"></div>
                    <br><br>
                </form>
                <br>
            </div>
        </div>
    </div>
    <br>
    <br>
    <br>
</section>

<?php require_once('staticFooter.php');?>
</body>
</html>
<link href="css/nouislider.pips.css" rel="stylesheet">
<script src="js/nouislider.min.js"></script>

<script>
    $(function() {

        $.material.init();

        // initialize age
        var dateOfBirth = new Date(2000, 0 ,1).getFullYear();
        var currentYear = new Date().getFullYear();
        var start = 0;
        if (dateOfBirth < currentYear)
            start = (Math.abs(currentYear - dateOfBirth));

        // slider configuration 
        noUiSlider.create($('#age')[0], {
            start: start,
            connect: 'lower',
            step : 1,
            range: {
                'min': 0,
                'max': 100
            },
            pips: {
                mode: 'count',
                values : 5,
                density: 2
            },
            format: {
                to: function ( value ) {
                    return value;
                },
                from: function ( value ) {
                    return value.replace(',-', '');
                }
            }
        });

        $('#dateOfBirth').bootstrapMaterialDatePicker({ weekStart : 0, time: false , maxDate : new Date()})
        .on('dateSelected', function(event) {
            $('#'+$(this).data('dtp')).find('.dtp-btn-ok').trigger('click');
        })
        .on('change', function(e, date) {
            var dateOfBirth = date.get('year');
            var currentYear = new Date().getFullYear();
            if (dateOfBirth < currentYear)
                $("#age")[0].noUiSlider.set(Math.abs(currentYear - dateOfBirth));
        });

        $('#dateOfBirth').bootstrapMaterialDatePicker('setDate' , new Date(2000, 0, 1));
        $("#dateOfBirth").parents(".is-empty").removeClass('is-empty');

        $(".toggle-area").hide();
        $(".continue").hide();

        $(".toggle-box").click(function(event) {
            $(".main-box").hide();
            $(".toggle-area").hide();
            $("#"+$(this).data('id')).show();
            $(this).siblings(".continue").show();
            $(this).parents(".main-box").show();
        });

        $(".continue").click(function(event) {
            $(".toggle-area").hide();
            $(".main-box").show();
            $(this).hide();
        });

        $(".control-slider").each(function(index, el) {
            var $slider = $(this);
            $(this).attr({
                'data-min': 0,
                'data-max': 1000000
            });

            noUiSlider.create(el, {
                connect : 'lower',
                start : 0,
                step : 1,
                range : {
                    min : 0,
                    max : 1000000
                },
                format: {
                    to: function ( value ) {
                        return value;
                    },
                    from: function ( value ) {
                        return value.replace(',-', '');
                    }
                }
            }).on('update', function( values, handle ) {

                var value = values[handle];
                $("#"+$slider.data('input')).val(Math.round(value));

            });

            $slider[0].noUiSlider.on('change', function(value, handle) {
                if (!$slider.hasClass('not-extendable') && value == $slider.attr('data-max')) {
                    $slider[0].noUiSlider.updateOptions({
                        range: {
                            min: 0,
                            max: value*10
                        },
                        step : 1
                    });
                    $slider.attr('data-max', value*10);
                }
            });
        }); 

        $(".control-input").on('change', function(event) {

            var slider = document.getElementById($(this).data('slider'));
            var value = $(this).val();

            if (!$(slider).hasClass('not-extendable') && value > 1*$(slider).attr('data-max')) {
                slider.noUiSlider.updateOptions({
                    range: {
                        min: 0,
                        max: Math.pow(10, (""+value).length)
                    },
                    step : 1
                });
                $(slider).attr('data-max', Math.pow(10, (""+value).length));
            }
            slider.noUiSlider.set(value);
        });

        (function() {
            // visible variables

            input_val = {
                'age' : 0,
                'basic' : 0,
                'hra' : 0,
                'city' : .50,  // in default Metro city is selected
                'rent' : 0,
                'uniform' : 0,
                'uniform-expense' : 0,
                'medical' : 0,
                'medical-expense' : 0,
                'driver' : 0,
                'driver-expense' : 0,
                'any-other-taxable' : 0,
                '80CCD1A': 0,
                '80CCD1B': 0,
                '80GGC': 0,
                '80D': 0,
                '80DDB': 0,
                'education': 0,
                'travel' : 0,
                'home': 0,

                'includeParent' : false,
                'seniorCitizen' : false,
                'anySeniorMember' : false,
                'haveCertificate' : false,
                'severeDamage' : false
            }

            // visible input elements
            // bind element

            input_el = {
                'dateOfBirth' : $("#dateOfBirth")[0],
                'age' : $("#age")[0],
                'basic' : $("#basic-slider")[0],
                'city' : $("#city1, #city2"),
                'hra' : $("#hra-slider")[0],
                'rent' : $("#rent-slider")[0],
                'uniform' : $("#uniform-slider")[0],
                'uniform-expense' : $("#uniform-expense-slider")[0],
                'medical' : $("#medical-slider")[0],
                'medical-expense' : $("#medical-expense-slider")[0],
                'driver' : $("#driver-slider")[0],
                'driver-expense' : $("#driver-expense-slider")[0],
                'any-other-taxable' : $("#any-other-taxable-slider")[0],
                '80CCD1A' : $("#80CCD1A-slider")[0],
                '80CCD1B' : $("#80CCD1B-slider")[0],
                '80GGC' : $("#80GGC-slider")[0],
                '80D' : $("#80D-slider")[0],
                '80DDB' : $("#80DDB-slider")[0],
                'education' : $("#education-slider")[0],
                'travel' : $("#travel-slider")[0],
                'home' : $("#home-slider")[0],

                'includeParent' : $("#includeParent"),
                'seniorCitizen' : $("#seniorCitizen"),
                'anySeniorMember' : $("#anySeniorMember"),
                'haveCertificate' : $("#haveCertificate"),
                'severeDamage' : $("#severeDamage")
            }

            var output_el = {
                'totalCTC' : $("#totalCTC"),
                'totalMonthlyPay' : $("#totalMonthlyPay"),
                'exemptAllowances' : $("#exemptAllowances"),
                'taxableSalary' : $("#taxableSalary"),
                'deductionESI_EPF' : $("#deductionESI_EPF"),
                'deductionUnderChapterVIA' : $("#deductionUnderChapterVIA"),
                'deductionTax' : $("#deductionTax"),
                'takeHomePay' : $("#takeHomePay"),

                'donutChart' : $("#donut-chart")
            }


            totalCTC = 0;
            totalMonthlyPay = 0;
            exemptAllowances = 0;
            taxableSalary = 0;
            deductionESI_EPF = 0;
            deductionTax = 0;
            deductionUnderChapterVIA = 0;

            takeHomePay = 0;

            var disabilityLimit = 0;

            // ==============================  defined constants  ========================

            var maxConveyance = 19200;
            var maxTravelAllowance = 1600*12;
            var maxMedicalAllowance = 1250*12;
            var MetroCity = 0.50;
            var NormalCity = 0.40;
            var max80CCD1A = 150000;
            var max80CCD1B = 50000;

            // =============================== logics ==============================

            var updateTotalCTC = function() {
                totalCTC = 
                    input_val['basic'] + 
                    input_val['hra'] + 
                    input_val['travel']+ 
                    input_val['uniform'] +
                    input_val['medical'] +
                    input_val['driver'] + 
                    input_val['any-other-taxable'];

                output_el['totalCTC'].text(totalCTC);
                totalMonthlyPay = Math.round(totalCTC / 12);
                output_el['totalMonthlyPay'].text(totalMonthlyPay);

                updateTaxableSalary();
            };

            var updateExemptAllowances = function() {
                exemptAllowances = 0;

                var exemptHRA = Math.min(
                    input_val['hra'] ,
                    Math.max(0, input_val['rent'] - 0.1 * input_val['basic']),
                    input_val['city']*input_val['basic']
                );

                var exemptTravel = Math.min(input_val['travel'], maxTravelAllowance);
                var exemptUniform = input_val['uniform-expense'];
                var exemptMedical = Math.min(input_val['medical-expense'], maxMedicalAllowance);
                var exemptDriver = input_val['driver-expense'];

                exemptAllowances = 
                    exemptHRA +
                    exemptTravel +
                    exemptUniform + 
                    exemptMedical + 
                    exemptDriver;

                output_el['exemptAllowances'].text(Math.round(exemptAllowances / 12));

                updateTaxableSalary();
            };

            var updateDeductionUnderChapterVIA = function() {
                deductionUnderChapterVIA = 0;

                deductionUnderChapterVIA = 
                    Math.min(input_val['80CCD1A'], max80CCD1A) +
                    Math.min(input_val['80CCD1B'], max80CCD1B) +
                    input_val['80GGC'] + 
                    input_val['80D'] + 
                    input_val['80DDB'] +
                    disabilityLimit +
                    input_val['education'] +
                    input_val['home'];

                deductionUnderChapterVIA = Math.round( deductionUnderChapterVIA);

                output_el['deductionUnderChapterVIA'].text(deductionUnderChapterVIA);
                updateTaxableSalary();
            }

            var updateTaxableSalary = function() {
                taxableSalary = Math.max(totalCTC - exemptAllowances - deductionUnderChapterVIA, 0);

                output_el['taxableSalary'].text(Math.round(taxableSalary / 12));
                updateDeductionTax();
            }

            var updateDeductionESI_EPF = function() {
                deductionESI_EPF = 0;

                var basic = input_val['basic'];

                if (input_val['basic'] <= 180000) {
                    deductionESI_EPF = 0.12 * basic + 0.0175 * basic;
                }
                else
                    deductionESI_EPF = 0;

                output_el['deductionESI_EPF'].text(Math.round(deductionESI_EPF / 12));
                updateTakeHomePay();
            }

            var updateTakeHomePay = function() {
                takeHomePay = Math.round((totalCTC - deductionESI_EPF - deductionTax) / 12);

                output_el['takeHomePay'].text(takeHomePay);

                output_el['donutChart'].children().remove();

                if (takeHomePay == 0 && deductionTax == 0 && deductionESI_EPF == 0) {
                    output_el['donutChart'].html("<h5>Your Total CTC is 0</h5>");
                }
                else {
                    Morris.Donut({
                        element: 'donut-chart',
                        colors : [
                            "#0B62A4",
                            "#ff0000",
                            "#cf3d3d",
                        ],
                        data: [
                        {
                            label: "Take Home Pay",
                            value: takeHomePay
                        }, 
                        {
                            label: "Deduction Of Taxes",
                            value: Math.round(deductionTax / 12)
                        }, 
                        {
                            label: "Deduction (ESI & EPF)",
                            value: Math.round(deductionESI_EPF /12)
                        }],
                        resize: true
                    });
                }

            }

            var updateMedicalLimit = function() {
                var limit80D = 0;
                var limit80DDB = 0;

                if (input_val['age'] > 60)
                    limit80D = 30000;
                else
                    limit80D = 25000;

                if (input_val['includeParent']) {
                    if (input_val['seniorCitizen']) 
                        limit80D += 30000;
                    else
                        limit80D += 25000;
                }

                if (input_val['anySeniorMember'])
                    limit80DDB = 60000;
                else
                    limit80DDB = 40000

                input_el['80D'].noUiSlider.updateOptions({
                    range: {
                        min: 0,
                        max: limit80D
                    },
                    step : 1
                });

                input_el['80DDB'].noUiSlider.updateOptions({
                    range: {
                        min: 0,
                        max: limit80DDB
                    },
                    step : 1
                });

                $(input_el['80D']).attr({
                    'data-max': limit80D
                });
                $(input_el['80DDB']).attr({
                    'data-max': limit80DDB
                });
            }

            var updateDisability = function() {
                disabilityLimit = 0;
                if (input_val['haveCertificate']) {
                    if (input_val['severeDamage'])
                        disabilityLimit = 150000;
                    else
                        disabilityLimit = 75000;
                }

                updateDeductionUnderChapterVIA();
            }

            var updateDeductionTax = function() {
                var taxable = taxableSalary;
                var age = input_val['age'];

                if (age < 60) {
                    if (taxableSalary <= 250000) {
                        deductionTax = 0;
                    }
                    else if(taxableSalary > 250000 && taxableSalary <= 500000) {
                        taxable -= 250000;
                        deductionTax = 0.1*taxable;
                    }
                    else if (taxableSalary > 500000 && taxableSalary <= 1000000) {
                        taxable -= 500000;
                        deductionTax = 0.2 * taxable + 25000; // 25000 == 0.1*250000
                    }
                    else {
                        taxable -= 1000000;
                        deductionTax = 0.3*taxable + 125000; // 125000 == 0.1 + 250000 + 0.2 * 500000
                    }
                }
                else if (age < 80) {
                    if (taxableSalary <= 300000) {
                        deductionTax = 0;
                    }
                    else if(taxableSalary > 300000 && taxableSalary <= 500000) {
                        taxable -= 300000;
                        deductionTax = 0.1*taxable;
                    }
                    else if (taxableSalary > 500000 && taxableSalary <= 1000000) {
                        taxable -= 500000;
                        deductionTax = 0.2 * taxable + 25000; // 25000 == 0.1*250000
                    }
                    else {
                        taxable -= 1000000;
                        deductionTax = 0.3*taxable + 125000; // 125000 == 0.1 + 250000 + 0.2 * 500000
                    }
                }
                else {
                    if (taxableSalary <= 500000) {
                        deductionTax = 0;
                    }
                    else if (taxableSalary > 500000 && taxableSalary <= 1000000) {
                        taxable -= 500000;
                        deductionTax = 0.2 * taxable + 25000; // 25000 == 0.1*250000
                    }
                    else {
                        taxable -= 1000000;
                        deductionTax = 0.3*taxable + 125000; // 125000 == 0.1 + 250000 + 0.2 * 500000
                    }
                }

                // Rebate
                if (taxableSalary <= 500000) {
                    deductionTax -= Math.max(0, Math.min(5000, taxableSalary - 250000));
                }
                deductionTax = Math.max(deductionTax, 0);

                // Surcharge
                if (taxableSalary > 10000000) {
                    var margin = taxableSalary - 10000000;  // for margin relief

                    var surcharge = 0.1 * deductionTax;
                    deductionTax += Math.min(margin, surcharge);
                }

                // Education Cess
                deductionTax += 0.03*deductionTax;


                output_el['deductionTax'].text(Math.round(deductionTax / 12));
                
                updateTakeHomePay();
            }

            //  binding elements with there values

            //================================= handler ================================
            input_el['age'].noUiSlider.on('update', function( values, handle ) {

                var value = values[handle];
                input_val['age'] = value;

                updateMedicalLimit();
            });

            input_el['basic'].noUiSlider.on('update', function( values, handle ) {

                var value = values[handle];
                input_val['basic'] = value;

                updateTotalCTC();
                updateExemptAllowances();
                updateDeductionESI_EPF();
            });

            input_el['city'].on('change', function(event) {

                if ($(this).val() == "Metro") 
                    input_val['city'] = MetroCity;
                else
                    input_val['city'] = NormalCity;

                updateExemptAllowances();
            });

            input_el['hra'].noUiSlider.on('update', function( values, handle ) {

                var value = values[handle];
                input_val['hra'] = value;

                if (value != 0) {
                    if (input_el['rent'].getAttribute('disabled')) {
                        input_el['rent'].removeAttribute('disabled');
                    }
                }
                if (value == 0) {
                    input_el['rent'].noUiSlider.set(0);
                    input_el['rent'].setAttribute('disabled', true);
                }

                updateTotalCTC();
                updateExemptAllowances();
            });

            input_el['rent'].noUiSlider.on('update', function( values, handle ) {

                var value = values[handle];
                input_val['rent'] = value;

                updateExemptAllowances();
            });

            input_el['travel'].noUiSlider.on('update', function( values, handle ) {

                var value = values[handle];
                input_val['travel'] = value;

                updateTotalCTC();
                updateExemptAllowances();
            });

            input_el['uniform'].noUiSlider.on('update', function( values, handle ) {

                var value = values[handle];
                input_val['uniform'] = value;

                if (value != 0) {
                    if (input_el['uniform-expense'].getAttribute('disabled')) {
                        input_el['uniform-expense'].removeAttribute('disabled');
                    }
                    input_el['uniform-expense'].noUiSlider.updateOptions({
                        range: {
                            min: 0,
                            max: value
                        },
                        step : 1,
                    });
                    input_el['uniform-expense'].noUiSlider.set(value);
                }
                if (value == 0) {
                    input_el['uniform-expense'].noUiSlider.set(0);
                    input_el['uniform-expense'].setAttribute('disabled', true);
                }

                $(input_el['uniform-expense']).attr({
                    'data-max': value
                });

                updateTotalCTC();
            });

            input_el['uniform-expense'].noUiSlider.on('update', function( values, handle ) {

                var value = values[handle];
                input_val['uniform-expense'] = value;

                updateExemptAllowances();
            });

            input_el['medical'].noUiSlider.on('update', function( values, handle ) {

                var value = values[handle];
                input_val['medical'] = value;

                if (value != 0) {
                    if (input_el['medical-expense'].getAttribute('disabled')) {
                        input_el['medical-expense'].removeAttribute('disabled');
                    }
                    input_el['medical-expense'].noUiSlider.updateOptions({
                        range: {
                            min: 0,
                            max: value
                        },
                        step : 1
                    });
                    input_el['medical-expense'].noUiSlider.set(value);
                }
                if (value == 0) {
                    input_el['medical-expense'].noUiSlider.set(0);
                    input_el['medical-expense'].setAttribute('disabled', true);
                }

                $(input_el['medical-expense']).attr({
                    'data-max': value
                });

                updateTotalCTC();
            });

            input_el['medical-expense'].noUiSlider.on('update', function( values, handle ) {

                var value = values[handle];
                input_val['medical-expense'] = value;

                updateExemptAllowances();
            });

            input_el['driver'].noUiSlider.on('update', function( values, handle ) {

                var value = values[handle];
                input_val['driver'] = value;

                if (value != 0) {
                    if (input_el['driver-expense'].getAttribute('disabled')) {
                        input_el['driver-expense'].removeAttribute('disabled');
                    }
                    input_el['driver-expense'].noUiSlider.updateOptions({
                        range: {
                            min: 0,
                            max: value
                        },
                        step : 0
                    });
                    input_el['driver-expense'].noUiSlider.set(value);
                }
                if (value == 0) {
                    input_el['driver-expense'].noUiSlider.set(0);
                    input_el['driver-expense'].setAttribute('disabled', true);
                }

                $(input_el['driver-expense']).attr({
                    'data-max': value
                });

                updateTotalCTC();
            });

            input_el['driver-expense'].noUiSlider.on('update', function( values, handle ) {

                var value = values[handle];
                input_val['driver-expense'] = value;

                updateExemptAllowances();
            });

            input_el['any-other-taxable'].noUiSlider.on('update', function( values, handle ) {

                var value = values[handle];
                input_val['any-other-taxable'] = value;

                updateTotalCTC();
            });

            input_el['80CCD1A'].noUiSlider.on('update', function( values, handle ) {

                var value = values[handle];
                input_val['80CCD1A'] = value;

                updateDeductionUnderChapterVIA();
            });

            input_el['80CCD1B'].noUiSlider.on('update', function( values, handle ) {

                var value = values[handle];
                input_val['80CCD1B'] = value;

                updateDeductionUnderChapterVIA();
            });

            input_el['80GGC'].noUiSlider.on('update', function( values, handle ) {

                var value = values[handle];
                input_val['80GGC'] = value;

                updateDeductionUnderChapterVIA();
            });

            input_el['80D'].noUiSlider.on('update', function( values, handle ) {

                var value = values[handle];
                input_val['80D'] = value;

                updateDeductionUnderChapterVIA();
            });

            input_el['80DDB'].noUiSlider.on('update', function( values, handle ) {

                var value = values[handle];
                input_val['80DDB'] = value;

                updateDeductionUnderChapterVIA();
            });

            input_el['education'].noUiSlider.on('update', function( values, handle ) {

                var value = values[handle];
                input_val['education'] = value;

                updateDeductionUnderChapterVIA();
            });

            input_el['home'].noUiSlider.on('update', function( values, handle ) {

                var value = values[handle];
                input_val['home'] = value;

                updateDeductionUnderChapterVIA();
            });

            input_el['includeParent'].on('change', function(event) {
                if ($(this).is(':checked')) {
                    input_val['includeParent'] = true;
                    input_el['seniorCitizen'].prop({
                        disabled: false,
                        checked: false,
                    }).trigger('change');
                }
                else{
                    input_val['includeParent'] = false;
                    input_el['seniorCitizen'].prop({
                        disabled: true,
                        checked: false,
                    }).trigger('change');
                }

                updateMedicalLimit();
                updateDeductionUnderChapterVIA();
            });

            input_el['seniorCitizen'].on('change', function(event) {
                if ($(this).is(':checked'))
                    input_val['seniorCitizen'] = true;
                else
                    input_val['seniorCitizen'] = false;

                updateMedicalLimit();
                updateDeductionUnderChapterVIA();
            });

            input_el['anySeniorMember'].on('change', function(event) {
                if ($(this).is(':checked')) {
                    input_val['anySeniorMember'] = true;
                }
                else
                    input_val['anySeniorMember'] = false;

                updateMedicalLimit();
                updateDeductionUnderChapterVIA();
            });

            input_el['haveCertificate'].on('change', function(event) {
                if ($(this).is(':checked')) {
                    input_val['haveCertificate'] = true;
                    input_el['severeDamage'].prop({
                        disabled: false,
                        checked: false,
                    }).trigger('change');
                }
                else{
                    input_val['haveCertificate'] = false;
                    input_el['severeDamage'].prop({
                        disabled: true,
                        checked: false,
                    }).trigger('change');
                }
            });

            input_el['severeDamage'].on('change', function(event) {
                if ($(this).is(':checked')) {
                    input_val['severeDamage'] = true;
                }
                else
                    input_val['severeDamage'] = false;

                updateDisability();
            });
        })();


        // ==========   disable some checkbox  ============
        $("#severeDamage, #seniorCitizen").prop('checked', false);

    });
</script>
