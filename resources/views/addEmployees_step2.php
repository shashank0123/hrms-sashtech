<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>

<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">

                <div class="breadcrumb flat text-center row">
                    <?php if(session()->get('plan') == 'payroll'): ?>
                        <span class="col-xs-6 col-sm-3 col-md-3 hidden-xs">The Basics</span>
                        <span class="col-xs-12 col-sm-3 col-md-3 active">Personal Details</span>
                        <span class="col-xs-6 col-sm-3 col-md-3 hidden-xs">Tax Details</span>
                        <span class="col-xs-6 col-sm-3 col-md-3 hidden-xs">Payment Details</span>
                    <?php elseif((session()->get('plan') == 'benefits')or(session()->get('plan') == 'manager')): ?>
                        <span class="col-xs-6 col-sm-2 col-md-2 hidden-xs">Basics</span>
                        <span class="col-xs-12 col-sm-3 col-md-3 active">Personal Details</span>
                        <span class="col-xs-6 col-sm-2 col-md-2 hidden-xs">Family</span>
                        <span class="col-xs-6 col-sm-2 col-md-2 hidden-xs">Tax</span>
                        <span class="col-xs-6 col-sm-3 col-md-3 hidden-xs">Payment</span>
                    <?php endif; ?>
                </div>
                    

                <br>
                <br>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <form class="form-horizontal">
                        <fieldset>
                            <legend>Enter your Personal Details</legend>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="form-group label-floating is-empty ">
                                        <label class="control-label" for="pan"><span>PAN</span></label>
                                        <input class="form-control" id="pan" name="pan" type="text" style="cursor: auto;">
                                    </div>
                                </div>
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p>Permanent Account Number (PAN) is a ten-digit alphanumeric number, issued in by the Income Tax Department</p>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="form-group label-floating">
                                        <label for="dateOfBirth" class="control-label">D.O.B</label>
                                        <input type="text" name="dateOfBirth" id="dateOfBirth" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Gender</label>

                                    <div class="col-md-10">
                                        <div class="radio radio-primary">
                                            <label>
                                                <input type="radio" name="gender" id="gender1" value="Male" checked="">
                                                    Male
                                            </label>
                                        </div>
                                        <div class="radio radio-primary">
                                            <label>
                                                <input type="radio" name="gender" id="gender2" value="Female">
                                                Female
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="form-group label-floating">
                                        <label for="permanentAddress" class="control-label">Address permanent </label>
                                        <textarea class="form-control" name="permanentAddress" rows="2" id="permanentAddress"></textarea>
                                    </div>                                
                                </div>
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p>Employer address other than the one in the first page. It can be anywhere in the India</p>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating is-empty">
                                        <label for="employeOthereMail" class="control-label">Alternative  Email</label>
                                        <input type="email" class="form-control" name="employeOthereMail" id="ContractorOthereMail">
                                    </div>
                                </div>   
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating is-empty ">
                                        <label class="control-label" for="phone"><span>Phone</span></label>
                                        <input class="form-control" id="phone" name="phone" type="number" style="cursor: auto;">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="col-md-8 col-sm-8 col-xs-8 col-md-offset-4 col-sm-offset-3 col-xs-offset-4">
                                        <span class="btn btn-default btn-raised cancel" onclick="history.back();">Cancel</span>
                                        <input type="submit" class="next btn bg-theme next btn-raised" value="next">
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
                    </div>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>

    function displayError() {
        swal({
            title : "Error !",
            text : 'Try Again!',
            type : 'error',
            confirmButtonClass : 'bg-theme',
            animation : false,
            width : 400
        });
    };

    function closeAlert() {
        $(".sweet-alert button").first().trigger('click');
    }
    
    $(function() {

        $.material.init();

        $('#dateOfBirth').bootstrapMaterialDatePicker({ weekStart : 0, time: false }).on('dateSelected', function(event) {
            $('#'+$(this).data('dtp')).find('.dtp-btn-ok').trigger('click');
        });

        $("#dateOfBirth").bootstrapMaterialDatePicker('setDate', moment(new Date(1980, 0,1)));

        $("form.form-horizontal").on('submit', function(event) {

            event.preventDefault();

            // remove all error messages
            $(".help-block").remove();
            $(".has-error").removeClass('has-error');
            
            swal({
                title : 'Saving',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 400
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: $('.main form').serialize(), 
            })
            .done(function(data) {
                // console.log(data
                if (data.status == 200) {
                    swal({
                        title : "Success !",
                        // text : data.message,
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        showConfirmButton: false,
                        animation : false,
                        width : 300,
                        timer : 1500
                    });   
                    getAndInsert(data.url);
                    history.pushState(null, null, data.url);
                }
                else if(data.status == 208) {  //  email already Exist
                    closeAlert();
                    var $email = $("#employeOthereMail");
                    $email.parent().append('<p class="help-block">'+ data.message +'</p>');
                    $email.focus();
                    $email.parents('.form-group').addClass('has-error');
                }
                else{  // Validation Error
                    displayError();
                    // show Errors
                    for(var fields in data) {
                        $('#'+fields).parents('.form-group').addClass('has-error');
                        $('#'+fields).parent().append('<p class="help-block">'+ data[fields][0] +'</p>');
                    }
                }
            })
            .error(function() {
                displayError();
            });
        });

        $(".main .form-horizontal input").trigger('change');
    })
</script>
