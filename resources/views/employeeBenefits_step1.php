<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>


<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9" style="overflow-x:hidden;position:relative;">
                    <div class="panel-body" style="min-height:36em;">

                            
                            <form action="#" class="form-horizontal">
                                
                                <br>
                                <h3 class="text-center">Tell us about the members you'd like to insure</h3>
                                <br><br>

                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="img-circle" ></div>
                                    <h6 class="text-center">Number of Adults</h6>
                                    <div class="center-block" style="width:220px;">
                                        <div class="btn-group" data-toggle="buttons">
                                          <label class="btn btn-primary">
                                            <input type="radio" name="adult" value="1" id="1" autocomplete="off"> 1
                                          </label>
                                          <label class="btn btn-primary">
                                            <input type="radio" name="adult" value="2" id="2" autocomplete="off"> 2
                                          </label>
                                          <label class="btn btn-primary">
                                            <input type="radio" name="adult" value="3" id="3" autocomplete="off"> 3
                                          </label>
                                          <label class="btn btn-primary">
                                            <input type="radio" name="adult" value="4" id="4" autocomplete="off"> 4
                                          </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="img-circle" style="background-position:-122px 0;"></div>
                                    <h6 class="text-center">Number of children (Below 21 years)</h6>
                                    <div class="center-block" style="width:275px;">
                                        <div class="btn-group" data-toggle="buttons">
                                          <label class="btn btn-primary">
                                            <input type="radio" name="children" value="0" id="0" autocomplete="off"> 0
                                          </label>
                                          <label class="btn btn-primary">
                                            <input type="radio" name="children" id="1" value="1" autocomplete="off"> 1
                                          </label>
                                          <label class="btn btn-primary">
                                            <input type="radio" name="children" id="2" value="2" autocomplete="off"> 2
                                          </label>
                                          <label class="btn btn-primary">
                                            <input type="radio" name="children" id="3" value="3" autocomplete="off"> 3
                                          </label>
                                          <label class="btn btn-primary">
                                            <input type="radio" name="children" id="4" value="4" autocomplete="off"> 4
                                          </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <br><br>

                                <br>
                                <h3 class="text-center">How old is the eldest member to be insured?</h3>
                                <br><br>
                                
                                <div class="col-md-4 col-sm-4 hidden-xs"><div class="img-thumbnail center-block"></div></div>
                                <div class="col-md-4 col-sm-4 hidden-xs"><div class="img-thumbnail center-block" style="background-position: -77px 0;"></div></div>
                                <div class="col-md-4 col-sm-4 hidden-xs"><div class="img-thumbnail center-block" style="background-position: -153px 0"></div></div>
                                
                                <div class="clearfix"></div>
                                <br>
                                <br>

                                <div class="col-md-8 col-sm-8 col-xs-8 col-md-offset-2 col-sm-offset-2 col-xs-offset-2">
                                    <div class="row">
                                        <div class="slider"></div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <br><br>
                                <br>
                                

                                <div class="col-md-4 col-md-offset-4">
                                    <button type="button" class="next btn btn-raised bg-theme btn-block" onclick="next()">Continue</button>
                                </div>

                                <div class="clearfix"></div>
                                
                            </form>
                    </div>

                    <style>
                        .img-circle{
                            width: 8em;
                            height: 8em;
                            margin: 0 auto;
                            background-image:url(images/members.png)
                        }
                        .img-thumbnail{
                            width:5em;
                            height:14em;
                            margin: 0 auto;
                            border: none;
                            background-repeat: no-repeat;
                            background-image:url(images/age.png)
                        }
                        .form-horizontal label.btn{
                            padding: 10px 23px;
                        }
                        .form-horizontal label.btn.active{
                            background: #FC6558 !important;
                            color: #fff;
                        }
                        .noUi-tooltip{
                            display: block;
                            position: absolute;
                            border: 1px solid #D9D9D9;
                            border-radius: 3px;
                            background: #fff;
                            padding: 0 5px ;
                            text-align: center;
                        }
                        .noUi-horizontal .noUi-handle-lower .noUi-tooltip {
                            top: -30px;
                            left: -8px;
                        }

                        .noUi-handle.noUi-handle-lower.noUi-active > .noUi-tooltip{
                            transform:scale(1);
                        }
                        .noUi-handle.noUi-handle-lower{
                            left:-6px;
                        }
                        
                    </style>
                    <link href="css/nouislider.pips.css" rel="stylesheet">
                    <script src="js/nouislider.min.js"></script>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>

    $(function() {
        $.material.init();
                            
        var softSlider = $(".slider")[0];

        noUiSlider.create(softSlider, {
            start: 18,
            step: 1,
            connect:'lower',
            range: {
                min: 18,
                max: 70
            },
            pips: {
                mode: 'values',
                values: [18,25,30,35,40,45,50,55,60,65,70],
                density: 2
            },
            tooltips: true,
            format: {
                to: function ( value ) {
                    return value;
                },
                from: function ( value ) {
                    return value.replace(',-', '');
                }
            }
        });

        function displayError() {
            swal({
                title : "Error !",
                text : 'Try Again!',
                type : 'error',
                confirmButtonClass : 'bg-theme',
                animation : false,
                width : 400
            });
        };

        function closeAlert() {
            $(".sweet-alert button").first().trigger('click');
        }

        $('.cancel').click(function(event) {
            $(".help-block").remove();
            $(".has-error").removeClass('has-error');
            event.preventDefault();
            var $form = $(this).parents('form')[0];
            $form.reset();
            $($form).find('.form-group.label-floating').addClass('is-empty');
        });

        $('.next').unbind('click').on('click', function(event) {

            event.preventDefault();

            // remove all error messages
            $(".help-block").remove();
            $(".has-error").removeClass('has-error');
            
            swal({
                title : 'Saving',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 400
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: $('.main form').serialize(), 
            })
            .done(function(data) {
                if (data.status == 200) {
                    swal({
                        title : "Success !",
                        text : data.message,
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });   
                    getAndInsert(data.url);
                    history.pushState(null, null, data.url);
                }
                else if (data.status == 400) {
                    swal({
                        title : "Error !",
                        text : data.message,
                        type : 'error',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });
                }
                else{  // Validation Error
                    displayError();
                    // show Errors
                    for(var fields in data) {
                        $('#'+fields).parents('.form-group').addClass('has-error');
                        $('#'+fields).parent().append('<p class="help-block">'+ data[fields][0] +'</p>');
                    }
                }
            })
            .error(function() {
                displayError();
            });
        });

    });
</script>
