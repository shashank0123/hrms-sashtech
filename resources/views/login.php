<!DOCTYPE html>
<html>
<head>
    <title> Login Sashtechs | HR Platform System for Business & Startups India</title>

    <meta name="description" content="Partner with Sashtechs for payroll services, human resources management, HRIS, time & attendance, reporting and tax filing."/>

    <meta name="keywords" content="hr software, hr platform, hr management system, payroll software, Benefits, Enterprise payroll, Group Health Insurance, payroll services, human resources management, HRIS, time & attendance, reporting and tax filing" />

    <?php require_once('links.php');?>
</head>

<style>
    .panel{
        width:320px;
        margin: 0 auto;
        padding: 5px;
    }
</style>

<body  style="margin-top:4em;">
        <div class="panel panel-default">
            <div class="panel-body">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <form class="form-horizontal" autocomplete="off" action="" method="POST">
                        <input type="text" style="display:none">
                        <input type="password" style="display:none">
                        <fieldset>
                            <legend><a href="/" id="logo" class="fa btn btn-block" >Sashtechs <div></div></a></legend>

                            <p class="help-block">
                                <?php if (!empty($result)) { echo $result;} ?>
                                <?php if (!empty($error_status)) { echo $error_status;} ?>
                            </p>

                            <?php $reset = session()->get('resetSuccess'); 
                                if (isset($reset)) 
                                { 
                                    echo    '<p class="text-success">' . 
                                            session()->get('resetSuccess') .
                                            '</p>'; 
                                } 
                                session()->flush();
                            ?>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating is-empty ">
                                        <label class="control-label" for="email"><span>Email</span></label>
                                        <input class="form-control" id="email" type="email" required name="email" style="cursor: auto;" autofocus autocomplete="off">
                                        <p class="help-block">
                                            <?php if (!empty($error_email)) { echo $error_email;} ?>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating is-empty ">
                                        <label class="control-label" for="pass"><span>Password</span></label>
                                        <input class="form-control" id="pass" type="password" name="password" style="cursor: auto;" autocomplete="off">
                                        <p class="help-block">
                                            <?php if (!empty($error_pass)) { echo $error_pass;} ?>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <br><br>

                            <div class="col-md-12 col-sm-12 col-xs-12 small">
                                <a href="forgotPassword" class="fa">Forgot Password</a>
                            </div>

                            <div class="clearfix"></div>
                            <br>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <button style="margin:0 auto;" type="submit" class="btn bg-theme btn-block btn-raised">Login</button>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </fieldset>
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>

                    </div>
                </div>
            </div>
        </div>

    <br><br>
</body>
</html>

<script>
    $.material.init();
</script>
