<!DOCTYPE html>
<html>
<head>
    <title>FAQ, Online Guide, tutorial, help to efile taxes online India </title>

    <meta name='description' content='Sashtechs offers statutory compliance services in HR across India to help companies deal with frequently changing laws of government to avoid penalties ' />

    <meta name='keywords' content=' Assistant Manager Payroll, Assistant Manager Benefits, Assistant Manager Taxation, Payroll Jobs, Payroll/Benefits Administration India, Tax Jobs, HR jobs, Finance Services jobs, Hr-payroll Process ' />

    <!--Import Google Icon Font-->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">

    <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet"></link>


    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Material Design -->
    <link href="css/bootstrap-material-design.css" rel="stylesheet">
    <link href="css/ripples.min.css" rel="stylesheet">
    <link href="css/roundslider.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">

    <script src="/js/jquery-1.12.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ripples.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/roundslider.min.js"></script>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    <style>

        .question{
            padding: 20px 15px;
            cursor: pointer;
            font-weight: 400;
        }
        .answer{
            padding: 15px;
        }
        .faq-item{
            font-family: FontAwesome;
            color: #555;
            border-bottom: 1px solid #ddd;
            margin: 10px auto;
        }

        .affix {
            top: 260px;
        }

        .affix-bottom {
            position: absolute;
        }

        #faqContainer {
            background: #fff;
            margin-top: -20px;
            padding-top: 2em;
            position: relative;
        }

        #myScrollspy .nav {
            width: 157px;
            height: 30em;
        }

        .nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover {
            background-color: #cf3d3d;
            font-weight: 400;
        }
    </style>
</head>

<body class="scroll-area" data-spy="scroll" data-target="#myScrollspy" data-offset="200">
<?php require_once("staticNavBar.php");?>

<!-- ================  Header  ===================== -->

<header class="parallax" style="background-image: url(images/faq.jpg);background-position: center">
    <div class="panel panel-default" style="  ">
        <div class="panel-body">
            <div class="text-center">
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <h5>FAQ</h5>
                <h2>FAQ’s for Payroll, Benefits and Taxes </h2>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
            </div>
        </div>
    </div>
</header>

<section id="faqContainer">
    <br>
    <br>

    <div class="container">
        <div class="row">
            <nav class="col-sm-3 hidden-xs " id="myScrollspy">
                <br>
                <ul class="nav nav-pills nav-stacked" data-spy="affix" data-offset-top="250" data-offset-bottom="1060">
                    <li><a href="#topQueries">Top Queries</a></li>
                    <li><a href="#payroll">Payroll</a></li>
                    <li><a href="#benefits">Health Benefits</a></li>
                    <li><a href="#expenses">Expenses</a></li>
                    <li><a href="#time">Time Tracking</a></li>
                </ul>
            </nav>
            
            <div class="col-sm-9">
                <div id="topQueries">
                    <h2>Top Queries</h2>  
                    <br> 
                    <div class="faq-item">
                        <div class="question">
                            <span class="h4"> What is the concept of CTC? </span><i class="fa fa-chevron-down pull-right"></i>
                        </div>
                        <div class="answer">
                            Any amount that company spends for the benefit of its employee can be calculated under Cost to Company. Along with salary, CTC may include medical insurance, accident policy, subsidized lunch / food, any attire / apparel / furnishing allowance etc., PF, Gratuity.
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="question">
                            <span class="h4"> What is Payslip? </span><i class="fa fa-chevron-down pull-right"></i>
                        </div>
                        <div class="answer">
                            A note given to an employee when they have been paid, detailing the amount of pay given and the tax and insurance deducted.
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="question">
                            <span class="h4">What is PF?</span><i class="fa fa-chevron-down pull-right"></i>
                        </div>
                        <div class="answer">
                            PF means Provident Fund. It is like a pension and Insurance; a part of your salary is deducted every month and deposited in your Provident Fund Account. If you are working in a private company then the company pays the same amount as it is deducted from your account and when you leave the company you can apply and withdraw the amount. It's your personal saving. If you are a government employee then you will get your saved PF of your whole life of your service when you retire.
                            <br>
                            PF amount which is accumulated in the PF Fund is also transferable. Employee can transfer his/her PF account, if the employment changes from one company to company.
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="question">
                            <span class="h4">What is Employee State Insurance (ESI)?</span><i class="fa fa-chevron-down pull-right"></i>
                        </div>
                        <div class="answer">
                            The ESI Act has been passed to provide for certain benefits to employees in case of sickness, maternity and employment injury and to make provisions for related matters. As the name suggests, it is basically an insurance scheme i.e. employee gets benefits if he is sick or disabled.
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="question">
                            <span class="h4">What is Profession Tax (PT)?</span><i class="fa fa-chevron-down pull-right"></i>
                        </div>
                        <div class="answer">
                            Profession tax is a state specific tax. Every state will have a slab of rates. PT is collected on profession, trades, and callings or holds any appointment public or private or is employed in any manner in the state for the benefit of the state. Every employee has to pay profession tax based on his earning. Employer deducts the tax from the salary and remits to state government account every month in the specified form. <br>
                            PT differs from state to state. <br> 
                            Some states never comes under the PT <br>
                            The payment term also differs from state to state. <br>
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="question">
                            <span class="h4">What is Gratuity?</span><i class="fa fa-chevron-down pull-right"></i>
                        </div>
                        <div class="answer">
                            Gratuity is a retirement benefit given by an employer to an employee. It is paid for employees at the time of leaving, as a gratitude for the service employee has rendered, provided that the employee has served the company more than 5 years. Gratuity is paid for a retiring employee as well as the employee who is leaving after completing 5 years of service in a company.
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="question">
                            <span class="h4">What is TDS?</span><i class="fa fa-chevron-down pull-right"></i>
                        </div>
                        <div class="answer">
                            TDS means Tax Deducted at Source. In the payroll perspective, deducting the income tax of employee to be paid to the government from employee's salary is TDS. The employer has to deduct and remit the full tax arising out of employee's salary. For this, employer has to make the estimation at the beginning of the financial year as to what would be the probable income and income tax for each employee based on the earnings, deductions and investments proposed to be made by such employees. This total tax is to be deducted from their salaries over the 12 months between April and March.
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="question">
                            <span class="h4">What is Group Health Insurance?</span><i class="fa fa-chevron-down pull-right"></i>
                        </div>
                        <div class="answer">
                            Group health insurance provides a health coverage collectively for the employees/members of an organization.
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="question">
                            <span class="h4">What is an expense?</span><i class="fa fa-chevron-down pull-right"></i>
                        </div>
                        <div class="answer">
                            The cost required for something; the money spent on something.
                        </div>
                    </div>
                </div>
                <div id="payroll"> 
                    <br>
                    <h2>Payroll</h2>  
                    <br> 
                    <div class="faq-item">
                        <div class="question">
                            <span class="h4">What does Salary Means?</span><i class="fa fa-chevron-down pull-right"></i>
                        </div>
                        <div class="answer">
                            A fixed regular payment, typically paid on a monthly or biweekly basis is called as salary. Many salaries also include such employee benefits as health and life insurance, savings plans, and Social Security. Salary income is taxable by the federal, state, and local government, where applicable, through payroll withholding.
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="question">
                            <span class="h4">What Is Form 16?</span><i class="fa fa-chevron-down pull-right"></i>
                        </div>
                        <div class="answer">
                            Form 16 Is A Certificate Issued By The Employer To Its Employees Under Section 203 Of Income Tax Act For Tax Deducted At Source From The Income Chargeable Under The Head “Salaries” This Is Issued Yearly And The Original Form 16 Is Required To File Individual Tax Returns
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="question">
                            <span class="h4">What do Allowance Means?</span><i class="fa fa-chevron-down pull-right"></i>
                        </div>
                        <div class="answer">
                            Allowances are fixed sums of money paid regularly in addition to salary. An amount that is allowed or granted is called as Allowance. There are three types of allowance. <br>
                            <ul>
                                <li>Taxable allowance, partially</li>
                                <li>Exempted allowance</li>
                                <li>Fully – exempted allowance</li>
                            </ul>
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="question">
                            <span class="h4">What is Permanent Account Number (PAN)?</span><i class="fa fa-chevron-down pull-right"></i>
                        </div>
                        <div class="answer">
                            Permanent Account Number (PAN) is a code that acts as identification of Indians, especially those who pay Income Tax. It is a unique, 10-character alpha-numeric identifier, issued to all judicial entities identifiable under the Indian Income Tax Act 1961. <br> An example number would be in the form of AAAPL1234C.
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="question">
                            <span class="h4">How to calculate House Rent Allowance (HRA) and exemption thereon?</span><i class="fa fa-chevron-down pull-right"></i>
                        </div>
                        <div class="answer">
                            From the House Rent Allowance (HRA) received as part of salary during the year, least of the following three amounts is exempted from tax (or not included in income):
                            <ul>
                                <li>HRA allowance actually received</li>
                                <li>Rent paid –10% salary (Basic + DA forming part of salary + commission as a % of turnover)</li>
                                <li>40% of salary (50% for Metro city)</li>
                            </ul>
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="question">
                            <span class="h4">What is Transport allowance / Conveyance Allowance?</span><i class="fa fa-chevron-down pull-right"></i>
                        </div>
                        <div class="answer">
                            Transport allowance for traveling from residence to work place up to Rs 800 per month is exempted. For a person with physical disability the amount is Rs. 1600.
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="question">
                            <span class="h4">What is Children Education Allowance?</span><i class="fa fa-chevron-down pull-right"></i>
                        </div>
                        <div class="answer">
                            Children Education Allowance is exempted up to Rs. 100/- per month per child up to a maximum of 2 children.
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="question">
                            <span class="h4">What are the statutory deductions from salary?</span><i class="fa fa-chevron-down pull-right"></i>
                        </div>
                        <div class="answer">
                            PF, ESI, PT and TDS are considered as Statutory Deductions that are to be deducted from salary.
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="question">
                            <span class="h4">Applicability of ESI?</span><i class="fa fa-chevron-down pull-right"></i>
                        </div>
                        <div class="answer">
                            Wage-limit for coverage under the Act, is Rs.15, 000/- per month (this is monthly salary including all allowances)
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="question">
                            <span class="h4">What is TAN No.?</span><i class="fa fa-chevron-down pull-right"></i>
                        </div>
                        <div class="answer">
                            TAN is Tax Deduction & Collection Account Number. For filing the TDS, organization must have a TAN Number.
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="question">
                            <span class="h4">How Do I Account My Salary From More Than One Employer?</span><i class="fa fa-chevron-down pull-right"></i>
                        </div>
                        <div class="answer">
                            The Employee Is Required To Furnish Details Of The Income, Under The Head "Salaries" Due Or Received From The Former/ Other Employer, To The Present/Current Employer, And Also Tax Deducted At Source, In Writing And Duly Verified By Him And By The Former/Other Employer In Form 12B (The Present/Current Employer Will Deduct Tax At Source Based On The Aggregate Amount Of Salary (Including Salary Received From The Former Or Other Employer), Though It Is Not Shown On The Form 16 Of The Current Employer.
                        </div>
                    </div>
                </div>        
                <div id="benefits">
                    <br>
                    <h2>Health Benefits</h2>
                    <br>
                    <div class="faq-item">
                        <div class="question">
                            <span class="h4">Why Group Health Insurance?</span><i class="fa fa-chevron-down pull-right"></i>
                        </div>
                        <div class="answer">
                            Buying a group coverage for the employees/members of an organization together rather than buying individual plans separately, is much cheaper. Hence, group health insurance works best for those who are looking for a cost effective insurance solution.
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="question">
                            <span class="h4">What are its Key Features of Group Health Insurance?</span><i class="fa fa-chevron-down pull-right"></i>
                        </div>
                        <div class="answer">
                            A group plan is in many ways better than an individual plan. It is very affordable and comes with lesser exclusions and shorter waiting periods. But a group plan covers an individual only as long as he's an employee/ member of the organization.
                        </div>
                    </div>
                </div>
                <div id="expenses">
                    <br>
                    <h2>Expenses</h2>
                    <br>         
                    <div class="faq-item">
                        <div class="question">
                            <span class="h4">what is expense report</span><i class="fa fa-chevron-down pull-right"></i>
                        </div>
                        <div class="answer">
                            A report that tracks expenses incurred during the course of performing necessary job functions. Examples include charges for gas, meals, parking or lodging. 
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="question">
                            <span class="h4">Why won't my receipt attach and/or why do I keep receiving an error message?</span><i class="fa fa-chevron-down pull-right"></i>
                        </div>
                        <div class="answer">
                            Please verify the following concerning your receipt: you can upload images that are up to 500 KB in size and they can be PNG, JPG, TIFF, HTML or PDF files.
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="question">
                            <span class="h4">My expense report is approved. When I will get money?</span><i class="fa fa-chevron-down pull-right"></i>
                        </div>
                        <div class="answer">
                            The money will be transferred along with next paycheck.
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="question">
                            <span class="h4">My expense report is disapproved. What is next step?</span><i class="fa fa-chevron-down pull-right"></i>
                        </div>
                        <div class="answer">
                            Talk to your manager or employers and edit expense with correct information.
                        </div>
                    </div>
                </div>      
                <div id="time">
                    <br>
                    <h2>Time Tracking</h2>   
                    <br> 
                    <div class="faq-item">
                        <div class="question">
                            <span class="h4">What is timesheet?</span><i class="fa fa-chevron-down pull-right"></i>
                        </div>
                        <div class="answer">
                            A timesheet (or time sheet) is a method for recording the amount of a worker's time spent on each job.
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="question">
                            <span class="h4">What is sick leave ?</span><i class="fa fa-chevron-down pull-right"></i>
                        </div>
                        <div class="answer">
                            Sick leave (or paid sick days or sick pay) is time off from work that workers can use to stay home to address their health and safety needs without losing pay.
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="question">
                            <span class="h4">what is floating leave?</span><i class="fa fa-chevron-down pull-right"></i>
                        </div>
                        <div class="answer">
                            Floating Holidays” are typically a fixed number of personal days that employees may use at any time during the year over and above any vacation, sick or other paid time off
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="question">
                            <span class="h4">what is vacation leave?</span><i class="fa fa-chevron-down pull-right"></i>
                        </div>
                        <div class="answer">
                            The amount of vacation time you earn can be affected by several factors. Some of these factors include your length of service at the company, the number of hours you work a year etc
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="question">
                            <span class="h4">What is Casual Leave?</span><i class="fa fa-chevron-down pull-right"></i>
                        </div>
                        <div class="answer">
                            These leave are granted for certain unforeseen situation or were you are require to go for one or two days leaves.
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="question">
                            <span class="h4">What is Leave without pay</span><i class="fa fa-chevron-down pull-right"></i>
                        </div>
                        <div class="answer">
                            If person do have any leave to his balance and the situation warrants him to take the leave, the leave is granted by the Company as loss of pay or which may be adjusted against the future leave or as a special case the special paid leave based on the person contribution to the Company at management discretion.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <br>
    </div>
</section>

<div class="clearfix"></div>

<div class="clearfix"> </div>

<section class="parallax" style="background-image: url(images/try.jpg);">
    <div class="panel panel-default" style="  ">
        <div class="panel-body">
            <div class="text-center">
                <br>
                <br>
                <br>
                <br>
                <h5><b>TRY Sashtechs</b></h5>
                <h3 class="text-uppercase">Start your one month free trial </h3>
                <br>

                <div class="panel text-left" id="panel-text-left">
                    <?php require 'demoForm.php'; ?>
                </div>
                <br>
                    <div class="clearfix"></div>
                    <br><br>
                    <br><br>
            </div>
        </div>
    </div>
</section>

<?php require_once('staticFooter.php');?>

</body>
</html>

<script>

    $(function() {
        $.material.init();

        $(".faq-item .question").on('click', function(event) {
            $(this).siblings('.answer').slideToggle(400);
            if ($(this).children('i').hasClass('fa-chevron-down')) {
                $(this).children('.fa-chevron-down').removeClass('fa-chevron-down').addClass('fa-chevron-up');
            }
            else
                $(this).children('.fa-chevron-up').removeClass('fa-chevron-up').addClass('fa-chevron-down');

        });

        $(".answer").hide();

        // $('#faqContainer').scrollspy({target: ".myScrollspy", offset: 50});   

        // Add smooth scrolling on all links inside the navbar
        $("#myScrollspy a").on('click', function(event) {

            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
            $('html, body').animate({
                scrollTop: $(hash).offset().top - 100
            }, 800, function(){
                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash;
            });
        });
    });
</script>
