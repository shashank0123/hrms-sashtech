<!-- <div class="navbar-fixed-top" style="background: #fff;box-shadow: 0 0px 10px;">
    <div class="container">
        <p class="text-small-caps-xs margin-bottom-none bg-white">
            Latest Step
        </p>
        <h5 class="text-lg-250 margin-top-none">
            Next Step To be executed in Dashboard
        </h5>
    </div>
</div> -->
<div class="container hidden-xs">
    <div class="row">
        <div class="col-md-3 col-sm-3"></div>
        <div class="col-md-9 col-sm-9 small text-uppercase">
            <div class="col-md-6 col-sm-6 heart-beat">We <i class="fa fa-heart text-danger"></i> <?php echo session()->get('companyName');?></div>

            <div class=" col-md-6 col-sm-6 text-right dropdown">
                <a href="javascript:void(0)" data-target="#" class="dropdown-toggle" data-toggle="dropdown">Welcome 
              <?php echo session()->get('employerName');?>
                <b class="caret"></b></a>
                <ul class="dropdown-menu pull-right">
                    <li><a class="h6 " id="changeView" href="/changeView">Switch to Employee View</a></li>
                    <li><a class="h6 " href="javascript:void(0)">Personal Details</a></li>
                    <?php if (!empty(session()->get('adminId'))): ?>
                        <li><a class="h6 " href="/adminView">Admin Panel</a></li>
                    <?php endif ?>
                    <?php if (!empty(session()->get('accountantId'))): ?>
                        <li><a class="h6 " href="/accountantView">Accountant Panel</a></li>
                    <?php endif ?>
                    <li><a class="h6" href="/logout">Log Out</a></li>
                </ul>
              <!--   <script>
                    $(function() {
                        $("#changeView").click(function(event) {
                            $.ajax({
                                url: location.host +'/changeView',
                                type: 'POST',
                                data: {type: 'employer'},
                            })
                            .done(function(data) {
                                console.log("success", data);
                            })
                            .fail(function() {
                                console.log("error");
                            })                            
                        });
                    })
                </script>   --> 
            </div>
        </div>
        <br>
    </div>
</div>
<!-- ================  Body  ===================== -->

<style>    
    .nav.nav-stacked .active>a{
        color: #e54255;
        font-weight: 400;
    }

    #logo .icon-bar {
        border: 1px solid;
    }

    #logo .navbar-toggle {
        position: absolute;
        top: 3px;
        right: 0;
    }
</style>

<div class="container">
    <div class="row">
        <nav class="col-md-3 col-sm-3">
            <div class="col-md-12 col-sm-12 col-xs-12">
            <!-- <img src="images/W.png" alt="Sashtechs"> -->
                <span id="logo" class="fa btn" >Sashtechs <div></div>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-warning-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </span>
            </div>
            <div class="clearfix"></div>
            <div class="navbar-collapse collapse navbar-warning-collapse">
                <ul class="nav navbar nav-stacked" style="background:transparent">
                    <?php if (Session::get('complete')=='on'){ ?>
                    <li data-url="employerDashboard" class="active"><a class="ajaxify btn" href="/employerDashboard"><i class="fa fa-flag fa-lg"></i>Dashboard</a></li>
                    <?php } else { ?>
                    <li data-url="getStarted" class="active"><a class="ajaxify btn" href="/getStarted"><i class="fa fa-flag fa-lg"></i>get&nbsp;started</a></li>
                    <!-- <li class="disabled"><br></li> -->
                    <?php }?>
                    <li data-url="runPayroll"><a class="ajaxify btn" href="/runPayroll"><i class="fa fa-inr fa-lg"></i>run payroll</a></li>
                    <li data-url="payContractor"><a class="ajaxify btn" href="/payContractor"><i class="fa fa-money fa-lg"></i>pay&nbsp;contractors</a></li>
                    <li class="disabled"><br></li>
                    <li data-url="manageEmployees"><a class="ajaxify btn" href="/manageEmployees"><i class="fa fa-user fa-lg"></i>employees</a></li>
                    <li data-url="manageContractors"><a class="ajaxify btn" href="/manageContractors"><i class="fa fa-briefcase fa-lg"></i>contractors</a></li>
                    <li data-url="companyDetail"><a class="ajaxify btn" href="/companyDetail"><i class="fa fa-building fa-lg"></i>company</a></li>
                    <?php if (Session::get('plan')=='benefits' || Session::get('plan')=='manager'){?>
                    <li data-url="benefit"><a class="ajaxify btn" href="/addBenefit_step0"><i class="fa fa-cog fa-lg"></i>benefits</a></li>
                    <?php } ?>
                    <li class="disabled"><br></li>
                    <li data-url="bulkFeature"><a class="ajaxify btn" href="/bulkFeature"><i class="fa fa-users fa-lg"></i>bulk feature</a></li>
                    <li data-url="employerExpenses"><a class="ajaxify btn" href="/employerExpenses"><i class="fa fa-credit-card fa-lg"></i>Expenses</a></li>
                    <li data-url="calendar"><a class="ajaxify btn" href="/calendar"><i class="fa fa-calendar fa-lg"></i>calendar</a></li>
                    <li data-url="businessIntelligence"><a class="ajaxify btn" href="/businessIntelligence"><i class="fa fa-list-alt fa-lg"></i>Business Intelligence</a></li>
                </ul>
            </div>
        </nav>
