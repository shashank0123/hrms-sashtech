<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>


<body  style="margin-top:2em;">
           <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">
                        <div id="view">
                            <h3> Employee Details </h3><hr>
                            <br>
                                
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="detail">
                                        
                                        <h5> 
                                            <i class="fa fa-user"> </i><b> &nbsp;&nbsp;<u>Personal Details</u>
                                            <span class="pull-right edit btn btn-default" data-id="personalDetail"><i class="fa fa-pencil"></i></span></b>
                                            <div class="clearfix"></div>
                                        </h5>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-4 col-sm-4 col-xs-4"><h6>Name</h6></div>
                                            <div class="col-md-8 col-sm-8 col-xs-8"><h6 id="viewName">
                                                <?php echo (isset($result1->firstName)) ? $result1->firstName : ''; ?>
                                                <?php echo (isset($result1->lastName))  ? $result1->lastName : ''; ?>
                                            </h6></div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-4 col-sm-4 col-xs-4"><h6>PAN</h6></div>
                                            <div class="col-md-8 col-sm-8 col-xs-8"><h6 id="viewPan">
                                            <?php echo (isset($result->PAN)) ? $result->PAN : ''; ?>
                                            </h6></div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-4 col-sm-4 col-xs-4"><h6>D.O.B</h6></div>
                                            <div class="col-md-8 col-sm-8 col-xs-8"><h6 id="viewDateOfBirth">
                                            <?php echo (isset($result->DateOfBirth)) ? $result->DateOfBirth:''; ?>
                                            </h6></div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-4 col-sm-4 col-xs-4"><h6>D.O.J</h6></div>
                                            <div class="col-md-8 col-sm-8 col-xs-8"><h6 id="viewJoinDate">
                                            <?php echo (isset($result1->joinDate)) ? $result1->joinDate:''; ?>
                                            </h6></div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-4 col-sm-4 col-xs-4"><h6>Email Id</h6></div>
                                            <div class="col-md-8 col-sm-8 col-xs-8"><h6 id="viewEmail">
                                            <?php echo (isset($result1->email)) ? $result1->email:''; ?>
                                            </h6></div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-4 col-sm-4 col-xs-4"><h6>Alternate Email Id</h6></div>
                                            <div class="col-md-8 col-sm-8 col-xs-8"><h6 id="viewAltEmail">
                                            <?php echo (isset($result->altEmail)) ? $result->altEmail:''; ?>
                                            </h6></div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-4 col-sm-4 col-xs-4"><h6>Phone</h6></div>
                                            <div class="col-md-8 col-sm-8 col-xs-8"><h6 id="viewPhone">
                                            <?php echo (isset($result->phone)) ? $result->phone:''; ?>
                                            </h6></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="detail">
                                        
                                        <h5>
                                            <i class="fa fa-users"> </i><b> &nbsp;&nbsp; <u>Reimbursements</u>
                                            <!-- <span class="pull-right edit btn btn-default" data-id="familyDetail"><i class="fa fa-pencil"></i></span> --></b>
                                            <div class="clearfix"></div>
                                        </h5>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6></h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6></h6></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="detail">
                                        
                                        <h5>
                                            <i class="fa fa-briefcase"></i><b> &nbsp;&nbsp; <u>Tax Details</u>
                                            <span class="pull-right edit btn btn-default" data-id="taxDetail"><i class="fa fa-pencil"></i></span></b>
                                            <div class="clearfix"></div>
                                        </h5>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>CTC</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 id="viewCTC">
                                            <?php echo (isset($result1->ctc)) ? $result1->ctc :''; ?>
                                            </h6></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="detail">
                                        <h5>
                                            <i class="fa fa-road"> </i><b> &nbsp;&nbsp;<u>Address</u>
                                            <span class="pull-right edit btn btn-default" data-id="address"><i class="fa fa-pencil"></i></span></b>
                                            <div class="clearfix"></div>
                                        </h5>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-4 col-sm-4 col-xs-4"><h6>Local</h6></div>
                                            <div class="col-md-8 col-sm-8 col-xs-8"><h6 id="viewLocalAddress">
                                            <?php echo (isset($result->localAddress)) ? $result->localAddress:''; ?>
                                            </h6></div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-4 col-sm-4 col-xs-4"><h6>Permanent</h6></div>
                                            <div class="col-md-8 col-sm-8 col-xs-8"><h6 id="viewPermanentAddress">
                                            <?php echo (isset($result->permanentAddress)) ? $result->permanentAddress:''; ?>
                                            </h6></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div> 
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="detail">
                                    
                                        <h5>
                                            <i class="fa fa-inr"> </i>
                                            <b>&nbsp;&nbsp;<u>Payment Method</u><span class="pull-right edit btn btn-default" data-id="paymentMethod"><i class="fa fa-pencil"></i></span></b>
                                            <div class="clearfix"></div>
                                        </h5>


                                        <?php if((count($result3)>0) and (count($result3)<2)): ?>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>Mode Of Payment:</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 id="viewModeOfPayment">
                                            <?php if($result3[0]->modeOfPayment == 'cheque'): ?>
                                            Cash / Cheque
                                            <?php elseif($result3[0]->modeOfPayment == 'directDeposit'): ?>
                                            Direct Deposit
                                            <?php endif; ?>
                                            </h6></div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>Direct Deposit1</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 id="viewSharePercent1"></h6></div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>Direct Deposit2</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 id="viewSharePercent2"></h6></div>
                                        </div>
                                        <?php elseif(count($result3) == 2): ?>
                                        <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>Mode Of Payment:</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 id="viewModeOfPayment">
                                            <?php if($result3[0]->modeOfPayment == 'cheque'): ?>
                                            Cash / Cheque
                                            <?php elseif($result3[0]->modeOfPayment == 'directDeposit'): ?>
                                            Direct Deposit
                                            <?php endif; ?>
                                            </h6></div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12 ">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>Direct Deposit1</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 id="viewSharePercent1">
                                                <?php echo $result3[0]->sharePercent; ?>
                                            </h6></div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12 ">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>Direct Deposit2</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 id="viewSharePercent2">
                                                <?php echo $result3[1]->sharePercent; ?>
                                            </h6></div>
                                        </div>
                                        <?php endif; ?>
                                        <div class="clearfix"></div>
                                    </div> 
                                </div>
                                
                            </div>

                            <div class="clearfix"></div>
                            <br>
                            <br>

                            <?php if (session()->get('plan')=='benefits' || Session::get('plan')=='manager'){?>
<!--                             <h3>Benefits</h3>
                            <hr>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="detail">
                                        
                                        <h5>
                                            <i class="fa fa-users"> </i><b> &nbsp;&nbsp; <u>Medical Policy</u></b>
                                            <div class="clearfix"></div>
                                        </h5>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6></h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6></h6></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="detail">
                                        
                                        <h5>
                                            <i class="fa fa-users"> </i><b> &nbsp;&nbsp; <u>Vacations</u></b>
                                            <div class="clearfix"></div>
                                        </h5>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6></h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6></h6></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="detail">
                                        
                                        <h5>
                                            <i class="fa fa-users"> </i><b> &nbsp;&nbsp; <u>Family Details</u></b>
                                            <div class="clearfix"></div>
                                        </h5>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6></h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6></h6></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
 -->
                            <div class="clearfix"></div>
                            <br><br>
                            <?php } ?>
                        </div>

                        <!-- ======================== EDITORS ==================================-->

                        <div class="editor hidden" id="personalDetail">
                            <form action="#" method="POST" class="form-horizontal">
                                <fieldset>
                                    <div class="clearfix"></div>
                                    <br>
                                    <legend>Edit Personal Detail</legend>
                                    <!-- identify type of form -->
                                    <input type="hidden" name="area" value="personal">

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-9 col-sm-10 col-xs-12">
                                            <div class="form-group label-floating">
                                                <label class="control-label" for="firstName"><span>First Name</span></label>
                                                <input class="form-control" id="firstName" value="<?php echo (isset($result1->firstName)) ? $result1->firstName : ''; ?>" name="firstName" type="text" value="" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-9 col-sm-10 col-xs-12">
                                            <div class="form-group label-floating">
                                                <label class="control-label" for="lastName"><span>Last Name</span></label>
                                                <input class="form-control" id="lastName" value="<?php echo (isset($result1->lastName))  ? $result1->lastName : ''; ?>" name="lastName" type="text" value="" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-9 col-sm-10 col-xs-12">
                                            <div class="form-group label-floating">
                                                <label class="control-label" for="pan"><span>PAN</span></label>
                                                <input class="form-control" id="pan" value="<?php echo (isset($result->PAN)) ? $result->PAN : ''; ?>" name="pan" type="text" value="" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-9 col-sm-10 col-xs-12">
                                        <div class="form-group">
                                            <label for="dateOfBirth" class="col-md-2 control-label">D.O.B</label>

                                            <div class="col-md-10">
                                                <input type="text" value="<?php echo (isset($result->DateOfBirth)) ? $result->DateOfBirth:''; ?>" id="dateOfBirth" name="dateOfBirth" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-9 col-sm-10 col-xs-12">
                                        <div class="form-group">
                                            <label for="dateOfJoining" class="col-md-2 control-label">D.O.J</label>

                                            <div class="col-md-10">
                                                <input type="text" value="<?php echo (isset($result1->joinDate)) ? $result1->joinDate:''; ?>" id="dateOfJoining" name="dateOfJoining" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-9 col-sm-10 col-xs-12">
                                            <div class="form-group label-floating">
                                                <label class="control-label" for="email"><span>Email Id</span></label>
                                                <input class="form-control" id="email" value="<?php echo (isset($result1->email)) ? $result1->email:''; ?>" name="email" type="email" value="" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-9 col-sm-10 col-xs-12">
                                            <div class="form-group label-floating">
                                                <label class="control-label" for="altEmail"><span>Alternative Email Id</span></label>
                                                <input class="form-control" id="altEmail" value="<?php echo (isset($result->altEmail)) ? $result->altEmail:''; ?>" name="altEmail" type="email" value="" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-9 col-sm-10 col-xs-12">
                                            <div class="form-group label-floating">
                                                <label class="control-label" for="phone"><span>Phone</span></label>
                                                <input class="form-control" id="phone" value="<?php echo (isset($result->phone)) ? $result->phone:''; ?>" name="phone" type="text" value="" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="reset" class="btn btn-raised btn-default cancel" value="Cancel">
                                        <input type="submit" class="btn btn-raised bg-theme save" value="Save">
                                    </div>

                                </fieldset>
                            </form>
                        </div>

                        <div class="editor hidden" id="address">
                            <form action="#" method="POST" class="form-horizontal">
                                <fieldset>
                                    <div class="clearfix"></div>
                                    <br>
                                    <legend>Edit Address </legend>
                                    <!-- identify type of form -->
                                    <input type="hidden" name="area" value="address">

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-9 col-sm-10 col-xs-12">
                                            <div class="form-group label-floating">
                                                <label class="control-label" for="localAddress"><span>Local</span></label>
                                                <input class="form-control" id="localAddress" value="<?php echo (isset($result->localAddress)) ? $result->localAddress:''; ?>" name="localAddress" type="text" value="" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-9 col-sm-10 col-xs-12">
                                            <div class="form-group label-floating">
                                                <label class="control-label" for="permanentAddress"><span>Permanent</span></label>
                                                <input class="form-control" id="permanentAddress" value="<?php echo (isset($result->permanentAddress)) ? $result->permanentAddress:''; ?>" name="permanentAddress" type="text" value="" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="reset" class="btn btn-raised btn-default cancel" value="Cancel">
                                        <input type="submit" class="btn btn-raised bg-theme save" value="Save">
                                    </div>

                                </fieldset>
                            </form>
                        </div>

                        <div class="editor hidden" id="taxDetail">
                            <div id="salaryComponents">
                                <div class="clearfix"></div>
                                <hr>
                                <form action="#" class="form-horizontal">

                                    <input type="hidden" name="area" value="tax" >
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="jobTitle" class="col-md-3 control-label">Job Title</label>
                                            
                                            <div class="col-md-9">
                                                <select id="jobTitle" name="jobTitle" class="form-control">
                                                    <option value="Worker">Worker</option>
                                                    <option value="Manager">Manager</option>
                                                    <option value="Staff">Staff</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating <?php isset($result1->ctc) ? '' : 'is-empty'; ?>">
                                                <label class="control-label" for="ctc"><span>CTC</span></label>
                                                <input class="form-control" id="ctc" name="ctc" type="number" min="0" style="cursor: auto;" value="<?php echo isset($result1->ctc) ? $result1->ctc : ''; ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <br>
                                    <legend>Salary Components</legend>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating <?php isset($result2->basic) ? '' : 'is-empty'; ?> ">
                                                <label class="control-label" for="basic"><span>Basic</span></label>
                                                <input class="form-control" id="basic" name="basic" type="number" min="0" style="cursor: auto;" value="<?php echo isset($result2->basic) ? $result2->basic : ''; ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating <?php isset($result2->hra) ? '' : 'is-empty'; ?> ">
                                                <label class="control-label" for="hra"><span>HRA</span></label>
                                                <input class="form-control" id="hra" name="hra" type="number" min="0" style="cursor: auto;" value="<?php echo isset($result2->hra) ? $result2->hra : ''; ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating <?php isset($result2->conveyance) ? '' : 'is-empty'; ?> ">
                                                <label class="control-label" for="conveyance"><span>Conveyance</span></label>
                                                <input class="form-control" id="conveyance" name="conveyance" type="number" min="0" style="cursor: auto;" max="19200" value="<?php echo isset($result2->conveyance) ? $result2->conveyance : ''; ?>" >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating <?php isset($result2->medicalAllowance) ? '' : 'is-empty'; ?>">
                                                <label class="control-label" for="medicalAllowance"><span>Medical Allowance</span></label>
                                                <input class="form-control" id="medicalAllowance" name="medicalAllowance" type="number" min="0" max="15000" style="cursor: auto;" value="<?php echo isset($result2->medicalAllowance) ? $result2->medicalAllowance : ''; ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating <?php isset($result2->others) ? '' : 'is-empty'; ?>">
                                                <label class="control-label" for="others"><span>Others </span></label>
                                                <input class="form-control" id="others" name="others" type="number" min="0" style="cursor: auto;" value="<?php echo isset($result2->others) ? $result2->others : ''; ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating is-empty ">
                                                <label class="control-label" for="medicalInsurance"><span>Medical Insurance</span></label>
                                                <input class="form-control" id="medicalInsurance" name="medicalInsurance" type="number" min="0" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating is-empty ">
                                                <label class="control-label" for="telephone"><span>Telephone Allowance</span></label>
                                                <input class="form-control" id="telephone" name="telephone" type="number" min="0" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating is-empty ">
                                                <label class="control-label" for="leaveTravel"><span>Leave Travel Allowance</span></label>
                                                <input class="form-control" id="leaveTravel" name="leaveTravel" type="number" min="0" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating is-empty ">
                                                <label class="control-label" for="uniform"><span>Uniform Allowance</span></label>
                                                <input class="form-control" id="uniform" name="uniform" type="number" min="0" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating is-empty ">
                                                <label class="control-label" for="gratuity"><span>Gratuity</span></label>
                                                <input class="form-control" id="gratuity" name="gratuity" type="number" min="0" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating is-empty ">
                                                <label class="control-label" for="superAnnuation"><span>Super Annuation</span></label>
                                                <input class="form-control" id="superAnnuation" name="superAnnuation" type="number" min="0" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating is-empty ">
                                                <label class="control-label" for="annualBonus"><span>Annual Bonus</span></label>
                                                <input class="form-control" id="annualBonus" name="annualBonus" type="number" min="0" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating is-empty ">
                                                <label class="control-label" for="festivalBonus"><span>Festival Bonus</span></label>
                                                <input class="form-control" id="festivalBonus" name="festivalBonus" type="number" min="0" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating is-empty ">
                                                <label class="control-label" for="incentives"><span>Incentives</span></label>
                                                <input class="form-control" id="incentives" name="incentives" type="number" min="0" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating is-empty ">
                                                <label class="control-label" for="leaveEncashment"><span>Leave Encashment</span></label>
                                                <input class="form-control" id="leaveEncashment" name="leaveEncashment" type="number" min="0" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating is-empty ">
                                                <label class="control-label" for="pfContribution"><span>PF Contribution</span></label>
                                                <input class="form-control" id="pfContribution" name="pfContribution" type="number" min="0" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating is-empty ">
                                                <label class="control-label" for="esiContribution"><span>ESI Contribution</span></label>
                                                <input class="form-control" id="esiContribution" name="esiContribution" type="number" min="0" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-8 addMoreButton">
                                        <div class="btn-group  pull-left">
                                            <a href="#" data-target="#" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
                                                Add more Fields
                                                <span class="caret"></span>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li><a href="javascript:void(0)" data-id="medicalInsurance" class="addMore">Medical Insurance</a></li>
                                                <li><a href="javascript:void(0)" data-id="telephone" class="addMore">Telephone Allowance</a></li>
                                                <li><a href="javascript:void(0)" data-id="leaveTravel" class="addMore">Leave Travel Allowance</a></li>
                                                <li><a href="javascript:void(0)" data-id="uniform" class="addMore">Uniform Allowance</a></li>
                                                <li><a href="javascript:void(0)" data-id="gratuity" class="addMore">Gratuity</a></li>
                                                <li><a href="javascript:void(0)" data-id="superAnnuation" class="addMore">Super Annuation</a></li>
                                                <li><a href="javascript:void(0)" data-id="annualBonus" class="addMore">Annual Bonus</a></li>
                                                <li><a href="javascript:void(0)" data-id="festivalBonus" class="addMore">Festival Bonus</a></li>
                                                <li><a href="javascript:void(0)" data-id="incentives" class="addMore">Incentives</a></li>
                                                <li><a href="javascript:void(0)" data-id="leaveEncashment" class="addMore">Leave Encashment</a></li>
                                                <li><a href="javascript:void(0)" data-id="pfContribution" class="addMore">PF Contribution</a></li>
                                                <li><a href="javascript:void(0)" data-id="esiContribution" class="addMore">ESI Contribution</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <br>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="reset" class="btn btn-raised btn-default cancel" value="Cancel">
                                        <input type="submit" class="btn btn-raised bg-theme save" value="Save">
                                    </div>

                                </form>
                            </div>
                        </div>

                        <!-- Payment Method =========================================================== -->

                        <div class="editor hidden" id="paymentMethod">
                            <form action="#" method="POST" class="form-horizontal">
                                <fieldset>
                                    <div class="clearfix"></div>
                                    <br>
                                    <input type="hidden" name="area" value="payment">
                                    <legend>Edit Payment Methods </legend>

                                    <div class="col-md-8 col-sm-10 col-xs-12">
                                        <div class="form-group">
                                            <label for="modeOfPayment1" class="col-md-4 control-label">Mode Of Payment</label>

                                            <div class="col-md-8">
                                                <select id="modeOfPayment1" name="modeOfPayment1" class="modeOfPayment form-control">
                                                <?php if (count($result3)>0): ?>
                                                <?php if ((isset($result3[0]->modeOfPayment))and($result3[0]->modeOfPayment == 'cheque')): ?>
                                                    <option value="directDeposit">Direct Deposit</option>
                                                    <option value="cheque" selected>Cash / Cheque</option>
                                                <?php elseif((isset($result3[0]->modeOfPayment))and($result3[0]->modeOfPayment == 'directDeposit')): ?>
                                                    <option value="directDeposit" selected>Direct Deposit</option>
                                                    <option value="cheque">Cash / Cheque</option>
                                                <?php endif; ?>
                                                <?php else: ?>
                                                    <option value="directDeposit">Direct Deposit</option>
                                                    <option value="cheque">Cash / Cheque</option>
                                                <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="directDeposit1" class="paymentForm" >
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-8 col-sm-10 col-xs-12">
                                                <div class="form-group label-floating is-empty ">
                                                    <label class="control-label" for="bankName1"><span>Name of Bank</span></label>
                                                    <input class="form-control bankName" name="bankName1" id="bankName1"  type="text" value="<?php echo ((count($result3)>0)and(isset($result3[0]->bankName))) ? $result3[0]->bankName : '' ; ?>" style="cursor: auto;">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-8 col-sm-10 col-xs-12">
                                                <div class="form-group label-floating is-empty ">
                                                    <label class="control-label" for="branch1"><span>Branch</span></label>
                                                    <input class="form-control branch" name="branch1" id="branch1" type="text" value="<?php echo ((count($result3)>0)and(isset($result3[0]->branch))) ? $result3[0]->branch : '' ; ?>" style="cursor: auto;">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-8 col-sm-10 col-xs-12">
                                                <div class="form-group label-floating is-empty ">
                                                    <label class="control-label" for="accountHolder1"><span>Name as in Bank Account</span></label>
                                                    <input class="form-control accountHolder" name="accountHolder1" id="accountHolder1" value="<?php echo ((count($result3)>0)and(isset($result3[0]->accountHolder))) ? $result3[0]->accountHolder : '' ; ?>" type="text" style="cursor: auto;">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-8 col-sm-10 col-xs-12">
                                                <div class="form-group label-floating is-empty ">
                                                    <label class="control-label" for="accountNumber1"><span>Account Number</span></label>
                                                    <input class="form-control accountNumber " id="accountNumber1" name="accountNumber1" value="<?php echo ((count($result3)>0)and(isset($result3[0]->accountNumber))) ? $result3[0]->accountNumber : '' ; ?>" type="text" style="cursor: auto;">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8 col-sm-10 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Type of Account</label>

                                                <div class="col-md-8">
                                                    <div class="radio radio-primary">
                                                        <label>
                                                            <input type="radio" class="accountType" name="accountType1" id="accountType1" value="saving" <?php echo ((count($result3)>0)and(isset($result3[0]->accountType))and(strtolower(($result3[0]->accountType))==strtolower('Saving'))) ? 'checked' : '' ; ?>>
                                                                Saving
                                                        </label>
                                                    </div>
                                                    <div class="radio radio-primary">
                                                        <label>
                                                            <input type="radio" name="accountType1" class="accountType" id="accountType2" value="current" <?php echo ((count($result3)>0)and(isset($result3[0]->accountType))and(strtolower(($result3[0]->accountType))==strtolower('Current'))) ? 'checked' : '' ; ?>>
                                                            Current
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-8 col-sm-10 col-xs-12">
                                                <div class="form-group label-floating is-empty ">
                                                    <label class="control-label" for="ifsc1"><span>IFSC Code</span></label>
                                                    <input class="form-control ifsc" value="<?php echo ((count($result3)>0)and(isset($result3[0]->ifsc))) ? $result3[0]->ifsc : '' ; ?>" name="ifsc1" id="ifsc1" type="text" style="cursor: auto;">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-8 col-sm-10 col-xs-12">
                                                <div class="form-group label-floating">
                                                    <label class="control-label" for="sharePercent1"><span>percent (%) share of net Payment</span></label>
                                                    <input class="form-control sharePercent" name="sharePercent1" id="sharePercent1" type="number" min="0" max="100" value="<?php echo ((count($result3)>0)and(isset($result3[0]->sharePercent))) ? $result3[0]->sharePercent : '100' ; ?>" style="cursor: auto;" readonly>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                    </div>

                                    <div id="cheque" class="hidden ">
                                        <div class="clearfix"></div>
                                        <br>
                                        <br>
                                        <br>
                                            <div class="col-md-12 col-sm-12 col-sx-12">
                                                <p class="lead">You have selected cash/cheque as payment option, <br> Consult your employer as it will be provided to you personally.</p>
                                            </div>
                                        <br>
                                    </div>
                                    <br>
                                    <br>

                                    

                                    <div id="directDeposit2" class="paymentForm <?php echo ((count($result3)>1)) ? '' : 'hidden' ;?>">
                                        <legend>Direct Deposit 2</legend>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-8 col-sm-10 col-xs-12">
                                                <div class="form-group label-floating is-empty ">
                                                    <label class="control-label" for="bankName2"><span>Name of Bank</span></label>
                                                    <input class="form-control bankName"  name="bankName2" id="bankName2" type="text" value="<?php echo ((count($result3)>1)and(isset($result3[1]->bankName))) ? $result3[1]->bankName : '' ; ?>" style="cursor: auto;">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-8 col-sm-10 col-xs-12">
                                                <div class="form-group label-floating is-empty ">
                                                    <label class="control-label" for="branch2"><span>Branch</span></label>
                                                    <input class="form-control branch" name="branch2" id="branch2" type="text" value="<?php echo ((count($result3)>1)and(isset($result3[1]->branch))) ? $result3[1]->branch : '' ; ?>" style="cursor: auto;">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-8 col-sm-10 col-xs-12">
                                                <div class="form-group label-floating is-empty ">
                                                    <label class="control-label" for="accountHolder2"><span>Name as in Bank Account</span></label>
                                                    <input class="form-control accountHolder" name="accountHolder2" id="accountHolder2" type="text" value="<?php echo ((count($result3)>1)and(isset($result3[1]->accountHolder))) ? $result3[1]->accountHolder : '' ; ?>" style="cursor: auto;">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-8 col-sm-10 col-xs-12">
                                                <div class="form-group label-floating is-empty ">
                                                    <label class="control-label" for="accountNumber2"><span>Account Number</span></label>
                                                    <input class="form-control accountNumber" id="accountNumber2" name="accountNumber2" type="text" value="<?php echo ((count($result3)>1)and(isset($result3[1]->accountNumber))) ? $result3[1]->accountNumber : '' ; ?>" style="cursor: auto;">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-8 col-sm-10 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Type of Account</label>

                                                <div class="col-md-8">
                                                    <div class="radio radio-primary">
                                                        <label>
                                                            <input type="radio" class="accountType" name="accountType2" id="accountType3" value="saving" <?php echo ((count($result3)>0)and(isset($result3[0]->accountType))and(strtolower(($result3[0]->accountType))==strtolower('Saving'))) ? 'checked' : '' ; ?>>
                                                                Saving
                                                        </label>
                                                    </div>
                                                    <div class="radio radio-primary">
                                                        <label>
                                                            <input type="radio" name="accountType2" class="accountType" id="accountType4" value="current" <?php echo ((count($result3)>0)and(isset($result3[0]->accountType))and(strtolower(($result3[0]->accountType))==strtolower('Current'))) ? 'checked' : '' ; ?>>
                                                            Current
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-8 col-sm-10 col-xs-12">
                                                <div class="form-group label-floating is-empty ">
                                                    <label class="control-label" for="ifsc2"><span>IFSC Code</span></label>
                                                    <input class="form-control ifsc" value="<?php echo ((count($result3)>1)and(isset($result3[1]->ifsc))) ? $result3[1]->ifsc : '' ; ?>" name="ifsc2" id="ifsc2" type="text" style="cursor: auto;">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-8 col-sm-10 col-xs-12">
                                                <div class="form-group label-floating">
                                                    <label class="control-label" for="sharePercent2"><span>percent (%) share of net Payment</span></label>
                                                    <input class="form-control sharePercent" name="sharePercent2" id="sharePercent2" type="number" min="0" max="100" value="<?php echo ((count($result3)>1)and(isset($result3[1]->sharePercent))) ? $result3[1]->sharePercent : '0' ; ?>" style="cursor: auto;">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                        <br>
                                    </div>

                                    <!-- <div id="paytm" class="paymentForm hidden">
                                        <br>
                                        <legend>Paytm </legend>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-8 col-sm-10 col-xs-12">
                                                <div class="form-group label-floating is-empty ">
                                                    <label class="control-label" for="paytmId"><span>Account Id / User Id</span></label>
                                                    <input class="form-control paytmId" id="paytmId" name="paytmId" type="text"  style="cursor: auto;">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-8 col-sm-10 col-xs-12">
                                                <div class="form-group label-floating">
                                                    <label class="control-label" for="paytmSharePercent"><span>percent (%) share of net Payment</span></label>
                                                    <input class="form-control paytmSharePercent" id="paytmSharePercent" name="paytmSharePercent" type="number" min="0" max="100" value="0" style="cursor: auto;">
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->

                                    <div class=" col-md-8 col-sm-10 col-xs-12 text-right  dropdown">
                                        <a href="javascript:void(0)" data-target="#" class="dropdown-toggle addAnother pull-right" data-toggle="dropdown"><i class="fa fa-plus"></i> &nbsp;add Another Account 
                                        <b class="caret"></b></a>
                                        <ul class="dropdown-menu pull-right">
                                            <li><a class="h5" id="selectDeposit" data-id="directDeposit2" href="javascript:void(0)">Direct Deposit</a></li>
                                            <!-- <li><a class="h5" id="selectPaytm" data-id="paytm" href="javascript:void(0)">Paytm</a></li> -->
                                        </ul>
                                    </div>
                                    
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="reset" class="btn btn-raised btn-default cancel" value="Cancel">
                                        <input type="submit" class="btn btn-raised bg-theme save" value="Save">
                                    </div>

                                    <div class="clearfix"></div>
                                    <br>
                                </fieldset>
                            </form>
                        </div>
                    
                        <div class="clearfix"></div>   

                    </div>

                </div>

                <br><br>

                    <style>
                        .detail {
                            box-shadow: 0 0 2px #888;
                            padding: 15px;
                            margin-top: 15px;
                            overflow-x: overlay;
                        }
                        .editor {
                            position: relative;
                            padding: 2em 0;
                        }
                        .edit {
                            margin: -15px;
                        }
                    </style>

                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
</body>
</html>

<script>

    function displayError(msg) {
        swal({
            title : "Error !",
            text : msg,
            type : 'error',
            confirmButtonClass : 'bg-theme',
            animation : false,
            width : 400
        });
    };

    function closeAlert() {
        $(".sweet-alert button").first().trigger('click');
    }

    $(function() {
        $.material.init();

        $('#dateOfBirth').bootstrapMaterialDatePicker({ weekStart : 0, time: false }).on('dateSelected', function(event) {
            $('#'+$(this).data('dtp')).find('.dtp-btn-ok').trigger('click');
        });
        $('#dateOfJoining').bootstrapMaterialDatePicker({ weekStart : 0, time: false }).on('dateSelected', function(event) {
            $('#'+$(this).data('dtp')).find('.dtp-btn-ok').trigger('click');
        });

        $("#dateOfBirth").bootstrapMaterialDatePicker('setDate', moment(new Date(1980, 0,1)));

        $("form.form-horizontal").keydown(function(event) {
            if (event.keyCode == 27) {
                $(this).find('.cancel').trigger('click');
            }
        });

        $("form.form-horizontal .cancel").click(function(event) {
            var $inputs = $(this).parent().parent().find('.form-group');
            $inputs.each(function(index, el) {
                if (!$(el).find('input').val()) {
                    $(el).removeClass('is-empty');
                }
                $(el).removeClass('has-error');
                $(el).find('help-block').remove();
            });

            $(this).parents(".editor").addClass('hidden');
            $(".main .panel-body #view").removeClass('hidden');
        });

        $(".edit").click(function(event) {
            $("#"+$(this).data('id')).removeClass('hidden');
            (function smoothscroll(){
                var currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
                if (currentScroll > 0) {
                     window.requestAnimationFrame(smoothscroll);
                     window.scrollTo (0,currentScroll - (currentScroll/5));
                }
            })();

            $(".main .panel-body #view").addClass('hidden');
        });

        $("#personalDetail > form.form-horizontal").submit(function(event) {
            event.preventDefault();

            var $editor = $(this).parent(".editor");
            $(this).find(".help-block").remove();
            $(this).find(".has-error").removeClass('has-error');
            
            swal({
                title : 'Saving',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 300
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: $(this).serialize(),
            })
            .done(function(data) {
                if (data.status == 200) {
                    swal({
                        title : "Saved!",
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        showConfirmButton: false,
                        animation : false,
                        width : 300,
                        timer: 1500
                    });   
                    $("#viewName").text($("#firstName").val() + " " + $("#lastName").val());
                    $("#viewPan").text($("#pan").val());
                    $("#viewDateOfBirth").text($("#dateOfBirth").val());
                    $("#viewJoinDate").text($("#dateOfJoining").val());
                    $("#viewEmail").text($("#email").val());
                    $("#viewAltEmail").text($("#altEmail").val());
                    $("#viewPhone").text($("#phone").val());
                    $editor.addClass('hidden');
                    $(".main .panel-body #view").removeClass('hidden');
                }
                else if(data.status == '406a') {  //  email already Exist
                    closeAlert();
                    var $email = $("#email");
                    $email.parent().append('<p class="help-block">'+ data.message +'</p>');
                    $email.focus();
                    $email.parents('.form-group').addClass('has-error');
                }
                else if(data.status == '406b') {  // alternate email already Exist
                    closeAlert();
                    var $email = $("#altEmail");
                    $email.parent().append('<p class="help-block">'+ data.message +'</p>');
                    $email.focus();
                    $email.parents('.form-group').addClass('has-error');
                }
                else if (data.status == 304) {
                    displayError(data.message);
                }
                else{  // Validation Error
                    displayError('Validation Error');
                    // show Errors
                    for(var fields in data) {
                        $('#'+fields).parents('.form-group').addClass('has-error');
                        $('#'+fields).parent().append('<p class="help-block">'+ data[fields][0] +'</p>');
                    }
                }
            })
            .error(function() {
                displayError('Try Again !');
            });
        });

        $("#address > form.form-horizontal").submit(function(event) {
            event.preventDefault();

            var $editor = $(this).parent(".editor");
            $(this).find(".help-block").remove();
            $(this).find(".has-error").removeClass('has-error');
            
            swal({
                title : 'Saving',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 300
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: $(this).serialize(),
            })
            .done(function(data) {
                if (data.status == 200) {
                    swal({
                        title : "Saved!",
                        // text : data.message,
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        showConfirmButton: false,
                        animation : false,
                        width : 300,
                        timer: 1500
                    });
                    $("#viewLocalAddress").text($("#localAddress").val());
                    $("#viewPermanentAddress").text($("#permanentAddress").val());
                    $editor.addClass('hidden');
                    $(".main .panel-body #view").removeClass('hidden');
                }
                else{  // Validation Error
                    displayError('Validation Error');
                    // show Errors
                    for(var fields in data) {
                        $('#'+fields).parents('.form-group').addClass('has-error');
                        $('#'+fields).parent().append('<p class="help-block">'+ data[fields][0] +'</p>');
                    }
                }
            })
            .error(function() {
                displayError('Try Again !');
            });
        });

        // tax detail  ==============================================================================

        $("#taxDetail form.form-horizontal").submit(function(event) {
            event.preventDefault();

            var $editor = $(this).parents(".editor");
            $(this).find(".help-block").remove();
            $(this).find(".has-error").removeClass('has-error');

            var input = $("#salaryComponents .form-group input");
            var ctc = 0;

            // checking sum of salary components w.r.t ctc
            input.each(function(index, el) {
                ctc += $(el).val() ? 1*$(el).val(): 0;
            });
            ctc -= $("#ctc").val() || 0; // remove ctc that added

            if (!$("#ctc").val()) {
                $("#ctc").val(ctc).parents(".label-floating").removeClass('is-empty');
            }

            if($("#ctc").val() < ctc){
                swal({
                    title : 'Adjust salary components',
                    text : "Sum of salary components can not be greater than CTC",
                    type : 'error',
                    animation : false,
                    confirmButtonClass : 'bg-theme',
                },function() {
                    $("#ctc").parents(".form-group").addClass('has-error');
                    $("#ctc").focus();
                });
            }
            else {
                if ($("#ctc").val() > ctc) {
                    swal({
                        title : 'Adjust CTC',
                        text : "CTC is greater than sum of salary components",
                        type : 'warning',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        confirmButtonText : 'Change'
                    }, function() {
                        $("#ctc").parents(".form-group").addClass('has-error');
                        $("#ctc").focus();
                    });
                }
                else {
                    swal({
                        title : 'Saving',
                        html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                        allowOutsideClick : false,
                        showConfirmButton : false,
                        animation : false,
                        width : 300
                    });

                    $.ajax({
                        url: location.href,
                        type: 'POST',
                        data: $(this).serialize(),
                    })
                    .done(function(data) {
                        if (data.status == 200) {
                            swal({
                                title : "Saved !",
                                // text : data.message,
                                type : 'success',
                                confirmButtonClass : 'bg-theme',
                                animation : false,
                                width : 300,
                                timer: 1500
                            });
                            $("#viewCTC").text($("#ctc").val());
                            $editor.addClass('hidden');
                            $(".main .panel-body #view").removeClass('hidden');
                        }
                        else{  // Validation Error
                            displayError('Validation Error');
                            // show Errors
                            for(var fields in data) {
                                $('#'+fields).parents('.form-group').addClass('has-error');
                                $('#'+fields).parent().append('<p class="help-block">'+ data[fields][0] +'</p>');
                            }
                        }
                    })
                    .error(function() {
                        displayError('Try Again !');
                    });
                }
            }
        });


        $("#paymentMethod > form.form-horizontal").submit(function(event) {

            event.preventDefault();

            var $editor = $(this).parents(".editor");

            // remove all error messages
            $(".help-block").remove();
            $(".has-error").removeClass('has-error');
            
            swal({
                title : 'Saving',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 300
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: $('.main form').serialize(), 
            })
            .done(function(data) {
                if (data.status == 200) {
                    swal({
                        title : "Saved !",
                        // text : "data updated Successfully",
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        showConfirmButton: false,
                        animation : false,
                        width : 300,
                        timer: 1500
                    });

                    if ($("#modeOfPayment1").val() == 'cheque') {
                        console.log("cheque")
                        $("#viewModeOfPayment").text('Cash / Cheque')
                        $("#viewSharePercent1").parent().parent().addClass('hidden');    
                        $("#viewSharePercent2").parent().parent().addClass('hidden');    
                    }
                    else if(data.result && data.result1 === 1) {
                        console.log("direct deposit")
                        $("#viewSharePercent1, #viewSharePercent2").parent().parent().addClass('hidden');
                        $("#viewModeOfPayment").parent().parent().removeClass('hidden');
                        $("#viewModeOfPayment").text('Direct Deposit');
                    }
                    else if (data.result && data.result1 === true) {
                        console.log("direct deposit1 , direct deposit2")
                        $("#viewModeOfPayment").parent().parent().addClass('hidden');
                        $("#viewSharePercent1, #viewSharePercent2").parent().parent().removeClass('hidden');
                        $("#viewSharePercent1").text($("#sharePercent1").val());
                        $("#viewSharePercent2").text($("#sharePercent2").val());
                    }
                    else 
                        console.log("............");
                    $editor.addClass('hidden');
                    $(".main .panel-body #view").removeClass('hidden');
                }
                else{  // Validation Error
                    displayError();
                    // show Errors
                    for (var i = 0; i < data.length; i++) {
                        var currentData= data[i];
                        var $currentForm=$('.paymentForm:eq('+i+')');
                        for(var fields in currentData) {
                            $currentForm.find('.'+fields).parents('.form-group').addClass('has-error');
                            $currentForm.find('.'+fields).parent().append('<p class="help-block">'+ currentData[fields][0] +'</p>');
                        }
                    }
                }
            })
            .error(function() {
                displayError();
            });
        });


        // Payment Methods   ========================================================================

        $("#modeOfPayment1").change(function(event) {
            var val = $(this).val();
            if (val == 'cheque') {
                $("#cheque").removeClass('hidden');
                $("#directDeposit1 ,#directDeposit2, #paytm").find('input').attr('disabled', true);
                $("#directDeposit1, #directDeposit2, #paytm").addClass('hidden');
                $(".addAnother").hide();
            }
            else {
                $("#cheque").addClass('hidden');
                $("#directDeposit1").find('input').attr('disabled', false);
                // $("#sharePercent1").val(100);
                $("#directDeposit1").removeClass('hidden');
                $("#directDeposit1 input").trigger('change');
                $('.addAnother').siblings().children('.hidden').removeClass('hidden');
                $(".addAnother").show();
            }
        });

        $('#modeOfPayment1').trigger('change');

        $("#directDeposit2, #paytm").find('input').attr('disabled', true);

        $("#selectDeposit, #selectPaytm").click(function(event) {
            var $item = $('#'+$(this).data('id')); // form id
            $item.removeClass('hidden');
            $item.find('input').attr('disabled', false).trigger('change');
            $(this).parent().addClass('hidden');
            // incase of paytm
            // if ($('.addAnother').siblings().children('.hidden').length == 2) { 
            if ($('.addAnother').siblings().children('.hidden').length == 1) { 
                $(".addAnother").hide();
            }
        });

        <?php if(count($result3)>1):?>
        $("#selectDeposit").trigger('click');
        <?php endif; ?>

        $("#sharePercent2, #paytmSharePercent").change(function(event) {
            sharePercent1 = $("#sharePercent1").val()*1;
            sharePercent2 = $("#sharePercent2").val()*1;
            paytmSharePercent = $("#paytmSharePercent").val()*1 || 0;
            val = $(this).val()*1;

            var newSharePercent1 = 100 - (sharePercent2 + paytmSharePercent);
            if (newSharePercent1 < 1) {
                $("#sharePercent1").val(1);
                $(this).val(99 - (sharePercent2 + paytmSharePercent - val));
            }
            else
                $("#sharePercent1").val(newSharePercent1);
        });

        //   salary components  ======================================================================

        $("#ctc").change(function(event) {
            //event.preventDefault();
            var $val = $(this).val();

            if (!$("#basic").val()) 
                $("#basic").val(Math.round($val * 0.4));
            if (!$("#hra").val()) 
                $("#hra").val(Math.round($val * 0.2));
            if (!$("#conveyance").val()) 
                $("#conveyance").val(19200);
            if (!$("#medicalAllowance").val()) 
                $("#medicalAllowance").val(15000);
            if (!$("#others").val()) 
                $("#others").val(Math.max(0, $val-Math.round($val*0.6)-34200));


            $("#basic, #hra, #conveyance, #medicalAllowance, #others").trigger('change');
        });

        // handle conveyance max value
        $("#conveyance").change(function(event) {
            event.preventDefault();
            $(this).val( Math.min($(this).val(), 19200) );
        });
         // handle conveyance max value
        $("#medicalAllowance").change(function(event) {
            event.preventDefault();
            $(this).val( Math.min($(this).val(), 15000) );
        });

        $(".addMore").click(function(event) {
            event.preventDefault();
            $("#" + $(this).data('id')).parents(".hidden").removeClass('hidden');
            $(this).remove();

            if (!$(".addMore").length) {$(".addMoreButton").hide()};
        });
    });
</script>
