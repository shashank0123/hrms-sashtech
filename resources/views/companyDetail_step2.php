<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>


<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">

                        <div class="breadcrumb flat text-center row">
                            <span class="col-md-4 col-sm-4 hidden-xs">Location</span>
                            <span class="active col-md-4 col-sm-4 col-xs-12">Bank Detail</span>
                            <span class="col-md-4 col-sm-4 hidden-xs">Tax Details</span>
                        </div>

                        <div class="col-md-9 col-sm-12 col-xs-12">
                            <form class="form-horizontal">
                                <fieldset>
                                    <legend>Bank Details</legend>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating is-empty ">
                                                <label class="control-label" for="bankName"><span>Name of Bank</span></label>
                                                <input class="form-control" id="bankName" type="text" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating is-empty ">
                                                <label class="control-label" for="branch"><span>Branch</span></label>
                                                <input class="form-control" id="branch" type="text" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating is-empty ">
                                                <label class="control-label" for="acNumber"><span>A/c number</span></label>
                                                <input class="form-control" id="acNumber" type="text" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="typeOfAccount" class="col-md-2 control-label">Type of Account</label>

                                            <div class="col-md-10">
                                                <select id="typeOfAccount" class="form-control">
                                                    <option value="saving">Saving</option>
                                                    <option value="current">Current</option>
                                                    <option value="overDraft">OverDraft</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating is-empty ">
                                                <label class="control-label" for="ifsc"><span>IFSC</span></label>
                                                <input class="form-control" id="ifsc" type="text" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <div class="col-md-8 col-sm-8 col-xs-8 col-md-offset-4 col-sm-offset-3 col-xs-offset-4">
                                                <button type="button" class="btn btn-default btn-raised">Cancel</button>
                                                <button type="submit" class="btn bg-theme btn-raised">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>


                    </div>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>

</body>
</html>

<script>

    $(function() {
        $.material.init();
    });
</script>
