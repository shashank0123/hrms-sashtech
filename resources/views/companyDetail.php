<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>

<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">
                        <div id="view">
                            
                            <h3><?php echo session()->get('companyName'); ?></h3><hr>


                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <div class="form-group">
                                    <label for="companyCode" class="col-md-6 control-label">Company Code</label>

                                    <div class="col-md-6">
                                        <input type="text" id="companyCode" class="form-control" value="<?php echo isset($result->companyCode) ? $result->companyCode : ''; ?>" disabled>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-5 col-sm-5 col-xs-12">
                                <div class="extra">
                                    <div id="preview" >
                                        <div class="saving text-center hidden"><i class="fa fa-spinner fa-pulse"></i></div>
                                        <label for="companyLogo" <?php echo isset($result1->logoUrl) ? 'style="background-image: url(\'https://Sashtechs.com/'.$result1->logoUrl.'\');"' : ' '; ?>>CHOOSE FILE</label>
                                        <input type="file" accept="image/*" name="companyLogo" id="companyLogo" style="display:none;">
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <br>

                            <div class="col-md-12 col-sm-12 col-xs-12" id="locationContainer">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="detail">
                                        <h5>
                                            <b> &nbsp;&nbsp;<u>Head Office Location</u>
                                            <span class="pull-right edit btn btn-default" data-id="location"><i class="fa fa-pencil"></i></span></b>
                                            <div class="clearfix"></div>
                                            <input type="hidden" class="addressType" value="headOffice">
                                        </h5>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>Address</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 class="viewAddress">
                                            <?php echo (isset($result1->address)) ? $result1->address : ''; ?>
                                            </h6></div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>Property No</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 class="viewProperty">
                                                <?php echo (isset($result1->propertyNo)) ? $result1->propertyNo : ''; ?>
                                            </h6></div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>Street</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 class="viewStreet">
                                            <?php echo (isset($result1->street)) ? $result1->street : ''; ?>
                                            </h6></div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>City</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 class="viewCity">
                                            <?php echo (isset($result1->city)) ? $result1->city : ''; ?>
                                            </h6></div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>State</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 class="viewState">
                                            <?php 
                                                if (isset($result1->state)) 
                                                {
                                                    switch ($result1->state) 
                                                    {
                                                        case 'AN':
                                                            echo "Andaman and Nicobar Islands";
                                                            break;
                                                        case 'AP':
                                                            echo "Andhra Pradesh";
                                                            break;
                                                        case 'AR':
                                                            echo "Arunachal Pradesh";
                                                            break;
                                                        case 'AS':
                                                            echo "Assam";
                                                            break;
                                                        case 'BR':
                                                            echo "Bihar";
                                                            break;
                                                        case 'CH':
                                                            echo "Chandigarh";
                                                            break;
                                                        case 'CT':
                                                            echo "Chhattisgarh";
                                                            break;
                                                        case 'DN':
                                                            echo "Dadra and Nagar Haveli";
                                                            break;
                                                        case 'DD':
                                                            echo "Daman and Diu";
                                                            break;
                                                        case 'DL':
                                                            echo "Delhi";
                                                            break;
                                                        case 'GA':
                                                            echo "GOA";
                                                            break;
                                                        case 'GJ':
                                                            echo "Gujarat";
                                                            break;
                                                        case 'HR':
                                                            echo "Haryana";
                                                            break;
                                                        case 'HP':
                                                            echo "Himachal Pradesh";
                                                            break;
                                                        case 'JK':
                                                            echo "Jammu and Kashmir";
                                                            break;
                                                        case 'JH':
                                                            echo "Jharkhand";
                                                            break;
                                                        case 'KA':
                                                            echo "Karnataka";
                                                            break;
                                                        case 'KL':
                                                            echo "Kerala";
                                                            break;
                                                        case 'LD':
                                                            echo "Lakshadweep";
                                                            break;
                                                        case 'MP':
                                                            echo "Madhya Pradesh";
                                                            break;
                                                        case 'MH':
                                                            echo "Maharashtra";
                                                            break;
                                                        case 'MN':
                                                            echo "Manipur";
                                                            break;
                                                        case 'ML':
                                                            echo "Meghalaya";
                                                            break;
                                                        case 'MZ':
                                                            echo "Mizoram";
                                                            break;
                                                        case 'NL':
                                                            echo "Nagaland";
                                                            break;
                                                        case 'OR':
                                                            echo "Odisha";
                                                            break;
                                                        case 'PY':
                                                            echo "Puducherry";
                                                            break;
                                                        case 'PB':
                                                            echo "Punjab";
                                                            break;
                                                        case 'RJ':
                                                            echo "Rajasthan";
                                                            break;
                                                        case 'SK':
                                                            echo "Sikkim";
                                                            break;
                                                        case 'TN':
                                                            echo "Tamil Nadu";
                                                            break;
                                                        case 'TG':
                                                            echo "Telangana";
                                                            break;
                                                        case 'TR':
                                                            echo "Tripura";
                                                            break;
                                                        case 'UT':
                                                            echo "Uttarakhand";
                                                            break;
                                                        case 'UP':
                                                            echo "Uttar Pradesh";
                                                            break;
                                                        case 'WB':
                                                            echo "West Bengal";
                                                            break;
                                                    }
                                                }
                                            ?>
                                            </h6>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>Pin</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 class="viewPin">
                                            <?php echo (isset($result1->pin)) ? $result1->pin : ''; ?>
                                            </h6></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div> 
                                </div>
                                <?php $i=0; ?>
                                <?php foreach ($result2 as $key => $value): ?>
                                <?php if (isset($value->type)) : ?>
                                <?php $i++; ?>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="detail">
                                    
                                        <h5>
                                            <b> &nbsp;&nbsp;<u>Branch Office Location <?php echo $i; ?></u>
                                            <span class="pull-right edit btn btn-default" data-id="location"><i class="fa fa-pencil"></i></span></b>
                                            <div class="clearfix"></div>
                                            <input type="hidden" class="addressType" value="<?php echo $value->type; ?>">
                                        </h5>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>Address</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 class="viewAddress">
                                            <?php echo (isset($value->address)) ? $value->address : ''; ?>
                                            </h6></div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>Property No</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 class="viewProperty">
                                            <?php echo (isset($value->propertyNo)) ? $value->propertyNo : ''; ?>
                                            </h6></div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>Street</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 class="viewStreet">
                                            <?php echo (isset($value->street)) ? $value->street : ''; ?>
                                            </h6></div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>City</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 class="viewCity">
                                            <?php echo (isset($value->city)) ? $value->city : ''; ?>
                                            </h6></div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>State</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 class="viewState">
                                                <?php 
                                                if (isset($value->state)) 
                                                {
                                                    switch ($value->state) 
                                                    {
                                                        case 'AN':
                                                            echo "Andaman and Nicobar Islands";
                                                            break;
                                                        case 'AP':
                                                            echo "Andhra Pradesh";
                                                            break;
                                                        case 'AR':
                                                            echo "Arunachal Pradesh";
                                                            break;
                                                        case 'AS':
                                                            echo "Assam";
                                                            break;
                                                        case 'BR':
                                                            echo "Bihar";
                                                            break;
                                                        case 'CH':
                                                            echo "Chandigarh";
                                                            break;
                                                        case 'CT':
                                                            echo "Chhattisgarh";
                                                            break;
                                                        case 'DN':
                                                            echo "Dadra and Nagar Haveli";
                                                            break;
                                                        case 'DD':
                                                            echo "Daman and Diu";
                                                            break;
                                                        case 'DL':
                                                            echo "Delhi";
                                                            break;
                                                        case 'GA':
                                                            echo "GOA";
                                                            break;
                                                        case 'GJ':
                                                            echo "Gujarat";
                                                            break;
                                                        case 'HR':
                                                            echo "Haryana";
                                                            break;
                                                        case 'HP':
                                                            echo "Himachal Pradesh";
                                                            break;
                                                        case 'JK':
                                                            echo "Jammu and Kashmir";
                                                            break;
                                                        case 'JH':
                                                            echo "Jharkhand";
                                                            break;
                                                        case 'KA':
                                                            echo "Karnataka";
                                                            break;
                                                        case 'KL':
                                                            echo "Kerala";
                                                            break;
                                                        case 'LD':
                                                            echo "Lakshadweep";
                                                            break;
                                                        case 'MP':
                                                            echo "Madhya Pradesh";
                                                            break;
                                                        case 'MH':
                                                            echo "Maharashtra";
                                                            break;
                                                        case 'MN':
                                                            echo "Manipur";
                                                            break;
                                                        case 'ML':
                                                            echo "Meghalaya";
                                                            break;
                                                        case 'MZ':
                                                            echo "Mizoram";
                                                            break;
                                                        case 'NL':
                                                            echo "Nagaland";
                                                            break;
                                                        case 'OR':
                                                            echo "Odisha";
                                                            break;
                                                        case 'PY':
                                                            echo "Puducherry";
                                                            break;
                                                        case 'PB':
                                                            echo "Punjab";
                                                            break;
                                                        case 'RJ':
                                                            echo "Rajasthan";
                                                            break;
                                                        case 'SK':
                                                            echo "Sikkim";
                                                            break;
                                                        case 'TN':
                                                            echo "Tamil Nadu";
                                                            break;
                                                        case 'TG':
                                                            echo "Telangana";
                                                            break;
                                                        case 'TR':
                                                            echo "Tripura";
                                                            break;
                                                        case 'UT':
                                                            echo "Uttarakhand";
                                                            break;
                                                        case 'UP':
                                                            echo "Uttar Pradesh";
                                                            break;
                                                        case 'WB':
                                                            echo "West Bengal";
                                                            break;
                                                    }
                                                }
                                            ?>
                                            </h6></div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>Pin</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 class="viewPin">
                                                <?php echo (isset($value->pin)) ? $value->pin : ''; ?>
                                            </h6></div>
                                        </div>
                                        <span class="pull-right delete btn btn-default"><i class="fa fa-times"></i></span>
                                        <div class="clearfix"></div>
                                    </div> 
                                </div>
                                <?php endif; ?>
                                <?php endforeach; ?>

                            </div>
        
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <button id="addMore" class="btn pull-right"><i class="fa fa-plus"></i> Add Branch Office</button>
                                </div>
                            </div>
                            
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="detail bankDetails">
                                    
                                        <h5>
                                            <i class="fa fa-bank"> </i><b>&nbsp;&nbsp;<u>Bank Details</u>
                                            <span class="pull-right edit btn btn-default" data-id="bankDetail"><i class="fa fa-pencil"></i></span></b>
                                            <div class="clearfix"></div>
                                        </h5>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>Name of Bank</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 id="viewNameOfBank">
                                            <?php echo (isset($result3->bankName)) ? $result3->bankName : ' ';?>
                                            </h6></div>
                                        </div><!-- 
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>Branch</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 id="viewBranch">
                                            <?php // echo (isset($result3->bankName)) ? $result3->bankName : ' ';?>
                                            </h6></div>
                                        </div> -->
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>A/c no</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 id="viewAccountNumber">
                                            <?php echo (isset($result3->accountNumber)) ? $result3->accountNumber : ' ';?>
                                            </h6></div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>Type</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 id="viewType"> 
                                            <?php echo (isset($result3->accountType)) ? $result3->accountType : ' ';?>
                                            </h6></div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>IFSC</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 id="viewIFSC">
                                            <?php echo (isset($result3->ifsc)) ? $result3->ifsc : ' ';?>
                                            </h6></div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>MICR</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 id="viewMICR">
                                            <?php echo (isset($result3->micr)) ? $result3->micr : ' ';?>
                                            </h6></div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>Login Id</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 id="viewLoginId">
                                            <?php echo (isset($result3->loginid)) ? $result3->loginid : ' ';?>
                                            </h6></div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>Bank URL</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 id="viewBankUrl">
                                            <?php echo (isset($result3->bankURL)) ? $result3->bankURL : ' ';?>
                                            </h6></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div> 
                                </div>

                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="detail taxDetails">
                                    
                                        <h5>
                                            <i class="fa fa-inr"> </i><b> &nbsp;&nbsp;<u>Tax Details</u>
                                            <span class="pull-right edit btn btn-default" data-id="taxDetail"><i class="fa fa-pencil"></i></span></b>
                                            <div class="clearfix"></div>
                                        </h5>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>PAN</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 id="viewPAN">
                                            <?php echo (!empty($result4->pan)) ? strtoupper($result4->pan) : '' ; ?>
                                            </h6></div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>TAN</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 id="viewTAN">
                                            <?php echo (!empty($result4->tan)) ? strtoupper($result4->tan) : '' ; ?>
                                            </h6></div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>EPF</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 id="viewEPF">
                                            <?php echo (!empty($result4->epf) and (strtolower($result4->epf) == 'on') ) ? 'Yes' : 'No' ; ?>
                                            </h6></div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>ESI</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 id="viewESI">
                                            <?php echo (!empty($result4->esi) and (strtolower($result4->esi) == 'on') ) ? 'Yes' : 'No' ; ?>
                                            </h6></div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>Gratuity</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 id="viewGratuity">
                                            <?php echo (!empty($result4->gratuity) and (strtolower($result4->gratuity) == 'on') ) ? 'Yes' : 'No' ; ?>
                                            </h6></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div> 
                                </div>

                                <!-- <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="detail">

                                        <h5>
                                            <i class="fa fa-legal"> </i><b> &nbsp;&nbsp; <u>Company Signatory</u></b>
                                            <div class="clearfix"></div>
                                        </h5>

                                        <div class="clearfix"></div>
                                    </div>
                                </div> -->
                            </div>

                            
                            <div class="clearfix"></div>
                            <br>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <a href="/billingHistory" class="ajaxify"><h4>Billing History</h4></a>
                                <a href="/companyPolicy" class="ajaxify"><h4>Add Company Policy</h4></a>
                                <a href="/setupLeave" class="ajaxify"><h4>Add Leaves</h4></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>


                        <!-- ====================================== Editors  ========================================== -->

                        <div class="editor hidden" id="location">
                            <form action="#" method="POST" class="form-horizontal">
                                <fieldset>
                                    <div class="clearfix"></div>
                                    <br>
                                    <legend>Edit Location</legend>
                                    <!-- identify type of form -->
                                    <input type="hidden" name="area" value="address">
                                    <input type="hidden" name="type" value="edit">
                                    <input type="hidden" name="addressType" id="addressType" value="">

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-9 col-sm-10 col-xs-12">
                                            <div class="form-group label-floating">
                                                <label class="control-label" for="address"><span>Address</span></label>
                                                <input class="form-control" id="address" name="address" type="text" style="cursor: auto;" placeholder=" ">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-9 col-sm-10 col-xs-12">
                                            <div class="form-group label-floating is-empty ">
                                                <label class="control-label" for="propertyNo"><span>Property No. / Description</span></label>
                                                <input class="form-control" id="propertyNo" name="propertyNo" type="text" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-9 col-sm-10 col-xs-12">
                                            <div class="form-group label-floating is-empty ">
                                                <label class="control-label" for="street"><span>Street</span></label>
                                                <input class="form-control" id="street" name="street" type="text" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-9 col-sm-10 col-xs-12">
                                            <div class="form-group label-floating is-empty ">
                                                <label class="control-label" for="city"><span>City</span></label>
                                                <input class="form-control" id="city" name="city" type="text" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="state" class="col-md-2 control-label">State</label>

                                            <div class="col-md-7">
                                                <select id="state" name="state" class="form-control">
                                                    <option value="0">Select State</option>
                                                    <option value="AN">Andaman and Nicobar Islands</option>
                                                    <option value="AP">Andhra Pradesh</option>
                                                    <option value="AR">Arunachal Pradesh</option>
                                                    <option value="AS">Assam</option>
                                                    <option value="BR">Bihar</option>
                                                    <option value="CH">Chandigarh</option>
                                                    <option value="CT">Chhattisgarh</option>
                                                    <option value="DN">Dadra and Nagar Haveli</option>
                                                    <option value="DD">Daman and Diu</option>
                                                    <option value="DL">Delhi</option>
                                                    <option value="GA">GOA</option>
                                                    <option value="GJ">Gujarat</option>
                                                    <option value="HR">Haryana</option>
                                                    <option value="HP">Himachal Pradesh</option>
                                                    <option value="JK">Jammu and Kashmir</option>
                                                    <option value="JH">Jharkhand</option>
                                                    <option value="KA">Karnataka</option>
                                                    <option value="KL">Kerala</option>
                                                    <option value="LD">Lakshadweep</option>
                                                    <option value="MP">Madhya Pradesh</option>
                                                    <option value="MH">Maharashtra</option>
                                                    <option value="MN">Manipur</option>
                                                    <option value="ML">Meghalaya</option>
                                                    <option value="MZ">Mizoram</option>
                                                    <option value="NL">Nagaland</option>
                                                    <option value="OR">Odisha</option>
                                                    <option value="PY">Puducherry</option>
                                                    <option value="PB">Punjab</option>
                                                    <option value="RJ">Rajasthan</option>
                                                    <option value="SK">Sikkim</option>
                                                    <option value="TN">Tamil Nadu</option>
                                                    <option value="TG">Telangana</option>
                                                    <option value="TR">Tripura</option>
                                                    <option value="UT">Uttarakhand</option>
                                                    <option value="UP">Uttar Pradesh</option>
                                                    <option value="WB">West Bengal</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-9 col-sm-10 col-xs-12">
                                            <div class="form-group label-floating is-empty ">
                                                <label class="control-label" for="pin"><span>Pin</span></label>
                                                <input class="form-control" id="pin" name="pin" type="text" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="reset" class="btn btn-raised btn-default cancel" value="Cancel">
                                        <input type="submit" class="btn btn-raised bg-theme save" value="Save">
                                    </div>

                                </fieldset>
                            </form>
                        </div>

                        <div class="editor hidden" id="bankDetail">
                            <form action="#" method="POST" class="form-horizontal">
                                <fieldset>
                                    <div class="clearfix"></div>
                                    <br>
                                    <legend>Edit Bank Detail</legend>
                                    <!-- identify type of form -->
                                    <input type="hidden" name="area" value="bank">

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-9 col-sm-10 col-xs-12">
                                            <div class="form-group label-floating">
                                                <label class="control-label" for="nameOfBank"><span>Name Of Bank</span></label>
                                                <input class="form-control" id="nameOfBank" name="nameOfBank" type="text" value="" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <!-- <div class="col-md-9 col-sm-10 col-xs-12">
                                        <div class="form-group">
                                            <label for="branch" class="col-md-2 control-label">Branch</label>

                                            <div class="col-md-10">
                                                <select id="branch" name="branch" class="form-control">
                                                    <option value="abc">abc</option>
                                                    <option value="xyz">xyz</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div> -->

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-9 col-sm-10 col-xs-12">
                                            <div class="form-group label-floating">
                                                <label class="control-label" for="accountNumber"><span>Account Number</span></label>
                                                <input class="form-control" id="accountNumber" name="accountNumber" type="text" value="" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="type" class="col-md-2 control-label">Type</label>

                                            <div class="col-md-7">
                                                <select id="type" name="type" class="form-control">
                                                    <option value="0">Select Account Type</option>
                                                    <option value="saving">Saving</option>
                                                    <option value="current">Current</option>
                                                    <option value="overDraft"> Over Draft</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-9 col-sm-10 col-xs-12">
                                            <div class="form-group label-floating">
                                                <label class="control-label" for="ifsc"><span>IFSC</span></label>
                                                <input class="form-control" maxlength="11" id="ifsc" name="ifsc" type="text" value="" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-9 col-sm-10 col-xs-12">
                                            <div class="form-group label-floating">
                                                <label class="control-label" for="micr"><span>MICR</span></label>
                                                <input class="form-control" id="micr" name="micr" type="text" value="" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-9 col-sm-10 col-xs-12">
                                            <div class="form-group label-floating">
                                                <label class="control-label" for="loginid"><span>Login Id</span></label>
                                                <input class="form-control" id="loginid" name="loginid" type="text" value="" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-9 col-sm-10 col-xs-12">
                                            <div class="form-group label-floating">
                                                <label class="control-label" for="bankURL"><span>Bank URL</span></label>
                                                <input class="form-control" id="bankURL" name="bankURL" type="text" value="" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="reset" class="btn btn-raised btn-default cancel" value="Cancel">
                                        <input type="submit" class="btn btn-raised bg-theme save" value="Save">
                                    </div>

                                </fieldset>
                            </form>
                        </div>

                        <div class="editor hidden" id="taxDetail">
                            <form action="#" method="POST" class="form-horizontal">
                                <fieldset>
                                    <div class="clearfix"></div>
                                    <br>
                                    <legend>Edit Tax Details </legend>
                                    <!-- identify type of form -->
                                    <input type="hidden" name="area" value="tax">

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-9 col-sm-10 col-xs-12">
                                            <div class="form-group label-floating">
                                                <label class="control-label" for="pan"><span>PAN</span></label>
                                                <input class="form-control" id="pan" maxlength="10" name="pan" type="text" value="" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-9 col-sm-10 col-xs-12">
                                            <div class="form-group label-floating">
                                                <label class="control-label" for="tan"><span>TAN</span></label>
                                                <input class="form-control" maxlength="10" id="tan" name="tan" type="text" value="" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-9 col-sm-10 col-xs-12">
                                            <div class="togglebutton form-group">
                                                <label>EPF
                                                    &nbsp;&nbsp;&nbsp; 
                                                    <input type="checkbox" id="epf" name="epf">
                                                </label>
                                            </div>
                                        </div>
                                    </div>  

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <div class="togglebutton form-group">
                                                <label>ESI
                                                    &nbsp;&nbsp;&nbsp; 
                                                    <input type="checkbox" id="esi" name="esi">
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                            <div class="togglebutton form-group">
                                                <label>Gratuity
                                                    &nbsp;&nbsp;&nbsp; 
                                                    <input type="checkbox" id="gratuity" name="gratuity">
                                                </label>
                                            </div>
                                        </div>
                                    </div>  

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="reset" class="btn btn-raised btn-default cancel" value="Cancel">
                                        <input type="submit" class="btn btn-raised bg-theme save" value="Save">
                                    </div>

                                </fieldset>
                            </form>
                        </div>

                        <br>
                        <br>
                    </div>

                    <style>
                        .extra{
                            border:none !important;
                            width: 12em;
                            margin: 1em auto;
                        }
                        #preview{
                            background: #fff;
                            margin: -10px;
                            position: relative;
                            height: 10em;
                            border: 1px solid #aaa;
                        }

                        #preview > label{
                            margin: 0 auto;
                            display: block;
                            width: 10em;
                            height: 100%;
                            text-align: center;
                            line-height: 9em;
                            background-size: contain;
                            background-repeat: no-repeat;
                            background-position: center;
                        }

                        #preview .saving {
                            position: absolute;
                            left: 0;
                            top: 0;
                            width: 100%;
                            height: 100%;
                            background: #FFF;
                            font-size: 25px;
                            line-height: 5;
                        }
                    
                        #popup {
                            display: none;
                            position: fixed;
                            width: 100%;
                            height: 100%;
                            background: #fff;
                            top: 0;
                            left: 0;
                            z-index: 10000;
                            padding: 1em;
                        }
                        .detail{
                            box-shadow: 0 0 2px #888;
                            padding: 15px;
                            margin-top: 15px;
                            overflow-x: overlay;
                        }
                        .editor {
                            position: relative;
                            padding: 2em 0;
                        }
                        .edit ,.delete{
                            margin: -15px;
                        }
                        #locationContainer div:nth-child(odd) {
                            clear: both;
                        }
                    </style>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>

</body>
</html>

<script>

        var placeSearch, autocomplete;

        function initAutocomplete() {
            // Create the autocomplete object, restricting the search to geographical
            // location types.
            autocomplete = new google.maps.places.Autocomplete(
                /** @type {!HTMLInputElement} */(document.getElementById('address')),
                {types: ['geocode']});

            // When the user selects an address from the dropdown, populate the address
            // fields in the form.
            autocomplete.addListener('place_changed', fillInAddress);
        }

        function fillInAddress() {
            // Get the place details from the autocomplete object.
            var place = autocomplete.getPlace();
            var componentForm = {
                street_number: 'propertyNo',
                route: 'street',
                // locality: 'street',
                administrative_area_level_1: 'state',
                administrative_area_level_2: 'city',
                // country: 'country',
                postal_code: 'pin'
            };

            for (var component in componentForm) {
                document.getElementById(componentForm[component]).value = '';
                document.getElementById(componentForm[component]).disabled = false;
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {
                    var val = place.address_components[i]['long_name'];
                    if (addressType=="administrative_area_level_1") {val = place.address_components[i]['short_name'];}
                    document.getElementById(componentForm[addressType]).value = val;
                    $("#"+componentForm[addressType]).trigger('change')
                }
            }
        }

          // Bias the autocomplete object to the user's geographical location,
          // as supplied by the browser's 'navigator.geolocation' object.
        function geolocate() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var geolocation = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    var circle = new google.maps.Circle({
                        center: geolocation,
                        radius: position.coords.accuracy
                    });
                    autocomplete.setBounds(circle.getBounds());
                });
            }
        }


    function displayError(msg) {
        swal({
            title : "Error !",
            text : msg,
            type : 'error',
            confirmButtonClass : 'bg-theme',
            animation : false,
            width : 400
        });
    };

    function closeAlert() {
        $(".sweet-alert button").first().trigger('click');
    }
                        

    $(function() {

        // add active class to corresponding link in menu 
        $(".nav li.active").removeClass('active');
        $(".nav.nav-stacked li").filter(function(index) {
            return $(this).data('url') == 'companyDetail';
        }).addClass('active');

        var $locationDetail = null;

        $.material.init();

        $("form.form-horizontal").keydown(function(event) {
            if (event.keyCode == 27) {
                $(this).find('.cancel').trigger('click');
            }
        });

        $("form.form-horizontal .cancel").click(function(event) {
            var $inputs = $(this).parent().parent().find('.form-group');
            $inputs.each(function(index, el) {
                if (!$(el).find('input').val()) {
                    $(el).removeClass('is-empty');
                }
                $(el).removeClass('has-error');
                $(el).find('help-block').remove();
            });

            $(this).parents(".editor").addClass('hidden');
            $(".main .panel-body #view").removeClass('hidden');
        });

        $(".edit").click(handleEdit);

        function handleEdit() {
            $("#"+$(this).data('id')).removeClass('hidden');

            $(".main .panel-body #view").addClass('hidden');

            (function smoothscroll(){
                var currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
                if (currentScroll > 0) {
                     window.requestAnimationFrame(smoothscroll);
                     window.scrollTo (0,currentScroll - (currentScroll/5));
                }
            })();

            if ($(this).data('id') == 'location') {
                $locationDetail = $(this).parents('.detail');

                var $form = $("#location .form-horizontal");

                $form.find('#addressType').val($locationDetail.find('.addressType').val().trim());
                $form.find('#address').val($locationDetail.find('.viewAddress').text().trim()).trigger('change');
                $form.find('#propertyNo').val($locationDetail.find('.viewProperty').text().trim()).trigger('change');
                $form.find('#street').val($locationDetail.find('.viewStreet').text().trim()).trigger('change');
                $form.find('#city').val($locationDetail.find('.viewCity').text().trim()).trigger('change');
                $form.find('#state option').filter(function() {
                    if($(this).text() == $locationDetail.find('.viewState').text().trim()) 
                    {
                        $(this).prop('selected', true);
                    }
                });
                $form.find('#pin').val($locationDetail.find('.viewPin').text().trim()).trigger('change');
            }
            else
                $locationDetail = null;

            if ($(this).data('id') == 'bankDetail') {
                $bankDetail = $(this).parents('.detail');

                var $form = $("#bankDetail .form-horizontal");

                $form.find('#nameOfBank').val($bankDetail.find('#viewNameOfBank').text().trim()).trigger('change');
                $form.find('#accountNumber').val($bankDetail.find('#viewAccountNumber').text().trim()).trigger('change');
                $form.find('#type').val($bankDetail.find('#viewType').text().trim()).trigger('change');
                $form.find('#title option').filter(function() {
                    return $(this).val() == $bankDetail.find('#viewType').text().trim();
                }).prop('selected', true);
                $form.find('#ifsc').val($bankDetail.find('#viewIFSC').text().trim()).trigger('change');
                $form.find('#micr').val($bankDetail.find('#viewMICR').text().trim()).trigger('change');
                $form.find('#loginid').val($bankDetail.find('#viewLoginId').text().trim()).trigger('change');
                $form.find('#bankURL').val($bankDetail.find('#viewBankUrl').text().trim()).trigger('change');
            }
            else
                $bankDetail = null;

            if ($(this).data('id') == 'taxDetail') {
                $taxDetail = $(this).parents('.detail');

                var $form = $("#taxDetail .form-horizontal");

                $form.find('#pan').val($taxDetail.find('#viewPAN').text().trim()).trigger('change');
                $form.find('#tan').val($taxDetail.find('#viewTAN').text().trim()).trigger('change');
                $form.find('#epf').prop('checked', $taxDetail.find('#viewEPF').text().trim() == 'Yes').trigger('change');
                $form.find('#esi').prop('checked', $taxDetail.find('#viewESI').text().trim() == 'Yes').trigger('change');
                $form.find('#gratuity').prop('checked', $taxDetail.find('#viewGratuity').text().trim() == 'Yes').trigger('change');
            }
            else
                $taxDetail = null;
        }

        $("#location > form.form-horizontal").submit(function(event) {
            event.preventDefault();

            var $editor = $(this).parent(".editor");
            $(this).find(".help-block").remove();
            $(this).find(".has-error").removeClass('has-error');
            
            swal({
                title : 'Saving',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 400
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: $(this).serialize(),
            })
            .done(function(data) {
                if (data.status == 200) {
                    swal({
                        title : "Success !",
                        text : data.message,
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });
                    $locationDetail.find('.viewAddress').text($('#address').val());
                    $locationDetail.find('.viewProperty').text($('#propertyNo').val());
                    $locationDetail.find('.viewStreet').text($('#street').val());
                    $locationDetail.find('.viewCity').text($('#city').val());
                    $locationDetail.find('.viewState').text($('#state option:selected').text());
                    $locationDetail.find('.viewPin').text($('#pin').val());
                    $editor.addClass('hidden');
                    $(".main .panel-body #view").removeClass('hidden');
                }
                else{  // Validation Error
                    displayError('Validation Error');
                    // show Errors
                    for(var fields in data) {
                        $('#'+fields).parents('.form-group').addClass('has-error');
                        $('#'+fields).parent().append('<p class="help-block">'+ data[fields][0] +'</p>');
                    }
                }
            })
            .error(function() {
                displayError('Try Again !');
            });
        });

        $("#taxDetail > form.form-horizontal").submit(function(event) {
            event.preventDefault();

            var $editor = $(this).parent(".editor");
            $(this).find(".help-block").remove();
            $(this).find(".has-error").removeClass('has-error');
            
            swal({
                title : 'Saving',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 400
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: $(this).serialize(),
            })
            .done(function(data) {
                if (data.status == 200) {
                    swal({
                        title : "Success !",
                        text : data.message,
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });
                    $("#viewPAN").text($("#pan").val());
                    $("#viewTAN").text($("#tan").val());
                    $("#viewEPF").text($('#epf').is(':checked') ? 'Yes' : 'No' );
                    $("#viewESI").text($('#esi').is(':checked') ? 'Yes' : 'No' );
                    $("#viewGratuity").text($('#gratuity').is(':checked') ? 'Yes' : 'No' );
                    $editor.addClass('hidden');
                    $(".main .panel-body #view").removeClass('hidden');
                }
                else{  // Validation Error
                    displayError('Validation Error');
                    // show Errors
                    for(var fields in data) {
                        $('#'+fields).parents('.form-group').addClass('has-error');
                        $('#'+fields).parent().append('<p class="help-block">'+ data[fields][0] +'</p>');
                    }
                }
            })
            .error(function() {
                displayError('Try Again !');
            });
        });

        $("#bankDetail > form.form-horizontal").submit(function(event) {
            event.preventDefault();

            var $editor = $(this).parent(".editor");
            $(this).find(".help-block").remove();
            $(this).find(".has-error").removeClass('has-error');
            
            swal({
                title : 'Saving',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 400
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: $(this).serialize(),
            })
            .done(function(data) {
                if (data.status == 200) {
                    swal({
                        title : "Success !",
                        text : data.message,
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });   
                    $("#viewNameOfBank").text($("#nameOfBank").val());
                    $("#viewAccountNumber").text($("#accountNumber").val());
                    $("#viewType").text($("#type").val());
                    $("#viewIFSC").text($("#ifsc").val());
                    $("#viewMICR").text($("#micr").val());
                    $("#viewLoginId").text($("#loginid").val());
                    $("#viewBankUrl").text($("#bankURL").val());
                    $editor.addClass('hidden');
                    $(".main .panel-body #view").removeClass('hidden');
                }
                else{  // Validation Error
                    displayError('Validation Error');
                    // show Errors
                    for(var fields in data) {
                        $('#'+fields).parents('.form-group').addClass('has-error');
                        $('#'+fields).parent().append('<p class="help-block">'+ data[fields][0] +'</p>');
                    }
                }
            })
            .error(function() {
                displayError('Try Again !');
            });
        });

        $(".detail .delete").click(function(event) {
           $locationDetail = $(this).parents('.detail');
           $locationDetail.find('.addressType').val();

           swal({
                title : 'Deleting',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 400
            });

           $.ajax({
               url: location.href,
               type: 'POST',
               data: {type: 'delete', addressType : $locationDetail.find('.addressType').val()},
           })
           .done(function(data) {

                if (data.status == 200) {
                    console.log("location deleted");
                    $locationDetail.parent().remove();
                    $locationDetail = null;
                    closeAlert();
                }
                else
                    displayError("Try Again!")

           })
           .fail(function() {
                displayError("Try Again!");
           })
        });

        function addBranchOffice() {
            $addressType = $('.addressType:last').val();

            if ($addressType == "headOffice") 
            {
                $branch = 'branchOffice';
            }
            else
            {
                $number = $addressType.split('branchOffice')[1];
                $number = parseFloat($number);
                $branch = 'branchOffice' + ($number+1);
            }


            var $html = $('' + 
            '<div class="col-md-6 col-sm-12 col-xs-12">'+
                '<div class="detail">'+
                    '<h5>'+
                        '<b> &nbsp;&nbsp;<u>Branch Office Location</u>'+
                        '<span class="pull-right edit btn btn-default" data-id="location"><i class="fa fa-pencil"></i></span></b>'+
                        '<div class="clearfix"></div>'+
                        '<input type="hidden" class="addressType" value="'+ $branch +'">'+
                    '</h5>'+
                    '<div class="col-md-12 col-sm-12 col-xs-12">'+
                        '<div class="col-md-6 col-sm-6 col-xs-6"><h6>Address</h6></div>'+
                        '<div class="col-md-6 col-sm-6 col-xs-6"><h6 class="viewAddress"></h6></div>'+
                    '</div>'+
                    '<div class="col-md-12 col-sm-12 col-xs-12">'+
                        '<div class="col-md-6 col-sm-6 col-xs-6"><h6>Property No</h6></div>'+
                        '<div class="col-md-6 col-sm-6 col-xs-6"><h6 class="viewProperty"></h6></div>'+
                    '</div>'+
                    '<div class="col-md-12 col-sm-12 col-xs-12">'+
                        '<div class="col-md-6 col-sm-6 col-xs-6"><h6>Street</h6></div>'+
                        '<div class="col-md-6 col-sm-6 col-xs-6"><h6 class="viewStreet"></h6></div>'+
                    '</div>'+
                    '<div class="col-md-12 col-sm-12 col-xs-12">'+
                        '<div class="col-md-6 col-sm-6 col-xs-6"><h6>City</h6></div>'+
                        '<div class="col-md-6 col-sm-6 col-xs-6"><h6 class="viewCity"></h6></div>'+
                    '</div>'+
                    '<div class="col-md-12 col-sm-12 col-xs-12">'+
                        '<div class="col-md-6 col-sm-6 col-xs-6"><h6>State</h6></div>'+
                        '<div class="col-md-6 col-sm-6 col-xs-6"><h6 class="viewState"></h6></div>'+
                    '</div>'+
                    '<div class="col-md-12 col-sm-12 col-xs-12">'+
                        '<div class="col-md-6 col-sm-6 col-xs-6"><h6>Pin</h6></div>'+
                        '<div class="col-md-6 col-sm-6 col-xs-6"><h6 class="viewPin"></h6></div>'+
                    '</div>'+
                    '<div class="clearfix"></div>'+
                '</div> '+
            '</div>');

            $html.find('.edit').click(handleEdit);

            $("#locationContainer").append($html)
        }

        $("#addMore").click(function(event) {
            addBranchOffice();
        });

        $("#address").focus(function() {
            geolocate();
        });

        (function () {
            if (typeof(google)=="undefined") {
                var script = document.createElement("script");
                script.type = "text/javascript";
                script.src = "https://maps.googleapis.com/maps/api/js?signed_in=false&libraries=places&components=country:IN";
                document.body.appendChild(script);
            }
            var done = function() {
                if (typeof(google)=="object") {
                    initAutocomplete();
                    clearTimeout(done);
                }
                else {
                    setTimeout(done, 100);
                }
            }
            done();
            // setTimeout(initAutocomplete, 500);
        })();
        
        $("#companyLogo").on('change', function(event) {

            if (event.target.files.length) {
                var file = event.target.files[0];
                if (file && file.size > (500*1024)) {// under 500kb file is allowed
                    swal({
                        title : "Error !",
                        text : 'File Size is more than 500 KB',
                        type : 'error',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });
                    return false;                    
                }

                if (file && file.type != 'image/png' && file.type != 'image/jpeg') {
                    swal({
                        title : "Error !",
                        text : 'only png and jpeg images are allowed',
                        type : 'error',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });
                    return false; 
                }
                var formData = new FormData();
                formData.append('companyLogo', file);
                var $preview = $("#preview");

                $(this).siblings('.saving').removeClass('hidden');
                $("#preview").css('border', '1px solid #aaa');

                $.ajax({
                    url: location.href,
                    type: 'POST',
                    data: formData,
                    enctype: 'multipart/form-data',
                    processData: false,
                    contentType: false  
                })
                .done(function(data) {
                    if (data.status == 200) {
                        $preview.find('.saving').addClass('hidden');
                        $("#preview > label").css({
                            backgroundImage: 'url('+URL.createObjectURL(event.target.files[0])+')',
                            color: 'transparent',
                            'border' : '1px dotted #ddd'
                        });
                        $("#preview").css('border', 'none');
                    }
                    else {
                        $preview.find('.saving').addClass('hidden');
                        $("#preview > label").css({
                            backgroundImage : 'url()',
                            color : '#bdbdbd',
                            border : 'none'
                        });
                        $("#preview").css({
                            border: '1px solid #aaa',
                        });
                    }
                });
            }
        });
    });
</script>
