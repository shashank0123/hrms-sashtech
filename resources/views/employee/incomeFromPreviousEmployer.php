<!DOCTYPE html>
<html>
<head>
    <title>Income received from previous employer</title>
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
    <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet"></link>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Material Design -->
    <link href="css/bootstrap-material-design.css" rel="stylesheet">
    <link href="css/ripples.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <link rel="stylesheet" href="css/sweetalert2.css">

    <script src="js/jquery-1.12.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ripples.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/sweetalert2.min.js"></script>


    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

</head>

<style>
    .section{
        width:70em;
        padding: 1em;
        margin: 0 auto;
    }
</style>
<body  style="margin-top:1em;background:#fff">
    <div class="section">
        <div class="h4 text-center">
            <a href="/employeeDeduction" class="btn btn-raised bg-theme pull-left">back</a>
            Income received from previous employer <br> <span class="h6"></span>
        </div>
        <form method="post" enctype="multipart/form-data" class="form-horizontal">
            
            <table class="table table-hover">
                <tr>
                    <th>Particulars</th>
                    <th>Claimed Amount</th>
                    <th>Submit Proof</th>
                    <th></th>
                </tr>
                <tr>
                    <td><span class="h6">Income received from previous employer</span></td>
                    <td><span class="h6"><input name="income_amount" id="income_amount" value="<?php echo (isset($result->income)) ? $result->income : '' ;?>" type="number"></span></td>
                    <td><span class="h6"><input name="income_file" id="income_file" type="file"></span></td>
                    <td class="income"></td>
                </tr>
                <tr>
                    <td><span class="h6">PF or any other sum deducted by previous employer</span></td>
                    <td><span class="h6"><input name="PF_amount" id="PF_amount" value="<?php echo (isset($result->pf)) ? $result->pf : '' ;?>" type="number"></span></td>
                    <td><span class="h6"><input name="PF_file" id="PF_file" type="file"></span></td>
                    <td class="pf"></td>
                </tr>
                <tr>
                    <td><span class="h6">TDS deducted by previous employer </span></td>
                    <td><span class="h6"><input name="TDS_amount" id="TDS_amount" value="<?php echo (isset($result->tds)) ? $result->tds : '' ;?>" type="number"></span></td>
                    <td><span class="h6"><input name="TDS_file" id="TDS_file" type="file"></span></td>
                    <td class="tds"></td>
                </tr>
            </table>

            <input type="submit" class="btn bg-theme" value="Submit">
        </form>
    </div>
</body>
</html>

<script>

    function displayError() {
        swal({
            title : "Error !",
            text : 'Try Again!',
            type : 'error',
            confirmButtonClass : 'bg-theme',
            animation : false,
            width : 400
        });
    };

    function closeAlert() {
        $(".sweet-alert button").first().trigger('click');
    }

    $(function() {
        $.material.init();
        
        function generateData() {
            var formData = new FormData();

            formData.append('income_amount', $('#income_amount').val() || "");
            formData.append('PF_amount', $('#PF_amount').val() || "");
            formData.append('TDS_amount', $('#TDS_amount').val() || "");

            formData.append('income_file', $('#income_file')[0].files[0] || "");
            formData.append('PF_file', $('#PF_file')[0].files[0] || "");
            formData.append('TDS_file', $('#TDS_file')[0].files[0] || "");

            return formData;
        }

        $("form.form-horizontal").on('submit', function(event) {

            event.preventDefault();

            // remove all error messages
            $(".help-block").remove();
            
            swal({
                title : 'Saving',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 400
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: generateData(),
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false   
            })
            .done(function(data) {
                // console.log(data
                if (data.status == 200) {
                    swal({
                        title : "Saved !",
                        type : 'success',
                        showConfirmButton : false,
                        animation : false,
                        width : 300,
                        timer : 1500
                    });   
                }
                else{  // Validation Error
                    displayError();
                    // show Errors
                    for(var fields in data) {
                        $('.'+fields).html('<p class="help-block">'+ data[fields] +'</p>');
                    }
                }
            })
            .error(function() {
                displayError();
            });
        });
    })
</script>
