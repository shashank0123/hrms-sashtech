<?php $result = json_decode($result); ?>
<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>

<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">

                <?php if ((strtolower(session()->get('plan')) == 'benefits')): ?>
                    <div class="breadcrumb flat text-center row">
                        <span class="col-md-3 col-sm-3 col-xs-12 active">Personal Details</span>
                        <span class="col-md-3 col-sm-3 col-xs-6 hidden-xs">Family Details</span>
                        <span class="col-md-3 col-sm-3 col-xs-6 hidden-xs">Tax Details</span>
                        <span class="col-md-3 col-sm-3 col-xs-6 hidden-xs">Payment Details</span>
                    </div>                
                <?php else: ?>
                    <div class="breadcrumb flat text-center row">
                        <span class="col-md-4 col-sm-4 col-xs-12 active">Personal Details</span>
                        <span class="col-md-4 col-sm-4 col-xs-6 hidden-xs">Tax Details</span>
                        <span class="col-md-4 col-sm-4 col-xs-6 hidden-xs">Payment Details</span>
                    </div>
                <?php endif ?> 
                    

                <br>
                <br>

                <div class="col-md-9 col-sm-12 col-xs-12">
                    <form class="form-horizontal" id="personalDetails" name="personalDetails" method="POST" action="#">
                        <fieldset>
                            <legend>Enter your Personal Details</legend>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating <?php if (empty($result->PAN)) {echo "is-empty";} ?> ">
                                        <label class="control-label" for="pan"><span>PAN</span></label>
                                        <input class="form-control" value="<?php if (!empty($result->PAN)) {
                                            echo $result->PAN;
                                        } ?>" id="pan" name="pan" type="text" style="cursor: auto;">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="dateOfBirth" class="col-md-2 control-label">D.O.B</label>

                                    <div class="col-md-10">
                                        <input type="text" value="<?php if (!empty($result->DateOfBirth)) {
                                            echo $result->DateOfBirth;
                                        } ?>" id="dateOfBirth" name="dateOfBirth" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Gender</label>

                                    <div class="col-md-10">
                                        <div class="radio radio-primary">
                                            <label>
                                                <input type="radio" name="gender" id="gender1" value="Male" <?php if (!empty($result->gender)&&($result->gender == 'Male')) 
                                                echo 'checked=""'; ?> >
                                                    Male
                                            </label>
                                        </div>
                                        <div class="radio radio-primary">
                                            <label>
                                                <input type="radio" name="gender" id="gender2" value="Female" <?php if (!empty($result->gender)&&($result->gender == 'Female'))
                                                echo 'checked=""'; ?>>
                                                Female
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="permanentAddress" class="col-md-2 control-label">Address <span class="small"> permanent </span></label>

                                    <div class="col-md-10">
                                        <textarea class="form-control" rows="2" id="permanentAddress" name="permanentAddress" value="<?php if (!empty($result->permanentAddress)) echo $result->permanentAddress; ?>" ><?php if (!empty($result->permanentAddress)) echo $result->permanentAddress; ?></textarea>
                                    </div>
                                </div>                                
                            </div>
                            

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating <?php if (empty($result->altEmail)) {echo "is-empty";} ?>">
                                        <label for="altEmail" class="control-label">Alternative  Email (Optional)</label>
                                        <input type="email" value="<?php if (!empty($result->altEmail)) echo $result->altEmail; ?>" class="form-control" id="altEmail" name="altEmail">
                                    </div>
                                </div>   
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating <?php if (empty($result->phone)) {echo "is-empty";} ?>">
                                        <label class="control-label" for="phone"><span>Phone</span></label>
                                        <input class="form-control" id="phone" value="<?php if (!empty($result->phone)) echo $result->phone; ?>" name="phone" type="number" min="0" style="cursor: auto;">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="col-md-8 col-sm-8 col-xs-8 col-md-offset-4 col-sm-offset-3 col-xs-offset-2">
                                        <input id="submit" type="submit" value="Save and Continue" class="add btn bg-theme btn-raised">
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
                    </div>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>
    function displayError() {
        swal({
            title : 'Try Again!',
            type : 'error',
            confirmButtonClass : 'bg-theme',
            animation : false,
            width : 300
        });
    };

    function closeAlert() {
        $(".sweet-alert button").first().trigger('click');
    }
        
    $(function() {
        $.material.init();

        // default select gender
        if (!$('#gender1').is(':checked') && !$('#gender2').is(':checked')) {
            $('#gender1').prop('checked', true);
        }

        $('#dateOfBirth').bootstrapMaterialDatePicker({ weekStart : 0, time: false }).on('dateSelected', function(event) {
            $('#'+$(this).data('dtp')).find('.dtp-btn-ok').trigger('click');
        });

        <?php if (empty($result->DateOfBirth)) : ?>
        $("#dateOfBirth").bootstrapMaterialDatePicker('setDate', moment(new Date(1980, 0,1)));
        <?php endif; ?>

        $("form.form-horizontal").submit(function(event) {
            event.preventDefault();

            // remove all error messages
            $(".help-block").remove();
            $(".has-error").removeClass('has-error');
            
            swal({
                title : 'Saving',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 300
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: $('.main form').serialize(), 
            })
            .done(function(data) {
                if (data.status == 200) {
                    swal({
                        title : "Saved!",
                        // text : data.message,
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        showConfirmButton: false,
                        animation : false,
                        width : 300,
                        timer: 1500
                    });   
                    getAndInsert(data.url);
                    history.pushState(null, null, data.url);
                }
                else if(data.status == 208) {  //  email already Exist
                    closeAlert();
                    var $email = $("#altEmail");
                    $email.parent().append('<p class="help-block">'+ data.message +'</p>');
                    $email.focus();
                    $email.parents('.form-group').addClass('has-error');
                }
                else{  // Validation Error
                    displayError();
                    // show Errors
                    for(var fields in data) {
                        $('#'+fields).parents('.form-group').addClass('has-error');
                        $('#'+fields).parent().append('<p class="help-block">'+ data[fields][0] +'</p>');
                    }
                }
            })
            .error(function() {
                displayError();
            });
        });
    });
</script>
