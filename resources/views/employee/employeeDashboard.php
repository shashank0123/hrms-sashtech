<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>


<body  style="margin-top:4em;">
           <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">
                        <center><h4>WELCOME TO <br> <h1>Sashtechs</h1></h4></center>
                        <br>

                        <div class="text-center">
                            <span>Hi <?php echo session()->get('employeeName'); ?>! Your account is almost ready. We just have a couple of quick questions to ask.</span>
                        </div>

                        <br>
                        <br>
                        <br>
                        <br>

                        <?php if (session()->get('employeeProgressNextStep') == 'step1'): ?>
                            <center><a href="/onboardingdetails1" class="ajaxify btn bg-theme btn-raised"><i class="fa fa-share"></i> &nbsp; Lets Get Started</a></center><br>
                        <?php elseif(session()->get('employeeProgressNextStep') == 'step1-2'): ?>
                            <center><a href="/onboardingdetails1-2" class="ajaxify btn bg-theme btn-raised"><i class="fa fa-share"></i> &nbsp; Continue</a></center><br>
                        <?php elseif(session()->get('employeeProgressNextStep') == 'step2'): ?>
                            <center><a href="/onboardingdetails2" class="ajaxify btn bg-theme btn-raised"><i class="fa fa-share"></i> &nbsp; Continue</a></center><br>
                        <?php elseif(session()->get('employeeProgressNextStep') == 'step3'): ?>
                            <center><a href="/onboardingdetails3" class="ajaxify btn bg-theme btn-raised"><i class="fa fa-share"></i> &nbsp; Continue</a></center><br>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>
    $(function() {
        
        // add active class to corresponding link in menu 
        $(".nav li.active").removeClass('active');
        $(".nav.nav-stacked li").filter(function(index) {
            return $(this).data('url') == 'dashboard';
        }).addClass('active');

        $.material.init();
    })
</script>
