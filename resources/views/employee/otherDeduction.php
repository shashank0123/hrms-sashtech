<!DOCTYPE html>
<html>
<head>
    <title>Other Deduction</title>
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
    <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet"></link>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Material Design -->
    <link href="css/bootstrap-material-design.css" rel="stylesheet">
    <link href="css/ripples.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <link rel="stylesheet" href="css/sweetalert2.css">

    <script src="js/jquery-1.12.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ripples.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/sweetalert2.min.js"></script>


    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

</head>

<style>
    .section{
        width:70em;
        padding: 1em;
        margin: 0 auto;
    }
</style>
<body  style="margin-top:1em;background:#fff">
    <div class="section">
        <div class="h4 text-center">
            <a href="/employeeDeduction" class="btn btn-raised bg-theme pull-left">back</a>
            Other Deduction <br> <span class="h6">80CCG, 80E, 80G, 80GGC & 80TTA</span>
        </div>
        <form method="post" enctype="multipart/form-data" class="form-horizontal">
            <br>
            <h6>Note: 
            <br>All the claimed amounts shoulbe be numeric.
            <br>All the proofs should have a maximum size of 500KB.
            <br>All the proofs should be in acceptable format (which is .jpg, .jpeg, .png, .pdf).
            </h6>
            
            <table class="table table-hover">
                <tr>
                    <th>Particulars</th>
                    <th>Claimed Amount</th>
                    <th>Submit Proof</th>
                    <th></th>
                </tr>
                <tr>
                    <td><span class="h6">Investment made in Rajiv Gandhi Equity Saving Scheme (80CCG)</span></td>
                    <td><span class="h6"><input name="80CCG_amount" id="80CCG_amount" type="number" value="<?php echo (isset($result->OD80CCG)) ? $result->OD80CCG : ''; ?>"></span></td>
                    <td><span class="h6"><input name="80CCG_file" id="80CCG_file" type="file" ></span></td>
                    <td class="OD80CCG"></td>
                </tr>
                <tr>
                    <td><span class="h6">Interest paid on higher education loan (80E)</span></td>
                    <td><span class="h6"><input name="80E_amount" id="80E_amount" type="number" value="<?php echo (isset($result->OD80E)) ? $result->OD80E : ''; ?>"></span></td>
                    <td><span class="h6"><input name="80E_file" id="80E_file" type="file"></span></td>
                    <td class="OD80E"></td>
                </tr>
                <tr>
                    <td><span class="h6">Donations made to approve charitable institutions (80G)</span></td>
                    <td><span class="h6"><input name="80G_amount" id="80G_amount" type="number"  value="<?php echo (isset($result->OD80G)) ? $result->OD80G : ''; ?>"></span></td>
                    <td><span class="h6"><input name="80G_file" id="80G_file" type="file"></span></td>
                    <td class="OD80G"></td>
                </tr>
                <tr>
                    <td><span class="h6">Donations made to Political Party (80GGC)</span></td>
                    <td><span class="h6"><input name="80GGC_amount" id="80GGC_amount" type="number"  value="<?php echo (isset($result->OD80GGC)) ? $result->OD80GGC : ''; ?>"></span></td>
                    <td><span class="h6"><input name="80GGC_file" id="80GGC_file" type="file"></td>
                    <td class="OD80GGC"></td>
                </tr>
                <tr>
                    <td><span class="h6">Interest received on Saving Bank Account (80TTA)</span></td>
                    <td><span class="h6"><input name="80TTA_amount" id="80TTA_amount" type="number"  value="<?php echo (isset($result->OD80TTA)) ? $result->OD80TTA : ''; ?>"></span></td>
                    <td><span class="h6"><input name="80TTA_file" id="80TTA_file" type="file"></td>
                    <td class="OD80TTA"></td>
                </tr>
            </table>

            <input type="submit" class="btn bg-theme" value="Submit">
        </form>
    </div>
</body>
</html>

<script>

    function displayError() {
        swal({
            title : "Error !",
            text : 'Try Again!',
            type : 'error',
            confirmButtonClass : 'bg-theme',
            animation : false,
            width : 400
        });
    };

    function closeAlert() {
        $(".sweet-alert button").first().trigger('click');
    }

    $(function() {

        $.material.init();

        function generateData() {
            var formData = new FormData();

            formData.append('80CCG_amount', $('#80CCG_amount').val() || "");
            formData.append('80E_amount', $('#80E_amount').val() || "");
            formData.append('80G_amount', $('#80G_amount').val() || "");
            formData.append('80GGC_amount', $('#80GGC_amount').val() || "");
            formData.append('80TTA_amount', $('#80TTA_amount').val() || "");

            formData.append('80CCG_file', $('#80CCG_file')[0].files[0] || "");
            formData.append('80E_file', $('#80E_file')[0].files[0] || "");
            formData.append('80G_file', $('#80G_file')[0].files[0] || "");
            formData.append('80GGC_file', $('#80GGC_file')[0].files[0] || "");
            formData.append('80TTA_file', $('#80TTA_file')[0].files[0] || "");

            return formData;
        }

        $("form.form-horizontal").on('submit', function(event) {

            event.preventDefault();

            // remove all error messages
            $(".help-block").remove();
            
            swal({
                title : 'Saving',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 300
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: generateData(),
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false   
            })
            .done(function(data) {
                // console.log(data
                if (data.status == 200) {
                    swal({
                        title : "Saved !",
                        type : 'success',
                        showConfirmButton : false,
                        animation : false,
                        width : 300,
                        timer : 1500
                    });   
                }
                else{  // Validation Error
                    displayError();
                    // show Errors
                    for(var fields in data) {
                        $('.'+fields).html('<p class="help-block">'+ data[fields] +'</p>');
                    }
                }
            })
            .error(function() {
                displayError();
            });
        });        
    })
</script>
