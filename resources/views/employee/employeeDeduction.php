<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>

<body  style="margin-top:4em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">
                        <br>
                        <br>
                        <div class="col-md-12 col-sm-12 col-sm-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title text-center"><b>Investment Declarations</b></h3>
                                </div>
                                <div class="panel-body table-responsive">
                                    <table class="table table-hover">
                                        <tr>
                                            <th>Particulars</th>
                                            <th>Claimed</th>
                                            <th>Verified</th>
                                            <th></th>
                                        </tr>
                                        <tr>
                                            <td><span class="h6">Investments Deductions 80C,80CCC,80CCD, 80CCD(2)</span></td>
                                            <td><span class="h6">
                                            <?php 
                                                $c = 0;
                                                if (isset($result)) 
                                                {
                                                    $result = array_filter($result);

                                                    foreach ($result as $key => $value) 
                                                    {
                                                        if (($key == 'id')||($key == 'userId')||($key == 'created_at')) 
                                                        {
                                                            continue;
                                                        }
                                                        else
                                                        {
                                                            $c++;
                                                        }
                                                    }
                                                }
                                                echo $c;
                                             ?>
                                            </span></td>
                                            <td><span class="h6">
                                            <?php 
                                                $c = 0;
                                                // if (isset($result)) 
                                                // {
                                                //     $result = array_filter($result);
                                                    
                                                //     foreach ($result as $key => $value) 
                                                //     {
                                                //         if (($key == 'id')||($key == 'userId')||($key == 'created_at'))
                                                //         {
                                                //             continue;
                                                //         }
                                                //         else
                                                //         {
                                                //             $c++;
                                                //         }
                                                //     }
                                                // }
                                                echo $c;
                                             ?>
                                             </span></td>
                                            <td class="text-right"><a href="/investmentDeduction" ></a><i class="fa fa-list-alt"> </i><span class="h6"> View Details</span></td>
                                        </tr>
                                        <tr>
                                            <td><span class="h6">Medical Deduction 80-D, 80 DD, 80 DDB </span></td>
                                            <td><span class="h6">
                                            <?php 
                                                $c = 0;
                                                if (isset($result1)) 
                                                {
                                                    $result1 = array_filter($result1);
                                                    
                                                    foreach ($result1 as $key => $value) 
                                                    {
                                                        if (($key == 'id')||($key == 'userid')||($key == 'created_at')) 
                                                        {
                                                            continue;
                                                        }
                                                        else
                                                        {
                                                            $c++;
                                                        }
                                                    }
                                                }
                                                echo $c;
                                             ?>
                                            </span></td>
                                            <td><span class="h6">
                                            <?php 
                                                $c = 0;
                                                // if (isset($result1)) 
                                                // {
                                                //     $result1 = array_filter($result1);
                                                    
                                                //     foreach ($result1 as $key => $value) 
                                                //     {
                                                //         if (($key == 'id')||($key == 'userid')||($key == 'created_at'))
                                                //         {
                                                //             continue;
                                                //         }
                                                //         else
                                                //         {
                                                //             $c++;
                                                //         }
                                                //     }
                                                // }
                                                echo $c;
                                             ?>
                                            </span></td>
                                            <td class="text-right"><a href="/medicalDeduction" ></a><i class="fa fa-list-alt"> </i><span class="h6"> View Details</span></td>
                                        </tr>
                                        <tr>
                                            <td><span class="h6">Deductions - Others 80E,80EE, 80G, 80U, 80TTA</span></td>
                                            <td><span class="h6">
                                            <?php 
                                                $c = 0;
                                                if (isset($result2)) 
                                                {
                                                    $result2 = array_filter($result2);
                                                    
                                                    foreach ($result2 as $key => $value) 
                                                    {
                                                        if (($key == 'id')||($key == 'userId')||($key == 'created_at')) 
                                                        {
                                                            continue;
                                                        }
                                                        else
                                                        {
                                                            $c++;
                                                        }
                                                    }
                                                }
                                                echo $c;
                                             ?>
                                            </span></td>
                                            <td><span class="h6">
                                            <?php 
                                                $c = 0;
                                                // if (isset($result2)) 
                                                // {
                                                //     $result2 = array_filter($result2);
                                                    
                                                //     foreach ($result2 as $key => $value) 
                                                //     {
                                                //         if (($key == 'id')||($key == 'userId')||($key == 'created_at')) 
                                                //         {
                                                //             continue;
                                                //         }
                                                //         else
                                                //         {
                                                //             $c++;
                                                //         }
                                                //     }
                                                // }
                                                echo $c;
                                             ?>
                                            </span></td>
                                            <td class="text-right"><a href="/otherDeduction" ></a><i class="fa fa-list-alt"> </i><span class="h6"> View Details</span></td>
                                        </tr>
                                        <tr>
                                            <td><span class="h6">HRA Monthly Details</span></td>
                                            <td><span class="h6">
                                            <?php 
                                                $c = 0;
                                                if (isset($result3)) 
                                                {
                                                    $result3 = array_filter($result3);
                                                    
                                                    foreach ($result3 as $key => $value) 
                                                    {
                                                        if (($key == 'id')||($key == 'userid')||($key == 'landlordPAN')||($key == 'total')||($key == 'year')||($key == 'created_at'))
                                                        {
                                                            continue;
                                                        }
                                                        else
                                                        {
                                                            $c++;
                                                        }
                                                    }
                                                }
                                                echo $c;
                                             ?>
                                            </span></td>
                                            <td><span class="h6">
                                            <?php 
                                                $c = 0;
                                                // if (isset($result3)) 
                                                // {
                                                //     $result3 = array_filter($result3);
                                                    
                                                //     foreach ($result3 as $key => $value) 
                                                //     {
                                                //         if (($key == 'id')||($key == 'userid')||($key == 'landlordPAN')||($key == 'total')||($key == 'year')||($key == 'created_at')) 
                                                //         {
                                                //             continue;
                                                //         }
                                                //         else
                                                //         {
                                                //             $c++;
                                                //         }
                                                //     }
                                                // }
                                                echo $c;
                                             ?>
                                            </span></td>
                                            <td class="text-right"><a href="/hraMonthlyDetails" ></a><i class="fa fa-list-alt"> </i><span class="h6"> View Details</span></td>
                                        </tr>
                                        <tr>
                                            <td><span class="h6">Interest on self occupied properties</span></td>
                                            <td><span class="h6">
                                            <?php 
                                                $c = 0;
                                                if (isset($result4)) 
                                                {
                                                    $result4 = array_filter($result4);
                                                    
                                                    foreach ($result4 as $key => $value) 
                                                    {
                                                        if (($key == 'id')||($key == 'userId')||($key == 'created_at')||($key == 'loanDeductable')) 
                                                        {
                                                            continue;
                                                        }
                                                        else
                                                        {
                                                            $c++;
                                                        }
                                                    }
                                                }
                                                echo $c;
                                             ?>
                                            </span></td>
                                            <td><span class="h6">
                                            <?php 
                                                $c = 0;
                                                // if (isset($result4)) 
                                                // {
                                                //     $result4 = array_filter($result4);
                                                    
                                                //     foreach ($result4 as $key => $value) 
                                                //     {
                                                //         if (($key == 'id')||($key == 'userId')||($key == 'created_at')||($key == 'loanDeductable')) 
                                                //         {
                                                //             continue;
                                                //         }
                                                //         else
                                                //         {
                                                //             $c++;
                                                //         }
                                                //     }
                                                // }
                                                echo $c;
                                             ?>
                                            </span></td>
                                            <td class="text-right"><a href="/interestOnSelfOccupiedProperty" ></a><i class="fa fa-list-alt"> </i><span class="h6"> View Details</span></td>
                                        </tr>
                                            <td><span class="h6">Income from Previous Employer</span></td>
                                            <td><span class="h6">
                                            <?php 
                                                $c = 0;
                                                if (isset($result5)) 
                                                {
                                                    $result5 = array_filter($result5);
                                                    
                                                    foreach ($result5 as $key => $value) 
                                                    {
                                                        if (($key == 'id')||($key == 'userId')||($key == 'created_at')) 
                                                        {
                                                            continue;
                                                        }
                                                        else
                                                        {
                                                            $c++;
                                                        }
                                                    }
                                                }
                                                echo $c;
                                             ?>
                                            </span></td>
                                            <td><span class="h6">
                                            <?php 
                                                $c = 0;
                                                // if (isset($result5)) 
                                                // {
                                                //     $result5 = array_filter($result5);
                                                    
                                                //     foreach ($result5 as $key => $value) 
                                                //     {
                                                //         if (($key == 'id')||($key == 'userId')||($key == 'created_at')) 
                                                //         {
                                                //             continue;
                                                //         }
                                                //         else
                                                //         {
                                                //             $c++;
                                                //         }
                                                //     }
                                                // }
                                                echo $c;
                                             ?>
                                            </span></td>
                                            <td class="text-right"><a href="/incomeFromPreviousEmployer" ></a><i class="fa fa-list-alt"> </i><span class="h6"> View Details</span></td>
                                        </tr>
                                        </tr>
                                            <td><span class="h6">Allowances Declarations</span></td>
                                            <td><span class="h6">
                                            <?php 
                                                $c = 0;
                                                if (isset($result6)) 
                                                {
                                                    $result6 = array_filter($result6);

                                                    $c = count($result6);
                                                }
                                                echo $c;
                                             ?>
                                            </span></td>
                                            <td><span class="h6">
                                            <?php 
                                                $c = 0;
                                                // if (isset($result6)) 
                                                // {
                                                //     $result6 = array_filter($result6);

                                                //     $c = count($result6);
                                                // }
                                                echo $c;
                                             ?>
                                            </span></td>
                                            <td class="text-right"><a href="/allowanceDeclaration" ></a><i class="fa fa-list-alt"> </i><span class="h6"> View Details</span></td>
                                        </tr>
                                    </table>

                                </div>
                            </div>
                        </div>  

                        <style>
                            .table tr {
                                cursor: pointer;
                            }
                        </style>
                    </div>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>
$(function() {


    // add active class to corresponding link in menu 
    $(".nav li.active").removeClass('active');
    $(".nav.nav-stacked li").filter(function(index) {
        return $(this).data('url') == 'employeeDeduction';
    }).addClass('active');

    $.material.init();

    $(".deduction .panel-body").hide();

    $(".deduction .panel-heading").click(function(event) {
        $(this).siblings('.panel-body').slideToggle();
        var $arrow = $(this).find('i.fa');
        if ($arrow.hasClass('fa-caret-down')) {
            $arrow.removeClass('fa-caret-down').addClass('fa-caret-up');
        }
        else
            $arrow.removeClass('fa-caret-up').addClass('fa-caret-down');
    });

    $(".table tr").not(':first').click(function(event) {
        $(this).find('a')[0].click();
    });
});
</script>
