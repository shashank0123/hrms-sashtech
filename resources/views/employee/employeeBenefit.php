<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>


<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">

                        <br>
                        <br>
                        <div class="row text-center" style="border-bottom: 1px solid #ddd;">
                            <div class="col-md-3 col-sm-3 col-xs-4"><h6>Plan Name</h6> </div>
                            <div class="col-md-2 col-sm-3 col-xs-4"><h6>Cover</h6></div>
                            <div class="col-md-3 hidden-sm hidden-xs"><h6>Factors</h6></div>
                            <div class="col-md-2 col-sm-3 col-xs-4"><h6>Total Sum Assured</h6></div>
                            <div class="col-md-2 col-sm-3 hidden-xs"></div>
                        </div>

                        <div class="planContainer row">
                            <div class="plan">
                                <div class="summary">
                                    <div class="col-md-3 col-sm-3 col-xs-4">
                                        <br>
                                        <h5>LIC</h5>
                                        <h6>Insurance</h6>
                                    </div>
                                    <div class="col-md-2 col-sm-3 col-xs-4">
                                        <br>
                                        <h6>Amount - &nbsp;<i class="fa fa-inr"></i>20000</h6>
                                        <h6>Group Size - &nbsp;2</h6>
                                    </div>
                                    <div class="col-md-3 hidden-sm hidden-xs">
                                        <br>
                                        <h6><i class="fa fa-users"></i> &nbsp; FAMILY DEFINITION</h6>
                                        <h6><i class="fa fa-medkit"></i> &nbsp; PRE-EXISTING DISEASES</h6>
                                        <h6><i class="fa fa-money"></i> &nbsp; WAIVER</h6>
                                    </div>
                                    <div class="col-md-2 text-center col-sm-3 col-xs-4"><br><br><h6><i class="fa fa-inr"></i>3000</h6></div>
                                    <div class="col-md-2 text-center col-sm-3 col-xs-12">
                                        <br>
                                        <h6>Cost Per Employee</h6>
                                        <h6><b><i class="fa fa-inr"></i>200</b></h6>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="body">
                                    <div class="col-md-6">
                                        <div class="detail">        
                                            <h6>
                                                <b> &nbsp;&nbsp;<u>Inclusions</u></b>
                                                <div class="clearfix"></div>
                                            </h6>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-8 col-sm-8 col-xs-8"><h6>Pre existing disease</h6></div>
                                                <div class="col-md-4 col-sm-4 col-xs-4"><h6>Waived</h6></div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-8 col-sm-8 col-xs-8"><h6>30 days waiting</h6></div>
                                                <div class="col-md-4 col-sm-4 col-xs-4"><h6>Waived</h6></div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-8 col-sm-8 col-xs-8"><h6>1st year waiting period</h6></div>
                                                <div class="col-md-4 col-sm-4 col-xs-4"><h6>Waived</h6></div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-12 col-sm-12 col-xs-12"><h6>Maternity Benefits covered from day one</h6></div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-12 col-sm-12 col-xs-12"><h6>Nine months waiting period</h6></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="detail">
                                            <h6>
                                                <b> &nbsp;&nbsp; <u>Exclusions </u></b>
                                                <div class="clearfix"></div>
                                            </h6>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-7 col-sm-7 col-xs-7"><h6></h6></div>
                                                <div class="col-md-5 col-sm-5 col-xs-5"><h6></h6></div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-7 col-sm-7 col-xs-7"><h6></h6></div>
                                                <div class="col-md-5 col-sm-5 col-xs-5"><h6></h6></div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-7 col-sm-7 col-xs-7"><h6></h6></div>
                                                <div class="col-md-5 col-sm-5 col-xs-5"><h6></h6></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    
                                    <div class="col-md-6">
                                        <div class="detail">   
                                            <h6>
                                                <b> &nbsp;&nbsp; <u>Other Factors</u></b>
                                                <div class="clearfix"></div>
                                            </h6>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-7 col-sm-7 col-xs-7"><h6>All Day care Procedures</h6></div>
                                                <div class="col-md-5 col-sm-5 col-xs-5"><h6>Covered / Not Covered</h6></div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-7 col-sm-7 col-xs-7"><h6>Co - Pay</h6></div>
                                                <div class="col-md-5 col-sm-5 col-xs-5"><h6>Yes / No</h6></div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-7 col-sm-7 col-xs-7"><h6>Pre hospitalisation</h6></div>
                                                <div class="col-md-5 col-sm-5 col-xs-5"><h6>30 Days</h6></div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-7 col-sm-7 col-xs-7"><h6>Post hospitalisation</h6></div>
                                                <div class="col-md-5 col-sm-5 col-xs-5"><h6>60 Daus</h6></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="detail">
                                    
                                            <h6>
                                                <b> &nbsp;&nbsp; <u>Hospitals and TPA</u></b>
                                                <div class="clearfix"></div>
                                            </h6>

                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-7 col-sm-7 col-xs-7"><h6>TPA</h6></div>
                                                <div class="col-md-5 col-sm-5 col-xs-5"><h6>Self</h6></div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-7 col-sm-7 col-xs-7"><h6>Claim intimation</h6></div>
                                                <div class="col-md-5 col-sm-5 col-xs-5"><h6>Yes / No</h6></div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-7 col-sm-7 col-xs-7"><h6>Mid term addition deletion of employees </h6></div>
                                                <div class="col-md-5 col-sm-5 col-xs-5"><h6>Yes / No</h6></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>

                                    <br>

                                    <!-- <h4>Limitations : </h4>
                                    <ul>
                                        <li></li>
                                        <li></li>
                                    </ul> -->

                                    <br>

                                    <h4>Downloads</h4>
                                    <a href="#">Product Brochure</a><br>
                                    <a href="#">Policy Wording</a>
                                </div>
                            </div>
                        </div>
                        <style>
                            .planContainer{
                                padding: 0 10px;
                            }
                            .plan {
                                /*perspective:1200px;*/
                                margin: 20px 0;
                            }
                            .plan > .summary{
                                transition:all 0.3s ease-in-out;
                                cursor: pointer;
                                background: #f6f6f6;
                                border-bottom: 1px solid #ddd;
                            }
                            .plan > .summary:hover{
                                box-shadow: 0 0 3px #aaa;
                            }

                            .plan >.summary > div{
                                border:1px solid #ddd;
                                border-left: none;
                                border-bottom: none;
                                box-sizing:border-box;
                                min-height: 8em;
                                padding:0 10px;
                            }
                            .plan >.summary > div:first-child{
                                border-left: 1px solid #ddd;
                            }
                            .plan > .summary .btn{
                                margin: 0;
                            }
                            .plan .body{
                                padding: 15px;
                                border: 1px solid #ddd;
                                box-sizing:border-box;
                                padding-top: 30px;
                                transform-style:preserve-3d;
                                transform-origin:top;
                                backface-visibility: hidden;
                            }
                            .detail{
                                box-shadow: 0 0 2px #888;
                                padding: 15px;
                                margin-top: 15px;
                                overflow-x: overlay;
                            }

                            .selected {
                                background: #03A9F4 !important;
                                color: #fff;
                            }
                            .btn.select.btn-raised, .btn.select.btn-raised:hover, .btn.select.btn-raised:active {
                                background: #cf3d3d;
                                color: #fff;
                            }
                        </style>

                    </div>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>

    function displayError() {
        swal({
            title : "Error !",
            text : 'Try Again!',
            type : 'error',
            confirmButtonClass : 'bg-theme',
            animation : false,
            width : 400
        });
    };

    function closeAlert() {
        $(".sweet-alert button").first().trigger('click');
    }

    $(function() {
        
        // add active class to corresponding link in menu 
        $(".nav li.active").removeClass('active');
        $(".nav.nav-stacked li").filter(function(index) {
            return $(this).data('url') == 'employeeBenefit';
        }).addClass('active');

        $.material.init();

        $(".plan .summary").click(function(event) {
            if (event.target.tagName!='SPAN'){
                $(".plan .body").not($(this).siblings('.body')).slideUp();
                $(this).siblings('.body').slideToggle();
            }
        });

        $(".select").click(function(event) {
            event.preventDefault();
            var id = $(this).data('id');
            var $this = $(this);
            
            $.ajax({
                url: location.href + '/' + id,
                type: 'POST',
                data: {id: id},
            })
            .done(function(data) {

                if (data.status == 200) {
  
                    getAndInsert(data.url);
                    history.pushState(null, null, data.url);
                }
                else 
                    displayError();
            })
            .fail(function() {
                displayError();
            })
        });

        $(".plan .body").hide();
    })
</script>
