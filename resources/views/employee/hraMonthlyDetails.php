<!DOCTYPE html>
<html>
<head>
    <title>HRA Monthly Detail</title>
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
    <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet"></link>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Material Design -->
    <link href="css/bootstrap-material-design.css" rel="stylesheet">
    <link href="css/ripples.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <link rel="stylesheet" href="css/sweetalert2.css">

    <script src="js/jquery-1.12.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ripples.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/sweetalert2.min.js"></script>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

</head>

<style>
    .section{
        width:70em;
        padding: 1em;
        margin: 0 auto;
    }
</style>
<body  style="margin-top:1em;background:#fff">
<?php 
    if (isset($errors1)) 
    {
        $errors1 = json_decode($errors1, TRUE);
    }
    
    if (isset($errors2)) 
    {
        $errors2 = json_decode($errors2, TRUE);
    }
 ?>
    <div class="section">
        <div class="h4 text-center">
            <a href="/employeeDeduction" class="btn btn-raised bg-theme pull-left">back</a>
            HRA Monthly Details<br> <span class="h6"></span>
        </div>
        <form method="post" enctype="multipart/form-data" class="form-horizontal">
            <br>
            <h6>Note: 
            <br>All the claimed amounts shoulbe be numeric.
            <br>All the proofs should have a maximum size of 500KB.
            <br>All the proofs should be in acceptable format (which is .jpg, .jpeg, .png, .pdf).
            </h6>
            
            <table class="table table-hover">
                <tr>
                    <th>Particulars</th>
                    <th>Claimed Amount</th>
                    <th>Submit Proof</th>
                    <th></th>
                </tr>
                <tr>
                    <td><span class="h6">Rent - April</span></td>
                    <td><span class="h6"><input name="April_amount" id="April_amount" type="number" class="month" value="<?php echo (isset($result->april)) ? $result->april : '' ; ?>"></span></td>
                    <td><span class="h6"><input name="April_file" id="April_file" type="file"></span></td>
                    <td class="April"></td>
                </tr>
                <tr>
                    <td><span class="h6">Rent - May</span></td>
                    <td><span class="h6"><input name="May_amount" id="May_amount" type="number" class="month" value="<?php echo (isset($result->may)) ? $result->may : '' ; ?>"></span></td>
                    <td><span class="h6"><input name="May_file" id="May_file" type="file"></span></td>
                    <td class="May"></td>
                </tr>
                <tr>
                    <td><span class="h6">Rent - June</span></td>
                    <td><span class="h6"><input name="June_amount" id="June_amount" type="number" class="month" value="<?php echo (isset($result->june)) ? $result->june : '' ; ?>"></span></td>
                    <td><span class="h6"><input name="June_file" id="June_file" type="file"></span></td>
                    <td class="June"></td>
                </tr>
                <tr>
                    <td><span class="h6">Rent - July</span></td>
                    <td><span class="h6"><input name="July_amount" id="July_amount" type="number" class="month" value="<?php echo (isset($result->july)) ? $result->july : '' ; ?>"></span></td>
                    <td><span class="h6"><input name="July_file" id="July_file" type="file"></td>
                    <td class="July"></td>
                </tr>
                <tr>
                    <td><span class="h6">Rent - August</span></td>
                    <td><span class="h6"><input name="August_amount" id="August_amount" type="number" class="month" value="<?php echo (isset($result->august)) ? $result->august : '' ; ?>"></span></td>
                    <td><span class="h6"><input name="August_file" id="August_file" type="file"></td>
                    <td class="August"></td>
                </tr>  
                <tr>
                    <td><span class="h6">Rent- September</span></td>
                    <td><span class="h6"><input name="September_amount" id="September_amount" type="number" class="month" value="<?php echo (isset($result->september)) ? $result->september : '' ; ?>"></span></td>
                    <td><span class="h6"><input name="September_file" id="September_file" type="file"></td>
                    <td class="September"></td>
                </tr>  
                <tr>
                    <td><span class="h6">Rent - October</span></td>
                    <td><span class="h6"><input name="October_amount" id="October_amount" type="number" class="month" value="<?php echo (isset($result->october)) ? $result->october : '' ; ?>"></span></td>
                    <td><span class="h6"><input name="October_file" id="October_file" type="file"></td>
                    <td class="October"></td>
                </tr>  
                <tr>
                    <td><span class="h6">Rent - November</span></td>
                    <td><span class="h6"><input name="November_amount" id="November_amount" type="number" class="month" value="<?php echo (isset($result->november)) ? $result->november : '' ; ?>"></span></td>
                    <td><span class="h6"><input name="November_file" id="November_file" type="file"></td>
                    <td class="November"></td>
                </tr>  
                <tr>
                    <td><span class="h6">Rent - December</span></td>
                    <td><span class="h6"><input name="December_amount" id="December_amount" type="number" class="month" value="<?php echo (isset($result->december)) ? $result->december : '' ; ?>"></span></td>
                    <td><span class="h6"><input name="December_file" id="December_file" type="file"></td>
                    <td class="December"></td>
                </tr>  
                <tr>
                    <td><span class="h6">Rent - January</span></td>
                    <td><span class="h6"><input name="January_amount" id="January_amount" type="number" class="month" value="<?php echo (isset($result->january)) ? $result->january : '' ; ?>"></span></td>
                    <td><span class="h6"><input name="January_file" id="January_file" type="file"></td>
                    <td class="January"></td>
                </tr>  
                <tr>
                    <td><span class="h6">Rent - February</span></td>
                    <td><span class="h6"><input name="February_amount" id="February_amount" type="number" class="month" value="<?php echo (isset($result->february)) ? $result->february : '' ; ?>"></span></td>
                    <td><span class="h6"><input name="February_file" id="February_file" type="file"></td>
                    <td class="February"></td>
                </tr>  
                <tr>
                    <td><span class="h6">Rent - March</span></td>
                    <td><span class="h6"><input name="March_amount" id="March_amount" type="number" class="month" value="<?php echo (isset($result->march)) ? $result->march : '' ; ?>"></span></td>
                    <td><span class="h6"><input name="March_file" id="March_file" type="file"></td>
                    <td class="March"></td>
                </tr>  
                <tr>
                    <td><span class="h6">Landlord’s PAN no.</span></td>
                    <td><span class="h6"><input name="landlord_pan_value" id="landlord_pan_value" type="text" pattern="([a-zA-Z]){5}([0-9]){4}([a-zA-Z])" placeholder="ABCDE1234A" value="<?php echo (isset($result->landlordPAN)) ? $result->landlordPAN : '' ; ?>"></span>
                    <?php if(isset($errors1['landlordPAN'])): ?>
                    <br>
                    <h6 class="text-danger"><?php echo $errors1['landlordPAN'][0]; ?></h6>
                    <?php endif; ?>
                    </td>
                    <td><span class="h6"><input name="landlord_pan_file" id="landlord_pan_file" type="file"></td>
                    <td class="landlordPAN"></td>
                </tr>  
                <tr>
                    <td><span class="h6">Sub Total</span></td>
                    <td><span class="h6"><input name="total_amount" id="total_amount"  type="number" value="<?php echo (isset($result->total)) ? $result->total : '' ; ?>"></span></td>
                    <td></td>
                    <td class="total"></td>
                </tr>                    
            </table>

            <h6>I undertake to produce a copy of the Lease/Rent agreement and undertake full responsibility for the correctness of this declaration in the event of any question/dispute raised by the Income Tax Department.</h6>

            <input type="submit" class="btn bg-theme" value="Submit">
        </form>
    </div>
</body>
</html>

<script>

    function displayError() {
        swal({
            title : "Error !",
            text : 'Try Again!',
            type : 'error',
            confirmButtonClass : 'bg-theme',
            animation : false,
            width : 400
        });
    };

    function closeAlert() {
        $(".sweet-alert button").first().trigger('click');
    }
    
    $(function() {
        $.material.init();
        
        $(".month").change(function(event) {
            var total = 0;
            $(".month").each(function(index, el) {
                total += 1*$(el).val() || 0; 
            });
            $("#total_amount").val(total);
        });

        $(".month").first().trigger('change');

        $.material.init();

        function generateData() {
            var formData = new FormData();

            formData.append('April_amount', $('#April_amount').val() || "");
            formData.append('May_amount', $('#May_amount').val() || "");
            formData.append('June_amount', $('#June_amount').val() || "");
            formData.append('July_amount', $('#July_amount').val() || "");
            formData.append('August_amount', $('#August_amount').val() || "");
            formData.append('September_amount', $('#September_amount').val() || "");
            formData.append('October_amount', $('#October_amount').val() || "");
            formData.append('November_amount', $('#November_amount').val() || "");
            formData.append('December_amount', $('#December_amount').val() || "");
            formData.append('January_amount', $('#January_amount').val() || "");
            formData.append('February_amount', $('#February_amount').val() || "");
            formData.append('March_amount', $('#March_amount').val() || "");
            formData.append('landlord_pan_value', $('#landlord_pan_value').val() || "");
            formData.append('total_amount', $('#total_amount').val() || "");

            formData.append('April_file', $('#April_file')[0].files[0] || "");
            formData.append('May_file', $('#May_file')[0].files[0] || "");
            formData.append('June_file', $('#June_file')[0].files[0] || "");
            formData.append('July_file', $('#July_file')[0].files[0] || "");
            formData.append('August_file', $('#August_file')[0].files[0] || "");
            formData.append('September_file', $('#September_file')[0].files[0] || "");
            formData.append('October_file', $('#October_file')[0].files[0] || "");
            formData.append('November_file', $('#November_file')[0].files[0] || "");
            formData.append('December_file', $('#December_file')[0].files[0] || "");
            formData.append('January_file', $('#January_file')[0].files[0] || "");
            formData.append('February_file', $('#February_file')[0].files[0] || "");
            formData.append('March_file', $('#March_file')[0].files[0] || "");
            formData.append('landlord_pan_file', $('#landlord_pan_file')[0].files[0] || "");

            return formData;
        }

        $("form.form-horizontal").on('submit', function(event) {

            event.preventDefault();

            // remove all error messages
            $(".help-block").remove();
            
            swal({
                title : 'Saving',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 300
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: generateData(),
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false   
            })
            .done(function(data) {
                // console.log(data
                if (data.status == 200) {
                    swal({
                        title : "Saved !",
                        type : 'success',
                        showConfirmButton : false,
                        animation : false,
                        width : 300,
                        timer : 1500
                    });   
                }
                else{  // Validation Error
                    displayError();
                    // show Errors
                    for(var fields in data) {
                        $('.'+fields).html('<p class="help-block">'+ data[fields] +'</p>');
                    }
                }
            })
            .error(function() {
                displayError();
            });
        });
    })
</script>
