<!DOCTYPE html>
<html>
<head>
    <title>Allowance Declaration</title>
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
    <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet"></link>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Material Design -->
    <link href="css/bootstrap-material-design.css" rel="stylesheet">
    <link href="css/ripples.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <link rel="stylesheet" href="css/sweetalert2.css">

    <script src="js/jquery-1.12.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ripples.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/sweetalert2.min.js"></script>


    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

</head>

<style>
    .section{
        width:70em;
        padding: 1em;
        margin: 0 auto;
    }
</style>
<body  style="margin-top:1em;background:#fff">
    <div class="section">
        <div class="h4 text-center">
            <a href="/employeeDeduction" class="btn btn-raised bg-theme pull-left">back</a>
            Allowance Declaration <br>
        </div>
        <form method="post" enctype="multipart/form-data" class="form-horizontal">
            <br>
            <h6>I hereby confirm that the amount of the following allowances incurred by me for attending the office is more than the allowances amount received by me for the year</h6>
            <br>
            <div class="table-responsive">
                <table class="table table-hover">
                    <tr>
                        <th></th>
                        <th>Amount of actual expense if less</th>
                        <th></th>
                    </tr>
                    <tr>
                        <td>
                            <span class="h6">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="conveyance" id="conveyance" value="on"> &nbsp;&nbsp;&nbsp; Conveyance expenses </label>
                                </div>
                            </span>
                        </td>
                        <td><span class="h6"><input name="conveyanceAmount" id="conveyanceAmount" type="number" value="0"></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <span class="h6">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="uniform" id="uniform" value="on"> &nbsp;&nbsp;&nbsp; Uniform Expense </label>
                                </div>
                            </span>
                        </td>
                        <td><span class="h6"><input name="uniformAmount" id="uniformAmount" type="number" value="0"></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <span class="h6">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="driver" id="driver" value="on"> &nbsp;&nbsp;&nbsp; Driver's Salary </label>
                                </div>
                            </span>
                        </td>
                        <td><span class="h6"><input name="driverAmount" id="driverAmount" type="number" value="0"></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <span class="h6">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="childrenHostel" id="childrenHostel" value="on"> &nbsp;&nbsp;&nbsp; Children hostel Allowance </label>
                                </div>
                            </span>
                        </td>
                        <td><span class="h6"><input name="childrenHostelAmount" id="childrenHostelAmount" type="number" value="0"></td>
                        <td></td>
                    </tr>
                    
                </table>
            </div>
            <br><br>
            <div class="table-responsive">
                <table class="table table-hover">
                    <tr>
                        <th></th>
                        <th>Amount of actual expense if less</th>
                        <th>Proof</th>
                        <th></th>
                    </tr>
                    <tr>
                        <td>
                            <span class="h6">Medical Reimbursements </span>
                        </td>
                        <td><span class="h6"><input name="medical_reimbursement_amount" id="medical_reimbursement_amount" type="number"></span></td>
                        <td><span class="h6"><input name="medicalFile" id="medicalFile" type="file"></span></td>
                        <td></td>
                    </tr>
                </table>
            </div>


            <input type="submit" class="btn bg-theme" value="Submit">
        </form>
    </div>
</body>
</html>

<script>

function displayError() {
    swal({
        title : "Error !",
        text : 'Try Again!',
        type : 'error',
        confirmButtonClass : 'bg-theme',
        animation : false,
        width : 400
    });
};

function closeAlert() {
    $(".sweet-alert button").first().trigger('click');
}

$(function() {
    
    $.material.init();

    function generateData() {
        var formData = new FormData();

        formData.append('conveyanceAmount', $('#conveyanceAmount').val() || "");
        formData.append('uniformAmount', $('#uniformAmount').val() || "");
        formData.append('driverAmount', $('#driverAmount').val() || "");
        formData.append('childrenHostelAmount', $('#childrenHostelAmount').val() || "");
        formData.append('medical_reimbursement_amount', $('#medical_reimbursement_amount').val() || "");

        formData.append('conveyance', $('#conveyance').val() || "");
        formData.append('uniform', $('#uniform').val() || "");
        formData.append('driver', $('#driver').val() || "");
        formData.append('childrenHostel', $('#childrenHostel').val() || "");

        formData.append('medicalFile', $('#medicalFile')[0].files[0] || "");

        return formData;
    }

    $("form.form-horizontal").on('submit', function(event) {

        event.preventDefault();

        // remove all error messages
        $(".help-block").remove();
        
        swal({
            title : 'Saving',
            html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
            allowOutsideClick : false,
            showConfirmButton : false,
            animation : false,
            width : 300
        });

        $.ajax({
            url: location.href,
            type: 'POST',
            data: generateData(),
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false   
        })
        .done(function(data) {
            // console.log(data
            if (data.status == 200) {
                swal({
                    title : "Saved !",
                    type : 'success',
                    showConfirmButton : false,
                    animation : false,
                    width : 300,
                    timer : 1500
                });   
            }
            else{  // Validation Error
                displayError();
                // show Errors
                for(var fields in data) {
                    $('.'+fields).html('<p class="help-block">'+ data[fields] +'</p>');
                }
            }
        })
        .error(function() {
            displayError();
        });
    });
})
</script>
