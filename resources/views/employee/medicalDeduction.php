<!DOCTYPE html>
<html>
<head>
    <title>Medical Deduction</title>
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
    <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet"></link>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Material Design -->
    <link href="css/bootstrap-material-design.css" rel="stylesheet">
    <link href="css/ripples.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <link rel="stylesheet" href="css/sweetalert2.css">

    <script src="js/jquery-1.12.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ripples.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/sweetalert2.min.js"></script>



    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

</head>

<style>
    .section{
        width:70em;
        padding: 1em;
        margin: 0 auto;
    }
</style>
<body  style="margin-top:1em;background:#fff">

    <div class="section">
        <div class="h4 text-center">
            <a href="/employeeDeduction" class="btn btn-raised bg-theme pull-left">back</a>
            Medical Deduction <br> <span class="h6">80-D, 80 DD, 80 DDB </span>
        </div>
        <form method="post" enctype="multipart/form-data" class="form-horizontal">
            <br>
            <h6>Note: 
            <br>All the claimed amounts shoulbe be numeric.
            <br>All the proofs should have a maximum size of 500KB.
            <br>All the proofs should be in acceptable format (which is .jpg, .jpeg, .png, .pdf).
            </h6>
            
            <table class="table table-hover">
                <tr>
                    <th>Particulars</th>
                    <th>Claimed Amount</th>
                    <th>Submit Proof</th>
                    <th></th>
                </tr>
                <tr>
                    <td><span class="h6">Mediclaim Paid for himself, Spouse or dependent children</span></td>
                    <td><span class="h6"><input name="medicalhimself" id="medicalhimself" type="number" value="<?php if (isset($result->medicalhimself)) echo $result->medicalhimself;?>"></span></td>
                    <td><span class="h6"><input name="medicalhimself_file" id="medicalhimself_file" type="file"></span></td>
                    <td class="medicalhimself"></td>
                </tr>
                <tr>
                    <td><span class="h6">Preventive health checkup of himself, Spouse or dependent children</span></td>
                    <td><span class="h6"><input name="preventivehimself" id="preventivehimself" type="number" value="<?php if (isset($result->preventivehimself)) echo $result->preventivehimself;?>"></span></td>
                    <td><span class="h6"><input name="preventivehimself_file" id="preventivehimself_file" type="file"></span></td>
                    <td class="preventivehimself"></td>
                </tr>
                <tr>
                    <td>
                        <span class="h6">Mediclaim Paid for parents 
                            <div class="radio" id="medicalparentage">
                                <label>
                                    <input type="radio" value="senior" <?php echo (isset($result->medicalparentage) and ($result->medicalparentage == "senior")) ? "checked" : ' '; ?>  name="medicalparentage">&nbsp;&nbsp;Senior Citizen
                                </label>&nbsp;&nbsp;&nbsp;
                                <label>
                                    <input type="radio" value="non-senior" <?php echo (isset($result->medicalparentage) and ($result->medicalparentage == "non-senior")) ? "checked" : ' '; ?> name="medicalparentage">&nbsp;&nbsp;&nbsp;Non Senior Citizen
                                </label>
                            </div>
                        </span>
                    </td>
                    <td><span class="h6"><input name="medicalparent" id="medicalparent" type="number" value="<?php if (isset($result->medicalparent)) echo $result->medicalparent;?>"></span></td>
                    <td><span class="h6"><input name="medicalparent_file" id="medicalparent_file" type="file"></span></td>
                    <td class="medicalparent medicalparentage"></td>
                </tr>
                <tr>
                    <td><span class="h6"> Preventive health checkup of parents</span></td>
                    <td><span class="h6"><input name="preventiveparent" id="preventiveparent" type="number" value="<?php if (isset($result->preventiveparent)) echo $result->preventiveparent;?>" ></span></td>
                    <td><span class="h6"><input name="preventiveparent_file" id="preventiveparent_file" type="file"></td>
                    <td class="preventiveparent"></td>
                </tr>
                <tr>
                    <td>
                        <span class="h6"> Medical Treatment of Special diseases (80DDB) 
                            <div class="radio" id="medicalspecialage">
                                <label>
                                    <input type="radio" value="senior" <?php echo (isset($result->medicalspecialage) and ($result->medicalspecialage == "senior") ) ? "checked" : ' '; ?> name="medicalspecialage">&nbsp;&nbsp;Senior Citizen
                                </label>&nbsp;&nbsp;&nbsp;
                                <label>
                                    <input type="radio" value="non-senior" <?php echo (isset($result->medicalspecialage) and ($result->medicalspecialage == "non-senior"))  ? "checked" : ' '; ?> name="medicalspecialage">&nbsp;&nbsp;&nbsp;Non Senior Citizen
                                </label>
                            </div>
                        </span>
                    </td>
                    <td><span class="h6"><input name="medicalspecial" id="medicalspecial" type="number" value="<?php if (isset($result->medicalspecial)) echo $result->medicalspecial;?>"></span></td>
                    <td><span class="h6"><input name="medicalspecial_file" id="medicalspecial_file" type="file"></td>
                    <td class="medicalspecial medicalspecialage"></td>
                </tr>
                <tr>
                    <td>
                        <span class="h6"> Medically Handicapped (80U)
                            <div class="radio" id="handicapped80U">
                                <label>
                                    <input type="radio" name="handicapped80U" value="yes" <?php 
                                    if (!empty($result->handicapped80U)) 
                                    {
                                        if (strtolower($result->handicapped80U) == 'no') 
                                        {
                                            echo " ";
                                        }   
                                        else
                                        {
                                            echo "checked";
                                        }
                                    }
                                    ?> > Yes
                                </label>
                                <label>
                                    <input type="radio" name="handicapped80U" value="no" <?php 
                                    if (!empty($result->handicapped80U)) 
                                    {
                                        if (strtolower($result->handicapped80U) == 'no') 
                                        {
                                            echo "checked";
                                        }  
                                    }
                                    ?>
                                    ?> No
                                </label>
                            </div>
                        </span>
                    </td>
                    <td><span class="h6"><input name="handicapped80Uamount" id="handicapped80Uamount" type="number" value="<?php if (isset($result->handicapped80Uamount)) echo $result->handicapped80Uamount;?>"></span></td>
                    <td><span class="h6"><input name="handicapped80U_file" id="handicapped80U_file" type="file"></td>
                    <td class="handicapped80U handicapped80Uamount handicapped80Uyes"></td>
                </tr>
                <tr>
                    <td>
                        <span class="h6"> Any dependent is medically handicapped (80DD) 
                            <div class="radio" id="handicapped80DD">
                                <label>
                                    <input type="radio" name="handicapped80DD" value="yes" <?php 
                                    if (!empty($result->handicapped80DD)) 
                                    {
                                        if (strtolower($result->handicapped80DD) == 'no') 
                                        {
                                            echo " ";
                                        }   
                                        else
                                        {
                                            echo "checked";
                                        }
                                    }
                                    ?> > Yes
                                </label>
                                <label>
                                    <input type="radio" name="handicapped80DD" value="no" <?php 
                                    if (!empty($result->handicapped80DD)) 
                                    {
                                        if (strtolower($result->handicapped80DD) == 'no') 
                                        {
                                            echo "checked";
                                        }  
                                    }
                                    ?>
                                    ?> No
                                </label>
                            </div>
                        </span>
                    </td>
                    <td><span class="h6"><input name="handicapped80DDamount" id="handicapped80DDamount" type="number" value="<?php if (isset($result->handicapped80DDamount)) echo $result->handicapped80DDamount;?>"></span></td>
                    <td><span class="h6"><input name="handicapped80DD_file" id="handicapped80DD_file" type="file"></td>
                    <td class="handicapped80DD handicapped80DDyes handicapped80DDamount"></td>
                </tr>
                
            </table>

            <input type="submit" class="btn bg-theme" value="Submit">
        </form>
    </div>
</body>
</html>

<script>

function displayError() {
    swal({
        title : "Error !",
        text : 'Try Again!',
        type : 'error',
        confirmButtonClass : 'bg-theme',
        animation : false,
        width : 400
    });
};

function closeAlert() {
    $(".sweet-alert button").first().trigger('click');
}

$(function() {
    
    $.material.init();

    var $html1 = '<div class="radio" id="handicapped80Uyes">'+
                    '<label>'+
                        '<input type="radio" name="handicapped80Uyes" value="partial" <?php if(!empty($result->handicapped80U)) { if (strtolower($result->handicapped80U) =='partial') { echo "checked"; } } ?> > Partial (More than 40%)'+
                    '</label>'+
                    '<label>'+
                        '<input type="radio" name="handicapped80Uyes" value="complete" <?php if(!empty($result->handicapped80U)) { if (strtolower($result->handicapped80U) =='complete') { echo "checked"; } } ?> > Complete (More than 80%)'+
                    '</label>'+
                '</div>';

    var $html2 = '<div class="radio" id="handicapped80DDyes">'+
                    '<label>'+
                        '<input type="radio" name="handicapped80DDyes" value="partial" <?php if(!empty($result->handicapped80DD)) { if (strtolower($result->handicapped80DD) =='partial') { echo "checked"; } } ?> > Partial (More than 40%)'+
                    '</label>'+
                    '<label>'+
                        '<input type="radio" name="handicapped80DDyes" value="complete" <?php if(!empty($result->handicapped80DD)) { if (strtolower($result->handicapped80DD) =='complete') { echo "checked"; } } ?>> Complete (More than 80%)'+
                    '</label>'+
                '</div>'

    $('input[name=handicapped80U], input[name=handicapped80DD]').change(function(event) {
        if ($(this).val()=='yes'){
            if ($(this).attr('name')=='handicapped80U')
                $(this).parent().parent().parent().append($html1);
            else
                $(this).parent().parent().parent().append($html2);
            $.material.init();
        }
        else{
            $(this).parent().parent().siblings('.radio').remove();
        }
    });

    function generateData() {
        var formData = new FormData();

        formData.append('medicalhimself', $('#medicalhimself').val() || "");
        formData.append('medicalhimself_file', $('#medicalhimself_file')[0].files[0] || "");
        formData.append('preventivehimself', $('#preventivehimself').val() || "");
        formData.append('preventivehimself_file', $('#preventivehimself_file')[0].files[0] || "");   

        formData.append('medicalparentage', $('#medicalparentage input:checked').val() || "");
        formData.append('medicalparent', $('#medicalparent').val() || "");
        formData.append('medicalparent_file', $('#medicalparent_file')[0].files[0] || "");
        formData.append('preventiveparent', $('#preventiveparent').val() || "");
        formData.append('preventiveparent_file', $('#preventiveparent_file')[0].files[0] || "");

        formData.append('medicalspecialage', $('#medicalspecialage input:checked').val() || "");
        formData.append('medicalspecial', $('#medicalspecial').val() || "");
        formData.append('medicalspecial_file', $('#medicalspecial_file')[0].files[0] || "");

        formData.append('handicapped80U', $('#handicapped80U input:checked').val() || "");
        formData.append('handicapped80Uamount', $('#handicapped80Uamount').val() || "");
        formData.append('handicapped80U_file', $('#handicapped80U_file')[0].files[0] || "");

        formData.append('handicapped80DD', $('#handicapped80DD input:checked').val() || "");
        formData.append('handicapped80DDamount', $('#handicapped80DDamount').val() || "");
        formData.append('handicapped80DD_file', $('#handicapped80DD_file')[0].files[0] || "");

        formData.append('handicapped80Uyes', $('#handicapped80Uyes input:checked').val() || "");
        formData.append('handicapped80DDyes', $('#handicapped80DDyes input:checked').val() || "");

        return formData;
    }

    $("form.form-horizontal").on('submit', function(event) {

        event.preventDefault();

        // remove all error messages
        $(".help-block").remove();
        
        swal({
            title : 'Saving',
            html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
            allowOutsideClick : false,
            showConfirmButton : false,
            animation : false,
            width : 300
        });

        $.ajax({
            url: location.href,
            type: 'POST',
            data: generateData(),
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false   
        })
        .done(function(data) {
            // console.log(data
            if (data.status == 200) {
                swal({
                    title : "Saved !",
                    type : 'success',
                    showConfirmButton : false,
                    animation : false,
                    width : 300,
                    timer : 1500
                });   
            }
            else{  // Validation Error
                displayError();
                // show Errors
                for(var fields in data) {
                    $('.'+fields).html('<p class="help-block">'+ data[fields] +'</p>');
                }
            }
        })
        .error(function() {
            displayError();
        });
    });

    <?php 
        if (!empty($result->handicapped80U)) 
        {
            if (strtolower($result->handicapped80U) != 'no' ) 
            { ?>
                $('#handicapped80U input:first').trigger('change');
            <?php }
        }
    ?>

    <?php 
        if (!empty($result->handicapped80DD)) 
        {
            if (strtolower($result->handicapped80DD) != 'no' ) 
            { ?>
                $('#handicapped80DD input:first').trigger('change');
            <?php }
        }
    ?>
})
</script>
