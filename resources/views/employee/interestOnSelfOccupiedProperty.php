<!DOCTYPE html>
<html>
<head>
    <title>Interest paid on Self Occupied Property </title>
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
    <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet"></link>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Material Design -->
    <link href="css/bootstrap-material-design.css" rel="stylesheet">
    <link href="css/ripples.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <link rel="stylesheet" href="css/sweetalert2.css">

    <script src="js/jquery-1.12.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ripples.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/sweetalert2.min.js"></script>


    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

</head>

<style>
    .section{
        width:70em;
        padding: 1em;
        margin: 0 auto;
    }
</style>
<body  style="margin-top:1em;background:#fff">
    <div class="section">
        <div class="h4 text-center">
            <a href="/employeeDeduction" class="btn btn-raised bg-theme pull-left">back</a>
            Interest paid on Self Occupied Property  <br> <span class="h6"></span>
        </div>
        <form method="post" class="form-horizontal" enctype="multipart/form-data" >
            
            <table class="table table-hover">
                <tr>
                    <th>Particulars</th>
                    <th>Claimed Amount</th>
                    <th>Submit Proof</th>
                    <th></th>
                </tr>
                <tr>
                    <td><span class="h6">Interest paid on Self Occupied Property </span></td>
                    <td><span class="h6"><input name="selfOccupied_amount" id="selfOccupied_amount" type="number" 
                    value="<?php echo (isset($result->selfOccupied)) ? $result->selfOccupied :'';?>"
                    ></span></td>
                    <td><span class="h6"><input name="selfOccupied_file" id="selfOccupied_file" type="file"></span></td>
                    <td class="selfOccupied"></td>
                </tr>
                <tr>
                    <td><span class="h6">Date of Sanction of Loan
                        <div class="radio" id="loan_deductable">
                            <label>
                                <input type="radio" value="no" 
                                <?php echo (isset($result->loanDeductable)and($result->loanDeductable == 'no'))  ? 'checked' :'';?> 
                                name="loan_deductable">&nbsp;&nbsp;Before 01.04.1999</label>&nbsp;&nbsp;&nbsp;
                            <label>
                                <input type="radio" value="yes" 
                                <?php echo (isset($result->loanDeductable)and($result->loanDeductable == 'yes'))  ? 'checked' :'';?> 
                                name="loan_deductable">&nbsp;&nbsp;&nbsp;On/After 01.04.1999</label>
                        </div>
                    </span></td>
                    <td><span class="h6"><input name="loanDate_amount" id="loanDate_amount" type="number"
                    value="<?php echo (isset($result->loanAmount)) ? $result->loanAmount :'';?>"
                    ></span></td>
                    <td></td>
                    <td class="loanDeductable loanAmount"></td>
                </tr>
            </table>

            <input type="submit" class="btn bg-theme" value="Submit">
        </form>
    </div>
</body>
</html>

<script>

    function displayError() {
        swal({
            title : "Error !",
            text : 'Try Again!',
            type : 'error',
            confirmButtonClass : 'bg-theme',
            animation : false,
            width : 400
        });
    };

    function closeAlert() {
        $(".sweet-alert button").first().trigger('click');
    }

    $(function() {
        $.material.init();
        
        function generateData() {
            var formData = new FormData();

            formData.append('selfOccupied_amount', $('#selfOccupied_amount').val() || "");
            formData.append('loanDate_amount', $('#loanDate_amount').val() || "");
            formData.append('loan_deductable', $('#loan_deductable input:checked').val() || "");

            formData.append('selfOccupied_file', $('#selfOccupied_file')[0].files[0] || "");

            return formData;
        }

        $("form.form-horizontal").on('submit', function(event) {

            event.preventDefault();

            // remove all error messages
            $(".help-block").remove();
            
            swal({
                title : 'Saving',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 300
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: generateData(),
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false   
            })
            .done(function(data) {
                // console.log(data
                if (data.status == 200) {
                    swal({
                        title : "Saved !",
                        type : 'success',
                        showConfirmButton : false,
                        animation : false,
                        width : 300,
                        timer : 1500
                    });   
                }
                else{  // Validation Error
                    displayError();
                    // show Errors
                    for(var fields in data) {
                        $('.'+fields).html('<p class="help-block">'+ data[fields] +'</p>');
                    }
                }
            })
            .error(function() {
                displayError();
            });
        });
    })
</script>
