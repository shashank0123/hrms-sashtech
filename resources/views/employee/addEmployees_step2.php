<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>

<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">

                <?php if ((strtolower(session()->get('plan')) == 'benefits')): ?>
                    <div class="breadcrumb flat text-center row">
                        <span class="col-md-3 col-sm-3 col-xs-6 hidden-xs">Personal Details</span>
                        <span class="col-md-3 col-sm-3 col-xs-6 hidden-xs">Family Details</span>
                        <span class="col-md-3 col-sm-3 col-xs-12 active">Tax Details</span>
                        <span class="col-md-3 col-sm-3 col-xs-6 hidden-xs">Payment Details</span>
                    </div>                
                <?php else: ?>
                    <div class="breadcrumb flat text-center row">
                        <span class="col-md-4 col-sm-4 col-xs-6 hidden-xs">Personal Details</span>
                        <span class="col-md-4 col-sm-4 col-xs-12 active">Tax Details</span>
                        <span class="col-md-4 col-sm-4 col-xs-6 hidden-xs">Payment Details</span>
                    </div>
                <?php endif ?> 

                <br>
                <br>

                <div class="col-md-9 col-sm-12 col-xs-12">
                    <form class="form-horizontal" method="POST" action="" id="taxDetail">
                        <fieldset>
                            <legend>Tax Details / Information</legend>
                            
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="togglebutton form-group">
                                        <label>
                                            Professional Tax Applicable Y/N
                                            &nbsp;&nbsp;&nbsp; 
                                            <input type="checkbox" name="taxApplicable" 
                                            <?php 
                                            if (isset($user->taxApplicable)) {
                                                if ($user->taxApplicable == "On") {
                                                     echo "checked";
                                                }
                                            }
                                             ?>
                                            >
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating <?php if (empty($user->PFAcNo)) {echo "is-empty";} ?> ">
                                        <label class="control-label" for="PFAcNo"><span>Provident Fund A/c Number</span></label>
                                        <input class="form-control" id="PFAcNo" name="PFAcNo" type="text" value="<?php if (!empty($user->PFAcNo)) echo $user->PFAcNo; ?>" style="cursor: auto;">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating <?php if (empty($user->esiNumber)) {echo "is-empty";} ?> ">
                                        <label class="control-label" for="esiNumber"><span>ESI Number</span></label>
                                        <input class="form-control" id="esiNumber" name="esiNumber" type="text" value="<?php if (!empty($user->esiNumber)) echo $user->esiNumber; ?>" style="cursor: auto;">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="col-md-8 col-sm-8 col-xs-8 col-md-offset-4 col-sm-offset-3 col-xs-offset-2">
                                        <input type="submit" id="submit" value="Save and Continue" class="add btn bg-theme btn-raised">
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
                    </div>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>
    function displayError() {
        swal({
            // title : "Error !",
            title : 'Try Again!',
            type : 'error',
            confirmButtonClass : 'bg-theme',
            animation : false,
            width : 300
        });
    };

    function closeAlert() {
        $(".sweet-alert button").first().trigger('click');
    }

    $(function() {
        $.material.init();

        $("form.form-horizontal").submit(function(event) {

            event.preventDefault();

            // remove all error messages
            $(".help-block").remove();
            $(".has-error").removeClass('has-error');
            
            swal({
                title : 'Saving',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 300
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: $('.main form').serialize(), 
            })
            .done(function(data) {
                if (data.status == 200) {
                    swal({
                        title : "Success!",
                        // text : data.message,
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        showConfirmButton: false,
                        animation : false,
                        width : 300,
                        timer: 1500
                    });   
                    getAndInsert(data.url);
                    history.pushState(null, null, data.url);
                }
                else{  // Validation Error
                    displayError();
                    // show Errors
                    for(var fields in data) {
                        $('#'+fields).parents('.form-group').addClass('has-error');
                        $('#'+fields).parent().append('<p class="help-block">'+ data[fields][0] +'</p>');
                    }
                }
            })
            .error(function() {
                displayError();
            });
        });
    });
</script>
