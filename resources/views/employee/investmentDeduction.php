<!DOCTYPE html>
<html>
<head>
    <title>Investement Deduction</title>
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
    <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet"></link>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Material Design -->
    <link href="css/bootstrap-material-design.css" rel="stylesheet">
    <link href="css/ripples.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <link rel="stylesheet" href="css/sweetalert2.css">

    <script src="js/jquery-1.12.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ripples.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/sweetalert2.min.js"></script>


    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <style>
        .section{
            width:70em;
            padding: 1em;
            margin: 0 auto;
        }
    </style>

</head>

<body  style="margin-top:1em;background:#fff">
    <div class="section">
        <div class="h4 text-center">
            <a href="/employeeDeduction" class="btn btn-raised bg-theme pull-left">back</a>
            Investement Deduction <br> <span class="h6">80C,80CCC,80CCD, 80CCD(2)</span>
        </div>
        <form method="post" class="form-horizontal" enctype="multipart/form-data">
            <br>
            <h6>Note: 
            <br>All the claimed amounts shoulbe be numeric.
            <br>All the proofs should have a maximum size of 500KB.
            <br>All the proofs should be in acceptable format (which is .jpg, .jpeg, .png, .pdf).
            </h6>
            
            <table class="table table-hover">
                <tr>
                    <th>Particulars</th>
                    <th>Claimed Amount</th>
                    <th>Submit Proof</th>
                    <th></th>
                </tr>
                <tr>
                    <td><span class="h6">Life insurance Premium (LIC)</span></td>
                    <td><span class="h6"><input name="LIC_value" id="LIC_value" type="number" min="0" value="<?php if (isset($result->LICamount)) echo $result->LICamount;?>"></span></td>
                    <td><span class="h6"><input name="LIC_file" id="LIC_file" type="file"></span></td>
                    <td class="LICurl LICamount"></td>
                </tr>
                <tr>
                    <td><span class="h6">Pension Scheme</span></td>
                    <td><span class="h6"><input name="PS_value" id="PS_value" type="number" min="0" value="<?php if (isset($result->pensionScheme)) echo $result->pensionScheme;?>"></span></td>
                    <td><span class="h6"><input name="PS_file" id="PS_file" type="file"></span></td>
                    <td class="pensionSchemeurl pensionScheme"></td>
                </tr>
                <tr>
                    <td><span class="h6">Public Provident Fund (PPF)</span></td>
                    <td><span class="h6"><input name="PPF_value" id="PPF_value" type="number" min="0" value="<?php if (isset($result->ppf)) echo $result->ppf;?>"></span></td>
                    <td><span class="h6"><input name="PPF_file" id="PPF_file" type="file"></span></td>
                    <td class="ppfurl ppf"></td>
                </tr>
                <tr>
                    <td><span class="h6">Tution Fee</span></td>
                    <td><span class="h6"><input name="TF_value" id="TF_value" type="number" min="0" value="<?php if (isset($result->tuitionfee)) echo $result->tuitionfee;?>"></span></td>
                    <td><span class="h6"><input name="TF_file" id="TF_file" type="file"></td>
                    <td class="tuitionfeeurl tuitionfee"></td>
                </tr>
                <tr>
                    <td><span class="h6">Sukanya Samriddhi Account </span></td>
                    <td><span class="h6"><input name="SSA_value" id="SSA_value" type="number" min="0" value="<?php if (isset($result->sukanyasamriddhiac)) echo $result->sukanyasamriddhiac;?>"></span></td>
                    <td><span class="h6"><input name="SSA_file" id="SSA_file" type="file"></td>
                    <td class="sukanyasamriddhiacurl sukanyasamriddhiac"></td>
                </tr>
                <tr>
                    <td><span class="h6">Post Office Time Deposits</span></td>
                    <td><span class="h6"><input name="POTD_value" id="POTD_value" type="number" min="0" value="<?php if (isset($result->postofficetimedeposit)) echo $result->postofficetimedeposit;?>"></span></td>
                    <td><span class="h6"><input name="POTD_file" id="POTD_file" type="file"></td>
                    <td class="postofficetimedepositurl postofficetimedeposit"></td>
                </tr>
                <tr>
                    <td><span class="h6">National Saving Certificate (NSC)</span></td>
                    <td><span class="h6"><input name="NSC_value" id="NSC_value" type="number" min="0" value="<?php if (isset($result->nsc)) echo $result->nsc;?>"></span></td>
                    <td><span class="h6"><input name="NSC_file" id="NSC_file" type="file"></td>
                    <td class="nscurl nsc"></td>
                </tr>
                <tr>
                    <td><span class="h6">Unit Linked Insurance Plans(ULIP)</span></td>
                    <td><span class="h6"><input name="ULIP_value" id="ULIP_value" type="number" min="0" value="<?php if (isset($result->ulip)) echo $result->ulip;?>"></span></td>
                    <td><span class="h6"><input name="ULIP_file" id="ULIP_file" type="file"></td>
                    <td class="ulipurl ulip"></td>
                </tr>
                <tr>
                    <td><span class="h6">Retirement Benefit Plans</span></td>
                    <td><span class="h6"><input name="RBP_value" id="RBP_value" type="number" min="0" value="<?php if (isset($result->retirementbplan)) echo $result->retirementbplan;?>"></span></td>
                    <td><span class="h6"><input name="RBP_file" id="RBP_file" type="file"></td>
                    <td class="retirementbplanurl retirementbplan"></td>
                </tr>
                <tr>
                    <td><span class="h6">Fixed Deposits- 5 Years</span></td>
                    <td><span class="h6"><input name="FD_value" id="FD_value" type="number" min="0" value="<?php if (isset($result->fdvy)) echo $result->fdvy;?>"></span></td>
                    <td><span class="h6"><input name="FD_file" id="FD_file" type="file"></td>
                    <td class="fdvyurl fdvy"></td>
                </tr>
                <tr>
                    <td><span class="h6">Infrastructure Bonds</span></td>
                    <td><span class="h6"><input name="IB_value" id="IB_value" type="number" min="0" value="<?php if (isset($result->infrastructurebonds)) echo $result->infrastructurebonds;?>"></span></td>
                    <td><span class="h6"><input name="IB_file" id="IB_file" type="file"></td>
                    <td class="infrastructurebondsurl infrastructurebonds"></td>
                </tr>
                <tr>
                    <td><span class="h6">Kisan Vikas Patra (KVP)</span></td>
                    <td><span class="h6"><input name="KVP_value" id="KVP_value" type="number" min="0" value="<?php if (isset($result->kvp)) echo $result->kvp;?>"></span></td>
                    <td><span class="h6"><input name="KVP_file" id="KVP_file" type="file"></td>
                    <td class="kvpurl kvp"></td>
                </tr>
                <tr>
                    <td><span class="h6">EPF/Other Pension Funds</span></td>
                    <td><span class="h6"><input name="EPF_value" id="EPF_value" type="number" min="0" value="<?php if (isset($result->epf)) echo $result->epf;?>"></span></td>
                    <td><span class="h6"><input name="EPF_file" id="EPF_file" type="file"></td>
                    <td class="epfurl epf"></td>
                </tr>
                <tr>
                    <td><span class="h6">Housing Loan Principal Repayment</span></td>
                    <td><span class="h6"><input name="HLPR_value" id="HLPR_value" type="number" min="0" value="<?php if (isset($result->hlprepayment)) echo $result->hlprepayment;?>"></span></td>
                    <td><span class="h6"><input name="HLPR_file" id="HLPR_file" type="file"></td>
                    <td class="hlprepaymenturl hlprepayment"></td>
                </tr>
                <tr>
                    <td><span class="h6">New Pension Scheme (80CCC)</span></td>
                    <td><span class="h6"><input name="NPS_value" id="NPS_value" type="number" min="0" value="<?php if (isset($result->nps80ccc)) echo $result->nps80ccc;?>"></span></td>
                    <td><span class="h6"><input name="NPS_file" id="NPS_file" type="file"></td>
                    <td class="nps80cccurl nps80ccc"></td>
                </tr>
                <tr>
                    <td><span class="h6">Pension Fund Investment by Employee (80CCD(1))</span></td>
                    <td><span class="h6"><input name="PFIE_value" id="PFIE_value" type="number" min="0" value="<?php if (isset($result->pfie80ccd1)) echo $result->pfie80ccd1;?>"></span></td>
                    <td><span class="h6"><input name="PFIE_file" id="PFIE_file" type="file"></td>
                    <td class="pfie80ccd1url pfie80ccd1"></td>
                </tr>
                <tr>
                    <td><span class="h6">Pension Fund Investment by Employer (80CCD(2))</span></td>
                    <td><span class="h6"><input name="PFIE2_value" id="PFIE2_value" type="number" min="0" value="<?php if (isset($result->pfie80ccd2)) echo $result->pfie80ccd2;?>"></span></td>
                    <td><span class="h6"><input name="PFIE2_file" id="PFIE2_file" type="file"></td>
                    <td class="pfie80ccd2url pfie80ccd2"></td>
                </tr>
                <tr>
                    <td><span class="h6">Any Other Eligible Investments </span></td>
                    <td><span class="h6"><input name="AOEI_value" id="AOEI_value" type="number" min="0" value="<?php if (isset($result->other)) echo $result->other;?>"></span></td>
                    <td><span class="h6"><input name="AOEI_file" id="AOEI_file" type="file"></td>
                    <td class="otherurl other"></td>
                </tr>
            </table>

            <input type="submit" class="btn bg-theme" value="Submit">
        </form>
    </div>
</body>
</html>

<script>

    function displayError() {
        swal({
            title : "Error !",
            text : 'Try Again!',
            type : 'error',
            confirmButtonClass : 'bg-theme',
            animation : false,
            width : 400
        });
    };

    function closeAlert() {
        $(".sweet-alert button").first().trigger('click');
    }

    $(function() {
        $.material.init();
        
        function generateData() {
            var formData = new FormData();

            formData.append('LIC_value', $('#LIC_value').val() || "");
            formData.append('PS_value', $('#PS_value').val() || "");
            formData.append('PPF_value', $('#PPF_value').val() || "");
            formData.append('TF_value', $('#TF_value').val() || "");
            formData.append('SSA_value', $('#SSA_value').val() || "");
            formData.append('POTD_value', $('#POTD_value').val() || "");
            formData.append('NSC_value', $('#NSC_value').val() || "");
            formData.append('ULIP_value', $('#ULIP_value').val() || "");
            formData.append('RBP_value', $('#RBP_value').val() || "");
            formData.append('FD_value', $('#FD_value').val() || "");
            formData.append('IB_value', $('#IB_value').val() || "");
            formData.append('KVP_value', $('#KVP_value').val() || "");
            formData.append('EPF_value', $('#EPF_value').val() || "");
            formData.append('HLPR_value', $('#HLPR_value').val() || "");
            formData.append('NPS_value', $('#NPS_value').val() || "");
            formData.append('PFIE_value', $('#PFIE_value').val() || "");
            formData.append('PFIE2_value', $('#PFIE2_value').val() || "");
            formData.append('AOEI_value', $('#AOEI_value').val() || "");

            formData.append('LIC_file', $('#LIC_file')[0].files[0] || "");
            formData.append('PS_file', $('#PS_file')[0].files[0] || "");
            formData.append('PPF_file', $('#PPF_file')[0].files[0] || "");
            formData.append('TF_file', $('#TF_file')[0].files[0] || "");
            formData.append('SSA_file', $('#SSA_file')[0].files[0] || "");
            formData.append('POTD_file', $('#POTD_file')[0].files[0] || "");
            formData.append('NSC_file', $('#NSC_file')[0].files[0] || "");
            formData.append('ULIP_file', $('#ULIP_file')[0].files[0] || "");
            formData.append('RBP_file', $('#RBP_file')[0].files[0] || "");
            formData.append('FD_file', $('#FD_file')[0].files[0] || "");
            formData.append('IB_file', $('#IB_file')[0].files[0] || "");
            formData.append('KVP_file', $('#KVP_file')[0].files[0] || "");
            formData.append('EPF_file', $('#EPF_file')[0].files[0] || "");
            formData.append('HLPR_file', $('#HLPR_file')[0].files[0] || "");
            formData.append('NPS_file', $('#NPS_file')[0].files[0] || "");
            formData.append('PFIE_file', $('#PFIE_file')[0].files[0] || "");
            formData.append('PFIE2_file', $('#PFIE2_file')[0].files[0] || "");
            formData.append('AOEI_file', $('#AOEI_file')[0].files[0] || "");

            return formData;
        }

        $("form.form-horizontal").on('submit', function(event) {

            event.preventDefault();

            // remove all error messages
            $(".help-block").remove();
            
            swal({
                title : 'Saving',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 300
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: generateData(),
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false   
            })
            .done(function(data) {
                // console.log(data
                if (data.status == 200) {
                    swal({
                        title : "Saved !",
                        type : 'success',
                        showConfirmButton : false,
                        animation : false,
                        width : 300,
                        timer : 1500
                    });   
                }
                else{  // Validation Error
                    displayError();
                    // show Errors
                    for(var fields in data) {
                        $('.'+fields).html('<p class="help-block">'+ data[fields][0] +'</p>');
                    }
                }
            })
            .error(function() {
                displayError();
            });
        });
    });
</script>
