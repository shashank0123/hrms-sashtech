<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>


<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">
                        <br>
                        <br>
                        <form action="" class="form-horizontal">
                            <fieldset>
                                <legend>Terminate</legend>
                                <br>
                                
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                        <div class="form-group">
                                            <label for="employeeId" class="col-md-4 control-label">Employee Id</label>

                                            <div class="col-md-8">
                                                <input type="text" class="form-control" id="employeeId" name="employeeId">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                        <div class="form-group">
                                            <label for="employeeName" class="col-md-4 control-label">Employee Name</label>

                                            <div class="col-md-8">
                                                <input type="text" class="form-control" id="employeeName" name="employeeName">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                        <div class="form-group">
                                            <label for="lastDate" class="col-md-4 control-label">Last Date</label>

                                            <div class="col-md-8">
                                                <input type="text" name="lastDate" id="lastDate" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <br>
                                <br>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                        <input type="submit" class="btn btn-raised bg-theme" value="submit">
                                    </div>
                                </div>

                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>

    function displayError(msg) {
        swal({
            title : "Error !",
            text : msg,
            type : 'error',
            confirmButtonClass : 'bg-theme',
            animation : false,
            width : 400
        });
    };

    function closeAlert() {
        $(".sweet-alert button").first().trigger('click');
    }

    $(function() {
        $.material.init();

        $('#lastDate').bootstrapMaterialDatePicker({ weekStart : 0, time: false }).on('dateSelected', function(event) {
            $('#'+$(this).data('dtp')).find('.dtp-btn-ok').trigger('click');
        });
        $('#lastDate').bootstrapMaterialDatePicker('setDate', moment());

        $("form.form-horizontal").submit(function(event) {
            event.preventDefault();

            swal({
                title : 'Please Wait',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 300
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: $(this).serialize(),
            })
            .done(function(data) {
                if (data.status == 200) {
                    swal({
                        title : "Terminated!",
                        // text : data.message,
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        showConfirmButton: false,
                        animation : false,
                        width : 300,
                        timer : 1500
                    });
                }
                else {
                    displayError(data.message);
                }
            })
            .fail(function() {
                displayError("Try Again!")
            });
            
        });
    })
</script>
