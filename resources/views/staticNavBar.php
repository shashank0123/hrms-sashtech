<!-- ================  Navbar  ===================== -->
<nav>
    <div class="container-fluid">

        <div class="row">
            
            <div class="navbar navbar-default">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-warning-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <!-- <a class="navbar-brand" >Sashtechs</a> -->
                        <!-- <a class="navbar-brand " href="javascript:void(0)" ><img src="images/W.png" alt="Sashtechs"></a> -->
                        <a class="navbar-brand " href="javascript:void(0)" ><span class="page-header"><b>Sashtechs</b></span></a>
                    </div>

                    <div class="navbar-collapse collapse navbar-warning-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="/payroll" >Payroll</a></li>
                            <li><a href="/health-insurance-benefit" >Health Benefits</a></li>
                            <li><a href="/pricing" >Pricing</a></li>
                            <li><a href="/accountant" >Accountant</a></li>
                            <li><a href="/demo" >Demo</a></li>
                            <li><a href="/contactUs" >Contact us</a></li>
                            <li><a href="/login" >Sign In</a></li>
                            <li><a href="/signup" >Sign Up</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</nav>

<script>

    $(".navbar").css('backgroundColor', 'transparent');

    $(function() {

        $(".navbar-toggle").click(function(event) {
            $(".navbar").css('backgroundColor', '#cf3d3d');
        });

        $(window).on('scroll', function(event) {
            // event.preventDefault();
            var top = $(this).scrollTop();
            if (top > 20 && !$(".navbar-collapse").hasClass('in')) {
                $(".navbar .container").css('padding', '0 15px');
                $(".navbar").css('backgroundColor', '#cf3d3d');
            }
            else {
                if (!$(".navbar-collapse").hasClass('in')) {
                    $(".navbar").css('backgroundColor', 'transparent');
                    $(".navbar .container").css('padding', '20px 30px');
                }
            }
        });
    })
</script>
