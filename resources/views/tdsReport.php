<!DOCTYPE html>
<html>
<head>
    <title>TDS Report</title>
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
    <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet"></link>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Material Design -->
    <link href="css/bootstrap-material-design.css" rel="stylesheet">
    <link href="css/ripples.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">

    <script src="js/jquery-1.12.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ripples.min.js"></script>
    <script src="js/material.min.js"></script>


    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

</head>

<style>
    .section{
        width:60em;
        padding: 1em;
        margin: 0 auto;
    }

    .table{
        border-collapse: collapse;
        border: 1px solid #000;
    }
    .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th{
        border-color:#000;
        padding-left: 10px;
    }

    .bg-theme1{
        background: #FC6558 !important;
        color: #fff;
    }

    #logo > div:first-child{
        border-top: 1px solid #aaa;
    }

    @media print {
        .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th{
            border-color:#000 !important;
        }

        table{
            border-collapse: collapse !important;
        }
        button.btn{
            display: none !important;
        }
        i.fa-heart{
            color: #f44336 !important;
        }
        
        table td.bg-theme1,table th.bg-theme1,table tr.bg-theme1{
            background: #FC6558 !important;
            color: #fff !important;
        }
        table tr td b{
            color:#fff !important;
        }
    }

</style>
<body  style="margin-top:1em;background:#fff">
    <div class="section">
        <span id="logo" class="fa btn btn-lg" >Sashtechs <div></div></span>
        
        <br>
            <h4 class="text-center">TDS Report</h4>
        <br>

        <table class="table table-bordered " cols="7">

            <tr>
                <th class="bg-theme1">id</th>
                <th class="bg-theme1" width="200">Name</th>
                <th class="bg-theme1">Salary Annual</th>
                <th class="bg-theme1">Annual TDS Liability</th>
                <th class="bg-theme1">TDS deducted so far</th>
                <th class="bg-theme1">Salary for the month</th>
                <th class="bg-theme1">Deduction for the month</th>
            </tr>
            <tr>
                <td> 0 </td>
                <td> 0 </td>
                <td> 0 </td>
                <td> 0 </td>
                <td> 0 </td>
                <td> 0 </td>
                <td> 0 </td>
            </tr>
            <tr>
                <td> 0 </td>
                <td> 0 </td>
                <td> 0 </td>
                <td> 0 </td>
                <td> 0 </td>
                <td> 0 </td>
                <td> 0 </td>
            </tr>
            
        </table>

        <div class="heart-beat">Designed with <i class="fa fa-heart text-danger"></i> Sashtechs</div>
        <br>
        <button class="btn btn-raised bg-theme" onclick="print()">PRINT</button>
    </div>
</body>
</html>

<script>
    $.material.init();
</script>
