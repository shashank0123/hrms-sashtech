<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
    <link href="css/roundslider.min.css" rel="stylesheet">
    <script src="js/roundslider.min.js"></script>
</head>


<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">
                        
                        <br>
                        <h2 class="text-warning text-center">Your Security is Important to Us</h2>

                        <br>

                        <p class="text-muted text-center">We just e-mailed you (xyz*******@abc.com) a 6-digit code to authenticate this session. Please enter it in the field below</p>

                        <br><br>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <h3>Why am I seeing this message ?</h3>
                            <ul>
                                <li>It's the first time you've used this computer or browser to log into your dashboard</li>
                                <li>You've recently cleared your browser's cookies (or they've expired)</li>
                                <li>We're authenticating existing users (just once) to keep things secure</li>
                            </ul>                            
                        
                            <br>

                        </div>

                        <form action="#" method="POST" class="form-horizontal">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group label-floating is-empty ">
                                        <label class="control-label" for="code"><span>Authentication Code</span></label>
                                        <input class="form-control" id="code" name="code" type="text" style="cursor: auto;">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <span class="btn btn-raised bg-theme validate">Validate</span>
                            </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>

    $(function() {
        $.material.init();

        $("nav .nav.navbar.nav-stacked li").addClass('disabled');
        $("nav .nav.navbar.nav-stacked li").css({
            webkitFilter: 'blur(3px)',
            filter: 'blur(3px)'
        });


        $(".validate").click(function(event) {
            $.ajax({
                url: location.href,
                type: 'POST',
                data: $('.main form').serialize(),
            })
            .done(function(data) {
                console.log("success", data);
            })
            .fail(function() {
                console.log("error");
            })
        });
    });
</script>