<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>


<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">
                        <div id="view">
                            <h3> Contractor Details </h3><hr>
                            <br>
                                
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="detail">
                                        
                                        <h5> 
                                            <i class="fa fa-user"> </i><b> &nbsp;&nbsp;<u>Personal Details</u>
                                            <span class="pull-right edit btn btn-default" data-id="personalDetail"><i class="fa fa-pencil"></i></span></b>
                                            <div class="clearfix"></div>
                                        </h5>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>Name</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <h6 style="display: flex;">
                                                <span id="viewFirstName">
                                                <?php echo isset($result->firstName) ? $result->firstName : ' ' ;?>
                                                </span>
                                                &nbsp;
                                                <span id="viewLastName">
                                                <?php echo isset($result->lastName) ? ' ' . $result->lastName : ' ' ;?>
                                                </span>
                                                </h6>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>Status</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6" id="viewStatus"><h6>
                                            <?php echo isset($result1->status) ? $result1->status : ' ' ;?>
                                            </h6></div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>PAN</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 id="viewPAN">
                                            <?php echo isset($result2->PAN) ? $result2->PAN : ' ' ;?>
                                            </h6></div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>D.O.B / D.O.I</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 id="viewDateOfBirth">
                                            <?php 
                                            if (isset($result2->DateOfBirth)) 
                                            {
                                                $date = DateTime::createFromFormat('Y-m-d', $result2->DateOfBirth)->format('F j, Y');
                                                echo $date;
                                            }
                                            ;?>
                                            </h6></div>
                                            <div class="hidden" id="dateOfBirthVal">
                                                <?php echo isset($result2->DateOfBirth) ? $result2->DateOfBirth : '' ;?>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="detail">
                                    
                                        <h5>
                                            <i class="fa fa-inr"> </i><b>&nbsp;&nbsp;<u>Contract Details</u>
                                            <span class="pull-right edit btn btn-default" data-id="contractDetail"><i class="fa fa-pencil"></i></span></b>
                                            <div class="clearfix"></div>
                                        </h5>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>Title of Contract</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 id="viewTitle">
                                            <?php echo isset($result1->titleContract) ? $result1->titleContract : ' ' ;?>
                                            </h6></div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>Nature of Contract</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 id="viewNatureOfContract"></h6></div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>TDS</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 id="viewTDS">
                                            <?php echo isset($result2->taxApplicable) ? 'YES' : 'NO' ;?>
                                            </h6></div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>Service Tax</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 id="viewServiceTax">YES</h6></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div> 
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="detail">
                                    
                                        <h5>
                                            <i class="fa fa-road"> </i><b> &nbsp;&nbsp;<u>Address</u>
                                            <span class="pull-right edit btn btn-default" data-id="address"><i class="fa fa-pencil"></i></span></b>
                                            <div class="clearfix"></div>
                                        </h5>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>Permanent Address</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 id="viewPermanentAddress">
                                                <?php echo isset($result2->permanentAddress) ? $result2->permanentAddress : ' ' ;?>
                                            </h6></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div> 
                                </div> 

                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="detail">
                                    
                                        <h5>
                                            <i class="fa fa-inr"> </i><b> &nbsp;&nbsp;<u>Payment Methods</u>
                                            <span class="pull-right edit btn btn-default" data-id="paymentMethod"><i class="fa fa-pencil"></i></span></b>
                                            <div class="clearfix"></div>
                                        </h5>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>Mode of Payment</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 id="viewModeOfPayment">
                                                <?php echo isset($result3->modeOfPayment) ? $result3->modeOfPayment :''; ?>
                                            </h6></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div> 
                                </div>                           
                            </div>

                            <div class="clearfix"></div>
                            
                            <div class="col-md-12 col-sm-12 col-xs-12">

                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="detail">
                                        
                                        <h5>
                                            <i class="fa fa-bank"></i><b> &nbsp;&nbsp; <u>Bank Details</u>
                                            <span class="pull-right edit btn btn-default" data-id="bankDetail"><i class="fa fa-pencil"></i></span></b>
                                            <div class="clearfix"></div>
                                        </h5>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>Name of Bank</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 id="viewNameOfBank">
                                            <?php echo isset($result3->bankName) ? $result3->bankName :''; ?>
                                            </h6></div>
                                        </div>
                                        <!-- <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>Branch</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 id="viewBranch">xxxx</h6></div>
                                        </div> -->
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>A/c no.</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 id="viewAccountNumber">
                                            <?php echo isset($result3->accountNumber) ? $result3->accountNumber :''; ?>
                                            </h6></div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>Type</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 id="viewType">
                                            <?php echo isset($result3->accountType) ? $result3->accountType :''; ?>
                                            </h6></div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6>IFSC</h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6 id="viewIFSC">
                                            <?php echo isset($result3->ifsc) ? $result3->ifsc :''; ?>
                                            </h6></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="detail">
                                        
                                        <h5>
                                            <i class="fa fa-users"> </i><b> &nbsp;&nbsp; <u>Reimbursements</u>
                                            <!-- <span class="pull-right edit btn btn-default" data-id="reimbursement"><i class="fa fa-pencil"></i></span> --></b>
                                            <div class="clearfix"></div>
                                        </h5>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6></h6></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6"><h6></h6></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                            </div>

                            <div class="clearfix"></div>
                            <br><br>
                        </div>
                        <!-- =========================  EDITOR  =================================== -->

                        <div class="editor hidden" id="personalDetail">
                            <form action="#" method="POST" class="form-horizontal">
                                <fieldset>
                                    <div class="clearfix"></div>
                                    <br>
                                    <legend>Edit Personal Detail</legend>
                                    <!-- identify type of form -->
                                    <input type="hidden" name="area" value="personal">

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-9 col-sm-10 col-xs-12">
                                            <div class="form-group label-floating">
                                                <label class="control-label" for="pan"><span>PAN</span></label>
                                                <input class="form-control" id="pan" name="pan" type="text" value="" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-9 col-sm-10 col-xs-12">
                                        <div class="form-group">
                                            <label for="dateOfBirth" class="col-md-4 control-label">D.O.B / D.O.I</label>

                                            <div class="col-md-8">
                                                <input type="text" id="dateOfBirth" name="dateOfBirth" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="reset" class="btn btn-raised btn-default cancel" value="Cancel">
                                        <input type="submit" class="btn btn-raised bg-theme save" value="Save">
                                    </div>

                                </fieldset>
                            </form>
                        </div>

                        <div class="editor hidden" id="address">
                            <form action="#" method="POST" class="form-horizontal">
                                <fieldset>
                                    <div class="clearfix"></div>
                                    <br>
                                    <legend>Edit Address</legend>
                                    <!-- identify type of form -->
                                    <input type="hidden" name="area" value="address">

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-9 col-sm-10 col-xs-12">
                                            <div class="form-group label-floating">
                                                <label class="control-label" for="permanentAddress"><span>Permanent</span></label>
                                                <input class="form-control" id="permanentAddress" name="permanentAddress" type="text" value="" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="reset" class="btn btn-raised btn-default cancel" value="Cancel">
                                        <input type="submit" class="btn btn-raised bg-theme save" value="Save">
                                    </div>

                                </fieldset>
                            </form>
                        </div>

                        <div class="editor hidden" id="contractDetail">
                            <form action="#" method="POST" class="form-horizontal">
                                <fieldset>
                                    <div class="clearfix"></div>
                                    <br>
                                    <legend>Edit Contract Detail</legend>
                                    <!-- identify type of form -->
                                    <input type="hidden" name="area" value="contract">

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-9 col-sm-10 col-xs-12">
                                            <div class="form-group label-floating">
                                                <label class="control-label" for="natureOfContract"><span>Nature Of Contract</span></label>
                                                <input class="form-control" id="natureOfContract" name="natureOfContract" type="text" value="" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-9 col-sm-10 col-xs-12">
                                            <div class="form-group label-floating">
                                                <label class="control-label" for="tds"><span>TDS</span></label>
                                                <input class="form-control" id="tds" name="tds" type="text" value="" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-9 col-sm-10 col-xs-12">
                                            <div class="form-group label-floating">
                                                <label class="control-label" for="serviceTax"><span>Service Tax</span></label>
                                                <input class="form-control" id="serviceTax" name="serviceTax" type="text" value="" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="reset" class="btn btn-raised btn-default cancel" value="Cancel">
                                        <input type="submit" class="btn btn-raised bg-theme save" value="Save">
                                    </div>

                                </fieldset>
                            </form>
                        </div>

                        <div class="editor hidden" id="bankDetail">
                            <form action="" method="POST" class="form-horizontal">
                                <fieldset>
                                    <div class="clearfix"></div>
                                    <br>
                                    <legend>Edit Bank Detail</legend>
                                    <!-- identify type of form -->
                                    <input type="hidden" name="area" value="bank">

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-9 col-sm-10 col-xs-12">
                                            <div class="form-group label-floating">
                                                <label class="control-label" for="nameOfBank"><span>Name Of Bank</span></label>
                                                <input class="form-control" id="nameOfBank" name="nameOfBank" type="text" value="" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <!-- <div class="col-md-9 col-sm-10 col-xs-12">
                                        <div class="form-group">
                                            <label for="branch" class="col-md-2 control-label">Branch</label>

                                            <div class="col-md-10">
                                                <select id="branch" name="branch" class="form-control">
                                                    <option value="abc">abc</option>
                                                    <option value="xyz">xyz</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div> -->

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-9 col-sm-10 col-xs-12">
                                            <div class="form-group label-floating">
                                                <label class="control-label" for="accountNumber"><span>Account Number</span></label>
                                                <input class="form-control" id="accountNumber" name="accountNumber" type="text" value="" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-9 col-sm-10 col-xs-12">
                                            <div class="form-group">
                                                <label for="type" class="col-md-2 control-label">Type</label>

                                                <div class="col-md-7">
                                                    <select id="type" name="type" class="form-control">
                                                        <option value="0">Select Account Type</option>
                                                        <option value="saving">Saving</option>
                                                        <option value="current">Current</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-9 col-sm-10 col-xs-12">
                                            <div class="form-group label-floating">
                                                <label class="control-label" for="ifsc"><span>IFSC</span></label>
                                                <input class="form-control" id="ifsc" name="ifsc" type="text" value="" style="cursor: auto;">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="reset" class="btn btn-raised btn-default cancel" value="Cancel">
                                        <input type="submit" class="btn btn-raised bg-theme save" value="Save">
                                    </div>

                                </fieldset>
                            </form>
                        </div>


                        <div class="editor hidden" id="paymentMethod">
                            <form class="form-horizontal" method="post">
                                <fieldset>
                                    <input type="hidden" name="area" value="payment">
                                    <legend>Edit Payment Method</legend>

                                    <div class="col-md-8 col-sm-10 col-xs-12">
                                        <div class="form-group">
                                            <label for="method" class="col-md-4 control-label">Mode Of Payment</label>

                                            <div class="col-md-8">
                                                <select id="modeOfPayment" name="modeOfPayment" class="form-control">
                                                    <option value="directDeposit">Direct Deposit</option>
                                                    <option value="cheque">Cash / Cheque</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="directDeposit">
                                        
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-8 col-sm-10 col-xs-12">
                                                <div class="form-group label-floating is-empty ">
                                                    <label class="control-label" for="bankName"><span>Name of Bank</span></label>
                                                    <input class="form-control" id="bankName" name="bankName" type="text" style="cursor: auto;">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-8 col-sm-10 col-xs-12">
                                                <div class="form-group label-floating is-empty ">
                                                    <label class="control-label" for="branch"><span>Branch</span></label>
                                                    <input class="form-control" id="branch" name="branch" type="text" style="cursor: auto;">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-8 col-sm-10 col-xs-12">
                                                <div class="form-group label-floating is-empty ">
                                                    <label class="control-label" for="accoundHolder"><span>Name as in Bank Account</span></label>
                                                    <input class="form-control" id="accoundHolder" name="accoundHolder" type="text" style="cursor: auto;">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-8 col-sm-10 col-xs-12">
                                                <div class="form-group label-floating is-empty ">
                                                    <label class="control-label" for="accountNumber"><span>Account Number</span></label>
                                                    <input class="form-control" id="accountNumber" name="accountNumber" type="text" style="cursor: auto;">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-8 col-sm-10 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Type of Account</label>

                                                <div class="col-md-8">
                                                    <div class="radio radio-primary">
                                                        <label>
                                                            <input type="radio" name="accountType" id="accountType1" value="saving" checked="">
                                                                Saving
                                                        </label>
                                                    </div>
                                                    <div class="radio radio-primary">
                                                        <label>
                                                            <input type="radio" name="accountType" id="accountType2" value="current">
                                                            Current
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-8 col-sm-10 col-xs-12">
                                                <div class="form-group label-floating is-empty ">
                                                    <label class="control-label" for="ifsc"><span>IFSC Code</span></label>
                                                    <input class="form-control" id="ifsc" type="text" style="cursor: auto;">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                    </div>

                                    <div id="cheque" class="hidden ">
                                        <div class="clearfix"></div>
                                        <br>
                                        <br>
                                        <br>
                                            <div class="col-md-12 col-sm-12 col-sx-12">
                                                <p class="lead">you have selected cheque / cash as payment option , <br> consult your employer as it will be provided to you personally</p>
                                            </div>
                                        <br>
                                    </div>


                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="reset" class="btn btn-raised btn-default cancel" value="Cancel">
                                        <input type="submit" class="btn btn-raised bg-theme save" value="Save">
                                    </div>
                                </fieldset>
                            </form>
                        </div>

                    
                    <br><br>

                    </div>


                    <style>
                        .detail{
                            box-shadow: 0 0 2px #888;
                            padding: 15px;
                            margin-top: 15px;
                            overflow-x: overlay;
                        }
                        .editor {
                            position: relative;
                            padding: 2em 0;
                        }
                        .edit {
                            margin: -15px;
                        }
                    </style>

                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>

</body>
</html>

<script>

    function displayError(msg) {
        swal({
            title : "Error !",
            text : msg,
            type : 'error',
            confirmButtonClass : 'bg-theme',
            animation : false,
            width : 400
        });
    };

    function closeAlert() {
        $(".sweet-alert button").first().trigger('click');
    }

    $(function() {
        $.material.init();

        $('#dateOfBirth').bootstrapMaterialDatePicker({ weekStart : 0, time: false }).on('dateSelected', function(event) {
            $('#'+$(this).data('dtp')).find('.dtp-btn-ok').trigger('click');
        });

        $("form.form-horizontal").keydown(function(event) {
            if (event.keyCode == 27) {
                $(this).find('.cancel').trigger('click');
            }
        });

        $("form.form-horizontal .cancel").click(function(event) {
            var $inputs = $(this).parent().parent().find('.form-group');
            $inputs.each(function(index, el) {
                if (!$(el).find('input').val()) {
                    $(el).removeClass('is-empty');
                }
                $(el).removeClass('has-error');
                $(el).find('help-block').remove();
            });

            $(this).parents(".editor").addClass('hidden');
            $(".main .panel-body #view").removeClass('hidden');
        });

        $(".edit").click(function(event) {
            $("#"+$(this).data('id')).removeClass('hidden');

            (function smoothscroll(){
                var currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
                if (currentScroll > 0) {
                     window.requestAnimationFrame(smoothscroll);
                     window.scrollTo (0,currentScroll - (currentScroll/5));
                }
            })();

            $(".main .panel-body #view").addClass('hidden');

            // Personal Detail
            if ($(this).data('id') == 'personalDetail') {
                $personalDetail = $(this).parents('.detail');

                var $form = $("#personalDetail .form-horizontal");

                $form.find('#pan').val($personalDetail.find('#viewPAN').text().trim()).trigger('change');
                $form.find('#dateOfBirth').val($personalDetail.find('#dateOfBirthVal').text().trim()).trigger('change');
            }
            else
                $personalDetail = null;

            // Location Detail
            if ($(this).data('id') == 'address') {
                $locationDetail = $(this).parents('.detail');

                var $form = $("#address .form-horizontal");

                $form.find('#permanentAddress').val($locationDetail.find('#viewPermanentAddress').text().trim()).trigger('change');
            }
            else
                $locationDetail = null;

            // Bank Detail
            if ($(this).data('id') == 'bankDetail') {
                $bankDetail = $(this).parents('.detail');

                var $form = $("#bankDetail .form-horizontal");

                $form.find('#nameOfBank').val($bankDetail.find('#viewNameOfBank').text().trim()).trigger('change');
                $form.find('#accountNumber').val($bankDetail.find('#viewAccountNumber').text().trim()).trigger('change');
                $form.find('#type option').filter(function() {
                    return $(this).val() == $bankDetail.find('#viewType').text().trim().toLocaleLowerCase(); 
                }).prop('selected', true);
                $form.find('#ifsc').val($bankDetail.find('#viewIFSC').text().trim()).trigger('change');
            }
            else
                $bankDetail = null;

            // Contract Detail
            if ($(this).data('id') == 'contractDetail') {
                $contractDetail = $(this).parents('.detail');

                var $form = $("#contractDetail .form-horizontal");

                $form.find('#natureOfContract').val($contractDetail.find('#viewNatureOfContract').text().trim()).trigger('change');
                $form.find('#tds').val($contractDetail.find('#viewTDS').text().trim()).trigger('change');
                $form.find('#serviceTax').val($contractDetail.find('#viewServiceTax').text().trim()).trigger('change');
            }
            else
                $contractDetail = null;
        });

        $("#personalDetail > form.form-horizontal").submit(function(event) {
            event.preventDefault();

            var $editor = $(this).parent(".editor");
            $(this).find(".help-block").remove();
            $(this).find(".has-error").removeClass('has-error');
            
            swal({
                title : 'Saving',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 400
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: $(this).serialize(),
            })
            .done(function(data) {
                if (data.status == 200) {
                    swal({
                        title : "Success !",
                        text : data.message,
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });   
                    $("#viewPAN").text($("#pan").val());
                    $("#viewDateOfBirth").text($("#dateOfBirth").val());
                    $editor.addClass('hidden');
                    $(".main .panel-body #view").removeClass('hidden');
                }
                else{  // Validation Error
                    displayError('Validation Error');
                    // show Errors
                    for(var fields in data) {
                        $('#'+fields).parents('.form-group').addClass('has-error');
                        $('#'+fields).parent().append('<p class="help-block">'+ data[fields][0] +'</p>');
                    }
                }
            })
            .error(function() {
                displayError('Try Again !');
            });
        });

        $("#address > form.form-horizontal").submit(function(event) {
            event.preventDefault();

            var $editor = $(this).parent(".editor");
            $(this).find(".help-block").remove();
            $(this).find(".has-error").removeClass('has-error');
            
            swal({
                title : 'Saving',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 400
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: $(this).serialize(),
            })
            .done(function(data) {
                if (data.status == 200) {
                    swal({
                        title : "Success !",
                        text : data.message,
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });   
                    $("#viewPermanentAddress").text($("#permanentAddress").val());
                    $editor.addClass('hidden');
                    $(".main .panel-body #view").removeClass('hidden');
                }
                else{  // Validation Error
                    displayError('Validation Error');
                    // show Errors
                    for(var fields in data) {
                        $('#'+fields).parents('.form-group').addClass('has-error');
                        $('#'+fields).parent().append('<p class="help-block">'+ data[fields][0] +'</p>');
                    }
                }
            })
            .error(function() {
                displayError('Try Again !');
            });
        });

        $("#contractDetail > form.form-horizontal").submit(function(event) {
            event.preventDefault();

            var $editor = $(this).parent(".editor");
            $(this).find(".help-block").remove();
            $(this).find(".has-error").removeClass('has-error');
            
            swal({
                title : 'Saving',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 400
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: $(this).serialize(),
            })
            .done(function(data) {
                if (data.status == 200) {
                    swal({
                        title : "Success !",
                        text : data.message,
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });   
                    $("#viewNatureOfContract").text($("#natureOfContract").val());
                    $("#viewTDS").text($("#tds").val());
                    $("#viewServiceTax").text($("#serviceTax").val());
                    $editor.addClass('hidden');
                    $(".main .panel-body #view").removeClass('hidden');
                }
                else{  // Validation Error
                    displayError('Validation Error');
                    // show Errors
                    for(var fields in data) {
                        $('#'+fields).parents('.form-group').addClass('has-error');
                        $('#'+fields).parent().append('<p class="help-block">'+ data[fields][0] +'</p>');
                    }
                }
            })
            .error(function() {
                displayError('Try Again !');
            });
        });

        $("#bankDetail > form.form-horizontal").submit(function(event) {
            event.preventDefault();

            var $editor = $(this).parent(".editor");
            $(this).find(".help-block").remove();
            $(this).find(".has-error").removeClass('has-error');
            
            swal({
                title : 'Saving',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 400
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: $(this).serialize(),
            })
            .done(function(data) {
                if (data.status == 200) {
                    swal({
                        title : "Success !",
                        text : data.message,
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });   
                    $("#viewNameOfBank").text($("#nameOfBank").val());
                    $("#viewBranch").text($("#branch").val());
                    $("#viewAccountNumber").text($("#accountNumber").val());
                    $("#viewType").text($("#type").val());
                    $("#viewIFSC").text($("#ifsc").val());
                    $editor.addClass('hidden');
                    $(".main .panel-body #view").removeClass('hidden');
                }
                else{  // Validation Error
                    displayError('Validation Error');
                    // show Errors
                    for(var fields in data) {
                        $('#'+fields).parents('.form-group').addClass('has-error');
                        $('#'+fields).parent().append('<p class="help-block">'+ data[fields][0] +'</p>');
                    }
                }
            })
            .error(function() {
                displayError('Try Again!');
            });
        });

        $("#paymentMethod > form.form-horizontal").submit(function(event) {
            event.preventDefault();

            var $editor = $(this).parent(".editor");
            $(this).find(".help-block").remove();
            $(this).find(".has-error").removeClass('has-error');
            
            swal({
                title : 'Saving',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 400
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: $(this).serialize(),
            })
            .done(function(data) {
                if (data.status == 200) {
                    swal({
                        title : "Success !",
                        text : data.message,
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });
                    if ($("#modeOfPayment").val() == 'directDeposit') $("viewModeOfPayment").val('Direct Deposit');
                    else $("viewModeOfPayment").val('Cash / Cheque');
                    $editor.addClass('hidden');
                    $(".main .panel-body #view").removeClass('hidden');
                }
                else{  // Validation Error
                    displayError('Validation Error');
                    // show Errors
                    for(var fields in data) {
                        $('#'+fields).parents('.form-group').addClass('has-error');
                        $('#'+fields).parent().append('<p class="help-block">'+ data[fields][0] +'</p>');
                    }
                }
            })
            .error(function() {
                displayError('Try Again!');
            });
        });

        // payment method
        $("#modeOfPayment").change(function(event) {
            var val = $(this).val();
            if (val == 'cheque') {
                $("#directDeposit").addClass('hidden');
                $("#directDeposit input").val('').trigger('change');
                $("#cheque").removeClass('hidden');
            }
            else {
                $("#cheque").addClass('hidden');
                $("#directDeposit").removeClass('hidden');
                $("#directDeposit input").val('').trigger('change');
            }
        }); 

    });
</script>
