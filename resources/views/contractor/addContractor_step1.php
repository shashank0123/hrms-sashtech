<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>
<style>
   

</style>

<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">

                <div class="breadcrumb flat text-center row">
                    <span class="active col-md-4 col-sm-4 col-xs-12">Personal Details</span>
                    <span class="col-md-4 col-sm-4 col-xs-6 hidden-xs">Tax Details</span>
                    <span class="col-md-4 col-sm-4 col-xs-6 hidden-xs">Payment Details</span>
                </div>


                <br>
                <br>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <form class="form-horizontal" method="post">
                        <fieldset>
                            <legend>Personal Details</legend>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="form-group label-floating is-empty ">
                                        <label class="control-label" for="pan"><span>PAN</span></label>
                                        <input class="form-control" id="pan" name="pan" type="text" style="cursor: auto;">
                                    </div>
                                </div>
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p>Permanent Account Number (PAN) is a ten-digit alphanumeric number, issued in by the Income Tax Department</p>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label for="DateOfBirth" class="control-label">DOB / DOI</label>
                                        <input type="text" id="DateOfBirth" name="DateOfBirth" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="form-group label-floating">
                                        <label for="permanentAddress" class="control-label">Address</label>
                                        <textarea class="form-control" rows="2" name="permanentAddress" id="permanentAddress"></textarea>
                                    </div> 
                                </div>
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p>Contractor address. It can be anywhere in the India</p>
                                </div>                               
                            </div>

                            <div class="col-md-8 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="col-md-8 col-sm-8 col-xs-8 col-md-offset-4 col-sm-offset-3 col-xs-offset-4">
                                        <span class="btn btn-default btn-raised cancel" onclick="history.back();">Cancel</span>
                                        <input type="submit" class="next btn bg-theme next btn-raised" value="next">
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
                    </div>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>
    $(function() {
        $.material.init();

        $('#DateOfBirth').bootstrapMaterialDatePicker({ weekStart : 0, time: false }).on('dateSelected', function(event) {
            $('#'+$(this).data('dtp')).find('.dtp-btn-ok').trigger('click');
        });

        $("#DateOfBirth").bootstrapMaterialDatePicker('setDate', moment(new Date(1980, 0,1)));

        function displayError() {
            swal({
                title : "Error !",
                text : 'Try Again!',
                type : 'error',
                confirmButtonClass : 'bg-theme',
                animation : false,
                width : 400
            });
        };

        function closeAlert() {
            $(".sweet-alert button").first().trigger('click');
        }

        // $('.cancel').click(function(event) {
        //     $(".help-block").remove();
        //     $(".has-error").removeClass('has-error');
        //     event.preventDefault();
        //     var $form = $(this).parents('form')[0];
        //     $form.reset();
        //     $($form).find('.form-group.label-floating').addClass('is-empty');
        // });

        $('form.form-horizontal').submit(function(event) {

            event.preventDefault();

            // remove all error messages
            $(".help-block").remove();
            $(".has-error").removeClass('has-error');
            
            swal({
                title : 'Saving',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 400
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: $('.main form').serialize(), 
            })
            .done(function(data) {
                if (data.status == 200) {
                    swal({
                        title : "Success !",
                        text : data.message,
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });   
                    getAndInsert(data.url);
                    history.pushState(null, null, data.url);
                }
                else{  // Validation Error
                    displayError();
                    // show Errors
                    for(var fields in data) {
                        $('#'+fields).parents('.form-group').addClass('has-error');
                        $('#'+fields).parent().append('<p class="help-block">'+ data[fields][0] +'</p>');
                    }
                }
            })
            .error(function() {
                displayError();
            });
        });

        $(".main .form-horizontal input").trigger('change');
    });
</script>
