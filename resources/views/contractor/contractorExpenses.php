<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>


<body  style="margin-top:4em;">
    <?php require_once('Menu.php');?>
    <div class="main panel panel-default col-md-9 col-sm-9">
        <div class="panel-body" style="min-height:36em;">

            <br>

            <h2>Expenses</h2>
            <br>

            <form action="" class="form-horizontal" id="expense" enctype="multipart/form-data">        
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tr>
                            <th width="100"><h6>Date of Expense</h6></th>
                            <th><h6>Description</h6></th>
                            <th><h6>Upload Document</h6></th>
                            <th><h6>Amount</h6></th>
                            <th><h6>Status</h6></th>
                            <th width="150"><h6>Expense Id</h6></th>
                            <th> &nbsp; </th>
                            <th> &nbsp; </th>
                        </tr>
                        <?php if(!empty($expenses)): ?>
                        <?php foreach ($expenses as $key => $value): ?>
                        <tr> 
                            <td> 
                                <div class="form-group form-group-sm"> 
                                    <input class="form-control dateOfExpense" id= "dateOfExpense<?php echo $key; ?>" type="text" name="dateOfExpense" value="<?php echo $value->dateOfExpense; ?>" > 
                                </div> 
                            </td> 
                            <td> 
                                <div class="form-group form-group-sm"> 
                                    <input class="form-control description" id="description<?php echo $key; ?>" name="description" type="text" value="<?php echo $value->description; ?>" > 
                                </div> 
                            </td> 
                            <td> 
                                <div class="form-group form-group-sm is-fileinput"> 
                                    <input type="text" readonly class="form-control" placeholder="Browse..."> 
                                    <input type="file" name="doc" class="docUrl" id="docUrl<?php echo $key; ?>"> 
                                </div> 
                            </td> 
                            <td> 
                                <div class="form-group form-group-sm"> 
                                    <input class="form-control amount" id="amount<?php echo $key; ?>" name="amount" value="<?php echo $value->amount; ?>"  type="number" min=0> 
                                </div> 
                            </td> 
                            <td class="text-center statusMessage" id="statusMessage<?php echo $key; ?>"> 
                                <?php echo $value->status; ?> 
                            </td> 
                            <td > 
                                <div class="form-group form-group-sm"> 
                                    <input class="form-control expenseId" type="text" name="expenseId" value="<?php echo $value->expenseId; ?>" id="expenseId<?php echo $key; ?>" disabled> 
                                </div> 
                            </td> 
                            <td class="text-center edit"><i class="fa fa-pencil"></i></td> 
                            <td class="text-center delete"><i class="fa fa-times"></i></td>
                        </tr>
                        <?php endforeach; ?>
                        <?php endif; ?>
                    </table>
                </div>
                <span class="btn btn-raised bg-theme pull-right add"><i class="fa fa-plus"></i> Add</span>
            </form>

            <style>
                .delete, .edit{
                    cursor: pointer;
                }
            </style>
        </div>
    </div>
</div>
</div>

    <br><br>
    <?php require_once('footer.php');?>

</body>
</html>

<script>

    function displayError() {
        swal({
            title : "Error !",
            text : 'Try Again!',
            type : 'error',
            confirmButtonClass : 'bg-theme',
            animation : false,
            width : 400
        });
    };

    function closeAlert() {
        $(".sweet-alert button").first().trigger('click');
    }

    $(function() {

        var noOfExpense = $('.table.table-bordered tr').length - 1;

        $.material.init();

        $(".add, td.edit").click(handleExpense);
        $("td.delete").click(deleteExpense);

        function handleExpense(event) {
            event.preventDefault();

            $(".help-block").remove();
            swal({
                title : 'Saving',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 400
            });

            var $row = null;
            if ($(this).hasClass('add')) {
                $row = $(".table.table-bordered tr").last();
            }
            else {
                $row = $(this).parent();
            }
            var dateOfExpense = $row.find('.dateOfExpense').val();
            var description = $row.find('.description').val();
            var amount = $row.find('.amount').val();
            var docUrl = $row.find('.docUrl')[0].files[0];
            var expenseId = $row.find('.expenseId').val();

            var formdata = new FormData();
            formdata.append('dateOfExpense', dateOfExpense);
            formdata.append('description', description);
            formdata.append('amount', amount);
            formdata.append('doc', docUrl || "");
            formdata.append('expenseId', expenseId);

            if (expenseId)
                formdata.append('action', 'edit');
            else
                formdata.append('action', 'add');

            $.ajax({
                url: location.href,
                type: 'POST',
                data: formdata,
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false 
            })
            .done(function(data) {
                if (data.status == 200) {
                    $row.find('.expenseId').val(data.expenseId);
                    swal({
                        title : "Success !",
                        text : data.message,
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });
                    if (!expenseId) noOfExpense++;
                    addExpense();
                }
                else if(data.status == 201){
                    $row.find('.statusMessage').text(data.statusMessage);
                    swal({
                        title : "Success !",
                        text : data.message,
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });
                }
                else {
                    displayError();
                    // show Errors
                    for(var fields in data) {
                        $row.find('.'+fields).parents('.form-group').addClass('has-error');
                        $row.find('.'+fields).parent().append('<p class="help-block">'+ data[fields][0] +'</p>');
                    }
                }
            })
            .fail(function() {
                displayError();
            })
        };

        function deleteExpense(event) {
            event.preventDefault();

            var $row = $(this).parent();
            var expenseId = $row.find('.expenseId').val();

            swal({
                title : 'Deleting',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 400
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: {expenseId: expenseId, action : 'delete'},
            })
            .done(function(data) {
                if (data.status == 410) {
                    swal({
                        title : "Success !",
                        text : data.message,
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });
                    $row.remove();
                }
                else{
                    displayError();
                }
            })
            .fail(function() {
                displayError();
            });
        }

        function addExpense() {
            var $html = $('<tr>'+
                            '<td>'+
                                '<div class="form-group form-group-sm">'+
                                    '<input class="form-control dateOfExpense" id= "dateOfExpense'+noOfExpense+'" type="text" name="dateOfExpense">'+
                                '</div>'+
                            '</td>'+
                            '<td>'+
                                '<div class="form-group form-group-sm">'+
                                    '<input class="form-control description" id="description'+noOfExpense+'" name="description" type="text">'+
                                '</div>'+
                            '</td>'+
                            '<td>'+
                                '<div class="form-group form-group-sm is-fileinput">'+
                                    '<input type="text" readonly class="form-control" placeholder="Browse...(MAX 500KB)">'+
                                    '<input type="file" name="doc" class="docUrl" id="docUrl'+noOfExpense+'">'+
                                '</div>'+
                            '</td>'+
                            '<td>'+
                                '<div class="form-group form-group-sm">'+
                                    '<input class="form-control amount" id="amount'+noOfExpense+'" name="amount" type="number" min=0>'+
                                '</div>'+
                            '</td>'+
                            '<td class="text-center statusMessage" id="statusMessage'+noOfExpense+'">'+
                                'Waiting'+
                            '</td>'+
                            '<td >'+
                                '<div class="form-group form-group-sm">'+
                                    '<input class="form-control expenseId" type="text" name="expenseId" id="expenseId'+noOfExpense+'" disabled>'+
                                '</div>'+
                            '</td>'+
                            '<td class="text-center edit"><i class="fa fa-pencil"></i></td>'+
                            '<td class="text-center delete"><i class="fa fa-times"></i></td>');

            $(".table.table-bordered").append($html);

            $html.find("td.text-center").css('padding', '5px');
            $html.find(".dateOfExpense").css('text-align', 'center');
            $html.find(".text-center").css('vertical-align', 'middle');
            $html.find(".form-group.form-group-sm").css('margin-top', '5px');
            $html.find('.dateOfExpense').bootstrapMaterialDatePicker({ weekStart : 0, time: false }).on('dateSelected', function(event) {
                $('#'+$(this).data('dtp')).find('.dtp-btn-ok').trigger('click');
            });
            $html.find('.dateOfExpense').bootstrapMaterialDatePicker('setDate', moment());

            $.material.init();

            $html.find('td.delete').click(deleteExpense);

            $html.find('td.edit').click(handleExpense);

            // $html.find('.edit').click(handleExpense);
        }

        addExpense();
        $("td.text-center").css('padding', '5px');
        $(".text-center").css('vertical-align', 'middle');
    })


</script>
