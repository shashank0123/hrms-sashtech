<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>


<body  style="margin-top:4em;">
<?php
    if (!empty($result)) 
    {
        $date           = strtotime($result->payDay);
        $day            = date('d', $date);

        $currentDay     = date("d");
        $currentMonth   = date('m');
        // $currentMonth   = 12;
        $currentYear    = date('Y');
        
        if ($currentDay > $day) 
        {
            if (($currentMonth + 1)>12) 
            {
                $dateObj    = DateTime::createFromFormat('!m', 1);
                $payDay     = $day . '-' . $dateObj->format('M') . '-' . ($currentYear+1);
            }
            else 
            {
                $dateObj    = DateTime::createFromFormat('!m', $currentMonth+1);
                $payDay     = $day . '-' . $dateObj->format('M') . '-' . $currentYear;
            }
        }
        elseif($currentDay == $day)
        {
            $payDay     = 'Today!';
        }
        else
        {
            $dateObj    = DateTime::createFromFormat('!m', $currentMonth);
            $payDay     = $day . '-' . $dateObj->format('M') . '-' . $currentYear;
        }
    }
?>
<?php 
    foreach ($result3 as $key => $value) 
    {
        $year = date('Y');

        $today = date('Y-m-d');
        $month = date('m');
        $currentDay = date('d');

        // Following values were used for testing
        // $today = '28-12-2016';
        // $month = 12;
        // $currentDay = 28;

        if ($month == 12) 
        {
            if ($currentDay > 24)
            {
                $bdayMonth = date('m', strtotime($value->DateOfBirth));
                if ($bdayMonth == 1)
                {
                    $bdayDate = date('d', strtotime($value->DateOfBirth));

                    $next = date_create(date('d-m-', strtotime($value->DateOfBirth)) . ($year+1));

                    $diff = date_diff($next, date_create($today));

                    if ($diff->days < 8)
                    {
                        $year +=1;
                    }
                }
            }
        }

        $next =  $year . date('-m-d', strtotime($value->DateOfBirth));
        
        $diff = date_diff(date_create($next), date_create($today));
        
        if ((($diff->days < 8) and ($diff->invert == 1)) or ($diff->days == 0))
        {
            // $bDay[$value->userid] = $next;

            foreach ($result4 as $key2 => $value2) 
            {
                if ($value->userid == $value2->id) 
                {
                    $name = $value2->firstName . ' ' . $value2->lastName;
                }
            }

            $bDay[] = array(
                'id' => $value->userid, 
                'name' => $name,
                'on' => $next,
                );
        }
    }

    if (isset($bDay)) 
    {
        for ($i=0; $i < count($bDay) ; $i++) 
        {
            for ($j=0; $j < count($bDay); $j++) 
            {
                if ($bDay[$i] == $bDay[$j]) 
                {
                    continue;
                }

                $datetime1  = new DateTime($bDay[$j]['on']);
                $datetime2  = new DateTime($bDay[$i]['on']);

                $interval   = $datetime1->diff($datetime2);

                $invert     = $interval->invert;

                if ($invert == 1)
                {
                    $temp       = $bDay[$i];
                    $bDay[$i]   = $bDay[$j];
                    $bDay[$j]   = $temp;
                }
            }
        }
    }
?>
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">
                        <div id="wrapper">

                            <div id="page-wrapper">

                                <div class="container-fluid row">
                                    <!-- /.row -->
                                    <br><br>

                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <div class="">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <div class="row">
                                                        <div class="col-xs-3">
                                                            <i class="fa fa-inr fa-4x"></i>
                                                        </div>
                                                        <div class="col-xs-9 text-right">
                                                            <div class="huge"> <?php echo isset($payDay) ? $payDay : ''; ?> </div>
                                                            <div>Company next pay day</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="">
                                            <div class="panel panel-green">
                                                <div class="panel-heading">
                                                    <div class="row">
                                                        <div class="col-xs-3">
                                                            <i class="fa fa-heartbeat fa-4x"></i>
                                                        </div>
                                                        <div class="col-xs-9 text-right">
                                                            <div class="huge">xxxx</div>
                                                            <div>Maximum coverage</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="">
                                            <div class="panel panel-yellow">
                                                <div class="panel-heading">
                                                    <div class="row">
                                                        <div class="col-xs-3">
                                                            <i class="fa fa-paper-plane fa-4x"></i>
                                                        </div>
                                                        <div class="col-xs-9 text-right">
                                                            <div class="huge">2016/04/1</div>
                                                            <div>Next Holiday</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="">
                                            <div class="panel panel-red">
                                                <div class="panel-heading">
                                                    <div class="row">
                                                        <div class="col-xs-3">
                                                            <i class="fa fa-money fa-4x"></i>
                                                        </div>
                                                        <div class="col-xs-9 text-right">
                                                            <div class="huge">
                                                            <?php echo isset($result2->amount) ? '<i class="fa fa-inr"></i> ' . $result2->amount : 'No Expense' ; ?></div>
                                                            <div>Last expense</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h3 class="panel-title"><i class="fa fa-long-arrow-right fa-fw"></i> Donut Chart</h3>
                                            </div>
                                            <div class="panel-body">
                                                <div id="morris-donut-chart"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <br>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h3 class="panel-title"><i class="fa fa-clock-o fa-fw"></i> Notifications</h3>
                                            </div>
                                            <div class="panel-body">
                                                <div class="list-group">
                                                    <h5>Quote of the Day</h5>
                                                    <span class="list-group-item">
                                                        <i class="fa fa-fw fa-bullhorn"></i> 
                                                        "<?php echo $result5->quote; ?>" - <?php echo $result5->author; ?>
                                                    </span>
                                                </div>

                                                <?php if (!empty($bDay)): ?>
                                                <h5>Upcoming Birthdays</h5>
                                                <div class="list-group">
                                                    <?php foreach ($bDay as $key => $value): ?>
                                                        <?php 
                                                            if ($value['on'] < date('d-m-Y')) 
                                                            {
                                                                continue;
                                                            }
                                                         ?>
                                                        <?php if(session()->get('employeeId') == $value['id']): ?>
                                                            <?php if (date_diff(date_create(date('d-m-Y', strtotime($value['on']))), date_create(date('d-m-Y')))->days == 0): ?>
                                                            <span class="list-group-item">
                                                                <i class="fa fa-fw fa-birthday-cake"></i> Happy Birthday! :D
                                                            </span>
                                                            <?php else: ?>
                                                            <span class="list-group-item">
                                                                <span class="badge"><?php echo date('d-M', strtotime($value['on'])); ?></span>
                                                                <i class="fa fa-fw fa-birthday-cake"></i> Your birthday is coming on
                                                            </span>
                                                            <?php endif; ?>
                                                        <?php else: ?>
                                                            <?php if (date_diff(date_create(date('d-m-Y', strtotime($value['on']))), date_create(date('d-m-Y')))->days == 0): ?>
                                                            <span class="list-group-item">
                                                                <span class="badge">Today!</span>
                                                                <i class="fa fa-fw fa-birthday-cake"></i> It's <?php echo $value['name'] . "'s"; ?> birthday today!
                                                            </span>
                                                            <?php else: ?>
                                                            <span class="list-group-item">
                                                                <span class="badge"><?php echo date('d-M', strtotime($value['on'])); ?></span>
                                                                <i class="fa fa-fw fa-birthday-cake"></i> Upcoming birthday of <?php echo $value['name']; ?> on
                                                            </span>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                </div>
                                                <?php endif; ?>

                                                <?php if (!empty($result7)) : ?>
                                                <h5>Tweets</h5>
                                                <div class="list-group">
                                                    <?php foreach($result7 as $key => $value): ?>
                                                        <?php 
                                                            $datetime1  = new DateTime($value->created_at);
                                                            $datetime2  = new DateTime(date('d-m-Y h:i:sa'));

                                                            $interval   = $datetime1->diff($datetime2);

                                                            if ($interval->y < 1) 
                                                            {
                                                                if ($interval->m == 0) 
                                                                {
                                                                    if ($interval->d == 0) 
                                                                    {
                                                                        if ($interval->h == 0) 
                                                                        {
                                                                            if ($interval->i == 0) { $time = $interval->s . ' seconds ago' ; }
                                                                            else { $time = $interval->i . ' mins ago' ; }
                                                                        }
                                                                        else { $time = $interval->h . ' hours ago' ; }
                                                                    }
                                                                    else { $time = $interval->d . ' days ago' ;
                                                                    } }
                                                                elseif ($interval->m == 1) 
                                                                {
                                                                    $time = $interval->m . ' month and ' . $interval->d . ' days ago' ;
                                                                }
                                                                else { $time = $interval->m . ' months and ' . $interval->d . ' days ago' ; }
                                                            }
                                                            else { $time = 'Older than a year'; }
                                                         ?>

                                                    <span class="list-group-item">
                                                        <span class="badge"><?php echo $time ?></span>
                                                        <i class="fa fa-fw fa-comment"></i>
                                                        <?php echo htmlspecialchars_decode($value->messageContent); ?>
                                                    </span>
                                                    <?php endforeach; ?>
                                                </div>
                                                <?php endif; ?>

                                                <?php if(!empty($result6)): ?>
                                                <h5>Company Policy Updates</h5>
                                                <div class="list-group">
                                                    <?php foreach ($result6 as $key => $value) : ?>
                                                        <?php 
                                                            $datetime1  = new DateTime($value->created_at);
                                                            $datetime2  = new DateTime(date('d-m-Y h:i:sa'));

                                                            $interval   = $datetime1->diff($datetime2);

                                                            if ($interval->y < 1) 
                                                            {
                                                                if ($interval->m == 0) 
                                                                {
                                                                    if ($interval->d == 0) 
                                                                    {
                                                                        if ($interval->h == 0) 
                                                                        {
                                                                            if ($interval->i == 0) { $time = $interval->s . ' seconds ago' ; }
                                                                            else { $time = $interval->i . ' mins ago' ; }
                                                                        }
                                                                        else { $time = $interval->h . ' hours ago' ; }
                                                                    }
                                                                    else { $time = $interval->d . ' days ago' ;
                                                                    } }
                                                                elseif ($interval->m == 1) 
                                                                {
                                                                    $time = $interval->m . ' month and ' . $interval->d . ' days ago' ;
                                                                }
                                                                else { $time = $interval->m . ' months and ' . $interval->d . ' days ago' ; }
                                                            }
                                                            else { $time = 'Older than a year'; }
                                                         ?>
                                                    <span class="list-group-item">
                                                        <span class="badge"><?php echo $time; $time = NULL; ?></span>
                                                        <i class="fa fa-fw fa-bullhorn"></i> Your employer added a new company policy - "<?php echo $value->policyName; ?>". Please check policies for more info.
                                                    </span>
                                                    <?php endforeach; ?>
                                                </div>
                                                <?php endif; ?>
                                                <!-- <div class="text-right">
                                                    <a href="#">View All Activity <i class="fa fa-arrow-circle-right"></i></a>
                                                </div> -->
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <!-- /.container-fluid -->

                            </div>
                            <!-- /#page-wrapper -->

                        </div>

                    </div>

                    <style>
                        .huge {
                            font-size: 30px;
                        }
                    </style>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>
    $(function() {
        $.material.init();
        
        // Donut Chart
        Morris.Donut({
            element: 'morris-donut-chart',
            data: [{
                label: "Free Users",
                value: 12
            }, {
                label: "Paid Users",
                value: 20
            }],
            resize: true
        });
    })
</script>
