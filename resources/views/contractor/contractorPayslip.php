<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
    <style>
    .table { width: 400px; }
    .table tr > td:nth-child(1) { width: 200px; }
    .table tr > td:nth-child(2) { text-align: right; }
    </style>
</head>

<body  style="margin-top:4em;">
<?php   
    
    $taxslip = null;

 ?>
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">
                        <br>
                        <h2>Payslips</h2>
                        <hr>

                        <br>

                        <?php if(isset($result) and empty($result)): ?>
                            <p class="clearfix">No payslips to show yet.</p>
                        <?php else: ?>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tbody>
                                <?php foreach($result as $key => $value): ?>
                                    <tr>
                                        <td><?php echo $value->month . ' ' . explode('-', $value->financialYear)[0]; ?></td>
                                        <td><?php echo date('d/m/Y', strtotime($value->created_at)); ?> &nbsp; <a target="_blank" href="/payslip/<?php echo $value->financialYear .'/'. $value->month ?>"><b>Payslip</b></a></td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <?php endif; ?>

                        <br>
                        <br>
                        <br>

                        <h2>Taxslip</h2>
                        <hr>

                        <br>    

                        <?php if(!empty($taxslip)): ?>

                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tr>
                                    <td>Up To (December 2015)</td>
                                    <td> 01/01/2016 &nbsp;  <a target="_blank" href="/taxslip"><b>Taxslip</b></a></td>
                                </tr>
                            </table>
                        </div>
                        <?php else: ?>
                            <p class="clearfix">No taxslips to show yet.</p>
                        <?php endif; ?>

                    </div>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>
    $.material.init();
</script>
