<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>

<style>
     #form_type > div > div{
        border-top:2px solid #ddd;
        padding: 5px 0;
    }

    #form_type > div.active > div{
        border-color:#00f;
    }

    .form-group{
        margin-top: 5px;
    }

</style>

<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">

                        <div class="breadcrumb flat text-center row">
                            <span class="col-md-3 col-sm-3 col-xs-6 hidden-xs">The Basics</span>
                            <span class="col-md-3 col-sm-3 col-xs-6 hidden-xs">Personal Details</span>
                            <span class="col-md-3 col-sm-3 col-xs-6 hidden-xs">Tax Details</span>
                            <span class="active col-md-3 col-sm-3 col-xs-12">Payment Details</span>
                        </div>


                        <br>
                        <br>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <form class="form-horizontal" method="post">
                                <fieldset>
                                    <legend>Payment Method</legend>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                            <div class="form-group label-floating">
                                                <label for="method" class="control-label">Mode Of Payment</label>
                                                <select id="modeOfPayment" name="modeOfPayment" class="form-control">
                                                    <option value="directDeposit">Direct Deposit</option>
                                                    <option value="cheque">Cash / Cheque</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                            <p> Be sure to cross check these values. We will verify the account by completing a test transaction</p>
                                        </div>
                                    </div>

                                    <div id="directDeposit">
                                        
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                                <div class="form-group label-floating is-empty ">
                                                    <label class="control-label" for="bankName"><span>Name of Bank</span></label>
                                                    <input class="form-control" id="bankName" name="bankName" type="text" style="cursor: auto;">
                                                </div>
                                            </div>
                                            <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                                <p>Name of the bank where you have account</p>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                                <div class="form-group label-floating is-empty ">
                                                    <label class="control-label" for="branch"><span>Branch</span></label>
                                                    <input class="form-control" id="branch" name="branch" type="text" style="cursor: auto;">
                                                </div>
                                            </div>
                                            <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                                <p>Branch of the bank where you have account</p>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                                <div class="form-group label-floating is-empty ">
                                                    <label class="control-label" for="accountHolder"><span>Name as in Bank Account</span></label>
                                                    <input class="form-control" id="accountHolder" name="accountHolder" type="text" style="cursor: auto;">
                                                </div>
                                            </div>
                                            <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                                <p>This name will be displayed on the employee paycheck to represent the employee bank account</p>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                                <div class="form-group label-floating is-empty ">
                                                    <label class="control-label" for="accountNumber"><span>Account Number</span></label>
                                                    <input class="form-control" id="accountNumber" name="accountNumber" type="text" style="cursor: auto;">
                                                </div>
                                            </div>
                                            <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                                <p>Account number of Bank account</p>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Type of Account</label>

                                                <div class="col-md-8">
                                                    <div class="radio radio-primary">
                                                        <label>
                                                            <input type="radio" name="accountType" id="accountType1" value="saving" checked="">
                                                                Saving
                                                        </label>
                                                    </div>
                                                    <div class="radio radio-primary">
                                                        <label>
                                                            <input type="radio" name="accountType" id="accountType2" value="current">
                                                            Current
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-8 col-sm-12 col-xs-12">
                                                <div class="form-group label-floating is-empty ">
                                                    <label class="control-label" for="ifsc"><span>IFSC Code</span></label>
                                                    <input class="form-control" id="ifsc" name="ifsc" type="text" style="cursor: auto;">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                    </div>

                                    <div id="cheque" class="hidden ">
                                        <div class="clearfix"></div>
                                        <br>
                                        <br>
                                        <br>
                                            <div class="col-md-8 col-sm-12 col-sx-12">
                                                <p class="lead">You have selected cash/cheque as payment option, <br> Consult your employer as it will be provided to you personally.</p>
                                            </div>
                                        <br>
                                    </div>


                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <div class="col-md-8 col-sm-8 col-xs-8 col-md-offset-4 col-sm-offset-4 col-xs-offset-4">
                                                <input type="submit" class="next btn bg-theme next btn-raised" value="save">
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>

    function displayError() {
        swal({
            title : "Error !",
            text : 'Try Again!',
            type : 'error',
            confirmButtonClass : 'bg-theme',
            animation : false,
            width : 400
        });
    };
    
    $(function() {
        $.material.init();

        $("#modeOfPayment").change(function(event) {
            var val = $(this).val();
            if (val == 'cheque') {
                $("#directDeposit").addClass('hidden');
                $("#directDeposit input").val('').trigger('change');
                $("#cheque").removeClass('hidden');
            }
            else {
                $("#cheque").addClass('hidden');
                $("#directDeposit").removeClass('hidden');
                $("#directDeposit input").val('').trigger('change');
            }
        });  

        $("form.form-horizontal").submit(function(event) {

            event.preventDefault();

            // remove all error messages
            $(".help-block").remove();
            $(".has-error").removeClass('has-error');
            
            swal({
                title : 'Saving',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 400
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: $('.main form').serialize(), 
            })
            .done(function(data) {
                if (data.status == 200) {
                    swal({
                        title : "Success !",
                        text : data.message,
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });   
                    getAndInsert(data.url);
                    history.pushState(null, null, data.url);
                }
                else{  // Validation Error
                    displayError();
                    // show Errors
                    for(var fields in data) {
                        $('#'+fields).parents('.form-group').addClass('has-error');
                        $('#'+fields).parent().append('<p class="help-block">'+ data[fields][0] +'</p>');
                    }
                }
            })
            .error(function() {
                displayError();
            });
        });

        $(".main .form-horizontal input").trigger('change');
    })
</script>
