<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>

<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">
                        <br>
                        <br>
                        <form action="" method="POST" class="form-horizontal">
                        <legend>Company Policy</legend>

                        <?php if (isset($result)): ?>
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tr>
                                        <th>Policy Name</th>
                                        <th>Description</th>
                                        <th>Completion Date</th>
                                        <th>status</th>
                                        <th>pdf</th>
                                        <th>Initial</th>
                                    </tr>
                                     <?php foreach ($result as $key => $value):?>
                                    <tr>
                                        <td><?php echo $value->policyName; ?></td>
                                        <td><?php echo $value->description; ?></td>
                                        <td><?php echo $value->completionDate; ?></td>
                                        <td>Unread</td>
                                        <td><a href="<?php echo $value->proof; ?>" class="btn btn-xs btn-default" >Open PDF</a></td>
                                        <td>
                                            <!-- <input type="text" class=""> -->
                                            <div class="form-group form-group-sm"> 
                                                <input class="form-control" type="text" name="" value="" > 
                                            </div> 
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </table>
                            </div>
                        <?php else: ?>
                            <p class="clearfix">Your employer has not added any policies.</p>
                        <?php endif; ?>
                        <input type="submit" value="save" class="btn btn-raised bg-theme">
                        </form>
                    </div>

                    <style>
                        .form-group.form-group-sm {
                            margin-top: 10px;
                        }
                    </style>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>

    $(function() {

        $.material.init();

        $("#openPolicy").click(function() {
            $("#policyDoc").removeClass('hidden');
            $("#employeeView").addClass('hidden');
        });

        $("#closePolicy").click(function() {
            $("#policyDoc").addClass('hidden');
            $("#employeeView").removeClass('hidden');
        });

        function displayError() {
            swal({
                title : "Error !",
                text : 'Try Again!',
                type : 'error',
                confirmButtonClass : 'bg-theme',
                animation : false,
                width : 400
            });
        };

        function closeAlert() {
            $(".sweet-alert button").first().trigger('click');
        }

        $(".main .form-horizontal").submit(function(event) {

            // =============================================================================
            event.preventDefault();

            // remove all error messages
            $(".help-block").remove();
            $(".has-error").removeClass('has-error');
            
            swal({
                title : 'Saving',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 400
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: $('.main form').serialize(), 
            })
            .done(function(data) {

                if (data.status == 200) {
                    swal({
                        title : "Success !",
                        text : data.message,
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });   
                    getAndInsert(data.url);
                    history.pushState(null, null, data.url);
                }
                else{  // Validation Error
                    displayError();
                    // show Errors
                    for(var fields in data) {
                        $('#'+fields).parents('.form-group').addClass('has-error');
                        $('#'+fields).parent().append('<p class="help-block">'+ data[fields][0] +'</p>');
                    }
                }
            })
            .error(function() {
                displayError();
            });
        });
        
    });
</script>
