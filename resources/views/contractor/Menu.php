<div class="container hidden-xs">
    <div class="row">
        <div class="col-md-3 col-sm-3"></div>
        <div class="col-md-9 col-sm-9 small text-uppercase">
            <div class="col-md-6 col-sm-6 heart-beat">We <i class="fa fa-heart text-danger"></i> <?php echo Session::get('companyName');?></div>
            <div class=" col-md-6 col-sm-6 text-right"><a href="javascript:void(0)" data-target="#" class="dropdown-toggle" data-toggle="dropdown">Welcome 
              <?php echo session()->get('employeeName');?>
                <b class="caret"></b></a>
                <ul class="dropdown-menu pull-right">
<!--                     <li><a class="h6 " href="/changeView">Switch to Employee View</a></li>
 -->
                        <?php if (session()->get('viewChanged')): ?>
                        <li><a class="h6 " href="/changeView">Switch to Employer View</a></li>
                        <?php endif; ?>
                        <li><a class="h6 " href="/viewDetails">Personal Details</a></li>
                        <li><a class="h6" href="/logout">Log Out</a></li>
                </ul></div>
        </div>
        <br>
    </div>
</div>
<!-- ================  Body  ===================== -->

<style>    
    .nav.nav-stacked .active>a{
        color: #e54255;
        font-weight: 400;
    }

    #logo .icon-bar {
        border: 1px solid;
    }

    #logo .navbar-toggle {
        position: absolute;
        top: 3px;
        right: 0;
    }
</style>

<div class="container">
    <div class="row">
        <nav class="col-md-3 col-sm-3">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <span id="logo" class="fa btn" >Sashtechs <div></div>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-warning-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </span>
            </div>
            <div class="clearfix"></div>
            <div class="navbar-collapse collapse navbar-warning-collapse">
                <ul class="nav navbar nav-stacked" style="background:transparent">
                    <li class="active"><a class="ajaxify btn" href="/contractorDashboard"><i class="fa fa-flag fa-lg"></i>Dashboard</a></li>
                    <!-- <li class="disabled"><br></li> -->
                    <li><a class="ajaxify btn" href="/contractorDetails"><i class="fa fa-user fa-lg"></i>personal&nbsp;details</a></li>
                    <li><a class="ajaxify btn" href="/contractorPayslip"><i class="fa fa-money fa-lg"></i>Payslips / taxslips</a></li>
                    <li class="disabled"><br></li>
                    <!-- <li><a class="ajaxify btn" href="/contractorExpenses"><i class="fa fa-inr fa-lg"></i>expenses</a></li> -->
                    <li><a class="ajaxify btn" href="/calendar"><i class="fa fa-calendar fa-lg"></i>Calendar</a></li>
                    <li class="disabled"><br></li>
                </ul>
            </div>
        </nav>