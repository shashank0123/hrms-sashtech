<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>


<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">
                        <br>
                        <br>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <a href="/oneTimePayment" class="large_button">
                                <h3><i class="fa fa-money"></i>&nbsp;&nbsp;One Time Payment </h3><br> 
                                <h6>Pay Expenses & Bonus to multiple employees through on click.</h6>
                            </a>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <a href="/bulkUserUpload" class="large_button">
                                <h3><i class="fa fa-users"></i>&nbsp;&nbsp;Bulk User Upload </h3><br> 
                                <h6>Upload multiple employees using csv or excel <br><br>  </h6>
                            </a>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <a href="/bulkUserEdit" class="large_button">
                                <h3><i class="fa fa-clipboard"></i> &nbsp;&nbsp;Bulk Edit </h3><br>
                                <h6>Edit multiple employees using csv or excel <br><br></h6>
                            </a>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <a href="/notification" class="large_button">
                                <h3><i class="fa fa-envelope"></i> &nbsp;&nbsp;Notification </h3><br> 
                                <h6>Send notification to a team <br><br></h6>
                            </a>
                        </div>
                    </div>

                    <style>
                        a.large_button {
                            display: block;
                            padding: 1em;
                            border: 1px solid #ddd;
                            box-shadow: 0 0 3px #ddd;
                            font-family: FontAwesome;
                            text-align: center;
                            margin: 1em;
                            text-decoration: none;
                            height: 13em;
                        }
                    </style>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>
    $(function() {

        // add active class to corresponding link in menu 
        $(".nav li.active").removeClass('active');
        $(".nav.nav-stacked li").filter(function(index) {
            return $(this).data('url') == 'bulkFeature';
        }).addClass('active');

        $.material.init();

        $(".large_button").click(function(event) {
            event.preventDefault();

            var href = $(this).attr('href');

            var loader = '<div class="panel-body" style="min-height:36em;"><div class="load-loader"><div class="load-flipper"><div class="load-front"></div><div class="load-back"></div></div></div></div>';

            $(".main").html(loader);

            getAndInsert(href);

            history.pushState(null, null, href);
        });
    });
</script>
