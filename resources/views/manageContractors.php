<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>

<?php $count = count($result); ?>

<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
            <div class="main panel panel-default col-md-9 col-sm-9">
                <div class="panel-body" style="min-height:36em;">
                        <h2>Manage Contractors</h2><hr>
                        <br>

                        <div class="col-md-8 col-sm-8 col-sm-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><b>Contractors </b><?php ((!empty($count))&&($count > 0)) ? $count : ''; ?></h3>
                                </div>
                                <div class="panel-body table-responsive">
                                    <table class="table table-hover">
                                        <?php if (!empty($result)) { 
                                                foreach ($result as $key => $value) { ?>
                                        <tr>
                                            <td><i class="fa fa-male"></i></td>
                                            <td>
                                                <span class="h6"><?php echo $value->id;?></span>
                                            </td>
                                            <td><span class="h6"><?php echo $value->firstName . ' ' . $value->lastName; ?></span></td>
                                            <td class="text-right"><a href="/viewContractor/<?php echo $value->id; ?>"><i class="fa fa-list-alt ajaxify"> </i><span class="h6"> View Details</span></a></td>
                                        </tr>
                                        <?php } } else { ?>
                                        <tr>
                                            <td><i class="fa fa-male"></i>/<i class="fa fa-female"></i></td>
                                            <td><span class="h6">You don't have any contractors till now.</span></td>
                                        </tr>
                                        <?php } ?>
                                        <!-- <tr>
                                            <td><i class="fa fa-cogs"></i></td>
                                            <td><span class="h6">Bakshi, Ricky</span></td>
                                            <td class="text-right"><a href="/viewContractor"><i class="fa fa-list-alt ajaxify"> </i><span class="h6"> View Details</span></a></td>
                                        </tr> -->
                                    </table>

                                </div>
                            </div>
                        </div>  
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="extra notice text-center">
                                <div class="box">
                                    <img src="images/Pin.png" class="boardPin" alt="pin">
                                    <br>
                                    <h4>idea to show you care</h4>
                                    <div><i class="fa fa-thumbs-o-up fa-4x"></i></div>
                                    <br>
                                    <h6>Surprise your team with little gifts such as movie tickets, homemade cookies, or a gift card to a local coffee shop.</h6>
                                </div>
                            </div>
                        </div> 

                        <div class="clearfix"></div>
                        <br>

                        <a href="/addContractor_step1" class="pull-left ajaxify"> <i class="fa fa-plus"></i> Add New Contractor</a>
                    </div>   
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>
    $(function() {

        // add active class to corresponding link in menu 
        $(".nav li.active").removeClass('active');
        $(".nav.nav-stacked li").filter(function(index) {
            return $(this).data('url') == 'manageContractors';
        }).addClass('active');
        
        $.material.init();
    })
</script>
