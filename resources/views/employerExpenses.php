<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>


<body  style="margin-top:4em;">
    <?php require_once('Menu.php');?>
    <div class="main panel panel-default col-md-9 col-sm-9">
        <div class="panel-body" style="min-height:36em;">

            <br>
            <h2>Unapproved Expenses</h2>
            <br>
            <?php if (!empty($pending)): ?>

            <form action="#" class="form-horizontal">        
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tr>
                            <th width="100" class="text-center"><h6>Date of Expense</h6></th>
                            <th class="text-center"><h6>Description</h6></th>
                            <th class="text-center"><h6>Uploaded Document</h6></th>
                            <th class="text-center"><h6>Amount</h6></th>
                            <th class="text-center"><h6>Employee Id</h6></th>
                            <th width="150" class="text-center"><h6>Expense Id</h6></th>
                            <th width="100" class="text-center"><h6>Approve</h6></th>
                        </tr>

                        <?php foreach ($pending as $key => $value):?>

                        <tr>
                            <td class="dateOfExpense"><?php if (isset($value->dateOfExpense)) echo $value->dateOfExpense; ?></td>
                            <td><?php if (isset($value->description)) echo $value->description; ?></td>
                            <td><a target="_blank" href="<?php if (isset($value->docUrl)) echo $value->docUrl; ?>">Proof</a></td>
                            <td class="text-center"><?php if (isset($value->amount)) echo $value->amount; ?></td>
                            <td class="text-center">
                                <div class="form-group form-group-sm">
                                    <input class="form-control" type="text" name="employeeId<?php echo $key;?>" value="<?php if (isset($value->userId)) echo $value->userId; ?>" readonly>
                                </div>
                            </td>
                            <td class="text-center">
                                <div class="form-group form-group-sm">
                                    <input class="form-control" type="text" value="<?php if (isset($value->expenseId)) echo $value->expenseId; ?>" name="expenseId<?php echo $key;?>" readonly="">
                                </div>
                            </td>
                            <td>
                                <div class="form-group form-group-sm">
                                    <div class="togglebutton">
                                        <label>
                                            <input type="checkbox" name="approve<?php echo $key;?>">
                                        </label>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <input type="hidden" name="varCount" value="<?php echo $key;?>">
                    <?php endforeach; ?>
                    </table>
                </div>
                <span class="btn btn-raised bg-theme pull-right submit">submit</span>
            </form>

            <br>
            <br>
            <?php else: ?>
                <p class="clearfix">No new expense added by your employees yet.</p>
            <?php endif; ?>
            <?php if (!empty($approved)): ?>
            <hr>
            <h2>Approved Expenses</h2>
            <br>

            <div class="table-responsive">
                <table class="table table-bordered">
                    <tr>
                        <tr>
                            <th width="100" class="text-center"><h6>Date of Expense</h6></th>
                            <th class="text-center"><h6>Description</h6></th>
                            <th class="text-center"><h6>Uploaded Document</h6></th>
                            <th class="text-center"><h6>Amount</h6></th>
                            <th class="text-center"><h6>Employee Id</h6></th>
                            <th width="150" class="text-center"><h6>Expense Id</h6></th>
                        </tr>
                        
                         <?php foreach ($approved as $key => $value):?>
                        <tr>
                            <td class="dateOfExpense"><?php if (isset($value->dateOfExpense)) echo $value->dateOfExpense; ?></td>
                            <td><?php if (isset($value->description)) echo $value->description; ?></td>
                            <td><a href="<?php if (isset($value->docUrl)) echo $value->docUrl; ?>">my document</a></td>
                            <td class="text-center"><?php if (isset($value->amount)) echo $value->amount; ?></td>
                            <td class="text-center"><?php if (isset($value->userId)) echo $value->userId; ?></td>
                            <td class="text-center"><?php if (isset($value->expenseId)) echo $value->expenseId; ?></td>
                        </tr>
                         <?php endforeach; ?>
                    </tr>
                </table>
            </div>
             <?php endif; ?>
        </div>
    </div>
</div>
</div>

    <br><br>
    <?php require_once('footer.php');?>

</body>
</html>

<script>

function displayError() {
    swal({
        title : "Error !",
        text : 'Try Again!',
        type : 'error',
        confirmButtonClass : 'bg-theme',
        animation : false,
        width : 400
    });
};

function closeAlert() {
    $(".sweet-alert button").first().trigger('click');
}

$(function() {
    
    // add active class to corresponding link in menu 
    $(".nav li.active").removeClass('active');
    $(".nav.nav-stacked li").filter(function(index) {
        return $(this).data('url') == 'employerExpenses';
    }).addClass('active');

    $.material.init();

    $("td.text-center").css('padding', '5px');
    $(".dateOfExpense").css('text-align', 'center');
    $("td").css('vertical-align', 'middle');
    $(".form-group.form-group-sm").css('margin-top', '5px');
    $(".togglebutton label").css('padding-left', '25px');

    $(".submit").click(function(event) {

        swal({
            title : 'Saving',
            html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
            allowOutsideClick : false,
            showConfirmButton : false,
            animation : false,
            width : 400
        });

        $.ajax({
            url: location.href,
            type: 'POST',
            data: $('.main form').serialize(),
        })
        .done(function(data) {
            console.log("success", data);
            if (data.status == 200) {
                swal({
                    title : "Success !",
                    text : data.message,
                    type : 'success',
                    confirmButtonClass : 'bg-theme',
                    animation : false,
                    width : 400
                });
            }
            else{
                displayError();
            }
        })
        .fail(function() {
            console.log("error");
        })
    });
});
</script>
