<!DOCTYPE html>
<html>
<head>
     <title>Tool for Calculating monthly pay and your rent reciept</title>

     <meta name="description" content="Enjoy autopilot payroll runs, automatic tax filing, and automatic onboarding of new employees with our platform and mobile app." />


    <meta name="keywords" content="hr software, hr platform, hr management system, payroll software, Benefits, Group Health Insurance, Time tracking, Expense reporting, hrms software, payroll software free, leave & attendance management system" />

    <!--Import Google Icon Font-->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
    <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet"></link>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Material Design -->
    <link href="css/bootstrap-material-design.css" rel="stylesheet">
    <link href="css/ripples.min.css" rel="stylesheet">
    <link href="css/roundslider.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">

    <script src="/js/jquery-1.12.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ripples.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/roundslider.min.js"></script>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    
    <style>
        .feature {
            margin: 1em;
            border: 1px solid #ddd;
            padding: 1em;
            height: 17em;
            box-shadow: 0 0 3px #ddd;
        }

        a, a:hover, a:active {
            text-decoration: none;
        }
    </style>

</head>

<body>
<?php require_once("staticNavBar.php");?>

<!-- ================  Header  ===================== -->

<header class="parallax" style="background-image: url(images/payroll.jpg);background-position: center">
    <div class="panel panel-default" style="  ">
        <div class="panel-body">
            <div class="text-center">
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <h5>TOOL</h5>
                <h2>Calculate your Monthly Pay and generate your rent reciept</h2>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
            </div>
        </div>
    </div>
</header>

<section style="background:#fff;margin-top: -20px">
    <div class="container-fluid">
        <div class="row">
            <div class="container">
                
                <br>
                <br>

                <div class="clearfix"></div>

                <div class="col-md-6 col-sm-6 col-xs-12">
                    <a href="/inHandPayCalculator">
                        <div class="center-block feature text-center">
                            <i class="fa fa-inr fa-3x"></i>
                            <br>
                            <h4>Monthly Pay</h4>
                            <br>
                            <p>You can calculate your monthly income after deduction Taxes from your CTC</p>
                            <br>
                        </div>
                    </a>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12">
                    <a href="/generateRentReceipt">
                        <div class="center-block feature text-center">
                            <i class="fa fa-file-text fa-3x"></i>
                            <br>
                            <h4>Rent Reciept</h4>
                            <br>
                            <p>Generate Your Monthly Rent Reciept</p>
                            <br>
                        </div>
                    </a>
                </div>
            </div>

            <br><hr><br>
        </div>
    </div>
</section>

<div class="clearfix"> </div>

<section class="parallax" style="background-image: url(images/try.jpg);">
    <div class="panel panel-default" style="  ">
        <div class="panel-body">
            <div class="text-center">
                <br>
                <br>
                <br>
                <br>
                <h5><b>TRY Sashtechs</b></h5>
                <h3 class="text-uppercase">Start your one month free trial </h3>
                <br>

                <div class="panel text-left" id="panel-text-left">
                    <form action="#" class="form-horizontal">
                        <div class="col-md-12">
                            <div class="form-group label-floating is-empty">
                                <label class="control-label" for="fullName">Full Name</label>
                                <input class="form-control" id="fullName" name="fullName" type="text" id="cursor-auto">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group label-floating is-empty">
                                <label class="control-label" for="workEmail">Email (work email) </label>
                                <input class="form-control" id="workEmail" name="workEmail" type="email" id="cursor-auto">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group label-floating is-empty">
                                <label class="control-label" for="companyName">Company Name</label>
                                <input class="form-control" id="companyName" name="companyName" type="text" style="cursor: auto;">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group label-floating is-empty">
                                <label class="control-label" for="phone">Phone</label>
                                <input class="form-control" id="phone" name="phone" type="text" id="cursor-auto">
                            </div>
                        </div>

                        <button id="submit" class="btn bg-theme btn-raised btn-block">TRY 1 MONTH FREE</button>
                    </form>
                </div>
                <br>
                    <div class="clearfix"></div>
                    <br><br>
                    <br><br>
            </div>
        </div>
    </div>
</section>

<?php require_once('staticFooter.php');?>

</body>
</html>

<script>
    $(function() {
        $.material.init();
    });
</script>
