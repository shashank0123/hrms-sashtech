<!DOCTYPE html>
<html>
<head>
    <title> Forgot Password Sashtechs | Health Insurance for Business & Startups India</title>

    <meta name="description" content="Partner with Sashtechs for payroll services, human resources management, HRIS, time & attendance, reporting and tax filing."/>

    <meta name="keywords" content="hr software, hr platform, hr management system, payroll software, Benefits, Enterprise payroll, Group Health Insurance, payroll services, human resources management, HRIS, time & attendance, reporting and tax filing" />

    <?php require_once('links.php');?>
</head>

<style>
    .panel{
        width:320px;
        margin: 0 auto;
        padding: 5px;
    }
</style>

<body  style="margin-top:4em;">
        <div class="panel panel-default">
            <div class="panel-body">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <form class="form-horizontal" autocomplete="off" method="POST">
                        <input style="display:none">
                        <input type="password" style="display:none">
                        <fieldset>
                            <legend><a id="logo" class="fa btn btn-block" href="javascript:void(0)" >Sashtechs <div></div></a></legend>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating is-empty ">
                                        <label class="control-label" for="email"><span>Email</span></label>
                                        <input class="form-control" id="email" type="email" name="email" style="cursor: auto;" autocomplete="off">
                                        <?php if (isset($errors) && is_string($errors)): ?>
                                            <p class="help-block">
                                                <?php echo $errors; ?>
                                            </p>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>

                            <br>
                            <?php if(isset($result->status) && ($result->status == '204')): ?>
                                <p class="help-block">
                                    <?php echo $result->message; ?>
                                </p>
                            <?php endif; ?>
                            <?php if(isset($result->status) && ($result->status == '200')): ?>
                                <p style="color: #00C851; margin-top: -9px; font-size: 12px; display:  block!important;">
                                    <?php echo $result->message; ?>
                                </p>
                            <?php endif; ?>
                            <div class="clearfix"></div>
                            <br>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <button style="margin:0 auto;" type="submit" class="btn bg-theme btn-block btn-raised">Get Reset Link</button>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </fieldset>
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>

                    </div>
                </div>
            </div>
        </div>

    <br><br>
</body>
</html>

<script>
    $.material.init();
</script>
