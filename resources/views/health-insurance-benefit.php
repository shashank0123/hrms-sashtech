<!DOCTYPE html>
<html>
<head>
    <title>Group Employee Health Insurance Policy plans linked with Payroll</title>
 
    <meta name='keywords' content='Employee Health Insurance, Group Health Insurance, Corporate Employee Health Insurance, Employee Health Insurance for business, policy, India, Third Party Administrator (TPA) online' /> 

    <meta name='description' content='Employee Health Insurance - We provide comparison for group health insurance coverage online. Get health insurance quotes for  employee health insurance benefits from us.' />

    <!--Import Google Icon Font-->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">

    <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet"></link>


    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Material Design -->
    <link href="css/bootstrap-material-design.css" rel="stylesheet">
    <link href="css/ripples.min.css" rel="stylesheet">
    <link href="css/roundslider.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">

    <script src="/js/jquery-1.12.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ripples.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/roundslider.min.js"></script>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    
    <style>
        .feature {
            margin: 1em;
            border: 1px solid #ddd;
            padding: 1em;
            height: 15em;
            box-shadow: 0 0 3px #ddd;
        }
    </style>

</head>

<body>
<?php require_once("staticNavBar.php");?>

<!-- ================  Header  ===================== -->

<header class="parallax" style="background-image: url(images/benefit.jpg);background-position: center">
    <div class="panel panel-default" style="  ">
        <div class="panel-body">
            <div class="text-center">
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <h5>BENEFIT</h5>
                <h2>Awesome benefits for employers & employees</h2>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
            </div>
        </div>
    </div>
</header>

<section style="background:#fff;margin-top: -20px">
    <div class="container-fluid">
        <div class="row">
            <div class="container">
                <br>
                <br>
                <h2 class="text-center text-muted">Why manage your business' health benefits  through Sashtechs?</h2>
                <br>

                <div class="clearfix"></div>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="center-block feature text-center">
                        <i class="fa fa-check-square-o fa-3x"></i>
                        <br>
                        <h4>Simple setup</h4>
                        <br>
                        <p>Integrating benefits with payroll makes setup easy. You won’t need census data, old payroll info or tax docs.</p>
                        <br>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="center-block feature text-center">
                        <i class="fa fa-user fa-3x"></i>
                        <br>
                        <h4>Personalized Plans</h4>
                        <br>
                        <p>Our recommendation engine customizes your health plan's by using our sophisticated   algorithms.</p>
                        <br>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="center-block feature text-center">
                        <i class="fa fa-exclamation-circle fa-3x"></i>
                        <br>
                        <h4>Expert Advice</h4>
                        <br>
                        <p>Our in-house experts will help you to choose the best beneﬁts package for your employees.</p>
                        <br>
                        <br>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="center-block feature text-center">
                        <i class="fa fa-thumbs-up fa-3x"></i>
                        <br>
                        <h4>Easy Management</h4>
                        <br>
                        <p>By combining payroll and benefits, Sashtechs makes management much simpler than other systems.</p>
                        <br>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="center-block feature text-center">
                        <i class="fa fa-slideshare fa-3x"></i>
                        <br>
                        <h4>101 relationship with Insurance carriers</h4>
                        <br>
                        <p>We have selection of best plans and carriers which ensure great rates and prompt support.</p>
                        <br>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="center-block feature text-center">
                        <i class="fa fa-comments fa-3x"></i>
                        <br>
                        <h4>Extended expertise.</h4>
                        <br>
                        <p>Unlike traditional brokers, we can help with more than just benefits. Ask about payroll, tax saving tips, and more.</p>
                        <br>
                    </div>
                </div>

                <div class="clearfix"></div>
                <br><br><hr><br><br>

                <h3 class="text-muted text-center">Setup a new health benefits or keep your existing plans and carriers in minutes. Sashtechs helps your business manage it all from one online dashboard.</h3>

                <br>
                <br>


                <div class="col-md-6 col-sm-6 col-xs-12">
                    <h2>EASIER FOR EMPLOYEES</h2>
                    <br>
                    <blockquote>
                        <p><i class="fa fa-user"></i> &nbsp; &nbsp;<strong>Employee onboarding</strong></p>
                        <small>Employees use private and secure web and mobile apps to enroll in and update their health plans.</small>
                    </blockquote>
                    <blockquote>
                        <p><i class="fa fa-envelope"></i> &nbsp; &nbsp;<strong>Notifications</strong></p>
                        <small>Receive notification on open enrollment or at the time of onboarding . </small>
                    </blockquote>
                    <blockquote>
                        <p><i class="fa fa-umbrella"></i> &nbsp; &nbsp;<strong>Coverage Details</strong></p>
                        <small>View and print medical insurance cards online</small>
                    </blockquote>
                </div>


                <div class="col-md-6 col-sm-6 col-xs-12">
                    <h2>EASIER FOR EMPLOYERS</h2>
                    <br>
                    <blockquote>
                        <p><i class="fa fa-check"></i> &nbsp; &nbsp;<strong>Selection</strong></p>
                        <small>Shop for plans & design comprehensive benefits plans.</small>
                    </blockquote>
                    <blockquote>
                        <p><i class="fa fa-line-chart"></i> &nbsp; &nbsp;<strong>Dashboard</strong></p>
                        <small>Monitor employee enrollment and renewal statuses.</small>
                    </blockquote>
                    <blockquote>
                        <p><i class="fa fa-mobile"></i> &nbsp; &nbsp;<strong>Mobility</strong></p>
                        <small>Your employees can access and edit health benefits entirely via smartphone.</small>
                    </blockquote>
                </div>

                <div class="clearfix"></div>
                <br>
            </div>
            <br><br>
        </div>
    </div>
</section>

<div class="clearfix"> </div>

<section class="parallax" style="background-image: url(images/try.jpg);">
    <div class="panel panel-default" style="  ">
        <div class="panel-body">
            <div class="text-center">
                <br>
                <br>
                <br>
                <br>
                <h5><b>TRY Sashtechs</b></h5>
                <h3 class="text-uppercase">Start your one month free trial </h3>
                <br>

                <div class="panel text-left" id="panel-text-left">
                    <?php require 'demoForm.php'; ?>
                </div>
                <br>
                    <div class="clearfix"></div>
                    <br><br>
                    <br><br>
            </div>
        </div>
    </div>
</section>

<?php require_once('staticFooter.php');?>

</body>
</html>

<script>
    $(function() {
        $.material.init();
    });
</script>
