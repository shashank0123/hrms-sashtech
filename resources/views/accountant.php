<!DOCTYPE html>
<html>
<head>
    <title>Payroll Software for Accountants</title>

    <meta name='description' content='Sashtechs offers online payroll services for accountants like Payroll Software,Payroll & HR software for india, Payroll Software for employee salary with ESIC , PT , PF and indian income tax,Online attendance, Payslip generation, HR procedures, Payroll Management Software System, Download Payroll Software and manage Payroll/Payslip generation, manage leaves, manage employee appraisal, manage employee promotion/increment and other HR procedures, Payroll Management Software System' />

    <meta name='keywords' content='Payroll Software,Payroll & HR software for india, Payroll Software for employee salary with ESIC , PT , PF and indian income tax,Online attendance, Payslip generation, HR procedures, Payroll Management Software System, Download Payroll Software and manage Payroll/Payslip generation, manage leaves, manage employee appraisal' />

    <!--Import Google Icon Font-->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
    <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet"></link>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Material Design -->
    <link href="css/bootstrap-material-design.css" rel="stylesheet">
    <link href="css/ripples.min.css" rel="stylesheet">
    <link href="css/roundslider.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">


    <script src="/js/jquery-1.12.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ripples.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/roundslider.min.js"></script>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    <style>
        .rs-handle  {
            background-color: #fff;
            border: 1px solid #000;
        }
        .rs-tooltip-text{
            font-size: 20px;
            font-family: FontAwesome;
        }
        .rs-control .rs-range-color {
            background-color: #fc6558;
        }
        .rs-control .rs-path-color {
            background-color: #ffffff;
        }
        .rs-control .rs-handle:after {
            background-color: #fc6558;
        }
        .rs-control .rs-bg-color {
            background-color: #ffffff;
        }
        .tab-content {
            padding: 2em 4em;
        }
    </style>
</head>

<body>
<?php require_once("staticNavBar.php");?>

<!-- ================  Header  ===================== -->

<header class="parallax" style="background-image: url(images/accountant.jpg);">
    <div class="panel panel-default" style="  ">
        <div class="panel-body">
            <div class="text-center">
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <h5>ACCOUNTANT</h5>
                <h3 class="text-uppercase">Smart, Innovative Solutions for Accountants</h3>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <i class="fa fa-arrow-down"></i>
                <br>
                <br>
            </div>
        </div>
    </div>
</header>

<section class="container">
    <br>
    <h4 class="text-center text-uppercase">Simple, Smart & Awesome payroll. Recommended by top accountants</h4>
    <br>
</section>

<br><br>

<section>
    <div class="container-fluid" >
        <div class="container" style="margin-bottom: -3em;position: relative;">
            <div class="panel-body bg-primary ">
                <h2 class="text-center">Why become a Sashtechs Partner ?</h2>
            </div>
        </div>
        <div class="row" style="background:#fff;">
            <br>
            <br>
            <br>
            <br>
            <div class="container">


                <div>
                    <ul class="nav nav-pills nav-justified" role="tablist">
                        <li role="presentation" class="active"><a href="#employeeFeatures" aria-controls="employeeFeatures" role="tab" data-toggle="tab"><b>Employee Features</b></a></li>
                        <li role="presentation"><a href="#automatedTasks" aria-controls="automatedTasks" role="tab" data-toggle="tab"><b>Automated Tasks</b></a></li>
                        <li role="presentation"><a href="#advancedFeatures" aria-controls="advancedFeatures" role="tab" data-toggle="tab"><b>Advanced Features</b></a></li>
                    </ul>

                  <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="employeeFeatures">
                            <h3>Employee Features</h3>
                            <hr>
                            <ul>
                                <li>
                                    <h5>Employee Self-Onboarding   </h5><p>No more exchange of emails for information. Employees can fill out their own Personal Details, select benefits and save you  from manual data entry.</p>
                                </li>
                                <li>
                                    <h5>Enrollment of Benefits   </h5><p>Select any health plan from our offering which fits your family needs.</p>
                                </li>
                                <li>
                                    <h5>Payslip For Employees   </h5><p>A detailed payslip and taxslip shows the source of income for an employees like Salary, bonus or reimbursements, vacation hours, timesheet and more.</p>
                                </li>
                                <li>
                                    <h5>Employer Accounts  </h5><p>Each employer gets an account to review the payroll, check dashboard for timesheets,  bonus or reimbursements, vacation hours and more.</p>
                                </li>
                                <li>
                                    <h5>Employee Accounts  </h5><p>Each employee gets an account to access payslip, Form 16, manage health benefits, timesheets and more. </p>
                                </li>
                            </ul>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="automatedTasks">
                            <h3>Automated Tasks</h3>
                            <hr>
                            <ul>
                                <li>
                                    <h5>Automatic Tax Filings   </h5><p>No more paper forms. ZenPayroll creates and electronically files your State & Federal payroll tax documents, including all end-of-year forms such as the W-2. Access your forms online at any time</p>
                                </li> 
                                <li>
                                    <h5>Automatic Tax Calculations & Payments  </h5> <p>ZenPayroll automatically calculates your state and federal payroll taxes. We then electronically pay them to the government on your behalf.</p>
                                </li>
                                <li>
                                    <h5>Payroll On Autopilot™  </h5> <p>Never worry about missing a deadline. With AutoPilot activated, our system will automatically process your company&rsquo;s payroll and let you know you’re good to go.</p>
                                </li>
                                <li>
                                    <h5>Contractor Payments & 1099 Forms  </h5> <p>Have the flexibility to work with contractors. We’ll automatically generate 1099 forms and send them electronically to contractors, plus file them with your tax documents at no extra charge</p>
                                </li>
                            </ul>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="advancedFeatures">
                            <h3>Advanced Features</h3>
                            <hr>
                            <ul>
                                <li>
                                    <h5>Unlimited Bonus & Off-Cycle Payrolls   </h5><p>Run payroll as many times as you need — at your convenience, for any reason, and at no extra cost.</p>
                                </li>
                                <li>
                                    <h5>Cancel Payroll   </h5><p>Made a mistake? No worries – quickly cancel a payroll with a single click and re-run it when you’re ready, for no extra charge.</p>
                                </li>
                                <li>
                                    <h5>Flexible Payment Schedules   </h5><p>Choose how often employees get to celebrate payday weekly, bi-weekly, twice a month, or monthly.</p>
                                </li>
                                <li>
                                    <h5>Detailed Payroll Reports   </h5><p>Get a better understanding of your payroll. Review detailed cost reports, payroll journals, and bank transaction statements. See the types of reports you can run.</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <br><br>
            </div>
        </div>
    </div>
</section>

<div class="clearfix"></div>

<section style="background:#fff;">
    <div class="container" >
        <h2 class="text-center">What makes Sashtechs Awesome ?</h2>
        <br>
        <br>
        <blockquote>
            <h5 class="text-info">Employee self-onboarding</h5>
            <p class="small">No more time-consuming manual data entry. Your clients’ employees can onboard and update personal information on their own.</p>
        </blockquote>
        <blockquote>
            <h5 class="text-info">Accounts for Forever</h5>
            <p class="small">Employees can access all of their information online, even after they leave their employer.</p>
        </blockquote>
        <blockquote>
            <h5 class="text-info">Free client support</h5>
            <p class="small">Your dedicated customer care advocate can help your clients at every step.</p>
        </blockquote>
        <blockquote>
            <h5 class="text-info">One Stop Shop</h5>
            <p class="small">Because payroll, benefits, timesheets, expenses are all one product, you don't have to manage in multiple places</p>
        </blockquote>
        <blockquote>
            <h5 class="text-info">Automated tax filings</h5>
            <p class="small">You don't have to handle client payroll taxes, TDS and Form 16. We handle everything.</p>
        </blockquote>
        <blockquote>
            <h5 class="text-info">Run Payroll in minutes</h5>
            <p class="small">Run payroll at anytime, from anywhere. Even from your phone.</p>
        </blockquote>
    </div>
    <div class="clearfix"></div>
    <br><br>
</section>

<br><br><br>
<form action="#" class="form-horizontal container">
    
    <section id="optionA" class="meter">
        <div class="panel panel-default">
            <div class="panel-body" style="padding:0 15px;">
                <div class="row">
                    <div class="col-md-8 col-sm-12 col-xs-12" style="padding-top:2em;">
                        <h4>Pricing</h4>
                        <h3 class="text-center">How many employees do you have?</h3>
                        <br>
                        
                        <div class="col-md-5 col-sm-3 col-xs-12"><div id="optionASlider" class="center-block"></div><div class="clearfix"></div></div>
                        <div class="col-md-3 col-sm-4 col-xs-12 text-center">
                            <br><br>
                            <span><i class="fa fa-inr"></i> </span>
                            <span class="h1 total">
                                550 
                            </span>
                            <br>
                            <span class="h6">PER MONTH</span> 
                        </div>
                        <div class="col-md-4 col-sm-5 col-xs-12 text-center">
                            <br><br>
                            <span class="h3">Here&rsquo;s the Math</span>
                            <br><br>
                            <span class="h5 employee">1 employee at <i class="fa fa-inr"></i> 50</span>
                        </div>
                        <div class="clearfix"></div>
                        <br><br>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12 bg-primary" style="height: 24em;padding:1em 10px;">
                        <h3 class="text-center">General Pricing</h3>
                        <br>
                        <table class="table">
                            <tr>
                                <th>ITEM</th>
                                <th>PRICE PER MONTH</th>
                            </tr>
                            <tr>
                                <td>Base Fee</td>
                                <td><i class="fa fa-inr"></i> 500</td>
                            </tr>
                            <tr>
                                <td>Employees</td>
                                <td><i class="fa fa-inr"></i> 50 per Employee</td>
                            </tr>
                            <tr>
                                <td>200+ Employees</td>
                                <td>Contact us for enterprise pricing</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="clearfix"></div>
    <div class="col-md-7 col-sm-12 col-xs-12 hidden-xs"><h6>With Sashtechs there are no set-up fees, and no hidden charges. All the payroll features are included in the pricing you see here.</h6></div>
    <div class="col-md-3 col-md-offset-1 col-sm-6 col-sm-offset-3 col-xs-8 col-xs-offset-2"><a href="/signup" class="btn btn-block btn-raised bg-theme">SIGN UP <i class="fa fa-angle-double-right"></i></a></div>
    <div class="clearfix"></div>
</form>

<br><br>

<div class="clearfix"> </div>

<section class="parallax" style="background-image: url(images/try.jpg);">
    <div class="panel panel-default" style="  ">
        <div class="panel-body">
            <div class="text-center">
                <br>
                <br>
                <br>
                <br>
                <h5><b>TRY Sashtechs</b></h5>
                <h3 class="text-uppercase">Become a Partner</h3>
                <br>

                <div class="panel text-left" id="panel-text-left">
                    <form action="#" class="form-horizontal">
                        <div class="col-md-12">
                            <div class="form-group label-floating is-empty">
                                <label class="control-label" for="name">Name</label>
                                <input class="form-control" id="name" name="name" type="text" style="cursor: auto;">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group label-floating is-empty">
                                <label class="control-label" for="workEmail">Email (work email) </label>
                                <input class="form-control" id="workEmail" name="workEmail" type="email" id="cursor-auto">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group label-floating is-empty">
                                <label class="control-label" for="phone">Phone</label>
                                <input class="form-control" id="phone" name="phone" type="text" id="cursor-auto">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="togglebutton form-group">
                                <label>
                                    Run Payrolls for Clients Y/N
                                    &nbsp;&nbsp;&nbsp; 
                                    <input type="checkbox" name="serviceTaxApplicable">
                                </label>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <br>
                        <button id="submit" class="btn bg-theme btn-raised btn-block">Become a partner</button>
                    </form>
                </div>
                <br>
                    <div class="clearfix"></div>
                    <br><br>
                    <br><br>
            </div>
        </div>
    </div>
</section>

<?php require_once('staticFooter.php');?>

</body>
</html>

<script>
    $.material.init();

    $(function() {

        $("#optionASlider").each(function(index, el) {

            var $global = $(this).parentsUntil(".meter").last().parent();
            
            $(el).roundSlider({
                radius: 80,
                width: 20,
                handleShape: "square",
                sliderType: "min-range",
                circleShape: "pie",
                startAngle: 315,
                value: 1,
                max: 200,
                min: 1,
                mouseScrollAction: true,

                baseRate : 0,
                employeeRate : 50,

                drag: function (args) {
                    handleInput.call($($global), args, $(el).roundSlider("option"));
                },
                change: function (args) {
                    handleInput.call($($global), args, $(el).roundSlider("option"));
                }
            });
        });

        function handleInput (args, slider) {
            var noOfEmployee = args.value;
            var employeeRate = slider.options.employeeRate;

            var baseRate = slider.options.baseRate;
            var total = baseRate + employeeRate * noOfEmployee ;

            $(this).find(".employee").html(' ' + noOfEmployee +  ' employee at <i class="fa fa-inr"></i> ' + employeeRate);

            $(this).find(".total").text(total);

            return true;
        }

        $("#optionASlider").roundSlider("option", "baseRate", 500 );
    });
</script>
