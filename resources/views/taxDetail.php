<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>

<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">
                        <br>
                        <br>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <form class="form-horizontal" method="POST">
                                <fieldset>
                                    <legend>Tax Details</legend>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-8 col-sm-12 col-xs-12 " data-hint="true">
                                            <div class="form-group label-floating <?php echo isset($result->pan) ? '' : 'is-empty' ; ?>">
                                                <label class="control-label" for="pan"><span>PAN</span></label>
                                                <input class="form-control" id="pan" name="pan" type="text" style="cursor: auto;" value="<?php echo isset($result->pan) ? $result->pan : '' ; ?>">
                                            </div>
                                        </div>
                                        <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                            <p> Permanent Account Number (PAN) is a ten-digit alphanumeric number, issued in by the Income Tax Department</p>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating <?php echo isset($result->tan) ? '' : 'is-empty' ; ?>">
                                                <label class="control-label" for="tan"><span>TAN</span></label>
                                                <input class="form-control" id="tan" name="tan" type="text" style="cursor: auto;" value="<?php echo isset($result->tan) ? $result->tan : '' ; ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                            <div class="togglebutton form-group">
                                                <label>EPF
                                                    &nbsp;&nbsp;&nbsp; 
                                                    <input type="checkbox" id="epf" name="epf" "<?php echo (!empty($result->epf) and (strtolower($result->epf) == 'on')) ? "checked" : 'false' ; ?>">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                            <p> Employee's Provident Fund (EPF) is a retirement benefit scheme that's available to all salaried employees</p>
                                        </div>

                                    </div>  

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                            <div class="togglebutton form-group">
                                                <label>ESI
                                                    &nbsp;&nbsp;&nbsp; 
                                                    <input type="checkbox" id="esi" name="esi" "<?php echo (!empty($result->esi) and (strtolower($result->esi) == 'on')) ? 'checked' : '' ; ?>">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                            <p> ESI is Employee State Insurance and it is used for the benefit of the employee and if the gross salary of the employees</p>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                            <div class="togglebutton form-group">
                                                <label>Gratuity
                                                    &nbsp;&nbsp;&nbsp; 
                                                    <input type="checkbox" id="gratuity" name="gratuity" "<?php echo (!empty($result->gratuity) and (strtolower($result->gratuity) == 'on')) ? 'checked' : '' ; ?>">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                            <p>Gratuity is a retirement benefit given by an employer to an employee.</p>
                                        </div>
                                    </div>                                  

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <div class="col-md-8 col-sm-8 col-xs-8 col-md-offset-4 col-sm-offset-3 col-xs-offset-4">
                                                <span type="button" class="btn cancel btn-default btn-raised">Cancel</span>
                                                <input type="submit" class="next btn bg-theme btn-raised" value="Submit">
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>

</body>
</html>

<script>

    $(function() {
        $.material.init();

        $("#esi, #epf").change(function(event) {
            var val = $(this).is(':checked');
            sibling = $(this).parents(".form-group").parent().siblings();
            if (val) {
                sibling.removeClass('hidden');
                sibling.find('input').trigger('change');
            }
            else{
                sibling.find('input').trigger('change');
                sibling.addClass('hidden');
            }
        });

        function displayError() {
            swal({
                title : "Error !",
                text : 'Try Again!',
                type : 'error',
                confirmButtonClass : 'bg-theme',
                animation : false,
                width : 400
            });
        };

        function closeAlert() {
            $(".sweet-alert button").first().trigger('click');
        }

        // $('.cancel').click(function(event) {
        //     $(".help-block").remove();
        //     $(".has-error").removeClass('has-error');
        //     event.preventDefault();
        //     var $form = $(this).parents('form')[0];
        //     $form.reset();
        //     $($form).find('.form-group.label-floating').addClass('is-empty');
        // });

        $("form.form-horizontal").submit(function(event) {

            event.preventDefault();

            // remove all error messages
            $(".help-block").remove();
            $(".has-error").removeClass('has-error');
            
            swal({
                title : 'Saving',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 400
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: $('.main form').serialize(), 
            })
            .done(function(data) {
                if (data.status == 200) {
                    swal({
                        title : "Success !",
                        text : data.message,
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });   
                    getAndInsert(data.url);
                    history.pushState(null, null, data.url);
                }
                else if (data.status == 400) {
                    swal({
                        title : "Error !",
                        text : data.message,
                        type : 'error',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });
                }
                else{  // Validation Error
                    displayError();
                    // show Errors
                    for(var fields in data) {
                        $('#'+fields).parents('.form-group').addClass('has-error');
                        $('#'+fields).parent().append('<p class="help-block">'+ data[fields][0] +'</p>');
                    }
                }
            })
            .error(function() {
                displayError();
            });
        });

        $(".main .form-horizontal input").trigger('change');
    });
</script>