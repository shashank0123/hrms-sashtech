<!DOCTYPE html>
<html>
<head>
    <title>Suspend Company</title>
    <?php require_once('links.php');?>
</head>


<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">

                    <div class="breadcrumb flat text-center row">
                        <span class="col-md-4 col-sm-4 hidden-xs">Tax Reconciliation </span>
                        <span class="col-md-4 col-sm-4 col-xs-4 hidden-xs">Reason For Leaving </span>
                        <span class="active col-md-4 col-sm-4 col-xs-12">Confirm And Finish</span>
                    </div>

                    <br>
                    <br>

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="container-fluid">
                            <div class="alert alert-warning text-center">
                                
                                <span class="h4"><b>Warning : </b></span><span class="h5">suspending your company in not reversible </span>

                                <h6>Future payrolls will no longer be performed by Sashtechs</h6>

                            </div>
                        </div>
                        
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <h6>You have no taxes to reconcile. Your suspension date will be today.</h6>
                        </div>

                        <div class="clearfix"></div>

                        
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <form class="form-horizontal" method="POST">
                                <hr>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <input class="btn btn-raised bg-theme col-md-3" type="submit" value="Confirm and Finish"></input>
                                </div>
                            </form>
                        </div>
                    </div>

                    </div>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>
    $.material.init();

</script>
