<style>
	button#submit[disabled]{
        color: #fff;
    }
</style>
<form action="#" class="form-horizontal" method="POST">
    <div class="col-md-12">
        <div class="form-group label-floating is-empty">
            <label class="control-label" for="EmployerName">Full Name</label>
            <input class="form-control" id="EmployerName" name="EmployerName" required type="text" id="cursor-auto">
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group label-floating is-empty">
            <label class="control-label" for="supportmail">Email (work email) </label>
            <input class="form-control" id="supportmail" name="supportmail" required type="email" id="cursor-auto">
        </div>
    </div>
    
    <div class="toggle">
        <div class="col-md-12">
            <div class="form-group label-floating ">
                <label class="control-label" for="password"><span>Password</span></label>
                <input class="form-control" id="password" type="password" required name="password" style="cursor: auto;" autocomplete="off">
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group label-floating ">
                <label class="control-label" for="confpassword"><span>Confirm Password</span></label>
                <input class="form-control" id="confpassword" type="password" required name="confpassword" style="cursor: auto;" autocomplete="off">
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group label-floating ">
                <label class="control-label" for="companyName"><span>Company Name</span></label>
                <input class="form-control" id="companyName" type="text" required name="companyName" style="cursor: auto;" autocomplete="off">
            </div>
        </div>
    </div>

    <button id="submit" class="btn bg-theme btn-raised btn-block">TRY 1 MONTH FREE</button>
    <h6 class="text-center text-primary">Start a free trial today. Cancel any time, no commitments.</h6>
</form>

<script>

	$(function() {
		
		$("#supportmail").on('keyup change', function(event) {

	        var val = $(this).val().split('@');
	        if (val.length<2) return false;
	        val = val.pop();
	        $("#companyName").val(val.split('.')[0].toUpperCase());
	        $("#companyName").trigger('change');
	    });

	    function passwordVerification() {
	        // remove all error messages
	        $(".help-block").remove();
	        $(".has-error").removeClass('has-error');
	        
	        var response = true;
	        $("#password, #confpassword").each(function(index, el) {
                if (!$(this).val()) {
                    $(this).parents(".label-floating").addClass('has-error');
                    $(this).parent().append('<p class="help-block"> password required</p>');
                    response = false;
                    clearError();
                }
                else if ($(this).val().length < 8) {
                    $(this).parents(".label-floating").addClass('has-error');
                    $(this).parent().append('<p class="help-block"> password is less than 8 characters</p>');
                    response = false;
                    clearError();
                }
            });

            if (!response) return response;

            if ($("#password").val()!=$("#confpassword").val()) {
                $("#confpassword").parents(".label-floating").addClass('has-error');
                $("#confpassword").parent().append('<p class="help-block"> passwords did not match</p>');    
                clearError();            
                return false
            };

	        return true;
	    }

	    function displayError() {
	        $("form.form-horizontal .text-primary").text("Please Try Again");
	    }

	    function clearError () {
	        $("form.form-horizontal #submit").text("TRY 1 MONTH FREE"); 
	        $('form.form-horizontal #submit').prop('disabled', false);
	        $("form.form-horizontal .text-primary").text("");
	    } 

	    $("form.form-horizontal").submit(function(event) {
	        $(this).find('.text-primary').text("");
	        $(this).find('#submit').text("Please Wait ...");

	        if ($(this).find('#submit').prop('disabled')) return false;

	        event.preventDefault();

	        if (!passwordVerification()) {
	            displayError();
	            return false;
	        }
	        // remove all error messages
	        $(".help-block").remove();
	        $(".has-error").removeClass('has-error');
	        
	        $(this).find('#submit').prop('disabled', true);

	        $.ajax({
	            url: '/signup',
	            type: 'POST',
	            data: $('.form-horizontal').serialize(), 
	        })
	        .done(function(data) {
	            if (data.status == 200) {
	                clearError();
	                $("form.form-horizontal .text-primary").text(data.message);
	                $('form.form-horizontal').trigger('reset');
	                $('form.form-horizontal .label-floating').addClass('is-empty');
	            }
	            else if(data.status == 208) {  //  email already Exist
	                clearError();
	                var $email = $("#supportmail");
	                $email.parent().append('<p class="help-block">'+ data.message +'</p>');
	                $email.focus();
	                $email.parents('.form-group').addClass('has-error');
	            }
	            else if(data.status == 209) {  //  email already Exist
	                clearError();
	                var $companyName = $("#companyName");
	                $companyName.parent().append('<p class="help-block">'+ data.message +'</p>');
	                $companyName.focus();
	                $companyName.parents('.form-group').addClass('has-error');
	            }
	            else if(data.status == 406) {  //  email already Exist
	                clearError();
	                var $email = $("#supportmail");
	                $email.parent().append('<p class="help-block">'+ data.message +'</p>');
	                $email.focus();
	                $email.parents('.form-group').addClass('has-error');
	            }
	            else{  // Validation Error
	                clearError();
	                displayError();
	                // show Errors
	                for(var fields in data) {
	                    $('#'+fields).parents('.form-group').addClass('has-error');
	                    $('#'+fields).parent().append('<p class="help-block">'+ data[fields][0] +'</p>');
	                }
	            }
	        })
	        .error(function() {
	            clearError();
	            displayError();
	        });
	    });
	})
</script>