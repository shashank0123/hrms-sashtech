<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>


<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">
                        <br>
                        <br>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <a href="/insuranceRequirement" class="large_button">
                                <h3><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;Fill Requirements </h3><br> 
                                <h6>if you don't have your own benefit let us know your requirements</h6>
                            </a>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <a href="/addBenefit_step1" class="large_button">
                                <h3><i class="fa fa-user-md"></i>&nbsp;&nbsp;Use existing benefits </h3><br> 
                                <h6>select this option if you already have your benefit </h6>
                            </a>
                        </div>
                    </div>

                    <style>
                        a.large_button {
                            display: block;
                            padding: 1em;
                            border: 1px solid #ddd;
                            box-shadow: 0 0 3px #ddd;
                            font-family: FontAwesome;
                            text-align: center;
                            margin: 1em;
                            text-decoration: none;
                            height: 12em;
                        }
                    </style>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>
    $(function() {

        // add active class to corresponding link in menu 
        $(".nav li.active").removeClass('active');
        $(".nav.nav-stacked li").filter(function(index) {
            return $(this).data('url') == 'benefit';
        }).addClass('active');

        $.material.init();

        $(".large_button").click(function(event) {
            event.preventDefault();

            var href = $(this).attr('href');

            var loader = '<div class="panel-body" style="min-height:36em;"><div class="load-loader"><div class="load-flipper"><div class="load-front"></div><div class="load-back"></div></div></div></div>';

            $(".main").html(loader);

            getAndInsert(href);

            history.pushState(null, null, href);
        });
    });
</script>
