<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');
        $count = count($employees);?>
</head>

<body  style="margin-top:2em;">
            <?php  require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <style>
                        .employee{
                            -webkit-box-shadow: none;
                            box-shadow: none;
                            position: relative;
                        }
                        .employee > .panel-body > div::after,.employee > .panel-body > div::before {
                            content: '';
                            position: absolute;
                            width: 15px;
                            height: 18px;
                            background: #9A8B8B;
                            top: -11px;
                        }
                        .employee > .panel-body > div::before{
                            left: 0;
                            transform:skewY(40deg);
                        }
                        .employee > .panel-body > div::after{
                            right: 0;
                            transform:skewY(-40deg);
                        }
                        .employee > .panel-heading{
                            z-index: 10;
                            cursor: pointer;
                        }
                        .employee > .panel-body {
                            position: relative;
                            padding-top: 0;
                            padding-bottom: 0;
                        }

                        .employee > .panel-body > div{
                            background: #f5f5f5;
                            padding-top: 15px;
                            margin: 0 15px;
                        }

                    </style>
                    <div class="panel-body" style="min-height:36em;">

                        <div class="breadcrumb flat text-center row">
                            <span class="active col-md-4 col-sm-4 col-xs-12">Adjust Salary</span>
                            <span class="col-md-4 col-sm-4 col-xs-4 hidden-xs">Review </span>
                            <span class="col-md-4 col-sm-4 col-xs-4 hidden-xs">Pay</span>
                        </div>

                        <br>
                        <p class="clearfix">Please complete your payroll by TIME on DATE. Your employees will be paid on DATE</p>

                        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                            <div class="col-md-2 col-sm-2 col-xs-3"><b>Id</b></div>
                            <div class="col-md-6 col-sm-6 col-xs-7"><b>Employee</b></div>
                            <div class="col-md-2 col-sm-2 col-xs-2"><b>Days Worked</b></div>
                            <div class="col-md-2 col-sm-2 col-xs-2 hidden-xs"><b>Gross Pay</b></div>
                        </div>
                        
                        <div class="clearfix"></div>
                        <br>
                        <form class="form-horizontal" type="POST" id="employeesForm">
                            <!-- ======================  employee ============================= -->
                            <?php if (!empty($employees)) { 
                                                foreach ($employees as $key => $value) { ?>
                            <div class="employee small panel panel-info">
                                <div class="panel-heading  text-center col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-2 col-sm-2 col-xs-3"><h4><?php echo $value->userid?></h4></div>
                                    <div class="col-md-6 col-sm-6 col-xs-7"><h4><?php echo $value->firstName." ".$value->lastName?> </h4></div>
                                    <div class="col-md-2 col-sm-2 col-xs-2 noOfDays"><h5>0</h5></div>
                                    <div class="col-md-2 col-sm-2 hidden-xs"><h5 class="grossPayHead"><i class="fa fa-inr"></i> <span>0</span></h5><i class="fa fa-caret-down"></i></div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="panel-body col-md-12 col-sm-12 col-xs-12">
                                    <input type="hidden" class="userId" name="userId<?php echo $key?>" value="<?php echo $value->userid;?>">
                                    <input type="hidden" class="monthlyPay" name="monthlyPay" value="<?php echo $value->monthlyPay;?>">
                                    <input type="hidden" class="oneDayPay" name="oneDayPay" value="<?php echo $value->oneDayPay;?>">
                                    <input type="hidden" class="otherFactorOfCTC" name="otherFactorOfCTC" value="<?php echo $value->sum;?>">
                                    <div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-3">
                                            <h5>CTC : <span><i class="fa fa-inr"></i> <span class="ctc"><?php echo $value->ctc; ?></span></span></h5>
                                            <h5>Direct Deposits</h5>
                                            <h6>
                                                <div class="form-group form-group-sm label-floating ">
                                                    <label class="control-label" for="note"><span><i class="fa fa-pencil"></i> Add Personal Note</span></label>
                                                    <input class="form-control note" name="note<?php echo $key?>" type="text" style="cursor: auto;" placeholder="">
                                                </div>
                                            </h6>
                                        </div>
                                        <div class="col-md-3">
                                            <h6>
                                                <div class="form-group form-group-sm label-floating <?php (isset($value->noOfLeave)) ? '' : ''; ?>">
                                                    <label class="control-label" for="noOfLeave"><span>No Of Leave</span></label>
                                                        <input class="form-control noOfLeave" name="noOfLeave<?php echo $key?>" type="number" min="0" style="cursor: auto;" placeholder="" value="<?php 
                                                            if (isset($value->noOfLeave)) {
                                                                echo $value->noOfLeave;
                                                            } else {
                                                                echo '0';
                                                            } ?>">
                                                </div>
                                            </h6>
                                            <h6>
                                                <div class="form-group form-group-sm label-floating ">
                                                    <label class="control-label" for="overtime"><span><i class="fa fa-plus"></i> Overtime</span></label>
                                                    <input class="form-control overtime" name="overtime<?php echo $key?>" type="number" min="0" style="cursor: auto;" placeholder="" value="<?php 
                                                            if (isset($value->overtime)) {
                                                                echo $value->overtime;
                                                            } else {
                                                                echo '0';
                                                            } ?>">
                                                </div>
                                            </h6>
                                        </div>
                                        <div class="col-md-3">
                                            <h6>
                                                <div class="form-group form-group-sm label-floating ">
                                                    <label class="control-label" for="bonus"><span><i class="fa fa-plus"></i> Bonus</span></label>
                                                    <input class="form-control bonus" name="bonus<?php echo $key?>" type="number" style="cursor: auto;" placeholder="" value="<?php 
                                                            if (isset($value->bonus)) {
                                                                echo $value->bonus;
                                                            } else {
                                                                echo '0';
                                                            } ?>">
                                                </div>
                                            </h6>
                                            <h6>
                                                <div class="form-group form-group-sm label-floating ">
                                                    <label class="control-label" for="commission"><span><i class="fa fa-plus"></i> Commission</span></label>
                                                    <input class="form-control commission" name="commission<?php echo $key?>" type="number" style="cursor: auto;" placeholder="" value="<?php 
                                                            if (isset($value->commission)) {
                                                                echo $value->commission;
                                                            } else {
                                                                echo '0';
                                                            } ?>" >
                                                </div>
                                            </h6>
                                            <h6 >
                                                <div class="form-group form-group-sm label-floating ">
                                                    <label class="control-label" for="otherEarning"><span><i class="fa fa-plus"></i> Other Earnings</span></label>
                                                    <input class="form-control otherEarning" name="otherEarning<?php echo $key?>" type="number" style="cursor: auto;" placeholder="" value="<?php 
                                                            if (isset($value->otherEarning)) {
                                                                echo $value->otherEarning;
                                                            } else {
                                                                echo '0';
                                                            } ?>" >
                                                </div>
                                            </h6>
                                        </div>
                                        <div class="col-md-3">
                                            <h6>
                                                <div class="form-group form-group-sm label-floating ">
                                                    <label class="control-label" for="grossPay"><span><i class="fa fa-inr"></i> Gross Pay</span></label>
                                                    <input class="form-control grossPay" name="grossPay<?php echo $key?>" type="number" style="cursor: auto;" placeholder="" readonly>
                                                </div>
                                            </h6>
                                            <h6>
                                                <div class="form-group form-group-sm label-floating ">
                                                    <label class="control-label" for="reimbursement"><span><i class="fa fa-plus"></i> Reimbursement</span></label>
                                                    <input class="form-control reimbursement" name="reimbursement<?php echo $key?>" type="number" style="cursor: auto;" placeholder="" value="<?php 
                                                            if (isset($value->reimbursement)) {
                                                                echo $value->reimbursement;
                                                            } else {
                                                                echo '';
                                                            } ?>" >
                                                </div>
                                            </h6>
                                            <h6>
                                                <div class="form-group form-group-sm label-floating ">
                                                    <label class="control-label" for="deduction"><span><i class="fa fa-minus"></i> Deduction</span></label>
                                                    <input class="form-control deduction" name="deduction<?php echo $key?>" type="number" style="cursor: auto;" placeholder="" value="<?php 
                                                            if (isset($value->deduction)) {
                                                                echo $value->deduction;
                                                            } else {
                                                                echo '0';
                                                            } ?>" >
                                                </div>
                                            </h6>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <input type="hidden" name="count" value="<?php echo $count;?>">
                                <?php } } else { ?>
                                        You dont have any employee
                                  <?php } ?>
                            <!-- ============================================================= -->
                            <input type="submit" class="btn bg-theme btn-raised submit" value="Submit">
                        </form>
                    </div>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>
$(function() {
    $.material.init();

    var lastMonth = "<?php echo date('Y-m')?>".split("-");

    $(".employee .panel-body").hide();

    $(".employee .panel-heading").click(function(event) {
        $(this).siblings('.panel-body').slideToggle();
        var $arrow = $(this).find('i.fa').toggleClass('fa-caret-down fa-caret-up');
    });

    $(".noOfLeave").change(function(event) {
        event.preventDefault();
        // php has 1 index date and js has 0. 
        var noOfDays = new Date(lastMonth[0], lastMonth[1]-1, 0).getDate();     

        // noOfDays = DaysInMonth - noOfLeave
        var val = $(this).val()*1;
        $(this).val( Math.min( val, noOfDays));
        $(this).parents(".panel-body").siblings('.panel-heading').find('.noOfDays h5').text( Math.max(noOfDays - 1*$(this).val(), 0));
    });

    $(".overtime, .bonus, .commission, .otherEarning, .reimbursement, .deduction, .noOfLeave, .grossPay").change(function(event) {
        event.preventDefault();
        $employee = $(this).parents(".employee");

        var overtime = 1*$employee.find('.overtime').val() | 0;
        var bonus = 1*$employee.find('.bonus').val() | 0;
        var commission = 1*$employee.find('.commission').val() | 0;
        var otherEarning = 1*$employee.find('.otherEarning').val() | 0;
        var reimbursement = 1*$employee.find('.reimbursement').val() | 0;
        var deduction = 1*$employee.find('.deduction').val() | 0;
        var noOfLeave = 1*$employee.find('.noOfLeave').val()| 0;
        var noOfDays = new Date(lastMonth[0], lastMonth[1], 0).getDate() | 30;
        var monthlyPay = 1*$employee.find('.monthlyPay').val() | 0;
        var oneDayPay = 1*$employee.find('.oneDayPay').val() | 0;
        var otherFactorOfCTC = 1*$employee.find('.otherFactorOfCTC').val() | 0;

        var grossPay = ( overtime + bonus + commission + otherEarning + reimbursement - deduction )  
                        + ( monthlyPay - oneDayPay * noOfLeave + otherFactorOfCTC );
        if (noOfLeave == 31) grossPay = 0;
        grossPay = Math.round(grossPay);
        $employee.find('.grossPay').val(grossPay).parents(".label-floating").removeClass('is-empty');
        $employee.find('.grossPayHead span').text(grossPay);
    });

    $(".grossPay").trigger('change');

    // sending Data

    $("#employeesForm .submit").click(function(event) {
        event.preventDefault();

        $.ajax({
            url: location.href,
            type: 'POST',
            data: $("#employeesForm").serialize(),
        })
        .done(function(data) {
            console.log("success", data);
            swal({
                        title : "Success !",
                        text : "Events Successfully Saved",
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });
        })
        .fail(function() {
            console.log("error");
        })        
    });
});
</script>
