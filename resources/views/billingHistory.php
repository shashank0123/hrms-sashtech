<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>


<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">
                        <br>
                        <h3>Billing History</h3>
                        <br>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <h3 class="text-center">Filter</h3>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-6">
                            <div class="form-group label-floating">
                                <label for="from" class="col-md-2 control-label">From</label>

                                <div class="col-md-10">
                                    <input type="text" id="from" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-6">
                            <div class="form-group label-floating">
                                <label for="to" class="col-md-2 control-label">To</label>

                                <div class="col-md-10">
                                    <input type="text" id="to" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        <br>
                        <br>

                        <table class="table table-bordered" cols="3">
                            <tr>
                                <th>Bill Date</th>
                                <th>Bill Amount</th>
                                <th>Download</th>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>
$(function() {
    
    $.material.init();

    $("#to").attr('disabled', true);
    $('#from').bootstrapMaterialDatePicker({ weekStart : 0, time: false }).on('dateSelected', function(event) {
        $('#'+$(this).data('dtp')).find('.dtp-btn-ok').trigger('click');
    })
    .on('change', function(e, date) {
        $("#to").attr('disabled', false);
        $('#to').bootstrapMaterialDatePicker({ weekStart : 0, time: false , minDate : date}).on('dateSelected', function(event) {
            $('#'+$(this).data('dtp')).find('.dtp-btn-ok').trigger('click');
        });
    });
});
</script>
