<!DOCTYPE html>
<html>
<head>
    <title>Small Business Software India by Sashtechs </title>

    <meta name="description" content="Guaranteed accurate payroll software, made for small businesses. Full feature set including direct deposit. Fast, no-hassle online setup. Pay salaried employees, hourly workers. Never need a payroll calculator again." />

    <meta name="keywords" content="Payroll, payroll software, payroll-services, payroll-taxes, small business accounting, #1 payroll services company, tax filing, workers comp"/>

    <!--Import Google Icon Font-->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">

    <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet"></link>


    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Material Design -->
    <link href="css/bootstrap-material-design.css" rel="stylesheet">
    <link href="css/ripples.min.css" rel="stylesheet">
    <link href="css/roundslider.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">

    <script src="/js/jquery-1.12.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ripples.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/roundslider.min.js"></script>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
</head>

<body>
<?php require_once("staticNavBar.php");?>

<!-- ================  Header  ===================== -->

<header class="parallax" style="background: #000;">
    <div class="panel panel-default" style="background-image: url(images/map-image.png);color: #fff;background-color: transparent;">
        <div class="panel-body">
            <div class="text-center">
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <h5>CONTACT US</h5>
                <h3>Got a question? We’d love to hear from you. Send us a message and we’ll respond as soon as possible.</h3>
                <br>
                <br>
                <i class="fa fa-arrow-down"></i>
                <br>
                <br>
                <br>
                <div class="panel text-left" style="max-width:450px;margin: 0 auto;padding:15px;">
                    <form action="#" class="">
                        <div class="col-md-12">
                            <div class="form-group label-floating is-empty">
                                <label class="control-label" for="name">Name</label>
                                <input class="form-control" id="name" name="name" type="text" style="cursor: auto;">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group label-floating is-empty">
                                <label class="control-label" for="email">Email</label>
                                <input class="form-control" id="email" name="email" type="email" style="cursor: auto;">
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group label-floating is-empty">
                                <label for="workAddress" class="control-label">Message</label>

                                <textarea class="form-control" rows="2" id="workAddress"></textarea>
                            </div>                                
                        </div>

                        <button id="submit" class="btn bg-theme btn-raised btn-block">submit</button>
                    </form>
                </div>
                <br>
                <br>
            </div>
        </div>
    </div>
</header>

<div class="clearfix"> </div>
<section class="text-center">
    <br>
    <h2>Our Offices</h2>
    <br><br>
    <div class="center-block" style="overflow-x:hidden;">
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14006.413507270578!2d77.178747!3d28.641647!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x599dd386acd8c6a7!2ssashtechs!5e0!3m2!1sen!2sus!4v1455687721218" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    <br><br>
</section>
<div class="clearfix"></div>

<section class="parallax" style="background-image: url(images/try.jpg);">
    <div class="panel panel-default" style="  ">
        <div class="panel-body">
            <div class="text-center">
                <br>
                <br>
                <br>
                <br>
                <h5><b>TRY Sashtechs</b></h5>
                <h3 class="text-uppercase">Start your one month free trial </h3>
                <br>

                <div class="panel text-left" id="panel-text-left">
                    <?php require 'demoForm.php'; ?>
                </div>
                <br>
                    <div class="clearfix"></div>
                    <br><br>
                    <br><br>
            </div>
        </div>
    </div>
</section>


<?php require_once('staticFooter.php');?>
<script type="text/javascript" src="js/slick.min.js"></script>
</body>
</html>

<script>

    $(function() {
        $.material.init();
        
        $('.tweet-box').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            autoplay: true,
            autoplaySpeed: 3000,
            centerMode: true,
            centerPadding: '1em',
            slidesToShow: 3,
            variableWidth:true,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        arrows: true,
                        centerMode: true,
                        centerPadding: '2em',
                        slidesToShow: 3,
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1,
                    }
                }
            ]
        });
    });
</script>
