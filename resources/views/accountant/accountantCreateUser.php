<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>


<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">
                        <br>
                        <form class="form-horizontal" autocomplete="off" action="" method="POST">
                            <fieldset>

                            <input style="display:none">
                            <input type="password" style="display:none">

                                <legend>Create User <hr></legend>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                        <div class="form-group">
                                          <label for="email" class="col-md-4 control-label">Email</label>

                                          <div class="col-md-8">
                                            <input type="email" class="form-control" id="email" name="email">
                                          </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                        <div class="form-group">
                                            <label for="userType" class="col-md-4 control-label">User Type</label>

                                            <div class="col-md-8">
                                                <select id="userType" class="form-control" name="userType">
                                                    <option value="CA">CA (Manager)</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <br>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-4 col-md-offset-3 col-sm-4 col-sm-offset-3 col-xs-12">
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-raised bg-theme" value="Submit">
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <br>
                            </fieldset>
                        </form>

                        <div class="clearfix"></div>
                        <br><br>
                        <?php if (!empty($users) and (count($users) > 0)): ?>
                        <div class="table-container">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th width="80">S.No</th>
                                        <th width="250">Email_id</th>
                                        <th>UserType</th>
                                        <th>Status</th>
                                        <th width="100">Action</th>
                                    </tr>
                                    <?php foreach ($users as $key => $value):?>
                                    <tr>
                                        <td><?php echo ($key+1); ?></td>
                                        <td><?php echo $value->email; ?></td>
                                        <td><?php echo $value->type; ?></td>
                                        <td class="status"><?php echo $value->status; ?></td>
                                        <td>
                                            <div class="togglebutton">
                                                <label> 
                                                     <input type="checkbox" class="toggle" data-id="<?php echo $value->id; ?>" 
                                                    <?php 
                                                        if (strtolower($value->status) == 'active') 
                                                        {
                                                            echo "checked";
                                                        }
                                                     ?>
                                                     >
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>

                        <?php else: ?>
                            You have no users as of now.
                        <?php endif; ?>
                        </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>

    function displayError() {
        swal({
            title : "Error !",
            text : 'Try Again!',
            type : 'error',
            confirmButtonClass : 'bg-theme',
            animation : false,
            width : 400
        });
    };

    function closeAlert() {
        $(".sweet-alert button").first().trigger('click');
    }

    $(function() {
        
        // add active class to corresponding link in menu 
        $(".nav li.active").removeClass('active');
        $(".nav.nav-stacked li").filter(function(index) {
            return $(this).data('url') == 'accountantCreateUser';
        }).addClass('active');

        $.material.init();

        $(".toggle").change(handleToggle);

        function handleToggle() {
            var _this = $(this);
            var id = $(this).data('id');
            var status = $(this).is(':checked') ? "Active" : "Inactive";
            $(this).prop('disabled', true);

            $.ajax({
                url: location.href + '/' + id,
                type: 'POST',
                data: {status: status},
            })
            .done(function(data) {
                if (data.status == 200) {
                    _this.parents("td").siblings('.status').text(status);
                }
                _this.prop('disabled', false);
            });
            
        };

        $("form.form-horizontal").submit(function(event) {
            event.preventDefault();

            $(".help-block").remove();
            $(".has-error").removeClass('has-error');
            
            swal({
                title : 'Saving',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 400
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: $('.main form').serialize(), 
            })
            .done(function(data) {
                if (data.status == 200) {
                    swal({
                        title : "Success !",
                        text : data.message,
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });

                    if (!$(".main .table").length) {

                        getAndInsert(location.pathname);
                        return false;
                    }

                    var slNO = 1*$(".table tr").last().children().first().text() + 1;
                    var $html = $('<tr>'+
                            '<td>'+slNO+'</td>'+
                            '<td>'+ $("#email").val() +'</td>'+
                            '<td>'+$("#userType").val()+'</td>'+
                            '<td class="status">Inactive</td>'+
                            '<td>'+
                            '   <div class="togglebutton">'+
                            '        <label>'+ 
                            '           <input type="checkbox" class="toggle" data-id="'+data.id+'" >'+
                            '        </label>'+
                            '    </div>'+
                            '</td>'+
                        '</tr>');

                    $html.find('.toggle').change(handleToggle);

                    $(".main .table").append($html);

                    $('form').trigger('reset');
                    $('form .has-error').removeClass('has-error');
                    $.material.init();
                }
                else if(data.status == 208) {  //  email already Exist
                    closeAlert();
                    var $email = $("#email");
                    $email.parent().append('<p class="help-block">'+ data.message +'</p>');
                    $email.focus();
                    $email.parents('.form-group').addClass('has-error');
                }
                else{  // Validation Error
                    displayError();
                    // show Errors
                    for(var fields in data) {
                        $('#'+fields).parents('.form-group').addClass('has-error');
                        $('#'+fields).parent().append('<p class="help-block">'+ data[fields][0] +'</p>');
                    }
                }
            })
            .error(function() {
                displayError();
            });
        });
    })
</script>
