<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>


<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">
                        <br>
                        <br>
                        
                        <form class="form-horizontal" enctype="multipart/form-data" autocomplete="off" action="" method="POST">
                            <fieldset>

                                <br>
                                <h4>Please complete your profile</h4>
                                <br>
                                <div id="preview">
                                    <label for="profilePic" <?php echo (!empty($result2->dp)) ? 'style="background-image: url(\'//'.$_SERVER['HTTP_HOST'].'/'.$result2->dp.'\');"' : ' '; ?>>CHOOSE PHOTO</label>
                                    <input type="file" accept="image/*" name="profilePic" id="profilePic" style="display:none;">
                                </div>
                                                                
                                <div class="col-md-12 col-sm-12 col xs-12">
                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="firstName"><span>First Name</span></label>
                                            <input class="form-control" id="firstName" name="firstName" type="text" style="cursor: auto;" <?php echo (!empty($result1->firstName)) ? 'value="'.$result1->firstName.'"' : '' ; ?> >
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-12 col-sm-12 col xs-12">
                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="lastName"><span>Last Name</span></label>
                                            <input class="form-control" id="lastName" name="lastName" type="text" style="cursor: auto;" <?php echo (!empty($result1->lastName) ) ? 'value="'.$result1->lastName.'"' : '' ; ?> >
                                        </div>
                                    </div>
                                </div>

                                
                                <div class="col-md-12 col-sm-12 col xs-12">
                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="phone"><span>Phone</span></label>
                                            <input class="form-control" id="phone" name="phone" type="text" id="cursor-auto" <?php echo (!empty($result2->phone)) ? 'value="'.$result2->phone.'"' : '' ; ?> >
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col xs-12">
                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="company"><span>Company Name</span></label>
                                            <input class="form-control" id="company" name="company" type="text" style="cursor: auto;"  <?php echo (!empty($result2->company)) ? 'value="'.$result2->company.'"' : '' ; ?> >
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-12 col-sm-12 col xs-12">
                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="email"><span>Email</span></label>
                                            <input class="form-control" id="email" name="email" type="email" style="cursor: auto;" value="<?php 
                                                $email = session()->get('email');
                                                echo (isset($email)and!empty($email)) ? $email . '" disabled=\"true' : '' ;?>" >
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-12 col-sm-12 col xs-12">
                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-raised bg-theme" value="submit">
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </fieldset>
                        </form>

                        <div class="clearfix"></div>

                        <style>

                            #preview{
                                background: #fff;
                                position: relative;
                                height: 10em;
                                width: 10em;
                                margin: 10px auto;
                                border: 1px solid #aaa;
                            }

                            #preview > label{
                                margin: 0 auto;
                                display: block;
                                width: 10em;
                                height: 100%;
                                text-align: center;
                                line-height: 9em;
                                border-radius: 50%;
                                background-size: cover;
                            }
                        </style>
                        <br><br>

                    </div>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>

    function displayError() {
        swal({
            title : "Error !",
            text : 'Try Again!',
            type : 'error',
            confirmButtonClass : 'bg-theme',
            animation : false,
            width : 400
        });
    };

    function closeAlert() {
        $(".sweet-alert button").first().trigger('click');
    }

    $(function() {

        // add active class to corresponding link in menu 
        $(".nav li.active").removeClass('active');
        $(".nav.nav-stacked li").filter(function(index) {
            return $(this).data('url') == 'accountantDetail';
        }).addClass('active');

        $.material.init();

        $("#profilePic").on('change', function(event) {
            if (event.target.files.length) {
                var file = event.target.files[0];
                if (file && file.size > (500*1024)) {// under 500kb file is allowed
                    swal({
                        title : "Error !",
                        text : 'File Size is more than 500 KB',
                        type : 'error',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });
                    return false;                    
                }

                if (file && file.type != 'image/png' && file.type != 'image/jpeg') {
                    swal({
                        title : "Error !",
                        text : 'only png and jpeg images are allowed',
                        type : 'error',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });
                    return false; 
                }
                $("#preview > label").css('background-image', 'url('+URL.createObjectURL(file)+')').text(' ');
                $("#preview").css('border', 'none');
            }
            else{
                $("#preview > label").css('background-image', 'url()').text('CHOOSE PHOTO');
                $("#preview").css('border', '1px solid #aaa');
            }

        });

        // if image is already uploaded
        <?php if (!empty($result2->dp)) : ?>
        $("#preview > label").text(' ');
        $("#preview").css('border', 'none');
        <?php endif; ?>

        $("form.form-horizontal").submit(function(event) {
            event.preventDefault();

            $(".help-block").remove();
            $(".has-error").removeClass('has-error');
            
            swal({
                title : 'Saving',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 400
            });

            var formData = new FormData();
            var file = $("#profilePic")[0].files[0];
            if (file && file.size <= (500*1024) && (file.type == 'image/png' || file.type == 'image/jpeg'))
                formData.append('profilePic', file);
            else
                formData.append('profilePic', "");
            formData.append('firstName', $("#firstName").val());
            formData.append('lastName', $("#lastName").val());
            formData.append('phone', $("#phone").val());
            formData.append('company', $("#company").val());
            formData.append('email', $("#email").val());

            $.ajax({
                url: location.href,
                type: 'POST',
                data: formData,
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false  
            })
            .done(function(data) {
                d = data;
                if (data.status == 200) {
                    swal({
                        title : "Success !",
                        text : data.message,
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });   
                    // getAndInsert(data.url);
                    // history.pushState(null, null, data.url);
                }
                else if (data.status == 405) {
                    swal({
                        title : "Not Allowed!",
                        text : data.message,
                        type : 'error',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });
                }
                else
                {  // Validation Error
                    displayError();
                    // show Errors
                    for(var fields in data) {
                        $('#'+fields).parents('.form-group').addClass('has-error');
                        $('#'+fields).parent().append('<p class="help-block">'+ data[fields][0] +'</p>');
                    }
                }
            })
            .error(function() {
                displayError();
            });
        });

        $('.main .form-group input').trigger('change');
    })
</script>
