<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>


<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <?php if (empty($profile)): ?>
                    <div class="panel-body" style="min-height:36em;">
                        <br><br><br>
                        <center><h4>WELCOME TO <br> <h1>Sashtechs</h1></h4></center>
                        <br>

                        <div class="text-center">
                            <span>Hi <?php echo session()->get('accountantName'); ?>! Your account is almost ready. We just have a couple of quick questions to ask.</span>
                        </div>

                        <br>
                        <br>
                        <br>
                        <br>
                        <center><a href="/accountantDetail" class="ajaxify btn bg-theme btn-raised"><i class="fa fa-share"></i> &nbsp; Lets Get Started</a></center><br>
                    </div>
                    <?php else: ?>
                    <div class="panel-body" style="min-height:36em;">
                            <div id="wrapper">

                                <div id="page-wrapper">

                                    <div class="container-fluid">

                                        <!-- Page Heading -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h1 class="page-header">
                                                    <!-- Dashboard --> <small>Statistics Overview</small>
                                                </h1>
                                            </div>
                                        </div>

                                        <!-- /.row -->

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">
                                                        <div class="row">
                                                            <div class="col-xs-3">
                                                                <i class="fa fa-building fa-5x"></i>
                                                            </div>
                                                            <div class="col-xs-9 text-right">
                                                                <div class="huge"><?php echo isset($count->company) ? $count->company : ''; ?></div>
                                                                <div>Company Registered</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a href="#">
                                                        <div class="panel-footer">
                                                            <span class="pull-left">View Details</span>
                                                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="panel panel-green">
                                                    <div class="panel-heading">
                                                        <div class="row">
                                                            <div class="col-xs-3">
                                                                <i class="fa fa-user fa-5x"></i>
                                                            </div>
                                                            <div class="col-xs-9 text-right">
                                                                <div class="huge"><?php echo isset($count->user) ? $count->user : ''; ?></div>
                                                                <div>Users Registered</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a href="#">
                                                        <div class="panel-footer">
                                                            <span class="pull-left">View Details</span>
                                                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="panel panel-yellow">
                                                    <div class="panel-heading">
                                                        <div class="row">
                                                            <div class="col-xs-3">
                                                                <i class="fa fa-list-alt fa-5x"></i>
                                                            </div>
                                                            <div class="col-xs-9 text-right">
                                                                <div class="huge"><?php echo isset($count->payslips) ? $count->payslips : ''; ?></div>
                                                                <div>Payslip Generated</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a href="#">
                                                        <div class="panel-footer">
                                                            <span class="pull-left">View Details</span>
                                                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="panel panel-red">
                                                    <div class="panel-heading">
                                                        <div class="row">
                                                            <div class="col-xs-3">
                                                                <i class="fa fa-user-md fa-5x"></i>
                                                            </div>
                                                            <div class="col-xs-9 text-right">
                                                                <div class="huge"><?php echo isset($count->benefits) ? $count->benefits : ''; ?></div>
                                                                <div>Benefits enrolled</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a href="#">
                                                        <div class="panel-footer">
                                                            <span class="pull-left"><?php echo (isset($count->newBenefits)and($count->newBenefits > 0)) ? $count->newBenefits . ' benefits require your attention' :'View Details'; ?></span>
                                                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- /.container-fluid -->

                                </div>
                                <!-- /#page-wrapper -->

                            </div>

                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>
    $(function() {

        // add active class to corresponding link in menu 
        $(".nav li.active").removeClass('active');
        $(".nav.nav-stacked li").filter(function(index) {
            return $(this).data('url') == 'accountantDashboard';
        }).addClass('active');

        $.material.init();

    })
</script>
