<!-- ======================= Footer ======================= -->
<footer class="footer">
    <br><br>    
    <div class="container">
        <div class="col-md-3 col-sm-3 col-xs-6">
            <div class="btn-group-vertical btn-block">
                <p class="text-center">RESOURCES</p>
                <!-- <a href="/blog" class="btn">Blog</a> -->
                <a href="/pressAndMedia" class="btn">Press</a>
                <!-- <a href="javascript:void(0)" class="btn">Help Center</a> -->
                <a href="/faq" class="btn">FAQ</a>
            </div>
            <br>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6">
            <div class="btn-group-vertical btn-block">
                <p class="text-center">COMPANY</p>
                <a href="/aboutUs" class="btn">About us</a>
                <a href="/customer" class="btn">Customers</a>
                <a href="/accountant" class="btn">Partners</a>
                <a href="/pricing" class="btn">Pricing</a>
                <a href="/security" class="btn">Security</a>
                <a href="/careers" class="btn">Careers</a>
            </div>
            <br>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6">
            <div class="btn-group-vertical btn-block">
                <p class="text-center">HR PLATFORM</p>
                <a href="/payroll" class="btn">Payroll</a>
                <a href="/expense" class="btn">Expenses</a>
                <a href="/health-insurance-benefit" class="btn">Benefits</a>
                <a href="/timeSheet" class="btn">Time</a>
            </div>
            <br>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6">
            <div class="btn-group-vertical btn-block">
                <p class="text-center">TOOLS</p>
                <a href="/inHandPayCalculator" class="btn">Monthly Pay</a>
                <a href="/generateRentReceipt" class="btn">Rent Receipt</a>
            </div>
            <br>
            <br>
            <div class="btn-group-vertical btn-block">
                <p class="text-center">GET IN TOUCH</p>
                <a href="/contactUs" class="btn">Contact</a>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="copyright pull-left hidden-xs">
            <h5><i class="fa fa-copyright"></i> &nbsp; 2015 Legal Stuff</h5>
        </div>
        <div class="social-icons pull-right">
            <a href="https://www.facebook.com/Sashtechs/" class="btn btn-sm btn-default"><h5><i class="fa fa-facebook"></i></h5></a>
            <a href="https://www.linkedin.com/company/Sashtechs" class="btn btn-sm btn-default"><h5><i class="fa fa-linkedin"></i></h5></a>
            <a href="https://twitter.com/Sashtechs" class="btn btn-sm btn-default"><h5><i class="fa fa-twitter"></i></h5></a>
            <!-- <a href="#" class="btn btn-sm btn-default"><h5><i class="fa fa-google-plus"></i></h5></a> -->
            <a href="https://www.youtube.com/channel/UC0sGh6p8sUsU6tw2ATqHw1Q" class="btn btn-sm btn-default"><h5><i class="fa fa-youtube-play"></i></h5></a>
        </div>
    </div>

    <div class="clearfix"></div>
</footer>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-75933519-1', 'auto');
  ga('send', 'pageview');

</script>
