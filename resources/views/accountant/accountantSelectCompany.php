<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>

<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">
                        <br>
                        <h3>Companies</h3>
                        <br>

                        <div id="companies">
                            <?php if (!empty($company)):?>
                                <?php foreach ($company as $key => $value):?>
                                    <div class="company col-md-4 col-sm-6 col-xs-12">
                                        <div class="content">
                                            <img src="//<?php echo isset($value->logoUrl) ? $_SERVER['HTTP_HOST'] . '/' . $value->logoUrl : '';?>" alt="<?php echo $value->companyName?>" class="img-thumbnail center-block">
                                            <br>
                                            <p class="text-center"><?php echo $value->companyName?></p>
                                            <div>
                                                <a href="accountantSelectCompany/<?php echo $value->id?>" class="btn btn-block bg-theme">Select 
                                                <?php
                                                if (strlen($value->companyName) > 10) 
                                                {
                                                    echo substr($value->companyName, 0, 10) . '...';
                                                }
                                                else
                                                {
                                                    echo $value->companyName;
                                                }
                                                ?></a>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <p>You don't have any companies as of now, please add one first.</p>
                            <?php endif; ?>
                        </div>
                    </div>
                    <style>
                        .company .content{
                            padding: 10px;
                            margin: 15px;
                            border: 1px solid #ddd;
                            box-shadow: 0px 0px 5px #ddd;
                        }
                        .company .content > img {
                            border: none;
                            max-height: 10em;
                            max-width: 10em;
                        }

                        @media screen and (max-width: 450px) {
                            .company .content{
                                padding: 0 5px;
                                margin: 15px 0px;
                            }
                        }
                    </style>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>

    $(function() {

        // add active class to corresponding link in menu 
        $(".nav li.active").removeClass('active');
        $(".nav.nav-stacked li").filter(function(index) {
            return $(this).data('url') == 'accountantSelectCompany';
        }).addClass('active');

        $.material.init();
        
    })
</script>
