<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial ManagemsetupLeaveent</title>
    <?php require_once('links.php');?>
</head>


<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">
                        
                        <br>
                        <br>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <form class="form-horizontal" autocomplete="off" action="" method="POST">
                                <input style="display:none">
                                <input type="password" style="display:none">
                                <fieldset>
                                    <legend>Add New Company</legend>
                                    
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating ">
                                                <label class="control-label" for="EmployerName"><span>Employer Name</span></label>
                                                <input class="form-control" id="EmployerName" type="text" name="EmployerName" style="cursor: auto;" autocomplete="off">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating ">
                                                <label class="control-label" for="supportmail"><span>Support Email</span></label>
                                                <input class="form-control" id="supportmail" type="email" name="supportmail" style="cursor: auto;" autocomplete="off">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating ">
                                                <label class="control-label" for="companyName"><span>Company Name</span></label>
                                                <input class="form-control" id="companyName" type="text" name="companyName" style="cursor: auto;" autocomplete="off">
                                            </div>
                                        </div>
                                    </div>

                                    <br><br>
                                    <div class="clearfix"></div>
                                    <br>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <input type="submit" class="btn bg-theme btn-raised submit" value="add company">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                </fieldset>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>


    function displayError() {
        swal({
            title : "Error !",
            text : 'Try Again or Fix your Error!',
            type : 'error',
            confirmButtonClass : 'bg-theme',
            animation : false,
            width : 400
        });
    };

    function closeAlert() {
        $(".sweet-alert button").first().trigger('click');
    }
        
    $(function() {

        // add active class to corresponding link in menu 
        $(".nav li.active").removeClass('active');
        $(".nav.nav-stacked li").filter(function(index) {
            return $(this).data('url') == 'addCompany';
        }).addClass('active');

        $.material.init();

        $("#supportmail").on('change', function(event) {
            event.preventDefault();
            var val = $(this).val().split('@');
            if (val.length<2) return false;
            val = val.pop();
            $("#companyName").val(val.split('.')[0].toUpperCase());
            $("#companyName").trigger('change');
        });

        $("form.form-horizontal").submit(function(event) {

            event.preventDefault();

            // remove all error messages
            $(".help-block").remove();
            $(".has-error").removeClass('has-error');
            
            swal({
                title : 'Please Wait',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 300
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: $('.form-horizontal').serialize(), 
            })
            .done(function(data) {
                if (data.status == 200) {
                    swal({
                        title : "Added!",
                        type : 'success',
                        showConfirmButton : false,
                        animation : false,
                        width : 300,
                        timer: 1500
                    });

                    $('form').trigger('reset');
                    $('form .has-error').removeClass('has-error');
                    $('form .label-floating').addClass('is-empty');
                }
                else if(data.status == 208) {  //  email already Exist
                   closeAlert();
                   var $email = $("#supportmail");
                   $email.parent().append('<p class="help-block">'+ data.message +'</p>');
                   $email.focus();
                   $email.parents('.form-group').addClass('has-error');
                }
                else if(data.status == 209) {  //  email already Exist
                   closeAlert();
                   var $companyName = $("#companyName");
                   $companyName.parent().append('<p class="help-block">'+ data.message +'</p>');
                   $companyName.focus();
                   $companyName.parents('.form-group').addClass('has-error');
                }
                else if(data.status == 406) {  //  email already Exist
                   closeAlert();
                   var $email = $("#supportmail");
                   $email.parent().append('<p class="help-block">'+ data.message +'</p>');
                   $email.focus();
                   $email.parents('.form-group').addClass('has-error');
                }
                else{  // Validation Error
                    displayError();
                    // show Errors
                    for(var fields in data) {
                        $('#'+fields).parents('.form-group').addClass('has-error');
                        $('#'+fields).parent().append('<p class="help-block">'+ data[fields][0] +'</p>');
                    }
                }
            })
            .error(function() {
                displayError();
            });
        });

    })
</script>
