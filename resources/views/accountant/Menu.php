<div class="container hidden-xs">
    <div class="row">
        <div class="col-md-3 col-sm-3"></div>
        <div class="col-md-9 col-sm-9 small text-uppercase">
            <div class="col-md-6 col-sm-6 heart-beat">
            </div>

            <div class=" col-md-6 col-sm-6 text-right dropdown">
                <a href="javascript:void(0)" data-target="#" class="dropdown-toggle" data-toggle="dropdown">Welcome 
              <?php echo session()->get('accountantName');?>
                <b class="caret"></b></a>
                <ul class="dropdown-menu pull-right">
                    <li><a class="h6" href="/logout">Log Out</a></li>
                </ul>
            </div>
        </div>
        <br>
    </div>
</div>
<!-- ================  Body  ===================== -->

<style>    
    .nav.nav-stacked .active>a{
        color: #e54255;
        font-weight: 400;
    }

    #logo .icon-bar {
        border: 1px solid;
    }

    #logo .navbar-toggle {
        position: absolute;
        top: 3px;
        right: 0;
    }
</style>

<div class="container">
    <div class="row">
        <nav class="col-md-3 col-sm-3">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <span id="logo" class="fa btn" >Sashtechs <div></div>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-warning-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </span>
            </div>
            <div class="clearfix"></div>
            <div class="navbar-collapse collapse navbar-warning-collapse">
                <ul class="nav navbar nav-stacked" style="background:transparent">
                    <li data-url="accountantDashboard" class="active"><a class="ajaxify btn" href="/accountantDashboard"><i class="fa fa-flag fa-lg"></i>Dashboard</a></li>
                    <li data-url="accountantDetail"><a class="ajaxify btn" href="/accountantDetail"><i class="fa fa-user fa-lg"></i>Profile</a></li>
                    <li data-url="accountantCreateUser"><a class="ajaxify btn" href="/accountantCreateUser"><i class="fa fa-users fa-lg"></i>Create User</a></li>
                    <li class="disabled"><br></li>
                    <li data-url="accountantAddCompany"><a class="ajaxify btn" href="/accountantAddCompany"><i class="fa fa-building-o fa-lg"></i>add Company</a></li>
                    <li data-url="accountantAssignCompany"><a class="ajaxify btn" href="/accountantAssignCompany"><i class="fa fa-building fa-lg"></i>assign Company</a></li>
                    <li data-url="accountantSelectCompany"><a class="ajaxify btn" href="/accountantSelectCompany"><i class="fa fa-building fa-lg"></i>select Company</a></li>
                    <li data-url="accountantBilling"><a class="ajaxify btn" href="/accountantBilling"><i class="fa fa-file fa-lg"></i>Billing</a></li>
                </ul>
            </div>
        </nav>
