<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>

<body  style="margin-top:2em;">.

<?php 
    if (isset($result)) 
    {
        $len = strlen($result->weekleave);
        if ($len > 1) 
        {
            $weekLeave = explode(', ', $result->weekleave);
        }
        else
        {
            $weekLeave = array('0' => $result->weekleave );
        }
    }
 ?>
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;padding-bottom: 1em;position:relative;">
                        <br>
                        <legend>Setup Leave</legend>
                            
                        <form action="#" class="form-horizontal col-md-12" id="setupLeave">

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="form-group">
                                        <label for="setupHoliday" class="col-md-4 control-label">Setup Holiday</label>
                                        <div class="col-md-7">
                                            <select id="setupHoliday" name="setupHoliday" required class="form-control">
                                            <?php if (!empty($result->setupHoliday)): ?>
                                                <?php 
                                                    switch (strtolower($result->setupHoliday)) 
                                                    {
                                                        case 'in':
                                                            echo '<option value="IN">Indian Holiday</option>';
                                                            break;

                                                        case 'uk':
                                                            echo '<option value="UK">UK Holiday</option>';
                                                            break;

                                                        case 'usa':
                                                            echo '<option value="USA">USA Holiday</option>';
                                                            break;
                                                        
                                                        default:
                                                            echo '
                                                            <option value="">Select Country Holiday</option>
                                                            <option value="IN">Indian Holiday</option>
                                                            <option value="UK">UK Holiday</option>
                                                            <option value="USA">USA Holiday</option>';
                                                            break;
                                                    }
                                                 ?>
                                             <?php else: ?>
                                                <option value="">Select Country Holiday</option>
                                                <option value="IN">Indian Holiday</option>
                                                <option value="UK">UK Holiday</option>
                                                <option value="USA">USA Holiday</option>
                                            <?php endif ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p> Select predefined leaves provided  by Sashtechs. Of course you can edit and change based on your company calendar</p>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12 ">
                                    <div class="form-group">
                                        <label for="weekLeave" class="col-md-4 control-label">Weekly Leave</label>
                                        <div class="col-md-7">
                                            <select id="weekLeave" name="weekLeave[]" class="form-control" multiple="multiple">
                                            
                                                <option value="0" selected <?php echo ((isset($weekLeave))&&(in_array('0', $weekLeave))) ? 'selected' : ''; ?>> Sunday</option>
                                                <option value="1" <?php echo ((isset($weekLeave))&&(in_array('1', $weekLeave))) ? 'selected' : ''; ?>> Monday</option>
                                                <option value="2" <?php echo ((isset($weekLeave))&&(in_array('2', $weekLeave))) ? 'selected' : ''; ?>> Tuesday</option>
                                                <option value="3" <?php echo ((isset($weekLeave))&&(in_array('3', $weekLeave))) ? 'selected' : ''; ?>> Wednesday</option>
                                                <option value="4" <?php echo ((isset($weekLeave))&&(in_array('4', $weekLeave))) ? 'selected' : ''; ?>> Thrusday</option>
                                                <option value="5" <?php echo ((isset($weekLeave))&&(in_array('5', $weekLeave))) ? 'selected' : ''; ?>> Friday</option>
                                                <option value="6" <?php echo ((isset($weekLeave))&&(in_array('6', $weekLeave))) ? 'selected' : ''; ?>> Saturday</option>
                                            </select>
                                            <p class="help-block">Hold Ctrl to select multiple days</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <br>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="casualLeave"><span>Casual Leave</span></label>
                                        <input class="form-control" id="casualLeave" value="<?php echo (isset($result->casualLeave)) ? $result->casualLeave :''; ?>" name="casualLeave" type="number" style="cursor: auto;" min="0">
                                    </div>
                                </div>
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p> Configure the days in a year for different types of leaves</p>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="earnedLeave"><span>Earned Leave</span></label>
                                        <input class="form-control" id="earnedLeave" value="<?php echo (isset($result->earnedLeave)) ? $result->earnedLeave :''; ?>" name="earnedLeave" type="number" style="cursor: auto;" min="0">
                                    </div>
                                </div>
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p> Configure the days in a year for different types of leaves</p>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="sickLeave"><span>Sick Leave</span></label>
                                        <input class="form-control" id="sickLeave" name="sickLeave" type="number" style="cursor: auto;" min="0" value="<?php echo (isset($result->sickLeave)) ? $result->sickLeave :''; ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="form-group label-floating">
                                        <label for="payDay" class="control-label">Pay Day</label>
                                        <input type="text" name="payDay" id="payDay" class="form-control" value="<?php echo (isset($result->payDay)) ? $result->payDay :''; ?>" <?php echo (isset($result->payDay)) ? 'readonly' :''; ?> >
                                        <p class="help-block">You can not change this after the initial save</p>
                                    </div>
                                </div>
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p> When your employees will be paid for that particular month</p>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="form-group label-floating">
                                        <label for="runPayroll" class="control-label">Run Payroll</label>
                                        <input type="text" name="runPayroll" id="runPayroll" class="form-control" value="<?php echo (isset($result->runPayroll)) ? $result->runPayroll :''; ?>" <?php echo (isset($result->runPayroll)) ? 'readonly' :''; ?>>
                                        <p class="help-block">You can not change this after the initial save</p>
                                    </div>
                                </div>
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p> The day when employer will review and trigger the payroll. Once confirmation is sent to banks then it's irreversible</p>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <br>
                            
                            <input type="submit" class="btn bg-theme btn-raised save" value="Save">
                            <span class="btn bg-theme btn-raised preview">Preview</span>
                        </form>

                        <div class="clearfix"></div>
                        <br>
                    <div class="calendarContainer hidden">
                        <hr>
                        <span id="prevYear" class="custom-btn btn bg-theme"><i class="fa fa-backward"></i></span>
                        <span id="prev" class="custom-btn btn bg-theme"><i class="fa fa-chevron-left"></i></span>
                        <span id="today" class="custom-btn btn bg-theme">today</span>
                        <span id="next" class="custom-btn btn bg-theme"><i class="fa fa-chevron-right"></i></span>
                        <span id="nextYear" class="custom-btn btn bg-theme"><i class="fa fa-forward"></i></span>
                        <div id="calendar"></div>
                    </div>
                    </div>
                    <style>
                        #calendar {
                            position: relative;
                        }
                        .custom-btn.btn{
                            padding: 8px 15px;
                        }
                        .event-btn.btn{
                            padding: 5px 13px;
                            font-size: 16px;
                        }
                        .eventPopup{
                            position: absolute;
                            top: 0;
                            left: 0;
                            z-index: 1000;
                            height: 100%;
                            width: 100%;
                        }
                        .eventPopup > .edit{
                            box-shadow: 0 0 10px #aaa;
                            position: relative;
                            z-index: 1;
                            padding: 1px 10px;
                            width: 20em;
                            margin: 0 auto;
                            top: 40%;
                            background: #fff;
                        }
                        .eventPopup > .overlay{
                            background: rgba(255, 255, 255, 0.75);
                            z-index: 1;
                            position: absolute;
                            top: 0;
                            left:0;
                            width: 100%;
                            height: 100%;
                        }
                        .form-group .help-block {
                            color: #888;
                            margin-top: -4px;
                        }
                        .fc-event { 
                            padding: 1px 5px;
                            cursor: pointer;
                        }
                    </style>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>

    function closeAlert() {
        $(".sweet-alert button").first().trigger('click');
    }

    function displayError(mesg) {
        swal({
            title : "Error !",
            text : mesg,
            type : 'error',
            confirmButtonClass : 'bg-theme',
            animation : false,
            width : 400
        });
    };

    $(function() {
        
        $.material.init();

        var $calendar = $('#calendar');
        var HOLIDAY = {
            backgroundColor : '#07C529',
            editable : true,
            events : []
        };
        var EVENTS = [];
        var WEEKLEAVE = [];
        var payDay = new moment(new Date($('#payDay').val()));
        var runPayroll = new moment(new Date($('#runPayroll').val()));

        var currentEvent = null;

        //  ===================================================================================
        var deleteHoliday = function(id) {
            var events = HOLIDAY.events;
            var index = events.findIndex(function(index) {
                return index.id == id;
            });

            var done = HOLIDAY.events.splice(index, 1);
            if (done) return true;
            else return false;
        }
        // ===================================================================================
        var addHoliday = function(event) {
            delete event.backgroundColor;
            HOLIDAY.events.push(event);
        }
        // =====================================================================================
            var updateEvent = function(changedEvent, title) {
            var index = HOLIDAY.events.findIndex(function(index) {
                return index.id == changedEvent.id;
            });

            HOLIDAY.events[index].start = changedEvent.start.format();
            HOLIDAY.events[index].end = changedEvent.end.format();
        }
        // =====================================================================================

        var generateFinalData = function() {
            var finalData = {};

            if (!EVENTS.length) {
                payDay = new moment(new Date($('#payDay').val()));
                runPayroll = new moment(new Date($('#runPayroll').val()));

                if ($('#payDay').val()) repeatEvent(payDay, 'Pay Day');
                if ($('#runPayroll').val()) repeatEvent(runPayroll, 'Run Payroll');
            }

            var payday = {
                backgroundColor : '#A52A2A',
                editable : false,
                events : []
            }

            if (EVENTS.length) {

                for (var i = EVENTS.length - 1; i >= 0; i--) {
                    delete EVENTS[i].backgroundColor;
                    delete EVENTS[i].editable;
                    delete EVENTS[i].stick;
                }

                payday.events = EVENTS;
            }
            // if there is no payday selected then event array will be empty
            finalData.payday = payday;
            // if there is no payday selected then event array will be empty
            finalData.holiday = HOLIDAY;

            finalData.weekleave = WEEKLEAVE;

            return finalData;
        }
// ===================================================================================== 

        $("#setupHoliday").change(function(event) {
            
            var val = $(this).val();
            HOLIDAY.events.length = 0;
            
            if (!val) return false;

            swal({
                title : 'Fetching Holiday',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 400
            });

            $.ajax({
                url: location.href + '/holiday',
                type: 'POST',
                data: {country: val},
            })
            .done(function(data) {
                if (!HOLIDAY.events.length) {
                    HOLIDAY.events = JSON.parse(data);
                    closeAlert();
                }
            })
            .fail(function() {
                displayError("Holiday Fetching Failed, Try Again");
            })
            
        });

        $("#setupHoliday").trigger('change');

        $("#weekLeave").change(function(event) {
            var days = $(this).val();
            if (days != null) 
            {
                days = days.join();
                days = days.split(',').map(function(i){return i*1});
                WEEKLEAVE = days;
            }
            else
            {
                WEEKLEAVE = [];
            }
        });

        $("#weekLeave").trigger('change');

        <?php if(empty($result->payDay)): ?>
        $('#payDay').bootstrapMaterialDatePicker({ weekStart : 0, time: false }).on('dateSelected', function(event) {
            $('#'+$(this).data('dtp')).find('.dtp-btn-ok').trigger('click');
        });
        <?php endif; ?>
        <?php if(empty($result->runPayroll)): ?>
        $('#runPayroll').bootstrapMaterialDatePicker({ weekStart : 0, time: false }).on('dateSelected', function(event) {
            $('#'+$(this).data('dtp')).find('.dtp-btn-ok').trigger('click');
        });
        <?php endif; ?>

        $(".form-horizontal").submit(function(event) {
            event.preventDefault();
            swal({
                title : 'Saving Events',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 400
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: $('.main form').serialize() + '&events='+JSON.stringify(generateFinalData()),
            })
            .done(function(data) {
                if (data.status==200) {
                    swal({
                        title : "Success !",
                        text : "Events Successfully Saved",
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });
                    getAndInsert(data.url);
                    history.pushState(null, null, data.url);
                }
                else {
                    displayError('Try Again!');
                }
            })
            .fail(function() {
                displayError('Try Again!');
            })             
        });

        function createPopup(operation) {

            var $html = "";
            if (operation == 'custom') {
                var $html = '<div class="eventPopup">'+
                    '<div class="overlay"></div>'+
                    '<div class="edit">'+
                        '<form>'+
                            '<div class="h6">New Event<span class="cancel pull-right"><i class="fa fa-times"></i></span></div>'+
                            '<input type="hidden" class="id">'+
                            '<input type="text" class="newTitle"/> <br />'+
                            '<span class="event-btn btn btn-xs bg-theme  pull-right change add"><i class="fa fa-check"></i></span>'+
                            '<span class="event-btn btn btn-xs bg-theme cancel"><i class="fa fa-times"></i></span>'+
                        '</form>'+
                    '</div>'+
                '</div>'
            }
            else {
                $html = '<div class="eventPopup">'+
                    '<div class="overlay"></div>'+
                    '<div class="edit">'+
                        '<form>'+
                            '<div class="h6">Event : <span class="title"></span><span class="cancel pull-right"><i class="fa fa-times"></i></span></div>'+
                            '<input type="hidden" class="id">'+
                            '<h5>Sure Delete this Event ?</h5>'+
                            '<span class="event-btn btn btn-xs bg-theme  pull-right delete"><i class="fa fa-check"></i></span>'+
                            '<span class="event-btn btn btn-xs bg-theme cancel"><i class="fa fa-times"></i></span>'+
                        '</form>'+
                    '</div>'+
                '</div>'
            }
            // adding popup
            var eventForm = $($html)
            $calendar.append(eventForm);
            $.material.init();
            return eventForm;
        };

        $(".preview").click(function(event) {
            event.preventDefault();

            if (!WEEKLEAVE.length) {
                return false;
            }

            $(".calendarContainer").removeClass('hidden');
            $calendar.remove();
            $(".calendarContainer").append('<div id="calendar"></div>');
            $calendar = $('#calendar');
            
            EVENTS = [];

            $calendar.fullCalendar({

                editable: true,
                selectable : true,
                eventLimit: false,
                dragScroll:false,
                droppable: false,
                firstDay : 0,

                header: {
                    left: '',
                    center: 'title',
                    right: ''
                },

                dayRender : function(date, cell) {
                    if (WEEKLEAVE && WEEKLEAVE.indexOf(date.day()) != -1) {
                        cell.css('backgroundColor', '#f5f5f5');
                    }
                },

                eventClick: function(event, jsEvent, view) {
                    var currentEvent = event;

                    if (event.editable == false || event.source.editable == false) return false;
                    else {
                        $eventPopup = createPopup('remove');

                        // assigning default values and id of event
                        $eventPopup.find('#id').val(currentEvent.id);
                        $eventPopup.find('.title').text(currentEvent.title);
                        
                        $eventPopup.find('.overlay').click(function() { $eventPopup.remove(); });
                        // close popup if cancel is fired
                        $eventPopup.find('.cancel').click(function() { $eventPopup.remove(); });
                        // delete event
                        $eventPopup.find('.delete').click(function() { 
                            $eventPopup.remove();
                            deleteHoliday(currentEvent.id);
                            $calendar.fullCalendar('removeEvents', currentEvent.id);
                        });
                    }
                },

                select : function(start, end) {
            
                    $eventPopup = createPopup('custom');

                    // assigning default values and id of event
                    var newEvent = new Object();
                    newEvent.id = Date.now();
                    newEvent.title = "";
                    newEvent.start = start;
                    newEvent.end = end;

                    $eventPopup.find(".add").click(function(e) {
                        if (!$eventPopup.find('.newTitle').val()) {
                            $eventPopup.find('.cancel').trigger('click');
                            return false;
                        }
                        newEvent.title = $eventPopup.find('.newTitle').val();
                        newEvent.backgroundColor = '#FC6558';
                        $calendar.fullCalendar('renderEvent', newEvent, true);
                        addHoliday(newEvent);
                        $eventPopup.remove();
                    });
                    // close event Popup
                    $eventPopup.find('.overlay').click(function() { $eventPopup.remove(); delete newEvent});
                    $eventPopup.find('.cancel').click(function() { $eventPopup.remove(); delete newEvent});
                },

                eventDrop: function(event, delta, revertFunc) {
                    currentEvent = event;
                    currentEvent.backgroundColor = '#FC6558';
                    updateEvent(currentEvent);
                },

                eventResize : function(event, jsEvent, ui, view) {
                    currentEvent = event;
                    currentEvent.backgroundColor = '#FC6558';
                    updateEvent(currentEvent);
                },

                windowResize: function(view) {
                    if ($(window).width() < 400){
                        $calendar.fullCalendar('option', 'aspectRatio', 0.75);
                    }
                },
            });

            payDay = new moment(new Date($('#payDay').val()));
            runPayroll = new moment(new Date($('#runPayroll').val()));

            if ($('#payDay').val()) repeatEvent(payDay, 'Pay Day');
            if ($('#runPayroll').val()) repeatEvent(runPayroll, 'Run Payroll');

            $calendar.fullCalendar( 'addEventSource', EVENTS );

            if (HOLIDAY.events.length) {
                $calendar.fullCalendar( 'addEventSource', HOLIDAY);
            }
        });

        function repeatEvent(event, title) {
            // 2 = march
            var date = event.date();
            var month = event.month();
            var year = event.year();
            var prevApril = 3;

            if (month <= 2){
                // [jan, feb, march]
                year--;
            }

            var localEvent = [];
            // remove added event
            for (var i = prevApril; i <= prevApril+11; i++) {

                var noOfDays = moment(new Date(year, i)).daysInMonth();
                var start = date > noOfDays ? new moment(new Date(year, i, noOfDays + 1)) : new moment(new Date(year, i, date+1));

                var newEvent = {
                    // check if assign date exist in that month or not
                    start : start.toJSON().slice(0, 10),
                    title : title,
                    id : Date.now(),
                    stick: true,
                    backgroundColor : '#A52A2A',
                    editable : false
                }
                localEvent.push(newEvent);
            }

            EVENTS.push.apply(EVENTS, localEvent);
        }

        // Responsive initialization
        if ($(window).width() < 400){
            $calendar.fullCalendar('option', 'aspectRatio', 0.75);
        }

        $("#next").click(function(event) {
            $("#calendar").fullCalendar('next');
        });
        $("#prev").click(function(event) {
            $("#calendar").fullCalendar('prev');
        });
        $("#today").click(function(event) {
            $("#calendar").fullCalendar('today');
        });
        $("#prevYear").click(function(event) {
            $("#calendar").fullCalendar('prevYear');
        });
        $("#nextYear").click(function(event) {
            $("#calendar").fullCalendar('nextYear');
        });


        $(".main .form-horizontal input").trigger('change');

    });


</script>
