<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>  
<?php $count = count($result); ?>
<?php $count2 = count($result2); ?>
<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
            <div class="main panel panel-default col-md-9 col-sm-9">
                <div class="panel-body" style="min-height:36em;">
                        <h2>Manage Employees</h2><hr>
                        <br> 
                        <div class="col-md-8 col-sm-8 col-sm-12">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><b>Employees <?php echo ((!empty($count))&&($count > 0)) ? '('.$count.')' : '';?> </b></h3>
                                </div>
                                <div class="panel-body table-responsive">
                                    <table class="table table-hover">
                                        <?php if (!empty($result)) { 
                                                foreach ($result as $key => $value) { ?>
                                        <tr>
                                            <td>
                                            <?php if(isset($value->gender)): ?>
                                                <i class="fa fa-<?php echo strtolower($value->gender); ?>"></i>
                                            <?php else: ?>
                                                <i class="fa fa-male"></i>/<i class="fa fa-female"></i>
                                            <?php endif; ?>
                                            </td>
                                            <td>
                                                <span class="h6"><?php echo $value->id;?></span>
                                            </td>
                                            <td><span class="h6"><?php echo $value->firstName . ' ' . $value->lastName; ?></span></td>
                                            <td class="text-right"><a href="/viewEmployee/<?php echo $value->id;?>"><i class="fa fa-list-alt ajaxify"> </i><span class="h6"> View Details</span></a></td>
                                        </tr>
                                        <?php } } else { ?>
                                        <tr>
                                            <td><i class="fa fa-male"></i>/<i class="fa fa-female"></i></td>
                                            <td><span class="h6">You don't have any employees till now.</span></td>
                                        </tr>
                                        <?php } ?>
                                    </table>

                                </div>
                            </div>
                        </div>  
                        
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="extra text-center notice">
                                <div class="box">
                                    <img src="images/Pin.png" class="boardPin" alt="pin">
                                    <br>
                                    <h4>idea to show you care</h4>
                                    <div><i class="fa fa-thumbs-o-up fa-4x"></i></div>
                                    <br>
                                    <h6>Surprise your team with little gifts such as movie tickets, homemade cookies, or a gift card to a local coffee shop.</h6>
                                </div>
                            </div>
                        </div> 

                        <div class="clearfix"></div>
                        <br>

                        <?php if (!empty($result2)): ?>
                        <div class="col-md-8 col-sm-8 col-sm-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading"> 
                                    <h3 class="panel-title"><b>Employees with Incomplete Details <?php echo ((!empty($count2))&&($count2 > 0)) ? '('.$count2.')' : '';?> </b></h3>
                                </div>
                                <div class="panel-body table-responsive">
                                    <table class="table table-hover" id="incompleteDetail">
                                        <?php foreach ($result2 as $key => $value): ?>
                                        <tr>
                                            <td>
                                            <?php if(isset($value->gender)): ?>
                                                <i class="fa fa-<?php echo strtolower($value->gender); ?>"></i>
                                            <?php else: ?>
                                                <i class="fa fa-male"></i>/<i class="fa fa-female"></i>
                                            <?php endif; ?>
                                            </td>
                                            <td>
                                                <span class="h6"><?php echo $value->id;?></span>
                                            </td>
                                            <td><span class="h6"><?php echo $value->firstName . ' ' . $value->lastName; ?></span></td>
                                            <td class="text-right">
                                            <a href="/viewEmployee/<?php echo $value->id;?>"><i class="fa fa-list-alt ajaxify"> </i><span class="h6"> Enter Details</span></a>
                                            <?php if (!empty($value->action)): ?>
                                            /
                                                <span class="delete text-muted" data-id="/<?php echo strtolower($value->action); ?>/<?php echo $value->id;?>">
                                                <i class="fa fa-list-alt ajaxify"> </i>
                                                <span class="h6"> <?php echo ucfirst(strtolower($value->action)) ?></span>
                                                </span>
                                            <?php endif ?>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </table>

                                </div>
                            </div>
                        </div> 
                        <?php endif ?>

                        <style>
                            .delete {
                                cursor: pointer;
                            }
                        </style>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <a href="/addEmployees_step1" class="pull-left ajaxify"> <i class="fa fa-plus"></i> Add New Employee</a>
                        </div>
                        <div class="clearfix"></div>
                        <br>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <a href="/terminate" class="pull-left ajaxify"> <i class="fa fa-times"></i> Terminate Employee</a>
                        </div>
                        <div class="clearfix"></div>
                        <br>
                    </div>    
                </div>

            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>

    function displayError() {
        swal({
            title : "Error !",
            text : 'Try Again!',
            type : 'error',
            confirmButtonClass : 'bg-theme',
            animation : false,
            width : 400
        });
    };

    function closeAlert() {
        $(".sweet-alert button").first().trigger('click');
    }

    $(function() {

        // add active class to corresponding link in menu 
        $(".nav li.active").removeClass('active');
        $(".nav.nav-stacked li").filter(function(index) {
            return $(this).data('url') == 'manageEmployees';
        }).addClass('active');

        $.material.init();

        $(".delete").click(function(event) {

            var $this = $(this);
            var url = $this.data('id');
            swal({
                title : 'Delete Employee',
                text : "Do you want to Delete employee?",
                type : 'info',
                confirmButtonClass : 'bg-theme',
                animation : false,
                showCancelButton: true,
                confirmButtonText: 'Yes',
                cancelButtonText: 'No',
                cancelButtonClass: 'bg-theme',
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function(isConfirm) 
            {
                if (isConfirm === true) 
                {
                    setTimeout(function(){
                        swal({
                            title : 'Deleting data',
                            html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                            allowOutsideClick : false,
                            showConfirmButton : false,
                            animation : false,
                            width : 300
                        });
                    
                    }, 200);

                    $.ajax({
                        url: location.href.split(location.pathname)[0]+url,
                        type: "POST",
                        data: {id: url.split('/').pop()},
                    })
                    .done(function(data) {
                        if (data.status == 200) {
                            swal({
                                title : "Deleted!",
                                // text : "Employee Deleted Successfully",
                                type : 'success',
                                showConfirmButton: false,
                                animation : false,
                                width : 300,
                                timer : 1500
                            }); 

                            if ($("#incompleteDetail tr").length == 1) {
                                $("#incompleteDetail").remove();
                                getAndInsert(location.pathname);
                            }
                            else {
                                $this.parents('tr').remove();
                            }
                        }
                        else if (data.status == 405) {
                            swal({
                                title : "Error !",
                                text : data.message,
                                type : 'error',
                                confirmButtonClass : 'bg-theme',
                                animation : false,
                                width : 300
                            }); 
                        }
                        else {
                            closeAlert();
                            displayError();
                        }
                    })
                    .fail(function() {
                        displayError();
                        console.log("request failed, try again");
                    })
                }
            })
        });
    })
</script>
