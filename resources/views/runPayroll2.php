<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>

<body  style="margin-top:2em;">
            <?php  require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <style>
                        .employee{
                            -webkit-box-shadow: none;
                            box-shadow: none;
                            position: relative;
                        }
                        .employee .panel-body > div::after,.employee .panel-body > div::before {
                            content: '';
                            position: absolute;
                            width: 15px;
                            height: 18px;
                            background: #9A8B8B;
                            top: -11px;
                        }
                        .employee .panel-body > div::before{
                            left: 0;
                            transform:skewY(40deg);
                        }
                        .employee .panel-body > div::after{
                            right: 0;
                            transform:skewY(-40deg);
                        }
                        .employee .panel-heading{
                            z-index: 10;
                            cursor: pointer;
                        }
                        .employee .panel-body {
                            position: relative;
                            padding-top: 0;
                            padding-bottom: 0;
                        }

                        .employee .panel-body > div{
                            background: #fff;
                            border: 1px solid #ddd;
                            box-shadow: 0 0 4px #ddd;
                            padding-top: 15px;
                            margin: 0 15px;
                        }

                    </style>
                    <div class="panel-body" style="min-height:36em;">

                        <div class="breadcrumb flat text-center row">
                            <span class="active col-md-4 col-sm-4 col-xs-12">Adjust Salary</span>
                            <span class="col-md-4 col-sm-4 col-xs-4 hidden-xs">Review </span>
                            <span class="col-md-4 col-sm-4 col-xs-4 hidden-xs">Pay</span>
                        </div>

                        <br>
                        <p class="clearfix">Please complete your payroll by 00:00 on <b><?php echo date('d M', strtotime($dates->runPayroll)) ?></b>. Your employees will be paid on <b><?php echo date('d M', strtotime($dates->payDay)) ?></b>.</p>

                        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                            <div class="col-md-2 col-sm-2 col-xs-3"><b>Id</b></div>
                            <div class="col-md-6 col-sm-6 col-xs-7"><b>Employee</b></div>
                            <div class="col-md-2 col-sm-2 col-xs-2"><b>Days Worked</b></div>
                            <div class="col-md-2 col-sm-2 col-xs-2 hidden-xs"><b>Gross Pay</b></div>
                        </div>
                        
                        <div class="clearfix"></div>
                        <br>
                            <!-- ======================  employee ============================= -->
                             <?php if (!empty($employees)) { 
                                                foreach ($employees as $key => $value) { ?>
                            <div class="employee small panel panel-info" data-id="<?php echo $value->userid?>">
                                <div class="panel-heading  text-center col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-2 col-sm-2 col-xs-3"><h4><?php echo $value->userid?></h4></h4></div>
                                    <div class="col-md-6 col-sm-6 col-xs-7"><h4><?php echo $value->firstName." ".$value->lastName?></h4></div>
                                    <div class="col-md-2 col-sm-2 col-xs-2 noOfDays"><h5><?php echo $value->noOfDays;?></h5></div>
                                    <div class="col-md-2 col-sm-2 hidden-xs"><h5 class="grossPayHead"><i class="fa fa-inr"></i> <span><?php echo $value->grossPay ?></span></h5><i class="fa fa-caret-down"></i></div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="panel-body col-md-12 col-sm-12 col-xs-12">
                                    <div class="drawer">
                                        <form class="form-horizontal" data-changed="untouched">
                                            <input type="hidden" class="userId" name="userId" value="<?php echo $value->userid;?>">
                                            <input type="hidden" class="monthlyPay" name="monthlyPay" value="<?php echo $value->monthlyPay;?>">
                                            <input type="hidden" class="oneDayPay" name="oneDayPay" value="<?php echo $value->oneDayPay;?>">
                                            <input type="hidden" class="otherFactorOfCTC" name="otherFactorOfCTC" value="<?php echo $value->sum;?>">
                                            <div class="clearfix"></div>
                                            <div class="col-md-3">
                                                <h5>CTC : <span><i class="fa fa-inr"></i> <span class="ctc"><?php echo $value->ctc;?></span></span></h5>
                                                <h5>Direct Deposits</h5>
                                                <h6>
                                                    <div class="form-group form-group-sm label-floating ">
                                                        <label class="control-label" for="note"><span><i class="fa fa-pencil"></i> Add Personal Note</span></label>
                                                        <input class="form-control note" name="note" type="text" style="cursor: auto;" placeholder="" value="<?php echo $value->note;?>" >
                                                    </div>
                                                </h6>
                                            </div>
                                            <div class="col-md-3">
                                                <h6>
                                                    <div class="form-group form-group-sm label-floating ">
                                                        <label class="control-label" for="noOfLeave"><span>No Of Leave</span></label>
                                                            <input class="form-control noOfLeave" name="noOfLeave" type="number" min="0" style="cursor: auto;" placeholder="" value="<?php echo $value->noOfLeave;?>">
                                                    </div>
                                                </h6>
                                                <h6>
                                                    <div class="form-group form-group-sm label-floating ">
                                                        <label class="control-label" for="overtime"><span><i class="fa fa-plus"></i> Overtime</span></label>
                                                        <input class="form-control overtime" name="overtime" type="number" min="0" style="cursor: auto;" placeholder="" value="<?php echo $value->overtime;?>">
                                                    </div>
                                                </h6>
                                            </div>
                                            <div class="col-md-3">
                                                <h6>
                                                    <div class="form-group form-group-sm label-floating ">
                                                        <label class="control-label" for="bonus"><span><i class="fa fa-plus"></i> Bonus</span></label>
                                                        <input class="form-control bonus" name="bonus" type="number" min="0" style="cursor: auto;" placeholder="" value="<?php echo $value->bonus;?>">
                                                    </div>
                                                </h6>
                                                <h6>
                                                    <div class="form-group form-group-sm label-floating ">
                                                        <label class="control-label" for="commission"><span><i class="fa fa-plus"></i> Commission</span></label>
                                                        <input class="form-control commission" name="commission" type="number" min="0" style="cursor: auto;" placeholder="" value="<?php echo $value->commission;?>" >
                                                    </div>
                                                </h6>
                                                <h6 >
                                                    <div class="form-group form-group-sm label-floating ">
                                                        <label class="control-label" for="otherEarning"><span><i class="fa fa-plus"></i> Other Earnings</span></label>
                                                        <input class="form-control otherEarning" name="otherEarning" type="number" min="0" style="cursor: auto;" placeholder="" value="<?php echo $value->otherEarning;?>" >
                                                    </div>
                                                </h6>
                                            </div>
                                            <div class="col-md-3">
                                                <h6>
                                                    <div class="form-group form-group-sm label-floating ">
                                                        <label class="control-label" for="grossPay"><span><i class="fa fa-inr"></i> Gross Pay</span></label>
                                                        <input class="form-control grossPay" name="grossPay" type="number" min="0" style="cursor: auto;" placeholder="" value="<?php echo $value->grossPay;?>" readonly>
                                                    </div>
                                                </h6>
                                                <h6>
                                                    <div class="form-group form-group-sm label-floating ">
                                                        <label class="control-label" for="reimbursement"><span><i class="fa fa-plus"></i> Reimbursement</span></label>
                                                        <input class="form-control reimbursement" name="reimbursement" type="number" min="0" style="cursor: auto;" placeholder="" value="<?php echo $value->reimbursement;?>" >
                                                    </div>
                                                </h6>
                                                <h6>
                                                    <div class="form-group form-group-sm label-floating ">
                                                        <label class="control-label" for="deduction"><span><i class="fa fa-minus"></i> Deduction</span></label>
                                                        <input class="form-control deduction" name="deduction" type="number" min="0" style="cursor: auto;" placeholder="" value="<?php echo $value->deduction;?>" >
                                                    </div>
                                                </h6>
                                            </div>
                                            <div class="clearfix"></div>
                                        </form>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                    <?php } } else { ?>
                                        You dont have any employee
                                  <?php } ?>
                        <!-- ============================================================= -->
                        <div class="clearfix"></div>
                        <br>
                        <br>
                        <!-- <ul class="pagination">
                            <li class="disabled"><a href="javascript:void(0)"><i class="fa fa-arrow-left"></i></a></li>
                            <li class="active"><a href="javascript:void(0)">1</a></li>
                            <li><a href="javascript:void(0)">2</a></li>
                            <li><a href="javascript:void(0)">3</a></li>
                            <li><a href="javascript:void(0)">4</a></li>
                            <li><a href="javascript:void(0)">5</a></li>
                            <li><a href="javascript:void(0)"><i class="fa fa-arrow-right"></i></a></li>
                        </ul> -->
                        <div class="clearfix"></div>
                        <br>
                        <br>
                        <span class="next btn btn-raised bg-theme">Next</span>
                        <br>
                        <br>
                    </div>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>
$(function() {
    
    // add active class to corresponding link in menu 
    $(".nav li.active").removeClass('active');
    $(".nav.nav-stacked li").filter(function(index) {
        return $(this).data('url') == 'runPayroll';
    }).addClass('active');

    $.material.init();

    function closeAlert() {
        $(".sweet-alert button").first().trigger('click');
    }

    var employee_target = $(".employee").first(); // last changed form
    var current_target = $(document);  // clicked target form or document
    var untouched = $(".form-horizontal").length;
    var updated = 0;

    var lastMonth = "<?php echo date('Y-m')?>".split("-");

    $(".employee .panel-body").hide();

    $(".employee .panel-heading").click(function(event) {
        $(this).siblings('.panel-body').slideToggle();
        var $arrow = $(this).find('i.fa').toggleClass('fa-caret-down fa-caret-up');
    });

    $(".noOfLeave").change(function(event) {
        event.preventDefault();
        // php has 1 index date and js has 0. 
        var noOfDays = new Date(lastMonth[0], lastMonth[1]-1, 0).getDate();     

        // noOfDays = DaysInMonth - noOfLeave
        var val = $(this).val()*1;
        $(this).val( Math.min( val, noOfDays));
        $(this).parents(".panel-body").siblings('.panel-heading').find('.noOfDays h5').text( Math.max(noOfDays - 1*$(this).val(), 0));
    });

    $(".employee .form-horizontal input").change(function(event) {
        $(this).parents(".form-horizontal").data('changed', 'touched');
        employee_target = $(event.target).parents('.employee') || current_target;
    });

    $(document).click(function(event) {
        current_target = $(event.target).parents('.employee');
        if (employee_target && (current_target.data('id') != employee_target.data('id')))
            sendData(employee_target, false);
    });

    function sendData($employee, pass) {
        var $form = $employee.find('.form-horizontal');
        if (pass || $form.data('changed') == 'touched') {
            $(".next").data('disabled', true);

            $employee.find('.panel-heading').css('background-color', '#cd3d3d');

            $.ajax({
                url: location.href,
                type: 'POST',
                data: $form.serialize(),
            })
            .done(function(data) {
                if (data) {
                    $employee.find('.grossPayHead span').text(data.grossPay);
                    $form.find('.grossPay').val(data.grossPay).trigger('change');
                    $form.find('.deduction').val(data.deduction).trigger('change');
                    $employee.find('.panel-heading').css('background-color', '#5cb85c');
                    $form.data('changed', 'updated');
                    employee_target = null;
                    $(".next").data('disabled', false);

                    updated = $(".form-horizontal").filter(function(index) {
                        return $(this).data('changed') == "updated";
                    }).length;

                    if (untouched == updated) {
                        closeAlert();
                        getAndInsert("/reviewPayroll");
                        history.pushState(null, null, "/reviewPayroll");
                    }
                }
            })
            .fail(function() {
                $(".next").data('disabled', false);
                console.log("error");
            });
        }
        else {
            $(".next").data('disabled', false);
        }
    }

    $(".next").click(function(event) {
        if ($(this).data('disabled')) {
            console.log("disabled");
            return false;
        }

        swal({
            title : 'Saving',
            html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
            allowOutsideClick : false,
            showConfirmButton : false,
            animation : false,
            width : 400
        });
        var $employee = $(".employee").filter(function(index) {
            return $(this).find('.form-horizontal').data('changed')=="untouched";
        });

        if (!$employee.length) {
            return false;
        }

        $employee.each(function(index, el) {
            employee_target = $(el);
            sendData($(el), true);
        });
    }).data('disabled', false);
});
</script>
