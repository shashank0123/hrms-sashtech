<!DOCTYPE html>
<html>
<head>
    <title>Security for Payroll, Statutory compliance</title>

    <meta name='description' content='Full service online payroll solutions. Direct deposit, unlimited payroll runs, and FORM16 with simple, transparent pricing. Sashtechs offers payroll management, online web payroll system, legal management, statutory compliance , right from consolidating HR/Finance, processing of payroll, payroll system and services ' /> 

    <meta name='keywords' content=' Payroll Security Procedures, Business & Entrepreneurship, Payroll entrepreneurs, Health insurance benefits entrepreneurs,Security Alerts, Data Security for Payroll System & Platform ' />

    <!--Import Google Icon Font-->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
    <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet"></link>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Material Design -->
    <link href="css/bootstrap-material-design.css" rel="stylesheet">
    <link href="css/ripples.min.css" rel="stylesheet">
    <link href="css/roundslider.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">

    <script src="/js/jquery-1.12.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ripples.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/roundslider.min.js"></script>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

</head>

<body>
<?php require_once("staticNavBar.php");?>

<!-- ================  Header  ===================== -->

<header class="parallax" style="background-image: url(images/security.jpg);background-position: center">
    <div class="panel panel-default" style="  ">
        <div class="panel-body">
            <div class="text-center">
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <h5>SECURITY</h5>
                <h2>Your peace of mind is our Top priority</h2>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
            </div>
        </div>
    </div>
</header>

<section style="background:#fff;margin-top: -20px">
    <div class="container-fluid">
        <div class="row">
            <div class="container">
                <br>
                <br>
                <div class="col-md-5 col-md-offset-1">
                    <h2>Encryption</h2>
                    <br>
                    <b class="h5 text-uppercase"><i class="fa fa-lock fa-2x"></i> &nbsp; &nbsp;256-BIT SSL ENCRYPTION</b>
                    <br>
                    <br>
                    <p>All information travelling between your browser to our servers is protected with 256-bit SSL encryption. The lock icon in your browser lets you verify that you aren’t talking to a phishing site impersonating Sashtechs and that your data is secure in transit. This is the same technology that banks use to keep your account information safe.</p>
                </div>

                <div class="col-md-5 col-md-offset-1">
                    <h2>Bank account security</h2>
                    <br>
                    <b class="h5 text-uppercase"><i class="fa fa-university fa-2x"></i> &nbsp; &nbsp;Strong encryption</b>
                    <br>
                    <br>
                    <p>Particularly sensitive information - credit card numbers, bank account information, and your payment gateway account details - are encrypted in our database using Advanced Encryption Standard (AES).</p>
                </div>

                <div class="clearfix"></div>
                <br>
                <br>

                <div class="col-md-5 col-md-offset-1">
                    <h2>Reliability</h2>
                    <br>
                    <b class="h5 text-uppercase"><i class="fa fa-database fa-2x"></i> &nbsp; &nbsp;Cloud servers & Backups</b>
                    <br>
                    <br>
                    <p>The Sashtechs infrastructure uses AWS with redundant storage and servers to keep the application and your data available in the case of hardware failure. Our system stores backups in multiple secure locations and is updated every few hours.</p>
                </div>

                <div class="col-md-5 col-md-offset-1">
                    <h2>Extra Safety Steps</h2>
                    <br>
                    <b class="h5 text-uppercase"><i class="fa fa-key fa-2x"></i> &nbsp; &nbsp;TWO-STEP AUTHENTICATION</b>
                    <br>
                    <br>
                    <p>We provide an extra layer of security to your account with two-step authentication. Once activated, we'll send a code to your email that you'll need to verify when you login. You can setup private keys to unlock the Sashtechs app on Mobile device.</p>
                </div>
            </div>
        </div>
        <br><hr><br>
    </div>
</section>

<div class="clearfix"> </div>

<section class="parallax" style="background-image: url(images/try.jpg);">
    <div class="panel panel-default" style="  ">
        <div class="panel-body">
            <div class="text-center">
                <br>
                <br>
                <br>
                <br>
                <h5><b>TRY Sashtechs</b></h5>
                <h3 class="text-uppercase">Start your one month free trial </h3>
                <br>

                <div class="panel text-left" id="panel-text-left">
                    <?php require 'demoForm.php'; ?>
                </div>
                <br>
                    <div class="clearfix"></div>
                    <br><br>
                    <br><br>
            </div>
        </div>
    </div>
</section>

<?php require_once('staticFooter.php');?>

</body>
</html>

<script>
    $(function() {
        $.material.init();
    });
</script>
