<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>

<?php $totalDays=cal_days_in_month(CAL_GREGORIAN, intval(date('m'))-1, date('Y')); $sum=0; $epf=0; $esi=0;?>
<body  style="margin-top:2em;">
            <?php require_once('Menu.php'); 
                    $tds=0;
                if (!empty($TDS)) { 
                                                foreach ($TDS as $key1 => $value1) {
                                                    $tds+=$value1->tdsDeducted;
                                                }
                                            }
             ?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">

                        <div class="breadcrumb flat text-center row">
                            <span class="col-md-4 col-sm-4 hidden-xs">Adjust Salary</span>
                            <span class="active col-md-4 col-sm-4 col-xs-12">Review </span>
                            <span class="col-md-4 col-sm-4 hidden-xs">Pay</span>
                        </div>
                        <h3 class="text-center">Review Payroll</h3>
                        <br>
                        <div class="table-responsive">
                            <table class="table table-hover table-striped">
                                <tr>
                                    <th>Id</th>
                                    <th>Employee</th>
                                    <th>Days Worked</th>
                                    <th>Gross Pay</th>
                                </tr>
                        <?php if (!empty($employees)) { 
                                                foreach ($employees as $key => $value) { ?>
                                <tr>
                                    <td><?php echo $value->userID;?></td>
                                    <td><?php echo $value->firstName." ".$value->firstName; ?></td>
                                    <td><?php echo $totalDays-$value->noOfLeave;?></td>
                                    <td><i class="fa fa-inr"></i> <?php echo $value->grossPay; $sum+=$value->grossPay; $epf += $value->epf; $esi += $value->esi;?></td>
                                </tr>
                                <?php } }?>
                                
                            </table>
                        </div>
                        <br>
                        <br>
                        <h4>Gross Total Income:- <span><?php echo $sum;?></span> </h4> 
                        <h4>EPF Deduction:- <span><?php echo round($epf, 2);?></h4>
                        <h4>ESI Deduction:- <span><?php echo round($esi, 2);?></h4>
                        <h4>TDS Deduction:- <span><?php echo $tds;?></h4>
                        <h4><b>Net Payment :- <?php echo round($sum-$epf-$esi-$value->grossPay, 2) ?></b></h4>

                        <a href="/generateBankData" class="btn bg-theme btn-raised"><i class="fa fa-share"></i> &nbsp; Pay Employees</a>
                    </div>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>
    $.material.init();
   
</script>
