<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>

<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">

                <div class="breadcrumb flat text-center row">
                    <span class="col-md-2 col-sm-2 col-xs-6 hidden-xs">Basics</span>
                    <span class="col-md-2 col-sm-2 col-xs-6 hidden-xs">Personal</span>
                    <span class="active col-md-3 col-sm-3 col-xs-12">Family</span>
                    <span class="col-md-2 col-sm-2 col-xs-6 hidden-xs">Tax</span>
                    <span class="col-md-3 col-sm-3 col-xs-6 hidden-xs">Payment</span>
                </div>
                    
                <br>
                <br>

                <div class="col-md-12">
                    <div class="table-responsive">
                            <legend>Family Details</legend>
                            <?php if (empty($family)) : ?>
                                <p class="clearifx">There are no family members for this employee yet.</p>
                            <?php else: ?>
                            <table class="table">
                                <fieldset>
                                    <tbody>
                                        <tr>
                                            <th>Name</th>
                                            <th>Relationship</th>
                                            <th>D.O.B</th>
                                            <th>Address</th>
                                            <th>Disabled</th>
                                            <th>Edit</th> 
                                            <th>Delete</th>
                                        </tr>
                                        <?php foreach ($family as $key => $value) :?>
                                        <tr>
                                            <td class="name"><?php echo $value->name; ?></td>
                                            <td class="relation"><?php echo $value->relation; ?></td>
                                            <td class="dob"><?php echo $value->dob; ?></td>
                                            <td class="address"><?php echo $value->address; ?></td>
                                            <td class="disabled"><?php echo ($value->disabled == 'on') ? 'Yes' : 'No'; ?></td>
                                            <td><span class="btn btn-sm edit" data-id="<?php echo $value->id; ?>"><i class="fa fa-pencil"></i></span></td>
                                            <td><span class="btn btn-sm delete" data-id="<?php echo $value->id; ?>"><i class="fa fa-times"></i></span></td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </fieldset>
                            </table>
                            <?php endif; ?>
                    </div>

                    <div class="clearfix"></div>
                    <a href="/addEmployees_step3" class="btn bg-theme btn-raised ajaxify pull-right">next</a>
                    <span id="add" class="pull-right btn btn-default"> <i class="fa fa-plus"></i> Add member</span>
                </div>

                <form id="familyForm" class="form-horizontal col-md-12 col-sm-12 col-xs-12 hidden">
                    <fieldset>
                        <legend>Enter your Family Details</legend>

                        <input type="hidden" name="id" id="id">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="col-md-8 col-sm-12 col-xs-12">
                                <div class="form-group label-floating is-empty ">
                                    <label class="control-label" for="name"><span>Name</span></label>
                                    <input class="form-control" id="name" name="name" type="text" style="cursor: auto;">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="col-md-8 col-sm-12 col-xs-12">
                                <div class="form-group label-floating is-empty ">
                                    <label class="control-label" for="relation"><span>Relation</span></label>
                                    <input class="form-control" id="relation" name="relation" type="text" style="cursor: auto;">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="col-md-8 col-sm-12 col-xs-12">
                                <div class="form-group label-floating">
                                    <label for="dob" class="control-label">D.O.B</label>
                                    <input type="text" name="dob" id="dob" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="col-md-8 col-sm-12 col-xs-12">
                                <div class="form-group label-floating">
                                    <label for="address" class="control-label">Address</label>
                                    <textarea class="form-control" name="address" rows="2" id="address"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="col-md-8 col-sm-12 col-xs-12">
                                <div class="togglebutton form-group">
                                    <label>Disabled 
                                        &nbsp;&nbsp;&nbsp; 
                                        <input type="checkbox" id="disabled" name="disabled">
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="col-md-8 col-sm-8 col-xs-8 ">
                                    <input type="reset" class="btn btn-default btn-raised cancel" value="Cancel">
                                    <input type="submit" class="next btn bg-theme next btn-raised" value="Save">
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>


                    <style>
                        .edit , .delete {
                            margin: 0;
                        }
                    </style>

                    </div>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>
    function displayError() {
        swal({
            // title : "Error !",
            title : 'Try Again!',
            type : 'error',
            confirmButtonClass : 'bg-theme',
            animation : false,
            width : 300
        });
    };

    function closeAlert() {
        $(".sweet-alert button").first().trigger('click');
    }

    $(function() {
        $.material.init();

        $('#dob').bootstrapMaterialDatePicker({ weekStart : 0, time: false }).on('dateSelected', function(event) {
            $('#'+$(this).data('dtp')).find('.dtp-btn-ok').trigger('click');
        });

        $("#dob").bootstrapMaterialDatePicker('setDate', moment(new Date(1990, 0,1)));

        $("#add").click(function(event) {
            $("#familyForm").find('.cancel').trigger('click');
            $("#familyForm").removeClass('hidden');
        });

        $(".cancel").click(function(event) {
            $("#familyForm .form-group.label-floating").addClass('is-empty');
            $("#familyForm").addClass('hidden');
        });

        $("#familyForm").submit(function(event) {
            event.preventDefault();

            // remove all error messages
            $(".help-block").remove();
            $(".has-error").removeClass('has-error');
            
            swal({
                title : 'Saving',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 300
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: $('.main form').serialize(), 
            })
            .done(function(data) {
                if (data.status == 200) {
                    swal({
                        title : "Saved!",
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        showConfirmButton : false,
                        animation : false,
                        width : 300,
                        timer: 1500
                    });
                    if (!$(".main .table").length) {
                        getAndInsert(location.pathname);
                        return false;
                    }

                    if (!$("#familyForm").find('#id').val())
                        pushToTable(data.id);
                    else
                        updateTable(data.id);
                    
                    $("#familyForm").find('#id').val("");
                    $("#familyForm").addClass('hidden');
                }
                else{  // Validation Error
                    displayError();
                    // show Errors
                    for(var fields in data) {
                        $('#'+fields).parents('.form-group').addClass('has-error');
                        $('#'+fields).parent().append('<p class="help-block">'+ data[fields][0] +'</p>');
                    }
                }
            })
            .error(function() {
                displayError();
            });
        });

        function pushToTable(id) {
            var $row = $('<tr>'+
                '<td class="name">'+$('#name').val()+'</td>'+
                '<td class="relation">'+$('#relation').val()+'</td>'+
                '<td class="dob">'+$('#dob').val()+'</td>'+
                '<td class="address">'+$('#address').val()+'</td>'+
                '<td class="disabled">'+($('#disabled').is(':checked') ? 'Yes' : 'No' )+'</td>'+
                '<td><a class="btn btn-sm edit" data-id="'+id+'"><i class="fa fa-pencil"></i></a></td>'+
                '<td><a class="btn btn-sm delete" data-id="'+id+'"><i class="fa fa-times"></i></a></td>'+
            '</tr>');

            $(".main .table").append($row);
            $row.find('.edit').click(handleEdit);
            $row.find('.delete').click(handleDelete);
            var $familyForm = $("#familyForm");
            $familyForm.addClass('hidden'); // first hide the form then clear it
            $familyForm.find('.cancel').trigger('click'); // clear all field
        }

        function updateTable(id) {
            var $row = $(".main .table tr").filter(function(index) {
                return $(this).find('.edit').data('id') == id;
            });

            var $form = $("#familyForm");

            $row.find('.name').text($form.find('#name').val());
            $row.find('.relation').text($form.find('#relation').val());
            $row.find('.dob').text($form.find('#dob').val());
            $row.find('.address').text($form.find('#address').val());
            $row.find('.disabled').text(($form.find('#disabled').is(':checked') ? 'Yes' : 'No'));
        }

        $(".edit").click(handleEdit);

        function handleEdit() {
            event.preventDefault();

            $("#familyForm .cancel").trigger('click');

            swal({
                title : 'Fetching data',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 300
            });

            var id = $(this).data('id');
            $.get(location.href+'/'+id, function(data) {
                for(var fields in data) {
                    var el = $('#'+fields);
                    var parent = el.parents(".form-group");
                    // if data fill the fields
                    if (fields == 'disabled') {
                        el.prop('checked', data[fields] == 'on');
                        continue;
                    }
                    if (data[fields]){
                        el.val(data[fields]);
                        parent.removeClass('is-empty');
                    }
                    else{
                        el.val('');
                        parent.addClass('is-empty');
                    }
                }

                $('#familyForm #id').val(id);
                $('#familyForm').removeClass('hidden');

                closeAlert();
            }).error(function() {
                displayError()
            });
        };

        $(".delete").click(handleDelete);

        function handleDelete() {
            event.preventDefault();

            $("#familyForm .cancel").trigger('click');

            var $familyForm = $("#familyForm");
            $familyForm.addClass('hidden'); // first hide the form then delete

            swal({
                title : 'Deleting data',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 300
            });

            var id = $(this).data('id');
            var $row = $(this).parents('tr');
            $.post(location.href + '/' + id, {id: id}, function(data, textStatus, xhr) {
                if (data.status == 200) {
                    $row.remove();
                    swal({
                        title : "Deleted!",
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        showConfirmButton : false,
                        animation : false,
                        width : 300,
                        timer: 1500
                    });
                }
                else if (data.status == 405) {
                    swal({
                        title : "Not allowed!",
                        type : 'error',
                        confirmButtonClass : 'bg-theme',
                        showConfirmButton : false,
                        animation : false,
                        width : 300,
                        timer: 1500
                    });
                }
                else {
                    displayError();
                }
            });
        };
    })
</script>
