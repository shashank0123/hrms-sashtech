<!DOCTYPE html>
<html>
<head>
     <title>Pricing for Payroll, Compliance & Health Insurance benefits</title>

    <meta name='description' content='Sashtechs Pricing for HR software. Free trial and free signup for human resource system and payroll system in India. Payroll software pricing in India, Health Insurance benefits, Compare plans for Health insurance, Free Employee Attendance, Free Leaves and Custom Payroll Management Software' />

    <meta name='keywords' content=' Free Payroll India, online web payroll system, Cheap payroll and benefits, Employers payroll free, Employee Payroll free, best payroll management services, payroll management, price payroll software, price payroll, Cost to run payroll, Outsource paycheck, payslips, legal management, statutory compliance' />

    <!--Import Google Icon Font-->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
    <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet"></link>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Material Design -->
    <link href="css/bootstrap-material-design.css" rel="stylesheet">
    <link href="css/ripples.min.css" rel="stylesheet">
    <link href="css/roundslider.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">

    <script src="/js/jquery-1.12.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ripples.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/roundslider.min.js"></script>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    <style>
        .rs-handle  {
            background-color: #fff;
            border: 1px solid #000;
        }
        .rs-tooltip-text{
            font-size: 20px;
            font-family: FontAwesome;
        }
        .rs-control .rs-range-color {
            background-color: #fc6558;
        }
        .rs-control .rs-path-color {
            background-color: #ffffff;
        }
        .rs-control .rs-handle:after {
            background-color: #fc6558;
        }
        .rs-control .rs-bg-color {
            background-color: #ffffff;
        }
        
        .question{
            padding: 20px 15px;
            cursor: pointer;
            font-weight: 400;
        }
        .answer{
            padding: 15px;
        }
        .faq-item{
            font-family: FontAwesome;
            color: #555;
            border-bottom: 1px solid #ddd;
        }
    </style>
</head>

<body>
<?php require_once("staticNavBar.php");?>

<!-- ================  Header  ===================== -->

<header class="parallax" style="background-image: url(images/pricing.jpg);">
    <div class="panel panel-default" style="  ">
        <div class="panel-body">
            <div class="text-center">
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <h5>PRICING</h5>
                <h3>SUPER AFFORDABLE PRICING, NO SURPRISES.</h3>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <h6><b>Select any Option </b></h6><i class="fa fa-arrow-down"></i>
                <br>
                <br>
            </div>
        </div>
    </div>
</header>

<section id="pricingOption">
    <div class="container hidden-xs">
        <div class="col-md-9 col-md-offset-3 col-sm-11 col-sm-offset-1 text-center">
            <div class="btn-toolbar">
                <div class="btn-group">
                    <a href="optionA" class="btn"><h6>OPTION A </h6> Payroll</a>
                    <a href="optionB" class="btn"><h6>OPTION B </h6>Payroll&nbsp;+&nbsp;Benefits </a>
                    <a href="optionC" class="btn"><h6>OPTION C </h6> Payroll&nbsp;+&nbsp;Benefits&nbsp;+&nbsp;Manager</a>
                </div>
            </div>  
        </div>
    </div>
    <div class="text-center visible-xs-block">
        <a href="optionA" class="btn btn-block"><h6>OPTION A </h6> Payroll</a>
        <a href="optionB" class="btn btn-block"><h6>OPTION B </h6>Payroll&nbsp;+&nbsp;Benefits </a>
        <a href="optionC" class="btn btn-block"><h6>OPTION C </h6> Payroll&nbsp;+&nbsp;Benefits&nbsp;+&nbsp;Manager</a>
    </div>
</section>

<form method="POST" action="#" class="container">
    
    <section id="optionA" class="meter">
        <div class="panel panel-default">
            <div class="panel-body" style="padding:0 15px;">
                <div class="row">
                    <div class="col-md-8 col-sm-12 col-xs-12" style="padding-top:2em;">
                        <h4>Payroll</h4>
                        <h3 class="text-center">How many employees do you have?</h3>
                        <br>
                        
                        <div class="col-md-5 col-sm-3 col-xs-12"><div id="optionASlider" class="center-block"></div><div class="clearfix"></div></div>
                        <div class="col-md-3 col-sm-4 col-xs-12 text-center">
                            <br><br>
                            <span><i class="fa fa-inr"></i> </span>
                            <span class="h1 total">
                                124 
                            </span>
                            <br>
                            <span class="h6">PER MONTH</span> 
                        </div>
                        <div class="col-md-4 col-sm-5 col-xs-12 text-center">
                            <br><br>
                            <span class="h3">Here&rsquo;s the Math</span>
                            <br><br>
                            <span class="h5 noOfEmployee">1 employee at <i class="fa fa-inr"></i>  25</span>
                            <br>
                        </div>
                        <div class="clearfix"></div>
                        <br><br>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12 bg-primary" style="height: 24em;padding:1em 10px;">
                        <h3 class="text-center">General Pricing</h3>
                        <br>
                        <table class="table">
                            <tr>
                                <th>ITEM</th>
                                <th>PRICE PER MONTH</th>
                            </tr>
                            <tr>
                                <td>Base Fee</td>
                                <td><i class="fa fa-inr"></i>  99</td>
                            </tr>
                            <tr>
                                <td>Employees</td>
                                <td><i class="fa fa-inr"></i>  25 per Employee</td>
                            </tr>
                            <tr>
                                <td>500+ Employees</td>
                                <td>Contact us for enterprise pricing</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="clearfix"></div>
    <div class="col-md-7 col-sm-12 col-xs-12 hidden-xs"><h6>With Sashtechs there are no set-up fees, and no hidden charges. All the payroll features are included in the pricing you see here.</h6></div>
    <div class="col-md-3 col-md-offset-1 col-sm-6 col-sm-offset-3 col-xs-8 col-xs-offset-2"><button class="btn btn-block btn-raised bg-theme">SIGN UP <i class="fa fa-angle-double-right"></i></button></div>
    <div class="clearfix"></div>
</form>

<br>
<br>

<form method="POST" action="#" class="container">
    
    <section id="optionB" class="meter">
        <div class="panel panel-default">
            <div class="panel-body" style="padding:0 15px;">
                <div class="row">
                    <div class="col-md-8 col-sm-12 col-xs-12" style="padding-top:2em;">
                        <h4>Payroll + Benefits</h4>
                        <h3 class="text-center">How many employees do you have?</h3>
                        <br>
                        
                        <div class="col-md-5 col-sm-3 col-xs-12"><div id="optionBSlider" class="center-block"></div><div class="clearfix"></div></div>
                        <div class="col-md-3 col-sm-4 col-xs-12 text-center">
                            <br><br>
                            <span><i class="fa fa-inr"></i> </span>
                            <span class="h1 total">
                                  0
                            </span>
                            <br>
                            <span class="h6">PER MONTH</span> 
                        </div>
                        <div class="col-md-4 col-sm-5 col-xs-12 text-center">
                            <br><br>
                            <span class="h3">Here&rsquo;s the Math</span>
                            <br><br>
                            <span class="h5 noOfEmployee">1 employee at <i class="fa fa-inr"></i>  0</span>
                            <br>
                        </div>
                        <div class="clearfix"></div>
                        <br><br>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12 bg-primary" style="height: 24em;padding:1em 10px;">
                        <h3 class="text-center">General Pricing</h3>
                        <br>
                        <table class="table">
                            <tr>
                                <th>ITEM</th>
                                <th>PRICE PER MONTH</th>
                            </tr>
                            <tr>
                                <td>Base Fee</td>
                                <td><i class="fa fa-inr"></i>  0</td>
                            </tr>
                            <tr>
                                <td>Employees</td>
                                <td><i class="fa fa-inr"></i>  0 per Employee</td>
                            </tr>
                            <tr>
                                <td>500+ Employees</td>
                                <td>Contact us for enterprise pricing</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="clearfix"></div>
    <div class="col-md-7 col-sm-12 col-xs-12 hidden-xs"><h6>With Sashtechs there are no set-up fees, and no hidden charges. All the payroll features are included in the pricing you see here.</h6></div>
    <div class="col-md-3 col-md-offset-1 col-sm-6 col-sm-offset-3 col-xs-8 col-xs-offset-2"><button class="btn btn-block btn-raised bg-theme">SIGN UP <i class="fa fa-angle-double-right"></i></button></div>
    <div class="clearfix"></div>
</form>

<br>
<br>

<form method="POST" action="#" class="container">
    
    <section id="optionC" class="meter">
        <div class="panel panel-default">
            <div class="panel-body" style="padding:0 15px;">
                <div class="row">
                    <div class="col-md-8 col-sm-12 col-xs-12" style="padding-top:2em;">
                        <h4>Payroll + Benefits + Manager</h4>
                        <h3 class="text-center">How many employees do you have?</h3>
                        <br>
                        
                        <div class="col-md-5 col-sm-3 col-xs-12"><div id="optionCSlider" class="center-block"></div><div class="clearfix"></div></div>
                        <div class="col-md-3 col-sm-4 col-xs-12 text-center">
                            <br><br>
                            <span><i class="fa fa-inr"></i> </span>
                            <span class="h1 total">
                                  3025
                            </span>
                            <br>
                            <span class="h6">PER MONTH</span> 
                        </div>
                        <div class="col-md-4 col-sm-5 col-xs-12 text-center">
                            <br><br>
                            <span class="h3">Here&rsquo;s the Math</span>
                            <br><br>
                            <span class="h5 noOfEmployee">1 employee at <i class="fa fa-inr"></i>  25</span>
                            <br>
                        </div>
                        <div class="clearfix"></div>
                        <br><br>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12 bg-primary" style="height: 24em;padding:1em 10px;">
                        <h3 class="text-center">General Pricing</h3>
                        <br>
                        <table class="table">
                            <tr>
                                <th>ITEM</th>
                                <th>PRICE PER MONTH</th>
                            </tr>
                            <tr>
                                <td>Manager Fee</td>
                                <td><i class="fa fa-inr"></i>  3000 per 100 employee</td>
                            </tr>
                            <tr>
                                <td>Employees</td>
                                <td><i class="fa fa-inr"></i>  25 per Employee</td>
                            </tr>
                            <tr>
                                <td>500+ Employees</td>
                                <td>Contact us for enterprise pricing</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="clearfix"></div>
    <div class="col-md-7 col-sm-12 col-xs-12 hidden-xs"><h6>With Sashtechs there are no set-up fees, and no hidden charges. All the payroll features are included in the pricing you see here.</h6></div>
    <div class="col-md-3 col-md-offset-1 col-sm-6 col-sm-offset-3 col-xs-8 col-xs-offset-2"><button class="btn btn-block btn-raised bg-theme">SIGN UP <i class="fa fa-angle-double-right"></i></button></div>
    <div class="clearfix"></div>
</form>

<br><br>

<section style="background:#fff;">
    <div class="container-fluid">
        <div class="row">
            <div class="container">
                <h3 class="text-center">Payroll made for real businesses.</h3>
                <br>
                <div class="col-md-10 col-md-offset-1">
                    <div class="col-md-4 col-sm-6 col-sm-12"><h6><i class="fa fa-check"></i> Fast, simple, online setup</h6> </div>
                    <div class="col-md-4 col-sm-6 col-sm-12"><h6><i class="fa fa-check"></i> Free email, phone and chat support  </h6></div>
                    <div class="col-md-4 col-sm-6 col-sm-12"><h6><i class="fa fa-check"></i> Seamless integration with sashtechs to file your IT returns </h6></div>
                    <div class="col-md-4 col-sm-6 col-sm-12"><h6><i class="fa fa-check"></i> Streamlined Year-end Filing </h6></div>
                    <div class="col-md-4 col-sm-6 col-sm-12"><h6><i class="fa fa-check"></i> Easy to understand tax liabilities tracking </h6></div>
                    <div class="col-md-4 col-sm-6 col-sm-12"><h6><i class="fa fa-check"></i> Employee self serve portal for pay-stubs and Form-16 </h6></div>
                    <div class="col-md-4 col-sm-6 col-sm-12"><h6><i class="fa fa-check"></i> Payroll reminders in your inbox </h6></div>
                    <div class="col-md-4 col-sm-6 col-sm-12"><h6><i class="fa fa-check"></i>  Direct deposit to multiple accounts </h6></div>
                    <div class="col-md-4 col-sm-6 col-sm-12"><h6><i class="fa fa-check"></i> 256-bit SSL encryption, and automatic backup </h6></div>
                </div>
            </div>
        </div>
            <br><hr><br>
    </div>
</section>

<div class="clearfix"></div>


<section style="background:#fff;">
    <div class="container">
        <h2><center>Frequently Asked Questions</center></h2>
        <br>

        <div class="col-md-8 col-md-offset-2">
                <div class="faq-item">
                    <div class="question">
                        <span class="h4">Can I try Sashtechs for free? </span><i class="fa fa-chevron-down pull-right"></i>
                    </div>
                    <div class="answer">
                        <p>You can try any product or combination of products free for 1 Month. You can buy, upgrade, or cancel at any time. Using the Sashtechs platform you can run seamless payroll and benefits.</p>
                    </div>
                </div>
        </div>

        <div class="col-md-8 col-md-offset-2">
                <div class="faq-item">
                    <div class="question">
                        <span class="h4">Is my customer data safe with Sashtechs?</span><i class="fa fa-chevron-down pull-right"></i>
                    </div>
                    <div class="answer">
                        <p>Keeping our customers’ data secure is the most important thing Sashtechs does. We go to considerable lengths to ensure that all data sent to Sashtechs is handled securely. See here for more information including our Terms of Use and Privacy Policy.</p>
                    </div>
                </div>
        </div>

        <div class="col-md-8 col-md-offset-2">
                <div class="faq-item">
                    <div class="question">
                        <span class="h4">How is pricing calculated when my ‘Employee’ count changes?</span><i class="fa fa-chevron-down pull-right"></i>
                    </div>
                    <div class="answer">
                        <p>Your price can increase or decrease based on the total number of active ‘Employee’ you have on platform. Drag the dial above to see exact price per ‘Employee’ for your plan.</p>
                    </div>
                </div>
        </div>

        <div class="col-md-8 col-md-offset-2">
                <div class="faq-item">
                    <div class="question">
                        <span class="h4">What’s the difference between Payroll, Payroll + Benefits and  Payroll + Benefits + Manager?</span><i class="fa fa-chevron-down pull-right"></i>
                    </div>
                    <div class="answer">
                        <p>We offer three plans right now: <br>   <br>
                            <b>Payroll </b>- It is a self driven Payroll system where you manage your employee details and run them on auto pilot mode or manually. <br><br>
                            <b>Payroll + Benefits </b>- It is also a  self driven Payroll system where you manage your employee details and run them on auto pilot mode or manually with extra feature of benefits. Where you can offer different health plans to your employees. <br><br>
                            <b>Payroll + Benefits + Manager </b>- Our dedicated manager will run and maintain the payroll and benefits for your company. He will take care of all the needs from adding employee, running payroll etc. <br>
                        </p>
                    </div>
                </div>
        </div>

    </div>
</section>

<div class="clearfix"> </div>

<section class="parallax" style="background-image: url(images/try.jpg);">
    <div class="panel panel-default" style="  ">
        <div class="panel-body">
            <div class="text-center">
                <br>
                <br>
                <br>
                <br>
                <h5><b>TRY Sashtechs</b></h5>
                <h3 class="text-uppercase">Start your one month free trial </h3>
                <br>

                <div class="panel text-left" id="panel-text-left">
                    <?php require 'demoForm.php'; ?>
                </div>
                <br>
                    <div class="clearfix"></div>
                    <br><br>
                    <br><br>
            </div>
        </div>
    </div>
</section>

<?php require_once('staticFooter.php');?>

</body>
</html>

<script>
    $.material.init();

    $(function() {

        $("#pricingOption a").click(function(event) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: $('#'+$(this).attr('href')).offset().top
            }, 500);
        });

        $("#optionASlider, #optionBSlider, #optionCSlider").each(function(index, el) {

            var $global = $(this).parentsUntil(".meter").last().parent();
            
            $(el).roundSlider({
                radius: 80,
                width: 20,
                handleShape: "square",
                sliderType: "min-range",
                circleShape: "pie",
                startAngle: 315,
                value: 1,
                max: 500,
                min: 1,
                mouseScrollAction: true,

                baseRate : 0,
                employeeRate : 25,
                hasManager : false,
                managerRate : 3000,

                drag: function (args) {
                    handleInput.call($($global), args, $(el).roundSlider("option"));
                },
                change: function (args) {
                    handleInput.call($($global), args, $(el).roundSlider("option"));
                }
            });
        });

        function handleInput (args, slider) {
            var noOfEmployee = args.value;

            var employeeRate = slider.options.employeeRate;
            var managerRate = slider.options.managerRate;
            var hasManager = slider.options.hasManager;
            var noOfManager = 0;

            if (hasManager) {
                noOfManager = Math.floor(noOfEmployee / 101) + 1;
            }
            var baseRate = slider.options.baseRate;

            var total = baseRate + employeeRate * noOfEmployee + noOfManager * managerRate;

            $(this).find(".noOfEmployee").html(' ' + noOfEmployee +  ' employee at <i class="fa fa-inr"></i> ' + employeeRate);

            $(this).find(".total").text(total);

            return true;
        }

        $("#optionASlider").roundSlider("option", "baseRate", 99 );
        $("#optionBSlider").roundSlider("option", "baseRate", 0 );
        $("#optionBSlider").roundSlider("option", "employeeRate", 0 );
        $("#optionCSlider").roundSlider("option", "baseRate", 0 );
        $("#optionCSlider").roundSlider("option", "hasManager", true );

        $(".faq-item .question").on('click', function(event) {
            $(this).siblings('.answer').slideToggle(400);
            if ($(this).children('i').hasClass('fa-chevron-down')) {
                $(this).children('.fa-chevron-down').removeClass('fa-chevron-down').addClass('fa-chevron-up');
            }
            else
                $(this).children('.fa-chevron-up').removeClass('fa-chevron-up').addClass('fa-chevron-down');

        });

        $(".answer").hide();

    });
</script>
