<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>


<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">
                        <center>

                    <h3>Welcome To Sashtechs, <?php echo Session::get('employerName');?></h3><br>
                    You're just a few steps away from running your first payroll. Need Help? email us at <a href="mailto:support@Sashtechs.com">support@Sashtechs.com</a>
                </center> 
                <br>
                <a href="/addCompanyLocation" class="ajaxify panel panel-<?php if (session()->get('step1')=="on") echo "success"; else echo "default";?> col-md-10 col-md-offset-1 col-sm-12 col-xs-12 col-sm-12 col-xs-12" >
                    <div class="row panel-heading dashboard-list">
                        <div class="col-md-2 col-sm-2" >
                            <i class="fa fa-building-o fa-3x" style="padding-top:16px;padding-left: 13px;"></i>
                        </div>
                        <div class="col-md-10 col-sm-10"> 
                            <h5>Step 1 </h5>
                            <h4 class="small"> Add Company Locations/ Addresses </h4>
                            <h5 class="small">Let us know your company's primary address, mailing address and additional work location</h5>
                        </div><div class="clearfix"></div>
                    </div>
                </a>

                <a href="/addEmployees_step1" class="ajaxify panel panel-<?php if (session()->get('step2')=="on") echo "success"; else echo "default";?> col-md-10 col-md-offset-1 col-sm-12 col-xs-12" >
                    <div class="row panel-heading dashboard-list">
                        <div class="col-md-2 col-sm-2" >
                            <i class="fa fa-user-plus fa-3x" style="padding-top:16px;padding-left: 13px;"></i>
                        </div>
                        <div class="col-md-10 col-sm-10"> 
                            <h5>Step 2 </h5>
                            <h4 class="small">Add Employees/Contractors</h4>
                            <h5 class="small">Add CTC, Title, Team & Personal details of the Employees/Contractors</h5>
                        </div><div class="clearfix"></div>
                    </div>
                </a>

                <a href="/salarySetup" class="ajaxify panel panel-<?php if (session()->get('step3')=="on") echo "success"; else echo "default";?> col-md-10 col-md-offset-1 col-sm-12 col-xs-12" >
                    <div class="row panel-heading dashboard-list">
                        <div class="col-md-2 col-sm-2" >
                            <i class="fa fa-pencil-square-o fa-3x" style="padding-top:16px;padding-left: 13px;"></i>
                        </div>
                        <div class="col-md-10 col-sm-10"> 
                            <h5>Step 3 </h5>
                            <h4 class="small">Set up Salary details</h4>
                            <h5 class="small">Setup CTC and Configure salary components which can be applied on employees</h5>
                        </div><div class="clearfix"></div>
                    </div>
                </a>

                <a href="<?php 
                        if ((session()->get('nextStep') == 'step4')or(!(strtolower(session()->get('step4')) == 'off'))) 
                        {
                            echo "/bankSetup";
                        }
                        else 
                        {
                            echo "";
                        }
                        ?>" class="ajaxify panel panel-<?php 
                        if (session()->get('step4')=="on") 
                        {
                            echo "success"; 
                        }
                        elseif (strtolower(session()->get('nextStep')) == 'step4') 
                        {
                            echo "default";
                        }
                        elseif((strtolower(session()->get('step4')) == 'off' )) 
                        {
                            echo 'off'; 
                        }
                        else 
                        {
                            echo "default";
                        }

                        ?> col-md-10 col-md-offset-1 col-sm-12 col-xs-12" >
                    <div class="row panel-heading dashboard-list">
                        <div class="col-md-2 col-sm-2" >
                            <i class="fa fa-university fa-3x" style="padding-top:16px;padding-left: 13px;"></i>
                        </div>
                        <div class="col-md-10 col-sm-10"> 
                            <h5>Step 4 </h5>
                            <h4 class="small">Set up Bank Details</h4>
                            <h5 class="small">Add Bank details of your company, so we can seamlessly credit salaries to your employees</h5>
                        </div><div class="clearfix"></div>
                    </div>
                </a>

                <a href="<?php 
                        if ((session()->get('nextStep') == 'step5')or(!(strtolower(session()->get('step5')) == 'off'))) 
                        {
                            echo "/taxDetail";
                        }
                        else 
                        {
                            echo "";
                        }
                        ?> " class="ajaxify panel panel-<?php 
                        if (session()->get('step5')=="on") 
                        {
                            echo "success"; 
                        }
                        elseif (strtolower(session()->get('nextStep')) == 'step5') 
                        {
                            echo "default";
                        }
                        elseif((strtolower(session()->get('step5')) == 'off' )) 
                        {
                            echo 'off'; 
                        }
                        else 
                        {
                            echo "default";
                        }

                        ?> col-md-10 col-md-offset-1 col-sm-12 col-xs-12" >
                    <div class="row panel-heading dashboard-list">
                        <div class="col-md-2 col-sm-2" >
                            <i class="fa fa-calculator fa-3x" style="padding-top:16px;padding-left: 13px;"></i>
                        </div>
                        <div class="col-md-10 col-sm-10"> 
                            <h5>Step 5 </h5>
                            <h4 class="small">Set Up Taxes</h4>
                            <h5 class="small">Add tax details for your company, so we can takes care of taxes</h5>
                        </div><div class="clearfix"></div>
                    </div>
                </a>

                <a href="<?php 
                        if ((session()->get('nextStep') == 'step6')or(!(strtolower(session()->get('step6')) == 'off'))) 
                        {
                            echo "/setupLeave";
                        }
                        else 
                        {
                            echo "";
                        }
                        ?>" class="ajaxify panel panel-<?php 
                        if (session()->get('step6')=="on") 
                        {
                            echo "success"; 
                        }
                        elseif (strtolower(session()->get('nextStep')) == 'step6') 
                        {
                            echo "default";
                        }
                        elseif((strtolower(session()->get('step6')) == 'off' )) 
                        {
                            echo 'off'; 
                        }
                        else 
                        {
                            echo "default";
                        }

                        ?> col-md-10 col-md-offset-1 col-sm-12 col-xs-12" >
                    <div class="row panel-heading dashboard-list">
                        <div class="col-md-2 col-sm-2" >
                            <i class="fa fa-calendar fa-3x" style="padding-top:16px;padding-left: 13px;"></i>
                        </div>
                        <div class="col-md-10 col-sm-10"> 
                            <h5>Step 6 </h5>
                            <h4 class="small">Set up Leave </h4>
                            <h5 class="small">Select Holiday calendar & setup leaves, also configure dates to run Payroll & Payday</h5>
                        </div><div class="clearfix"></div>
                    </div>
                </a>

                <a href="<?php 
                        if ((session()->get('nextStep') == 'step7')or(!(strtolower(session()->get('step7')) == 'off'))) 
                        {
                            echo "/companyPolicy";
                        }
                        else 
                        {
                            echo "";
                        }
                        ?>" class="ajaxify panel panel-<?php 
                        if (session()->get('step7')=="on") 
                        {
                            echo "success"; 
                        }
                        elseif (strtolower(session()->get('nextStep')) == 'step7') 
                        {
                            echo "default";
                        }
                        elseif((strtolower(session()->get('step7')) == 'off' )) 
                        {
                            echo 'off'; 
                        }
                        else 
                        {
                            echo "default";
                        }

                        ?> col-md-10 col-md-offset-1 col-sm-12 col-xs-12" >
                    <div class="row panel-heading dashboard-list">
                        <div class="col-md-2 col-sm-2" >
                            <i class="fa fa-bullhorn fa-3x" style="padding-top:16px;padding-left: 13px;"></i>
                        </div>
                        <div class="col-md-10 col-sm-10"> 
                            <h5>Step 7 </h5>
                            <h4 class="small"> Define Company Policies </h4>
                            <h5 class="small">Upload Company Policies & corporate Trainings for employees</h5>
                        </div><div class="clearfix"></div>
                    </div>
                </a>

                <?php if ((session()->get('plan')=='benefits') || (session()->get('plan')=='manager') ) {?>
                <a href="<?php 
                        if ((session()->get('nextStep') == 'step8')or(!(strtolower(session()->get('step8')) == 'off'))) 
                        {
                            echo "/addBenefit_step0";
                        }
                        else 
                        {
                            echo "";
                        }
                        ?>" class="ajaxify panel panel-<?php 
                        if (session()->get('step8')=="on") 
                        {
                            echo "success"; 
                        }
                        elseif (strtolower(session()->get('nextStep')) == 'step8') 
                        {
                            echo "default";
                        }
                        elseif((strtolower(session()->get('step8')) == 'off' )) 
                        {
                            echo 'off'; 
                        }
                        else 
                        {
                            echo "default";
                        }

                        ?> col-md-10 col-md-offset-1 col-sm-12 col-xs-12" >
                    <div class="row panel-heading dashboard-list">
                        <div class="col-md-2 col-sm-2" >
                            <i class="fa fa-user-md fa-3x" style="padding-top:16px;padding-left: 13px;"></i>
                        </div>
                        <div class="col-md-10 col-sm-10"> 
                            <h5>Step 8 </h5>
                            <h4 class="small">Setup Benefits </h4>
                            <h5 class="small">Select Health benefits for your employees or add the existing one</h5>
                        </div><div class="clearfix"></div>
                    </div>
                </a>
                <?php } ?>


                    </div>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    
</body>
</html>

<script>
    $(function() {

        // add active class to corresponding link in menu 
        $(".nav li.active").removeClass('active');
        $(".nav.nav-stacked li").filter(function(index) {
            return $(this).data('url') == 'getStarted';
        }).addClass('active');

        $.material.init();
    })
</script>
