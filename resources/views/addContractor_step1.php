<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>


<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">

                <div class="breadcrumb flat text-center row">
                    <span class="active col-md-3 col-sm-3 col-xs-12">The Basics</span>
                    <span class="col-md-3 col-sm-3 col-xs-6 hidden-xs">Personal Details</span>
                    <span class="col-md-3 col-sm-3 col-xs-6 hidden-xs">Tax Details</span>
                    <span class="col-md-3 col-sm-3 col-xs-6 hidden-xs">Payment Details</span>
                </div>

                <br>
                <br>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <form class="form-horizontal" method="post">
                        <fieldset>
                            <legend>Let's Bring them on Board!</legend>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating is-empty ">
                                        <label class="control-label" for="firstName"><span>First Name</span></label>
                                        <input class="form-control" id="firstName" name="firstName" type="text" style="cursor: auto;">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating is-empty ">
                                        <label class="control-label" for="lastName"><span>Last Name</span></label>
                                        <input class="form-control" id="lastName" name="lastName" type="text" style="cursor: auto;">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label for="status" class="control-label">Status</label>
                                        <select id="status" name="status" class="form-control">
                                            <option value="individual">Individual</option>
                                            <!-- <option value="huf">HUF</option>
                                            <option value="proprietorship">Proprietorship Firm</option>
                                            <option value="partnership">Partnership Firm</option>
                                            <option value="llp">LLP</option>
                                            <option value="pvtOrPublic">Pvt/Public Company</option>
                                            <option value="aopOrBoi">AOP/BOI</option> -->
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="form-group label-floating is-empty ">
                                        <label class="control-label" for="team"><span>Team</span></label>
                                        <input class="form-control" id="team" name="team" type="text" style="cursor: auto;">
                                    </div>
                                </div>
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p>You can create small teams within your company and assign employees within the team.</p>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="form-group label-floating is-empty">
                                        <label for="email" class="control-label">Email</label>
                                        <input type="email" class="form-control" name="email" id="email">
                                    </div>
                                </div>
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p>Employee email address,  this will be your employee username to login in Sashtechs</p>
                                </div>   
                            </div>


                            <div class="clearfix"></div>
                            <br>

                            <legend>Contract Details</legend>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label for="date" class="control-label">Date of Contract(s)</label>
                                        <input type="text" id="joinDate" name="joinDate" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating is-empty ">
                                        <label class="control-label" for="titleContract"><span>Name / Title of contract</span></label>
                                        <input class="form-control" id="titleContract" name="titleContract" type="text" style="cursor: auto;">
                                    </div>
                                </div>
                            </div>

                            
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label for="earning" class="control-label">Earning</label>
                                        <select id="earning" name="earning" class="form-control">
                                            <option value="hour">Hourly Rate</option>
                                            <option value="month">Monthly</option>
                                            <option value="project">Project </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4 col-md-offset-1 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating is-empty">
                                        <label for="earningAmount" class="control-label">Amount</label>
                                        <input type="number" class="form-control" id="earningAmount" name="earningAmount">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12">
                                    <div class="togglebutton form-group">
                                        <label>Contractor Self On-Boarding Y/N
                                            &nbsp;&nbsp;&nbsp; 
                                            <input type="checkbox" id="contractorSelfOnBoarding" name="contractorSelfOnBoarding">
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-8 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="col-md-8 col-sm-8 col-xs-8 col-md-offset-4 col-sm-offset-3 col-xs-offset-4">
                                        <span class="btn btn-default btn-raised cancel" onclick="history.back();">Cancel</span>
                                        <input type="submit" class="next btn bg-theme next btn-raised" value="save">
                                    </div>
                                </div>
                            </div>

                        </fieldset>
                    </form>
                </div>

                    </div>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>
    $(function() {
        $.material.init();

        $('#joinDate').bootstrapMaterialDatePicker({ weekStart : 0, time: false }).on('dateSelected', function(event) {
            $('#'+$(this).data('dtp')).find('.dtp-btn-ok').trigger('click');
        });
        $('#joinDate').bootstrapMaterialDatePicker('setDate', moment());

        function displayError() {
            swal({
                title : "Error !",
                text : 'Try Again!',
                type : 'error',
                confirmButtonClass : 'bg-theme',
                animation : false,
                width : 400
            });
        };

        function closeAlert() {
            $(".sweet-alert button").first().trigger('click');
        }

        // $('.cancel').click(function(event) {
        //     $(".help-block").remove();
        //     $(".has-error").removeClass('has-error');
        //     event.preventDefault();
        //     var $form = $(this).parents('form')[0];
        //     $form.reset();
        //     $($form).find('.form-group.label-floating').addClass('is-empty');
        // });

        $("form.form-horizontal").submit(function(event) {

            event.preventDefault();

            // remove all error messages
            $(".help-block").remove();
            $(".has-error").removeClass('has-error');
            
            swal({
                title : 'Saving',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 300
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: $('.main form').serialize(), 
            })
            .done(function(data) {
                if (data.status == 200) {
                    swal({
                        title : "Saved !",
                        // text : data.message,
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        showConfirmButton: false,
                        animation : false,
                        width : 300,
                        timer : 1500
                    });
                    if ($('#contractorSelfOnBoarding').is(':checked') && data.url=="/manageContractors") {
                        swal({
                            title : 'Another Contractor',
                            text : "Do you want to add another contractor?",
                            type : 'info',
                            confirmButtonClass : 'bg-theme',
                            animation : false,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            cancelButtonText: 'No',
                            cancelButtonClass: 'bg-theme',
                            closeOnConfirm: true,
                            closeOnCancel: true
                        },
                        function(isConfirm) {
                            if (isConfirm === true) {
                                data.url = location.pathname;
                                getAndInsert(data.url);
                                history.pushState(null, null, data.url);
                            }
                            else {
                                getAndInsert(data.url);
                                history.pushState(null, null, data.url);
                            }
                        })
                    }
                    else {
                        getAndInsert(data.url);
                        history.pushState(null, null, data.url);
                    }
                }
                else if(data.status == 208) {  //  email already Exist
                    closeAlert();
                    var $email = $("#email");
                    $email.parent().append('<p class="help-block">'+ data.message +'</p>');
                    $email.focus();
                    $email.parents('.form-group').addClass('has-error');
                }
                else{  // Validation Error
                    displayError();
                    // show Errors
                    for(var fields in data) {
                        $('#'+fields).parents('.form-group').addClass('has-error');
                        $('#'+fields).parent().append('<p class="help-block">'+ data[fields][0] +'</p>');
                    }
                }
            })
            .error(function() {
                displayError();
            });
        });

        $(".main .form-horizontal input").trigger('change');
    });
</script>
