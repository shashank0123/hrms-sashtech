<div class="container hidden-xs">
    <div class="row">
        <div class="col-md-3 col-sm-3"></div>
        <div class="col-md-9 col-sm-9 small text-uppercase">
            <div class="col-md-6 col-sm-6 heart-beat">We <i class="fa fa-heart text-danger"></i> Sashtechs</div>

            <div class=" col-md-6 col-sm-6 text-right dropdown">
                <a href="javascript:void(0)" data-target="#" class="dropdown-toggle" data-toggle="dropdown">Welcome Admin <b class="caret"></b></a>
                <ul class="dropdown-menu pull-right">
                    <li><a class="h6" href="/logout">Log Out</a></li>
                </ul>
            </div>
        </div>
        <br>
    </div>
</div>
<!-- ================  Body  ===================== -->

<style>    
    .nav.nav-stacked .active>a{
        color: #e54255;
        font-weight: 400;
    }

    #logo .icon-bar {
        border: 1px solid;
    }

    #logo .navbar-toggle {
        position: absolute;
        top: 3px;
        right: 0;
    }
</style>

<div class="container">
    <div class="row">
        <nav class="col-md-3 col-sm-3">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <span id="logo" class="fa btn" >Sashtechs <div></div>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-warning-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </span>
            </div>
            <div class="clearfix"></div>
            <div class="navbar-collapse collapse navbar-warning-collapse">
                <ul class="nav navbar nav-stacked" style="background:transparent">
                    <li class="active" data-url="adminDashboard"><a class="ajaxify btn" href="/adminDashboard"><i class="fa fa-flag fa-lg"></i>Dashboard</a></li>
                    <!-- <li class="disabled"><br></li> -->
                    <li data-url="adminCreateUser"><a class="ajaxify btn" href="/adminCreateUser"><i class="fa fa-users fa-lg"></i>Create User</a></li>
                    <li data-url="coupon"><a class="ajaxify btn" href="/coupon"><i class="fa fa-money fa-lg"></i>Coupon</a></li>
                    <li class="disabled"><br></li>
                    <li data-url="adminAddCompany"><a class="ajaxify btn" href="/adminAddCompany"><i class="fa fa-building-o fa-lg"></i>Add Company</a></li>
                    <li data-url="adminAssignCompany"><a class="ajaxify btn" href="/adminAssignCompany"><i class="fa fa-building fa-lg"></i>assign Company</a></li>
                    <li data-url="adminSelectCompany"><a class="ajaxify btn" href="/adminSelectCompany"><i class="fa fa-building fa-lg"></i>select Company</a></li>
                    <li data-url="adminBilling"><a class="ajaxify btn" href="/adminBilling"><i class="fa fa-file fa-lg"></i>Billing</a></li>
                </ul>
            </div>
        </nav>
