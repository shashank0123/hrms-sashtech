<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>


<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">
                        <br>
                        <form class="form-horizontal" autocomplete="off" action="" method="POST">

                            <fieldset>
                                <input style="display:none">
                                <input type="password" style="display:none">

                                <legend>Add Coupon Codes<hr></legend>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-6 col-sm-8 col-xs-12">
                                        <div class="form-group label-floating is-empty">
                                            <label class="control-label" for="code">Coupon Code</label>
                                            <input class="form-control" name="code" id="code" type="text">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-6 col-sm-8 col-xs-12">
                                        <div class="form-group">
                                            <label for="month" class="control-label">Coupon Month Value</label>
                                            
                                            <select id="month" class="form-control" name="month">
                                                <option selected value="">Select month</option>
                                                <option value="1">1 month</option>
                                                <option value="2">2 month</option>
                                                <option value="3">3 month</option>
                                                <option value="4">4 month</option>
                                                <option value="5">5 month</option>
                                                <option value="6">6 month</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-6 col-sm-8 col-xs-12">
                                        <div class="form-group label-floating is-empty">
                                            <label class="control-label" for="value">Coupon value</label>
                                            <input class="form-control" name="value" id="value" type="number" min="0">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-6 col-sm-8 col-xs-12">
                                        <div class="form-group">
                                            <label for="type" class="control-label">Usage Type</label>
                                            
                                            <select id="type" class="form-control" name="type">
                                                <option value="single">Single</option>
                                                <option value="multiple">Multiple</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-4 col-md-offset-3 col-sm-4 col-sm-offset-3 col-xs-12">
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-raised bg-theme" value="Submit">
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <br>
                            </fieldset>
                        </form>

                        <div class="clearfix"></div>
                        <br><br>

                        <?php if (!empty($coupon)): ?>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>Coupon Code</th>
                                    <th>Month Value</th>
                                    <th>Coupon Value</th>
                                    <th>Usage Type</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                 <?php foreach ($coupon as $key => $value):?>
                                <tr>
                                    <td><?php echo $value->code; ?></td>
                                    <td><?php echo $value->month; ?> month</td>
                                    <td><?php echo $value->value; ?></td>
                                    <td><?php echo $value->type; ?></td>
                                    <td><?php echo $value->status; ?></td>
                                    <td></td>
                                </tr>
                                <?php endforeach; ?>
                            </table>
                        </div>
                         <?php endif; ?>

                    </div>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>

    function displayError() {
        swal({
            title : "Error !",
            text : 'Try Again!',
            type : 'error',
            confirmButtonClass : 'bg-theme',
            animation : false,
            width : 400
        });
    };

    function closeAlert() {
        $(".sweet-alert button").first().trigger('click');
    }

    $(function() {

        // add active class to corresponding link in menu 
        $(".nav li.active").removeClass('active');
        $(".nav.nav-stacked li").filter(function(index) {
            return $(this).data('url') == 'coupon';
        }).addClass('active');

        $.material.init();

        $("form.form-horizontal").submit(function(event) {
            event.preventDefault();

            $(".help-block").remove();
            $(".has-error").removeClass('has-error');
            
            swal({
                title : 'Saving',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 400
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: $('.main form').serialize(), 
            })
            .done(function(data) 
            {
                if (data.status == 200) 
                {
                    swal({
                        title : "Success !",
                        text : data.message,
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });   
                    $('form').trigger('reset');
                    $('form .has-error').removeClass('has-error');
                }
                else if(data.status == 208)
                {
                    swal({
                        title : "Already Exists!",
                        text : data.message,
                        type : 'error',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });
                }
                else
                {  // Validation Error
                    displayError();
                    // show Errors
                    for(var fields in data) {
                        $('#'+fields).parents('.form-group').addClass('has-error');
                        $('#'+fields).parent().append('<p class="help-block">'+ data[fields][0] +'</p>');
                    }
                }
            })
            .error(function() {
                displayError();
            });
        });
    })
</script>
