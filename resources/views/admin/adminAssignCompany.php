<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>

<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">
                        <br>
                        <?php if(!empty($manager)): ?>
                        <form action="#" class="form-horizontal" id="assignCompany">

                            <br>
                            <h3>Managers</h3>
                            <br>

                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <tr>
                                        <th width="50"> </th>
                                        <th>Manager Name</th>
                                        <th>Contact</th>
                                        <th width="200">Companies</th>
                                        <th width="100"> </th>
                                    </tr>
                                    <?php 
                                    $c = 0;
                                     foreach ($manager as $key => $value):?>
                                    <tr class=" <?php echo ($c == 0) ? 'active' : ''; ?>">
                                        <td>
                                            <div class="radio"><label><input type="radio" name="userid" value="<?php echo $value->id; ?>" <?php echo ($c == 0) ? 'checked' : ''; ?>></label></div>
                                        </td>
                                        <td><span class="h5"><?php echo (!empty($value->firstName)) ? $value->firstName . ' ' . $value->lastName : $value->email ; ?></span></td>
                                        <td><span class="h5"><?php echo (!empty($value->phone)) ? $value->phone : ''; ?></span></td>
                                        <td class="list">
                                        <?php 
                                        if (!empty($value->companyId)) 
                                        {
                                            if (strlen($value->companyId) > 1) 
                                            {
                                                $cid    = explode(',', $value->companyId);
                                                foreach ($cid as $key2 => $value2) 
                                                {
                                                    foreach ($company as $key3 => $value3) 
                                                    {
                                                        if ($value3->id == $value2) 
                                                        {                                                   
                                                            echo '<span class="company-sm icon_size btn btn-xs" data-company="'.$value2.'">'.$value3->companyName.'</span>';
                                                        }
                                                    }
                                                }     
                                            }
                                            else
                                            {
                                                foreach ($company as $key3 => $value3) 
                                                {
                                                    if ($value3->id == $value->companyId)
                                                    {
                                                        $name   = $value3->companyName;
                                                    }
                                                }
                                                echo '<span class="company-sm icon_size btn btn-xs" data-company="'.$value->companyId.'">'.$name.'</span>';
                                            }
                                        }
                                         ?>
                                        </td>
                                        <td><span class="save btn btn-sm bg-theme" data-manager="<?php echo $value->id; ?>">Save</span></td>
                                    </tr>
                                    <?php $c++; ?>
                                    <?php endforeach; ?>
                                </table>
                            </div>
                            
                            <br>
                            <h3>Companies</h3>
                            <br>
                            <?php if(!empty($company)): ?>
                            <div id="companies">
                                <?php foreach ($company as $key => $value): ?>
                                <div class="company col-md-4 col-sm-6 col-xs-12">
                                    <div class="content" id="<?php echo $value->id; ?>">
                                        <img src="<?php echo (!empty($value->logoUrl)) ? '//' . $_SERVER['HTTP_HOST'] . '/' . $value->logoUrl : '' ; ?>" alt="<?php echo $value->companyName; ?>" class="img-thumbnail center-block">
                                        <br>
                                        <p class="text-center" ><?php echo $value->companyName; ?> </p>
                                        <div class="checkbox"><label><input type="checkbox" name="company[]" value="<?php echo $value->id; ?>" disabled></label></div>
                                    </div>
                                </div>
                                <?php endforeach; ?>
                            </div>
                            <?php else: ?>
                                <br>
                                <p class="clearfix">You don't have any active companies as of now, please add one first.</p>
                                <br>
                            <?php endif; ?>
                        </form>
                        <?php else: ?>
                            <br>
                            <p>You have no active managers as of now. You can only assign companies when you have at least one active manager.</p>
                            <br>
                        <?php endif; ?>
                    </div>
                    <style>
                        .form-horizontal .radio{
                            padding: 13px 0px 0px 7px;
                            margin: 0;
                        }
                        .table-bordered>tbody>tr>td>span.h5{
                            line-height: 42px;
                        }
                        .company .content{
                            cursor: pointer;
                            margin: 15px;
                            border: 1px solid #ddd;
                            box-shadow: 0px 0px 5px #ddd;
                            height: 12em;
                        }
                        .company .content > img {
                            border: none;
                            max-height: 5em;
                        }
                        .content .checkbox > label {
                            position: relative;
                            left: 44%;
                            margin-bottom: 10px;
                        }
                        .icon_size {
                            display: inline-block;
                        }
                        .icon_size img{
                            width: 4em;
                        }
                        .icon_size.btn.btn-xs{
                            padding: 4px 8px;
                        }
                        @media screen and (max-width: 450px) {
                            .company .content{
                                padding: 0 5px;
                                margin: 15px 0px;
                            }
                        }
                    </style>

                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>

    function displayError() {
        swal({
            title : "Error !",
            text : 'Try Again!',
            type : 'error',
            confirmButtonClass : 'bg-theme',
            animation : false,
            width : 400
        });
    };

    function closeAlert() {
        $(".sweet-alert button").first().trigger('click');
    }

    $(function() {

        // add active class to corresponding link in menu 
        $(".nav li.active").removeClass('active');
        $(".nav.nav-stacked li").filter(function(index) {
            return $(this).data('url') == 'adminAssignCompany';
        }).addClass('active');

        $.material.init();

        $(".table tr").click(function(event) {
            event.preventDefault();
            if ($(event.target).hasClass('save')) {
                var managerId = $(event.target).data('manager');
                var companies = [];

                $(this).find('.company-sm').each(function(index, el) {
                    companies.push($(el).data('company'));      
                });

                if (companies.length) {

                    swal({
                        title : 'Saving',
                        html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                        allowOutsideClick : false,
                        showConfirmButton : false,
                        animation : false,
                        width : 400
                    });

                    $.ajax({
                        url: location.href,
                        type: 'POST',
                        data: {
                            manager: managerId,
                            companies : companies.join()
                        },
                    })
                    .done(function(data) {

                        if (data.status == 200) 
                        {
                            swal({
                                title : "Success !",
                                text : data.message,
                                type : 'success',
                                confirmButtonClass : 'bg-theme',
                                animation : false,
                                width : 400
                            });   
                        }
                        else {
                            displayError();
                        }
                    })
                    .fail(function() {
                        displayError();
                    })
                }

                
            }
            else{
                $("tr.active").removeClass('active');
                $(this).addClass('active');
                $(this).find('.radio label input').prop('checked', true);

                $companySmall = $(this).find('.list .company-sm');

                $(".company .checkbox input").prop('checked', false);
                $companySmall.each(function(index, el) {
                    var companyId = $(el).data('company');
                    $("#"+companyId + ' .checkbox input').prop('checked',true);
                });
            }
        });

        $(".company > .content").click(function(event) {
            var $check = $(this).find('.checkbox input');
            $check.prop('checked', !$check.prop('checked'));
            // add icon in row if checked
            if ($check.is(':checked')) {
                var $company = $(this).find('p').text();
                // $company.removeClass('img-thumbnail center-block');
                $(".table tr.active > .list").append($('<span class="company-sm icon_size btn btn-xs" data-company="'+$(this).attr('id')+'"></span>').text($company));
            }
            else{
                var companyId = $(this).attr('id');
                $(".table tr.active .list .company-sm").filter(function(index) {
                    return $(this).data('company') == companyId;
                }).remove();
            }
        });

        var tr = $(".table tr:eq(1)");
        tr.addClass('active');
        tr.find('.radio label input').prop('checked', true);

        var $companySmall = tr.find('.list .company-sm');

        $(".company .checkbox input").prop('checked', false);
        $companySmall.each(function(index, el) {
            var companyId = $(el).data('company');
            $("#"+companyId + ' .checkbox input').prop('checked',true);
        });

    });

</script>
