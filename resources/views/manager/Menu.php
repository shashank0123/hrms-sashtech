<div class="container hidden-xs">
    <div class="row">
        <div class="col-md-3 col-sm-3"></div>
        <div class="col-md-9 col-sm-9 small text-uppercase">
            <div class="col-md-6 col-sm-6 heart-beat">
            
            </div>

            <div class=" col-md-6 col-sm-6 text-right dropdown">
                <a href="javascript:void(0)" data-target="#" class="dropdown-toggle" data-toggle="dropdown">Welcome 
              <?php echo session()->get('managerName');?>
                <b class="caret"></b></a>
                <ul class="dropdown-menu pull-right">
                    <li><a class="h6" href="/logout">Log Out</a></li>
                </ul>
            </div>
        </div>
        <br>
    </div>
</div>
<!-- ================  Body  ===================== -->

<style>    
    .nav.nav-stacked .active>a{
        color: #e54255;
        font-weight: 400;
    }

    #logo .icon-bar {
        border: 1px solid;
    }

    #logo .navbar-toggle {
        position: absolute;
        top: 3px;
        right: 0;
    }
</style>

<div class="container">
    <div class="row">
        <nav class="col-md-3 col-sm-3">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <span id="logo" class="fa btn" >Sashtechs <div></div>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-warning-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </span>
            </div>
            <div class="clearfix"></div>
            <div class="navbar-collapse collapse navbar-warning-collapse">
                <ul class="nav navbar nav-stacked" style="background:transparent">
                    <li data-url="managerDashboard" class="active"><a class="ajaxify btn" href="/managerDashboard"><i class="fa fa-flag fa-lg"></i>Dashboard</a></li>
                    <li data-url="managerDetail" class="active"><a class="ajaxify btn" href="/managerDetail"><i class="fa fa-flag fa-lg"></i>Personal Detail</a></li>
                    <!-- <li class="disabled"><br></li> -->
                    <li data-url="managerSelectCompany"><a class="ajaxify btn" href="/managerSelectCompany"><i class="fa fa-building fa-lg"></i>select Company</a></li>
                    <li data-url="managerHistory"><a class="ajaxify btn" href="/managerHistory"><i class="fa fa-file fa-lg"></i>History</a></li>
                </ul>
            </div>
        </nav>
