<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>


<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <?php if (empty($profile)): ?>
                    <div class="panel-body" style="min-height:36em;">
                        <br><br><br>
                        <center><h4>WELCOME TO <br> <h1>Sashtechs</h1></h4></center>
                        <br>

                        <div class="text-center">
                            <span>Hi <?php echo session()->get('accountantName'); ?>! Your account is almost ready. We just have a couple of quick questions to ask.</span>
                        </div>

                        <br>
                        <br>
                        <br>
                        <br>
                        <center><a href="/managerDetail" class="ajaxify btn bg-theme btn-raised"><i class="fa fa-share"></i> &nbsp; Lets Get Started</a></center><br>
                    </div>
                    <?php else: ?>
                    <div class="panel-body" style="min-height:36em;">

                        <br><br>
                        <div class="col-md-4 col-sm-3 col-xs-12">
                        </div>

                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="extra text-center notice">
                                <div class="box">
                                    <img src="images/Pin.png" class="boardPin" alt="pin">
                                    <h4>Your Accountant</h4>
                                    <div class="profilePic"></div>
                                    <br>
                                    <h6><?php if (!empty($result3->firstName)): ?>
                                    <?php echo $result3->firstName . ' ' . $result3->lastName; ?>
                                    <?php else: ?>
                                        Accountant
                                    <?php endif ?>
                                    </h6>
                                    <h6>
                                        <?php if (!empty($result4->phone)): ?>
                                            <?php echo $result4->phone; ?>
                                        <?php else: ?>
                                            0123456789
                                        <?php endif ?>
                                    </h6>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <br><br>

                        <div id="wrapper">

                            <div id="page-wrapper">

                                <div class="container-fluid row">
                                    <!-- /.row -->

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h3 class="panel-title"><i class="fa fa-clock-o fa-fw"></i> Notifications</h3>
                                            </div>
                                            <div class="panel-body">
                                                <div class="list-group">
                                                    <h5>Quote of the Day</h5>
                                                    <span class="list-group-item">
                                                        <i class="fa fa-fw fa-bullhorn"></i> 
                                                        "<?php echo $result->quote; ?>" - <?php echo $result->author; ?>
                                                    </span>
                                                </div>

                                                <?php if (!empty($bDay) and isset($bDay)): ?>
                                                <h5>Upcoming Birthdays</h5>
                                                <div class="list-group">
                                                    <?php foreach ($bDay as $key => $value): ?>
                                                        <?php 
                                                            if ($value['on'] < date('Y-m-d')) 
                                                            {
                                                                continue;
                                                            }
                                                         ?>
                                                        <?php if(session()->get('employeeId') == $value['id']): ?>
                                                            <?php if (date_diff(date_create(date('d-m-Y', strtotime($value['on']))), date_create(date('d-m-Y')))->days == 0): ?>
                                                            <span class="list-group-item">
                                                                <i class="fa fa-fw fa-birthday-cake"></i> Happy Birthday! :D
                                                            </span>
                                                            <?php else: ?>
                                                            <span class="list-group-item">
                                                                <span class="badge"><?php //echo date('d-M', strtotime($value['on'])); ?></span>
                                                                <i class="fa fa-fw fa-birthday-cake"></i> Your birthday is coming on
                                                            </span>
                                                            <?php endif; ?>
                                                        <?php else: ?>
                                                            <?php if (date_diff(date_create(date('d-m-Y', strtotime($value['on']))), date_create(date('d-m-Y')))->days == 0): ?>
                                                            <span class="list-group-item">
                                                                <span class="badge">Today!</span>
                                                                <i class="fa fa-fw fa-birthday-cake"></i> It's <?php //echo $value['name'] . "'s"; ?> birthday today!
                                                            </span>
                                                            <?php else: ?>
                                                            <span class="list-group-item">
                                                                <span class="badge"><?php //echo date('d-M', strtotime($value['on'])); ?></span>
                                                                <i class="fa fa-fw fa-birthday-cake"></i> Upcoming birthday of <?php //echo $value['name']; ?> on
                                                            </span>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                </div>
                                                <?php endif; ?>

                                                <?php if (!empty($result5)) : ?>
                                                <h5>Tweets</h5>
                                                <div class="list-group">
                                                    <?php foreach($result5 as $key => $value): ?>
                                                        <?php 
                                                            $datetime1  = new DateTime($value->created_at);
                                                            $datetime2  = new DateTime(date('d-m-Y h:i:sa'));

                                                            $interval   = $datetime1->diff($datetime2);

                                                            if ($interval->y < 1) 
                                                            {
                                                                if ($interval->m == 0) 
                                                                {
                                                                    if ($interval->d == 0) 
                                                                    {
                                                                        if ($interval->h == 0) 
                                                                        {
                                                                            if ($interval->i == 0) { $time = $interval->s . ' seconds ago' ; }
                                                                            else { $time = $interval->i . ' mins ago' ; }
                                                                        }
                                                                        else { $time = $interval->h . ' hours ago' ; }
                                                                    }
                                                                    else { $time = $interval->d . ' days ago' ;
                                                                    } }
                                                                elseif ($interval->m == 1) 
                                                                {
                                                                    $time = $interval->m . ' month and ' . $interval->d . ' days ago' ;
                                                                }
                                                                else { $time = $interval->m . ' months and ' . $interval->d . ' days ago' ; }
                                                            }
                                                            else { $time = 'Older than a year'; }
                                                         ?>

                                                    <span class="list-group-item">
                                                        <span class="badge"><?php //echo $time ?></span>
                                                        <i class="fa fa-fw fa-comment"></i>
                                                        <?php //echo htmlspecialchars_decode($value->messageContent); ?>
                                                    </span>
                                                    <?php endforeach; ?>
                                                </div>
                                                <?php endif; ?>

                                                <?php if(!empty($result6)): ?>
                                                <h5>Company Policy Updates</h5>
                                                <div class="list-group">
                                                    <?php foreach ($result6 as $key => $value) : ?>
                                                        <?php 
                                                            $datetime1  = new DateTime($value->created_at);
                                                            $datetime2  = new DateTime(date('d-m-Y h:i:sa'));

                                                            $interval   = $datetime1->diff($datetime2);

                                                            if ($interval->y < 1) 
                                                            {
                                                                if ($interval->m == 0) 
                                                                {
                                                                    if ($interval->d == 0) 
                                                                    {
                                                                        if ($interval->h == 0) 
                                                                        {
                                                                            if ($interval->i == 0) { $time = $interval->s . ' seconds ago' ; }
                                                                            else { $time = $interval->i . ' mins ago' ; }
                                                                        }
                                                                        else { $time = $interval->h . ' hours ago' ; }
                                                                    }
                                                                    else { $time = $interval->d . ' days ago' ;
                                                                    } }
                                                                elseif ($interval->m == 1) 
                                                                {
                                                                    $time = $interval->m . ' month and ' . $interval->d . ' days ago' ;
                                                                }
                                                                else { $time = $interval->m . ' months and ' . $interval->d . ' days ago' ; }
                                                            }
                                                            else { $time = 'Older than a year'; }
                                                         ?>
                                                    <span class="list-group-item">
                                                        <span class="badge"><?php //echo $time; $time = NULL; ?></span>
                                                        <i class="fa fa-fw fa-bullhorn"></i> Your employer added a new company policy - "<?php //echo $value->policyName; ?>". Please check policies for more info.
                                                    </span>
                                                    <?php endforeach; ?>
                                                </div>
                                                <?php endif; ?>
                                                <!-- <div class="text-right">
                                                    <a href="#">View All Activity <i class="fa fa-arrow-circle-right"></i></a>
                                                </div> -->
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <!-- /.container-fluid -->

                            </div>
                            <!-- /#page-wrapper -->

                        </div>

                        <style>
                            .tweet .textarea {
                                width: 100%;
                                border-radius: 3px;
                                padding: 10px;
                                border: 1px solid #ddd;
                            }

                            .profilePic {
                                margin: 0 auto;
                                display: block;
                                width: 8em;
                                height: 8em;
                                text-align: center;
                                line-height: 9em;
                                border-radius: 50%;
                                <?php if (!empty($result4->dp)): ?>
                                background-image: url(<?php echo $result4->dp; ?>);
                                <?php else:; ?>
                                background-image: url(images/testLogo.png);
                                <?php endif ?>
                                background-size: contain;
                                background-repeat: no-repeat;
                                background-position: center;
                                border: 1px dotted;
                            }
                        </style>

                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>
    $(function() {

        // add active class to corresponding link in menu
        $(".nav li.active").removeClass('active');
        $(".nav.nav-stacked li").filter(function(index) {
            return $(this).data('url') == 'managerDashboard';
        }).addClass('active');

        $.material.init();
    })
</script>
