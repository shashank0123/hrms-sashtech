<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>

<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">

                <br>
                <br>
                  <?php if (isset($result)&&(!empty($result))): ?>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>Policy Name</th>
                                    <th>Description</th>
                                    <th>Completion Date</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                 <?php foreach ($result as $key => $value):?>
                                <tr>
                                    <td><?php echo $value->policyName; ?></td>
                                    <td><?php echo $value->description; ?></td>
                                    <td><?php echo $value->completionDate; ?></td>
                                    <td><span class="edit btn btn-sm" data-id="<?php echo $value->id ?> "><i class="fa fa-pencil"></i></span></td>
                                    <td><span class="delete btn btn-sm" data-id="<?php echo $value->id ?>"><i class="fa fa-times"></i></span></td>
                                </tr>
                                <?php endforeach; ?>
                            </table>
                        </div>
                    <?php endif; ?>
                <br>
                <br>
                <form class="form-horizontal" id="companyPolicy" method="POST" enctype="multipart/form-data">
                    <fieldset>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <legend>Define Company Policy</legend>

                            <input type="hidden" id="id" name="id">

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="form-group label-floating is-empty ">
                                        <label class="control-label" for="policyName"><span>Name of Policy</span></label>
                                        <input class="form-control" id="policyName" name="policyName" type="text" style="cursor: auto;">
                                    </div>
                                </div>
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p>Enter Name of Policy or training you want to offer to employees</p>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="form-group label-floating">
                                        <label for="description" class="control-label">Description</label>
                                        <textarea class="form-control" rows="2" name="description" id="description"></textarea>
                                    </div> 
                                </div>
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p>Enter Name of Policy or training you want to offer to employees</p>
                                </div>   
                            </div>   

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="form-group is-fileinput label-floating">
                                        <label for="proof" class="control-label">Upload (PDF)</label>

                                        <input type="text" readonly="" class="form-control" name="filename" placeholder=" ">
                                        <input type="file" accept="application/pdf" name="proof" id="proof" multiple="">
                                    </div>
                                </div>
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p>Enter details of Policy</p>
                                </div> 
                            </div> 

                            <div class="clearfix"></div>
                            
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="form-group label-floating">
                                        <label for="completionDate" class="control-label">Completion Date </label>
                                        <input type="text" name="completionDate" id="completionDate" class="form-control">
                                    </div>
                                </div>
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p>Enter details of Policy</p>
                                </div> 
                            </div> 

                            <div class="clearfix"></div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-12 col-sm-12 8l-xs-12">
                                    <div class="togglebutton form-group">
                                        <label>Required Employee Sign 
                                            &nbsp;&nbsp;&nbsp; 
                                            <input type="checkbox" name="requireEmployeeSign">
                                        </label>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-8 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <span class="btn btn-default btn-raised cancel">Cancel</span>
                                        <input type="submit" class="next btn bg-theme btn-raised" value="next">
                                        <span id="skip" class="btn btn-default btn-raised">Skip</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </fieldset>
                </form>

                    </div>

                    <style>
                        .edit , .delete{
                            cursor: pointer;
                            margin: 0;
                        }
                    </style>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>

    function displayError() {
        swal({
            title : "Error !",
            text : 'Try Again!',
            type : 'error',
            confirmButtonClass : 'bg-theme',
            animation : false,
            width : 400
        });
    };

    function closeAlert() {
        $(".sweet-alert button").first().trigger('click');
    }

    $(function() {
        $.material.init();

        $('#completionDate').bootstrapMaterialDatePicker({ weekStart : 0, time: false }).on('dateSelected', function(event) {
           $('#'+$(this).data('dtp')).find('.dtp-btn-ok').trigger('click');
        });

        $('.cancel').click(function(event) {
            $(".help-block").remove();
            $(".has-error").removeClass('has-error');
            event.preventDefault();
            var $form = $(this).parents('form')[0];
            $form.reset();
            $($form).find('.form-group.label-floating').addClass('is-empty');
        });

        $('#companyPolicy').submit(function(event) {

            // =============================================================================
            event.preventDefault();

            // remove all error messages
            $(".help-block").remove();
            $(".has-error").removeClass('has-error');
            
            swal({
                title : 'Saving',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 400
            });

            var $companyPolicy = $("#companyPolicy");
            var id = $companyPolicy.find('#id').val();
            var policyName = $companyPolicy.find('#policyName').val();
            var description = $companyPolicy.find('#description').val();
            var proof = $companyPolicy.find('#proof')[0].files[0];
            var completionDate = $companyPolicy.find('#completionDate').val();
            var requireEmployeeSign = $companyPolicy.find('#requireEmployeeSign').is(':checked');


            var formdata = new FormData();
            formdata.append('id', id);
            formdata.append('policyName', policyName);
            formdata.append('description', description);
            formdata.append('proof', proof || '');
            formdata.append('completionDate', completionDate);
            formdata.append('requireEmployeeSign', requireEmployeeSign);
            formdata.append('filename', proof ? proof.name : '');

            $.ajax({
                url: location.href,
                type: 'POST',
                data: formdata,
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false
            })
            .done(function(data) {

                if (data.status == 200) {
                    swal({
                        title : "Success !",
                        text : data.message,
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });   
                    getAndInsert(data.url);
                    history.pushState(null, null, data.url);
                }
                else if(data.status == 208) {  //  email already Exist
                    closeAlert();
                    var $email = $("#email");
                    $email.parent().append('<p class="help-block">'+ data.message +'</p>');
                    $email.focus();
                    $email.parents('.form-group').addClass('has-error');
                }
                else{  // Validation Error
                    displayError();
                    // show Errors
                    for(var fields in data) {
                        $('#'+fields).parents('.form-group').addClass('has-error');
                        $('#'+fields).parent().append('<p class="help-block">'+ data[fields][0] +'</p>');
                    }
                }
            })
            .error(function() {
                displayError();
            });
        });

        function handleEdit() {
            event.preventDefault();

            swal({
                title : 'Fetching Data',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 400
            });

            var id = $(this).data('id');
            $.get(location.href+'/'+id, function(data) {
                for(var fields in data) {
                    var el = $('#'+fields);
                    var parent = el.parents(".form-group");
                    // if data fill the fields
                    if (data[fields]){
                        el.val(data[fields]);
                        parent.removeClass('is-empty');
                    }
                    else{
                        el.val('');
                        parent.addClass('is-empty');
                    }
                }

                $('#companyPolicy #type').val(id);
                $('#companyPolicy').removeClass('hidden');

                closeAlert();
            }).error(function() {
                displayError()
            });
        };

        $(".edit").click(handleEdit);

        function handleDelete() {
            event.preventDefault();

            var $companyPolicy = $("#companyPolicy");
            $companyPolicy.addClass('hidden'); // first hide the form then clear it

            swal({
                title : 'Deleting Data',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 400
            });

            var id = $(this).data('id');
            var $row = $(this).parents('tr');
            $.post(location.href + '/' + id, {id: id}, function(data, textStatus, xhr) {
                if (data.status == 200) {
                    $row.remove();
                    swal({
                        title : "Success !",
                        text : "Policy Deleted",
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });
                }
                else {
                    displayError();
                }
            });
        };

        $(".delete").click(handleDelete);

        // Skip this page
        $("#skip").on('click', function(event) {
            swal({
                title : 'Please Wait',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 400
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: "skip=true",
            })
            .done(function(data) {
                // console.log(data);
                if (data.status == 200) {
                    closeAlert();
                    var href = data.url;
                    getAndInsert(href);
                    history.pushState(null, null, href);
                }
                else {
                    displayError();
                }
            })
            .fail(function() {
                closeAlert();
                displayError();
            })
        });

        $("#proof").change(function(event) {

            if (event.target.files.length) {
                var file = event.target.files[0];
                if (file.size > 1024*1024) {// under 500kb file is allowed
                    swal({
                        title : "Error !",
                        text : 'File Size is more than 1 MB',
                        type : 'error',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });
                    return false;                    
                }

                if (file.type != 'application/pdf') {
                    swal({
                        title : "Error !",
                        text : 'only pdf document are allowed',
                        type : 'error',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });
                    return false; 
                }
            }
        });
        
    });
</script>
