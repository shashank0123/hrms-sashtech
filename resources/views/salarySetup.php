<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>


<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">
                        <style>
                            .table.table-hover tbody tr td{
                                line-height: 25px;
                            }
                            .table.table-hover .btn.btn-sm{
                                margin: -2px;
                                text-transform: capitalize;
                            }
                        </style>
                        <br>
                        <legend>Cost to Company (CTC)</legend>

                        <br>

                        <div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
                            <table class="table table-hover">
                                <tbody>
                                    <tr>
                                        <th>Job title</a> </th>
                                        <th>CTC Package</a> </th>
                                        <th></th>
                                        <th></th>
                                    </tr>                                            
                                    <?php if (isset($result)and (count($result)>0)): ?>
                                    <?php foreach ($result as $key => $array): ?>
                                    <tr>
                                        <td><h6><?php echo $array->jobTitle; ?></h6></td>
                                        <td><h6><i class="fa fa-inr"></i> &nbsp;&nbsp;<?php echo $array->ctc; ?></h6></td>
                                        <td><h6 class="btn btn-sm edit" data-id="<?php echo $array->id ?>"><i class="fa fa-pencil"></i> edit</h6></td>
                                        <td><h6 class="btn btn-sm delete" data-id="<?php echo $array->id ?>"><i class="fa fa-remove"></i> delete</h6></td>
                                    </tr>
                                    <?php endforeach; ?>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="col-md-4 col-sm-4 col-md-offset-8 col-sm-offset-8">
                                <span id="addCTC" class="btn"><i class="fa fa-plus"></i>Add CTC</span>
                                <?php if(isset($result)and(count($result)>0)): ?>
                                    <span id="next" class="btn">Next</span>
                                <?php else: ?>
                                    <span id="next" class="btn">Skip</span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <br><br>

                        <div id="salaryComponents" class="hidden">
                            <div class="clearfix"></div>
                            <hr>
                            <form action="#" class="form-horizontal">
                                <input type="hidden" name="id" id="ctcId" value="0">

                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="jobTitle"><span>Job Title</span></label>
                                            <input class="form-control" id="jobTitle" name="jobTitle" type="text" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>   

                                <div class="col-md-4 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="ctc"><span>CTC</span></label>
                                            <input class="form-control" id="ctc" name="ctc" type="number" min="0" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p>Gross pay to be paid to Employees</p>
                                </div>

                                <div class="clearfix"></div>
                                <br>
                                <legend>Salary Components</legend>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="basic"><span>Basic</span></label>
                                            <input class="form-control" id="basic" name="basic" type="number" min="0" style="cursor: auto;">
                                        </div>
                                    </div>
                                    <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                        <p>Basic salary is the core salary and it is the fixed part of the compensation package</p>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="hra"><span>HRA</span></label>
                                            <input class="form-control" id="hra" name="hra" type="number" min="0" style="cursor: auto;">
                                        </div>
                                    </div>
                                    <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                        <p>This amount is for allowance for housing</p>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="conveyance"><span>Conveyance</span></label>
                                            <input class="form-control" id="conveyance" name="conveyance" type="number" min="0" style="cursor: auto;" max="19200">
                                        </div>
                                    </div>
                                    <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                        <p>Enter details to complete CTC</p>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="medicalAllowance"><span>Medical Allowance</span></label>
                                            <input class="form-control" id="medicalAllowance" name="medicalAllowance" type="number" min="0" max="15000" style="cursor: auto;">
                                        </div>
                                    </div>
                                    <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                        <p>Enter details to complete CTC</p>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="others"><span>Others </span></label>
                                            <input class="form-control" id="others" name="others" type="number" min="0" style="cursor: auto;">
                                        </div>
                                    </div>
                                    <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                        <p>Enter details to complete CTC</p>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="medicalInsurance"><span>Medical Insurance</span></label>
                                            <input class="form-control" id="medicalInsurance" name="medicalInsurance" type="number" min="0" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="telephone"><span>Telephone Allowance</span></label>
                                            <input class="form-control" id="telephone" name="telephone" type="number" min="0" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="leaveTravel"><span>Leave Travel Allowance</span></label>
                                            <input class="form-control" id="leaveTravel" name="leaveTravel" type="number" min="0" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="uniform"><span>Uniform Allowance</span></label>
                                            <input class="form-control" id="uniform" name="uniform" type="number" min="0" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="gratuity"><span>Gratuity</span></label>
                                            <input class="form-control" id="gratuity" name="gratuity" type="number" min="0" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="superAnnuation"><span>Super Annuation</span></label>
                                            <input class="form-control" id="superAnnuation" name="superAnnuation" type="number" min="0" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="annualBonus"><span>Annual Bonus</span></label>
                                            <input class="form-control" id="annualBonus" name="annualBonus" type="number" min="0" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="festivalBonus"><span>Festival Bonus</span></label>
                                            <input class="form-control" id="festivalBonus" name="festivalBonus" type="number" min="0" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="incentives"><span>Incentives</span></label>
                                            <input class="form-control" id="incentives" name="incentives" type="number" min="0" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="leaveEncashment"><span>Leave Encashment</span></label>
                                            <input class="form-control" id="leaveEncashment" name="leaveEncashment" type="number" min="0" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="pfContribution"><span>PF Contribution</span></label>
                                            <input class="form-control" id="pfContribution" name="pfContribution" type="number" min="0" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="esiContribution"><span>ESI Contribution</span></label>
                                            <input class="form-control" id="esiContribution" name="esiContribution" type="number" min="0" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-8 addMoreButton">
                                    <div class="btn-group  pull-left">
                                        <a href="#" data-target="#" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
                                            Add more Fields
                                            <span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a href="javascript:void(0)" data-id="medicalInsurance" class="addMore">Medical Insurance</a></li>
                                            <li><a href="javascript:void(0)" data-id="telephone" class="addMore">Telephone Allowance</a></li>
                                            <li><a href="javascript:void(0)" data-id="leaveTravel" class="addMore">Leave Travel Allowance</a></li>
                                            <li><a href="javascript:void(0)" data-id="uniform" class="addMore">Uniform Allowance</a></li>
                                            <li><a href="javascript:void(0)" data-id="gratuity" class="addMore">Gratuity</a></li>
                                            <li><a href="javascript:void(0)" data-id="superAnnuation" class="addMore">Super Annuation</a></li>
                                            <li><a href="javascript:void(0)" data-id="annualBonus" class="addMore">Annual Bonus</a></li>
                                            <li><a href="javascript:void(0)" data-id="festivalBonus" class="addMore">Festival Bonus</a></li>
                                            <li><a href="javascript:void(0)" data-id="incentives" class="addMore">Incentives</a></li>
                                            <li><a href="javascript:void(0)" data-id="leaveEncashment" class="addMore">Leave Encashment</a></li>
                                            <li><a href="javascript:void(0)" data-id="pfContribution" class="addMore">PF Contribution</a></li>
                                            <li><a href="javascript:void(0)" data-id="esiContribution" class="addMore">ESI Contribution</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <br>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="">
                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                            <input type="submit" class="btn bg-theme btn-raised pull-left submit" value="Save CTC">
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>
    $(function() {

        $.material.init();
        var $tableBody = $(".table tbody");

        var availableTitle = [
            "Chief Product Officer",
            "CEO",
            "Associate Vice President",
            "QA Manager",
            "Manager",
            "Technical Manager",
            "Senior QA Engineer",
            "Business Analyst",
            "Chief Finance Officer",
            "Senior Vice President - Marketing",
            "Data Scientist",
            "Talent Acquisition",
            "Senior Recruiter",
            "Director of Engineering",
            "Software Developer",
            "Assistant Manager Payroll",
            "Associate Payroll",
            "Associate HR",
            "Assistant Manager Taxation",
            "Team Manager",
            "Sales Manager",
            "Sales Executive",
            "Office Assistant",
            "Computer Operator",
            "Director",
            "Managing Director",
            "Accounts Executive",
            "Accounts Manager",
            "HR Manager",
            "Team Lead",
            "Scheme"
        ].sort();

        $("#jobTitle").autocomplete({
            source : availableTitle,
        })

        function closeAlert() {
            $(".sweet-alert button").first().trigger('click');
        }

        function displayError() {
            swal({
                title : "Error !",
                text : 'Try Again!',
                type : 'error',
                confirmButtonClass : 'bg-theme',
                animation : false,
                width : 400
            });
        };

        function sendData(ctc) {
            swal({
                title : 'Saving',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 400
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: $("#salaryComponents form").serialize(),
            })
            .done(function(data) {
                if (data.status == 200) {
                    swal({
                        title : "Success !",
                        text : data.message,
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });

                    if ($("#ctcId").val() == 0) {
                        pushToTable(data, data.id);
                    }
                    else {
                        $row = $(".edit").filter(function(index) {
                            return $(this).data('id') == $("#ctcId").val();
                        }).parents('tr');
                        $row.find('h6:eq(0)').text($('#jobTitle').val());
                        $row.find('h6:eq(1)').html('<i class="fa fa-inr"></i> '+$('#ctc').val()+'</h6>');
                        $("#ctcId").val(0);
                        var $salaryComponents = $("#salaryComponents");
                        $salaryComponents.addClass('hidden'); // first hide the form then clear it
                        $salaryComponents.find('form')[0].reset(); // clear all field
                        $salaryComponents.find('.form-group').addClass('is-empty');
                    }
                    
                }
                else {
                    displayError();
                }

            })
            .fail(function() {
                closeAlert();
                displayError();
            })
        }

        function resetSalaryComponent() {
            // reset salary component and add more drop down 
            // =============================
            $(".addMoreButton").show();
            $(".addMoreButton ul li a").addClass('hidden');

            var $salaryComponents = $("#salaryComponents");
            $salaryComponents.find('.hidden').removeClass('hidden');
            $salaryComponents.find('.form-group input').val('');
            $salaryComponents.find('.form-group').addClass('is-empty');
            $(".addMoreButton ul li a").each(function(index, el) {
                $('#'+$(this).data('id')).parent().parent().addClass('hidden');
            });
            // =============================
        }

        function clearTable() {
            $tableBody.empty();
            var head = '<tr><th>Job title</a> </th><th>CTC Package</a> </th><th><i class="fa fa-pencil"></i> edit</th><th><i class="fa fa-remove"></i> delete</th></tr>';
            $tableBody.append(head);
        }

        function pushToTable(ctc, id) {
            $row = $('<tr>'+
                '<td><h6>'+$('#jobTitle').val()+'</h6></td>'+
                '<td><h6><i class="fa fa-inr"></i> '+$('#ctc').val()+'</h6></td>'+
                '<td><h6><a class="btn btn-sm edit" data-id="'+id+'"><i class="fa fa-pencil"></i> edit</a></h6></td>'+
                '<td><h6><a class="btn btn-sm delete" data-id="'+id+'"><i class="fa fa-remove"></i> delete</a></h6></td>'+
            '</tr>');

            $(".table").append($row);
            $row.find('.edit').click(handleEdit);
            $row.find('.delete').click(handleDelete);
            var $salaryComponents = $("#salaryComponents");
            $salaryComponents.addClass('hidden'); // first hide the form then clear it
            $salaryComponents.find('form')[0].reset(); // clear all field
            $salaryComponents.find('.form-group').addClass('is-empty');

            if ($(".table tr").length > 1) {
                $("#next").text("Next");
            }
            else {
                $("#next").text('skip');
            }

        }

        // show CTC form
        $("#addCTC").on('click', function(event) {

            if ($(this).attr('disabled')) return false;
            // reset salary component and add more drop down 
            // =============================
            $(".addMoreButton").show();
            $(".addMoreButton ul li a").addClass('hidden');

            var $salaryComponents = $("#salaryComponents");
            $salaryComponents.find('.hidden').removeClass('hidden');
            $salaryComponents.find('.form-group input:not(":last")').val('');
            $salaryComponents.find('.form-group').addClass('is-empty');
            $(".addMoreButton ul li a").each(function(index, el) {
                $('#'+$(this).data('id')).parent().parent().addClass('hidden');
            });
            // =============================

            if ($(this).attr('disabled')) return;
            event.preventDefault();

            // $("#salaryComponents form")[0].reset();
            $("#salaryComponents").removeClass('hidden');
        });

        // submit or skip CTC form
        $("#next").on('click', function(event) {
            swal({
                title : 'Please Wait',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 400
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: "next=true",
            })
            .done(function(data) {
                // console.log(data);
                if (data.status == 200) 
                {
                    closeAlert();
                    var href = data.url;
                    getAndInsert(href);
                    history.pushState(null, null, href);
                }
                else 
                {
                    displayError();
                }
            })
            .fail(function() {
                closeAlert();
                displayError();
            })
        });

        $('#salaryComponents form').submit(function(event) {

            $("#addCTC").attr('disabled', false);
            
            event.preventDefault();

            var input = $("#salaryComponents .form-group input");
            var ctc = 0;

            // checking sum of salary components w.r.t ctc
            input.each(function(index, el) {
                ctc += $(el).val() ? 1*$(el).val() | 0: 0;
            });
            ctc -= $("#ctc").val() || 0; // remove ctc that added

            if (!$("#ctc").val()) {
                $("#ctc").val(ctc).parents(".label-floating").removeClass('is-empty');
            }

            if($("#ctc").val() < ctc){
                swal({
                    title : 'Adjust Salary Components',
                    text : "Sum of your Salary Components can not be greater than CTC",
                    type : 'error',
                    animation : false,
                    confirmButtonClass : 'bg-theme',
                },function() {
                    $("#ctc").parents(".form-group").addClass('has-error');
                    $("#ctc").focus();
                });
            }
            else if ($("#ctc").val() > ctc) {
                swal({
                    title : 'Adjust CTC',
                    text : "CTC is greater than Sum of Salary Components",
                    type : 'warning',
                    confirmButtonClass : 'bg-theme',
                    animation : false,
                    confirmButtonText : 'Change'
                }, function() {
                    $("#ctc").parents(".form-group").addClass('has-error');
                    $("#ctc").focus();
                });
            }
            else {
                sendData(ctc);
            }
        });

        $("#ctc").change(function(event) {
            // event.preventDefault();
            var $val = $(this).val();
            var used = 0;

            if (!(1*$("#basic").val())){
                $("#basic").val(Math.round($val * 0.4));
            }
            used += 1*$("#basic").val();

            if (!(1*$("#hra").val())) {
                $("#hra").val(Math.round($val * 0.2));
            }
            used += 1*$("#hra").val();

            if (!(1*$("#conveyance").val())) {
                $("#conveyance").val(Math.max(Math.min(19200, $val-used), 0));
            }
            used += 1*$("#conveyance").val();

            if (!(1*$("#medicalAllowance").val())) {
                $("#medicalAllowance").val(Math.max(Math.min(15000, $val-used), 0));
            }
            used += 1*$("#medicalAllowance").val();

            if (!(1*$("#others").val())) 
                $("#others").val(Math.max(0, $val-used));

            $("#basic, #hra, #conveyance, #medicalAllowance, #others").trigger('change');
        });

        // handle conveyance max value
        $("#conveyance").change(function(event) {
            event.preventDefault();
            $(this).val( Math.min($(this).val(), 19200) );
        });
         // handle conveyance max value
        $("#medicalAllowance").change(function(event) {
            event.preventDefault();
            $(this).val( Math.min($(this).val(), 15000) );
        });

        // dropdown for extra fields
        $(".addMore").click(function(event) {
            event.preventDefault();
            $("#" + $(this).data('id')).parents(".hidden").removeClass('hidden');
            $(this).addClass('hidden');

            if ($(".addMore.hidden").length == $(".addMore").length) {$(".addMoreButton").hide()};
        });

        $(".edit").click(handleEdit);

        function handleEdit() {
            event.preventDefault();
            // first disable addCTC button below the table
            $("#addCTC").attr('disabled', true);

            // reset salary component and add more drop down 
            resetSalaryComponent();

            swal({
                title : 'Fetching Data',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 400
            });

            var id = $(this).data('id');
            $.get(location.href+'/'+id, function(data) {
                for(var fields in data) {
                    var el = $('#'+fields);
                    var parent = el.parents(".form-group");
                    // if data fill the fields
                    if (data[fields]){
                        el.val(data[fields]);
                        parent.removeClass('is-empty');
                    }
                    else{
                        el.val('');
                        parent.addClass('is-empty');
                    }
                    var superParent = parent.parent().parent();
                    if (data[fields] && superParent.hasClass('hidden')) {
                        superParent.removeClass('hidden');
                        $(".addMoreButton ul li a").filter(function(index) {
                            return ($(this).data('id') == fields);
                        }).addClass('hidden');// do not use hide(), need to count hidden element below
                    }
                }

                $('#salaryComponents #ctcId').val(id);
                $('#salaryComponents').removeClass('hidden');

                closeAlert();
            }).error(function() {
                displayError()
            });
        };

        $(".delete").click(handleDelete);

        function handleDelete() {
            event.preventDefault();

            resetSalaryComponent();

            var $salaryComponents = $("#salaryComponents");
            $salaryComponents.addClass('hidden'); // first hide the form then clear it
            // $salaryComponents.find('form')[0].reset(); // clear all field
            // $salaryComponents.find('.form-group').addClass('is-empty');

            swal({
                title : 'Deleting Data',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 400
            });

            var id = $(this).data('id');
            var $row = $(this).parents('tr');
            $.post(location.href + '/' + id, {id: id}, function(data, textStatus, xhr) {
                if (data.status == 200) {
                    $row.remove();
                    swal({
                        title : "Success !",
                        text : "CTC Deleted",
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });

                    if ($(".table tr").length > 1) {
                        $("#next").text("Next");
                    }
                    else {
                        $("#next").text('skip');
                    }
                }
                else {
                    displayError();
                }
            });
        };
    });
</script>