<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>


<body  style="margin-top:4em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">
                        <br>
                        <h2>  &nbsp;<i class="fa fa-inr"> </i> &nbsp; Payroll History</h2>
                        <hr>

                        <br>

                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td>February 2016</td>
                                        <td>01/03/2016 &nbsp; <a target="_blank" href="/monthslip/february"><b>Month slip</b></a></td>
                                    </tr>
                                    <tr>
                                        <td>March 2016</td>
                                        <td>01/04/2016 &nbsp; <a target="_blank" href="/monthslip/march"><b>Month slip</b></a></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <br>
                        <br>
                        <a  class="text-default" href="/tdsReport" target="_blank"> <h3><i class="fa fa-money"></i>  &nbsp; TDS Report</h3></a>
                        <br>
                        <a  class="text-default" href="/otherReport" target="_blank"> <h3><i class="fa fa-file-text-o"></i>  &nbsp; Other Report</h3></a>
                        <br>

                    </div>
                </div>
            </div>
        </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>
$(function() {

    // add active class to corresponding link in menu 
    $(".nav li.active").removeClass('active');
    $(".nav.nav-stacked li").filter(function(index) {
        return $(this).data('url') == 'businessIntelligence';
    }).addClass('active');

    $.material.init();

    $(".text-default").css({
        textDecoration: 'none',
        color: '#333'
    });
})
</script>
