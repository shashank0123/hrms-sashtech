<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>

<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">

                    <br>
                    <br>
                    <form enctype="multipart/form-data" method="POST" id="addCompanyLocation" class="form-horizontal">

                    <div class="col-md-8 col-sm-8">
                        <div class="form-group">
                            <label for="companyCode" class="col-md-4">Company code</label>

                            <div class="col-md-6">
                                <?php if(isset($company->companyCode))echo $company->companyCode; ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-4 col-xs 12">
                        <div class="extra">
                            <div id="preview">
                                <label for="companyLogo" 
                                <?php echo isset($companyAddress->logoUrl) ? 'style="background-image: url(\'https://Sashtechs.com/'.$companyAddress->logoUrl . '"' : ' '; ?>
                                >CHOOSE FILE</label>
                                <input type="file" accept="image/*" name="companyLogo" id="companyLogo" style="display:none;">
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <br>

                        <fieldset class="col-md-12 col-sm-12 col-xs-12">
                            <input type="hidden" name="type" id="type" value="headOffice"></input> 
                            
                            <legend>Add / Edit Company Location</legend>
                            <p>Company locations can be used in a variety of ways, including as your company government filing address, company government mailing address, or as a work location for your employees.</p>
                            
                            <br><br>
                            <div class="clearfix"></div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="address"><span>Address</span></label>
                                        <input class="form-control" id="address" name="address" type="text" style="cursor: auto;" placeholder="" value="<?php 
                                        if (isset($companyAddress->address)) {
                                            echo $companyAddress->address;
                                        } else {
                                            echo '';
                                        }
                                        ?>">
                                    </div>
                                </div>
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p> Company address. It can be anywhere in the India</p>
                                </div>
                            </div>
                            
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating ">
                                        <label class="control-label" for="propertyNo"><span>Property No. / Description</span></label>
                                        <input class="form-control" id="propertyNo" name="propertyNo" type="text" style="cursor: auto;" placeholder="" value="<?php 
                                        if (isset($companyAddress->propertyNo)) {
                                            echo $companyAddress->propertyNo;
                                        } else {
                                            echo '';
                                        }
                                        ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating ">
                                        <label class="control-label" for="street"><span>Street</span></label>
                                        <input class="form-control" id="street" name="street" type="text" style="cursor: auto;" placeholder="" value="<?php 
                                        if (isset($companyAddress->street)) {
                                            echo $companyAddress->street;
                                        } else {
                                            echo '';
                                        }
                                        ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating ">
                                        <label class="control-label" for="city"><span>City</span></label>
                                        <input class="form-control" id="city" name="city" type="text" style="cursor: auto;" placeholder="" value="<?php 
                                        if (isset($companyAddress->city)) {
                                            echo $companyAddress->city;
                                        } else {
                                            echo '';
                                        }
                                        ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating ">
                                        <label for="state" class="control-label">State</label>

                                        <select id="state" name="state" class="form-control">
                                            <option value="0">Select State</option>
                                            <option <?php if (isset($companyAddress->state)) {
                                                if ($companyAddress->state == 'AN') {echo "selected";}
                                            } ?> value="AN">Andaman and Nicobar Islands</option>
                                            <option <?php if (isset($companyAddress->state)) {
                                                if ($companyAddress->state == 'AP') {echo "selected";}
                                            } ?> value="AP">Andhra Pradesh</option>
                                            <option <?php if (isset($companyAddress->state)) {
                                                if ($companyAddress->state == 'AR') {echo "selected";}
                                            } ?> value="AR">Arunachal Pradesh</option>
                                            <option <?php if (isset($companyAddress->state)) {
                                                if ($companyAddress->state == 'AS') {echo "selected";}
                                            } ?> value="AS">Assam</option>
                                            <option <?php if (isset($companyAddress->state)) {
                                                if ($companyAddress->state == 'BR') {echo "selected";}
                                            } ?> value="BR">Bihar</option>
                                            <option <?php if (isset($companyAddress->state)) {
                                                if ($companyAddress->state == 'CH') {echo "selected";}
                                            } ?> value="CH">Chandigarh</option>
                                            <option <?php if (isset($companyAddress->state)) {
                                                if ($companyAddress->state == 'CT') {echo "selected";}
                                            } ?> value="CT">Chhattisgarh</option>
                                            <option <?php if (isset($companyAddress->state)) {
                                                if ($companyAddress->state == 'DN') {echo "selected";}
                                            } ?> value="DN">Dadra and Nagar Haveli</option>
                                            <option <?php if (isset($companyAddress->state)) {
                                                if ($companyAddress->state == 'DD') {echo "selected";}
                                            } ?> value="DD">Daman and Diu</option>
                                            <option <?php if (isset($companyAddress->state)) {
                                                if ($companyAddress->state == 'DL') {echo "selected";}
                                            } ?> value="DL">Delhi</option>
                                            <option <?php if (isset($companyAddress->state)) {
                                                if ($companyAddress->state == 'GA') {echo "selected";}
                                            } ?> value="GA">GOA</option>
                                            <option <?php if (isset($companyAddress->state)) {
                                                if ($companyAddress->state == 'GJ') {echo "selected";}
                                            } ?> value="GJ">Gujarat</option>
                                            <option <?php if (isset($companyAddress->state)) {
                                                if ($companyAddress->state == 'HR') {echo "selected";}
                                            } ?> value="HR">Haryana</option>
                                            <option <?php if (isset($companyAddress->state)) {
                                                if ($companyAddress->state == 'HP') {echo "selected";}
                                            } ?> value="HP">Himachal Pradesh</option>
                                            <option <?php if (isset($companyAddress->state)) {
                                                if ($companyAddress->state == 'JK') {echo "selected";}
                                            } ?> value="JK">Jammu and Kashmir</option>
                                            <option <?php if (isset($companyAddress->state)) {
                                                if ($companyAddress->state == 'JH') {echo "selected";}
                                            } ?> value="JH">Jharkhand</option>
                                            <option <?php if (isset($companyAddress->state)) {
                                                if ($companyAddress->state == 'KA') {echo "selected";}
                                            } ?> value="KA">Karnataka</option>
                                            <option <?php if (isset($companyAddress->state)) {
                                                if ($companyAddress->state == 'KL') {echo "selected";}
                                            } ?> value="KL">Kerala</option>
                                            <option <?php if (isset($companyAddress->state)) {
                                                if ($companyAddress->state == 'LD') {echo "selected";}
                                            } ?> value="LD">Lakshadweep</option>
                                            <option <?php if (isset($companyAddress->state)) {
                                                if ($companyAddress->state == 'MP') {echo "selected";}
                                            } ?> value="MP">Madhya Pradesh</option>
                                            <option <?php if (isset($companyAddress->state)) {
                                                if ($companyAddress->state == 'MH') {echo "selected";}
                                            } ?> value="MH">Maharashtra</option>
                                            <option <?php if (isset($companyAddress->state)) {
                                                if ($companyAddress->state == 'MN') {echo "selected";}
                                            } ?> value="MN">Manipur</option>
                                            <option <?php if (isset($companyAddress->state)) {
                                                if ($companyAddress->state == 'ML') {echo "selected";}
                                            } ?> value="ML">Meghalaya</option>
                                            <option <?php if (isset($companyAddress->state)) {
                                                if ($companyAddress->state == 'MZ') {echo "selected";}
                                            } ?> value="MZ">Mizoram</option>
                                            <option <?php if (isset($companyAddress->state)) {
                                                if ($companyAddress->state == 'NL') {echo "selected";}
                                            } ?> value="NL">Nagaland</option>
                                            <option <?php if (isset($companyAddress->state)) {
                                                if ($companyAddress->state == 'OR') {echo "selected";}
                                            } ?> value="OR">Odisha</option>
                                            <option <?php if (isset($companyAddress->state)) {
                                                if ($companyAddress->state == 'PY') {echo "selected";}
                                            } ?> value="PY">Puducherry</option>
                                            <option <?php if (isset($companyAddress->state)) {
                                                if ($companyAddress->state == 'PB') {echo "selected";}
                                            } ?> value="PB">Punjab</option>
                                            <option <?php if (isset($companyAddress->state)) {
                                                if ($companyAddress->state == 'RJ') {echo "selected";}
                                            } ?> value="RJ">Rajasthan</option>
                                            <option <?php if (isset($companyAddress->state)) {
                                                if ($companyAddress->state == 'SK') {echo "selected";}
                                            } ?> value="SK">Sikkim</option>
                                            <option <?php if (isset($companyAddress->state)) {
                                                if ($companyAddress->state == 'TN') {echo "selected";}
                                            } ?> value="TN">Tamil Nadu</option>
                                            <option <?php if (isset($companyAddress->state)) {
                                                if ($companyAddress->state == 'TG') {echo "selected";}
                                            } ?> value="TG">Telangana</option>
                                            <option <?php if (isset($companyAddress->state)) {
                                                if ($companyAddress->state == 'TR') {echo "selected";}
                                            } ?> value="TR">Tripura</option>
                                            <option <?php if (isset($companyAddress->state)) {
                                                if ($companyAddress->state == 'UT') {echo "selected";}
                                            } ?> value="UT">Uttarakhand</option>
                                            <option <?php if (isset($companyAddress->state)) {
                                                if ($companyAddress->state == 'UP') {echo "selected";}
                                            } ?> value="UP">Uttar Pradesh</option>
                                            <option <?php if (isset($companyAddress->state)) {
                                                if ($companyAddress->state == 'WB') {echo "selected";}
                                            } ?> value="WB">West Bengal</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating ">
                                        <label class="control-label" for="pin"><span>Pin</span></label>
                                        <input class="form-control" id="pin" name="pin" type="text" style="cursor: auto;" placeholder="" value="<?php 
                                        if (isset($companyAddress->pin)) {
                                            echo $companyAddress->pin;
                                        } else {
                                            echo '';
                                        }
                                        ?>">
                                    </div>
                                </div>
                            </div>
        
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <span class="btn btn-default btn-raised cancel" onclick="history.back();">Cancel</span>
                                        <span class="btn next btn-raised bg-theme">next</span>
                                        <span class="add btn btn-raised bg-theme pull-right">Add Branch office</span>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>

                    <style>
                    .extra{
                        border:none !important;
                        width: 12em;
                        margin: 0 auto;
                    }
                    #preview{
                        background: #fff;
                        margin: -10px;
                        position: relative;
                        height: 10em;
                        border: 1px solid #aaa;
                    }

                    #preview > label{
                        margin: 0 auto;
                        display: block;
                        width: 10em;
                        height: 100%;
                        text-align: center;
                        line-height: 9em;
                        background-size: contain;
                        background-repeat: no-repeat;
                        background-position: center;
                    }
                </style>
                    
                </div>
            </div>
        </div>
    </div>
    <br><br>
    <?php require_once('footer.php');?>

</body>
</html>

<script>

    var placeSearch, autocomplete;

    function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('address')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();
        var componentForm = {
            street_number: 'propertyNo',
            route: 'street',
            // locality: 'street',
            administrative_area_level_1: 'state',
            administrative_area_level_2: 'city',
            // country: 'country',
            postal_code: 'pin'
        };

        for (var component in componentForm) {
            document.getElementById(componentForm[component]).value = '';
            document.getElementById(componentForm[component]).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
                var val = place.address_components[i]['long_name'];
                if (addressType=="administrative_area_level_1") {val = place.address_components[i]['short_name'];}
                document.getElementById(componentForm[addressType]).value = val;
                $("#"+componentForm[addressType]).trigger('change')
            }
        }
    }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
    function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }


    $(function() {
        $.material.init();

        $('form.form-horizontal').submit(function(event) {
            event.preventDefault();

            return false;
        });

        function getFormData(target) {

            var formData = new FormData();
            // formData.append('companyCode', $("#companyCode").val());
            formData.append('address', $("#address").val());
            formData.append('propertyNo', $("#propertyNo").val() || ' ');
            formData.append('street', $("#street").val() || ' ');
            formData.append('city', $("#city").val());
            formData.append('state', $("#state").val());
            formData.append('pin', $("#pin").val());
            var file = $("#companyLogo")[0].files[0];
            if (file && file.size <= (500*1024) && (file.type == 'image/png' || file.type == 'image/jpeg'))
                formData.append('companyLogo', file);
            else
                formData.append('companyLogo', "");
            // formData.append('companyLogo', $("#companyLogo")[0].files[0]);
            formData.append('target', target);
            formData.append('type', $("#type").val());

            return formData;
        }

        $('.add, .next').unbind('click').on('click', function(event) {
            $(".help-block").remove();
            $(".has-error").removeClass('has-error');
            event.preventDefault();
            swal({
                title : 'Saving',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                width : 300
            });
            var formData = getFormData(($(event.target).hasClass('add') ? 'add' : 'next'));
            $.ajax({
                url: location.href,
                type: 'POST',
                data: formData,
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false  
            })
            .done(function(data) {
                // console.log(data)
                if (data.url) {
                    swal({
                        title : "Saved",
                        type : 'success',
                        width : 300,
                        animation : false,
                        confirmButtonClass : 'bg-theme',
                        timer : 1000,
                        showConfirmButton: false
                    });   
                    getAndInsert(data.url);
                    history.pushState(null, null, data.url);
                }
                else if(data.logoUrl) {
                    swal({
                        title : "Error !",
                        text : data.logoUrl[0],
                        type : 'error',
                        width : 400
                    });
                }
                else{
                    swal({
                        title : "Error !",
                        text : 'Try Again!',
                        type : 'error',
                        width : 400
                    });
                    for(var fields in data) {
                        $('#'+fields).parents('.form-group').addClass('has-error');
                        $('#'+fields).parent().append('<p class="help-block">'+ data[fields][0] +'</p>');
                    }
                }
            });
        });

        $("#address").focus(function() {
            geolocate();
        });

        (function () {
            if (typeof(google)=="undefined") {
                var script = document.createElement("script");
                script.type = "text/javascript";
                script.src = "https://maps.googleapis.com/maps/api/js?signed_in=false&libraries=places&components=country:IN";
                document.body.appendChild(script);
            }
            var done = function() {
                if (typeof(google)=="object") {
                    initAutocomplete();
                    clearTimeout(done);
                }
                else
                    setTimeout(done, 100);
            }
            done();
            // setTimeout(initAutocomplete, 500);
        })();

        $("#companyLogo").on('change', function(event) {

            if (event.target.files.length) {
                var file = event.target.files[0];
                if (file && file.size > (500*1024)) {// under 500kb file is allowed
                    swal({
                        title : "Error !",
                        text : 'File Size is more than 500 KB',
                        type : 'error',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });
                    return false;                    
                }

                if (file && file.type != 'image/png' && file.type != 'image/jpeg') {
                    swal({
                        title : "Error !",
                        text : 'only png and jpeg images are allowed',
                        type : 'error',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });
                    return false; 
                }
                
                $("#preview > label").css({
                    backgroundImage: 'url('+URL.createObjectURL(file)+')',
                    color: 'transparent',
                    'border' : '1px dotted #ddd'
                });
                $("#preview").css('border', 'none');
            }
            else{
                $("#preview > label").css({
                    backgroundImage : 'url()',
                    color : '#bdbdbd',
                    border : 'none'
                });
                $("#preview").css({
                    border: '1px solid #aaa',
                });
            }

        });
    });
</script>