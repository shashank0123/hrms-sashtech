<!DOCTYPE html>
<html>
<head>
    <title> Signup Sashtechs | Best Payroll & Health Insurance provider</title>

    <meta name='keywords' content='Payroll, Employee Health Insurance, Group Health Insurance, Corporate Employee Health Insurance, Employee Health Insurance for business India, Third Party Administrator (TPA) online' /> 

    <meta name='description' content='Payroll, Employee Health Insurance - We provide comparison for group health insurance coverage online. Get health insurance quotes from us.' />

    <?php require_once('links.php');?>
</head>

<style>
    .panel{
        width:320px;
        margin: 0 auto;
        padding: 5px;
    }
</style>

<body  style="margin-top:4em;">

        <div class="panel panel-default">
            <div class="panel-body">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <form class="form-horizontal" autocomplete="off" action="" method="POST">
                        <input style="display:none">
                        <input type="password" style="display:none">
                        <fieldset>
                            <legend><a id="logo" class="fa btn btn-block" href="javascript:void(0)" >Sashtechs <div></div></a></legend>
                            
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating ">
                                        <label class="control-label" for="EmployerName"><span>Name</span></label>
                                        <input class="form-control" id="EmployerName" type="text" name="EmployerName" required style="cursor: auto;" autocomplete="off">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating ">
                                        <label class="control-label" for="supportmail"><span>Email</span></label>
                                        <input class="form-control" id="supportmail" type="email" name="supportmail" required style="cursor: auto;" autocomplete="off">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating ">
                                        <label class="control-label" for="password"><span>Password</span></label>
                                        <input class="form-control" id="password" type="password" name="password" required style="cursor: auto;" autocomplete="off">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating ">
                                        <label class="control-label" for="confpassword"><span>Confirm Password</span></label>
                                        <input class="form-control" id="confpassword" type="password" required name="confpassword" style="cursor: auto;" autocomplete="off">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group label-floating ">
                                        <label class="control-label" for="companyName"><span>Company Name</span></label>
                                        <input class="form-control" id="companyName" type="text" required name="companyName" style="cursor: auto;" autocomplete="off">
                                    </div>
                                </div>
                            </div>

                            <br><br>
                            <div class="clearfix"></div>
                            <br>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <input type="submit" class="btn bg-theme btn-block btn-raised submit" value="Sign Up">
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </fieldset>
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>

                    </div>
                </div>
            </div>
        </div>

    <br><br>
</body>
</html>

<script>
    $(function() {

        $.material.init();

        $("#supportmail").on('keyup change', function(event) {
            event.preventDefault();
            var val = $(this).val().split('@');
            if (val.length<2) return false;
            val = val.pop();
            $("#companyName").val(val.split('.')[0].toUpperCase());
            $("#companyName").trigger('change');
        });

        function passwordVerification() {
            // remove all error messages
            $(".help-block").remove();
            $(".has-error").removeClass('has-error');
            
            var response = true;
            $("#password, #confpassword").each(function(index, el) {
                if (!$(this).val()) {
                    $(this).parents(".label-floating").addClass('has-error');
                    $(this).parent().append('<p class="help-block"> password required</p>');
                    response = false;
                }
                else if ($(this).val().length < 8) {
                    $(this).parents(".label-floating").addClass('has-error');
                    $(this).parent().append('<p class="help-block"> password is less than 8 characters</p>');
                    response = false;
                }
            });

            if (!response) return response;

            if ($("#password").val()!=$("#confpassword").val()) {
                $("#confpassword").parents(".label-floating").addClass('has-error');
                $("#confpassword").parent().append('<p class="help-block"> passwords did not match</p>');                
                return false
            };

            return true;
        }

        function displayError() {
            swal({
                title : "Error !",
                text : 'Try Again or Fix your Error!',
                type : 'error',
                confirmButtonClass : 'bg-theme',
                animation : false,
                width : 400
            });
        };

        function closeAlert() {
            $(".sweet-alert button").first().trigger('click');
        }

        $("form.form-horizontal").submit(function(event) {

            event.preventDefault();

            if (!passwordVerification()) {
                displayError();
                return false;
            }
            // remove all error messages
            $(".help-block").remove();
            $(".has-error").removeClass('has-error');
            
            swal({
                title : 'Please Wait',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 300
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: $('.form-horizontal').serialize(), 
            })
            .done(function(data) {
                if (data.status == 200) {
                    swal({
                        title : "Welcome to Sashtechs!",
                        text : 'Please verify your email now.',
                        type : 'success',
                        showConfirmButton : false,
                        animation : false,
                        width : 300,
                        timer: 1500
                    });
                    location.assign(data.url)   
                }
                else if(data.status == 208) {  //  email already Exist
                   closeAlert();
                   var $email = $("#supportmail");
                   $email.parent().append('<p class="help-block">'+ data.message +'</p>');
                   $email.focus();
                   $email.parents('.form-group').addClass('has-error');
                }
                else if(data.status == 209) {  //  email already Exist
                   closeAlert();
                   var $companyName = $("#companyName");
                   $companyName.parent().append('<p class="help-block">'+ data.message +'</p>');
                   $companyName.focus();
                   $companyName.parents('.form-group').addClass('has-error');
                }
                else if(data.status == 406) {  //  email already Exist
                   closeAlert();
                   var $email = $("#supportmail");
                   $email.parent().append('<p class="help-block">'+ data.message +'</p>');
                   $email.focus();
                   $email.parents('.form-group').addClass('has-error');
                }
                else{  // Validation Error
                    displayError();
                    // show Errors
                    for(var fields in data) {
                        $('#'+fields).parents('.form-group').addClass('has-error');
                        $('#'+fields).parent().append('<p class="help-block">'+ data[fields][0] +'</p>');
                    }
                }
            })
            .error(function() {
                displayError();
            });
        });
    });
</script>
