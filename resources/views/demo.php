<!DOCTYPE html>
<html>
<head>
    <title>Financial and Payroll Accounting Software </title>
    <meta name="description" content="Manage  payroll with a single sign-on with google gmail, mobile apps, smartphone app, payroll app, run payroll app, Payroll App iPhone, Android with  seamless integration with QuickBook , SAP & Tally" />

    <meta name="keywords" content="Manage  payroll with a single sign-on with google gmail, mobile apps, smartphone app, payroll app, run payroll app, Payroll App iPhone, Android with  seamless integration with QuickBook & Tally" />

    <!--Import Google Icon Font-->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">

    <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet"></link>


    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Material Design -->
    <link href="css/bootstrap-material-design.css" rel="stylesheet">
    <link href="css/ripples.min.css" rel="stylesheet">
    <link href="css/roundslider.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">

    <script src="js/jquery-1.12.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ripples.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/roundslider.min.js"></script>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
</head>


<body>
<?php require_once("staticNavBar.php");?>

<div class="clearfix"> </div>

<section class="parallax" style="background-image: url(images/try.jpg);">
    <div class="panel panel-default" style="  ">
        <div class="panel-body">
            <div class="text-center">
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <h5><b>TRY Sashtechs</b></h5>
                <h3 class="text-uppercase">Start your one month free trial </h3>
                <br>

                <div class="panel text-left" style="width:320px;margin: 0 auto;padding:15px;">
                    <?php require 'demoForm.php'; ?>
                </div>
                <br>
                    <div class="clearfix"></div>
                    <br><br>
                    <br><br>
            </div>
        </div>
    </div>
</section>

<div class="container-fluid">
    <div class="panel-body text-center">
        <br>
        <h3>Watch, ask and learn </h3>
        <br>
        <p>Get a demo of Sashtechs tailored to your use case. One of our friendly account executives will answer all your questions there and then.</p>
        <br>
        <p>Awesome payroll is here. We’re so confident you’ll love sashtechs that your first month on us. Setup is free too!</p>
    </div>
</div>


<div class="clearfix"> </div>
<br><br>


<?php require_once('staticFooter.php');?>

</body>
</html>

<script>

    $(function() {
        $.material.init();
    });
</script>
