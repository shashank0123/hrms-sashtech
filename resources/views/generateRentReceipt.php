<!DOCTYPE html>
<html>
<head>
     <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <!--Import Google Icon Font-->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">

    <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet"></link>


    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Material Design -->
    <link href="css/bootstrap-material-design.css" rel="stylesheet">
    <link href="css/ripples.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <link href="css/bootstrap-material-datetimepicker.css" rel="stylesheet">

    <script src="/js/jquery-1.12.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ripples.min.js"></script>
    <script src="js/material.min.js"></script>

    <script src="js/moment-with-locales.min.js"></script>
    <script src="js/bootstrap-material-datetimepicker.js"></script>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <style>
        .face {
            border: 1px solid #ddd;
            box-shadow: 0 0 3px #ddd;
            padding: 15px;
        }

        .help-block.text-muted {
            color: #777;
            margin-top: 0px;
        }

        #pan {
            text-transform: uppercase;
        }

        .report {
            border: 1px solid #ddd;
            box-shadow: 0 1px 3px #ddd;
            padding: 10px;
            margin-top: 15px;
        }
    </style>
</head>

<body>
<?php require_once("staticNavBar.php");?>

<header style="padding: 4em;background: #cf3d3d;"></header>

<section>
    <br>
    <br>
    <br>
    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="text-center" style="font-weight: 400">Enter your Information below generate your Rent Receipt</h3>
            </div>
            <div class="panel-body">
                <br>
                <br>
                <form action="" class="form form-horizontal col-xs-12" method="POST">
                    <div class="face-container">

                        <div class="face col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-12" id="face1">
                            <legend>Property / Rent Details</legend>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group label-floating">
                                    <label for="rent" class="control-label">Monthly Rent</label>
                                    <input class="form-control" id="rent" name="rent" type="number" required style="cursor: auto;">
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group label-floating">
                                    <label for="address" class="control-label">Address</label>
                                    <input class="form-control" id="address" required name="address" type="text" style="cursor: auto;">
                                </div>
                            </div>

                            <div class="clearix"></div>
                            <h4>Rental Period</h4>

                            <div class="col-md-5 col-sm-5 col-xs-12">
                                <div class="form-group label-floating">
                                    <label for="from" class="control-label">From</label>
                                    <input class="form-control" id="from" name="from" type="text" required style="cursor: auto;">
                                </div>
                            </div>

                            <div class="col-md-5 col-md-offset-1 col-sm-5 col-sm-offset-1 col-xs-12">
                                <div class="form-group label-floating">
                                    <label for="to" class="control-label">To</label>
                                    <input class="form-control" id="to" name="to" type="text" required style="cursor: auto;">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <br>
                            <br>
                        </div>

                        <div class="clearfix"></div>
                        <br>
                        <br>

                        <div class="face col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-12" id="face2">
                            <legend>Personal Details</legend>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group label-floating">
                                    <label for="nameOfTenant" class="control-label">Name of Tenant</label>
                                    <input class="form-control" id="nameOfTenant" name="nameOfTenant" required type="text" style="cursor: auto;">
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group label-floating">
                                    <label for="nameLandlord" class="control-label">Name of House owner</label>
                                    <input class="form-control" id="nameLandlord" name="nameLandlord" type="text" required style="cursor: auto;">
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group label-floating">
                                    <label for="pan" class="control-label">PAN of Landlord</label>
                                    <input class="form-control" id="pan" name="pan" type="text" pattern="([a-zA-Z]){5}([0-9]){4}([a-zA-Z])" style="cursor: auto;">
                                    <p class="help-block text-muted">Example : ABCDE1234F</p>
                                </div>
                            </div>
                            
                            <div class="clearfix"></div>
                            <br>
                            <br>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <br>

                    <div class="col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-12">
                        <div class="col-md-6 col-md-offset-4 col-sm-6 col-sm-offset-1 col-xs-12">
                            <input type="submit" value="generate" class="btn btn-raised bg-theme">
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <br>
                </form>
                <br>

                <div id="result">
                    
                </div>
                <div class="clearfix"></div>
                <br>
                <br>
            </div>
        </div>
    </div>
    <br>
    <br>
    <br>
</section>

<?php require_once('staticFooter.php');?>
</body>
</html>
<link href="css/nouislider.pips.css" rel="stylesheet">
<script src="js/nouislider.min.js"></script>

<script>
    // $(function() {

        $.material.init();

        var MONTH = ['January','February','March','April','May','June','July','August','September','October','November','December'];
        var SHORTMONTH = ['Jan','Feb','Mar','Apr','May','June','July','Aug','Sept','Oct','Nov','Dec'];

        $("#to").attr('disabled', true);
        $('#from').bootstrapMaterialDatePicker({ weekStart : 0, time: false }).on('dateSelected', function(event) {
            $('#'+$(this).data('dtp')).find('.dtp-btn-ok').trigger('click');
        })
        .on('change', function(e, date) {
            $("#to").attr('disabled', false);
            $('#to').bootstrapMaterialDatePicker({ weekStart : 0, time: false , minDate : date}).on('dateSelected', function(event) {
                $('#'+$(this).data('dtp')).find('.dtp-btn-ok').trigger('click');
            });
        });

        var $html = $('<div class="clearfix"><br>'+
                '<br><div class="report col-md-8 col-md-offset-2 col-sm-12 col-sx-12">'+
                    '<div class="col-md-7 col-sm-7 col-xs-12">'+
                        '<span class="h3">RENT RECIEPT </span>'+
                        '<span class="month"> April 2015</span>'+
                    '</div>'+
                    '<div class="col-md-5 col-sm-5 col-xs-12 text-right">'+
                       'Receipt No : <span class="recieptNo"></span><br>'+
                        'Date : <span class="date"></span>'+
                    '</div>'+
                    '<div class="clearfix"></div>'+
                    '<br>'+
                    '<p class="col-md-12 col-sm-12 col-xs-12">'+
                        'Recieved sum of <i class="fa fa-inr"></i> <span class="amount"></span> from <span class="nameOfTenant"></span> , towards the rent of property located at <span class="address"></span> for the period from <span class="from">01/02/2016</span> to <span class="to">21/9/2016</span>.'+
                    '</p>'+
                    '<div class="clearfix"></div>'+
                    '<br>'+
                    '<div class="col-md-12 col-sm-12 col-xs-12">'+
                        '<h5><span class="nameLandlord">SDFFSF</span></h5>'+
                        '<h6>PAN : <span class="pan">abcde1234f</span></h6>'+
                    '</div>'+
                    '<div class="clearfix"></div>'+
                    '<br>'+
                '</div>');


        $(".form.form-horizontal").submit(function(event) {
            event.preventDefault();
            console.log("generating");

            var rent = $("#rent").val();
            var address = $("#address").val();
            var from = $("#from").val().split('-');
            var to = $("#to").val().split('-');
            var nameOfTenant = $("#nameOfTenant").val();
            var nameLandlord = $("#nameLandlord").val();
            var pan = $("#pan").val();

            var noOfMonth = 1*to[1] - 1*from[1];
            if (noOfMonth) {
                for (var i = 0; i < noOfMonth; i++) {
                    var currentMonth = 1*from[1]+i;

                    var report = $html.clone();

                    report.find('.month').text(MONTH[currentMonth - 1] + ' ' + from[0]);
                    report.find('.recieptNo').text(i+1);

                    var noOfDays = new Date(from[0], currentMonth, 0).getDate();

                    report.find('.date').text(noOfDays + ' ' + SHORTMONTH[currentMonth - 1] + ' ' + from[0]);
                    report.find('.amount').text(rent);
                    report.find('.nameOfTenant').text(nameOfTenant);
                    report.find('.address').text(address);
                    report.find('.from').text(1 + ' ' + SHORTMONTH[currentMonth - 1] + ' ' + from[0]);
                    report.find('.to').text(noOfDays + ' ' + SHORTMONTH[currentMonth - 1] + ' ' + from[0]);
                    report.find('.nameLandlord').text(nameLandlord);
                    report.find('.pan').text(pan);

                    $("#result").append(report);
                }
            }
            else {
                $('#result').children().remove();
            }
        });
    // });
</script>
