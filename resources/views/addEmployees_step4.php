<!DOCTYPE html>
<html>
<head>
    <title>Sashtechs - WorkPlace, Workspace, Payroll, ERP For HR and Financial Management</title>
    <?php require_once('links.php');?>
</head>

<body  style="margin-top:2em;">
            <?php require_once('Menu.php');?>
                <div class="main panel panel-default col-md-9 col-sm-9">
                    <div class="panel-body" style="min-height:36em;">

                <div class="breadcrumb flat text-center row">
                    <?php if(session()->get('plan') == 'payroll'): ?>
                        <span class="col-xs-6 col-sm-3 col-md-3 hidden-xs">The Basics</span>
                        <span class="col-xs-6 col-sm-3 col-md-3 hidden-xs">Personal Details</span>
                        <span class="col-xs-6 col-sm-3 col-md-3 hidden-xs">Tax Details</span>
                        <span class="col-xs-12 col-sm-3 col-md-3 active">Payment Details</span>
                    <?php elseif((session()->get('plan') == 'benefits')or(session()->get('plan') == 'manager')): ?>
                        <span class="col-xs-6 col-sm-2 col-md-2 hidden-xs">Basics</span>
                        <span class="col-xs-6 col-sm-2 col-md-2 hidden-xs">Personal</span>
                        <span class="col-xs-6 col-sm-3 col-md-3 hidden-xs">Family</span>
                        <span class="col-xs-6 col-sm-2 col-md-2 hidden-xs">Tax</span>
                        <span class="col-xs-12 col-sm-3 col-md-3 active">Payment Details</span>
                    <?php endif; ?>
                </div>


                <br>
                <br>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <form class="form-horizontal" method="POST" action="">
                        <fieldset>
                            <legend>Payment Method</legend>
                            
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                    <div class="form-group label-floating">
                                        <label for="modeOfPayment1" class="control-label">Mode Of Payment</label>
                                        <select id="modeOfPayment1" name="modeOfPayment1" class="modeOfPayment form-control">
                                            <option value="directDeposit">Direct Deposit</option>
                                            <option value="cheque">Cash / Cheque</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                    <p> Be sure to cross check these values. We will verify the account by completing a test transaction</p>
                                </div>
                            </div>

                            <div id="directDeposit1" class="paymentForm" >
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="bankName1"><span>Name of Bank</span></label>
                                            <input class="form-control bankName" name="bankName1" id="bankName1"  type="text" style="cursor: auto;">
                                        </div>
                                    </div>
                                    <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                        <p>Name of the bank where you have account</p>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="branch1"><span>Branch</span></label>
                                            <input class="form-control branch" name="branch1" id="branch1" type="text" style="cursor: auto;">
                                        </div>
                                    </div>
                                    <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                        <p>Branch of the bank where you have account</p>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="accountHolder1"><span>Name as in Bank Account</span></label>
                                            <input class="form-control accountHolder" name="accountHolder1" id="accountHolder1" type="text" style="cursor: auto;">
                                        </div>
                                    </div>
                                    <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                        <p>This name will be displayed on the employee paycheck to represent the employee bank account</p>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="accountNumber1"><span>Account Number</span></label>
                                            <input class="form-control accountNumber " id="accountNumber1" name="accountNumber1" type="text" style="cursor: auto;">
                                        </div>
                                    </div>
                                    <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                        <p>Account number of Bank account</p>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Type of Account</label>

                                        <div class="col-md-8">
                                            <div class="radio radio-primary">
                                                <label>
                                                    <input type="radio" class="accountType" name="accountType1" id="accountType1" value="saving" checked>
                                                        Saving
                                                </label>
                                            </div>
                                            <div class="radio radio-primary">
                                                <label>
                                                    <input type="radio" name="accountType1" class="accountType" id="accountType2" value="current">
                                                    Current
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="ifsc1"><span>IFSC Code</span></label>
                                            <input class="form-control ifsc" name="ifsc1" id="ifsc1" type="text" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="sharePercent1"><span>percent (%) share of net Payment</span></label>
                                            <input class="form-control sharePercent" name="sharePercent1" id="sharePercent1" type="number" min="0" max="100" value="100" style="cursor: auto;" readonly>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </div>

                            <div id="cheque" class="hidden ">
                                <div class="clearfix"></div>
                                <br>
                                <br>
                                <br>
                                    <div class="col-md-8 col-sm-12 col-sx-12">
                                        <p class="lead">You have selected cash/cheque as payment option, <br> Consult your employer as it will be provided to you personally.</p>
                                    </div>
                                <br>
                            </div>
                            <br>
                            <br>

                            

                            <div id="directDeposit2" class="paymentForm hidden">
                                <legend>Direct Deposit 2</legend>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="bankName2"><span>Name of Bank</span></label>
                                            <input class="form-control bankName"  name="bankName2" id="bankName2" type="text" style="cursor: auto;">
                                        </div>
                                    </div>
                                    <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                        <p>Name of the bank where you have account</p>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="branch2"><span>Branch</span></label>
                                            <input class="form-control branch" name="branch2" id="branch2" type="text" style="cursor: auto;">
                                        </div>
                                    </div>
                                    <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                        <p>Branch of the bank where you have account</p>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="accountHolder2"><span>Name as in Bank Account</span></label>
                                            <input class="form-control accountHolder" name="accountHolder2" id="accountHolder2" type="text" style="cursor: auto;">
                                        </div>
                                    </div>
                                    <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                        <p>This name will be displayed on the employee paycheck to represent the employee bank account</p>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12" data-hint="true">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="accountNumber2"><span>Account Number</span></label>
                                            <input class="form-control accountNumber" id="accountNumber2" name="accountNumber2" type="text" style="cursor: auto;">
                                        </div>
                                    </div>
                                    <div class="hint-text col-md-4 text-muted hidden-sm hidden-xs">
                                        <p>Account number of Bank account</p>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Type of Account</label>

                                        <div class="col-md-8">
                                            <div class="radio radio-primary">
                                                <label>
                                                    <input type="radio" class="accountType" name="accountType2" id="accountType3" value="saving" checked>
                                                        Saving
                                                </label>
                                            </div>
                                            <div class="radio radio-primary">
                                                <label>
                                                    <input type="radio" class="accountType" name="accountType2" id="accountType4" value="current">
                                                    Current
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="ifsc2"><span>IFSC Code</span></label>
                                            <input class="form-control ifsc" name="ifsc2" id="ifsc2" type="text" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="sharePercent2"><span>percent (%) share of net Payment</span></label>
                                            <input class="form-control sharePercent" name="sharePercent2" id="sharePercent2" type="number" min="0" max="100" value="0" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <br>
                            </div>

<!--                             <div id="paytm" class="paymentForm hidden">
                                <br>
                                <legend>Paytm </legend>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label" for="paytmId"><span>Account Id / User Id</span></label>
                                            <input class="form-control paytmId" id="paytmId" name="paytmId" type="text" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="paytmSharePercent"><span>percent (%) share of net Payment</span></label>
                                            <input class="form-control paytmSharePercent" id="paytmSharePercent" name="paytmSharePercent" type="number" min="0" max="100" value="0" style="cursor: auto;">
                                        </div>
                                    </div>
                                </div>
                            </div>

 -->                            <div class=" col-md-8 col-sm-12 col-xs-12 text-right dropdown">
                                <a href="javascript:void(0)" data-target="#" class="dropdown-toggle addAnother pull-right" data-toggle="dropdown"><i class="fa fa-plus"></i> &nbsp;add Another Account 
                                <b class="caret"></b></a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a class="h5" id="selectDeposit" data-id="directDeposit2" href="javascript:void(0)">Direct Deposit</a></li>
                                    <!-- <li><a class="h5" id="selectPaytm" data-id="paytm" href="javascript:void(0)">Paytm</a></li> -->
                                </ul>
                            </div>

                            <div class="col-md-8 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="col-md-8 col-sm-8 col-xs-8 col-md-offset-4 col-sm-offset-4 col-xs-offset-4">
                                        <input type="submit" class="btn next bg-theme btn-raised" value="Save">
                                    </div>
                                </div>
                            </div>

                        </fieldset>
                    </form>
                </div>
                </div>
            </div>
        </div>
    </div>

    <br><br>
    <?php require_once('footer.php');?>
    

</body>
</html>

<script>
    $(function() {

        $.material.init();

        function displayError() {
            swal({
                title : "Error !",
                text : 'Try Again!',
                type : 'error',
                confirmButtonClass : 'bg-theme',
                animation : false,
                width : 400
            });
        };

        function closeAlert() {
            $(".sweet-alert button").first().trigger('click');
        }

        $("#modeOfPayment1").change(function(event) {
            var val = $(this).val();
            if (val == 'cheque') {
                $("#cheque").removeClass('hidden');
                $("#directDeposit1 ,#directDeposit2, #paytm").find('input').attr('disabled', true);
                $("#directDeposit1, #directDeposit2, #paytm").addClass('hidden');
                $(".addAnother").hide();
            }
            else {
                $("#cheque").addClass('hidden');
                $("#directDeposit1").find('input').attr('disabled', false);
                // $("#sharePercent1").val(100);
                $("#directDeposit1").removeClass('hidden');
                $("#directDeposit1 input").trigger('change');
                $('.addAnother').siblings().children('.hidden').removeClass('hidden');
                $(".addAnother").show();
            }
        });

        $("#directDeposit2, #paytm").find('input').attr('disabled', true);

        $("#selectDeposit, #selectPaytm").click(function(event) {
            var $item = $('#'+$(this).data('id')); // form id
            $item.removeClass('hidden');
            $item.find('input').attr('disabled', false);
            $(this).parent().addClass('hidden');
            if ($('.addAnother').siblings().children('.hidden').length == 1) { // make it == 2 if paytm is added
                $(".addAnother").hide();
            }
        });

        $("#sharePercent2, #paytmSharePercent").change(function(event) {
            var sharePercent1 = $("#sharePercent1").val()*1;
            var sharePercent2 = $("#sharePercent2").val()*1;
            var paytmSharePercent = $("#paytmSharePercent").val()*1 || 0;
            val = $(this).val()*1;

            var newSharePercent1 = 100 - (sharePercent2 + paytmSharePercent);
            if (newSharePercent1 < 1) {
                $("#sharePercent1").val(1);
                $(this).val(99 - (sharePercent2 + paytmSharePercent - val));
            }
            else
                $("#sharePercent1").val(newSharePercent1);
        });

        $("form.form-horizontal").submit(function(event) {

            event.preventDefault();

            // remove all error messages
            $(".help-block").remove();
            $(".has-error").removeClass('has-error');
            
            swal({
                title : 'Saving',
                html:'<div class="center-block" style="width:42px;position:relative;"><i class="fa-3x fa fa-spinner fa-pulse"></i></div>',
                allowOutsideClick : false,
                showConfirmButton : false,
                animation : false,
                width : 400
            });

            $.ajax({
                url: location.href,
                type: 'POST',
                data: $('.main form').serialize(), 
            })
            .done(function(data) {
                if (data.status == 200) {
                    swal({
                        title : "Success !",
                        text : data.message,
                        type : 'success',
                        confirmButtonClass : 'bg-theme',
                        animation : false,
                        width : 400
                    });   
                    if (data.url) 
                    {
                        swal({
                            title : 'Another Employee',
                            text : "Do you want to add another employee?",
                            type : 'info',
                            confirmButtonClass : 'bg-theme',
                            animation : false,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            cancelButtonText: 'No',
                            cancelButtonClass: 'bg-theme',
                            closeOnConfirm: true,
                            closeOnCancel: true
                        },
                        function(isConfirm) 
                        {
                            if (isConfirm === true) 
                            {
                                getAndInsert(data.url1);
                                history.pushState(null, null, data.url1);
                            }
                            else 
                            {
                                getAndInsert(data.url);
                                history.pushState(null, null, data.url);
                            }
                        })
                    }
                }
                else
                {  // Validation Error
                    displayError();
                    // show Errors
                    for (var i = 0; i < data.length; i++) {
                        var currentData= data[i];
                        var $currentForm=$('.paymentForm:eq('+i+')');
                        for(var fields in currentData) {
                            $currentForm.find('.'+fields).parents('.form-group').addClass('has-error');
                            $currentForm.find('.'+fields).parent().append('<p class="help-block">'+ currentData[fields][0] +'</p>');
                        }
                    }
                }
            })
            .error(function() {
                displayError();
            });
        });

        $(".main .form-horizontal input").trigger('change');
    })
</script>