<!DOCTYPE html>
<html>
<head>
    <title>Time Tracking, Online Timesheets , Mobile Time Clock</title>

    <meta name="keywords" content="expense, report, reports, online, expense report, template, expenses, small business, expense reimbursement, online expense reporting, time tracker alternative india, reimbursement reporting, reimbursement reports, reimbursement online, reimbursement tracker, mobile app, android & ios phone expense">

    <meta name="description" content="Import expenses directly from mobile app android or ios phone to create free expense reports quickly.  Approve reports online and reimburse directly to a checking account with one click.">


    <!--Import Google Icon Font-->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">

    <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet"></link>


    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Material Design -->
    <link href="css/bootstrap-material-design.css" rel="stylesheet">
    <link href="css/ripples.min.css" rel="stylesheet">
    <link href="css/roundslider.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">

    <script src="/js/jquery-1.12.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ripples.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/roundslider.min.js"></script>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    
    <style>
        .feature {
            margin: 1em;
            border: 1px solid #ddd;
            padding: 1em;
            height: 15em;
            box-shadow: 0 0 3px #ddd;
        }
    </style>

</head>

<body>
<?php require_once("staticNavBar.php");?>

<!-- ================  Header  ===================== -->

<header class="parallax" style="background-image: url(images/expense.jpg);background-position: center">
    <div class="panel panel-default" style="  ">
        <div class="panel-body">
            <div class="text-center">
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <h5>EXPENSE</h5>
                <h2>Expense reporting which don't suck!</h2>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
            </div>
        </div>
    </div>
</header>

<section style="background:#fff;margin-top: -20px">
    <div class="container-fluid">
        <div class="row">
            <div class="container">
                <br>
                <br>
                <h2 class="text-center text-muted">Expense reporting that rocks</h2>
                <br>

                <div class="clearfix"></div>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="center-block feature text-center">
                        <i class="fa fa-star fa-3x"></i>
                        <br>
                        <h4>SmartExpense</h4>
                        <br>
                        <p>SmartExpense inputs the receipt information, then matches the receipt to the expense, eliminating any manual entry</p>
                        <br>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="center-block feature text-center">
                        <i class="fa fa-mobile fa-3x"></i>
                        <br>
                        <h4>Mobile</h4>
                        <br>
                        <p>Snap pictures of receipts and create and submit expense reports from your phone</p>
                        <br>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="center-block feature text-center">
                        <i class="fa fa-heart fa-3x"></i>
                        <br>
                        <h4>Administration</h4>
                        <br>
                        <p>Built for Payroll admins, loved by employees</p>
                        <br>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="center-block feature text-center">
                        <i class="fa fa-file-text fa-3x"></i>
                        <br>
                        <h4>Go Paperless</h4>
                        <br>
                        <p>Unlimited receipt upload and storage for employees.</p>
                        <br>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="center-block feature text-center">
                        <i class="fa fa-smile-o fa-3x"></i>
                        <br>
                        <h4>Simplified approval process.</h4>
                        <br>
                        <p>Make your work easy with instant approvals and reminders for pending expense reports.</p>
                        <br>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="center-block feature text-center">
                        <i class="fa fa-line-chart fa-3x"></i>
                        <br>
                        <h4>Business Intelligence</h4>
                        <br>
                        <p>Manage expenses to make your business more efficient. View reports based on expense category, employees, team and more.</p>
                        <br>
                    </div>
                </div>

                <div class="clearfix"></div>
                <br><br><br>

            </div>
            <br><br>
        </div>
    </div>
</section>

<div class="clearfix"> </div>

<section class="parallax" style="background-image: url(images/try.jpg);">
    <div class="panel panel-default" style="  ">
        <div class="panel-body">
            <div class="text-center">
                <br>
                <br>
                <br>
                <br>
                <h5><b>TRY Sashtechs</b></h5>
                <h3 class="text-uppercase">Start your one month free trial </h3>
                <br>

                <div class="panel text-left" id="panel-text-left">
                    <?php require 'demoForm.php'; ?>
                </div>
                <br>
                    <div class="clearfix"></div>
                    <br><br>
                    <br><br>
            </div>
        </div>
    </div>
</section>

<?php require_once('staticFooter.php');?>

</body>
</html>

<script>
    $(function() {
        $.material.init();
    });
</script>
