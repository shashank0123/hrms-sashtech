<!DOCTYPE html>
<html>
<head>
     <title>Customer Stories for Health Insurance Policy & Payroll</title>
 
    <meta name='description' content='Sashtechs take Cares of Your People/ Employees With Our Online Payroll, Health Benefits, and Contractors Compensation Services.' />

    <meta name='keywords' content='small businesses payroll, Outsource payroll India, Manage Payroll, Startup Payroll, Startup formation,Company Incorporation, Online Company Incorporation, LLP Registration, Service Tax Registration, Private Limited Company, Pvt Ltd Company, Private Limited Company India, LLP Incorporation, Start a business, India Business Incorporation, Business Registration, Online Company Registration, MSME Registration, Digital Signature' /> 

    <!--Import Google Icon Font-->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">

    <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet"></link>


    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Material Design -->
    <link href="css/bootstrap-material-design.css" rel="stylesheet">
    <link href="css/ripples.min.css" rel="stylesheet">
    <link href="css/roundslider.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">

    <script src="/js/jquery-1.12.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ripples.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/roundslider.min.js"></script>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    <link rel="stylesheet" type="text/css" href="css/slick.css"/>
    <link rel="stylesheet" type="text/css" href="css/slick-theme.css"/>
    <style>
        .carousel-control .fa-chevron-left,.carousel-control .fa-chevron-right{
            position: absolute;
            top: 45%;
            right: 50%;
            font-size: 25px;
        }

        .img-responsive{
            width:100%;
            max-height: 45em;
        }

        .customers .panel{
            margin: 20px;
        }
        .customers .panel .card{
            padding: 15px;
            height: 30em;
            box-shadow: 0 0 0px;
        }
        .customers .panel .card img.face{
            width: 100%;
            height: 15em;
        }
        .customers .panel .card img.logo{
            padding: 10px 0;
            height: 5em;
        }

        @media screen and (max-width: 420px) {
            .customers .panel{
                margin: 5px;
            }

            .customers .panel .card h6{display: none;}
        }
        @media screen and (max-width: 768px) {
            .customers .panel .card img.face{
                width: initial;
                max-width: 100%;
            }
        }
        .tweet-box{
            height: 25em;
            padding-top: 2em;
        }
        .tweet-box .slick-slide{
            padding: 2em;
            margin: 1em;
        }
    </style>
</head>

<body>
<?php require_once("staticNavBar.php");?>

<!-- ================  Header  ===================== -->

<header class="parallax" style="background-image: url(images/customer.jpg);">
    <div class="panel panel-default" style="  ">
        <div class="panel-body">
            <div class="text-center">
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <h3>Customers <i class="fa fa-heart"></i> #Sashtechs</h3>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <i class="fa fa-arrow-down"></i>
                <br>
                <br>
            </div>
        </div>
    </div>
</header>

<div class="clearfix"> </div>

<section>
    <div class="container customers">
        <br>
        <h1 class="text-center">Customer Stories</h1>
        <br>

        <div class="col-md-4 col-sm-6 col-xs-12">
            <a href="#">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="card">
                            <img src="images/amit-sinha.jpg" alt="amit sinha" class="face center-block">
                            <img src="images/mifa.jpg" alt="mifa" class="logo center-block">
                            <br>

                            <h6>Sashtechs is few clicks solution. We’ve saved so much time because we no longer worry about payroll</h6>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <a href="#">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="card">
                            <img src="images/himanshu.jpg" alt="himanshu" class="face center-block">
                            <img src="images/avantika.png" alt="avantika" class="logo center-block">
                            <br>

                            <h6>Sashtechs offered us the most cost-effective, efficient and simple solution for getting our payroll done</h6>
                        </div>
                    </div>
                </div>
            </a>

        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <a href="#">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="card">
                            <img src="images/stickon.png" alt="" class="face center-block">
                            <img src="images/sticon-1.png" alt="stickon" class="logo center-block">
                            <br>

                            <h6>At Sticon we were looking for reliable partner to manage our Health benefits & payroll services and with Sashtechs robust system we were able to achieve it. They have a great team which go out of box to find the solutions.</h6>
                        </div>
                    </div>
                </div>
            </a>

        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <a href="#">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="card">
                            <img src="images/wembley1.png" alt="" class="face center-block">
                            <img src="images/wembleypaints.png" alt="" class="logo center-block">
                            <br>

                            <h6>Wembley Paints & Chemicals was looking for cloud based HR system which automatically runs the payroll and with outsourced manager help from Sashtechs, we are able to achieve that. </h6>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <a href="#">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="card">
                            <img src="images/autio.png" alt="" class="face center-block">
                            <img src="images/auto-logo.png" alt="" class="logo center-block" style="height: 4em; width: 60%" >
                            <br>

                            <h6>Sashtechs is an exceptionally good deal and clearly meets all our business needs. I honestly can’t imagine having a payroll experience any better than this one.</h6>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <a href="#">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="card">
                            <img src="images/bsb-1.jpg" alt="" class="face center-block">
                            <img src="images/bsb.jpg" alt="" class="logo center-block">
                            <br>

                            <h6>I can run payroll in a 1/10 of the time it took me with other companies, and the support is fantastic. Wokzippy gives you wide range of features which every startup needs on daily basis.</h6>
                        </div>
                    </div>
                </div>
            </a>
        </div>

    </div>
</section>


<section style="margin:0px;">
    
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
      </ol>

      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="img-responsive" src="images/customer1.jpg" alt="customer1">
          <div class="carousel-caption">
            <center><h3>  Neeti Singh Founder & CEO <a href="//www.challenjr.com">challenjr.com</a></h3> </center>
          </div>
        </div>
        <div class="item">
          <img class="img-responsive" src="images/customer2.jpg" alt="customer2">
          <div class="carousel-caption">
            <center><h3> Divya Seth HR Head of <a href="//www.teclogy.com">teclogy.com</a> </h3> </center>
          </div>
        </div>
      </div>

      <!-- Controls -->
      <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
        <span class="fa fa-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
        <span class="fa fa-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
    <div class="clearfix"></div>
</section>

<div class="clearfix"></div>

<section>
    <div class="container">
        <div class="tweet-box">
            <div>
                <blockquote class="twitter-tweet" lang="en"><p lang="en" dir="ltr"><a href="https://twitter.com/sashtechs">@sashtechs</a> Thanks you for making the Tax filing so seamless for NRIs. The UI is pretty simple and customer service team was very supportive.</p>&mdash; Prateek Sharma (@sharmaprateek) <a href="https://twitter.com/sharmaprateek/status/601557567362306049">May 22, 2015</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                <br>
            </div>
            <div>
                <blockquote class="twitter-tweet" lang="en"><p lang="en" dir="ltr"><a href="https://twitter.com/sashtechs">@sashtechs</a> Great website to file your taxes and beautifully crafted engineering product. Highly recommended for NRIs to file their return.</p>&mdash; Varun Kaushik (@varun_fom) <a href="https://twitter.com/varun_fom/status/601779484589625344">May 22, 2015</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                <br>
            </div>
            <div>
                <blockquote class="twitter-tweet" lang="en"><p lang="en" dir="ltr"><a href="https://twitter.com/sashtechs">@sashtechs</a> try it out! innovative and much needed! <a href="https://twitter.com/MyBigPlunge">@MyBigPlunge</a></p>&mdash; AshutoshBhattacharya (@Aarcuz) <a href="https://twitter.com/Aarcuz/status/603644878610890752">May 27, 2015</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                <br>
            </div>
            <div>
                <blockquote class="twitter-tweet" lang="en"><p lang="en" dir="ltr"><a href="https://twitter.com/sashtechs">@sashtechs</a> I used sashtechs for my business needs, just upload docs and leave everything to them. Great Support &amp; Price.</p>&mdash; Amit Sinha (@write2sinha) <a href="https://twitter.com/write2sinha/status/601302725742043136">May 21, 2015</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                <br>
            </div>
            <div>
                <blockquote class="twitter-tweet" lang="en"><p lang="en" dir="ltr"><a href="https://twitter.com/sashtechs">@sashtechs</a> Used the free edition of the website and its amazing. It covers all the modules. Great UI and easy process to file tax returns.</p>&mdash; Karan Rao (@raokaran) <a href="https://twitter.com/raokaran/status/601279141950787585">May 21, 2015</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                <br>
            </div>
        </div>
                
    </div>
</section>

<div class="clearfix"></div>

<section class="parallax" style="background-image: url(images/try.jpg);">
    <div class="panel panel-default" style="  ">
        <div class="panel-body">
            <div class="text-center">
                <br>
                <br>
                <br>
                <br>
                <h5><b>TRY Sashtechs</b></h5>
                <h3 class="text-uppercase">Start your one month free trial </h3>
                <br>

                <div class="panel text-left" id="panel-text-left">
                    <?php require 'demoForm.php'; ?>
                </div>
                <br>
                    <div class="clearfix"></div>
                    <br><br>
                    <br><br>
            </div>
        </div>
    </div>
</section>


<?php require_once('staticFooter.php');?>
<script type="text/javascript" src="js/slick.min.js"></script>
</body>
</html>

<script>
    $.material.init();

    $(function() {
        $('.tweet-box').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            autoplay: true,
            autoplaySpeed: 3000,
            centerMode: true,
            centerPadding: '1em',
            slidesToShow: 3,
            variableWidth:true,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        arrows: true,
                        centerMode: true,
                        centerPadding: '2em',
                        slidesToShow: 3,
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1,
                    }
                }
            ]
        });
    });
</script>
