<!DOCTYPE html>
<html>
<head>
    <title>Jobs in India’s first Payroll & Health benefits</title>

    <meta name='description' content='Sashtechs offers statutory compliance services in HR across India to help companies deal with frequently changing laws of government to avoid penalties ' />

    <meta name='keywords' content=' Assistant Manager Payroll, Assistant Manager Benefits, Assistant Manager Taxation, Payroll Jobs, Payroll/Benefits Administration India, Tax Jobs, HR jobs, Finance Services jobs, Hr-payroll Process ' />

    <!--Import Google Icon Font-->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">

    <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet"></link>


    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Material Design -->
    <link href="css/bootstrap-material-design.css" rel="stylesheet">
    <link href="css/ripples.min.css" rel="stylesheet">
    <link href="css/roundslider.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">

    <script src="/js/jquery-1.12.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ripples.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/roundslider.min.js"></script>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    <style>
        .job{
            padding: 20px 15px;
            cursor: pointer;
            font-weight: 400;
        }
        .jobDetail{
            padding: 15px;
        }
        .faq-item{
            font-family: FontAwesome;
            color: #555;
            border-bottom: 1px solid #ddd;
        }
    </style>
</head>

<body>
<?php require_once("staticNavBar.php")?>

<!-- ================  Header  ===================== -->

<header class="parallax" style="background-image: url(images/career.jpg);background-position: center">
    <div class="panel panel-default" style="  ">
        <div class="panel-body">
            <div class="text-center">
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <h5>CAREERS</h5>
                <h2>Join the Sashtechs Team</h2>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
            </div>
        </div>
    </div>
</header>

<section style="background:#fff;margin-top: -20px">
    <div class="container-fluid">
        <div class="row">
            <div class="container">
                <h1 class="text-center">YOU @ Sashtechs</h1>
                <br>
                <h3 class="text-center">A Great Workplace Combines Stunning Colleagues and Hard Problems</h3>
                <br>
                <p class="text-center">If you are looking to have a large impact at a growing company and work with a high performance team – start here. <br>
                    You’ll advance our goal of “Revolutionize the WorkSpace” by providing expert guidance and explanations of payroll, benefits and technical terms to our clients.
                </p>
            </div>
        </div>
        <br><hr><br>
    </div>
</section>

<div class="clearfix"></div>

<section style="background:#fff;">
    <div class="container">
        <h2 class="text-center" >Position : Payroll Assistants, Payroll Consultants</h2>
        <br>

        <div class="col-md-10 col-md-offset-1">
            <div class="faq-item">
                <div class="job">
                    <span class="h4">Assistant Manager Payroll </span><i class="fa fa-chevron-down pull-right"></i>
                </div>
                <div class="jobDetail">
                    <b>Duration </b> : <span>Full Time</span> <br>
                    <b>Job Profile </b> : <span>Assistant Manager Payroll</span><br>
                    <b>Qualifications </b> : <span>Responsible for Payroll Processing for Employees. Preparing and checking of salary reconciliation. Responsible for Full and Final settlement of separating employees. Accounting of Salary and deputation cost on monthly basis.</span><br>
                    <b>Send Resume at </b> : <span><a href="mailto:jobs@Sashtechs.com">jobs@Sashtechs.com</a></span><br>

                </div>
            </div>
        </div>

        <div class="col-md-10 col-md-offset-1">
            <div class="faq-item">
                <div class="job">
                    <span class="h4">Assistant Manager Benefits</span><i class="fa fa-chevron-down pull-right"></i>
                </div>
                <div class="jobDetail">
                    <b>Duration </b> : <span>Full Time</span> <br>
                    <b>Job Profile </b> : <span>Assistant Manager Payroll</span><br>
                    <b>Qualifications </b> : <span>Manage Health Insurance plans & SME in this area. Training the internal staff on about the usage and process Group Health Insurance. Supervise the Health plan for all the clients to ensure they are satisfied. • Conduct monthly performance review (PRP) with internal team & update sales.</span><br>
                    <b>Send Resume at </b> : <span><a href="mailto:jobs@Sashtechs.com">jobs@Sashtechs.com</a></span><br>

                </div>
            </div>
        </div>

        <div class="col-md-10 col-md-offset-1">
            <div class="faq-item">
                <div class="job">
                    <span class="h4">Assistant Manager Taxation</span><i class="fa fa-chevron-down pull-right"></i>
                </div>
                <div class="jobDetail">
                    <b>Duration </b> : <span>Full Time</span> <br>
                    <b>Job Profile </b> : <span>Assistant Manager Taxation</span><br>
                    <b>Qualifications </b> : <span>Drafting replies to the notices received from Tax Authorities, Maintenance of Notice Register Calculation and deposition of Service Tax, Assisting in filing of Service tax return Calculation and deposition of TDS and return filing Processing and Payments of bills / invoices received from Vendors matching with purchase orders / agreements Preparation of Bank Reconciliations.</span><br>
                    <b>Send Resume at </b> : <span><a href="mailto:jobs@Sashtechs.com">jobs@Sashtechs.com</a></span><br>

                </div>
            </div>
        </div>

        <div class="col-md-10 col-md-offset-1">
            <div class="faq-item">
                <div class="job">
                    <span class="h4"> PHP – Sr. Software Developer</span><i class="fa fa-chevron-down pull-right"></i>
                </div>
                <div class="jobDetail">
                    <b>Duration </b> : <span>Full Time</span> <br>
                    <b>Job Profile </b> : <span>Sr. Software Developer in PHP</span><br>
                    <b>Location </b> : <span>Delhi</span><br>
                    <b>Qualifications </b> : 
                        <ul>
                            <li>3+ experience in PHP – Laravel or similar framework.</li>
                            <li>Any B.Tech. or CS student with good communication skills in English.</li>
                            <li>Excellent coding skills especially in PHP.</li>
                            <li>Server side experience required.</li>
                            <li>Willing to work in a start-up environment.</li>
                            <li>Good working knowledge of PHP-MySQL and strong technical inclination.</li>
                            <li>Strong Understanding of OOPs concepts.</li>
                        </ul>
                    <b>Send Resume at </b> : <span><a href="mailto:jobs@Sashtechs.com">jobs@Sashtechs.com</a></span><br>

                </div>
            </div>
        </div>

        <div class="col-md-10 col-md-offset-1">
            <div class="faq-item">
                <div class="job">
                    <span class="h4">PHP – Jr. Developer</span><i class="fa fa-chevron-down pull-right"></i>
                </div>
                <div class="jobDetail">
                    <b>Duration </b> : <span>Full Time</span> <br>
                    <b>Job Profile </b> : <span>Jr. Software Developer in PHP</span><br>
                    <b>Location </b> : <span>Delhi</span><br>
                    <b>Qualifications </b> : 
                        <ul>
                            <li>1+ experience in PHP – Laravel or similar framework.</li>
                            <li>Any B.Tech. or CS student with good communication skills in English.</li>
                            <li>Excellent coding skills especially in PHP.</li>
                            <li>Server side experience required.</li>
                            <li>Willing to work in a start-up environment.</li>
                            <li>Good working knowledge of PHP-MySQL and strong technical inclination.</li>
                            <li>Strong Understanding of OOPs concepts.</li>
                        </ul>
                    <b>Send Resume at </b> : <span><a href="mailto:jobs@Sashtechs.com">jobs@Sashtechs.com</a></span><br>

                </div>
            </div>
        </div>

        <div class="col-md-10 col-md-offset-1">
            <div class="faq-item">
                <div class="job">
                    <span class="h4">Software Developer</span><i class="fa fa-chevron-down pull-right"></i>
                </div>
                <div class="jobDetail">
                    <b>Duration </b> : <span>Full Time</span> <br>
                    <b>Job Profile </b> : <span>UI/UX Engineer</span><br>
                    <b>Location </b> : <span>Delhi</span><br>
                    <b>Qualifications </b> : 
                        <ul>
                            <li>Any Btech or CS student with good communication skills in English.</li>
                            <li>2+ years of experience.</li>
                            <li>HTML5, CSS & jQuery, Ajax and Angular JS</li>
                            <li>Familiar with Frameworks – Bootstrap, Foundation.</li>
                            <li>Raw JavaScript and Frameworks such as JQuery.</li>
                            <li>XML/JSON.</li>
                            <li>WordPress.</li>
                        </ul>
                    <b>Send Resume at </b> : <span><a href="mailto:jobs@Sashtechs.com">jobs@Sashtechs.com</a></span><br>

                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <br>
        <br>    
    </div>
</section>

<div class="clearfix"> </div>

<section class="parallax" style="background-image: url(images/try.jpg);">
    <div class="panel panel-default" style="  ">
        <div class="panel-body">
            <div class="text-center">
                <br>
                <br>
                <br>
                <br>
                <h5><b>TRY Sashtechs</b></h5>
                <h3 class="text-uppercase">Start your one month free trial </h3>
                <br>

                <div class="panel text-left" id="panel-text-left">
                    <?php require 'demoForm.php'; ?>
                </div>
                <br>
                    <div class="clearfix"></div>
                    <br><br>
                    <br><br>
            </div>
        </div>
    </div>
</section>

<?php require_once('staticFooter.php');?>

</body>
</html>

<script>
    $.material.init();

    $(function() {

        $(".faq-item .job").on('click', function(event) {
            $(this).siblings('.jobDetail').slideToggle(400);
            if ($(this).children('i').hasClass('fa-chevron-down')) {
                $(this).children('.fa-chevron-down').removeClass('fa-chevron-down').addClass('fa-chevron-up');
            }
            else
                $(this).children('.fa-chevron-up').removeClass('fa-chevron-up').addClass('fa-chevron-down');

        });

        $(".jobDetail").hide();

    });
</script>
