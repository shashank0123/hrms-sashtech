<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB, View;
use Illuminate\Http\Request;

class reportController extends Controller {

	public function employerMonthSlip($month = null)
	{
		if (empty((session()->get('employerId')))) {
			return redirect('login');
		}
		if (isset($month)) {
			$select = DB::table('user')
							->join('employeeCtc', 'user.id', '=', 'employeeCtc.userId')
							->join('payroll', 'user.id', '=', 'payroll.userID')
							->where('user.companyId', '=' , session()->get('companyId'))
							->where('user.type', '=' , "Employee")
							->where('payroll.month', '=' , $month)
							->select('employeeCtc.*', 'payroll.*', 'user.*')
							->get();

			if (!empty($select)) {
				return View::make('monthslip')->with('employees', $select);
			}
			else {
				return View::make('monthslip');
			}
		}
		else {
			return View::make('monthslip');
		}

	}
	public function employeePaySlip($month = null)
	{
		if (isset($month)) {
			$select = DB::table('user')
							->join('employeeCtc', 'user.id', '=', 'employeeCtc.userId')
							->join('payroll', 'user.id', '=', 'payroll.userID')
							->where('user.companyId', '=' , session()->get('companyId'))
							->where('user.type', '=' , "Employee")
							->where('payroll.month', '=' , $month)
							->select('employeeCtc.*', 'payroll.*', 'user.*')
							->get();

			if (!empty($select)) {
				return View::make('monthslip')->with('employees', $select);
			}
			else {
				return View::make('monthslip');
			}
		}
		else {
			return View::make('monthslip');
		}

	}
	public function viewPayslip()
	{
		if (session()->get('employeeId') and (strtolower(session()->get('type')) == 'employee'))
		{
			$year 	= session()->get('payslipYear');
			$month 	= session()->get('payslipMonth');

			session()->put('payslipYear', "");
			session()->put('payslipMonth', "");

			$result 	= DB::table('personalDetails')
						->where('userid', session()->get('employeeId'))
						->first();

			$result1 	= DB::table('user')
						->where('id', session()->get('employeeId'))
						->first();

			$result2	= DB::table('employeeCtc')
						->where('userId', session()->get('employeeId'))
						->first();

			$result3	= DB::table('BankDetails')
						->where('userId', session()->get('employeeId'))
						->first();

			$result4	= DB::table('payroll')
						->where('userID', session()->get('employeeId'))
						->where('financialYear', $year)
						->where('month', $month)
						->first();

			$result5	= DB::table('company')
						->join('companyAddress', 'company.id', '=', 'companyAddress.companyId')
						->where('company.id', session()->get('companyId'))
						->select('company.id', 'company.companyName', 'companyAddress.city', 'companyAddress.logoUrl')
						->first();

			// $result6	= DB::table('leaveSetup')
			// 			->where('companyId', session()->get('companyId'))
			// 			->first();

			return View::make('payslip')
					->with('result', $result)
					->with('result1', $result1)
					->with('result2', $result2)
					->with('result3', $result3)
					->with('result4', $result4)
					->with('result5', $result5);
		}
		else
		{
			return redirect('login');
		}
	}

	public function getPayslip($year, $month)
	{
		if (session()->get('employeeId') and (strtolower(session()->get('type')) == 'employee'))
		{
			session()->put('payslipYear', $year);
			session()->put('payslipMonth', $month);
			return redirect('/payslip');
		}
		else
		{
			return redirect('login');
		}
	}
}
