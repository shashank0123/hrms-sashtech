<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator, Input, Redirect, DB, Hash, Session, View, File, StdClass, Response;
use Illuminate\Http\Request;

class companyDetailsController extends Controller {

	public function viewCompanyDetails()
	{
		$result 	= DB::table('company')
					->where('id', Session::get('companyId'))
					->first();

		$result1	= DB::table('companyAddress')
					->where('companyId', Session::get('companyId'))
					->where('type', 'headOffice')
					->first();

		$result2	= DB::table('companyAddress')
					->where('companyId', Session::get('companyId'))
					->where('type', 'LIKE', 'branchOffice%')
					->get();

		$result3	= DB::table('companyBankDetail')
					->where('companyId', Session::get('companyId'))
					->first();

		$result4	= DB::table('companyTaxDetail')
					->where('companyId', Session::get('companyId'))
					->first();

		return View::make('companyDetail')
		->with('result', $result)
		->with('result1', $result1)
		->with('result2', $result2)
		->with('result3', $result3)
		->with('result4', $result4);
	}

	public function getCompanyDetails() 
	{
		if (strtolower(session()->get('type')) != 'employer') 
		{
			return redirect('login');
		}

		if (strtolower(session()->get('complete')) == 'on' ) 
		{
			return redirect('employerDashboard');
		}

		$select = DB::table('company')
				->where('id', Session::get('companyId'))
				->first();

		$select1 = DB::table('companyAddress')
				->where('companyId', Session::get('companyId'))
				->where('type', 'headOffice')
				->first();

		return View::make('addCompanyLocation')
				->with('company', $select)
				->with('companyAddress', $select1);

	}

	public function getCompanyBankDetails()
	{
		if (strtolower(session()->get('type')) != 'employer') 
		{
			return redirect('login');
		}

		// if (strtolower(session()->get('complete')) == 'on' ) 
		// {
		// 	return redirect('employerDashboard');
		// }

		$result = DB::table('companyBankDetail')
				->where('companyId', Session::get('companyId'))
				->orderBy('created_at', 'DESC')
				->first();

		return View::make('bankSetup')
				->with('result', $result);
	}

	public function updateCompanyAddress()
	{
		$validator = Validator::make(
			$entries = array(
				'companyId' => session()->get('companyId'),
				// 'companyCode'=> strtoupper(Input::get('companyCode')),
				'type'=> Input::get('type'),
				'propertyNo' => ucwords(Input::get('propertyNo')),
				'address' => ucwords(Input::get('address')),
				'street' => ucwords(Input::get('street')),
				'city' => ucwords(Input::get('city')),
				'state' => ucwords(Input::get('state')),
				'pin' => ucwords(Input::get('pin')),
				'logoUrl' => Input::file('companyLogo'),
				),
			array(
				// 'companyCode' => 'required|size:4',
				'address' => 'required',
				'city' => 'required',
				'state' => 'required',
				'pin' => 'required|digits:6',
				'logoUrl' => 'max:500|mimes:jpg,jpeg,png',
				),
			$messages = array(
				'required' => 'The :attribute field is required.',
				'mimes' => 'The file is not in acceptable format (i.e. jpg,jpeg,png).',
				'max' => 'The file size is bigger than 500KB.'
				)
			);

		if ($validator->fails())
		{
			return response()->json($validator->messages());
		}			   
		else
		{	
			// if (session()->get('companyCode')!=Input::get('companyCode') || session()->get('step2')!="on")
			// if (session()->get('companyCode') != $entries['companyCode'])
			// {
			// 	$code 	= DB::table('company')
			// 			->where('companyCode', $entries['companyCode'])
			// 			->first();

			// 	$result = new StdClass;

			// 	if (empty($code)) 
			// 	{
			// 		$users 	= DB::table('user')
			// 				->where('companyId', session()->get('companyId'))
			// 				->get();

			// 		if (count($users) > 1) 
			// 		{
			// 			$result->companyCode[0]="Company code cannot be changed now.";
			// 			return response()->json($result);
			// 		}
			// 		else
			// 		{
			// 			$users 	= DB::table('user')
			// 					->where('companyId', session()->get('companyId'))
			// 					->orderBy('created_at', 'DESC')
			// 					->first();

			// 			$id 	= str_replace(session()->get('companyCode'), $entries['companyCode'], $users->id);

			// 			$update = DB::table('user')
			// 					->where('id', $users->id)
			// 					->update(['id' => $id]);

			// 			$report = DB::table('company')
			// 					->where('id', session()->get('companyId'))
			// 					->update([
			// 						'companyCode'	=> $entries['companyCode']
			// 						]);
			// 		}
			// 	}
			// 	else
			// 	{
			// 		$result->companyCode[0]="Company code already in use.";

			// 		return response()->json($result);
			// 	}
			// }

			$entries = array_filter($entries);
			$uploadPath = public_path() . '/HTML/Uploads/Company/' . $entries['companyId'] . '/';

			//check if the directory exists
			if(!File::exists($uploadPath)) {
				File::makeDirectory($uploadPath , 0775, true);
			}

			// Move file to upload directory
			if (isset($entries['logoUrl'])) 
			{
				$logoUrl = $uploadPath . 'logo-' . $entries['logoUrl']->getClientOriginalName();
				if(File::exists($logoUrl)) 
				{
					unlink($logoUrl);
				}
				$file = $entries['logoUrl']->move($uploadPath, $logoUrl);
				
				$logoUrl = explode('public/', $logoUrl)[1];
				$entries['logoUrl'] = $logoUrl;
			}

			$select = DB::table('companyAddress')->where('companyId', session()->get('companyId'))->first();
			if (empty($select)){
				$result=DB::table('companyAddress')->insert($entries);
				$select=DB::table('employerProgress')->where('companyId', session()->get('companyId'))->update(['step1' => 'on', 'nextStep' => 'step2']);
			}
			else {
				$result=DB::table('companyAddress')->where('companyId', session()->get('companyId'))->update($entries);			
				$select=DB::table('employerProgress')->where('companyId', session()->get('companyId'))->update(['step1' => 'on', 'nextStep' => 'step2']);
			}
			if (Input::get('target')=="add") {
				$url="/addBranchLocation";
			}
			else
				$url="/addEmployees_step1";
			return response()->json(['result' => $result, 'status' => 200, 'url' => $url, 'message'=>'Data inserted successfully']);
		}
	}

	public function updateBranchAddress()
	{
		$validator = Validator::make(
			$entries = array(
				'companyId' => session()->get('companyId'),
				'type'=> Input::get('type'),
				'propertyNo' => ucwords(Input::get('propertyNo')),
				'address' => ucwords(Input::get('address')),
				'street' => ucwords(Input::get('street')),
				'city' => ucwords(Input::get('city')),
				'state' => ucwords(Input::get('state')),
				'pin' => ucwords(Input::get('pin')),
				'target' => strtolower(Input::get('target')),
				),
			array(
				'address' => 'required',
				'city' => 'required',
				'state' => 'required',
				'pin' => 'required|digits:6',
				'target' => 'required',
				),
			$messages = array(
				'required' => 'The :attribute field is required.',
				'mimes' => 'The file is not in acceptable format (i.e. jpg,jpeg,png).',
				'max' => 'The file file is bigger than 500KB.'
				)
			);

		// echo "<pre>";
		// var_dump(Input::all());
		// var_dump($entries);
		// die;

		if ($validator->fails())
		{
			return response()->json($validator->messages());
		}			   
		else
		{	
			$target 	= $entries['target'];

			$entries['target']	= NULL;

			$entries 	= array_filter($entries);

			if ($target == 'add') 
			{
				$url="/addBranchLocation";
			}
			elseif ($target == 'next') 
			{
				$url="/addEmployees_step1";
			}
			else
			{
				return redirect('login');
			}

			// $company 	= DB::table('company')
			// 			->where('id', session()->get('companyId'))
			// 			->first();

			$entries['companyCode'] = session()->get('companyId');

			$count 		= DB::table('companyAddress')
						->where('companyId', session()->get('companyId'))
						->where('type', '<>', 'headOffice')
						->get();

			$count 		= count($count);
			$count++;
			$officeType = 'branchOffice'.$count;
			$entries['type'] = $officeType;

			$select 	= DB::table('companyAddress')
						->where('companyId', session()->get('companyId'))
						->where('type', $officeType)
						->first();

			if (empty($select))
			{
				$result = DB::table('companyAddress')
						->insert($entries);
			}
			else 
			{
				$result = DB::table('companyAddress')
						->where('companyId', session()->get('companyId'))
						->where('type', 'branchOffice')
						->update($entries);
			}			

			return response()->json([
				'result' => $result, 
				'status' => 200,
				'url' => $url,
				'message'=>'Data inserted successfully.'
				]);
		}
	}

	public function updateCompanyDetails()
	{
		// For Company Logo
		$companyLogo = Input::file('companyLogo');

		if (isset($companyLogo)) 
		{
			$validator = Validator::make(
				$input 	= array(
					'id' 		=> session()->get('companyId'),
					'logo' 		=> $companyLogo,
					),
				array(
					'logo' 		=> 'max:500|mimes:jpg,jpeg,png',
					),
				$messages = array(
					'max' 		=> 'This file is bigger than allowed size (500KB)',
					'mimes' 	=> 'The file is not in acceptable format i.e. jpg,jpeg,png.',
					)
			);

			if ($validator->fails()) 
			{
				return response()->json([$validator->messages()]);
			}
			else
			{
				$uploadPath = public_path() . '/HTML/Uploads/Company/' . $input['id'] . '/';

				//check if the directory exists
				if(!File::exists($uploadPath)) {
					File::makeDirectory($uploadPath, 0775, true);
				}

				// Move file to upload directory
				$logoUrl = $uploadPath . 'logo-' . $input['logo']->getClientOriginalName();
				
				if(File::exists($logoUrl)) 
				{
					unlink($logoUrl);
				}

				$file = $input['logo']->move($uploadPath, $logoUrl);
				$logoUrl=explode('public/', $logoUrl)[1];
				$input['logo'] = $logoUrl;

				$update = DB::table('companyAddress')
						->where('companyId', $input['id'])
						->update([
							'logoUrl' 		=> $input['logo'],
							'updated_at'	=> date("Y-m-d h:i:sa"),
						]);

				$result	= new StdClass;

				if ($update) 
				{
					$result->status 	= "200";
					$result->message 	= "Data was updated successfully.";
				}
				return Response::json($result);
			}
		}

		$type 	= strtolower(Input::get('type'));
		$area 	= strtolower(Input::get('area'));

		if (($type) && ($type == 'delete'))
		{
			$validator 	= Validator::make(
				$input 	= array(
					'addressType' 	=> strtolower(Input::get('addressType')),
					),
				array(
					'addressType'	=> 'required|string',
					)
				);

			if ($validator->fails()) 
			{
				return Response::json($validator->messages());
			}
			else
			{
				$result 	= new StdClass;

				if ($input['addressType'] == 'headoffice') 
				{
					$result->status 	= 405;
					$result->message 	= "Deleting your head office is not allowed.";
				}
				else
				{
					$delete = DB::table('companyAddress')
							->where('type', $input['addressType'])
							->delete();

					if ($delete) 
					{
						$result->status 	= 200;
						$result->message 	= "Data deleted successfully.";
					}
				}

				return Response::json($result);
			}
		}
		if ($area) 
		{
			if ($area == 'address') 
			{
				if (Input::get('addressType') == "headOffice") 
				{
					$validator = Validator::make(
						$input = array(
							'companyId' 	=> Session::get('companyId'),
							'address' 		=> ucwords(strtolower(Input::get('address'))),
							'propertyNo'	=> Input::get('propertyNo'),
							'street' 		=> ucwords(strtolower(Input::get('street'))),
							'city' 			=> ucwords(strtolower(Input::get('city'))),
							'state' 		=> strtoupper(Input::get('state')),
							'pin' 			=> Input::get('pin'),
							), 
						array(
							'address' 		=> 'required',
							'propertyNo'	=> 'required',
							'street' 		=> 'required',
							'city' 			=> 'required|string',
							'state' 		=> 'required|string',
							'pin' 			=> 'required|numeric',
							), 
						$messages = array(
							'required' 	=> "This field is required.",
							'string' 	=> "This must be a string.",
							'numeric' 	=> "This must be a number.",
							)
						);

					if ($validator->fails()) 
					{
						return Response::json($validator->messages());
					}
					else
					{
						$result	= new StdClass;

						$update = DB::table('companyAddress')
						->where('companyId', $input['companyId'])
						->where('type', 'headOffice')
						->update([
							'propertyNo' 	=> $input['propertyNo'],
							'street' 		=> $input['street'],
							'city' 			=> $input['city'],
							'state' 		=> $input['state'],
							'pin' 			=> $input['pin'],
							'address' 		=> $input['address'],
							'updated_at' 	=> date("Y-m-d h:i:sa"),
							]);

						if ($update) 
						{
							$result->status 	= "200";
							$result->message 	= "Data was updated successfully.";
						}
						return Response::json($result);
					}
				}
				elseif (preg_match('#^branchOffice#i', Input::get('addressType')) === 1)
				{
					$validator = Validator::make(
						$input = array(
							'companyId' 	=> Session::get('companyId'),
							'companyCode' 	=> Session::get('companyCode'),
							'type'			=> Input::get('addressType'),
							'address' 		=> ucwords(strtolower(Input::get('address'))),
							'propertyNo'	=> Input::get('propertyNo'),
							'street' 		=> ucwords(strtolower(Input::get('street'))),
							'city' 			=> ucwords(strtolower(Input::get('city'))),
							'state' 		=> strtoupper(Input::get('state')),
							'pin' 			=> Input::get('pin'),
							), 
						array(
							'address' 		=> 'required',
							'propertyNo'	=> 'required',
							'street' 		=> 'required',
							'city' 			=> 'required|string',
							'state' 		=> 'required|string',
							'pin' 			=> 'required|numeric',
							), 
						$messages = array(
							'required' 	=> "This field is required.",
							'string' 	=> "This must be a string.",
							'numeric' 	=> "This must be a number.",
							)
						);

					if ($validator->fails()) 
					{
						return Response::json($validator->messages());
					}
					else
					{
						$result	= new StdClass;

						$select = DB::table('companyAddress')
						->where('companyId', $input['companyId'])
						->where('type', $input['type'])
						->first();

						if (empty($select)) 
						{
							$insert = DB::table('companyAddress')
							->insert($input);
						}
						else
						{
							$update = DB::table('companyAddress')
							->where('companyId', $input['companyId'])
							->where('type', $input['type'])
							->update([
								'propertyNo' 	=> $input['propertyNo'],
								'street' 		=> $input['street'],
								'city' 			=> $input['city'],
								'state' 		=> $input['state'],
								'pin' 			=> $input['pin'],
								'address' 		=> $input['address'],
								'updated_at' 	=> date("Y-m-d h:i:sa"),
								]);
						}

						if (isset($insert) || isset($update))
						{
							$result->status 	= "200";
							$result->message 	= "Data was updated successfully.";
						}
						return Response::json($result);
					}
				}
			}
			elseif ($area == 'bank')
			{
				$validator = Validator::make(
					$input = array(
						'companyId' 	=> Session::get('companyId'),
						'bankName' 		=> ucwords(strtolower(Input::get('nameOfBank'))),
						'accountNumber' => Input::get('accountNumber'),
						'accountType'	=> ucwords(strtolower(Input::get('type'))),
						'ifsc' 			=> strtoupper(Input::get('ifsc')),
						'micr' 			=> strtoupper(Input::get('micr')),
						'loginid' 		=> Input::get('loginid'),
						'bankURL' 		=> strtolower(Input::get('bankURL')),
						), 
					array(
						'bankName' 		=> 'required|string',
						'accountNumber' => 'required|numeric|regex:/([0-9]){10,}\d/',
						'accountType'	=> 'required|string',
						'ifsc' 			=> 'required|alpha_num|size:11|regex:/([A-Za-z]){4}[0][0-9A-Za-z]{6}/',
						'micr' 			=> 'required',
						'loginid' 		=> 'required',
						'bankURL' 		=> 'required',
						), 
					$messages = array(
						'required' 	=> "This field is required.",
						'size'		=> "This field must have a length of 11.",
						'regex'		=> "This is not a valid IFSC.",
						'string' 	=> "This must be a string.",
						'numeric' 	=> "This must be a number.",
						)
					);

				if ($validator->fails()) 
				{
					return Response::json($validator->messages());
				}
				else
				{
					$result	= new StdClass;

					$update = DB::table('companyBankDetail')
					->where('companyId', $input['companyId'])
					->update([
						'bankName' 		=> $input['bankName'],
						'accountNumber' => $input['accountNumber'],
						'accountType'	=> $input['accountType'],
						'ifsc' 			=> $input['ifsc'],
						'micr' 			=> $input['micr'],
						'loginid' 		=> $input['loginid'],
						'bankURL' 		=> $input['bankURL'],
						'updated_at' 	=> date("Y-m-d h:i:sa"),
						]);

					if ($update) 
					{
						$result->status 	= "200";
						$result->message 	= "Data was updated successfully.";
					}
					return Response::json($result);
				}
			}
			elseif ($area == 'tax')
			{
				$validator = Validator::make(
					$input = array(
						'companyId' => Session::get('companyId'),
						'pan'		=> Input::get('pan'),
						'tan'		=> Input::get('tan'),
						'epf'		=> strtolower(Input::get('epf')),
						'esi' 		=> strtolower(Input::get('esi')),
						'gratuity' 	=> strtolower(Input::get('gratuity')),
						), 
					array(
						'pan' 		=> 'required|alpha_num|size:10|regex:/([a-zA-Z]){5}([0-9]){4}([a-zA-Z])/',
						'tan' 		=> 'required|alpha_num|size:10|regex:/([A-Za-z]){4}[0-9]{5}([A-Za-z])/',
						), 
					$messages = array(
						'required' 	=> "This field is required.",
						'size'		=> "This field must have a length of 10.",
						'regex'		=> "This is not valid.",
						)
					);

				if (empty($input['epf'])) 
				{
					$input['epf']	= 'off';
				}

				if (empty($input['esi'])) 
				{
					$input['esi']	= 'off';
				}

				if (empty($input['gratuity'])) 
				{
					$input['gratuity'] = 'off';
				}

				if ($validator->fails()) 
				{
					return Response::json($validator->messages());
				}
				else
				{
					$result	= new StdClass;

					$update = DB::table('companyTaxDetail')
							->where('companyId', $input['companyId'])
							->update([
								'pan' 			=> $input['pan'],
								'tan' 			=> $input['tan'],
								'epf'			=> $input['epf'],
								'esi' 			=> $input['esi'],
								'gratuity' 		=> $input['gratuity'],
								'updated_at' 	=> date("Y-m-d h:i:sa"),
								]);

					if ($update) 
					{
						$result->status 	= "200";
						$result->message 	= "Data was updated successfully.";
					}
					return Response::json($result);
				}
			}
		}
	}

	// public function updateBankDetails()
	// {
	// 	$validator = Validator::make(
	// 		$entries = array(
	// 				'companyId' => session()->get('companyId'),
	// 				'bankName' => ucfirst(Input::get('bankName')),
	// 				'branch' => ucfirst(Input::get('branch')),
	// 				'acNumber' => ucfirst(Input::get('acNumber')),
	// 				'accountType' => ucfirst(Input::get('accountType')),
	// 				'ifsc' => ucfirst(Input::get('ifsc')),
	// 				// 'created_at' => date("d-m-y h:i:sa"),
	// 			),
	// 			array(
	// 				'bankName' => 'required',
	// 				'branch' => 'required',
	// 				'acNumber' => 'required|numeric',
	// 				'accountType' => 'required',
	// 				'ifsc' => 'required|alpha_num|size:11|regex:/([A-Za-z]){4}[0][0-9A-Za-z]{6}/'
	// 		    	),
	// 		$messages = array(
	// 				   'required' => 'The :attribute field is required.',
	// 				   'regex' => 'Enter a valid IFSC code.'
	// 				)
	// 		);

	// 	if ($validator->fails())
 //        {
	// 	   return response()->json([$validator->messages()]);;
	// 	}			   
	// 	else
	// 	{	
	// 		$select = DB::table('bankDetails')->where('companyId', session()->get('companyId'))->first();
	// 		if (empty($select)){
	// 			$result=DB::table('bankDetails')->insert($entries);
	// 			$select=DB::table('employerProgress')->where('companyId', session()->get('companyId'))->update(['step3' => 'on', 'nextStep' => 'step4', 'step4' => 'pending']);

	// 		}
	// 		else {
	// 			$result=DB::table('bankDetails')->where('companyId', session()->get('companyId'))->update($entries);			
	// 			$select=DB::table('employerProgress')->where('companyId', session()->get('companyId'))->update(['step3' => 'on', 'nextStep' => 'step4', 'step4' => 'pending']);
	// 		}

	// 	   	return response()->json([$validator->messages()]);
	// 	}
	// }

	public function updateTaxDetails()
	{
		$validator = Validator::make(
			$entries = array(
				'companyId' => session()->get('companyId'),
				'pan' => Input::get('pan'),
				'tan' => Input::get('tan'),
				),
			array(
				'pan' => 'required|alpha_num|size:10|regex:/([a-z|A-Z]){5}([0-9]){4}[a-z|A-Z]/',
				'tan' => 'required|alpha_num|size:10|regex:/([A-Za-z]){4}[0-9]{5}([A-Za-z])/'
				),
			$messages = array(
				'required' => 'The :attribute field is required.',
				'regex' => 'Enter a valid IFSC code.'
				)
			);

		if ($validator->fails())
		{
			return response()->json([$validator->messages()]);
		}			   
		else
		{	
			$select = DB::table('bankDetails')->where('companyId', session()->get('companyId'))->first();
			if (empty($select)){
				$result=DB::table('bankDetails')->insert($entries);
				$select=DB::table('employerProgress')->where('companyId', session()->get('companyId'))->update(['step5' => 'on', 'nextStep' => 'step6']);
			}
			else {
				$result=DB::table('bankDetails')->where('companyId', session()->get('companyId'))->update($entries);
				$select=DB::table('employerProgress')->where('companyId', session()->get('companyId'))->update(['step5' => 'on', 'nextStep' => 'step6']);					
			}

			return response()->json([$result]);
		}
	}

	public function viewCTC()
	{
		if (strtolower(session()->get('type')) != 'employer') 
		{
			return redirect('login');
		}

		$select = DB::table('ctc')
				->where('companyId', session()->get('companyId'))
				->get();

		$select1 = DB::table('titles')
				->where('companyId', session()->get('companyId'))
				->select('titles')
				->get();

		return View::make('salarySetup')
				->with('result', $select)
				->with('titles', $select1);
	}

	public function editCTC($id)
	{
		$select = DB::table('ctc')
		->where('id', $id)
		->first();
		return response()->json($select);
	}

	public function deleteCTC($id)
	{
		$delete = DB::table('ctc')
				->where('id', $id)
				->delete();

		$result = new StdClass;
		if ($delete) 
		{
			$result->status = 200;
		}
		return response()->json($result);
	}

	public function addCTC()
	{
		$next = Input::get('next');
		if ($next)
		{
			// $getCTC = DB::table('ctc')
			// 		->where('companyId', Session::get('companyId'))
			// 		->first();

			// if (!empty($getCTC)) 
			// {
				$progress 	= DB::table('employerProgress')
							->where('companyId', session()->get('companyId'))
							->update(['step3' => 'on', 'nextStep' => 'step4']);
			// }
			// else
			// {
			// 	$progree = 1;
			// }

			$result = new StdClass;
			$result->status 	= 200;
			$result->url 		= '/bankSetup';
			
			return response()->json($result);
		}

		$validator = Validator::make(
			$entries = array(
				'id' =>  Input::get('id'),
				'companyId' => session()->get('companyId'),
				'jobTitle' => Input::get('jobTitle'),
				'ctc' => Input::get('ctc'),
				'basic' => Input::get('basic'),
				'hra' => Input::get('hra'),
				'conveyance' => Input::get('conveyance'),
				'medicalAllowance' => Input::get('medicalAllowance'),
				'medicalInsurance' => Input::get('medicalInsurance'),
				'telephone' => Input::get('telephone'),
				'leaveTravel' => Input::get('leaveTravel'),
				'uniform' => Input::get('uniform'),
				'gratuity' => Input::get('gratuity'),
				'superAnnuation' => Input::get('superAnnuation'),
				'annualBonus' => Input::get('annualBonus'),
				'festivalBonus' => Input::get('festivalBonus'),
				'incentives' => Input::get('incentives'),
				'others' => Input::get('others'),
				'leaveEncashment' => Input::get('leaveEncashment'),
				'pfContribution' => Input::get('pfContribution'),
				'esiContribution' => Input::get('esiContribution'),
				),
			array(
				'id' => 'required',
				'companyId' => 'required',
				'basic' => 'required|numeric',
				'hra' => 'numeric',
				'conveyance' => 'numeric',
				'medicalAllowance' => 'numeric',
				'medicalInsurance' => 'numeric',
				'telephone' => 'numeric',
				'leaveTravel' => 'numeric',
				'uniform' => 'numeric',
				'gratuity' => 'numeric',
				'superAnnuation' => 'numeric',
				'annualBonus' => 'numeric',
				'festivalBonus' => 'numeric',
				'incentives' => 'numeric',
				'others' => 'numeric',
				'leaveEncashment' => 'numeric',
				'pfContribution' => 'numeric',
				'esiContribution' => 'numeric',
				),
			$messages = array(
				'required' => 'The :attribute field is required.',
				'numeric' => 'The :attribute field must be a number.'
				)
			);

		if ($validator->fails())
		{
			return response()->json( $validator->messages());
		}			   
		else
		{

			$query 	= DB::table('ctc')
					->where('companyId', session()->get('companyId'))
					->where('jobTitle', Input::get('jobTitle'))
					->where('ctc', Input::get('ctc'))
					->first();

			$result = new StdClass;

			if (empty($query))
			{
				if ($entries['id'] == '0') 
				{
					$entries['id'] = null;
					$entries = array_filter($entries);

					$insert = DB::table('ctc')
							->insert($entries);

					$getLatest 	= DB::table('ctc')
								->where('companyId', session()->get('companyId'))
								->orderBy('id', 'DESC')
								->select('id')
								->first();

					if ($insert && $getLatest) 
					{
						$result->status 	= 200;
						$result->id 		= $getLatest->id;
						$result->message 	= "CTC added successfully."; 
					}
				}
				else 
				{				
					$id = $entries['id'];
					$entries['id'] = null;
					$entries = array_filter($entries);
					$entries['updated_at'] = date("Y-m-d h:i:sa");

					$update = DB::table('ctc')
							->where('id', $id)
							->update($entries);

					if ($update) 
					{
						$result->status 	= 200;
						$result->id 		= $id;
						$result->message 	= "CTC updated successfully."; 
					}
				}
				// $result = DB::table('ctc')
				// 		->where('companyId', session()->get('companyId'))
				// 		->get();

				$progress 	= DB::table('employerProgress')
							->where('companyId', session()->get('companyId'))
							->update(['step3' => 'on', 'nextStep' => 'step4']);
			}
			else
			{
				$result->message  	= "This title already has a template.";
				$result->status  	= 206;
			}

			return response()->json($result);
		}
		
	}

	public function addBankAccount()
	{
		$validator = Validator::make(
			$entries = array(
				'companyId' 	=> session()->get('companyId'),
				'bankName' 		=> ucwords(strtolower(Input::get('bankName'))),
				'accountNumber' => Input::get('accountNumber'),
				'accountType' 	=> Input::get('accountType'),
				'ifsc' 			=> Input::get('ifsc'),
				'micr' 			=> Input::get('micr'),
				'loginid' 		=> Input::get('loginid'),
				'bankURL' 		=> Input::get('bankURL'),
				),
			array(
				'companyId' 	=> 'required',
				'accountNumber' => 'required|numeric|regex:/([0-9]){10,}\d/',
				'ifsc' 			=> 'required|alpha_num|size:11|regex:/([A-Za-z]){4}[0][0-9A-Za-z]{6}/',
				'loginid' 		=> 'required',
				'bankURL' 		=> 'required|active_url',
				'micr' 			=> 'required|numeric',
				),
			$messages = array(
				'required' 		=> 'The :attribute field is required.',
				'numeric' 		=> 'The :attribute field must be a number.',
				'active_url' 	=> 'The link is not working.',
				'regex'			=> 'This is invalid.',
				)
			);

		if ($validator->fails())
		{
			return response()->json($validator->messages());
		}			   
		else
		{	
			$select = DB::table('companyBankDetail')
			->where('companyId', session()->get('companyId'))
			->first();

			$result = DB::table('companyBankDetail')
			->insert($entries);

			$select = DB::table('employerProgress')
			->where('companyId', session()->get('companyId'))
			->update(['step4' => 'on', 'nextStep' => 'step5']);

			$result = new StdClass;
			$result->status = '200';
			$result->message = 'Data inserted successfully.';
			$result->url = '/taxDetail';

			return response()->json($result);
		}
	}

	public function investmentDeduction()
	{
		$validator = Validator::make(
			$entries = array(
				'userid' => Session::get('id'),
				'LICamount' => Input::get('LIC_value'),
				'LICamounturl' => Input::get('LIC_file'),
				'pensionScheme' => Input::get('PS_value'),
				'pensionSchemeurl' => Input::get('PS_file'),
				'ppf' => Input::get('PPF_value'),
				'ppfurl' => Input::get('PPF_file'),
				'tuitionfee' => Input::get('TF_value'),
				'tuitionfeeurl' => Input::get('TF_file'),
				'sukanyasamriddhiac' => Input::get('SSA_value'),
				'sukanyasamriddhiacurl' => Input::get('SSA_file'),
				'postofficetimedeposit' => Input::get('POTD_value'),
				'postofficetimedepositurl' => Input::get('POTD_file'),
				'nsc' => Input::get('NSC_value'),
				'nscurl' => Input::get('NSC_file'),
				'ulip' => Input::get('ULIP_value'),
				'ulipurl' => Input::get('ULIP_file'),
				'retirementbplan' => Input::get('RBP_value'),
				'retirementbplanurl' => Input::get('RBP_file'),
				'fdvy' => Input::get('FD_value'),
				'fdvyurl' => Input::get('FD_file'),
				'infrastructurebonds' => Input::get('IB_value'),
				'infrastructurebondsurl' => Input::get('IB_file'),
				'kvp' => Input::get('KVP_value'),
				'kvpurl' => Input::get('KVP_file'),
				'epf' => Input::get('EPF_value'),
				'epfurl' => Input::get('EPF_file'),
				'hlprepayment' => Input::get('HLPR_value'),
				'hlprepaymenturl' => Input::get('HLPR_file'),
				'nps80ccc' => Input::get('NPS_value'),
				'nps80cccurl' => Input::get('NPS_file'),
				'pfie80ccd1' => Input::get('PFIE_value'),
				'pfie80ccd1url' => Input::get('PFIE_file'),
				'pfie80ccd2' => Input::get('PFIE2_value'),
				'pfie80ccd2url' => Input::get('PFIE2_file'),
				'other' => Input::get('AOEI_value'),
				'otherurl' => Input::get('AOEI_file'),
						// 'created_at' => date("d-m-y h:i:sa"),
				),
			array(
				'accountNumber' => 'required|numeric|regex:/([0-9]){10,}\d/',
				),
			$messages = array(
				'required' => 'The :attribute field is required.',
				'numeric' => 'The :attribute field must be a number.'
				)
			);

		if ($validator->fails())
		{
			return response()->json($validator->messages());
		}			   
		else
		{	
			$select = DB::table('BankDetails')->where('userid', session()->get('companyId'))->first();
			$result=DB::table('BankDetails')->insert($entries);
			$select=DB::table('employerProgress')->where('companyId', session()->get('companyId'))->update(['step3' => 'on', 'nextStep' => 'step4']);
			return response()->json([$result]);
		}
	}

	public function viewTDS()
	{
		if (empty((session()->get('employerId')))) {
			return redirect('login');
		}	
		$select = DB::table('user')
		->join('employeeCtc', 'user.id', '=', 'employeeCtc.userId')
		->join('TDS', 'user.id', '=', 'TDS.userId')
		->where('user.companyId', '=' , session()->get('companyId'))
		->where('user.type', '=' , "Employee")
		->get();
		return View::make('tdsReport')->with('rows', $select);
	}

	public function companyPlan()
	{
		if (strtolower(session()->get('type')) == 'employer')
		{
			if (empty(session()->get('plan'))) 
			{
				if (!empty(session()->get('created_by'))) 
				{
					return view('accountantSelectPlan');
				}
				return view('selectPlan');
			}
			return redirect('getStarted');
		}
		else
		{
			return redirect('login');
		}
	}

	public function selectPlan()
	{
		// echo "<pre>";
		// var_dump(Input::all());
		// die;

		if (Input::get('plan'))
		{
			$select = DB::table('company')
					->where('id', session()->get('companyId'))
					->update(['plan' => Input::get('plan')]);

			session()->put('plan', Input::get('plan'));
			return redirect('getStarted');
		}
	}
}