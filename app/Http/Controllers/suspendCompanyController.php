<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator, DB, Session, Input, Response, StdClass;
use Illuminate\Http\Request;

class suspendCompanyController extends Controller {

	public function suspendCompany_step1()
	{
		return redirect('suspendCompany_step2');
	}
	public function suspendCompany_step2()
	{
		$validator = Validator::make(
				$entries = array(
						'companyId' 		=> Session::get('companyId'),
						'leaveCause' => ucfirst(Input::get('leaveCause')),
						'suggestion'=>Input::get('suggestion'),
					    
					),
					array(
						'companyId' => 'required',						
						'leaveCause' => 'required',
				    ),
				$messages = array(
					   'required' => 'The :attribute field is required.',
					)
				);
			if ($validator->fails())
            {
			   return Response::json($validator->messages());
			}			   
			else
			{		
				$result = DB::table('suspendCompany')
						->where('companyId', Session::get('companyId'))
						->insert($entries);

				if ($result)
				{
					$url="/suspendCompany_step3";
			   		return response()->json([
			   				'result' 	=> $result, 
			   				'status' 	=> 200, 
			   				'url' 		=> $url, 
			   				'message'	=> 'Data inserted successfully'
			   				]);
				}
				else 
				{	
		   			return response()->json([
		   					'result' => $result, 
		   					'status' => 400,  
		   					'message'=>'Data can not inserted'
		   					]);
				}
			}		
	}

}
