<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB, View,Input,Validator,Response ,Session, StdClass, File, Excel;

use Illuminate\Http\Request;

class payrollController extends Controller {


	public function getPayrolluser()
	{
		if (date('m')<4) 
		{
			$year=(intval(date('Y'))-1)."-".(intval(date('y')));
		}
		else
		{
			$year=(intval(date('Y')))."-".(intval(date('y'))+1);
		}
		
		$totalDays=cal_days_in_month(CAL_GREGORIAN, intval(date('m'))-1, date('Y'));

		if (session()->get('complete')=="on") 
		{
			$taxDetail = DB::table('companyTaxDetail')->where('companyId', session()->get('companyId'))->first();

			$months = DB::table('month')->where('id', intval(date('m'))-1)->select('month')->first();
			$month= $months->month;
			$select = DB::table('user')
					->join('personalDetails', 'user.id', '=', 'personalDetails.userId')
					->join('employeeCtc', 'user.id', '=', 'employeeCtc.userId')		
					->join('userProgress', 'user.id', '=', 'userProgress.userId')
					->where('userProgress.complete', '=' , "on")		
					->where('user.companyId', '=' , session()->get('companyId'))					
					->where('user.status', '=' , "Active")
					->where('user.registeration', '=' , "bank")
					->select('user.id', 'user.ctc' ,'personalDetails.userid', 'user.firstName', 'user.lastName', 'user.team', 'user.firstName', 'personalDetails.dateOfBirth')
					->get();

			foreach ($select as $key => $value) 
			{
				$select[$key]->noOfLeave=0;
				$select[$key]->note="";
				$select[$key]->bonus=0;
				$select[$key]->overtime=0;
				$select[$key]->reimbursement=0;
				$select[$key]->commission=0;
				$select[$key]->otherEarning=0;
				$select[$key]->grossPay=0;
				$select[$key]->noOfDays=0;
				$select[$key]->deduction=0;
				$ctc=DB::table('employeeCtc')->where('userid', $value->userid)->first();

				$payroll=DB::table('payroll')->where('userid', $value->userid)->where('month', $month)->where('financialYear', $year)->first();
				

				if (!isset($value->oneDayPay))
				{
					$sum=($ctc->medicalInsurance+$ctc->telephone+$ctc->leaveTravel+$ctc->uniform+$ctc->incentives+$ctc->others);

					$basic = $ctc->basic+$ctc->hra+$ctc->conveyance+$ctc->medicalAllowance;
					$monthlyPay=($basic)/12;
					$oneDayPay=($basic + $sum)/365;
					$insert=DB::table('employeeCtc')->where('userid', $value->userid)->update(['monthlyPay'=> $monthlyPay, 'oneDayPay'=>$oneDayPay, 'sum'=>$sum]);
					$select[$key]->oneDayPay=$oneDayPay;
					$select[$key]->monthlyPay=$monthlyPay;
					$select[$key]->sum=$sum;
				}

				$calendar=DB::table('calendar')->where('userid', $value->userid)->first();

				if ($calendar)
				{
					if(File::exists($calendar->jsonObject)) 
					{
						$object=file_get_contents($calendar->jsonObject);
						$object=json_decode($object);
						
						foreach ($object as $key1 => $value1) 
						{

							if ($value1->title=="Sick Leave")
							{
								$date1 = date_create($value1->start);

								$date2 = date_create($value1->end);
								$dMonth=date_format($date1, 'm');
								$dMonth1=date_format($date2, 'm');

								if ($dMonth==(intval(date('m'))-1) && $dMonth1==(intval(date('m'))-1))
								{
									$diff = date_diff($date1, $date2);
									$select[$key]->noOfLeave+=$diff->days;
								}
								elseif ($dMonth<(intval(date('m'))-1) && $dMonth1==(intval(date('m'))-1)) 
								{
									$select[$key]->noOfLeave+=date_format($date2, 'd')-1;	
								}
								elseif ($dMonth==(intval(date('m'))-1) && $dMonth1>(intval(date('m'))-1)) 
								{
									$select[$key]->noOfLeave+=$totalDays-date_format($date1, 'd')+1;

								}
								elseif ($dMonth<(intval(date('m'))-1) && $dMonth1>(intval(date('m'))-1)) {
									$select[$key]->noOfLeave=$totalDays;
								}
							}
						}
						if ($select[$key]->noOfLeave>$totalDays) {
							$select[$key]->noOfLeave=$totalDays;
						}
					}
				}
				$amt=(($ctc->basic/12)/$totalDays)*($totalDays-$select[$key]->noOfLeave);
				
				if ($taxDetail->epf=='on') 
				{
					if ($ctc->basic<180000) 
					{
						$select[$key]->deduction += ($amt/12)*0.12;
					}
				}
				
				if ($taxDetail->esi=='on') 
				{
					if ($ctc->basic<180000) 
					{
						$select[$key]->deduction += ($amt)*0.0175;
					}
				}

				// $value->medicalInsurance=NULL;
				// $value->telephone=NULL;
				// $value->leaveTravel=NULL;
				// $value->uniform=NULL;
				// $value->gratuity=NULL;
				// $value->superAnnuation=NULL;
				// $value->annualBonus=NULL;
				// $value->festivalBonus=NULL;
				// $value->incentives=NULL;
				// $value->others=NULL;
				// $value->leaveEncashment=NULL;
				// $value->pfContribution=NULL;
				// $value->esiContribution=NULL;

				$reimbursement=DB::table('reimbursement')->where('userId', $value->userid)->where('status', 'Approved')->get();
				$reimburse=0;
				$bonus=0;

				foreach ($reimbursement as $key2 => $value2) 
				{
					$reimburse += $value2->amount;
				}
				$select[$key]->reimbursement=$reimburse;
				$oneTimePayment=DB::table('oneTimePayment')->where('userId', $value->userid)->where('month', $month)->get();
				
				foreach ($oneTimePayment as $key3 => $value3) 
				{
					$bonus += $value3->amount;
				}
				$select[$key]->bonus= $bonus;
				
				
				
				$netPay=$ctc->monthlyPay-($ctc->oneDayPay*$select[$key]->noOfLeave);
				$select[$key]->grossPay=round($netPay+$select[$key]->overtime+$select[$key]->commission+$select[$key]->reimbursement+$select[$key]->bonus+$select[$key]->otherEarning-$select[$key]->deduction+(($select[$key]->sum)/12));
				$select[$key]->deduction=round($select[$key]->deduction);
				$select[$key]->noOfDays=$totalDays-$select[$key]->noOfLeave;
				if (isset($payroll)){
					$select[$key]->noOfLeave=max($select[$key]->noOfLeave, $payroll->noOfLeave);
					$select[$key]->deduction=$payroll->deduction;
					$select[$key]->note=$payroll->note;
					$select[$key]->bonus=$payroll->bonus;
					$select[$key]->overtime=$payroll->overtime;
					$select[$key]->reimbursement=$payroll->reimbursement;
					$select[$key]->commission=$payroll->commission;
					$select[$key]->otherEarning=$payroll->otherEarning;
					$select[$key]->grossPay=$payroll->grossPay;
					$select[$key]->deduction=$payroll->deduction;

				}
			}

			$dates 	= DB::table('leaveSetup')
					->where('companyId', session()->get('companyId'))
					->select('payDay', 'runPayroll')
					->first();

			return View::make('runPayroll2')
					->with('employees', $select)
					->with('dates', $dates);
		}
		else 
		{
			return View::make('runPayroll');
		}
	}

	public function savePayroll()
	{

		$totalDays=cal_days_in_month(CAL_GREGORIAN, intval(date('m'))-1, date('Y'));
		if (date('m')<4) {
			$year=(intval(date('Y'))-1)."-".(intval(date('y')));
		}
		else
			$year=(intval(date('Y')))."-".(intval(date('y'))+1);
		$taxDetail = DB::table('companyTaxDetail')->where('companyId', session()->get('companyId'))->first();
		$months = DB::table('month')->where('id', intval(date('m'))-1)->select('month')->first();
		$month= $months->month;
		$id=Input::get('userId');
		$ctc=DB::table('employeeCtc')->where('userId', $id)->first();
		$ctcForTDS=$ctc->ctc/12;
		$select=DB::table('payroll')->where('userid', $id)->where('month', $month)->where('financialYear', $year)->get();
		if ((Input::get('monthlyPay')==$ctc->monthlyPay) && (Input::get('oneDayPay')==$ctc->oneDayPay) && (Input::get('otherFactorOfCTC')==$ctc->sum)) {
			$netPay=$ctc->monthlyPay-($ctc->oneDayPay*Input::get('noOfLeave'));
			$result=new StdClass;
			$result->grossPay=round($netPay+Input::get('overtime')+Input::get('commission')+Input::get('reimbursement')+Input::get('bonus')+Input::get('otherEarning')-Input::get('deduction')+(Input::get('otherFactorOfCTC')/12));
			$deduction=Input::get('deduction');
				// if ($taxDetail->epf=='on') {
				// 	if ($ctc->basic<180000) {
				// 		$deduction += ($amt/12)*0.12;
				// 	}
				// }
				// if ($taxDetail->esi=='on') {
				// 	if ($ctc->basic<180000) {
				// 		$deduction += ($amt)*0.0175;
				// 	}
				// }
			$result->deduction=round($deduction);

			// ==============================================Deduction=================================
			$taxDetail = DB::table('companyTaxDetail')->where('companyId', session()->get('companyId'))->first();
			// ========================================================================================

			$result->noOfDays=$totalDays-Input::get('noOfLeave');
			$validator = Validator::make(
				$input = array(
					'userID' 	=> Input::get('userId'),
					'companyId' 	=> Session::get('companyId'),
					'month' 		=> $month,
					'financialYear'	=> $year,
					'noOfLeave' 	=> Input::get('noOfLeave'),
					'overtime' 		=> Input::get('overtime'),
					'bonus' 		=> Input::get('bonus'),
					'commission' 	=> Input::get('commission'),
					'otherEarning' 	=> Input::get('otherEarning'),
					'grossPay' 		=> $result->grossPay,
					'reimbursement' => Input::get('reimbursement'),
					'deduction' 	=> $result->deduction,
					'note' 			=> Input::get('note'),
					), 
				array(
					'userID' 		=> 'required',
					'companyId'	=> 'required',
					'month' 		=> 'required',
					'financialYear' 			=> 'required',
					'noOfLeave' 		=> 'required|numeric',
					'grossPay' 			=> 'required|numeric',
					), 
				$messages = array(
					'required' 	=> "This field is required.",
					'numeric' 	=> "This must be a number.",
					)
				);
			 
			 $amt=(($ctc->basic/12)/$totalDays)*($totalDays-Input::get('noOfLeave'));
				if ($taxDetail->epf=='on') {
					if ($ctc->basic<180000) {
						$input['epf'] = ($amt)*0.12;

					}
				}
				if ($taxDetail->esi=='on') {
					if ($ctc->basic<180000) {
						$input['esi'] = ($amt)*0.0175;
					}
				}

			if ($validator->fails()) 
			{
				return Response::json($validator->messages());
			}
			else
			{
				// var_dump($result->deduction);
				// var_dump($input['deduction']);
				// die();
				$insert=DB::table('payroll')->where('userID', Input::get('userId'))->where('month', $month)->where('financialYear', $year)->first();
				if (!empty($insert)){
					$select=DB::table('payroll')->where('userID', Input::get('userId'))->where('month', $month)->where('financialYear', $year)->update($input);
				}
				else
					$select=DB::table('payroll')->insert($input);

				// ===========================TDS===========================
				$tdsCheck=DB::table('TDS')->where('month', $month)->where('userId', Input::get('userId'))->where('financialYear', $year)->first();
				
				// TDS
					$paid=DB::table('payroll')->where('month', $month)->where('userId', Input::get('userId'))->where('financialYear', $year)->first();
					$netTaxable=$result->grossPay;


					$deduction=DB::table('investmentDeduction')->where('userId', Input::get('userId'))->first();
					if (!empty($deduction)){
						$c80=$deduction->LICamount+$deduction->pensionScheme+$deduction->ppf+$deduction->tuitionfee+$deduction->sukanyasamriddhiac+$deduction->postofficetimedeposit+$deduction->nsc+$deduction->fdvy+$deduction->kvp+$deduction->epf+$deduction->hlprepayment;
						$c80=min($c80, 150000);
						$ccc80=$deduction->nps80ccc;
						$ccc80=min($ccc80, 100000);
						$netTaxable+=$ccc80;
						$ccd1=$deduction->pfie80ccd1; 
						$ccd1=min($ccd1, 100000, $ctc->basic*0.10);
						$netTaxable+=min($ccd1+$ccd1+$ccd1, 150000);
						$ccd2=$deduction->pfie80ccd2; 
						$ccd2=min($ccd2, 50000);
						$netTaxable+=$ccd2;
					}


				// $select[$key]->LICamount= $deduction->LICamount;
				// $select[$key]->pensionScheme= $deduction->pensionScheme;
				// $select[$key]->ppf= $deduction->ppf;
				// $select[$key]->tuitionfee= $deduction->tuitionfee;
				// $select[$key]->sukanyasamriddhiac= $deduction->sukanyasamriddhiac;
				// $select[$key]->postofficetimedeposit= $deduction->postofficetimedeposit;
				// $select[$key]->nsc= $deduction->nsc;
				// $select[$key]->ulip= $deduction->ulip;
				// $select[$key]->retirementbplan= $deduction->retirementbplan;
				// $select[$key]->fdvy= $deduction->fdvy;
				// $select[$key]->epf= $deduction->epf;
				// $select[$key]->hlprepayment= $deduction->hlprepayment;
				// $select[$key]->nps80ccc= $deduction->nps80ccc;
				// $select[$key]->pfie80ccd1= $deduction->pfie80ccd1;
				// $select[$key]->pfie80ccd2= $deduction->pfie80ccd2;
				// $select[$key]->other= $deduction->other;


					$medical=DB::table('medicalDeduction')->where('userId', Input::get('userId'))->first();
					if (!empty($medical)){
						$d80=$medical->medicalhimself+$medical->preventivehimself+$medical->medicalparent+$medical->preventiveparent;
						$d80=min($d80, 25000);

						$parentage=$medical->medicalparentage;
						if ($medical->handicapped80DD!='no')
							$dd80limit=$medical->handicapped80DD;
						$dd80=$medical->handicapped80DDamount;
						if ($dd80>75000) {
							$dd80-=75000;
						}
						if ($medical->handicapped80U!='no')
							$u80limit=$medical->handicapped80U;
						$u80=$medical->handicapped80Uamount;
					}


					$allowanceDeclaration=DB::table('allowanceDeclaration')->where('userId', Input::get('userId'))->get();
					if (!empty($allowanceDeclaration)){
						foreach ($allowanceDeclaration as $key4 => $value4) {
							if ($value4->type == 'medical'){
								$medicalLimit= $value4->amount;
							}
							if ($value4->type == 'uniform'){
								$uniformLimit= $value4->amount;
							}
							if ($value4->type == 'driver'){
								$driverLimit= $value4->amount;
							}
							if ($value4->type == 'childrenHostel'){
								$select[$key]->childrenHostelLimit= $value4->amount;
							}
						}
					}
					$HRAmonthlyDeduction=DB::table('HRAmonthlyDeduction')->where('userId', Input::get('userId'))->first();
					$ctcnew=$ctc->ctc/12;
					if (date("m")-1==4){
						$netTaxable=$ctcnew*12;
					}
					if (date("m")-1==5){
						$netTaxable+=$ctcnew*11;
					}
					if (date("m")-1==6){
						$netTaxable+=$ctcnew*10;
					}
					if (date("m")-1==7){
						$netTaxable+=$ctcnew*9;
					}
					if (date("m")-1==8){
						$netTaxable+=$ctcnew*8;
					}
					if (date("m")-1==9){
						$netTaxable+=$ctcnew*7;
					}
					if (date("m")-1==10){
						$netTaxable+=$ctcnew*6;
					}
					if (date("m")-1==11){
						$netTaxable+=$ctcnew*5;
					}
					if (date("m")-1==12){
						$netTaxable+=$ctcnew*4;
					}
					if (date("m")-1==1){
						$netTaxable+=$ctcnew*3;
					}
					if (date("m")-1==2){
						$netTaxable+=$ctcnew*2;
					}
					if (date("m")-1==3){
						$netTaxable+=$ctcnew*1;
					}
					$tax=0;
					
					if ($netTaxable<=250000){
						$tds=0;
					}
					if ($netTaxable<=500000 && $netTaxable>250000){
						$net=$netTaxable-250000;
						$tax=$net*0.10;
						$tax-=2000;
					}
					if ($netTaxable>500000 && $netTaxable<=1000000){
						$net=$netTaxable-500000;
						$tax=($net*0.20)+25000;					
					}
					if ($netTaxable>1000000) {
						$net=$netTaxable-1000000;
						$tax=$net*0.30+125000;
					}
					$tax*=1.03;
					
					$tdsarray=DB::table('TDS')->where('userId', Input::get('userId'))->where('financialYear', $year)->get();
					$tdsPaid=0;
					foreach ($tdsarray as $key5 => $value5) {
						$tdsPaid+=$value5->tdsDeducted;
					}
					if (date("m")-1==4){
					
						$tds=$tax/12;
					}
					if (date("m")-1==5){
						$tds -= $tdsPaid;
						$tds=$tax/11;
					}
					if (date("m")-1==6){
						$tds -= $tdsPaid;
						$tds=$tax/10;
					}
					if (date("m")-1==7){
						$tds -= $tdsPaid;
						$tds = $tax/9;
					}
					if (date("m")-1==8){
						$tds -= $tdsPaid;
						$tds = $tax/8;
					}
					if (date("m")-1==9){
						$tds -= $tdsPaid;
						$tds=$tax/7;
					}
					if (date("m")-1==10){
						$tds-=$tdsPaid;
						$tds=$tax/6;
					}
					if (date("m")-1==11){
						$tds-=$tdsPaid;
						$tds=$tax/5;
					}
					if (date("m")-1==12){
						$tds-=$tdsPaid;
						$tds=$tax/4;
					}
					if (date("m")-1==1){
						$tds-=$tdsPaid;
						$tds=$tax/3;
					}
					if (date("m")-1==2){
						$tds-=$tdsPaid;
						$tds=$tax/2;
					}
					if (date("m")-1==3){
						$tds-=$tdsPaid;
						$tds=$tax/1;
					}
					$tds=max($tds,0);								
					$tdsArray=array(
						'userId' => Input::get('userId'),
						'tdsDeducted' => $tds,
						'month' => $month,
						'financialYear' => $year,
						'companyId' => Session::get('companyId'),
						);
					$tdsSave=DB::table('TDS')->insert($tdsArray);
				




				// ===========================TDS===========================


			}

			return response()->json($result);

		}
		else 
			session()->flush();


	}

	public function generatePayroll()
	{
		if (date('m')<4) {
			$year=(intval(date('Y'))-1)."-".(intval(date('y')));
		}
		else
			$year=(intval(date('Y'))-1)."-".(intval(date('y')));
		if (session()->get('complete')=="on") {
			# code...
		}

		if (session()->get('complete')=="on") 
		{	
			$taxDetail = DB::table('companyTaxDetail')->where('companyId', session()->get('companyId'))->first();
			$months = DB::table('month')->where('id', intval(date('m'))-1)->select('month')->first();
			$month= $months->month;
			$payroll= DB::table('payroll')
			->where('companyId', session()->get('companyId'))
			->where('month', $month)
			->where('financialYear', $year)
				// ->select('month')
			->get();

			if (empty($payroll)) {
				$select = DB::table('user')
				->join('personalDetails', 'user.id', '=', 'personalDetails.userId')
				->join('employeeCtc', 'user.id', '=', 'employeeCtc.userId')		
				->where('user.companyId', '=' , session()->get('companyId'))
				->where('user.type', '=' , "Employee")
				->orWhere('user.type', '=' , "Employer")
				->where('user.status', '=' , "Active")
				->where('user.registeration', '=' , "bank")
				->select('user.id', 'user.ctc' ,'personalDetails.userid', 'user.firstName', 'user.lastName', 'user.team', 'user.firstName', 'personalDetails.dateOfBirth')
				->get();
			}
			else
			{
				$select = DB::table('user')
				->join('personalDetails', 'user.id', '=', 'personalDetails.userId')
				->join('employeeCtc', 'user.id', '=', 'employeeCtc.userId')
				->join('payroll', 'user.id', '=', 'payroll.userID')
				->where('payroll.month', '=' , $month)
				->where('payroll.financialYear', '=' , $year)
				->where('user.companyId', '=' , session()->get('companyId'))
				->where('user.type', '=' , "Employee")
				->orWhere('user.type', '=' , "Employer")
				->where('user.status', '=' , "Active")
				->where('user.registeration', '=' , "bank")
				->select('user.id', 'employeeCtc.userid', 'user.firstName', 'user.lastName', 'user.team','user.ctc' ,'user.firstName', 'personalDetails.dateOfBirth', 'employeeCtc.sum', 'employeeCtc.monthlyPay', 'employeeCtc.oneDayPay','payroll.noOfLeave', 'payroll.overtime', 'payroll.overtime', 'payroll.bonus', 'payroll.commission', 'payroll.otherEarning', 'payroll.grossPay', 'payroll.reimbursement', 'payroll.deduction', 'payroll.note')
				->get();	
			}

			foreach ($select as $key => $value) {
				$ctc=DB::table('employeeCtc')->where('userid', $value->userid)->first();
				if (!isset($value->oneDayPay)){
					$sum=$ctc->medicalInsurance+$ctc->telephone+$ctc->leaveTravel+$ctc->uniform+$ctc->gratuity+$ctc->superAnnuation+$ctc->annualBonus+$ctc->festivalBonus+$ctc->incentives+$ctc->others+$ctc->leaveEncashment+$ctc->pfContribution+$ctc->esiContribution;

					$basic = $ctc->basic+$ctc->hra+$ctc->conveyance+$ctc->medicalAllowance;
					$monthlyPay=($basic)/12;
					$oneDayPay=($basic)/365;
					$insert=DB::table('employeeCtc')->where('userid', $value->userid)->update(['monthlyPay'=> $monthlyPay, 'oneDayPay'=>$oneDayPay, 'sum'=>$sum]);
					$select[$key]->oneDayPay=$oneDayPay;
					$select[$key]->monthlyPay=$monthlyPay;
					$select[$key]->sum=$sum;
				}
				$calendar=DB::table('calendar')->where('userid', $value->userid)->first();
				$select[$key]->noOfLeave=0;
				if ($calendar)
				{
					
					$object=file_get_contents($calendar->jsonObject);
					$object=json_decode($object);
					foreach ($object as $key1 => $value1) {
						if ($value1->title=="Sick Leave"){
							$date1 = date_create($value1->end);

							$date2 = date_create($value1->start);
							$dMonth=date_format($date1, 'm');
							$dMonth1=date_format($date2, 'm');
							if ($dMonth==(intval(date('m'))-1) && $dMonth1==(intval(date('m'))-1)){
								$diff = date_diff($date1, $date2);
								$select[$key]->noOfLeave+=$diff->days;
							}
							elseif ($dMonth<(intval(date('m'))-1) && $dMonth1==(intval(date('m'))-1)) {
								$select[$key]->noOfLeave+=date_format($date2, 'd');	
							}
							elseif ($dMonth==(intval(date('m'))-1) && $dMonth1>(intval(date('m'))-1)) {
								$select[$key]->noOfLeave+=cal_days_in_month(CAL_GREGORIAN, intval(date('m'))-1, date('Y'))-date_format($date1, 'd');

							}
							elseif ($dMonth<(intval(date('m'))-1) && $dMonth1>(intval(date('m'))-1)) {
								$select[$key]->noOfLeave=cal_days_in_month(CAL_GREGORIAN, intval(date('m'))-1, date('Y'));
							}

						}
					}

				}
				$select[$key]->deduction=0;
				if ($taxDetail->epf=='on') {
					if ($ctc->basic<180000) {
						$select[$key]->deduction += ($ctc->basic/12)*0.12;
					}
				}
				if ($taxDetail->esi=='on') {
					if ($ctc->basic<180000) {
						$select[$key]->deduction += ($ctc->basic/12)*0.0175;
					}
				}


				// $value->medicalInsurance=NULL;
				// $value->telephone=NULL;
				// $value->leaveTravel=NULL;
				// $value->uniform=NULL;
				// $value->gratuity=NULL;
				// $value->superAnnuation=NULL;
				// $value->annualBonus=NULL;
				// $value->festivalBonus=NULL;
				// $value->incentives=NULL;
				// $value->others=NULL;
				// $value->leaveEncashment=NULL;
				// $value->pfContribution=NULL;
				// $value->esiContribution=NULL;

				$reimbursement=DB::table('reimbursement')->where('userId', $value->userid)->where('status', 'Approved')->get();
				$reimburse=0;
				$bonus=0;
				foreach ($reimbursement as $key2 => $value2) {
					$reimburse += $value2->amount;
				}
				$select[$key]->reimbursement=$reimburse;
				$oneTimePayment=DB::table('oneTimePayment')->where('userId', $value->userid)->where('month', $month)->get();
				foreach ($oneTimePayment as $key3 => $value3) {
					$bonus += $value3->amount;
				}
				$select[$key]->bonus= $bonus;
				

				


				return View::make('runPayroll1')->with('employees', $select);
			}
		}
		else 
			return View::make('runPayroll');
	}




	public function calculatePayroll()
	{
		if (date('m')<4) {
			$year=((intval(date('Y')-1))."-".(intval(date('y'))));
		}
		else
			$year=intval(date('Y'))."-".intval(date('y')+1);
		$count=Input::get('count');			
		$select = DB::table('month')->where('id', intval(date('m'))-1)->select('month')->first();
		$month= $select->month;
		for ($i=0;$i<$count;$i++){
			$validator = Validator::make(
				$entries[$i] = array(
					'userId' => Input::get('userId'.$i),
					'companyId' => Session::get('companyId'),
					'month' => $month,
					'note' => Input::get('note'.$i),
					'noOfLeave' => Input::get('noOfLeave'.$i),
					'overtime' => Input::get('overtime'.$i),
					'bonus' => Input::get('bonus'.$i),
					'commission' => Input::get('commission'.$i),
					'otherEarning' => Input::get('otherEarning'.$i),
					'grossPay' => Input::get('grossPay'.$i),
					'reimbursement' => Input::get('reimbursement'.$i),
					'deduction' => Input::get('deduction'.$i),
					'financialYear' => $year,
							// 'created_at' => date("d-m-y h:i:sa"),

					),
				array(
					'month' => 'required',
					'noOfLeave' => 'required',
					'grossPay' => 'required'
					),
				$messages = array(
					'required' => 'The :attribute field is required.'
					)
				);

		}
		if ($validator->fails())
		{
			return response()->json($validator->messages());
		   //return Response::json($validator->messages());
		}			   
		else
		{
			foreach ($entries as $key => $value) {
				$select1=NULL;
				$select1= DB::table("payroll")->where('userid', $value['userId'])->where('month', $$value['month'])->where('financialYear', $year)->first();
				if (empty($select1)){
					$select[$key]=DB::table('payroll')->insert($entries[$i]);
				}
				else
					$select[$key]=DB::table('payroll')->where('userid', $entries[$i]['userId'])->where('month', $entries[$i]['month'])->update($entries[$i]);

				$tdsCheck=DB::table('tds')->where('month', intval(date('m')))->where('userId', $value->userid)->first();
				if (empty($tdsCheck)) {

				// TDS
					$paid=DB::table('payroll');
					$netTaxable=$value['grossPay']-$value['reimbursement'];
					$deduction=DB::table('investmentDeduction')->where('userId', $value->userid)->first();
					$select[$key]->c80=$deduction->LICamount+$deduction->pensionScheme+$deduction->ppf+$deduction->tuitionfee+$deduction->sukanyasamriddhiac+$deduction->postofficetimedeposit+$deduction->nsc+$deduction->fdvy+$deduction->kvp+$deduction->epf+$deduction->hlprepayment;
					$select[$key]->c80=min($select[$key]->c80, 150000);
					$select[$key]->ccc80=$deduction->nps80ccc;
					$select[$key]->ccc80=min($select[$key]->ccc80, 100000);
					$netTaxable+=$select[$key]->ccc80;
					$select[$key]->ccd1=$deduction->pfie80ccd1; 
					$select[$key]->ccd1=min($select[$key]->ccd1, 100000, $ctc->basic*0.10);
					$netTaxable+=min($select[$key]->ccd1+$select[$key]->ccd1+$select[$key]->ccd1, 150000);


					$select[$key]->ccd2=$deduction->pfie80ccd2; 
					$select[$key]->ccd2=min($select[$key]->ccd2, 50000);
					$netTaxable+=$select[$key]->ccd2;
				// $select[$key]->LICamount= $deduction->LICamount;
				// $select[$key]->pensionScheme= $deduction->pensionScheme;
				// $select[$key]->ppf= $deduction->ppf;
				// $select[$key]->tuitionfee= $deduction->tuitionfee;
				// $select[$key]->sukanyasamriddhiac= $deduction->sukanyasamriddhiac;
				// $select[$key]->postofficetimedeposit= $deduction->postofficetimedeposit;
				// $select[$key]->nsc= $deduction->nsc;
				// $select[$key]->ulip= $deduction->ulip;
				// $select[$key]->retirementbplan= $deduction->retirementbplan;
				// $select[$key]->fdvy= $deduction->fdvy;
				// $select[$key]->epf= $deduction->epf;
				// $select[$key]->hlprepayment= $deduction->hlprepayment;
				// $select[$key]->nps80ccc= $deduction->nps80ccc;
				// $select[$key]->pfie80ccd1= $deduction->pfie80ccd1;
				// $select[$key]->pfie80ccd2= $deduction->pfie80ccd2;
				// $select[$key]->other= $deduction->other;


					$medical=DB::table('medicalDeduction')->where('userId', $value->userid)->first();
					$select[$key]->d80=$medical->medicalhimself+$medical->preventivehimself+$medical->medicalparent+$medical->preventiveparent;
					$select[$key]->d80=min($select[$key]->d80, 25000);

					$select[$key]->parentage=$medical->medicalparentage;
					if ($medical->handicapped80DD!='no')
						$select[$key]->dd80limit=$medical->handicapped80DD;
					$select[$key]->dd80=$medical->handicapped80DDamount;
					if ($select[$key]->dd80>75000) {
						$select[$key]->dd80-=75000;
					}
					if ($medical->handicapped80U!='no')
						$select[$key]->u80limit=$medical->handicapped80U;
					$select[$key]->u80=$medical->handicapped80Uamount;


					$allowanceDeclaration=DB::table('allowanceDeclaration')->where('userId', $value->userid)->get();
					foreach ($allowanceDeclaration as $key4 => $value4) {
						if ($value4->type == 'medical'){
							$select[$key]->medicalLimit= $value4->amount;
						}
						if ($value4->type == 'uniform'){
							$select[$key]->uniformLimit= $value4->amount;
						}
						if ($value4->type == 'driver'){
							$select[$key]->driverLimit= $value4->amount;
						}
						if ($value4->type == 'childrenHostel'){
							$select[$key]->childrenHostelLimit= $value4->amount;
						}
					}
					$HRAmonthlyDeduction=DB::table('HRAmonthlyDeduction')->where('userId', $value->userid)->first();
				// var_dump($month);
				// var_dump($select);
				// die();
					if ($netTaxable<=250000){
						$tds=0;
					}
					if ($netTaxable<=500000 && $netTaxable>250000){
						$net=$netTaxable-250000;
						$tax=$net*0.10;
					}
					if ($netTaxable>500000 && $netTaxable<=1000000){
						$net=$netTaxable-500000;
						$tax=($net*0.20)+25000;					
					}
					if ($netTaxable>1000000) {
						$net=$netTaxable-1000000;
						$tax=$net*0.30+125000;
					}
					$netTaxable*=1.03;
					$tds=DB::table('tds')->where('userId', $value->userid)->get();
					$tdsPaid=0;
					foreach ($tds as $key5 => $value5) {
						$tdsPaid+=$value5->amount;
					}
					if (intval(date('m'))==4){
						$tds=$tax/12;
					}
					if (intval(date('m'))==5){
						$tds-=$tdsPaid;
						$tds=$tax/11;
					}
					if (intval(date('m'))==6){
						$tds-=$tdsPaid;
						$tds=$tax/10;
					}
					if (intval(date('m'))==7){
						$tds-=$tdsPaid;
						$tds=$tax/9;
					}
					if (intval(date('m'))==8){
						$tds-=$tdsPaid;
						$tds=$tax/8;
					}
					if (intval(date('m'))==9){
						$tds-=$tdsPaid;
						$tds=$tax/7;
					}
					if (intval(date('m'))==10){
						$tds-=$tdsPaid;
						$tds=$tax/6;
					}
					if (intval(date('m'))==11){
						$tds-=$tdsPaid;
						$tds=$tax/5;
					}
					if (intval(date('m'))==12){
						$tds-=$tdsPaid;
						$tds=$tax/4;
					}
					if (intval(date('m'))==1){
						$tds-=$tdsPaid;
						$tds=$tax/3;
					}
					if (intval(date('m'))==2){
						$tds-=$tdsPaid;
						$tds=$tax/2;
					}
					if (intval(date('m'))==3){
						$tds-=$tdsPaid;
						$tds=$tax/1;
					}								
					$tdsArray=array(
						'userId' => $value->userid,
						'amount' => $tds,
						'month' => intval(date('m')),
						);
					$tdsCheck=DB::table('tds')->where('userId', $value->userid)->where('month', $month)->where('financialYear', $year)->first();
					if(!empty($tdsCheck)){
					$tdsSave=DB::table('tds')->where('userId', $value->userid)->where('month', $month)->where('financialYear', $year)->update($tdsArray);						
					}
					else
						$tdsSave=DB::table('tds')->insert($tdsArray);
				}
				else 
					$tds=$tdsCheck->amount;


			}	
			return response()->json($select);
		}
	}

	public function getReview()
	{
		if (date('m')<4) {
			$year=(intval(date('Y'))-1)."-".(intval(date('y')));
		}
		else
			$year=(intval(date('Y')))."-".(intval(date('y'))+1);
		$totalDays=cal_days_in_month(CAL_GREGORIAN, intval(date('m'))-1, date('Y'));
		$months = DB::table('month')->where('id', intval(date('m'))-1)->select('month')->first();
		$month= $months->month;
		$select=DB::table('payroll')
					->join('user', 'user.id', '=', 'payroll.userID')
					->where('payroll.month', $month)->where('payroll.financialYear', $year)->where('payroll.companyId', session()->get('companyId'))
					->select('payroll.*', 'user.firstName', 'user.lastName')
					->get();
		$select1=DB::table('TDS')
					->where('month', $month)->where('financialYear', $year)->where('companyId', session()->get('companyId'))
					->get();
		return View::make('reviewPayroll')->with('employees', $select)->with('TDS', $select1);

	}

	public function payEmployee()
	{
		
		// var_dump($user);
		// die();
		Excel::create('hdfc'.date('M'), function($excel) {

	        // Sheet manipulation
		    $excel->sheet('sheet1', function($sheet) {
				if (date('m')<4) {
				$year=(intval(date('Y'))-1)."-".(intval(date('y')));
				}
				else
					$year=(intval(date('Y'))-1)."-".(intval(date('y')));
				$months = DB::table('month')->where('id', intval(date('m'))-1)->select('month')->first();
				$month= $months->month;
				$totalDays=cal_days_in_month(CAL_GREGORIAN, intval(date('m'))-1, date('Y'));
		    	$user=DB::table('payroll')
					->join('BankDetails', 'BankDetails.userId', '=', 'payroll.userID')
					->where('payroll.month', $month)->where('payroll.financialYear', $year)->where('payroll.companyId', session()->get('companyId'))
					->where('BankDetails.bankName', 'HDFC')->where('payroll.financialYear', $year)->where('payroll.companyId', session()->get('companyId'))
					->select('payroll.*', 'BankDetails.accountNumber')
					->get();	
		    	$sheet->row(1, array(
						     'Account No.', 'DR/CR', 'Amount', 'Narration'
						));
		    	for ($i=0; $i < count($user); $i++) { 
		    	$sheet->row($i+2, array(
						     $user[$i]->accountNumber,'CR' ,$user[$i]->grossPay, ' '
						));
		    	}

		    });

		})->export('xls');
	}
	

// public function tax()
// {
// 		// //limits
// 		// $medicalInsuranceLimit=15000;
// 		// $medicalInsuranceProof="yes";
// 		// $conveyanceAllowance= 19200;
// 		// $leaveTravelLocation="India";
// 		// $leaveTravelTime=4;
// 		// $uniformAllowance=0;
// 		// $assistantAllowance=0;
// 		// $dailyAllowance=0;



// }

}
