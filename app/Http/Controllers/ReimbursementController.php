<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB, Validator, Input, File, View, StdClass;
use Illuminate\Http\Request;

class ReimbursementController extends Controller {

	public function getExpenses()
	{
		$select = DB::table('reimbursement')
				->where('userId', session()->get('employeeId'))
				->where('month', date('M'))
				->get();

		return View::make('employee/employeeExpenses')
				->with('expenses', $select);
	}

	public function updateExpense()
	{
		$action = strtolower(Input::get('action'));
		$expenseId = strtoupper(Input::get('expenseId'));

		if ($action == "delete") 
		{
			// To update an already existing expense/reimbursement
			$select = DB::table('reimbursement')
					->where('expenseId', $expenseId)
					->select();

			$result = new StdClass;

			if (isset($select)&&(!empty($expenseId))) 
			{
				$delete = DB::table('reimbursement')
						->where('expenseId', $expenseId)
						->delete();

				$result->status = '410';
				// $result->statusMessage = $entries['status'];
				$result->message = 'Data deleted successfully.';
				// $result->expenseId = $entries['expenseId'];

				return response()->json($result);
			}
			else 
			{
				$result->status = '304';
				// $result->statusMessage = $entries['status'];
				$result->message = 'No data was modified.';
				// $result->expenseId = $entries['expenseId'];

				return response()->json($result);
			}
		}

		$validator = Validator::make(
			$entries = array(
					'userId' 		=> session()->get('employeeId'),
					'companyId'		=> session()->get('companyId'),
					'dateOfExpense' => Input::get('dateOfExpense'),
					'description' 	=> Input::get('description'),
					'docUrl' 		=> Input::file('doc'),
					'amount' 		=> Input::get('amount'),
					// 'preApproved' => ucfirst(strtolower(Input::get('preApproved'))),
					'expenseId' 	=> Input::get('expenseId'),
					// 'created_at' => date("d-m-y h:i:sa"),
					),
				array(
					'userId' => 'required',
					'dateOfExpense' => 'required',
					'description' => 'required',
					'amount' => 'required|numeric',
					'docUrl' => 'max:500|mimes:jpg,jpeg,png,pdf',
					// 'preApproved' => 'required|string',
					),
				$messages = array(
				   'required' => 'This field is required.',
				   'mimes' => 'The file is not in acceptable format.',
				   'max' => 'The file is bigger than allowed file size.',
				   'numeric' => 'This field must be a number'
				   )
			);

		if ($validator->fails())
        {
        	return response()->json($validator->messages());
		}			   
		else
		{
			$entries['month'] = date('M', strtotime($entries['dateOfExpense']));
			$entries = array_filter($entries);

			$uploadPath = public_path() . '/HTML/Uploads/' . $entries['userId'] . '/Reimbursement' . '/';

			//check if the directory exists
			if(!File::exists($uploadPath)) 
			{
				File::makeDirectory($uploadPath, 0775, true);
			}
			
			$entries['status'] = "Waiting";


			$result = new StdClass;

			if (isset($entries['expenseId'])) 
			{
				if ($action == "edit") 
				{
					// To update an already existing expense/reimbursement
					$select = DB::table('reimbursement')
							->where('expenseId', $entries['expenseId'])
							->first();

					// Move file to upload directory
					if (isset($entries['docUrl'])) 
					{
						$docUrl = $uploadPath . $entries['month'] . '-' . $entries['expenseId'] . '-' . $entries['docUrl']->getClientOriginalName();
						
						if (File::exists($select->docUrl)) 
						{
							unlink($select->docUrl);
						}
						$file = $entries['docUrl']->move($uploadPath, $docUrl);

						$docUrl = explode('public/', $docUrl)[1];
						$entries['docUrl'] = $docUrl;
					}

					if (!empty($select)) {
						$entries['updated_at'] = date("Y-m-d h:i:sa");

						$update = DB::table('reimbursement')
								->where('expenseId', $entries['expenseId'])
								->update($entries);

						$result->status 		= '201';
						$result->statusMessage 	= $entries['status'];
						$result->message 		= 'Data updated successfully.';
						$result->expenseId 		= $entries['expenseId'];
	
						return response()->json($result);
					}
				}
			}
			else
			{
				$select = DB::table('reimbursement')
						->where('userId', $entries['userId'])
						->where('month', $entries['month'])
						->orderBy('id', 'desc')
						->select('expenseId', 'docUrl')
						->first();

				// To make expenseId
				if (empty($select)) 
				{
					$entries['expenseId'] = $entries['userId'] . $entries['month'] . '1';
				}
				else 
				{
					$expenseId = intval(explode($entries['userId'] . $entries['month'], $select->expenseId)[1]);
					$expenseId++;
					$entries['expenseId'] = $entries['userId'] . $entries['month'] .  $expenseId;
				}

				// Move file to upload directory
				if (isset($entries['docUrl'])) 
				{
					$docUrl = $uploadPath . $entries['month'] . '-' . $entries['expenseId'] . '-' . $entries['docUrl']->getClientOriginalName();
					if(File::exists($docUrl)) 
					{
						unlink($docUrl);
						if (!empty($select)) 
						{
							if (File::exists($select->docUrl)) 
							{
								unlink($select->docUrl);
							}
						}
					}
					$file = $entries['docUrl']->move($uploadPath, $docUrl);

					$docUrl = explode('public/', $docUrl)[1];
					$entries['docUrl'] = $docUrl;
				}

				$insert = DB::table('reimbursement')
						->insert($entries);

				$result->status 		= '200';
				$result->statusMessage 	= $entries['status'];
				$result->message 		= 'Data inserted successfully.';
				$result->expenseId 		= $entries['expenseId'];

				return response()->json($result);
			}

			$result->status = '304';
			// $result->statusMessage = $entries['status'];
			$result->message = 'No data was modified 2.';
			// $result->expenseId = $entries['expenseId'];

			return response()->json($result);
		}
	}

	public function addAllowanceDeclaration()
	{
		$validator = Validator::make(
			$entries = array(
				'userId' 				=> session()->get('employeeId'),
				'companyId' 			=> session()->get('companyId'),
				'conveyanceAmount' 		=> Input::get('conveyanceAmount'),
				'uniformAmount' 		=> Input::get('uniformAmount'),
				'driverAmount' 			=> Input::get('driverAmount'),
				'childrenHostelAmount' 	=> Input::get('childrenHostelAmount'),
				'medicalFile' 			=> Input::file('medicalFile'),
				'medicalAmount' 		=> Input::get('medical_reimbursement_amount'),
				),
			array(
				'userId' 				=> 'required',
				'medicalFile' 			=> 'max:500|mimes:jpg,jpeg,png,pdf',
				'conveyanceAmount' 		=> 'numeric',
				'uniformAmount' 		=> 'numeric',
				'driverAmount' 			=> 'numeric',
				'childrenHostelAmount' 	=> 'numeric',
				'medicalAmount' 		=> 'numeric',
				// 'preApproved' => 'required|string',
				),
			$messages = array(
			   'required' 	=> 'This field is required.',
			   'mimes' 		=> 'The file is not in acceptable format.',
			   'max' 		=> 'The file is bigger than allowed file size.',
			   'numeric' 	=> 'This field must be a number'
			   )
			);

		if ($validator->fails())
        {
        	return response()->json($validator->messages());
		}			   
		else
		{	
			if (Input::get('conveyance')!='on')
			{
				$entries0 = array(
					'userId' => session()->get('employeeId'),
					'type' => "conveyance",
					'amount' => Input::get('conveyanceAmount')
					);
			
				$value	= DB::table('allowanceDeclaration')
						->where('userId', session()->get('employeeId'))
						->where('type', 'conveyance')
						->get();

				if (empty($value)) 
				{
					$select0 = DB::table('allowanceDeclaration')
							->insert($entries0);
				}
				else 
					$select0 	= DB::table('allowanceDeclaration')
								->where('userId' , session()->get('employeeId'))
								->where('type', 'conveyance')
								->update($entries0);
			}

			if (Input::get('uniform')!='on')
			{
				$entries1 = array(
					'userId' => session()->get('employeeId'),
					'type' => "uniform",
					'amount' => Input::get('uniformAmount'),
					);
				$value= DB::table('allowanceDeclaration')->where('userId', session()->get('employeeId'))->where('type', 'uniform')->get();
				if (empty($value)) {
					$select1 = DB::table('allowanceDeclaration')->insert($entries1);
				}
				else 
					$select1 = DB::table('allowanceDeclaration')->where('userId' , session()->get('employeeId'))->where('type', 'uniform')->update($entries1);
			}

			if (Input::get('driver')!='on')
			{
				$entries2 = array(
					'userId' => session()->get('employeeId'),
					'type' => "driver",
					'amount' => Input::get('driverAmount'),
					);
				$value= DB::table('allowanceDeclaration')->where('userId', session()->get('employeeId'))->where('type', 'driver')->get();
				if (empty($value)) {
					$select2 = DB::table('allowanceDeclaration')->insert($entries2);
				}
				else 
					$select2 = DB::table('allowanceDeclaration')->where('userId' , session()->get('employeeId'))->where('type', 'driver')->update($entries2);
			}

			if (Input::get('childrenHostel')!='on')
			{
				$entries3 = array(
					'userId' => session()->get('employeeId'),
					'type' => "childrenHostel",
					'amount' => Input::get('childrenHostelAmount'),
					);

				$value= DB::table('allowanceDeclaration')->where('userId', session()->get('employeeId'))->where('type', 'childrenHostel')->get();
				if (empty($value)) {
					$select3 = DB::table('allowanceDeclaration')->insert($entries3);
				}
				else 
					$select3 = DB::table('allowanceDeclaration')->where('userId' , session()->get('employeeId'))->where('type', 'childrenHostel')->update($entries3);
			}

			$entries4 = array(
				'userId' => session()->get('employeeId'),
				'type' => "medical",
				'medicalFile' => Input::file('medicalFile'),
				'amount' => Input::get('medical_reimbursement_amount'),
				);
			
			$uploadPath = public_path() . '/HTML/Uploads/' . $entries['userId'] . '/Reimbursement' . '/';

			//check if the directory exists
			if(!File::exists($uploadPath)) 
			{
				File::makeDirectory($uploadPath, 0775, true);
			}
			if (isset($entries['medicalFile'])) 
			{
				$medicalFile = $uploadPath.'-' . $entries['userId'] . '-' . $entries['medicalFile']->getClientOriginalName();

				if (File::exists($medicalFile))
				{
					unlink($medicalFile);
				}
				$file = $entries['medicalFile']->move($uploadPath, $medicalFile);
				
				$medicalFile = explode('public/', $medicalFile)[1]; 
				$entries4['medicalFile'] = $medicalFile;
			}

			$value 	= DB::table('allowanceDeclaration')
					->where('userId', session()->get('employeeId'))
					->where('type', 'medical')
					->get();
			
			if ($entries4['amount']==NULL)
			{
				$entries4['amount']=-1;
			}

			if (empty($value)) 
			{
				$select4 = DB::table('allowanceDeclaration')
						->insert($entries4);
			}
			else 
			{
				$select4 = DB::table('allowanceDeclaration')
						->where('userId' , session()->get('employeeId'))
						->where('type', 'medical')
						->update($entries4);
			}
		}

		$result = new StdClass;
		$result->status = 200;

		return response()->json($result);

		// return redirect('allowanceDeclaration');
		// return response()->json([$select0, $select1, $select3, $select4]);
	}

		public function viewAllowanceDeclaration()
		{
			$select4	= DB::table('allowanceDeclaration')
						->where('userId' , session()->get('employeeId'))
						->get();

			return View::make('employee/allowanceDeclaration')
					->with('result', $select4);
		}

		public function approveExpense()
		{
			$select = DB::table('reimbursement')
					->where('companyId', session()->get('companyId'))
					->where('status', 'Waiting')
					->orderBy('id', 'desc')
					->select('expenseId', 'docUrl', 'dateOfExpense', 'userId', 'description', 'amount')
					->get();
			$select1 = DB::table('reimbursement')
					->where('companyId', session()->get('companyId'))
					->where('status', 'Approved')
					->orderBy('id', 'desc')
					->select('expenseId', 'docUrl', 'dateOfExpense', 'userId', 'description', 'amount')
					->get();

			return View::make('employerExpenses')->with('pending', $select)->with('approved', $select1);

		}
		public function changeExpenseStatus()
		{
			
			$count=Input::get('varCount');
			$errors=  array();
			for ($i=0; $i<=$count; $i++){
				if (Input::get('approve'.$i)=="on"){
					$validator = Validator::make(
					$entries[$i] = array(
						'expenseId' 		=> Input::get('expenseId'.$i),
						'userId' 				=> Input::get('employeeId'.$i) ,
						'companyId' 			=> session()->get('companyId'),
						'status' 			=> 'Approved',
						),
						array(
							'userId' 				=> 'required',
							'expenseId' 			=> 'required',
						),
					$messages = array(
					   'required' 	=> 'This field is required.',
				   )
				);

				if ($validator->fails())
		        {
		        	$errors[$i]= $validator->messages();
				}			   
				else
				{
					$select=DB::table('reimbursement')->where('expenseId', Input::get('expenseId'.$i))->update(['status' => 'Approved']);
				}
				}
			}
			if (empty($errors))
			{				
				$result=new StdClass;
				$result->status=200;
				$result->url='/employerExpenses';
				return response()->json($result);
			}
			else 
				return response()->json($errors);
		}
	}

