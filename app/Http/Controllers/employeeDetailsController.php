<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator, Input, Redirect, DB, Response, Session, View, Hash, Mail, StdClass, File, Storage;

use Illuminate\Http\Request;

class employeeDetailsController extends Controller 
{
	public function getEmployeeDetails()
	{
		$id 		= Session::get('employeeId');

		$result 	= DB::table('user')
					->where('id', $id)
					->first();

		$result1	= DB::table('personalDetails')
					->where('userid', $id)
					->first();

		$result2	= DB::table('BankDetails')
					->where('userid', $id)
					->get();

		$result3	= DB::table('reimbursement')
					->where('userid', $id)
					->where('month', date('M'))
					->get();

		$result4	= DB::table('paytmId')
					->where('userId', $id)
					->first();

		return View::make('employee/viewDetails')
				->with('result', $result)
				->with('result1', $result1)
				->with('result2', $result2)
				->with('result3', $result3)
				->with('result4', $result4);
	}

	public function updateEmployeeDetails()
	{
		$area = Input::get('area');

		if ($area == "personal") 
		{
			$validator = Validator::make(
				$input = array(
					'altEmail'	=> strtolower(Input::get('altEmail')),
					'phone'		=> Input::get('phone'),
					'updated_at'=> date("Y-m-d h:i:sa"),
				), 
				array(
					'altEmail' 	=> 'email',
					'phone' 	=> 'numeric',
				), 
				$messages = array(
					'required' 	=> 'This field is required.',
					'email' 	=> 'This email is not a valid email.',
					'numeric' 	=> 'The phone number must be numeric.',
				)
			);

			if ($validator->fails()) 
			{
				return Response::json($validator->messages());
			}
			else
			{
				$id		= Session::get('employeeId');
				$result = new StdClass;

				$update = DB::table('personalDetails')
						->where('userid', $id)
						->update($input);

				if ($update) 
				{
					$result->status 	= 200;
					$result->message 	= "Data was updated successfully.";
				}
				return Response::json($result);
			}
		}
		elseif ($area == "address") 
		{
			$validator = Validator::make(
				$input = array(
					'localAddress' 		=> ucwords(strtolower(Input::get('localAddress'))),
					'permanentAddress' 	=> ucwords(strtolower(Input::get('permanentAddress'))),
					'updated_at' 		=> date("Y-m-d h:i:sa"),
					), 
				array(
					'localAddress' 		=> 'required',
					'permanentAddress' 	=> 'required',
					), 
				$messages = array(
					'required' 	=> 'This field is required.',
					)
				);	

			if ($validator->fails()) 
			{
				return Response::json($validator->messages());
			}
			else 
			{
				$id 	= Session::get('employeeId');
				$result	= new StdClass;

				$update = DB::table('personalDetails')
						->where('userid', $id)
						->update($input);

				if ($update) 
				{
					$result->status 	= "200";
					$result->message 	= "Data was updated successfully.";
				}
				return Response::json($result);
			}
		}
		elseif ($area == 'payment') 
		{
			if (Input::get('modeOfPayment1')=='cheque') 
			{
				$check 	= DB::table('BankDetails')
						->where('userId', session()->get('employeeId'))
						->first();

				if (!empty($check)) 
				{
					$delete = DB::table('BankDetails')
							->where('userId', session()->get('employeeId'))
							->delete();
				}

				$checkPaytm = DB::table('paytmId')
							->where('userId', Session::get('employeeId'))
							->first();

				if (!empty($checkPaytm)) 
				{
					$delete = DB::table('paytmId')
							->where('userId', session()->get('employeeId'))
							->delete();
				}

				$checkPrev 	= DB::table('BankDetails')
							->where('userId', session()->get('employeeId'))
							->where('modeOfPayment', 'cheque')
							->first();

				$entries['userId'] 			= session()->get('employeeId');
				$entries['modeOfPayment']	= 'cheque';

				if (empty($checkPrev)) 
				{
					$insert = DB::table('BankDetails')
							->insert($entries);
				}
				else
				{
					$entries['updated_at'] = date('Y-m-d h:i:sa');

					$update = DB::table('BankDetails')
							->where('userId', $entries['userId'])
							->update($entries);
				}

				return response()->json([
		   				'status' => 200, 
		   				'message'=>'Success'
		   				]);	
			}

			if (Input::get('modeOfPayment1')=='directDeposit')
			{
				$validator = Validator::make(
					$entries = array(
							'userid' 		=> Session::get('employeeId'),
							'modeOfPayment' => Input::get('modeOfPayment1'),
							'bankName' 		=> Input::get('bankName1'),
							'branch' 		=> strtoupper(Input::get('branch1')),
							'accountHolder' => strtoupper(Input::get('accountHolder1')),
							'accountNumber' => Input::get('accountNumber1'),
							'accountType' 	=> strtoupper(Input::get('accountType1')),
							'ifsc' 			=> strtoupper(Input::get('ifsc1')),
							'sharePercent' 	=> strtoupper(Input::get('sharePercent1')),
						    
						),
						array(
							'modeOfPayment' => 'required',
							'bankName' 		=> 'required',
							'branch' 		=> 'required',
							'accountHolder' => 'required',
							'accountNumber' => 'required|numeric|regex:/([0-9]){10,}\d/',
							'accountType' 	=> 'required',
							'ifsc' 			=> 'required|alpha_num|size:11|regex:/([A-Za-z]){4}[0][0-9A-Za-z]{6}/',
							'sharePercent' 	=> 'required',
					    	),
					$messages = array(
							   'required'	=> 'The :attribute field is required.',
							   'numeric' 	=> 'The :attribute field must be a number'
							)

					);
			}
			
			$form2 = Input::get('bankName2');
			if (isset($form2))
			{
				$validator1 = Validator::make(
					$entries1 = array(
							'userid' 		=> Session::get('employeeId'),
							'modeOfPayment' => "Direct Deposit",
							'bankName' 		=> Input::get('bankName2'),
							'branch' 		=> strtoupper(Input::get('branch2')),
							'accountHolder' => strtoupper(Input::get('accountHolder2')),
							'accountNumber' => Input::get('accountNumber2'),
							'accountType' 	=> strtoupper(Input::get('accountType2')),
							'ifsc' 			=> strtoupper(Input::get('ifsc2')),
							'sharePercent' 	=> strtoupper(Input::get('sharePercent2')),
						    
						),
						array(
							'userid' 		=> 'required',
							'bankName' 		=> 'required',
							'branch' 		=> 'required',
							'accountHolder' => 'required',
							'accountNumber' => 'required|numeric|regex:/([0-9]){10,}\d/',
							'accountType' 	=> 'required',
							'ifsc' 			=> 'required|alpha_num|size:11|regex:/([A-Za-z]){4}[0][0-9A-Za-z]{6}/',
							'sharePercent' 	=> 'required',
					    	),
					$messages = array(
							   'required' 	=> 'The :attribute field is required.',
							   'numeric' 	=> 'The :attribute field must be a number'
							)

					);
			}

			$form3 = Input::get('paytmId');
			if (isset($form3))
			{
				$validator2 = Validator::make(
					$entries2 = array(
							'userid' 			=> Session::get('employeeId'),
							'companyId' 		=> Session::get('companyId'),
							'paytmId' 			=> Input::get('paytmId'),
							'paytmSharePercent' => strtoupper(Input::get('paytmSharePercent')),
						    
						),
						array(
							'userid' 			=> 'required',
							'paytmId' 			=> 'required',
							'paytmSharePercent' => 'required',
					    	),
					$messages = array(
							   'required' 	=> 'The :attribute field is required.',
							   'numeric' 	=> 'The :attribute field must be a number'
							)
					);
			}

			if ( (isset($validator)and($validator->fails()))||(isset($validator1)and($validator1->fails()))||(isset($validator2)and($validator2->fails())))
	        {
	        	$error = array();
	        	
	        	if (isset($validator))
	        	{
	        		if ($validator->fails()) 
	        		{
		        		$error[0]	=	$validator->messages();
	        		}
	        		else
	        		{
	        			$error[0]	=	 new StdClass;
	        		} 
	        	}
	        	if (isset($validator1)) 
	        	{
	        		if ($validator1->fails()) 
	        		{
		        		$error[1]	=	$validator1->messages();
	        		}
	        		else
	        		{
	        			$error[1]	=	 new StdClass;
	        		} 
	        	}
	        	if (isset($validator2)) 
	        	{
	        		if ($validator2->fails()) 
	        		{
		        		$error[2]	=	$validator2->messages();
	        		}
	        		else
	        		{
	        			$error[2]	=	 new StdClass;
	        		} 
	        	}

			    return $error;
			}
			else
			{
				$check 	= DB::table('BankDetails')
						->where('userId', Session::get('employeeId'))
						->first();

				if (!empty($check)) 
				{
					$delete = DB::table('BankDetails')
							->where('userId', Session::get('employeeId'))
							->delete();				
				}

				$checkPaytm = DB::table('paytmId')
							->where('userId', Session::get('employeeId'))
							->first();

				if (!empty($checkPaytm)) 
				{
					$deletePaytm 	= DB::table('paytmId')
									->where('userId', Session::get('employeeId'))
									->delete();
				}

				if (isset($entries))
				{
					$select=DB::table('BankDetails')
							->where('userid', Session::get('employeeId'))
							->where('modeOfPayment', "directDeposit")
							->first();

					if (empty($select))
					{
						$result = DB::table('BankDetails')
								->insert($entries);										
					}
					else
					{	
						$entries['updated_at'] = date('Y-m-d h:i:sa');
						$result = DB::table('BankDetails')
								->where('userid', Session::get('employeeId'))
								->where('modeOfPayment', "directDeposit")
								->update($entries);
					}
				}
				if (isset($entries1))
				{
					$select1=DB::table('BankDetails')
							->where('userid', Session::get('employeeId'))
							->where('modeOfPayment', "Direct Deposit")
							->first();
					
					if (empty($select1))
					{
						$result1 = DB::table('BankDetails')
							->insert($entries1);										
					}
					else
					{	
						$entries1['updated_at'] = date('Y-m-d h:i:sa');
						$result1 = DB::table('BankDetails')
								->where('userid', Session::get('employeeId'))
								->where('modeOfPayment', "Direct Deposit")
								->update($entries1);
					}
				}
				else 
					$result1=1;

				if (isset($entries2))
				{
					$select2=DB::table('paytmId')
							->where('userId', Session::get('employeeId'))
							->first();

					if (empty($select2))
					{
						$result2 = DB::table('paytmId')
								->insert($entries2);										
					}
					else
						$result2 = DB::table('paytmId')
								->where('userid', Session::get('employeeId'))
								->where('paytmId', $entries2['paytmId'])
								->update($entries2);
				}
				else 
					$result2=1;

				if ($result || $result1 || $result2)
				{
					$select = DB::table('user')
							->where('id', session()->get('employeeId'))
							->update(['registeration'=> 'bank']);
				}
				// if ($result && $result1 && $result2) 
				// {
				// 	$url="/manageEmployees";
				// }
				// else
				// {
				// 	$url="/addEmployees_step4";
				// }

		   		return response()->json([
		   				'result' 	=> $result, 
		   				'result1' 	=> $result1, 
		   				'result2' 	=> $result2, 
		   				'status' 	=> 200, 
		   				// 'url' => $url, 
		   				'message'	=> 'Success'
		   				]);	
		   	}
		}
	}

	public function insertPersonalData()
	{
		if (Input::get('id')){
			session_id(Input::get('id'));
			session_start();
		}
			$validator = Validator::make(
					$entries = array(
							'userid' 			=> Session::get('employeeId'),
							'pan' 				=> strtoupper(Input::get('pan')),
							'dateOfBirth' 		=> Input::get('dateOfBirth'),
							'permanentAddress' 	=> ucwords(strtolower(Input::get('permanentAddress'))),
							'gender' 			=> Input::get('gender'),
							'altEmail' 			=> Input::get('altEmail'),
							'phone' 			=> Input::get('phone'),
						),
						array(
							'userid' 			=> 'required',
							'pan' 				=> 'required|alpha_num|size:10|regex:/([a-zA-Z]){5}([0-9]){4}[a-zdA-Z]/',
							'dateOfBirth' 		=> 'required|date',
							'permanentAddress' 	=> 'required',
							'altEmail' 			=> 'email',
							'gender' 			=> 'required'
					    	),
					$messages = array(
 						   'required' 	=> 'The :attribute field is required.',
 						   'email' 		=> 'Enter a valid email address',
 						   'regex' 		=> 'Enter a valid pan no..'
							)
					);

			$date = date('Y-m-d');
		    $date1 = date_create($date);

		    $date2 = date_create($entries['dateOfBirth']);

		    $diff = date_diff($date1, $date2);
		    $age = $diff->y;

			if ($validator->fails())
            {
			   return Response::json($validator->messages());
			}
			elseif ($age < 16) {
				return Response::json(['dateOfBirth' => ['The age of the employee can not be less than 16 years.']]);
			}
			else
			{
				$gender = $entries['gender'];
				$entries['gender'] = null;
				$entries = array_filter($entries);

				$select = DB::table('personalDetails')->where('userid', Session::get('employeeId'))->first();

				if (empty($select)){
					$result=DB::table('personalDetails')
							->insert($entries);
				}
				else {
					$result=DB::table('personalDetails')->where('userid', Session::get('employeeId'))->update($entries);
				}

				$result1 = DB::table('user')
						->where('id', $entries['userid'])
						->update(array('gender' => $gender));

				if (isset($result) && isset($result1)) 
				{
					$check 	= DB::table('userProgress')
							->where('userid', $entries['userid'])
							->first();

					if (strtolower(session()->get('plan')) == 'benefits') 
					{
						$url="/onboardingdetails1-2";
						$next = 'step1-2';

						session()->put('employeeProgressNextStep', 'step1-2');
						session()->put('employeeProgressComplete', 'off');
					}
					else
					{
						$url="/onboardingdetails2";
						$next = 'step2';

						session()->put('employeeProgressNextStep', 'step2');
						session()->put('employeeProgressComplete', 'off');
					}

					$progress['userid'] 	= $entries['userid'];
					$progress['nextStep']	= $next;
					$progress['complete']	= 'off';

					if (empty($check)) 
					{
						$insertProgress = DB::table('userProgress')
										->insert($progress);
					}
					else
					{
						$progress['updated_at'] = date('Y-m-d h:i:sa');

						$updateProgress = DB::table('userProgress')
										->where('userid', $progress['userid'])
										->update($progress);
					}

					session()->put('employeeProgressNextStep', 'step2');

			   		return response()->json([
			   			'result' 	=> $result, 
			   			'status' 	=> 200, 
			   			'url' 		=> $url, 
			   			'message'	=> 'Data inserted successfully'
			   			]);
				}
				else
			   		return response()->json(['result' => $result, 'status' => 400, 'message'=>'data not inserted']);
			}
	}

	public function getuserinfo($id)
	{
		$select = DB::table('personalDetails')
		 		->where('userid', $id)
	     		->first();

		$result = json_encode($select);
		return $result;
	}

	public function getPersonalData()
	{
		$id=Session::get('employeeId');
		$result= $this->getuserinfo($id);
		return View::make('employee/addEmployees_step1')->with('result' , $result);
	}

	public function insertTaxData()
	{
		$validator = Validator::make(
				$entries = array(
						'userid' 			=> Session::get('employeeId'),
						'taxApplicable' 	=> Input::get('taxApplicable'),
						'PFAcNo' 			=> Input::get('PFAcNo'),
						'esiNumber' 		=> strtoupper(Input::get('esiNumber')),
					    
					),
					array(
						'PFAcNo' 	=> 'size:23|regex:/([A-Za-z]{5})[0-9]{18}/',
						'esiNumber' => 'digits:10|numeric',
						'UAN' 		=> 'digits:12|numeric',
				    	),
				$messages = array(
						   'required' 	=> 'The field is required.',
						)

				);
		if ($validator->fails())
        {
		   return Response::json($validator->messages());
		}			   
		else
		{		
			$result1=DB::table('personalDetails')->where('userid', Session::get('employeeId'))->update($entries);	

			$progress['userid'] 	= $entries['userid'];
			$progress['nextStep']	= 'step3';
			$progress['complete']	= 'off';
			$progress['updated_at']	= date('Y-m-d h:i:sa');

			$updateProgress = DB::table('userProgress')
							->where('userid', $progress['userid'])
							->update($progress);

			session()->put('employeeProgressNextStep', 'step3');
			session()->put('employeeProgressComplete', 'off');

			$result= new StdClass;
			$result->response=$result1;
			$result->status=200;
			$result->message="Data inserted successfully";
			$result->url="/onboardingdetails3";
		   	return response()->json($result);
		}		
	}

	public function getTaxData()
	{
		$id=Session::get('employeeId');
		$user= $this->getUserTaxInfo($id);
		return View::make('employee/addEmployees_step2')->with('user', $user);
	}

	public function getFamilyData()
	{
		if (strtolower(session()->get('type')) == 'employee') 
		{
			if (strtolower(Session::get('plan')) != 'benefits')
			{
				return redirect('employeeDashboard1');	
			}

			$select = DB::table('familyDetails')
					->where('userid', session()->get('employeeId'))
					->get();

			return view('employee/addEmployees_step2-2')
					->with('family', $select);
		}
		else 
		{
			return redirect('login');
		}
	}

	public function getUserTaxInfo($id)
	{
		$select = DB::table('personalDetails')->where('userid', $id)->first();
		return $select;
	}

	public function insertBankData()
	{
		if (Input::get('modeOfPayment1')=='cheque') 
		{
			$check 	= DB::table('BankDetails')
					->where('userId', session()->get('employeeId'))
					->first();

			if (!empty($check)) 
			{
				$delete = DB::table('BankDetails')
						->where('userId', session()->get('employeeId'))
						->delete();
			}

			$checkPaytm = DB::table('paytmId')
						->where('userId', Session::get('employeeId'))
						->first();

			if (!empty($checkPaytm)) 
			{
				$delete = DB::table('paytmId')
						->where('userId', session()->get('employeeId'))
						->delete();
			}

			$checkPrev 	= DB::table('BankDetails')
						->where('userId', session()->get('employeeId'))
						->where('modeOfPayment', 'cheque')
						->first();

			$entries['userId'] 			= session()->get('employeeId');
			$entries['modeOfPayment']	= 'cheque';

			if (empty($checkPrev)) 
			{
				$insert = DB::table('BankDetails')
						->insert($entries);
			}
			else
			{
				$entries['updated_at'] = date('Y-m-d h:i:sa');

				$update = DB::table('BankDetails')
						->where('userId', $entries['userId'])
						->update($entries);
			}

			$progress['userid'] 	= session()->get('employeeId');
			$progress['nextStep']	= 'done';
			$progress['complete']	= 'on';
			$progress['updated_at']	= date('Y-m-d h:i:sa');

			$updateProgress = DB::table('userProgress')
							->where('userid', $progress['userid'])
							->update($progress);

			session()->put('employeeProgressNextStep', 'done');
			session()->put('employeeProgressComplete', 'on');

			return response()->json([
	   				'status' => 200, 
	   				'url' => '/employeeDashboard1',
	   				'message'=>'Success'
	   				]);	
		}

		if (Input::get('modeOfPayment1')=='directDeposit')
		{
			$validator = Validator::make(
				$entries = array(
						'userid' 		=> Session::get('employeeId'),
						'modeOfPayment' => Input::get('modeOfPayment1'),
						'bankName' 		=> Input::get('bankName1'),
						'branch' 		=> strtoupper(Input::get('branch1')),
						'accountHolder' => strtoupper(Input::get('accountHolder1')),
						'accountNumber' => Input::get('accountNumber1'),
						'accountType' 	=> strtoupper(Input::get('accountType1')),
						'ifsc' 			=> strtoupper(Input::get('ifsc1')),
						'sharePercent' 	=> strtoupper(Input::get('sharePercent1')),
					    
					),
					array(
						'modeOfPayment' => 'required',
						'bankName' 		=> 'required',
						'branch' 		=> 'required',
						'accountHolder' => 'required',
						'accountNumber' => 'required|numeric|regex:/([0-9]){10,}\d/',
						'accountType' 	=> 'required',
						'ifsc' 			=> 'required|alpha_num|size:11|regex:/([A-Za-z]){4}[0][0-9A-Za-z]{6}/',
						'sharePercent' 	=> 'required',
				    	),
				$messages = array(
						   'required' 	=> 'The :attribute field is required.',
						   'numeric' 	=> 'The :attribute field must be a number'
						)

				);
		}
		
		$form2 = Input::get('bankName2');
		if (isset($form2))
		{
			$validator1 = Validator::make(
				$entries1 = array(
						'userid' 		=> Session::get('employeeId'),
						'modeOfPayment' => "Direct Deposit",
						'bankName' 		=> Input::get('bankName2'),
						'branch' 		=> strtoupper(Input::get('branch2')),
						'accountHolder' => strtoupper(Input::get('accountHolder2')),
						'accountNumber' => Input::get('accountNumber2'),
						'accountType' 	=> strtoupper(Input::get('accountType2')),
						'ifsc' 			=> strtoupper(Input::get('ifsc2')),
						'sharePercent' 	=> strtoupper(Input::get('sharePercent2')),
					    
					),
					array(
						'userid' 		=> 'required',
						'bankName' 		=> 'required',
						'branch' 		=> 'required',
						'accountHolder' => 'required',
						'accountNumber' => 'required|numeric|regex:/([0-9]){10,}\d/',
						'accountType' 	=> 'required',
						'ifsc' 			=> 'required|alpha_num|size:11|regex:/([A-Za-z]){4}[0][0-9A-Za-z]{6}/',
						'sharePercent' 	=> 'required',
				    	),
				$messages = array(
						   'required' 	=> 'The :attribute field is required.',
						   'numeric' 	=> 'The :attribute field must be a number'
						)

				);
		}

		$form3 = Input::get('paytmId');
		if (isset($form3))
		{
			$validator2 = Validator::make(
				$entries2 = array(
						'userid' 			=> Session::get('employeeId'),
						'companyId' 		=> Session::get('companyId'),
						'paytmId' 			=> Input::get('paytmId'),
						'paytmSharePercent' => strtoupper(Input::get('paytmSharePercent')),
					    
					),
					array(
						'userid' 			=> 'required',
						'paytmId' 			=> 'required',
						'paytmSharePercent' => 'required',
				    	),
				$messages = array(
						   'required' 	=> 'The :attribute field is required.',
						   'numeric' 	=> 'The :attribute field must be a number'
						)

				);
		}

		if ( (isset($validator)and($validator->fails()))||(isset($validator1)and($validator1->fails()))||(isset($validator2)and($validator2->fails())))
        {
        	$error = array();
        	
        	if (isset($validator))
        	{
        		if ($validator->fails()) 
        		{
	        		$error[0]	=	$validator->messages();
        		}
        		else
        		{
        			$error[0]	=	 new StdClass;
        		} 
        	}
        	if (isset($validator1)) 
        	{
        		if ($validator1->fails()) 
        		{
	        		$error[1]	=	$validator1->messages();
        		}
        		else
        		{
        			$error[1]	=	 new StdClass;
        		} 
        	}
        	if (isset($validator2)) 
        	{
        		if ($validator2->fails()) 
        		{
	        		$error[2]	=	$validator2->messages();
        		}
        		else
        		{
        			$error[2]	=	 new StdClass;
        		} 
        	}

		    return $error;
		}
		else
		{
			$check 	= DB::table('BankDetails')
					->where('userId', Session::get('employeeId'))
					->first();

			if (!empty($check)) 
			{
				$delete = DB::table('BankDetails')
						->where('userId', Session::get('employeeId'))
						->delete();				
			}

			$checkPaytm = DB::table('paytmId')
						->where('userId', Session::get('employeeId'))
						->first();

			if (!empty($checkPaytm)) 
			{
				$deletePaytm 	= DB::table('paytmId')
								->where('userId', Session::get('employeeId'))
								->delete();
			}

			if (isset($entries))
			{
				$select=DB::table('BankDetails')
						->where('userid', Session::get('employeeId'))
						->where('modeOfPayment', "directDeposit")
						->first();

				if (empty($select))
				{
					$result = DB::table('BankDetails')
							->insert($entries);										
				}
				else
				{	
					$entries['updated_at'] = date('Y-m-d h:i:sa');
					$result = DB::table('BankDetails')
							->where('userid', Session::get('employeeId'))
							->where('modeOfPayment', "directDeposit")
							->update($entries);
				}
			}
			if (isset($entries1))
			{
				$select1=DB::table('BankDetails')
						->where('userid', Session::get('employeeId'))
						->where('modeOfPayment', "Direct Deposit")
						->first();
				
				if (empty($select1))
				{
					$result1 = DB::table('BankDetails')
						->insert($entries1);										
				}
				else
				{	
					$entries1['updated_at'] = date('Y-m-d h:i:sa');
					$result1 = DB::table('BankDetails')
							->where('userid', Session::get('employeeId'))
							->where('modeOfPayment', "Direct Deposit")
							->update($entries1);
				}
			}
			else 
				$result1=1;

			if (isset($entries2))
			{
				$select2=DB::table('paytmId')
						->where('userId', Session::get('employeeId'))
						->first();

				if (empty($select2))
				{
					$result2 = DB::table('paytmId')
							->insert($entries2);										
				}
				else
					$result2 = DB::table('paytmId')
							->where('userid', Session::get('employeeId'))
							->where('paytmId', $entries2['paytmId'])
							->update($entries2);
			}
			else 
				$result2=1;

			if ($result || $result1 || $result2)
			{
				$select = DB::table('user')
						->where('id', session()->get('employeeId'))
						->update(['registeration'=> 'bank']);
			}
			if ($result && $result1 && $result2) 
			{
				$url="/employeeDashboard1";
				
				$progress['userid'] 	= session()->get('employeeId');
				$progress['nextStep']	= 'done';
				$progress['complete']	= 'on';
				$progress['updated_at']	= date('Y-m-d h:i:sa');

				$updateProgress = DB::table('userProgress')
								->where('userid', $progress['userid'])
								->update($progress);

				session()->put('employeeProgressNextStep', 'done');
				session()->put('employeeProgressComplete', 'on');
			}
			else
			{
				$url="/onboardingdetails3";
			}


	   		return response()->json([
	   				'result' 	=> $result, 
	   				'result1' 	=> $result1, 
	   				'result2' 	=> $result2, 
	   				'status' 	=> 200, 
	   				'url' 		=> $url, 
	   				'message'	=> 'Success'
	   				]);	
	   	}	   	
	}

	public function getBankData()
	{
		$id=Session::get('employeeId');
		$result= $this->getuserinfo($id);
		return View::make('employee/addEmployees_step3')->with($result);
	}

	public function getUserBankInfo($id)
	{
		$select[0] = DB::table('BankDetails')->where('userid', $id)->get();
		$select[1] = DB::table('paytmId')->where('userId', $id)->get();
		return $select;	
	}
// public function insertBankData()
// 	{			
// 		$validator = Validator::make(
// 			$entries = array(
// 					'userid' => Session::get('employeeId'),
// 					'modeOfPayment' => Input::get('modeOfPayment'),
// 					'bankName' => Input::get('bankName'),
// 					'branch' => strtoupper(Input::get('branch')),
// 					'accountHolder' => strtoupper(Input::get('accountHolder')),
// 					'accountNumber' => Input::get('accountNumber'),
// 					'accountType' => strtoupper(Input::get('accountType')),
// 					'ifsc' => strtoupper(Input::get('ifsc')),
// 					'sharePercent' => strtoupper(Input::get('sharePercent')),
				    
// 				),
// 				array(
// 					'modeOfPayment' => 'required',
// 					'bankName' => 'required',
// 					'branch' => 'required',
// 					'accountHolder' => 'required',
// 					'accountNumber' => 'required|numeric|regex:/([0-9]){10,}\d/',
// 					'accountType' => 'required',
// 					'ifsc' => 'required|alpha_num|size:11|regex:/([A-Za-z]){4}[0][0-9A-Za-z]{6}/',
// 					'sharePercent' => 'required',
// 			    	),
// 			$messages = array(
// 					   'required' => 'The :attribute field is required.',
// 					   'numeric' => 'The :attribute field must be a number'
// 					)

// 			);

// 		if ($validator->fails())
//         {
// 		   return Response::json($validator->messages());
// 		}			   
// 		else
// 		{
// 			$result = DB::table('BankDetails')
// 					->where('userid', Session::get('employeeId'))
// 					->update($entries);

// 			if (Input::get('target')=="add") 
// 			{
// 				$url="/employeeDashboard1";
// 			}
// 			else
// 			{
// 				$url="/onboardingdetails3";
// 			}

// 	   		return response()->json([
// 	   				'result' => $result, 
// 	   				'status' => 200, 
// 	   				'url' => $url, 
// 	   				'message'=>'Success'
// 	   				]);	
// 	   	}
// 	}

// 	public function getBankData()
// 	{
// 		$id=Session::get('employeeId');
// 		$result= $this->getuserinfo($id);
// 		return View::make('employee/addEmployees_step3')->with($result);
// 	}

// 	public function getUserBankInfo($id)
// 	{
// 		$select = DB::table('personalDetails')->where('userid', $id)->first();
// 		return $select;	}

	public function investmentDeduction()
	{
		$validator = Validator::make(
				$entries = array(
						'userid' 				=> Session::get('employeeId'),
						'LICamount' 			=> Input::get('LIC_value'),
						'pensionScheme' 		=> Input::get('PS_value'),
						'ppf' 					=> Input::get('PPF_value'),						
						'tuitionfee' 			=> Input::get('TF_value'),
						'sukanyasamriddhiac' 	=> Input::get('SSA_value'),
						'postofficetimedeposit' => Input::get('POTD_value'),
						'nsc' 					=> Input::get('NSC_value'),
						'ulip' 					=> Input::get('ULIP_value'),
						'retirementbplan' 		=> Input::get('RBP_value'),
						'fdvy' 					=> Input::get('FD_value'),
						'infrastructurebonds' 	=> Input::get('IB_value'),
						'kvp' 					=> Input::get('KVP_value'),
						'epf' 					=> Input::get('EPF_value'),
						'hlprepayment' 			=> Input::get('HLPR_value'),
						'nps80ccc' 				=> Input::get('NPS_value'),
						'pfie80ccd1' 			=> Input::get('PFIE_value'),
						'pfie80ccd2' 			=> Input::get('PFIE2_value'),
						'other' 				=> Input::get('AOEI_value'),
					),
					array(
						'LICamount' 			=> 'numeric',
						'pensionScheme' 		=> 'numeric',
						'ppf' 					=> 'numeric',						
						'tuitionfee' 			=> 'numeric',
						'sukanyasamriddhiac' 	=> 'numeric',
						'postofficetimedeposit' => 'numeric',
						'nsc' 					=> 'numeric',
						'ulip' 					=> 'numeric',
						'retirementbplan' 		=> 'numeric',
						'fdvy' 					=> 'numeric',
						'infrastructurebonds' 	=> 'numeric',
						'kvp' 					=> 'numeric',
						'epf' 					=> 'numeric',
						'hlprepayment' 			=> 'numeric',
						'nps80ccc' 				=> 'numeric',
						'pfie80ccd1' 			=> 'numeric',
						'pfie80ccd2' 			=> 'numeric',
						'other' 				=> 'numeric',
					),
				$messages = array(
						   'numeric' 	=> 'This field must be a number.'
						)
				);

		$validator2 = Validator::make(
				$entries2 = array(
						'userid' 					=> Session::get('employeeId'),
						'LICurl' 					=> Input::file('LIC_file'),
						'pensionSchemeurl' 			=> Input::file('PS_file'),
						'ppfurl' 					=> Input::file('PPF_file'),
						'tuitionfeeurl' 			=> Input::file('TF_file'),
						'sukanyasamriddhiacurl' 	=> Input::file('SSA_file'),
						'postofficetimedepositurl' 	=> Input::file('POTD_file'),
						'nscurl' 					=> Input::file('NSC_file'),
						'ulipurl' 					=> Input::file('ULIP_file'),
						'retirementbplanurl' 		=> Input::file('RBP_file'),
						'fdvyurl' 					=> Input::file('FD_file'),
						'infrastructurebondsurl' 	=> Input::file('IB_file'),
						'kvpurl' 					=> Input::file('KVP_file'),
						'epfurl' 					=> Input::file('EPF_file'),
						'hlprepaymenturl' 			=> Input::file('HLPR_file'),
						'nps80cccurl' 				=> Input::file('NPS_file'),
						'pfie80ccd1url' 			=> Input::file('PFIE_file'),
						'pfie80ccd2url' 			=> Input::file('PFIE2_file'),
						'otherurl' 					=> Input::file('AOEI_file'),
					),
					array(
						'LICurl' 				=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'pensionSchemeurl' 			=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'ppfurl' 					=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'tuitionfeeurl' 			=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'sukanyasamriddhiacurl' 	=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'postofficetimedepositurl' 	=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'nscurl' 					=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'ulipurl' 					=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'retirementbplanurl' 		=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'fdvyurl' 					=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'infrastructurebondsurl' 	=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'kvpurl' 					=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'epfurl' 					=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'hlprepaymenturl' 			=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'nps80cccurl' 				=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'pfie80ccd1url' 			=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'pfie80ccd2url' 			=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'otherurl' 					=> 'max:500|mimes:jpg,jpeg,png,pdf',
				    	),
				$messages = array(
						   'mimes' 	=> 'This file is invalid.',
						   'max' 	=> 'This file is bigger than 500 KB.'
						)
				);

		if ($validator->fails())
        {
		   return response()->json($validator->messages());
		}
		elseif ($validator2->fails())
        {
		   return response()->json($validator2->messages());
		}			   
		else
		{
			$entries 	= array_filter($entries);
			$entries2 	= array_filter($entries2);
			$uploadPath = public_path() . '/HTML/Uploads/' . $entries2['userid'] . '/';

			//check if the directory exists
			if(!File::exists($uploadPath)) 
			{
				File::makeDirectory($uploadPath, 0775, true);
			}

			foreach ($entries2 as $key => $value) 
			{
				// if (($key == 'userid')||($key == 'created_at')) 
				// {
				if ($key == 'userid') 
				{
					continue;
				}
				
				if (!empty($value)) 
				{
					$entries2[$key] = $uploadPath . Session::get('employeeId') . '-' . chop($key,"url") . '-' . $value->getClientOriginalName();
					
					if(File::exists($entries2[$key])) 
					{
						unlink($entries2[$key]);
					}
					
					$file = $value->move($uploadPath, $entries2[$key]);

					$entries2[$key]=explode('public/', $entries2[$key])[1];
				}
			}

			$select = DB::table('investmentDeduction')
					->where('userid', Session::get('employeeId'))
					->first();

			if (empty($select)) 
			{
				$insert = DB::table('investmentDeduction')
						->insert($entries);
			}
			else 
			{
				$insert = DB::table('investmentDeduction')
						->where('userid', Session::get('employeeId'))
						->update($entries);
			}

			$select1 = DB::table('investmentDeductionUrl')
					->where('userid', Session::get('employeeId'))
					->first();

			if (empty($select1)) 
			{
				$insert1 = DB::table('investmentDeductionUrl')
						->insert($entries2);
			}
			else 
			{
				$insert1 = DB::table('investmentDeductionUrl')
						->where('userid', Session::get('employeeId'))
						->update($entries2);
			}

			// $select = DB::table('investmentDeduction')->where('userid', Session::get('employeeId'))->first();
			// $select1 = DB::table('investmentDeductionUrl')->where('userid', Session::get('employeeId'))->first();

			$result = new StdClass;
			$result->status = 200;

			// foreach ($select as $key => $value) {
			// 	$result->$key = $value;
			// }
			// foreach ($select1 as $key => $value) {
			// 	$result->$key = $value;
			// }

			
			return response()->json($result);
		}
	}

	public function medicalDeduction()
	{
		$validator = Validator::make(
				$entries = array(
						'userid' 				=> Session::get('employeeId'),
						'medicalhimself' 		=> Input::get('medicalhimself'),
						'preventivehimself' 	=> Input::get('preventivehimself'),
						'medicalparentage' 		=> Input::get('medicalparentage'),
						'medicalparent' 		=> Input::get('medicalparent'),
						'preventiveparent' 		=> Input::get('preventiveparent'),
						'medicalspecialage' 	=> Input::get('medicalspecialage'),
						'medicalspecial' 		=> Input::get('medicalspecial'),
						'handicapped80U' 		=> Input::get('handicapped80U'),
						'handicapped80Uyes' 	=> Input::get('handicapped80Uyes'),
						'handicapped80Uamount' 	=> Input::get('handicapped80Uamount'),
						'handicapped80DD' 		=> Input::get('handicapped80DD'),
						'handicapped80DDyes' 	=> Input::get('handicapped80DDyes'),
						'handicapped80DDamount' => Input::get('handicapped80DDamount'),
					),
					array(
						'medicalhimself' 		=> 'numeric',
						'preventivehimself' 	=> 'numeric',
						'medicalparentage' 		=> 'required|string',
						'medicalparent' 		=> 'numeric',
						'preventiveparent' 		=> 'numeric',
						'medicalspecialage' 	=> 'required|string',
						'medicalspecial' 		=> 'numeric',
						'handicapped80U' 		=> 'required|string',
						'handicapped80Uyes' 	=> 'string',
						'handicapped80Uamount' 	=> 'numeric',
						'handicapped80DD' 		=> 'required|string',
						'handicapped80DDyes' 	=> 'string',
						'handicapped80DDamount' => 'numeric',
					),
				$messages = array(
						   'numeric' 	=> 'This field must be a number.',
						   'required' 	=> 'This field is required.'
						)
				);

		$validator2 = Validator::make(
				$entries2 = array(
						'userid' => Session::get('employeeId'),
						'medicalhimself' 	=> Input::file('medicalhimself_file'),
						'preventivehimself' => Input::file('preventivehimself_file'),
						'medicalparent' 	=> Input::file('medicalparent_file'),
						'preventiveparent' 	=> Input::file('preventiveparent_file'),
						'medicalspecial' 	=> Input::file('medicalspecial_file'),
						'handicapped80U' 	=> Input::file('handicapped80U_file'),
						'handicapped80DD' 	=> Input::file('handicapped80DD_file'),

						// 'created_at' => date("d-m-y h:i:sa"),
					),
					array(
						'medicalhimself' 	=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'preventivehimself' => 'max:500|mimes:jpg,jpeg,png,pdf',
						'medicalparent' 	=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'preventiveparent' 	=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'medicalspecial' 	=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'handicapped80U' 	=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'handicapped80DD' 	=> 'max:500|mimes:jpg,jpeg,png,pdf',
				    	),
				$messages = array(
						   'mimes' 	=> 'This file is invalid.',
						   'max' 	=> 'This file is bigger than 500 KB.'
						)
				);

		if ($validator->fails())
        {
		   return response()->json($validator->messages());
		}
		elseif ($validator2->fails())
        {
		   return response()->json($validator2->messages());
		}			   
		else
		{
			if ($entries['handicapped80U'] == 'yes') {
				$entries['handicapped80U'] = $entries['handicapped80Uyes'];
				$entries['handicapped80Uyes'] = null;
			}

			if ($entries['handicapped80DD'] == 'yes') {
				$entries['handicapped80DD'] = $entries['handicapped80DDyes'];
				$entries['handicapped80DDyes'] = null;
			}

			$entries = array_filter($entries);
			$entries2 = array_filter($entries2);
			$uploadPath = public_path() . '/HTML/Uploads/' . $entries['userid'] . '/';

			//check if the directory exists
			if(!File::exists($uploadPath)) {
				File::makeDirectory($uploadPath, 0775, true);
			}

			foreach ($entries2 as $key => $value) 
			{
				// if (($key == 'userid')||($key == 'created_at')) {
				if ($key == 'userid') 
				{
					continue;
				}
				
				if (!empty($value)) 
				{
					$entries2[$key] = $uploadPath . Session::get('employeeId') . '-' . $key . '-' . $value->getClientOriginalName();
			
					if(File::exists($entries2[$key])) 
					{
						unlink($entries2[$key]);
					}
			
					$file = $value->move($uploadPath, $entries2[$key]);
					
					$entries2[$key]=explode('public/', $entries2[$key])[1];
				}
			}

			$select = DB::table('medicalDeduction')
					->where('userid', Session::get('employeeId'))
					->first();

			if (empty($select)) 
			{
				$insert 	= DB::table('medicalDeduction')
							->insert($entries);
			}
			else 
			{
				$insert 	= DB::table('medicalDeduction')
							->where('userid', Session::get('employeeId')) 
							->update($entries);
			}

			$select1 	= DB::table('medicalDeductionUrl')
						->where('userid', Session::get('employeeId'))
						->first();

			if (empty($select1)) 
			{
				$insert1 	= DB::table('medicalDeductionUrl')
							->insert($entries2);
			}
			else 
			{
				$insert1 	= DB::table('medicalDeductionUrl')
							->where('userid', Session::get('employeeId'))
							->update($entries2);
			}

			// $select = DB::table('medicalDeduction')->where('userid', Session::get('employeeId'))->first();
			// $select1 = DB::table('medicalDeductionUrl')->where('userid', Session::get('employeeId'))->first();

			$result = new StdClass;
			$result->status = 200;

			// foreach ($select as $key => $value) 
			// {
				// $result->$key = $value;
			// }
			// foreach ($select1 as $key => $value) 
			// {
				// $result->$key = $value;
			// }
			
			return response()->json($result);
		}
	}

	public function HRAmonthlyDeduction()
	{
		$validator = Validator::make(
				$entries = array(
						'userid' 		=> Session::get('employeeId'),
						'April' 		=> Input::get('April_amount'),
						'May' 			=> Input::get('May_amount'),
						'June' 			=> Input::get('June_amount'),
						'July' 			=> Input::get('July_amount'),
						'August' 		=> Input::get('August_amount'),
						'September' 	=> Input::get('September_amount'),
						'October'		=> Input::get('October_amount'),
						'November'		=> Input::get('November_amount'),
						'December'		=> Input::get('December_amount'),
						'January' 		=> Input::get('January_amount'),
						'February' 		=> Input::get('February_amount'),
						'March' 		=> Input::get('March_amount'),
						'landlordPAN' 	=> Input::get('landlord_pan_value'),
						'total' 		=> Input::get('total_amount'),
						// 'created_at'	=> date("d-m-y h:i:sa"),
					),
					array(
						'April' 		=> 'numeric',
						'May' 			=> 'numeric',
						'June' 			=> 'numeric',
						'July' 			=> 'numeric',
						'August' 		=> 'numeric',
						'September' 	=> 'numeric',
						'October' 		=> 'numeric',
						'November' 		=> 'numeric',
						'December' 		=> 'numeric',
						'January' 		=> 'numeric',
						'February' 		=> 'numeric',
						'March' 		=> 'numeric',
						'landlordPAN' 	=> 'required|alpha_num|size:10|regex:/([a-zA-Z]){5}([0-9]){4}([a-zA-Z])/',
						'total' 		=> 'numeric',
					),
				$messages = array(
					'numeric' 	=> 'The :attribute field must be a number.',
					'required' 	=> 'This field is required.',
					'size' 		=> 'PAN must be of length 10.',
					'regex' 	=> 'This is not a valid PAN.',
					)
				);

		$validator2 = Validator::make(
				$entries2 = array(
						'userid' 		=> Session::get('employeeId'),
						'April' 		=> Input::file('April_file'),
						'May' 			=> Input::file('May_file'),
						'June' 			=> Input::file('June_file'),
						'July' 			=> Input::file('July_file'),
						'August' 		=> Input::file('August_file'),
						'September' 	=> Input::file('September_file'),
						'October' 		=> Input::file('October_file'),
						'November' 		=> Input::file('November_file'),
						'December' 		=> Input::file('December_file'),
						'January' 		=> Input::file('January_file'),
						'February' 		=> Input::file('February_file'),
						'March' 		=> Input::file('March_file'),
						'landlordPAN' 	=> Input::file('landlord_pan_file'),
						// 'created_at' => date("d-m-y h:i:sa"),
					),
					array(
						'April' 		=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'May' 			=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'June' 			=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'July' 			=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'August' 		=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'September' 	=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'October' 		=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'November' 		=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'December' 		=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'January' 		=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'February' 		=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'March' 		=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'landlordPAN' 	=> 'max:500|mimes:jpg,jpeg,png,pdf',
					),
				$messages = array(
					   'mimes' 	=> 'The file is invalid.',
					   'max' 	=> 'The :attribute file is bigger than 500 KB.'
					)
				);

		if ($validator->fails())
        {
		   return View::make('employee/hraMonthlyDetails')
		   				->with('errors1', json_encode($validator->messages()));
		}
		elseif ($validator2->fails())
        {
        	return View::make('employee/hraMonthlyDetails')
		   				->with('errors2', $validator2->messages());
		}			   
		else
		{
			$month = date('m');
			
			if ($month > '03') 
			{
				$year 		= date('Y');
				$nextYear 	= date('Y', strtotime('+1 year'));
				$entries['year'] 	= $year . '-' .  substr($nextYear, 2,4);
				$entries2['year'] 	= $year . '-' .  substr($nextYear, 2,4);
			}
			
			$entries 	= array_filter($entries);
			$entries2 	= array_filter($entries2);
			$uploadPath = public_path() . '/HTML/Uploads/' . $entries['userid'] . '/';

			//check if the directory exists
			if(!File::exists($uploadPath)) 
			{
				File::makeDirectory($uploadPath, 0775, true);
			}

			foreach ($entries2 as $key => $value) 
			{
				// if (($key == 'userid')||($key == 'created_at')||($key == 'year')) 
				// {
				if (($key == 'userid')||($key == 'year')) 
				{
					continue;
				}
				
				if (!empty($value)) 
				{
					$entries2[$key] = $uploadPath . Session::get('employeeId') . '-Rent-' . $key . '-' . $value->getClientOriginalName();
				
					if(File::exists($entries2[$key])) 
					{
						unlink($entries2[$key]);
					}
					
					$file = $value->move($uploadPath, $entries2[$key]);

					$entries2[$key]=explode('public/', $entries2[$key])[1];
				}
			}

			$select = DB::table('HRAmonthlyDeduction')
					->where('userid', Session::get('employeeId'))
					->first();

			if (empty($select)) 
			{
				$insert = DB::table('HRAmonthlyDeduction')
						->insert($entries);
			}
			else 
			{
				$insert = DB::table('HRAmonthlyDeduction')
						->where('userid', Session::get('employeeId')) 
						->update($entries);
			}

			$select1 	= DB::table('HRAmonthlyDeductionUrl')
						->where('userid', Session::get('employeeId'))
						->first();

			if (empty($select1)) 
			{
				$insert1 = DB::table('HRAmonthlyDeductionUrl')
						->insert($entries2);
			}
			else 
			{
				$insert1 = DB::table('HRAmonthlyDeductionUrl')
						->where('userid', Session::get('employeeId'))
						->update($entries2);
			}

			// $select = DB::table('HRAmonthlyDeduction')->where('userid', Session::get('employeeId'))->first();
			// $select1 = DB::table('HRAmonthlyDeductionUrl')->where('userid', Session::get('employeeId'))->first();

			// return View::make('employee/hraMonthlyDetails')
			// 		->with('result', $select)
			// 		->with('result1', $select1);

			$result = new StdClass;
			$result->status = 200;

			// foreach ($select as $key => $value) 
			// {
				// $result->$key = $value;
			// }
			// foreach ($select1 as $key => $value) 
			// {
				// $result->$key = $value;
			// }
			
			return response()->json($result);
		}
	}

	public function otherDeduction()
	{
		$validator = Validator::make(
				$entries = array(
						'userid' 	=> Session::get('employeeId'),
						'OD80CCG' 	=> Input::get('80CCG_amount'),
						'OD80E' 	=> Input::get('80E_amount'),
						'OD80G' 	=> Input::get('80G_amount'),
						'OD80GGC' 	=> Input::get('80GGC_amount'),
						'OD80TTA' 	=> Input::get('80TTA_amount'),
					),
					array(
						'OD80CCG' 	=> 'numeric',
						'OD80E' 	=> 'numeric',
						'OD80G' 	=> 'numeric',
						'OD80GGC' 	=> 'numeric',
						'OD80TTA' 	=> 'numeric',
					),
				$messages = array(
						   'numeric' => 'The :attribute field must be a number.'
						)
				);

		$validator2 = Validator::make(
				$entries2 = array(
						'userid' 	=> Session::get('employeeId'),
						'OD80CCG' 	=> Input::file('80CCG_file'),
						'OD80E' 	=> Input::file('80E_file'),
						'OD80G' 	=> Input::file('80G_file'),
						'OD80GGC' 	=> Input::file('80GGC_file'),
						'OD80TTA' 	=> Input::file('80TTA_file'),
					),
					array(
						'OD80CCG' 	=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'OD80E' 	=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'OD80G' 	=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'OD80GGC' 	=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'OD80TTA' 	=> 'max:500|mimes:jpg,jpeg,png,pdf',
					),
				$messages = array(
						   'mimes' 	=> 'The file is invalid.',
						   'max' 	=> 'The :attribute file is bigger than 500 KB.'
					)
				);

		if ($validator->fails())
        {
		   return response()->json($validator->messages());
		}
		elseif ($validator2->fails())
        {
		   return response()->json($validator2->messages());
		}			   
		else
		{
			$entries 	= array_filter($entries);
			$entries2 	= array_filter($entries2);
			$uploadPath = public_path() . '/HTML/Uploads/' . $entries['userid'] . '/';

			//check if the directory exists
			if(!File::exists($uploadPath)) 
			{
				File::makeDirectory($uploadPath);
			}

			foreach ($entries2 as $key => $value) 
			{
				// if (($key == 'userid')||($key == 'created_at')) 
				// {
				
				if ($key == 'userid') 
				{
					continue;
				}
				
				if (!empty($value)) 
				{
					$entries2[$key] = $uploadPath . Session::get('employeeId') . '-' . $key . '-' . $value->getClientOriginalName();
					
					if(File::exists($entries2[$key])) 
					{
						unlink($entries2[$key]);
					}
					
					$file = $value->move($uploadPath, $entries2[$key]);

					$entries2[$key]=explode('public/', $entries2[$key])[1];
				}
			}

			$select = DB::table('otherDeduction')
					->where('userid', Session::get('employeeId'))
					->first();

			if (empty($select)) 
			{
				$insert = DB::table('otherDeduction')
						->insert($entries);
			}
			else 
			{
				$insert = DB::table('otherDeduction')
						->where('userid', Session::get('employeeId')) 
						->update($entries);
			}

			$select1 	= DB::table('otherDeductionUrl')
						->where('userid', Session::get('employeeId'))
						->first();

			if (empty($select1)) 
			{
				$insert1 = DB::table('otherDeductionUrl')
						->insert($entries2);
			}
			else 
			{
				$insert1 = DB::table('otherDeductionUrl')
						->where('userid', Session::get('employeeId'))
						->update($entries2);
			}

			// $select 	= DB::table('otherDeduction')
			// 			->where('userid', Session::get('employeeId'))
			// 			->first();

			// $select1 	= DB::table('otherDeductionUrl')
			// 			->where('userid', Session::get('employeeId'))
			// 			->first();

			// return View::make('employee/otherDeduction')
			// 		->with('result', $select)
			// 		->with('result1', $select1);

			$result = new StdClass;

			if ($insert or $insert1) 
			{
				$result->status = 200;
			}

			// foreach ($select as $key => $value) 
			// {
				// $result->$key = $value;
			// }
			// foreach ($select1 as $key => $value) 
			// {
				// $result->$key = $value;
			// }
			
			return response()->json($result);
		}
	}

	public function selfOccupiedDeduction()
	{
		$validator = Validator::make(
				$entries = array(
						'userid' 			=> Session::get('employeeId'),
						'selfOccupied' 		=> Input::get('selfOccupied_amount'),
						'loanDeductable' 	=> Input::get('loan_deductable'),
						'loanAmount' 		=> Input::get('loanDate_amount'),
						// 'created_at' => date("d-m-y h:i:sa"),
					),
					array(
						'selfOccupied' 		=> 'numeric',
						'loanDeductable' 	=> 'required|string',
						'loanAmount' 		=> 'numeric',
					),
				$messages = array(
						   'required' 	=> 'The :attribute field is required.',
						   'numeric' 	=> 'The :attribute field must be a number.',
						   'string' 	=> 'The :attribute field must be a string.',
						)
				);

		$validator2 = Validator::make(
				$entries2 = array(
						'userid' 		=> Session::get('employeeId'),
						'selfOccupied' 	=> Input::file('selfOccupied_file'),
						// 'created_at' => date("d-m-y h:i:sa"),
					),
					array(
						'selfOccupied' => 'max:500|mimes:jpg,jpeg,png,pdf',
					),
				$messages = array(
						   'mimes' 	=> 'The file is invalid.',
						   'max' 	=> 'The :attribute file is bigger than 500 KB.'
					)
				);

		if ($validator->fails())
        {
		   return response()->json($validator->messages());
		}
		elseif ($validator2->fails())
        {
		   return response()->json($validator2->messages());
		}			   
		else
		{
			$entries = array_filter($entries);
			$entries2 = array_filter($entries2);
			$uploadPath = public_path() . '/HTML/Uploads/' . $entries['userid'] . '/';

			//check if the directory exists
			if(!File::exists($uploadPath)) {
				File::makeDirectory($uploadPath);
			}

			foreach ($entries2 as $key => $value) {
				// if (($key == 'userid')||($key == 'created_at')) {
				if ($key == 'userid') {
					continue;
				}
				if (!empty($value)) {
					$entries2[$key] = $uploadPath . Session::get('employeeId') . '-' . $key . '-' . $value->getClientOriginalName();
					if(File::exists($entries2[$key])) {
						unlink($entries2[$key]);
					}
					$file = $value->move($uploadPath, $entries2[$key]);

					$entries2[$key]=explode('public/', $entries2[$key])[1];
				}
			}

			$select = DB::table('selfOccupiedDeduction')->where('userid', Session::get('employeeId'))->first();

			if (empty($select)) {
				$insert = DB::table('selfOccupiedDeduction')->insert($entries);
			}
			else {
				$insert = DB::table('selfOccupiedDeduction')->where('userid', Session::get('employeeId')) ->update($entries);
			}

			$select1 = DB::table('selfOccupiedDeductionUrl')->where('userid', Session::get('employeeId'))->first();
			if (empty($select1)) {
				$insert1 = DB::table('selfOccupiedDeductionUrl')->insert($entries2);
			}
			else {
				$insert1 = DB::table('selfOccupiedDeductionUrl')->where('userid', Session::get('employeeId'))->update($entries2);
			}

			// $select = DB::table('selfOccupiedDeduction')->where('userid', Session::get('employeeId'))->first();
			// $select1 = DB::table('selfOccupiedDeductionUrl')->where('userid', Session::get('employeeId'))->first();

			$result = new StdClass;
			$result->status = 200;

			// foreach ($select as $key => $value) {
			// 	$result->$key = $value;
			// }
			// foreach ($select1 as $key => $value) {
			// 	$result->$key = $value;
			// }

			return response()->json($result);
		}
	}

	public function previousEmployerDeduction()
	{
		$input = Input::all();
		$validator = Validator::make(
				$entries = array(
						'userid' 	=> Session::get('employeeId'),
						'income' 	=> Input::get('income_amount'),
						'pf' 		=> Input::get('PF_amount'),
						'tds' 		=> Input::get('TDS_amount'),
						// 'created_at' => date("d-m-y h:i:sa"),
					),
					array(
						'income' 	=> 'numeric',
						'pf' 		=> 'numeric',
						'tds' 		=> 'numeric',
					),
				$messages = array(
					   'numeric' 	=> 'The :attribute field must be a number.',
					)
				);

		$validator2 = Validator::make(
				$entries2 = array(
						'userid' 	=> Session::get('employeeId'),
						'income' 	=> Input::file('income_file'),
						'pf' 		=> Input::file('PF_file'),
						'tds' 		=> Input::file('TDS_file'),
						// 'created_at' => date("d-m-y h:i:sa"),
					),
					array(
						'income' 	=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'pf' 		=> 'max:500|mimes:jpg,jpeg,png,pdf',
						'tds' 		=> 'max:500|mimes:jpg,jpeg,png,pdf',
					),
				$messages = array(
					   'mimes' 	=> 'The file is invalid.',
					   'max' 	=> 'The :attribute file is bigger than 500 KB.'
					)
				);

		if ($validator->fails())
        {
		   return response()->json($validator->messages());
		}
		elseif ($validator2->fails())
        {
		   return response()->json($validator2->messages());
		}			   
		else
		{
			if (!isset($entries['userid'])) 
			{
				return View::make('employee/incomeFromPreviousEmployer');
			}
			$entries = array_filter($entries);
			$entries2 = array_filter($entries2);
			$uploadPath = public_path() . '/HTML/Uploads/' . $entries['userid'] . '/';

			//check if the directory exists
			if(!File::exists($uploadPath)) {
				File::makeDirectory($uploadPath, 0775, true);
			}

			foreach ($entries2 as $key => $value) {
				// if (($key == 'userid')||($key == 'created_at')) {
				if ($key == 'userid') {
					continue;
				}
				if (!empty($value)) {
					$entries2[$key] = $uploadPath . Session::get('employeeId') . '-' . $key . '-' . $value->getClientOriginalName();
					if(File::exists($entries2[$key])) {
						unlink($entries2[$key]);
					}
					$file = $value->move($uploadPath, $entries2[$key]);

					$entries2[$key]=explode('public/', $entries2[$key])[1];
				}
			}

			$select = DB::table('previousEmployerDeduction')->where('userid', Session::get('employeeId'))->first();

			if (empty($select)) {
				$insert = DB::table('previousEmployerDeduction')->insert($entries);
			}
			else {
				$insert = DB::table('previousEmployerDeduction')->where('userid', Session::get('employeeId')) ->update($entries);
			}

			$select1 = DB::table('previousEmployerDeductionUrl')->where('userid', Session::get('employeeId'))->first();
			if (empty($select1)) {
				$insert1 = DB::table('previousEmployerDeductionUrl')->insert($entries2);
			}
			else {
				$insert1 = DB::table('previousEmployerDeductionUrl')->where('userid', Session::get('employeeId'))->update($entries2);
			}

			$select = DB::table('previousEmployerDeduction')->where('userid', Session::get('employeeId'))->first();
			$select1 = DB::table('previousEmployerDeductionUrl')->where('userid', Session::get('employeeId'))->first();

			$result = new StdClass;
			$result->status = 200;

			// foreach ($select as $key => $value) {
			// 	$result->$key = $value;
			// }
			// foreach ($select1 as $key => $value) {
			// 	$result->$key = $value;
			// }

			return response()->json($result);
		}
	}

	public function editEmployeeDetails()
	{
		$area = strtolower(Input::get('area'));

		// This function is just a template for now, table names, input names and some other things also need to be changed
		if ($area == 'personal') 
		{
			$validator = Validator::make(
				$input = array(
					'id' 			=> Session::get('employeeId'),
					'firstName' 	=> ucwords(strtolower(Input::get('firstName'))),
					'lastName' 		=> ucwords(strtolower(Input::get('lastName'))),
					'pan' 			=> strtoupper(Input::get('pan')),
					'dateOfBirth' 	=> Input::get('dateOfBirth'),
					'dateOfJoining' => Input::get('dateOfJoining'),
					'email' 		=> strtolower(Input::get('email')),
					'altEmail' 		=> strtolower(Input::get('altEmail')),
					'phone' 		=> Input::get('phone'),
					'updated_at' 	=> date("Y-m-d h:i:sa")
					), 
				array(
					'firstName' 	=> 'required|string',
					'lastName' 		=> 'required|string',
					'pan' 			=> 'alpha_num|size:10|regex:/([a-zA-Z]){5}([0-9]){4}([a-zA-Z])/',
					'dateOfBirth' 	=> 'required|date',
					'dateOfJoining'	=> 'required|date',
					'email' 		=> 'required|email',
					'altEmail' 		=> 'email',
					'phone' 		=> 'numeric',
					), 
				$messages = array(
					'required' 	=> 'This field is required.',
					'string' 	=> 'This field must be a string.',
					'size' 		=> 'The PAN must be of length 10',
					'regex' 	=> 'This PAN is not in valid format.',
					'email' 	=> 'This email doesn\'t seem to be in a valid format.',
					'numeric' 	=> 'This must be numberic.',
					'date' 		=> 'This date doesn\'t seem in the right format.',
					)
				);	

			$date = date('Y-m-d');
		    $date1 = date_create($date);

		    $date2 = date_create($input['dateOfBirth']);

		    $diff = date_diff($date1, $date2);
		    $age = $diff->y;

			if ($age < 16) {
				return Response::json(['dateOfBirth' => ['The age of the employee can not be less than 16 years.']]);
			}

			if ($validator->fails()) 
			{
				return Response::json($validator->messages());
			}
			else 
			{
				$result	= new StdClass;
				
				if ($input['email'] == $input['altEmail']) 
				{
					$result->status 	= "406b";
					$result->message 	= "Primary and alternative emails can not be same.";
					return Response::json($result);
				}
				$checkEmail = DB::table('user')
							->where('email', $input['email'])
							->select('id')
							->first();

				if (isset($checkEmail->id)) 
				{
					if ($checkEmail->id != $input['id']) 
					{
						$result->status 	= "406a"; 
						$result->message 	= "That email address is already occupied.";
						return Response::json($result);
					}
				}
				else 
				{
					$input['apiKey'] = Hash::make($input['email']);
				}

				// $checkAltEmail 	= DB::table('personalDetails')
				// 				->where('altEmail', $input['altEmail'])
				// 				->select('userid', 'altEmail')
				// 				->first();

				// if (isset($checkAltEmail->userid)) 
				// {
				// 	if ($checkAltEmail->userid != $input['id'] ) 
				// 	{
				// 		$result->status 	= "406b";
				// 		$rseult->message 	= "That email address is occupied.";
				// 		return $result;
				// 	}
				// }

				if (isset($input['apiKey'])) 
				{
					$userUpdate = DB::table('user')
								->where('id', $input['id'])
								->update([
									'firstName'		=> $input['firstName'],
									'lastName' 		=> $input['lastName'],
									'joinDate' 		=> $input['dateOfJoining'],
									'email' 		=> $input['email'],
									'apiKey' 		=> $input['apiKey'],
									'updated_at' 	=> $input['updated_at']
								]);
				}
				else
				{
					$userUpdate = DB::table('user')
								->where('id', $input['id'])
								->update([
									'firstName'		=> $input['firstName'],
									'lastName' 		=> $input['lastName'],
									'joinDate' 		=> $input['dateOfJoining'],
									'email' 		=> $input['email'],
									'updated_at' 	=> $input['updated_at']
								]);
				}


				$personalDetailsUpdate 	= DB::table('personalDetails')
										->where('userid', $input['id'])
										->update([
											'PAN' 			=> $input['pan'],
											'DateOfBirth' 	=> $input['dateOfBirth'],
											'altEmail' 		=> $input['altEmail'],
											'phone' 		=> $input['phone'],
											'updated_at' 	=> $input['updated_at']
										]);

				if ($userUpdate || $personalDetailsUpdate) 
				{
					$result->status 	= "200";
					$result->message 	= "Data was updated successfully.";
				}
				else
				{
					$result->status 	= "304";
					$result->message 	= "No data was updated.";
				}

				return Response::json($result);

			}
		}
		elseif ($area == 'address') 
		{
			$validator = Validator::make(
				$input = array(
					'id' 		=> Session::get('employeeId'),
					'localAddress' 	=> ucwords(strtolower(Input::get('localAddress'))),
					'permanentAddress' => ucwords(strtolower(Input::get('permanentAddress'))),
					), 
				array(
					'localAddress' 	=> 'required',
					'permanentAddress' => 'required',
					), 
				$messages = array(
					'required' => 'This field is required.',
					)
				);	

			if ($validator->fails()) 
			{
				return Response::json($validator->messages());
			}
			else 
			{
				$result	= new StdClass;

				$update = DB::table('personalDetails')
						->where('userid', $input['id'])
						->update([
							'localAddress' 		=> $input['localAddress'],
							'permanentAddress' 	=> $input['permanentAddress'],
						]);

				if ($update) 
				{
					$result->status 	= "200";
					$result->message 	= "Data was updated successfully.";
				}
				return Response::json($result);
			}
		}
		elseif ($area == 'family') 
		{
			$validator = Validator::make(
				$input = array(
					'id' 		=> Session::get('employeeId'),
					'family' 	=> ucwords(strtolower(Input::get(''))),
					), 
				array(
					'family'	=> 'required|string',
					), 
				$messages = array(
					'required' 	=> 'This field is required.',
					'string' 	=> 'This field must be a string.',
					)
				);	

			if ($validator->fails()) 
			{
				return Response::json($validator->messages());
			}
			else 
			{
				$update	= DB::table('familyDetails')
						->where('userid', $input['id'])
						->update([
							'family'	=> $input['family'],
						]);

				if ($update) 
				{
					$result->status 	= "200";
					$result->message 	= "Data was updated successfully.";
				}
			}
		}
		elseif ($area == 'tax') 
		{
			$validator = Validator::make(
				$input = array(
					'id' 				=> Session::get('employeeId'),
					'jobTitle'			=> ucwords(strtolower(Input::get('jobTitle'))),
					'ctc'			 	=> Input::get('ctc'),
					'basic'			 	=> Input::get('basic'),
					'hra'			 	=> Input::get('hra'),
					'conveyance'		=> Input::get('conveyance'),
					'medicalAllowance'	=> Input::get('medicalAllowance'),
					'medicalInsurance'	=> Input::get('medicalInsurance'),
					'telephone'			=> Input::get('telephone'),
					'leaveTravel'		=> Input::get('leaveTravel'),
					'uniform'			=> Input::get('uniform'),
					'gratuity'			=> Input::get('gratuity'),
					'superAnnuation'	=> Input::get('superAnnuation'),
					'annualBonus'		=> Input::get('annualBonus'),
					'festivalBonus'		=> Input::get('festivalBonus'),
					'incentives'		=> Input::get('incentives'),
					'others'			=> Input::get('others'),
					'leaveEncashment'	=> Input::get('leaveEncashment'),
					'pfContribution'	=> Input::get('pfContribution'),
					'esiContribution'	=> Input::get('esiContribution'),
					'updated_at' 		=> date("Y-m-d h:i:sa"),
					// 'created_at'			 => date("d-m-y h:i:sa"),
				),
				array(
					'ctc' 				=> 'required|numeric',
					'basic' 			=> 'required|numeric',
					'hra' 				=> 'numeric',
					'conveyance' 		=> 'numeric',
					'medicalAllowance' 	=> 'numeric',
					'medicalInsurance' 	=> 'numeric',
					'telephone' 		=> 'numeric',
					'leaveTravel' 		=> 'numeric',
					'uniform' 			=> 'numeric',
					'gratuity' 			=> 'numeric',
					'superAnnuation' 	=> 'numeric',
					'annualBonus' 		=> 'numeric',
					'festivalBonus' 	=> 'numeric',
					'incentives' 		=> 'numeric',
					'others' 			=> 'numeric',
					'leaveEncashment' 	=> 'numeric',
					'pfContribution' 	=> 'numeric',
					'esiContribution' 	=> 'numeric',
			    ),
				$messages = array(
				   'required' 	=> 'The :attribute field is required.',
				   'numeric' 	=> 'The :attribute field must be a number.'
				)
			);

			if ($validator->fails()) 
			{
				return Response::json($validator->messages());
			}
			else 
			{
				$updateUser	= DB::table('user')
							->where('id', $input['id'])
							->update([
								'ctc'			=> $input['ctc'],
								'title' 		=> $input['jobTitle'],
								'updated_at'	=> $input['updated_at'],
							]);

				$updateCTC	= DB::table('employeeCtc')
							->where('userId', $input['id'])
							->update([
								'jobTitle' 			=> $input['jobTitle'],
								'ctc'			 	=> $input['ctc'],
								'basic'			 	=> $input['basic'],
								'hra'			 	=> $input['hra'],
								'conveyance'		=> $input['conveyance'],
								'medicalAllowance'	=> $input['medicalAllowance'],
								'medicalInsurance'	=> $input['medicalInsurance'],
								'telephone'			=> $input['telephone'],
								'leaveTravel'		=> $input['leaveTravel'],
								'uniform'			=> $input['uniform'],
								'gratuity'			=> $input['gratuity'],
								'superAnnuation'	=> $input['superAnnuation'],
								'annualBonus'		=> $input['annualBonus'],
								'festivalBonus'		=> $input['festivalBonus'],
								'incentives'		=> $input['incentives'],
								'others'			=> $input['others'],
								'leaveEncashment'	=> $input['leaveEncashment'],
								'pfContribution'	=> $input['pfContribution'],
								'esiContribution'	=> $input['esiContribution'],
								'updated_at' 		=> $input['updated_at'],
							]);

				$result = new StdClass;

				if ($updateUser || $updateCTC) 
				{
					$result->status 	= "200";
					$result->message 	= "Data was updated successfully.";
				}

				return Response::json($result);
			}
		}
		elseif ($area == 'payment') 
		{
			if (Input::get('modeOfPayment1')=='cheque') 
			{
				$check 	= DB::table('BankDetails')
						->where('userId', session()->get('employeeId'))
						->first();

				if (!empty($check)) 
				{
					$delete = DB::table('BankDetails')
							->where('userId', session()->get('employeeId'))
							->delete();
				}

				$checkPaytm = DB::table('paytmId')
							->where('userId', Session::get('employeeId'))
							->first();

				if (!empty($checkPaytm)) 
				{
					$delete = DB::table('paytmId')
							->where('userId', session()->get('employeeId'))
							->delete();
				}

				$checkPrev 	= DB::table('BankDetails')
							->where('userId', session()->get('employeeId'))
							->where('modeOfPayment', 'cheque')
							->first();

				$entries['userId'] 			= session()->get('employeeId');
				$entries['modeOfPayment']	= 'cheque';

				if (empty($checkPrev)) 
				{
					$insert = DB::table('BankDetails')
							->insert($entries);
				}
				else
				{
					$entries['updated_at'] = date('Y-m-d h:i:sa');

					$update = DB::table('BankDetails')
							->where('userId', $entries['userId'])
							->update($entries);
				}

				return response()->json([
		   				'status' => 200, 
		   				'message'=>'Success'
		   				]);	
			}

			if (Input::get('modeOfPayment1')=='directDeposit')
			{
				$validator = Validator::make(
					$entries = array(
							'userid' 		=> Session::get('employeeId'),
							'modeOfPayment' => Input::get('modeOfPayment1'),
							'bankName' 		=> Input::get('bankName1'),
							'branch' 		=> strtoupper(Input::get('branch1')),
							'accountHolder' => strtoupper(Input::get('accountHolder1')),
							'accountNumber' => Input::get('accountNumber1'),
							'accountType' 	=> strtoupper(Input::get('accountType1')),
							'ifsc' 			=> strtoupper(Input::get('ifsc1')),
							'sharePercent' 	=> strtoupper(Input::get('sharePercent1')),
						    
						),
						array(
							'modeOfPayment' => 'required',
							'bankName' 		=> 'required',
							'branch' 		=> 'required',
							'accountHolder' => 'required',
							'accountNumber' => 'required|numeric|regex:/([0-9]){10,}\d/',
							'accountType' 	=> 'required',
							'ifsc' 			=> 'required|alpha_num|size:11|regex:/([A-Za-z]){4}[0][0-9A-Za-z]{6}/',
							'sharePercent' 	=> 'required',
					    	),
					$messages = array(
							   'required' 	=> 'The :attribute field is required.',
							   'numeric' 	=> 'The :attribute field must be a number'
							)

					);
			}
			
			$form2 = Input::get('bankName2');
			if (isset($form2))
			{
				$validator1 = Validator::make(
					$entries1 = array(
							'userid' 		=> Session::get('employeeId'),
							'modeOfPayment' => "Direct Deposit",
							'bankName' 		=> Input::get('bankName2'),
							'branch' 		=> strtoupper(Input::get('branch2')),
							'accountHolder' => strtoupper(Input::get('accountHolder2')),
							'accountNumber' => Input::get('accountNumber2'),
							'accountType'	=> strtoupper(Input::get('accountType2')),
							'ifsc'			=> strtoupper(Input::get('ifsc2')),
							'sharePercent' 	=> strtoupper(Input::get('sharePercent2')),
						    
						),
						array(
							'userid' 		=> 'required',
							'bankName' 		=> 'required',
							'branch' 		=> 'required',
							'accountHolder' => 'required',
							'accountNumber' => 'required|numeric|regex:/([0-9]){10,}\d/',
							'accountType' 	=> 'required',
							'ifsc' 			=> 'required|alpha_num|size:11|regex:/([A-Za-z]){4}[0][0-9A-Za-z]{6}/',
							'sharePercent' 	=> 'required',
					    	),
					$messages = array(
							   'required' 	=> 'The :attribute field is required.',
							   'numeric' 	=> 'The :attribute field must be a number'
							)

					);
			}

			$form3 = Input::get('paytmId');
			if (isset($form3))
			{
				$validator2 = Validator::make(
					$entries2 = array(
							'userid' 			=> Session::get('employeeId'),
							'companyId' 		=> Session::get('companyId'),
							'paytmId' 			=> Input::get('paytmId'),
							'paytmSharePercent' => strtoupper(Input::get('paytmSharePercent')),
						    
						),
						array(
							'userid' 			=> 'required',
							'paytmId' 			=> 'required',
							'paytmSharePercent' => 'required',
					    	),
					$messages = array(
							   'required' 	=> 'The :attribute field is required.',
							   'numeric' 	=> 'The :attribute field must be a number'
							)
					);
			}

			if ( (isset($validator)and($validator->fails()))||(isset($validator1)and($validator1->fails()))||(isset($validator2)and($validator2->fails())))
	        {
	        	$error = array();
	        	
	        	if (isset($validator))
	        	{
	        		if ($validator->fails()) 
	        		{
		        		$error[0]	=	$validator->messages();
	        		}
	        		else
	        		{
	        			$error[0]	=	 new StdClass;
	        		} 
	        	}
	        	if (isset($validator1)) 
	        	{
	        		if ($validator1->fails()) 
	        		{
		        		$error[1]	=	$validator1->messages();
	        		}
	        		else
	        		{
	        			$error[1]	=	 new StdClass;
	        		} 
	        	}
	        	if (isset($validator2)) 
	        	{
	        		if ($validator2->fails()) 
	        		{
		        		$error[2]	=	$validator2->messages();
	        		}
	        		else
	        		{
	        			$error[2]	=	 new StdClass;
	        		} 
	        	}

			    return $error;
			}
			else
			{
				$check 	= DB::table('BankDetails')
						->where('userId', Session::get('employeeId'))
						->first();

				if (!empty($check)) 
				{
					$delete = DB::table('BankDetails')
							->where('userId', Session::get('employeeId'))
							->delete();				
				}

				$checkPaytm = DB::table('paytmId')
							->where('userId', Session::get('employeeId'))
							->first();

				if (!empty($checkPaytm)) 
				{
					$deletePaytm 	= DB::table('paytmId')
									->where('userId', Session::get('employeeId'))
									->delete();
				}

				if (isset($entries))
				{
					$select=DB::table('BankDetails')
							->where('userid', Session::get('employeeId'))
							->where('modeOfPayment', "directDeposit")
							->first();

					if (empty($select))
					{
						$result = DB::table('BankDetails')
								->insert($entries);										
					}
					else
					{	
						$entries['updated_at'] = date('Y-m-d h:i:sa');
						$result = DB::table('BankDetails')
								->where('userid', Session::get('employeeId'))
								->where('modeOfPayment', "directDeposit")
								->update($entries);
					}
				}
				if (isset($entries1))
				{
					$select1=DB::table('BankDetails')
							->where('userid', Session::get('employeeId'))
							->where('modeOfPayment', "Direct Deposit")
							->first();
					
					if (empty($select1))
					{
						$result1 = DB::table('BankDetails')
							->insert($entries1);										
					}
					else
					{	
						$entries1['updated_at'] = date('Y-m-d h:i:sa');
						$result1 = DB::table('BankDetails')
								->where('userid', Session::get('employeeId'))
								->where('modeOfPayment', "Direct Deposit")
								->update($entries1);
					}
				}
				else 
					$result1=1;

				if (isset($entries2))
				{
					$select2=DB::table('paytmId')
							->where('userId', Session::get('employeeId'))
							->first();

					if (empty($select2))
					{
						$result2 = DB::table('paytmId')
								->insert($entries2);										
					}
					else
						$result2 = DB::table('paytmId')
								->where('userid', Session::get('employeeId'))
								->where('paytmId', $entries2['paytmId'])
								->update($entries2);
				}
				else 
					$result2=1;

				if ($result || $result1 || $result2)
				{
					$select = DB::table('user')
							->where('id', session()->get('employeeId'))
							->update(['registeration'=> 'bank']);
				}
				// if ($result && $result1 && $result2) 
				// {
				// 	$url="/manageEmployees";
				// }
				// else
				// {
				// 	$url="/addEmployees_step4";
				// }

		   		return response()->json([
		   				'result' 	=> $result, 
		   				'result1' 	=> $result1, 
		   				'result2' 	=> $result2, 
		   				'status' 	=> 200, 
		   				// 'url' 	=> $url, 
		   				'message'	=> 'Success'
		   				]);	
		   	}
		}
		elseif ($area == 'medical') 
		{
			$validator = Validator::make(
				$input = array(
					'id' 		=> Session::get('employeeId'),
					'medical' 	=> ucwords(strtolower(Input::get(''))),
					), 
				array(
					'medical' 	=> 'required',
					), 
				$messages = array(
					'required' 	=> 'This field is required.',
					)
				);	

			if ($validator->fails()) 
			{
				return Response::json($validator->messages());
			}
			else 
			{
				$update	= DB::table('medical')
						->where('userid', $input['id'])
						->update([
							'medical'	=> $input['medical'],
						]);

				if ($update) 
				{
					$result->status 	= "200";
					$result->message 	= "Data was updated successfully.";
				}
			}
		}
		elseif ($area == 'vacation') 
		{
			$validator = Validator::make(
				$input = array(
					'id' 		=> Session::get('employeeId'),
					'vacation' 	=> ucwords(strtolower(Input::get(''))),
					), 
				array(
					'vacation' 	=> 'required',
					), 
				$messages = array(
					'required' 	=> 'This field is required.',
					)
				);	

			if ($validator->fails()) 
			{
				return Response::json($validator->messages());
			}
			else 
			{
				$update	= DB::table('vacation')
						->where('userid', $input['id'])
						->update([
							'vacation'	=> $input['vacation'],
						]);

				if ($update) 
				{
					$result->status 	= "200";
					$result->message 	= "Data was updated successfully.";
				}
			}
		}
		elseif ($area == 'reimbursement') 
		{
			$validator = Validator::make(
				$input = array(
					'id' 		=> Session::get('employeeId'),
					'reimbursement' 	=> ucwords(strtolower(Input::get(''))),
					), 
				array(
					'reimbursement' 	=> 'required',
					), 
				$messages = array(
					'required' 	=> 'This field is required.',
					)
				);	

			if ($validator->fails()) 
			{
				return Response::json($validator->messages());
			}
			else 
			{
				$update	= DB::table('reimbursement')
						->where('userid', $input['id'])
						->update([
							'reimbursement'	=> $input['reimbursement'],
						]);

				if ($update) 
				{
					$result->status 	= "200";
					$result->message 	= "Data was updated successfully.";
				}
			}
		}
	}

	public function terminateEmployee()
	{
		if (empty($id)) 
		{
			return response()->json(['message'=> 'Employee Id is required!']);	
		}

		$select = DB::table('user')
				->where('id', Input::get('employeeId'))
				->where('companyId', session()->get('companyId'))
				->first();

		if (empty($select))
		{
			return response()->json(['message'=> 'Employee not found!']);
		}
		else
		{
			if ((strtolower($select->type) == 'employee') or (strtolower($select->type) == 'contractor')) 
			{
				$select = DB::table('user')
						->where('id', Input::get('employeeId'))
						->where('type', 'Employee')
						->orWhere('type', 'Contractor')
						->update([
							'terminateDate'=> Input::get('lastDate')
							]);
			}
			else
			{
				return response()->json([
					'message'=> 'This employee does not belong to your company.', 
					'status'=> 401
					]);
			}
		}

		return response()->json([
			'message'=> 'Employee terminated successfully.', 
			'status'=> 200
			]);
	}

	public function insertFamilyData()
	{
		$id = Input::get('id');

		$validator = Validator::make(
			$entries = array(
					'userid' 			=> Session::get('employeeId'),
					'name' 				=> ucwords(strtolower(Input::get('name'))),
					'relation'			=> ucwords(strtolower(Input::get('relation'))),
					'dob'				=> Input::get('dob'),
					'address'	 		=> ucwords(Input::get('address')),
					'disabled' 			=> strtolower(Input::get('disabled')),
				),
				array(
					'name' 		=> 'required',
					'relation' 	=> 'required',
					'dob' 		=> 'required|date',
					'address' 	=> 'required',
			    ),
			$messages = array(
				   'required' 	=> 'This field is required.',
				   'date' 		=> 'This is not a valid date.',
				)
			);

		if ($validator->fails())
        {
        	return response()->json($validator->messages());
		}
		else
		{
			if (!empty($id)) 
			{
				$entries['updated_at'] 	= date('Y-m-d h:i:sa');

				$update = DB::table('familyDetails')
						->where('id', $id)
						->where('userid', session()->get('employeeId'))
						->update($entries);

				$select = DB::table('familyDetails')
						->where('id', $id)
						->first();
			}
			else
			{
				$insert = DB::table('familyDetails')
						->insert($entries);

				$select = DB::table('familyDetails')
						->orderBy('id', 'DESC')
						->first();
			}
		

			$progress['userid'] 	= session()->get('employeeId');
			$progress['nextStep']	= 'step2';
			$progress['complete']	= 'off';
			$progress['updated_at']	= date('Y-m-d h:i:sa');

			$updateProgress = DB::table('userProgress')
							->where('userid', $progress['userid'])
							->update($progress);

			session()->put('employeeProgressNextStep', 'step2');
			session()->put('employeeProgressComplete', 'off');

	   		return response()->json([
	   				'status' 	=> 200,
	   				'message'	=> 'Data inserted successfully.', 
	   				'id'		=> $select->id,
	   			]);
		}
	}

	public function editFamilyData($id)
	{
		$select = DB::table('familyDetails')
				->where('id', $id)
				->where('userid', session()->get('employeeId'))
				->first();

		return response()->json($select);
	}

	public function deleteFamilyData($id)
	{
		$delete = DB::table('familyDetails')
				->where('id', $id)
				->where('userid', session()->get('employeeId'))
				->delete();

		$result = new StdClass;
		
		if (!empty($delete)) 
		{
			$result->status 	= 200;
		}
		else
		{
			$result->status 	= 405;
			$result->message 	= "You're not allowed to delete this data.";
		}

		return response()->json($result);
	}
}
