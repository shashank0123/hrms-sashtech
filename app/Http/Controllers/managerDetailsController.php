<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB, Validator, Input, Session, File, StdClass, Response;
use Illuminate\Http\Request;

class managerDetailsController extends Controller 
{
	public function dashboard()
	{
		if ((strtolower(session()->get('type')) != "manager") and (strtolower(session()->get('type')) != 'ca')) 
		{
			return redirect('login');
		}

		$profile = DB::table('personalDetails2')
				->where('userid', session()->get('caId'))
				->first();

		$select = DB::table('quotes')
				->where('status', 'active')
				->first();

		$d = date_diff(date_create(date('d-m-Y')), date_create(date('d-m-Y', strtotime($select->updated_at))));

		if ($d->days > 0) 
		{
			$id = intval($select->id);

			$update = DB::table('quotes')
			->where('id', $id)
			->update(['status'=>'not active']);

			$rows = count(DB::table('quotes')->get());

			// var_dump($rows);
			// var_dump($id);

			if (($id+1) > $rows) 
			{
				$id =1;
			}
			else
			{
				$id += 1;
			}

			// var_dump($id);

			$update2=DB::table('quotes')
			->where('id', $id)
			->update([
				'status' => 'active',
				'updated_at' => date('Y-m-d h:i:sa'),
				]);

			$select= DB::table('quotes')
			->where('status', 'active')
			->first();
		}

		$select2 	= DB::table('user')
					->where('id', session()->get('caId'))
					->select('created_by')
					->first();

		if (!empty($select2->created_by)) 
		{
			$select3 = DB::table('user')
					->where('id', $select2->created_by)
					->first();

			$select4 = DB::table('personalDetails2')
					->where('userid', $select2->created_by)
					->first();

			if (empty($select4)) 
			{
				$select4 	= NULL;
			}
		}
		else
		{
			$select2 	= NULL;
			$select3 	= NULL;
			$select4 	= NULL;
		}

		return view('manager/managerDashboard')
				->with('profile', $profile)
				->with('result', $select)
				->with('result2', $select2)
				->with('result3', $select3)
				->with('result4', $select4);
	}

	public function addDetails()
	{
		if ((strtolower(session()->get('type')) != 'manager') and (strtolower(session()->get('type')) != 'ca')) 
		{
			return redirect('login');
		}

		$validator = Validator::make(
				$entries = array(
					'userid'		=> session()->get('caId'),
					'type'			=> strtolower(session()->get('type')),
					'firstName' 	=> ucwords(strtolower(Input::get('firstName'))),
					'lastName' 		=> ucwords(strtolower(Input::get('lastName'))),
					'phone' 		=> Input::get('phone'),
					'email' 		=> strtolower(Input::get('email')),
					'dp'			=> Input::file('profilePic'),
					), 
				array(
					'firstName' 	=> 'required',
					'phone' 		=> 'required|numeric',
					'email' 		=> 'required|email',
					'dp'			=> 'max:500|mimes:jpg,jpeg,png',
					), 
				$messages = array(
					'required' 	=> 'This field is required.',
					'numeric' 	=> 'This must be a number.',
					'email' 	=> 'This is not a valid email.',
					'max'		=> 'The file should be bigger than 500KB.',
					'mimes'		=> 'The file is not in an acceptable format (ie. jpg, jpeg, png).'
					)
			);

		if ($validator->fails()) 
		{
			return Response::json($validator->messages());
		}
		else
		{
			$result = new StdClass;

			if ($entries['email'] != session()->get('email'))
			{
				$result->status 	= 405;
				$result->message 	= 'You are not allowed to change your email. If you still insist, please contact our admin panel.';

				return Response::json($result);
			}

			$entries = array_filter($entries);
			$uploadPath = public_path() . '/HTML/Uploads/Manager/' . $entries['userid'] . '/';

			//check if the directory exists
			if(!File::exists($uploadPath)) 
			{
				File::makeDirectory($uploadPath , 0775, true);
			}

			// Move file to upload directory
			if (isset($entries['dp'])) 
			{
				$dp = $uploadPath . 'dp-' . $entries['dp']->getClientOriginalName();
				if(File::exists($dp)) 
				{
					unlink($dp);
				}
				$file = $entries['dp']->move($uploadPath, $dp);
				
				$dp = explode('public/', $dp)[1];
				$entries['dp'] = $dp;
			}

			$update = DB::table('user')
					->where('email', $entries['email'])
					->update([
						'firstName' 	=> $entries['firstName'],
						'lastName'	 	=> $entries['lastName'],
						'updated_at'	=> date(('Y-m-d h:i:sa')),
						]);

			$entries1 	= $entries;
			$entries1['firstName']	= NULL;
			$entries1['lastName']	= NULL;
			$entries1['email']		= NULL;

			$entries1 	= array_filter($entries1);

			if (empty($entries1['dp']) or !isset($entries1['dp'])) 
			{
				$entries1['dp'] = NULL;
			}

			$check 	= DB::table('personalDetails2')
					->where('userid', $entries1['userid'])
					->first();

			if (empty($check)) 
			{
			 	$insertDetails 	= DB::table('personalDetails2')
								->insert($entries1);
			} 
			else
			{
				if (!empty($check->dp))
				{
					$prev = public_path() . '/' . $check->dp;

					if(File::exists($prev)) 
					{
						unlink($prev);
					}
				}

				$entries1['updated_at'] = date('Y-m-d h:i:sa');

				$insertDetails 	= DB::table('personalDetails2')
								->where('userid', $entries1['userid'])
								->update($entries1);
			}

			$result->status 	= 200;
			$result->message 	= 'Data updated successfully.';

			return Response::json($result);
		}
	}

	public function getDetails()
	{
		if ((strtolower(session()->get('type')) != 'manager') and (strtolower(session()->get('type')) != 'ca')) 
		{
			return redirect('login');
		}

		$id 	= session()->get('caId'); 

		$select1 	= DB::table('user')
					->where('id', $id)
					->first();

		$select2	= DB::table('personalDetails2')
					->where('userid', $id)
					->first();

		return view('manager/managerDetail')
				->with('result1', $select1)
				->with('result2', $select2);
	}

	public function viewSelectCompany() 
	{
		if ((strtolower(session()->get('type')) != 'manager') and (strtolower(session()->get('type')) != 'ca')) 
		{
			return redirect('login');
		}

		$company 	= DB::table('company')
					->where('managerId', session()->get('caId'))
					->orderBy('created_at', 'desc')
					->get();
			
		return view('manager/managerSelectCompany')
				->with('company', $company);
	}

	public function changeCompany($id)
	{
		if ((strtolower(session()->get('type')) != 'manager') and (strtolower(session()->get('type')) != 'ca')) 
		{
			return redirect('login');
		}
		
		$select = DB::table('company')
				->where('id', $id)
				->first();

		session()->put('companyId', $select->id);
		session()->put('companyName', $select->companyName);
		session()->put('companyCode', $select->companyCode);
		session()->put('plan', $select->plan);
		session()->put('type', 'employer');
		session()->put('employerId', session()->get('caId'));
		session()->put('employerName', session()->get('managerName'));

		return redirect('getStarted');
	}
}
