<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB, View, Input;

use Illuminate\Http\Request;

class payContractorController extends Controller {

	public function getContractor()
	{
		if (date('m') < 4) 
		{
			$year=(intval(date('Y'))-1)."-".(intval(date('y')));
		}
		else
		{
			// $year=(intval(date('Y'))-1)."-".(intval(date('y')));
			// shouldn't the below one be correct instead of the above one? 
			// what're you trying to do by keeping both the year values same in the if and else conditions?
			$year=(intval(date('Y')))."-".(intval(date('y'))+1);
		}

		$months = DB::table('month')
				->where('id', intval(date('m'))-1)
				->select('month')
				->first();

		$month 	= $months->month;

		if ((session()->get('complete')=="on"))
		{
			$payday = DB::table('leaveSetup')
					->where('companyId', session()->get('companyId'))
					->first();
			
			$users 	= DB::table('user')
		            ->join('BankDetails', 'BankDetails.userid', '=', 'user.id')
		            ->join('contractor', 'contractor.userid', '=', 'user.id')
		            ->where('user.companyId', '=' , session()->get('companyId'))
		            ->where('user.type', '=' , "Contractor")
		            ->where('user.status', '=' , "Active")
		            ->select('user.*', 'BankDetails.modeOfPayment', 'contractor.paid')
		            ->get();

		    if (!empty($users)) 
		    {
			    $users[0]->payday 		= $payday->payDay;
			    $users[0]->runPayroll 	= $payday->runPayroll;
			    
			    foreach ($users as $key => $value) 
			    {
			    	$users[$key]->noOfLeave=0;
					$users[$key]->note="";
					$users[$key]->bonus=0;
					$users[$key]->overtime=0;
					$users[$key]->reimbursement=0;
					$users[$key]->commission=0;
					$users[$key]->otherEarning=0;
					$users[$key]->grossPay=0;
					$users[$key]->noOfDays=0;
					$users[$key]->deduction=0;
			    }

			    return View::make('payContractor2')->with('contractors', $users);
		    }
		    else
		    {
		    	// $noUsers = true;
		    	return View::make('payContractor1');
		    }

			
			// else
			// {
			// 	$payContractor = DB::table('payContractor')
			//             ->join('payrollMonth', 'payContractor.month', '=', 'payrollMonth.nextMonth')
			//             ->where('payContractor.companyId', '=' , session()->get('companyId'))
			//             ->get();

			//     if (empty($payContractor)) {
			//     	return View::make('payContractor')->with('contractors', $users);
			//     }
			//     else
			//     {
			// 		$users2 = DB::table('user')
			// 	            ->join('payContractor', 'user.id', '=', 'payContractor.userId')
			// 	            ->where('user.companyId', '=' , session()->get('companyId'))
			// 	            ->where('user.type', '=' , "Contractor")
			// 	            ->get();

			// 	    return View::make('payContractor')->with('contractors', $users2);
			//     }
			// }
		}
		else 
		{
			return View::make('payContractor1');
		}
	}

	public function saveContractor()
	{
		if (date('m')<4) {
			$year=(intval(date('Y'))-1)."-".(intval(date('y')));
		}
		else
			$year=(intval(date('Y'))-1)."-".(intval(date('y')));
		$months = DB::table('month')->where('id', intval(date('m'))-1)->select('month')->first();
		$month= $months->month;
		$id=Input::get('userId');
		$users = DB::table('user')
		            ->join('BankDetails', 'BankDetails.userid', '=', 'user.id')
		            ->join('contractor', 'contractor.userid', '=', 'user.id')
		            ->where('user.id', '=' , $id)
		            ->select('user.*', 'BankDetails.modeOfPayment', 'contractor.paid')
		            ->first();
		$netPay=0;
		if ($users->earning=="Hour"){
			$netPay=Input::get('noOfHour')*$users->ctc;
		}
		// other earning types

		$grossPay=$netPay+Input::get('overtime')+Input::get('bonus')+Input::get('commission')+Input::get('otherEarning')+Input::get('reimbursement')-Input::get('deduction');

		$validator = Validator::make(
				$input = array(
					'userID' 	=> Input::get('userId'),
					'companyId' 	=> Session::get('companyId'),
					'month' 		=> $month,
					'financialYear'	=> $year,
					'rateMultiplier' 	=> Input::get('noOfLeave'),
					'overtime' 		=> Input::get('overtime'),
					'bonus' 		=> Input::get('bonus'),
					'commission' 	=> Input::get('commission'),
					'otherEarning' 	=> Input::get('otherEarning'),
					'grossPay' 		=> $result->grossPay,
					'reimbursement' => Input::get('reimbursement'),
					'deduction' 	=> Input::get('deduction'),
					'note' 			=> Input::get('note'),
					), 
				array(
					'userID' 		=> 'required',
					'companyId'	=> 'required',
					'month' 		=> 'required',
					'financialYear' 			=> 'required',
					'noOfLeave' 		=> 'required|numeric',
					'grossPay' 			=> 'required|numeric',
					), 
				$messages = array(
					'required' 	=> "This field is required.",
					'numeric' 	=> "This must be a number.",
					)
				);

			if ($validator->fails()) 
			{
				return Response::json($validator->messages());
			}
			else
			{

		$check=DB::table('payContractor')->where('userId', $id)->where('month', $month)->where('financialYear', $year)->first();
		if ($check){
			$select=DB::table('payContractor')->where('userId', $id)->where('month', $month)->where('financialYear', $year)->update();			
		}
		else
			$select=DB::table('payContractor')->where('userId', $id)->where('month', $month)->where('financialYear', $year)->insert();			
	}
		var_dump($users);
		var_dump($grossPay);
	}
}
