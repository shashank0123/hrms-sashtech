<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response, Input, Session, DB;
use Illuminate\Http\Request;

class changeViewController extends Controller 
{
	public function switchView()
	{
		if (session()->get('type') == 'Employer') 
		{
			session()->put('type', 'Employee');
			session()->put('employeeId', session()->get('employerId'));
			session()->put('employeeName', session()->get('employerName'));
			session()->put('last_acted_on', time());

			$select3 = DB::table('userProgress')
					->where('userid', session()->get('employerId'))
					->first();

			if (empty($select3)) 
			{
				$insertUserProgress = DB::table('userProgress')
									->insert([
										'userid' 	=> session()->get('employerId'),
										'nextStep' 	=> 'step1',
										'complete'  => 'off'
										]);

				$select3 	= DB::table('userProgress')
							->where('userid', session()->get('employerId'))
							->first();
			}


			if (!empty($select3)) 
			{
				session()->put('employeeProgressNextStep', $select3->nextStep);
				session()->put('employeeProgressComplete', $select3->complete);
			}
			else
			{
				session()->put('employeeProgressNextStep', 'step1');
				session()->put('employeeProgressComplete', 'off');
			}

			session()->put('employerId', null);
			session()->put('employerName', null);
			session()->put('viewChanged', true);

			return redirect('employeeDashboard');
		}
		elseif ((session()->get('type') == 'Employee')and(session()->get('viewChanged'))) 
		{
			session()->put('type', 'Employer');
			session()->put('employerId', session()->get('employeeId'));
			session()->put('employerName', session()->get('employeeName'));
			session()->put('employeeId', null);
			session()->put('employeeName', null);
			session()->put('viewChanged', false);

			return redirect('getStarted');
		}
	}

	public function adminView()
	{
		if (strtolower(session()->get('type')) == 'employer') 
		{
			session()->put('type', 'Admin');
			session()->put('adminId', session()->get('employerId'));
			session()->put('adminName', session()->get('employerName'));
			session()->put('last_acted_on', time());

			session()->put('employerId', null);
			session()->put('employerName', null);
			session()->put('viewChanged', true);

			return redirect('adminSelectCompany');
		}
		else
		{
			return redirect('login');
		}
	}

	public function accountantView()
	{
		if (strtolower(session()->get('type')) == 'employer') 
		{
			session()->put('type', 'Accountant');
			session()->put('accountantId', session()->get('employerId'));
			session()->put('accountantName', session()->get('employerName'));
			session()->put('last_acted_on', time());

			session()->put('employerId', null);
			session()->put('employerName', null);
			session()->put('viewChanged', true);

			return redirect('accountantSelectCompany');
		}
		else
		{
			return redirect('login');
		}
	}
}
