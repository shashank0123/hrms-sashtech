<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB, File;
use Illuminate\Http\Request;

class employeeAccessController extends Controller 
{
	public function dashboard()
	{
		if (strtolower(session()->get('type')) == 'employee') 
		{
			if (session()->get('employeeProgressComplete') == 'on') 
			{
				return redirect('employeeDashboard1');
			}
			else
			{
				return view('employee/employeeDashboard');	
			}
		}
		else 
		{
			return redirect('login');
		}
	}
	
	public function dashboard1()
	{
		if (strtolower(session()->get('type')) == 'employee') 
		{
			if (session()->get('employeeProgressComplete') == 'on') 
			{
				// echo "<pre>";
				$select = DB::table('leaveSetup')
						->where('companyId', session()->get('companyId'))
						->first();

				$select2 = DB::table('reimbursement')
						->where('userId', session()->get('employeeId'))
						->orderBy('created_at', 'DESC')
						->first();

				$select3 = DB::table('personalDetails')
						->where('userid',  'like', substr(session()->get('employeeId'), 0,4) . '%')
						->orderBy('DateOfBirth', 'ASC')
						->select('DateOfBirth', 'userid')
						->get();

				$select4 = DB::table('user')
						->where('companyId',  session()->get('companyId'))
						->get();

				$select5 = DB::table('quotes')
						->where('status', 'active')
						->first();


				$d = date_diff(date_create(date('d-m-Y')), date_create(date('d-m-Y', strtotime($select5->updated_at))));

				if ($d->days > 0) 
				{
					$id = intval($select5->id);

					$update = DB::table('quotes')
							->where('id', $id)
							->update(['status'=>'not active']);

					$rows = count(DB::table('quotes')->get());

					//var_dump($rows);
					//var_dump($id);

					if (($id+1) > $rows) 
					{
						$id =1;
					}
					else
					{
						$id += 1;
					}

					//var_dump($id);

					$update2=DB::table('quotes')
							->where('id', $id)
							->update([
								'status' => 'active',
								'updated_at' => date('Y-m-d h:i:sa'),
								]);

					$select5= DB::table('quotes')
							->where('status', 'active')
							->first();
				}

				$select6 	= DB::table('companyPolicy')
							->where('companyId', session()->get('companyId'))
							->orderBy('id', 'DESC')
							->get();

				$select7 	= DB::table('notification')
							->where('companyId', session()->get('companyId'))
							->orderBy('id', 'DESC')
							->get();

				// coverage part
				$select8	= DB::table('benefit')
							->where('companyId', session()->get('companyId'))
							->orderBy('id', 'DESC')
							->first();

				// donut part
				$month 		= date('m');
				$month 		= intval($month) - 1;
				$month 		= DB::table('month')
							->where('id', $month)
							->first();

				$month 		= $month->month;

				$select9 	= DB::table('payroll')
							->where('userID', session()->get('employeeId'))
							->where('month', $month)
							->orderBy('id', 'DESC')
							->first();

				$select10 	= DB::table('calendar')
							->where('userID', session()->get('companyId'))
							->first();

				if (!empty($select10)) 
				{
					$select10->jsonObject = public_path() . $select10->jsonObject;

					if (File::exists($select10->jsonObject)) 
					{
						$file = File::get($select10->jsonObject);
					}
					
					if (!isset($file)||empty($file))
					{
						$select10 = new StdClass;
						$select10 ='[]';
					}
					else 
					{
						$select10 = $file;
					}
				}

				return view('employee/employeeDashboard1', [
					'result' => $select, 
					'result2' => $select2,
					'result3' => $select3,
					'result4' => $select4,
					'result5' => $select5,
					'result6' => $select6,
					'result7' => $select7,
					'result8' => $select8,
					'result9' => $select9,
					'result10' => $select10,
				]);
			}
			else
			{
				return redirect('employeeDashboard');
			}
		}
		else 
		{
			return redirect('login');
		}
	}

	public function tdsReport()
	{
		if (strtolower(session()->get('type')) == 'employee') 
		{
			return view('tdsReport');		
		}
		else 
		{
			return redirect('login');
		}
	}

	public function otherReport()
	{
		if (strtolower(session()->get('type')) == 'employee') 
		{
			return view('otherReport');		
		}
		else 
		{
			return redirect('login');
		}	
	}
	
	public function employeePayslip()
	{
		if (strtolower(session()->get('type')) == 'employee')
		{
			$year = date('Y');
			$year = $year . '-' . substr(($year+1), 2);

			$result1 = DB::table('payroll')
					->where('userID', session()->get('employeeId'))
					->where('financialYear', $year)
					->select('month', 'financialYear', 'created_at')
					->get();

			return view('employee/employeePayslip')
					->with('result', $result1);
		}
		else 
		{
			return redirect('login');
		}	
	}

	public function employeeDeduction()
	{
		if (strtolower(session()->get('type')) == 'employee')
		{
			// $result = DB::table('')

			$result 	= DB::table('investmentDeduction')
						->where('userid', session()->get('employeeId'))
						->first();
		
			$result1	= DB::table('medicalDeduction')
						->where('userid', session()->get('employeeId'))
						->first();	
			
			$result2	= DB::table('otherDeduction')
						->where('userid', session()->get('employeeId'))
						->first();	
			
			$result3	= DB::table('HRAmonthlyDeduction')
						->where('userid', session()->get('employeeId'))
						->first();	
			
			$result4	= DB::table('selfOccupiedDeduction')
						->where('userid', session()->get('employeeId'))
						->first();	
			
			$result5	= DB::table('previousEmployerDeduction')
						->where('userid', session()->get('employeeId'))
						->first();

			$result6	= DB::table('allowanceDeclaration')
						->where('userId', session()->get('employeeId'))
						->get();

			return view('employee/employeeDeduction')
					->with('result', json_decode(json_encode($result), true))
					->with('result1', json_decode(json_encode($result1), true))
					->with('result2', json_decode(json_encode($result2), true))
					->with('result3', json_decode(json_encode($result3), true))
					->with('result4', json_decode(json_encode($result4), true))
					->with('result5', json_decode(json_encode($result5), true))
					->with('result6', json_decode(json_encode($result6), true));
		}
		else 
		{
			return redirect('login');
		}	
	}

	public function employeeBenefits_step1()
	{
		if (strtolower(session()->get('type')) == 'employee') 
		{
			return view('employeeBenefits_step1');		
		}
		else 
		{
			return redirect('login');
		}	
	}
}
