<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator, Input, Redirect, DB, Hash, Session, View, Response, StdClass, Auth;
use Illuminate\Http\Request;

class employeeLoginController extends Controller {

	public function resetPasswordApiKey()
	{
		$validator = Validator::make(
			$auth = array(
				'newPass'  		=> Input::get('newPass'),
				'confirmPass'  	=> Input::get('confirmPass'),
				'apiKey'  		=> Input::get('apiKey')
				),
			array(
				'newPass'  		=> 'required|min:8',
				'confirmPass' 	=> 'required|min:8|same:newPass',
				'apiKey'  		=> 'required',
				),
			$messages = array(
				'min' 	=> 'This must be at least 8 characters.',
				'same' 	=> 'Confirm password does not match the above entered password.'
				)
			);

		if($validator->fails())
		{
			return View::make('resetPassword')->with('errors', json_encode($validator->messages()));
		}
		else
		{
			$entries = array(
					'password'  	=> Hash::make($auth['newPass']),
					'randomNumber'  => mt_rand(1000000,9999999),
					'status' 		=> 'Active',
					'updated_at'	=> date('Y-m-d h:i:sa'),
					);

			$checkUser = DB::table('user')
						->where('apiKey', $auth['apiKey'])
						->first();

			if (empty($checkUser))
			{
				$result 			= new StdClass;
				$result->status 	= 401;
				$result->message 	= "That reset link is not valid. Please request a valid one.";

				return View::make('resetPassword')
						->with('result', $result);
			}

			if (!empty($checkUser->password)) 
			{
				if (!empty($checkUser->randomNumber)) 
				{
					$now 	= strtotime(date("Y-m-d h:i:sa"));
					$init 	= strtotime($checkUser->updated_at);

					$mins 	= intval((($now - $init)/60));

					// If mins is less than 121 mins (less than 2 hours)
					if (($mins < 121) and ($mins >= 0))
					{
						$update = DB::table("user")
								->where("id", $checkUser->id)
								->update([
									'password' 		=> $entries['password'],
									'randomNumber' 	=> NULL,
									'updated_at'	=> date("Y-m-d h:i:sa"),
								]);

						session()->put("resetSuccess", 'Your password has been reset. You may login now.');
						return Redirect::to('login');
					}
					else
					{
						$result 			= new StdClass;
						$result->status 	= 406;
						$result->message 	= "That reset link is no longer valid. Please request one again. Remember that the reset links are only valid for 2 hours from the point of initiation.";

						$update = DB::table('user')
								->where('id', $checkUser->id)
								->update([
									'randomNumber' 	=> NULL,
									'updated_at' 	=> date('Y-m-d h:i:sa'),
								]);
					}
				}
				else
				{
					$result 			= new StdClass;
					$result->status 	= 401;
					$result->message 	= "That reset link is not valid anymore. Please request a new one.";
				}

				return View::make('resetPassword')
						->with('result', $result);
			}

			$entries['randomNumber'] 	= NULL;
			$entries 	= array_filter($entries);

			$select = DB::table("user")
					->where("apiKey", $auth['apiKey'])
					->update($entries);

			$user 	= DB::table("user")
					->where("apiKey", $auth['apiKey'])
					->first();

			if(!empty($user))
			{
				session()->put("resetSuccess", 'Your password has been reset. You may login now.');
				return Redirect::to('login');

				// if ($user->type=='Employee')
				// {
				// 	$select = DB::table('company')->where('id', $user->companyId)->first();
				// 	Session::put('companyName' , $select->companyName);
				// 	Session::put('companyId', $user->companyId);
				// 	Session::put('userSession', $user);
				// 	Session::put('type', $user->type);
				// 	Session::put('employeeId', $user->id);
				// 	Session::put('email', $user->email);
				// 	Session::put('employeeName', $user->firstName);
				// 	Session::put('last_acted_on', time());
					
				// 	return Redirect::to('login');
				// }	
				// elseif ($user->type=="Employer") 
				// {
				// 	$select = DB::table('company')->where('id', $user->companyId)->first();
				// 	Session::put('companyName' , $select->companyName);
				// 	Session::put('companyId', $user->companyId);
				// 	Session::put('userSession', $user);
				// 	Session::put('type', $user->type);
				// 	Session::put('employerId', $user->id);
				// 	Session::put('email', $user->email);
				// 	Session::put('employerName', $user->firstName);
				// 	Session::put('last_acted_on', time());
				// 	Session::put('plan', $select->plan);

				// 	return Redirect::to('login');
				//  } 
				// elseif ($user->type=="Admin") 
				// {
				// 	return Redirect::to('adminDashboard');
				// } 
				// elseif ($user->type=="SuperAdmin") 
				// {
				// 	return Redirect::to('getStarted');
				// }
				// else 
				// {
				// 	return Redirect::to('login')->withErrors("Company ID is not defined. Contact your employer ASAP.");
				// }
			}
			else 
			{
				return Redirect::to('login')->withErrors("Database error. Please contact administrator.");
			}
		}
	}

	public function resetPassword($email)
	{
		$user 	= DB::table("user")
				->where("apiKey", $email)
				->first();
		
		$update = DB::table("user")
				->where("apiKey", $email)
				->update([
					'password' 		=> Hash::make(Input::get('password')),
					'updated_at'	=> date('Y-m-d h:i:sa'),
					]);

		return Response::json($update);
	}

	public function changePassword()
	{
		// echo "<pre>";
		// var_dump(Session::all());
		$validator = Validator::make(
			$auth = array(
				'newPass'  		=> Input::get('newPass'),
				'confirmPass'  	=> Input::get('confirmPass'),
				'oldPass'  		=> Input::get('oldPass')
				),
			array(
				'newPass'  		=> 'required|min:8',
				'confirmPass' 	=> 'required|min:8|same:newPass',
				'oldPass'  		=> 'required',
				),
			$messages = array(
				'min' 	=> 'This must be at least 8 characters.',
				'same' 	=> 'Confirm password does not match the above entered password.'
				)
			);

		if($validator->fails())
		{
			return View::make('changePassword')->with('errors', json_encode($validator->messages()));
		}
		else
		{
			$user = DB::table("user")->where("email", session()->get('email'))->where('password', Hash::make(Input::get('oldPass')))->update(['password'=> Hash::make(Input::get('newPass'))]);
			return redirect('login')->with('errors', 'Login with new Password');
		}
		
		// $update = DB::table("user")->where("apiKey", 'email')->update(['password', Hash::make(Input::get('password'))]);

		// return Response::json($update);
	}

	public function login()
	{
		session()->flush();

		$validator = Validator::make(
			$auth = array(
				'email'  => Input::get('email'),
				'password'  => Input::get('password'),
				),
			array(
				'email'  => 'required|email',
				'password'  => 'required',
				)
			);

		if($validator->fails())
		{
			return View::make('login')->with('result', $validator->errors()->all()[0]);
		}
		else
		{
			$user 	= DB::table("user")
					->where("email", $auth['email']) // "=" is optional
					->first();

			if(isset($user))
			{
				if ($user->status=="Active")
				{
					if(Hash::check($auth['password'], $user->password))
					{
						$select = DB::table('company')->where('id', $user->companyId)->first();

						if (isset($select))
						{
							Session::put('companyName' , $select->companyName);
							Session::put('companyCode', $select->companyCode);
							Session::put('companyId', $user->companyId);
							Session::put('plan', $select->plan);
						}		
						
						if ($user->type=='Employee')
						{
							$select1 = DB::table('personalDetails')->where('id', $user->id)->first();
							$select2 = DB::table('BankDetails')->where('id', $user->id)->first();
							$select3 = DB::table('userProgress')->where('userid', $user->id)->first();

							if (empty($select3)) 
							{
								$insertUserProgress = DB::table('userProgress')
													->insert([
														'userid' 	=> $user->id,
														'nextStep' 	=> 'step1',
														'complete'  => 'off'
														]);

								$select3 	= DB::table('userProgress')
											->where('userid', $user->id)
											->first();
							}

							Session::put('userSession', $user);
							Session::put('type', $user->type);
							Session::put('employeeId', $user->id);
							Session::put('email', $user->email);
							Session::put('employeeName', $user->firstName);
							Session::put('last_acted_on', time());

							if (!empty($select3)) 
							{
								Session::put('employeeProgressNextStep', $select3->nextStep);
								Session::put('employeeProgressComplete', $select3->complete);
							}
							else
							{
								Session::put('employeeProgressNextStep', 'step1');
								Session::put('employeeProgressComplete', 'off');
							}

							// echo "<pre>";
							// var_dump(session()->get('employeeProgressNextStep'));
							// var_dump(session()->get('employeeProgressComplete'));
							// die;
							
							$complete = Session::get('employeeProgressComplete');

							if (isset($complete) and ($complete == 'on'))
							{
								return Redirect::to('employeeDashboard1');
							}
							else
							{
								return Redirect::to('employeeDashboard');
							}
						}
						elseif ($user->type=='Contractor') 
						{
							$select1 = DB::table('personalDetails')->where('id', $user->id)->first();
							$select2 = DB::table('BankDetails')->where('id', $user->id)->first();
							$select3 = DB::table('userProgress')->where('userid', $user->id)->first();

							if (empty($select3)) 
							{
								$insertUserProgress = DB::table('userProgress')
													->insert([
														'userid' 	=> $user->id,
														'nextStep' 	=> 'step1',
														'complete'  => 'off'
														]);

								$select3 	= DB::table('userProgress')
											->where('userid', $user->id)
											->first();
							}

							Session::put('userSession', $user);
							Session::put('type', $user->type);
							Session::put('contractorId', $user->id);
							Session::put('email', $user->email);
							Session::put('contractorName', $user->firstName);
							Session::put('last_acted_on', time());

							if (!empty($select3)) 
							{
								Session::put('employeeProgressNextStep', $select3->nextStep);
								Session::put('employeeProgressComplete', $select3->complete);
							}
							else
							{
								Session::put('employeeProgressNextStep', 'step1');
								Session::put('employeeProgressComplete', 'off');
							}

							// echo "<pre>";
							// var_dump(session()->get('employeeProgressNextStep'));
							// var_dump(session()->get('employeeProgressComplete'));
							// die;
							
							$complete = Session::get('employeeProgressComplete');

							if (isset($complete) and ($complete == 'on'))
							{
								return Redirect::to('contractorDashboard1');
							}
							else
							{
								return Redirect::to('contractorDashboard');
							}
						}
						elseif ($user->type=="Employer") 
						{
							$check 	= DB::table('user')
									->where('id', $select->created_by)
									->where('type', 'accountant')
									->first();


							if (!empty($check)) 
							{
								Session::put('created_by', $check->id);
							}
							else
							{
								Session::put('created_by', NULL);
							}

							Session::put('userSession', $user);
							Session::put('type', $user->type);
							Session::put('employerId', $user->id);
							Session::put('email', $user->email);
							Session::put('employerName', $user->firstName);
							Session::put('last_acted_on', time());

							return Redirect::to('getStarted');
						} 
						elseif ($user->type=="Admin") 
						{
							Session::put('userSession', $user);
							Session::put('type', $user->type);
							Session::put('adminId', $user->id);
							Session::put('email', $user->email);
							Session::put('adminName', $user->firstName);
							Session::put('last_acted_on', time());

							return Redirect::to('adminDashboard');
						} 
						elseif (($user->type=="Manager") or (strtolower($user->type) == 'ca')) 
						{
							Session::put('type', $user->type);
							Session::put('caId', $user->id);
							Session::put('email', $user->email);
							Session::put('managerName', $user->firstName);

							return Redirect::to('managerDashboard');
						}
						elseif ($user->type=="Accountant") 
						{
							$details 	= DB::table('personalDetails2')
										->where('userid', $user->id)
										->first();

							Session::put('userSession', $user);
							Session::put('type', $user->type);
							Session::put('accountantId', $user->id);
							Session::put('email', $user->email);
							Session::put('accountantName', $user->firstName);
							Session::put('accountantFullName', $user->firstName . ' ' . $user->lastName);
							Session::put('accountantDetails', $details);
							Session::put('last_acted_on', time());

							return Redirect::to('accountantDashboard');
						}
						else 
						{
							return View::make('login')
									->with('result', "Your access level is not defined. Contact your SUPERIOR ASAP.");
						}
					}
					else {
						return View::make('login')->with('error_pass', "Incorrect Password");
					}
				}
				else
				{
					return View::make('login')->with('error_status', "Your account has not been activated yet. Please activate it using the activation link sent to you by email.");
				}
			}
			else {
				return View::make('login')->with('error_email', "That email address is not registered with us.");
			}
		}
	}

	public function signin()
	{
		
		session()->flush();
		session_start();

		$validator = Validator::make(
			$auth = array(
				'email'  => Input::get('email'),
				'password'  => Input::get('password'),
				),
			array(
				'email'  => 'required|email',
				'password'  => 'required',
				)
			);
		
		if($validator->fails())
		{
			return response()->json(['id'=> session_id() , 'messages'=>$validator->errors()->all()[0]]);
			// return response()->json('id'=> session_id() ,  ,'messages',$validator->errors()->all()[0] );
		}
		else
		{
			$user 	= DB::table("user")
					->where("email", $auth['email']) // "=" is optional
					->first();

			if(isset($user))
			{
				if ($user->status=="Active")
				{
					if(Hash::check($auth['password'], $user->password))
					{
						$select = DB::table('company')->where('id', $user->companyId)->first();
						if (isset($select)){
							Session::put('companyName' , $select->companyName);
							Session::put('companyCode', $select->companyCode);
							Session::put('companyId', $user->companyId);
							Session::put('plan', $select->plan);
						}		
						if ($user->type=='Employee'){
							$select1 = DB::table('personalDetails')->where('id', Session::get('id'))->first();
							$select2 = DB::table('BankDetails')->where('id', Session::get('id'))->first();
							$select3 = DB::table('userProgress')->where('userid', $user->id)->first();

							Session::put('userSession', $user);
							Session::put('type', $user->type);
							Session::put('employeeId', $user->id);
							Session::put('email', $user->email);
							Session::put('employeeName', $user->firstName);
							Session::put('apiKey', $user->apiKey);
							Session::put('last_acted_on', time());

							if (!empty($select3)) 
							{
								Session::put('employeeProgressNextStep', $select3->nextStep);
								Session::put('employeeProgressComplete', $select3->complete);
							}
							else
							{
								Session::put('employeeProgressNextStep', 'step1');
								Session::put('employeeProgressComplete', 'off');
							}							
							
							$complete = Session::get('employeeProgressComplete');

							if (isset($complete) and ($complete == 'on'))
							{
								return response()->json(['id'=> session_id() , 'apiKey'=> session()->get('apiKey') ,'messages'=>'complete']);
								// return response()->json('id'=> session_id() ,  ,'messages'=> 'complete');
							}
							else
							{
								return response()->json(['id'=> session_id() , 'apiKey'=> session()->get('apiKey') ,'messages'=> 'forms']);
							}
						}
						
						else {
							return response()->json(['id'=> session_id() ,  'messages'=> 'wrong userType']);
						}
					}
					else {
						return response()->json(['id'=> session_id() , 'messages'=> 'wrong password']);
					}
				}
				else
				{
					return response()->json(['id'=> session_id() ,  'messages'=> 'not active']);
				}
			}
			else {
				return response()->json(['id'=> session_id() , 'messages'=> 'not registered']);
			}
		}
	}

	public function logout()
	{
		Auth::logout();
		Session::flush();

		return Redirect::to('/');
	}
}	


