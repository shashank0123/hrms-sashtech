<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator, Response, DB, Input,Session, Hash,View, Mail, StdClass;
use Illuminate\Http\Request;

class adminController extends Controller {

	public function dashboard()
	{
		if (strtolower(session()->get('type')) != 'admin') 
		{
			return redirect('login');
		}

		$company 	= DB::table('company')->get();
		$user 		= DB::table('user')->get();
		$payroll 	= DB::table('payroll')->get();
		$benefit 	= DB::table('benefit')->get();

		$count 				= new StdClass;
		$count->company 	= count($company);
		$count->user 		= count($user);
		$count->payslips 	= count($payroll);
		$count->benefit 	= count($benefit);

		return View::make('admin/adminDashboard')->with('count', $count);
	}

	public function createUser()
	{
		$validator = Validator::make(
			$entries = array(
					'email' => strtolower(Input::get('email')),
					'type' => Input::get('userType'),
				),
				array(
					'type' => 'required',
					'email' => 'required|email',
			    ),
				$messages = array(
				   'required' => 'The :attribute field is required.',
				   'email' => 'The email is not valid.'
				)
			);

		if ($validator->fails())
	    {
		   return Response::json($validator->messages());
		}			   
		else
		{	
			$count	= DB::table('user')
					->where('type', 'Admin')
					->orWhere('type', 'Manager')
					->orWhere('type', 'Accountant')
					->orWhere('type', 'CA')
					->orderBy('created_at', 'DESC')
					->first();

			$id 			=	$count->id+1;
			$entries['id'] 	= $id;

			$select = DB::table('user')
					->where('email', $entries['email'])
					->first();
			
			if (empty($select))
			{
				$apiKey = Hash::make($entries['email']);
				$entries['apiKey'] = $apiKey;

				$result = DB::table('user')
						->insert($entries);
				
				$fullName = "user";

				$email 	= Mail::send(
							'emails.newUser',
							array(
								'email'=>$entries['email'],
								'fullname'=>$fullName,
								'apiKey' => $apiKey,
								'adminfrom' => 'rajbabuhome@gmail.com'
							),
							function($message) 
							{
								$message->from('rajbabuhome@gmail.com', 'Sashtechs');
								$message->to(Input::get('email'))->subject('Welcome to Sashtechs!');
							}
						);
				
				$select = DB::table('user')
						->where('email', $entries['email'])
						->first();

				if ($email == 1) 
				{
					$result 			= new StdClass;
					$result->message 	= "The user has been created.";
					$result->status 	= 200;
					$result->id 		= $select->id;
				}
				else
				{
					$delete 	= DB::table('user')
								->where('id', $select->id)
								->delete();

					return false;
				}
			}
			else 
			{
				$result = new StdClass;
				$result->message = "The user with that email address already exists.";
				$result->status = 208;
			}

			return response()->json($result);
		}
	}	

	public function viewUser()
	{
		if (strtolower(session()->get('type')) != 'admin') 
		{
			return redirect('login');
		}

		$users 	= DB::table('user')
				->where('type', 'Admin')
				->orWhere('type', 'Manager')
				->orWhere('type', 'Accountant')
				->orWhere('type', 'CA')
				->orderBy('created_at', 'desc')
				->get();
		
		return View::make('admin/adminCreateUser')
				->with('users', $users);		
	}	

	public function selectCompany()
	{
		if (strtolower(session()->get('type')) != 'admin') 
		{
			return redirect('login');
		}

		$company = DB::table('company')
	            ->join('companyAddress', 'company.id', '=', 'companyAddress.companyId')
	            ->get();

		return View::make('admin/adminSelectCompany')
				->with('company', $company);		
	}	

	public function changeCompany($id)
	{
		if (strtolower(session()->get('type')) != 'admin') 
		{
			return redirect('login');
		}
		
		$select = DB::table('company')
				->where('id', $id)
				->first();

		session()->put('companyId', $select->id);
		session()->put('companyName', $select->companyName);
		session()->put('companyCode', $select->companyCode);
		session()->put('plan', $select->plan);
		session()->put('type', 'employer');
		session()->put('employerId', session()->get('adminId'));
		session()->put('employerName', 'Admin');
		session()->put('viewChanged', true);

		return redirect('getStarted');
	}	

	public function updateStatus($id)
	{
		$status 	= ucfirst(strtolower(Input::get('status')));

		$update 	= DB::table('user')
					->where('id', $id)
					->update([
						'status' 		=> $status,
						'updated_at'	=> date('Y-m-d h:i:sa'),
						]);

		$result 			= new StdClass;
		$result->status 	= 200;

		return response()->json($result);
	}

	public function viewAssignCompany()
	{
		if (strtolower(session()->get('type')) != 'admin') 
		{
			return redirect('login');
		}

		$manager 	= DB::table('user')
					->where('type', 'Manager')
					->orWhere('type', 'CA')
					->where('status', 'Active')
					->get();

		$company 	= DB::table('company')
					->join('companyAddress', 'company.id', '=', 'companyAddress.companyId')
					->where('company.plan', 'manager')
					->orderBy('company.id', 'ASC')
					->select('company.*', 'companyAddress.logoUrl')
					->get();

		return View::make('admin/adminAssignCompany')
				->with('manager', $manager)
				->with('company', $company);
	}

	public function assignCompany()
	{
		$id 		= Input::get('manager');
		$company 	= Input::get('companies');

		$check 	= DB::table('user')
				->where('id', $id)
				->first();

		$result 			= new StdClass;
		
		if (!empty($check)) 
		{
			$prev 		= $check->companyId;

			if (strlen($prev) > 1) 
			{
				$prev 		= explode(',', $prev);
				$company1 	= explode(',', $company);

				$diff 		= array_diff($prev, $company1);

				foreach ($diff as $key => $value) 
				{
					$delete = DB::table('company')
							->where('id', $value)
							->update([
								'managerId' 	=> '',
								'updated_at'	=> date('Y-m-d h:i:sa'),
								]);
				}
			}
			elseif((strlen($prev) == 1) and ($prev != $company))
			{
				$delete = DB::table('company')
						->where('id', $prev)
						->update([
							'managerId' 	=> '',
							'updated_at'	=> date('Y-m-d h:i:sa'),
							]);
			}

			$update 	= DB::table('user')
						->where('id', $id)
						->update([
							'companyId' 	=> $company,
							'updated_at'	=> date('Y-m-d h:i:sa'),
							]);

			if (strlen($company) > 1) 
			{
				$companies 	= explode(',', $company);

				foreach ($companies as $key => $value) 
				{
					$updateCompany 	= DB::table('company')
									->where('id', $value)
									->update([
										'managerId' 	=> $id,
										'updated_at'	=> date('Y-m-d h:i:sa'),
										]);
				}
			}
			else
			{
				$updateCompany 	= DB::table('company')
								->where('id', $company)
								->update([
									'managerId' 	=> $id,
									'updated_at'	=> date('Y-m-d h:i:sa'),
									]);
			}

			$result->status 	= 200;
			$result->message 	= 'Company assigned successfully.';

		}
		return response()->json($result);
 	}

 	public function addCompany()
	{
		$validator = Validator::make(
			$entries = array(
					'EmployerName' => ucwords(Input::get('EmployerName')),
					'supportmail' => strtolower(Input::get('supportmail')),
					'companyName' => Input::get('companyName'),
				),
				array(
					'EmployerName' => 'required',
					'supportmail' => 'required|email',
					'companyName' => 'required',
				),
			$messages = array(
				   'required' => 'The :attribute field is required.',
				   'email' => 'The email is not valid.'
				)
			);
		
		if ($validator->fails())
		{
			return Response::json($validator->messages());
		}
		else
		{
			$checkEmail 	= $entries['supportmail'];
			$checkEmail 	= explode("@", $checkEmail)[1];
			$emailDomain 	= explode(".", $checkEmail)[0];
			// Not allowed email domains
			$notAllowedDomains = array( 'yahoo', 'rediffmail', 'hotmail', 'outlook', 'ibibo', 'live', 'facebook');

			if (in_array($emailDomain, $notAllowedDomains)) 
			{
				$result = new StdClass;
				$result->message = "That email address is not allowed.";
				$result->status = 406;
				return response()->json($result);
			}

			$word_count 	= str_word_count($entries['companyName']);			
			$getCompanyCode = DB::table('company')
							->select('companyCode')->get();

			if (isset($getCompanyCode)) 
			{
				foreach ($getCompanyCode as $key => $value) 
				{
					$arr[] = $value->companyCode;
				}
			}

			if (!isset($arr)) 
			{
				$arr = array();
			}

			do 
			{
				if (strlen($entries['companyName']) < 4) {
					$word_count = 0;
				}

				if ($word_count == 0) 
				{
					$pool = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
					$length = 4;
					$entries['companyCode'] = strtoupper(substr(str_shuffle(str_repeat($pool, $length)), 0, $length));
				}
				elseif ($word_count < 2) 
				{
					$entries['companyCode'] = strtoupper(substr($entries['companyName'], 0, 4));
				}
				elseif ( $word_count < 4 ) 
				{
					$words = preg_split("/\s+/", $entries['companyName']);
					$entries['companyCode'] = "";
					foreach ($words as $w) 
					{
						$entries['companyCode'] .= $w[0];
						$entries['companyCode'] .= $w[1];
					}
					$entries['companyCode'] = strtoupper(substr($entries['companyCode'], 0, 4));
				}
				else 
				{
					$words = preg_split("/\s+/", $entries['companyName']);
					$entries['companyCode'] = "";
					foreach ($words as $w) 
					{
						$entries['companyCode'] .= $w[0];
					}
					$entries['companyCode'] = strtoupper(substr($entries['companyCode'], 0, 4));
				}

				$word_count = 0;
			} while (in_array($entries['companyCode'], $arr));

			$checkUser 	= DB::table('user')
						->where('email', $entries['supportmail'])
						->first();

			if (!empty($checkUser)) 
			{
				$result = new StdClass;
				$result->message = "The employer with that email address already exists.";
				$result->status = 208;

				return response()->json($result);
			}

			$select = DB::table('company')
					->where('supportmail', $entries['supportmail'])
					->first();

			if (!empty($select)) 
			{
				$result = new StdClass;
				$result->message = "That email address is already registered.";
				$result->status = 208;
				return response()->json($result);
			}

			// $select1 = DB::table('company')
			// 		->where('companyName', $entries['companyName'])
			// 		->first();

			// if (!empty($select1)) 
			// {
			// 	$result = new StdClass;
			// 	$result->message = "That company name is already taken.";
			// 	$result->status = 209;
			// 	return response()->json($result);
			// }

			$entries['created_by'] = session()->get('adminId');

			$result = DB::table('company')
					->insert($entries);

			$id = DB::getPdo()->lastInsertId();
			
			$entries1 = array(
				'id' 		=> strtoupper(substr($entries['companyCode'], 0, 4)).'1',
				'firstName' => ucfirst(strtolower(explode(' ', Input::get('EmployerName'))[0])),
				'email' 	=> strtolower(Input::get('supportmail')),
				'companyId' => $id,
				'status' 	=> 'Inactive',
				'type'		=> 'Employer',
				'created_by'=> session()->get('adminId'),
			);

			$apiKey 			= Hash::make($entries1['email']);
			$entries1['apiKey'] = $apiKey;

			$result = DB::table('user')
					->insert($entries1);    
			
			$id1 	= DB::getPdo()
					->lastInsertId();

			// for email
			$select1 = DB::table('user')->where('email', $entries1['email'])->first();
			
			$email = Mail::send(
					'emails.newUser',
					array(
						'email'		=> $entries1['email'], 
						'fullname'	=> $entries1['firstName'], 
						'apiKey'	=> $apiKey, 
						'adminfrom' => 'rajbabuhome@gmail.com'
					), function($message) 
						{
							$message->from('rajbabuhome@gmail.com', 'Sashtechs');
							$message->to(Input::get('supportmail'))->subject('Welcome to Sashtechs!');
						}
					);

			if ($email == 1) 
			{
				$result = new StdClass;
				// $result->message = "Registration successfull. Please verify your email.";
				$result->status = 200;
			}
			else
			{
				$deleteUser		= DB::table('user')
								->where('id', $id1)
								->delete();

				$deleteCompany 	= DB::table('company')
								->where('id', $id)
								->delete();

				$result 	= new StdClass;

				return response()->json($result);
			}

			$next 	= Db::table('employerProgress')
					->insert([
						'companyId' => $id, 
						'nextStep' => 'step1'
						]);

			return response()->json($result);
		}
	}
}
