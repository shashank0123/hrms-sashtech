<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB, View, Validator, Input, StdClass;
use Illuminate\Http\Request;

class oneTimePaymentController extends Controller {

	public function getOneTimePaymentData()
	{
		$payments=DB::table('oneTimePayment')
				->where('companyId', session()->get('companyId'))
				->where('month', date('M'))
				->get();

		$teams = DB::table('team')
				->where('companyId', session()->get('companyId'))			
				->get();

		return View::make('oneTimePayment')
				->with('teams', $teams)
				->with('payments', $payments);
	}

	public function teamDetails($team)
	{
		$details = DB::table('user')
				->where('companyId', session()->get('companyId'))
				->where('team', $team)
				->select('id', 'firstName', 'lastName')
				->get();

		return $details;
	}

	public function enterOTP()
	{
		$count = Input::get('varcount');
		
		for ($i=0;$i<$count;$i++)
		{
			$validator = Validator::make(
				$entries[$i] = array(
					'companyId' => session()->get('companyId'),
					'type' => Input::get('type'),
					'team' => Input::get('team'),
					'userId' => Input::get('employeeId'.$i),
					'amount' => Input::get('employeeAmount'.$i),
					'month' => date('F'),
				),
				array(
					'type' => 'required|string',
					'userId' => 'required',
					'amount' => 'required|numeric',
			    ),
				$messages = array(
				   'required' => 'This is required.',
				   'string' => 'This must be a string.',
				   'numeric' => 'This must be a number.',
				)
			);

			if ($validator->fails())
	        {
	        	return response()->json($validator->messages());
			}
		}

		foreach ($entries as $key => $value) 
		{
			$select = DB::table('oneTimePayment')
				->insert($value);
		}

		$result = new StdClass;
		$result->status = '200';
		$result->message = 'Data inserted successfully.';

		return response()->json($result);
	}
}
