<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input, Validator, Session,DB ,Response, View;
use Illuminate\Http\Request;

class couponController extends Controller {

	public function addCoupon()
	{
		$validator = Validator::make(
				$entries = array(
						'code' 		=> strtoupper(Input::get('code')),
						'month' 	=> Input::get('month'),
						'value' 	=> Input::get('value'),
						'type' 		=> Input::get('type'),
					    
					),
					array(
						'code' 	=> 'required',
						// 'month'	=> 'required',
						'value' => 'numeric',
						'type'	=> 'required',
				    ),
				$messages = array(
					   'required' => 'The :attribute field is required.',
					)
				);

			if ($validator->fails())
            {
			   return Response::json($validator->messages());
			}			   
			else
			{
				$check 	= DB::table('coupon')
						->where('code', $entries['code'])
						->first();

				if (!empty($check)) 
				{
					return response()->json([
			   				'status' => 208,
			   				'message'=>'That coupon code already exists.', 
			   			]);	
				}

				$result = DB::table('coupon')
						->where('code', $entries['code'])
						->insert($entries);
				
		   		return response()->json([
		   				'result' => $result, 
		   				'status' => 200, 
		   				'message'=>'Data inserted', 
		   			]);
			}		
	}

	public function viewCoupon()
	{
		if (strtolower(session()->get('type')) != 'admin') 
		{
			return redirect('login');
		}
		
		$coupon = DB::table('coupon')
			    ->get();

		return View::make('admin/coupon')
				->with('coupon', $coupon);		
	}	

	public function updateStatus($id)
	{
		$status 	= ucfirst(strtolower(Input::get('status')));

		$update 	= DB::table('coupon')
					->where('id', $id)
					->update([
						'status' 		=> $status,
						'updated_at'	=> date('Y-m-d h:i:sa'),
						]);

		$result 			= new StdClass;
		$result->status 	= 200;

		return response()->json($result);
	}

}
