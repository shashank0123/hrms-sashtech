<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB, Session, View;
use Illuminate\Http\Request;

class taxslipController extends Controller {
	public function getTaxSlip()
	{
		if (strtolower(session()->get('type')) != 'employee' ) 
		{
			return redirect('login');
		}

		$select 	= DB::table('investmentDeduction')
					->where('userid', session()->get('employeeId'))
					->first();	
		
		$select1	= DB::table('medicalDeduction')
					->where('userid', session()->get('employeeId'))
					->first();	
		
		$select2	= DB::table('HRAmonthlyDeduction')
					->where('userid', session()->get('employeeId'))
					->first();	
		
		$select3	= DB::table('otherDeduction')
					->where('userid', session()->get('employeeId'))
					->first();	
		
		$select4	= DB::table('selfOccupiedDeduction')
					->where('userid', session()->get('employeeId'))
					->first();	
		
		$select5	= DB::table('previousEmployerDeduction')
					->where('userid', session()->get('employeeId'))
					->first();	
		
		$select6	= DB::table('user')
					->where('id', session()->get('employeeId'))
					->first();
		
		$select7	= DB::table('personalDetails')
					->where('userid', session()->get('employeeId'))
					->first();	
		
		$select8	= DB::table('employeeCtc')
					->where('userid', session()->get('employeeId'))
					->first();

		return view::make('taxslip')
				->with('select', $select)
				->with('select1', $select1)
				->with('select2', $select2)
				->with('select3', $select3)
				->with('select4', $select4)
				->with('select5', $select5)
				->with('select6', $select6)
				->with('select7', $select7)
				->with('select8', $select8);
	}
}
