<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input, Validator, Session, DB, Mail, StdClass, Hash, Response;

use Illuminate\Http\Request;

class addEmployerController extends Controller 
{

	public function signUpEmployer()
	{
		if ((!empty(Input::get('supportmail')))&&(!is_null(Input::get('supportmail')))) 
		{
			$validator = Validator::make(
				$entries = array(
						'EmployerName' => ucwords(Input::get('EmployerName')),
						'supportmail' => strtolower(Input::get('supportmail')),
						'companyName' => Input::get('companyName'),

					),
					array(
						'EmployerName' => 'required',
						'supportmail' => 'required|email',
						'companyName' => 'required',
					),
				$messages = array(
					   'required' => 'The :attribute field is required.',
					   'email' => 'The email is not valid.'
					)
				);

			if (session()->get('plan'))
			{
				$entries['plan']=session()->get('plan');
			}
			
			if ($validator->fails())
			{
				return Response::json($validator->messages());
			}
			else
			{
				$checkEmail = $entries['supportmail'];
				$checkEmail = explode("@", $checkEmail)[1];
				$emailDomain = explode(".", $checkEmail)[0];
				// Not allowed email domains
				$notAllowedDomains = array( 'gmail', 'yahoo', 'rediffmail', 'hotmail', 'outlook', 'ibibo', 'live', 'facebook');

				if (in_array($emailDomain, $notAllowedDomains)) 
				{
					$result = new StdClass;
					$result->message = "That email address is not allowed.";
					$result->status = 406;
					return response()->json($result);
				}

				$word_count 	= str_word_count($entries['companyName']);			
				$getCompanyCode = DB::table('company')
								->select('companyCode')->get();

				if (isset($getCompanyCode)) 
				{
					foreach ($getCompanyCode as $key => $value) 
					{
						$arr[] = $value->companyCode;
					}
				}

				if (!isset($arr)) 
				{
					$arr = array();
				}

				do 
				{
					if (strlen($entries['companyName']) < 4) {
						$word_count = 0;
					}

					if ($word_count == 0) 
					{
						$pool = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
						$length = 4;
						$entries['companyCode'] = strtoupper(substr(str_shuffle(str_repeat($pool, $length)), 0, $length));
					}
					elseif ($word_count < 2) 
					{
						$entries['companyCode'] = strtoupper(substr($entries['companyName'], 0, 4));
					}
					elseif ( $word_count < 4 ) 
					{
						$words = preg_split("/\s+/", $entries['companyName']);
						$entries['companyCode'] = "";
						foreach ($words as $w) 
						{
							$entries['companyCode'] .= $w[0];
							$entries['companyCode'] .= $w[1];
						}
						$entries['companyCode'] = strtoupper(substr($entries['companyCode'], 0, 4));
					}
					else 
					{
						$words = preg_split("/\s+/", $entries['companyName']);
						$entries['companyCode'] = "";
						foreach ($words as $w) 
						{
							$entries['companyCode'] .= $w[0];
						}
						$entries['companyCode'] = strtoupper(substr($entries['companyCode'], 0, 4));
					}

					$word_count = 0;
				} while (in_array($entries['companyCode'], $arr));

				$checkuser	= DB::table('user')
							->where('email', $entries['supportmail'])
							->first();

				if (!empty($checkuser)) 
				{
					$result = new StdClass;
					$result->message = "A user with that email address already exists.";
					$result->status = 208;

					return response()->json($result);
				}

				$select = DB::table('company')
						->where('supportmail', $entries['supportmail'])
						->first();

				if (!empty($select)) 
				{
					$result = new StdClass;
					$result->message = "That email address is already registered.";
					$result->status = 208;
					return response()->json($result);
				}

				// $select1 = DB::table('company')
				// 	->where('companyName', $entries['companyName'])
				// 	->first();

				// if (!empty($select1)) 
				// {
				// 	$result = new StdClass;
				// 	$result->message = "That company name is already taken.";
				// 	$result->status = 209;
				// 	return response()->json($result);
				// }

				if (session()->get('plan'))
				{
					$entries['plan']=session()->get('plan');
				}

				$result = DB::table('company')
						->insert($entries);

				$id = DB::getPdo()->lastInsertId();
				
				$entries1 = array(
					'id' 		=> strtoupper(substr($entries['companyCode'], 0, 4)).'1',
					'firstName' => ucfirst(strtolower(Input::get('EmployerName'))),
					'email' 	=> strtolower(Input::get('supportmail')),
					'companyId' => $id,
					'password'  => Hash::make(Input::get('password')),
					'status' 	=> 'Inactive',
					'type'		=> 'Employer',
					// 'created_at' => date("d-m-y h:i:sa")
				);

				$apiKey 			= Hash::make($entries1['email']);
				$entries1['apiKey'] = $apiKey;

				$result = DB::table('user')
						->insert($entries1);    
				
				$id1 	= DB::getPdo()
						->lastInsertId();

				// for email
				$select1 = DB::table('user')->where('email', $entries1['email'])->first();
				
				$email = Mail::send(
						'emails.signUpEmployer',
						array(
							'email'		=> $entries1['email'], 
							'name'		=> $entries1['firstName'], 
							'apiKey'	=> $apiKey, 
							'adminfrom' => 'rajbabuhome@gmail.com'
						), function($message) 
							{
								$message->from('rajbabuhome@gmail.com', 'Sashtechs');
								$message->to(Input::get('supportmail'))->subject('Welcome to Sashtechs!');
							}
						);

				if ($email == 1) 
				{
					$result 			= new StdClass;
					$result->message 	= "Registration successfull. Please verify your email.";
					$result->status 	= 200;
					$result->url 		= 'login';
				}
				else
				{
					$deleteUser		= DB::table('user')
									->where('id', $id1)
									->delete();

					$deleteCompany 	= DB::table('company')
									->where('id', $id)
									->delete();

					$result 	= new StdClass;

					return response()->json($result);
				}

				$next 	= Db::table('employerProgress')
						->insert([
							'companyId' => $id, 
							'nextStep' => 'step1'
							]);

				return response()->json($result);
			}
		}
	}

	public function confirmEmail()
	{	
		$apiKey = Input::get('apiKey');
		
		$select = DB::table('user')
				->where('apiKey', $apiKey)
				->update(['status' => 'Active']);

		if ($select) 
		{
			return redirect('getStarted');
		}
		else
		{
			return redirect('signup');
		}
	}

	public function verifyNumber($value='')
	{	
		$user 	= DB::table('user')
				->where('id', session()->get('employerId'))
				->where('randomNumber', Input::get('code'))
				->first();

		if (empty($user)) 
		{
			$result=new StdClass;
			$result->status=400;
			$result->message="This authentication code is not valid. Please enter again or request another one";
			return response()->json($result);
		}
		else 
		{
			return response()->json($user);
		}
	}

	public function selectPlan()
	{
		session()->put('plan', Input::get('plan'));
		return redirect('signup');
	}

	// used for the form from the main page
	public function fromMain()
	{
		if ((!empty(Input::get('supportmail')))&&(!is_null(Input::get('supportmail')))) 
		{
			$validator = Validator::make(
				$entries = array(
						'EmployerName' => ucwords(Input::get('EmployerName')),
						'supportmail' => strtolower(Input::get('supportmail')),
						'companyName' => Input::get('companyName'),
					),
					array(
						'EmployerName' => 'required',
						'supportmail' => 'required|email',
						'companyName' => 'required',
					),
				$messages = array(
					   'required' => 'The :attribute field is required.',
					   'email' => 'The email is not valid.'
					)
				);

			if (session()->get('plan'))
			{
				$entries['plan']=session()->get('plan');
			}
			
			if ($validator->fails())
			{
				return Response::json($validator->messages());
			}
			else
			{
				$checkEmail = $entries['supportmail'];
				$checkEmail = explode("@", $checkEmail)[1];
				$emailDomain = explode(".", $checkEmail)[0];
				// Not allowed email domains
				$notAllowedDomains = array( 'gmail', 'yahoo', 'rediffmail', 'hotmail', 'outlook', 'ibibo', 'live', 'facebook');

				if (in_array($emailDomain, $notAllowedDomains)) 
				{
					$result = new StdClass;
					$result->message = "That email address is not allowed.";
					// $result->status = 406;

					return view('hello')
							->with('result', $result);
				}

				$word_count 	= str_word_count($entries['companyName']);			
				$getCompanyCode = DB::table('company')
								->select('companyCode')->get();

				if (isset($getCompanyCode)) 
				{
					foreach ($getCompanyCode as $key => $value) 
					{
						$arr[] = $value->companyCode;
					}
				}

				if (!isset($arr)) 
				{
					$arr = array();
				}

				do 
				{
					if (strlen($entries['companyName']) < 4) 
					{
						$word_count = 0;
					}

					if ($word_count == 0) 
					{
						$pool = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
						$length = 4;
						$entries['companyCode'] = strtoupper(substr(str_shuffle(str_repeat($pool, $length)), 0, $length));
					}
					elseif ($word_count < 2) 
					{
						$entries['companyCode'] = strtoupper(substr($entries['companyName'], 0, 4));
					}
					elseif ( $word_count < 4 ) 
					{
						$words = preg_split("/\s+/", $entries['companyName']);
						$entries['companyCode'] = "";
						foreach ($words as $w) 
						{
							$entries['companyCode'] .= $w[0];
							$entries['companyCode'] .= $w[1];
						}
						$entries['companyCode'] = strtoupper(substr($entries['companyCode'], 0, 4));
					}
					else 
					{
						$words = preg_split("/\s+/", $entries['companyName']);
						$entries['companyCode'] = "";
						foreach ($words as $w) 
						{
							$entries['companyCode'] .= $w[0];
						}
						$entries['companyCode'] = strtoupper(substr($entries['companyCode'], 0, 4));
					}

					$word_count = 0;
				} while (in_array($entries['companyCode'], $arr));

				$checkuser	= DB::table('user')
							->where('email', $entries['supportmail'])
							->first();

				if (!empty($checkuser)) 
				{
					$result = new StdClass;
					$result->message = "A user with that email address already exists.";
					// $result->status = 208;

					return view('hello')
							->with('result', $result);
				}

				$select = DB::table('company')
						->where('supportmail', $entries['supportmail'])
						->first();

				if (!empty($select)) 
				{
					$result = new StdClass;
					$result->message = "That email address is already registered.";
					// $result->status = 208;

					return view('hello')
							->with('result', $result);
				}

				// $select1 = DB::table('company')
				// 	->where('companyName', $entries['companyName'])
				// 	->first();

				// if (!empty($select1)) 
				// {
				// 	$result = new StdClass;
				// 	$result->message = "That company name is already taken.";
				// 	$result->status = 209;
				// 	return response()->json($result);
				// }

				if (session()->get('plan'))
				{
					$entries['plan']=session()->get('plan');
				}

				$result = DB::table('company')
						->insert($entries);

				$id = DB::getPdo()->lastInsertId();
				
				$entries1 = array(
					'id' 		=> strtoupper(substr($entries['companyCode'], 0, 4)).'1',
					'firstName' => ucwords(strtolower(Input::get('EmployerName'))),
					'email' 	=> strtolower(Input::get('supportmail')),
					'companyId' => $id,
					'password'  => Hash::make(Input::get('password')),
					'status' 	=> 'Inactive',
					'type'		=> 'Employer',
					// 'created_at' => date("d-m-y h:i:sa")
				);

				$apiKey 			= Hash::make($entries1['email']);
				$entries1['apiKey'] = $apiKey;

				$result = DB::table('user')
						->insert($entries1);    
				
				$id1 	= DB::getPdo()
						->lastInsertId();

				// for email
				$select1 = DB::table('user')->where('email', $entries1['email'])->first();
				
				$email = Mail::send(
						'emails.signUpEmployer',
						array(
							'email'		=> $entries1['email'], 
							'name'		=> $entries1['firstName'], 
							'apiKey'	=> $apiKey, 
							'adminfrom' => 'rajbabuhome@gmail.com'
						), function($message) 
							{
								$message->from('rajbabuhome@gmail.com', 'Sashtechs');
								$message->to(Input::get('supportmail'))->subject('Welcome to Sashtechs!');
							}
						);

				if ($email == 1) 
				{
					$result 			= new StdClass;
					$result->message 	= "Registration successfull. Please verify your email.";
					// $result->status 	= 200;
					// $result->url 		= 'login';
				}
				else
				{
					$deleteUser		= DB::table('user')
									->where('id', $id1)
									->delete();

					$deleteCompany 	= DB::table('company')
									->where('id', $id)
									->delete();

					$result 	= new StdClass;

					return response()->json($result);
				}

				$next 	= Db::table('employerProgress')
						->insert([
							'companyId' => $id, 
							'nextStep' => 'step1'
							]);

				return view('hello')
						->with('result', $result);
			}
		}
	}

}
