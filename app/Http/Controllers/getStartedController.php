<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;

class getStartedController extends Controller {

	public function viewDashboard()
	{
		if (strtolower(session()->get('type')) != 'employer') 
		{
			return redirect('login');
		}
		else
		{

			if (empty(session()->get('plan')))
			{
				if (!empty(session()->get('created_by'))) 
				{
					return redirect('accountantSelectPlan');
				}

				return redirect('selectPlan');
			}
			
			$select = DB::table('employerProgress')
					->where('companyId', session()->get('companyId'))
					->first();
			
			if (!empty($select))
			{
				session()->put('step1', $select->step1);
				session()->put('step2', $select->step2);
				session()->put('step3', $select->step3);
				session()->put('step4', $select->step4);
				session()->put('step5', $select->step5);
				session()->put('step6', $select->step6);
				session()->put('step7', $select->step7);
				session()->put('step8', $select->step8);
				session()->put('nextStep', $select->nextStep);
				session()->put('complete', $select->complete);
				
				if (session()->get('complete')!='on')
				{
					if (session()->get('step1')=='on' && session()->get('step2')=='on' && session()->get('step3')=='on' && session()->get('step4')=='on' && session()->get('step5')=='on' && session()->get('step6')=='on' && session()->get('step7')=='on')
					{
						if (session()->get('plan')=='benefits' || session()->get('plan')=='manager')
						{
							if (session()->get('step8')=='on') 
							{
								$select=DB::table('employerProgress')->where('companyId', session()->get('companyId'))->update(['complete'=> 'on']);
								session()->put('complete', 'on');
							}
						}
						else
						{
							$select=DB::table('employerProgress')->where('companyId', session()->get('companyId'))->update(['complete'=> 'on']);	
							session()->put('complete', 'on');
						}		
					}
				}
			}

			if (session()->get('complete')=='on')
			{
				return redirect('/employerDashboard');
			}
			else
			{
				return view('getStarted');
			}
		}
	}
}
