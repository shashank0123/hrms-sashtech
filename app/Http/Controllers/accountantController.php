<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB, View, Input, StdClass, Validator, Response, File, Storage, Hash, Mail;
use Illuminate\Http\Request;

class accountantController extends Controller 
{	
	public function dashboard()
	{
		if (strtolower(session()->get('type')) == 'accountant') 
		{
			$users		= DB::table('user')
						->where('created_by', session()->get('accountantId'))
						->get();

			$usersCount = 0;
			$company 	= 0;
			$payslip 	= 0;
			$benefits 	= 0;
			$newBenefits= 0;

			if (!empty($users)) 
			{
				$usersCount++;
				foreach ($users as $key => $value) 
				{
					if (!empty($value->companyId)) 
					{
						$selectCompany 	= DB::table('company')
										->where('id', $value->companyId)
										->first();

						if (!empty($selectCompany)) 
						{
							$company++;
						}

						$selectPayslip	= DB::table('payroll')
										->where('companyId', $value->companyId)
										->first();

						if (!empty($selectPayslip))
						{
							$payslip++;
						}

						$selectBenefits 	= DB::table('benefit')
											->where('companyId', $value->companyId)
											->first();

						if(!empty($selectBenefits))
						{
							$benefits++;
						}

						$selectRequirements	= DB::table('benefitSearch')
											->where('companyId', $value->companyId)
											->first();

						if (!empty($selectRequirements)) 
						{
							$newBenefits++;
						}
					}
				}
			}

			$count			 	= new StdClass;
			$count->user 		= $usersCount;
			$count->company 	= $company;
			$count->payslips 	= $payslip;
			$count->benefits 	= $benefits;
			$count->newBenefits	= $newBenefits;

			$profile 	= DB::table('personalDetails2')
						->where('userid', session()->get('accountantId'))
						->first();

			if (empty($profile)) 
			{
				$profile 	= NULL;
			}

			return View::make('accountant/accountantDashboard')
					->with('count', $count)
					->with('profile', $profile);
		}
		else 
		{
			return redirect('login');
		}
	}

	public function accountantDetail()
	{
		if (strtolower(session()->get('type')) != 'accountant') 
		{
			return redirect('login');
		}

		$validator = Validator::make(
				$entries = array(
					'userid'		=> session()->get('accountantId'),
					'type'			=> strtolower(session()->get('type')),
					'firstName' 	=> ucwords(strtolower(Input::get('firstName'))),
					'lastName' 		=> ucwords(strtolower(Input::get('lastName'))),
					'phone' 		=> Input::get('phone'),
					'company' 	=> ucwords(strtolower(Input::get('company'))),
					'email' 		=> strtolower(Input::get('email')),
					'dp'			=> Input::file('profilePic'),
					), 
				array(
					'firstName' 	=> 'required',
					'phone' 		=> 'required|numeric',
					'email' 		=> 'required|email',
					'dp'			=> 'max:500|mimes:jpg,jpeg,png',
					), 
				$messages = array(
					'required' 	=> 'This field is required.',
					'numeric' 	=> 'This must be a number.',
					'email' 	=> 'This is not a valid email.',
					'max'		=> 'The file should be bigger than 500KB.',
					'mimes'		=> 'The file is not in an acceptable format (ie. jpg, jpeg, png).'
					)
			);

		if ($validator->fails()) 
		{
			return Response::json($validator->messages());
		}
		else
		{
			$result = new StdClass;

			if ($entries['email'] != session()->get('email'))
			{
				$result->status 	= 405;
				$result->message 	= 'You are not allowed to change your email. If you still insist, please contact our admin panel.';

				return Response::json($result);
			}

			$entries = array_filter($entries);
			$uploadPath = public_path() . '/HTML/Uploads/Accountant/' . $entries['userid'] . '/';

			//check if the directory exists
			if(!File::exists($uploadPath)) 
			{
				File::makeDirectory($uploadPath , 0775, true);
			}

			// Move file to upload directory
			if (isset($entries['dp'])) 
			{
				$dp = $uploadPath . 'dp-' . $entries['dp']->getClientOriginalName();
				if(File::exists($dp)) 
				{
					unlink($dp);
				}
				$file = $entries['dp']->move($uploadPath, $dp);
				
				$dp = explode('public/', $dp)[1];
				$entries['dp'] = $dp;
			}

			$update = DB::table('user')
					->where('email', $entries['email'])
					->update([
						'firstName' 	=> $entries['firstName'],
						'lastName'	 	=> $entries['lastName'],
						'updated_at'	=> date(('Y-m-d h:i:sa')),
						]);

			$entries1 	= $entries;
			$entries1['firstName']	= NULL;
			$entries1['lastName']	= NULL;
			$entries1['email']		= NULL;

			$entries1 	= array_filter($entries1);

			if (empty($entries1['dp']) or !isset($entries1['dp'])) 
			{
				$entries1['dp'] = NULL;
			}

			$check 	= DB::table('personalDetails2')
					->where('userid', $entries1['userid'])
					->first();

			if (empty($check)) 
			{
			 	$insertDetails 	= DB::table('personalDetails2')
								->insert($entries1);
			} 
			else
			{
				if (!empty($check->dp))
				{
					$prev = public_path() . '/' . $check->dp;

					if(File::exists($prev)) 
					{
						unlink($prev);
					}
				}

				$entries1['updated_at'] = date('Y-m-d h:i:sa');

				$insertDetails 	= DB::table('personalDetails2')
								->where('userid', $entries1['userid'])
								->update($entries1);
			}

			$result->status 	= 200;
			$result->message 	= 'Data updated successfully.';
			// $result->url 		= 'accountantDetail';

			return Response::json($result);
		}
	}

	public function getDetails()
	{
		if (strtolower(session()->get('type')) != 'accountant') 
		{
			return redirect('login');
		}

		$id 	= session()->get('accountantId'); 

		$select1 	= DB::table('user')
					->where('id', $id)
					->first();

		$select2	= DB::table('personalDetails2')
					->where('userid', $id)
					->first();

		return view('accountant/accountantDetail')
				->with('result1', $select1)
				->with('result2', $select2);
	}

	public function createUser() 
	{
		if (strtolower(session()->get('type')) != 'accountant') 
		{
			return redirect('login');
		}

		$validator = Validator::make(
			$entries = array(
				'email' => strtolower(Input::get('email')),
				'type' => Input::get('userType'),
				),
				array(
					'type' => 'required',
					'email' => 'required|email',
			    ),
				$messages = array(
				   'required' => 'The :attribute field is required.',
				   'email' => 'The email is not valid.'
				)
			);

		if ($validator->fails())
	    {
		   return Response::json($validator->messages());
		}			   
		else
		{
			$result = new StdClass;

			if (strtolower($entries['type']) != 'ca')
			{
				$result->message 	= "You are not allowed to create this user type.";
				$result->status 	= 208;

				return Response::json($result);
			}

			$count	= DB::table('user')
					->where('type', 'Admin')
					->orWhere('type', 'Manager')
					->orWhere('type', 'Accountant')
					->orWhere('type', 'CA')
					->orderBy('created_at', 'DESC')
					->first();

			$id 			= $count->id+1;
			$entries['id'] 	= $id;

			$entries['created_by'] = session()->get('accountantId');

			$select = DB::table('user')
					->where('email', $entries['email'])
					->first();
			
			if (empty($select))
			{
				$apiKey = Hash::make($entries['email']);
				$entries['apiKey'] = $apiKey;

				$insert = DB::table('user')
						->insert($entries);

				$select = DB::table('user')
						->where('email', $entries['email'])
						->select('id')
						->first();
				
				$fullName = "user";

				$email 	= Mail::send(
							'emails.newUser',
							array(
								'email'=>$entries['email'],
								'fullname'=>$fullName,
								'apiKey' => $apiKey,
								'adminfrom' => 'rajbabuhome@gmail.com'
							),
							function($message) 
							{
								$message->from('rajbabuhome@gmail.com', 'SashTechs');
								$message->to(Input::get('email'))->subject('Welcome to SashTechs!');
							}
						);

				$result->message 	= "The user has been created.";
				$result->status 	= 200;
				$result->id 		= $select->id;
			}
			else 
			{
				$result->message = "An account with that email address already exists.";
				$result->status = 208;
			}

			return response()->json($result);
		}
	}

	public function viewUser()
	{
		if (strtolower(session()->get('type')) != 'accountant') 
		{
			return redirect('login');
		}

		$users 	= DB::table('user')
				->where('created_by', session()->get('accountantId'))
				->where('type', 'ca')
				->orderBy('created_at', 'desc')
				->get();
			
		return View::make('accountant/accountantCreateUser')
				->with('users', $users);
	}

	public function updateStatus($id)
	{
		$status 	= ucfirst(strtolower(Input::get('status')));

		$update 	= DB::table('user')
					->where('id', $id)
					->update([
						'status' 		=> $status,
						'updated_at'	=> date('Y-m-d h:i:sa'),
						]);

		$result 			= new StdClass;
		$result->status 	= 200;

		return response()->json($result);
	}

	public function viewAssignCompany() 
	{
		if (strtolower(session()->get('type')) != 'accountant') 
		{
			return redirect('login');
		}

		$users 		= DB::table('user')
					->where('type', 'Manager')
					->orWhere('type', 'CA')
					->where('status', 'Active')
					->where('user.created_by', session()->get('accountantId'))
					->get();

		$company 	= DB::table('company')
					// ->where('created_by', session()->get('accountantId'))
					->join('companyAddress', 'company.id', '=', 'companyAddress.companyId')
					->where('company.plan', 'manager')
					->where('company.created_by', session()->get('accountantId'))
					->orderBy('company.id', 'ASC')
					->select('company.*', 'companyAddress.logoUrl')
					->get();

		return View::make('accountant/accountantAssignCompany')
				->with('users', $users)
				->with('company', $company);
	}

	public function assignCompany()
	{
		$id 		= Input::get('manager');
		$company 	= Input::get('companies');

		$check 	= DB::table('user')
				->where('id', $id)
				->first();

		$result 			= new StdClass;
		
		if (!empty($check)) 
		{
			$prev 		= $check->companyId;

			if (strlen($prev) > 1) 
			{
				$prev 		= explode(',', $prev);
				$company1 	= explode(',', $company);

				$diff 		= array_diff($prev, $company1);

				foreach ($diff as $key => $value) 
				{
					$delete = DB::table('company')
							->where('id', $value)
							->update([
								'managerId' 	=> '',
								'updated_at'	=> date('Y-m-d h:i:sa'),
								]);
				}
			}
			elseif((strlen($prev) == 1) and ($prev != $company))
			{
				$delete = DB::table('company')
						->where('id', $prev)
						->update([
							'managerId' 	=> '',
							'updated_at'	=> date('Y-m-d h:i:sa'),
							]);
			}

			$update 	= DB::table('user')
						->where('id', $id)
						->update([
							'companyId' 	=> $company,
							'updated_at'	=> date('Y-m-d h:i:sa'),
							]);

			if (strlen($company) > 1) 
			{
				$companies 	= explode(',', $company);

				foreach ($companies as $key => $value) 
				{
					$updateCompany 	= DB::table('company')
									->where('id', $value)
									->update([
										'managerId' 	=> $id,
										'updated_at'	=> date('Y-m-d h:i:sa'),
										]);
				}
			}
			else
			{
				$updateCompany 	= DB::table('company')
								->where('id', $company)
								->update([
									'managerId' 	=> $id,
									'updated_at'	=> date('Y-m-d h:i:sa'),
									]);
			}

			$result->status 	= 200;
			$result->message 	= 'Company assigned successfully.';

		}
		return response()->json($result);
 	}

	public function viewSelectCompany() 
	{
		if (strtolower(session()->get('type')) != 'accountant') 
		{
			return redirect('login');
		}

		$company 	= DB::table('company')
					->where('created_by', session()->get('accountantId'))
					->orderBy('created_at', 'desc')
					->get();
			
		return View::make('accountant/accountantSelectCompany')
				->with('company', $company);
	}

	public function changeCompany($id)
	{
		if (strtolower(session()->get('type')) != 'accountant') 
		{
			return redirect('login');
		}
		
		$select = DB::table('company')
				->where('id', $id)
				->where('created_by', session()->get('accountantId'))
				->first();

		if (empty($select)) 
		{
			return redirect('accountantSelectCompany');
		}

		session()->put('companyId', $select->id);
		session()->put('companyName', $select->companyName);
		session()->put('companyCode', $select->companyCode);
		session()->put('plan', $select->plan);
		session()->put('type', 'employer');
		session()->put('employerId', session()->get('accountantId'));
		session()->put('employerName', session()->get('accountantName'));
		session()->put('created_by', session()->get('accountantId'));
		session()->put('viewChanged', true);

		return redirect('getStarted');
	}

	public function addCompany()
	{
		if (!empty(Input::get('supportmail')))
		{
			$validator = Validator::make(
				$entries = array(
						'EmployerName' => ucwords(Input::get('EmployerName')),
						'supportmail' => strtolower(Input::get('supportmail')),
						'companyName' => Input::get('companyName'),
					),
					array(
						'EmployerName' => 'required',
						'supportmail' => 'required|email',
						'companyName' => 'required',
					),
				$messages = array(
					   'required' => 'The :attribute field is required.',
					   'email' => 'The email is not valid.'
					)
				);
			
			if ($validator->fails())
			{
				return Response::json($validator->messages());
			}
			else
			{
				$checkEmail 	= $entries['supportmail'];
				$checkEmail 	= explode("@", $checkEmail)[1];
				$emailDomain 	= explode(".", $checkEmail)[0];
				// Not allowed email domains
				$notAllowedDomains = array( 'gmail', 'yahoo', 'rediffmail', 'hotmail', 'outlook', 'ibibo', 'live', 'facebook');

				if (in_array($emailDomain, $notAllowedDomains)) 
				{
					$result = new StdClass;
					$result->message = "That email address is not allowed.";
					$result->status = 406;
					return response()->json($result);
				}

				$word_count 	= str_word_count($entries['companyName']);			
				$getCompanyCode = DB::table('company')
								->select('companyCode')->get();

				if (isset($getCompanyCode)) 
				{
					foreach ($getCompanyCode as $key => $value) 
					{
						$arr[] = $value->companyCode;
					}
				}

				if (!isset($arr)) 
				{
					$arr = array();
				}

				do 
				{
					if (strlen($entries['companyName']) < 4) {
						$word_count = 0;
					}

					if ($word_count == 0) 
					{
						$pool = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
						$length = 4;
						$entries['companyCode'] = strtoupper(substr(str_shuffle(str_repeat($pool, $length)), 0, $length));
					}
					elseif ($word_count < 2) 
					{
						$entries['companyCode'] = strtoupper(substr($entries['companyName'], 0, 4));
					}
					elseif ( $word_count < 4 ) 
					{
						$words = preg_split("/\s+/", $entries['companyName']);
						$entries['companyCode'] = "";
						foreach ($words as $w) 
						{
							$entries['companyCode'] .= $w[0];
							$entries['companyCode'] .= $w[1];
						}
						$entries['companyCode'] = strtoupper(substr($entries['companyCode'], 0, 4));
					}
					else 
					{
						$words = preg_split("/\s+/", $entries['companyName']);
						$entries['companyCode'] = "";
						foreach ($words as $w) 
						{
							$entries['companyCode'] .= $w[0];
						}
						$entries['companyCode'] = strtoupper(substr($entries['companyCode'], 0, 4));
					}

					$word_count = 0;
				} while (in_array($entries['companyCode'], $arr));

				$checkUser 	= DB::table('user')
							->where('email', $entries['supportmail'])
							->first();

				if (!empty($checkUser)) 
				{
					$result = new StdClass;
					$result->message = "The employer with that email address already exists.";
					$result->status = 208;

					return response()->json($result);
				}

				$select = DB::table('company')
						->where('supportmail', $entries['supportmail'])
						->first();

				if (!empty($select)) 
				{
					$result = new StdClass;
					$result->message = "That email address is already registered.";
					$result->status = 208;
					return response()->json($result);
				}

				// $select1 = DB::table('company')
				// 		->where('companyName', $entries['companyName'])
				// 		->first();

				// if (!empty($select1)) 
				// {
				// 	$result = new StdClass;
				// 	$result->message = "That company name is already taken.";
				// 	$result->status = 209;
				// 	return response()->json($result);
				// }

				$entries['created_by'] = session()->get('accountantId');

				$result = DB::table('company')
						->insert($entries);

				$id = DB::getPdo()->lastInsertId();
				
				$entries1 = array(
					'id' 		=> strtoupper(substr($entries['companyCode'], 0, 4)).'1',
					'firstName' => ucfirst(strtolower(explode(' ', Input::get('EmployerName'))[0])),
					'email' 	=> strtolower(Input::get('supportmail')),
					'companyId' => $id,
					'status' 	=> 'Inactive',
					'type'		=> 'Employer',
					'created_by'=> session()->get('accountantId'),
				);

				$apiKey 			= Hash::make($entries1['email']);
				$entries1['apiKey'] = $apiKey;

				$result = DB::table('user')
						->insert($entries1);    
				
				$id1 	= DB::getPdo()
						->lastInsertId();

				// for email
				$select1 = DB::table('user')->where('email', $entries1['email'])->first();
				
				$email = Mail::send(
						'emails.newUser',
						array(
							'email'		=> $entries1['email'], 
							'fullname'	=> $entries1['firstName'], 
							'apiKey'	=> $apiKey, 
							'adminfrom' => 'rajbabuhome@gmail.com'
						), function($message) 
							{
								$message->from('rajbabuhome@gmail.com', 'SashTechs');
								$message->to(Input::get('supportmail'))->subject('Welcome to SashTechs!');
							}
						);

				if ($email == 1) 
				{
					$result = new StdClass;
					// $result->message = "Registration successfull. Please verify your email.";
					$result->status = 200;
				}
				else
				{
					$deleteUser		= DB::table('user')
									->where('id', $id1)
									->delete();

					$deleteCompany 	= DB::table('company')
									->where('id', $id)
									->delete();

					$result 	= new StdClass;

					return response()->json($result);
				}

				$next 	= Db::table('employerProgress')
						->insert([
							'companyId' => $id, 
							'nextStep' => 'step1'
							]);

				return response()->json($result);
			}
		}
	}
}
