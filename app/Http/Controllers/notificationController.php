<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input, Validator, DB, StdClass;
use Illuminate\Http\Request;

class notificationController extends Controller {
	public function addNotification()
		{
			$validator = Validator::make(
			$entries = array(
					'companyId' => session()->get('companyId'),
					'type'=> Input::get('type'),
					'messageTitle' => Input::get('messageTitle'),
					'messageContent' => Input::get('messageContent'),
				),
				array(
					'companyId' => 'required',
					'type' => 'required',
					'messageTitle' => 'required',
			    ),
			$messages = array(
				   'required' => 'The :attribute field is required.',
				)
		);		
		if ($validator->fails())
        {
        	return response()->json($validator->messages());
		}			   
		else
		{	
			$select=DB::table('notification')->insert($entries);
			$result=new StdClass;
			$result->status=200;
			$result->response=$select;
			$result->url="/getStarted";

			return response()->json($result);
		}	
	}
}