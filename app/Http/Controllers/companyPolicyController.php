<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator, DB, Input , File, View, StdClass;
use Illuminate\Http\Request;

class companyPolicyController extends Controller {

	public function saveCompanyPolicy()
	{
		$skip = Input::get('skip');
		if ($skip)
		{
			$progress 	= DB::table('employerProgress')
						->where('companyId', session()->get('companyId'))
						->update([
							'step7' 	=> 'on',
							'step8' 	=> 'pending',
							'nextStep' 	=> 'step8',
							]);

			$result 		= new StdClass;
			$result->status = 200;

			if((strtolower(session()->get('plan')) == 'benefits') or (strtolower(session()->get('plan')) == 'manager'))
			{
				$url="/addBenefit_step0";
			}
			else
			{
				$url="/employerDashboard";
			}

			$result->url 	= $url;
			
			return response()->json($result);
		}

		$validator = Validator::make(
			$entries = array(
					'companyId' 			=> session()->get('companyId'),
					'policyName'			=> ucwords(strtolower(Input::get('policyName'))),
					'description' 			=> ucfirst(strtolower(Input::get('description'))),
					'proof' 				=> Input::file('proof'),
					'completionDate' 		=> Input::get('completionDate'),
					'requireEmployeeSign' 	=> strtolower(Input::get('requireEmployeeSign')),
				),
				array(
					'policyName' 	=> 'required',
					'description' 	=> 'required',
					'proof' 		=> 'required|max:1000|mimes:pdf',
					'completionDate'=> 'required',
			    ),
			$messages = array(
				   'required' 	=> 'This field is required.',
				   'mimes' 		=> 'The file is not in acceptable format i.e. .pdf',
				   'max' 		=> 'The file is bigger than 1000KB.'
				)
		);
		if ($validator->fails())
        {
        	return response()->json($validator->messages());
		}			   
		else
		{	
			$uploadPath = public_path() . '/HTML/Uploads/Company/' . $entries['companyId'] . '/';

			if(!File::exists($uploadPath)) 
			{
				$select=File::makeDirectory($uploadPath, 0775, true);
			}

            if (isset($entries['proof'])) 
            {
                $docUrl = $uploadPath . $entries['proof']->getClientOriginalName();
                if(File::exists($docUrl)) 
                {
                    unlink($docUrl);
                }
                $file = $entries['proof']->move($uploadPath, $docUrl);

	            $docUrl=explode('public/', $docUrl)[1];
	            $entries['proof'] = $docUrl;
            }
		} 
	

		$select	= DB::table('companyPolicy')
				->where('companyId', session()->get('companyId'))
				->first();

		$result	= DB::table('companyPolicy')
				->insert($entries);

		$select	= DB::table('employerProgress')
				->where('companyId', session()->get('companyId'))
				->update([
					'step7' 	=> 'on',
					'step8' 	=> 'pending',
					'nextStep' 	=> 'step8',
					]);

		if((strtolower(session()->get('plan')) == 'benefits') or (strtolower(session()->get('plan')) == 'manager'))
		{
			$url="/addBenefit_step0";
		}
		else
		{
			$url="/employerDashboard";
		}


	   	return response()->json([
	   			'result' 	=> $result, 
	   			'status' 	=> 200, 
	   			'url' 		=> $url, 
	   			'message'	=>'Data inserted successfully'
	   			]);
	}

	public function editPolicy($id)
	{
		$select = DB::table('companyPolicy')
		->where('id', $id)
		->first();
		return response()->json($select);
	}

	public function deletePolicy($id)
	{
		$delete = DB::table('companyPolicy')
				->where('id', $id)
				->delete();

		$result = new StdClass;
		if ($delete) 
		{
			$result->status = 200;
		}
		return response()->json($result);
	}

	public function getPolicy()
	{
		if (strtolower(session()->get('type')) != 'employer') 
		{
			return redirect('login');
		}

		// if (strtolower(session()->get('complete')) == 'on' ) 
		// {
		// 	return redirect('employerDashboard');
		// }
	
		$result = DB::table('companyPolicy')
				->where('companyId', session()->get('companyId'))
				->get();

		return View::make('companyPolicy')
				->with('result', $result);
	}

	public function viewEmployeePolicy()
	{
		if (session()->get('employeeId')) 
		{
			$result=DB::table('companyPolicy')->where('companyId', session()->get('companyId'))->get();
			return View::make('employee/employeeViewCompanyPolicy')->with('result', $result);
		}
		
		else 
		{
			return redirect('login');
		}	
	}
}
