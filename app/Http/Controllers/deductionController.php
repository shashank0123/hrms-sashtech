<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB,View;
use Illuminate\Http\Request;

class deductionController extends Controller {

	public function getInvestment()
	{
		if (session()->get('employeeId')) {
			$select=DB::table('investmentDeduction')->where('userId', session()->get('employeeId'))->first();
			return View::make('employee/investmentDeduction')->with('result', $select);		
		}
		
		else {
			return redirect('login');
		}	
	}

	public function getMedical()
	{
		if (session()->get('employeeId')) 
		{
			$select = DB::table('medicalDeduction')
					->where('userId', session()->get('employeeId'))
					->first();

			return View::make('employee/medicalDeduction')
					->with('result', $select);		
		}
		else 
		{
			return redirect('login');
		}	
	}
	public function getOther()
	{
		if (session()->get('employeeId')) {
			$select=DB::table('otherDeduction')->where('userId', session()->get('employeeId'))->first();
			return View::make('employee/otherDeduction')->with('result', $select);		
		}
		
		else {
			return redirect('login');
		}	
	}
	public function getHraMonthly()
		{
			if (session()->get('employeeId')) {
				$select=DB::table('HRAmonthlyDeduction')->where('userId', session()->get('employeeId'))->first();
				return View::make('employee/hraMonthlyDetails')->with('result', $select);		
			}
			
			else {
				return redirect('login');
			}	
		}
	public function getSelfOccupiedController()
		{
			if (session()->get('employeeId')) {
				$select = DB::table('selfOccupiedDeduction')
						->where('userId', session()->get('employeeId'))
						->first();

				return View::make('employee/interestOnSelfOccupiedProperty')
						->with('result', $select);
			}
			
			else {
				return redirect('login');
			}	
		}
	public function getIncomeFromPreEmployer()
		{
			if (session()->get('employeeId')) {
				$select=DB::table('previousEmployerDeduction')->where('userId', session()->get('employeeId'))->first();
				return View::make('employee/incomeFromPreviousEmployer')->with('result', $select);		
			}
			
			else {
				return redirect('login');
			}	
		}


}
