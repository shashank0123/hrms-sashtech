<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class contractorController extends Controller {

	public function insertPersonalData()
	{
		$validator = Validator::make(
			$entries = array(
					'userid' => Session::get('contractorId'),
					'pan' => Input::get('pan'),
					'DateOfBirth' => Input::get('DateOfBirth'),
					'permanentAddress' => ucfirst(strtolower(Input::get('permanentAddress'))),
					// 'created_at' => date("d-m-y h:i:sa")
				),
			array(
					'pan' => 'required|alpha_num|size:10|regex:/([a-zA-Z]){5}([0-9]){4}[a-zdA-Z]/',
					'DateOfBirth' => 'required',
					'permanentAddress' => 'required',
		    	),
			$messages = array(
				   'required' => 'The :attribute field is required.'
				)
			);

		if ($validator->fails()) 
		{
			return Response::json($validator->messages());
		}
		else
		{
			$select = DB::table('personalDetails')->where('userid', Session::get('contractorId'))->first();
			if (empty($select)){
				$result = DB::table('personalDetails')->insert($entries);
			}
			else {
				$result=DB::table('personalDetails')->where('userid', Session::get('contractorId'))->update($entries);
			}
			$url="/addContractor_step3";
	   		return response()->json(['result' => $result, 'status' => 200, 'url' => $url, 'message'=>'Data inserted successfully']);
		}
	}

	public function insertTaxData()
	{
		$validator = Validator::make(
				$entries = array(
						'userid' 		=> Session::get('contractorId'),
						'taxApplicable' => ucfirst(Input::get('taxApplicable')),
						'serviceTaxApplicable'=>Input::get('serviceTaxApplicable'),
						'PFAcNo' 		=> Input::get('PFAcNo'),
						'esiNumber' 	=> strtoupper(Input::get('esiNumber')),
						'UAN' 			=> strtoupper(Input::get('uan')),
						'contractorNature' 		=> Input::get('nature'),
					    
					),
					array(
						'PFAcNo' => 'required',
						'esiNumber' => 'required|digits:10|numeric',
				    ),
				$messages = array(
					   'required' => 'The :attribute field is required.',
					)
				);
			if ($validator->fails())
            {
			   return Response::json($validator->messages());
			}			   
			else
			{		
				$result = DB::table('personalDetails')
						->where('userid', Session::get('contractorId'))
						->update($entries);

				if ($result)
				{
					$url="/addContractor_step4";
			   		return response()->json([
			   				'result' 	=> $result, 
			   				'status' 	=> 200, 
			   				'url' 		=> $url, 
			   				'message'	=> 'Data inserted successfully'
			   				]);
				}
				else 
				{	
		   			return response()->json([
		   					'result' => $result, 
		   					'status' => 400,  
		   					'message'=>'Data can not inserted'
		   					]);
				}
			}		
	}

	public function insertPaymentData()
	{			
		$validator = Validator::make(
				$entries = array(
						'userid' => Session::get('contractorId'),
						'modeOfPayment' => ucwords(strtolower(Input::get('modeOfPayment'))),
						'bankName' => ucwords(strtolower(Input::get('bankName'))),
						'branch' => ucwords(strtolower(Input::get('branch'))),
						'accountHolder' => ucwords(strtolower(Input::get('accountHolder'))),
						'accountNumber' => Input::get('accountNumber'),
						'accountType' => ucwords(strtolower(Input::get('accountType'))),
						'ifsc' => strtoupper(Input::get('ifsc'))
					    
					),
					array(
						'modeOfPayment' => 'required',
						'bankName' => 'required',
						'branch' => 'required',
						'accountHolder' => 'required',
						'accountNumber' => 'required|numeric|regex:/([0-9]){10,}\d/',
						'accountType' => 'required',
						'ifsc' => 'required|alpha_num|size:11|regex:/([A-Za-z]){4}[0][0-9A-Za-z]{6}/'
				    	),
				$messages = array(
						   'required' => 'The :attribute field is required.',
						   'numeric' => 'The :attribute field must be a number'
						)

				);
		if ($validator->fails())
        {
		   return Response::json($validator->messages());
		}			   
		else
		{
			$select=DB::table('BankDetails')->where('userid', Session::get('contractorId'))->get();
			if ($select){
				$result=DB::table('BankDetails')->where('userid', Session::get('contractorId'))->update($entries);
			}		
			else
				$result=DB::table('BankDetails')->insert($entries);


			$url="/manageContractors";

	   		return response()->json(['result' => $result, 'status' => 200, 'url' => $url, 'message'=>'Success']);	
	   	}
	}

}
