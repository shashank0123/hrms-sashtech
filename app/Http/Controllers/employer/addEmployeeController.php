<?php namespace App\Http\Controllers\employer;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator, Input, Redirect, DB, Response, Session, View, Hash, Mail, StdClass;
use Illuminate\Http\Request;


class addEmployeeController extends Controller 
{
	public function addEmployee()
	{
		$validator = Validator::make(
			$entries = array(
				'firstName' => ucwords(strtolower(Input::get('firstName'))),
				'lastName' => ucwords(strtolower(Input::get('lastName'))),
				'title' => ucwords(strtolower(Input::get('title'))),
				'localAddress' => ucwords(Input::get('localAddress')),
				'team' => ucwords(strtolower(Input::get('team'))),
				'email' => strtolower(Input::get('email')),
				'companyId' => Session::get('companyId'),
				'type' => 'Employee',
				'joinDate' => Input::get('joinDate'),
				'ctc' => Input::get('ctc'),
				'employeeSelfOnBoarding' => ucfirst(strtolower(Input::get('employeeSelfOnBoarding'))),
				// 'created_at' => date("d-m-y h:i:sa")
			),
			array(
				'firstName' => 'required|regex:/([A-Za-z])/',
				'lastName' => 'required|regex:/([A-Za-z])/',
				'title' => 'required',
				'companyId' => 'required',
				'type' => 'required',
				// 'team' => 'required',
				'localAddress' => 'required',
				'email' => 'required|email',
				'joinDate' => 'required|date',
				'ctc' => 'required'
		    ),
			$messages = array(
			   'required' => 'The :attribute field is required.',
			   'date' => 'The date is not valid.',
			   'email' => 'The email is not valid.',
			   'regex' => 'This is not in acceptable format.'
			)
		);
		if ($validator->fails())
        {
		   return Response::json($validator->messages());
		}			   
		else
		{
			$personalDetail['localAddress'] = $entries['localAddress'];
			$entries['localAddress'] = null;
			$entries = array_filter($entries);

			$validator1 = Validator::make(
				$entries1 = array(
					'companyId' => session()->get('companyId'),
					'jobTitle' => Input::get('title'),
					'ctc' => Input::get('ctc'),
					'basic' => Input::get('basic'),
					'hra' => Input::get('hra'),
					'conveyance' => Input::get('conveyance'),
					'medicalAllowance' => Input::get('medicalAllowance'),
					'medicalInsurance' => Input::get('medicalInsurance'),
					'telephone' => Input::get('telephone'),
					'leaveTravel' => Input::get('leaveTravel'),
					'uniform' => Input::get('uniform'),
					'gratuity' => Input::get('gratuity'),
					'superAnnuation' => Input::get('superAnnuation'),
					'annualBonus' => Input::get('annualBonus'),
					'festivalBonus' => Input::get('festivalBonus'),
					'incentives' => Input::get('incentives'),
					'others' => Input::get('others'),
					'leaveEncashment' => Input::get('leaveEncashment'),
					'pfContribution' => Input::get('pfContribution'),
					'esiContribution' => Input::get('esiContribution'),
					// 'created_at' => date("d-m-y h:i:sa"),
				),
				array(
					'basic' => 'required|numeric',
					'hra' => 'numeric',
					'conveyance' => 'numeric',
					'medicalAllowance' => 'numeric',
					'medicalInsurance' => 'numeric',
					'telephone' => 'numeric',
					'leaveTravel' => 'numeric',
					'uniform' => 'numeric',
					'gratuity' => 'numeric',
					'superAnnuation' => 'numeric',
					'annualBonus' => 'numeric',
					'festivalBonus' => 'numeric',
					'incentives' => 'numeric',
					'others' => 'numeric',
					'leaveEncashment' => 'numeric',
					'pfContribution' => 'numeric',
					'esiContribution' => 'numeric',
			    ),
				$messages = array(
				   'required' => 'The :attribute field is required.',
				   'numeric' => 'The :attribute field must be a number.'
				)
			);

			if ($validator1->fails())
	        {
			   return response()->json( $validator->messages());
			}			   
			else
			{	
				$select = DB::table('user')
						->where('email', $entries['email'])
						->first();

				if (!empty($select)) 
				{
					$result = new StdClass;
					$result->message = "The employee with that email address already exists.";
					$result->status = 208;
					
					return response()->json($result);
				}

				if (Input::get('ctc')=="other")
				{
					$entries['ctc'] 	= Input::get('CTC');
					$entries1['ctc']	= Input::get('CTC');
				}

				$template 	= DB::table('ctc')
							->where('companyId', session()->get('companyId'))
							->where('jobTitle', Input::get('title'))
							->where('ctc', $entries1['ctc'])
							->first();

				if (empty($template))
				{
					$result=DB::table('ctc')->insert($entries1);					
				}

				$sum = Input::get('medicalInsurance') + Input::get('telephone') + Input::get('leaveTravel') + Input::get('uniform') + Input::get('gratuity') + Input::get('superAnnuation') + Input::get('annualBonus') + Input::get('festivalBonus') + Input::get('incentives') + Input::get('others') + Input::get('leaveEncashment') + Input::get('pfContribution') + Input::get('esiContribution');

				$basic = Input::get('basic') + Input::get('hra') + Input::get('conveyance') + Input::get('medicalAllowance');

				$monthlyPay = ($basic)/12;
				$oneDayPay 	= ($basic)/365;

				$entries1['monthlyPay'] = $monthlyPay;
				$entries1['oneDayPay'] 	= $oneDayPay;
				$entries1['sum'] 		= $sum;
				
				if (Input::get('ctc')!='other')
				{
					$entries1['ctc'] = Input::get('ctc');
				}
					

				$validator2 = Validator::make(
					$entries2 = array(
						'team' => ucwords(strtolower(Input::get('team'))),
						'companyId' => Session::get('companyId'),
						'companyCode' => strtoupper(Session::get('companyCode')),
						// 'created_at' => date("d-m-y h:i:sa")
					),
					array(
						'companyCode' => 'required',
						// 'team' => 'required',
				    ),
					$messages = array(
					   'required' => 'The :attribute field is required.',
					)
				);

				if ($validator2->fails())
		        {
				   return Response::json($validator2->messages());
				}			   
				else
				{	
					$select = DB::table('user')
							->where('email', $entries['email'])
							->first();

					$company = DB::table('company')
							->where('id', session()->get('companyId'))
							->select('companyCode')
							->first();
					
					$user 	= DB::table('user')
							->where('companyId', $entries['companyId'])
							->orderBy('created_at', 'desc')
							->select('id')
							->first();

					$IdNo = intval(str_replace($company->companyCode, '', $user->id));
					$IdNo++;
					$userid = ($company->companyCode . $IdNo);
					$entries['id'] = $userid;
					$entries1['userId'] = $userid;
					$personalDetail['userid'] = $userid;

					if (empty($select))
					{
						$apiKey = Hash::make($entries['email']);
						$entries['apiKey'] = $apiKey;

						$result = DB::table('user')
								->insert($entries);

						$title 	= DB::table('titles')
								->where('companyId', session()->get('companyId'))
								->where('titles', $entries['title'])
								->first();

						$titleEntry['companyId'] 	= session()->get('companyId');
						$titleEntry['titles'] 		= $entries['title'];

						if (empty($title)) 
						{
							$titleEntry['members'] 	= 1;

							$title 	= DB::table('titles')
									->insert($titleEntry);
						}
						else
						{
							$titleEntry['members'] 	= ($title->members + 1);

							$title 	= DB::table('titles')
									->where('companyId', $titleEntry['companyId'])
									->where('titles', $titleEntry['titles'])
									->update([
										'members' 	=> $titleEntry['members']
										]);
						}

						$team = DB::table('team')
								->where('companyId', session()->get('companyId'))
								->where('team' , ucwords(strtolower(Input::get('team'))))
								->first();

						if (empty($team))
						{
							$team = DB::table('team')
									->insert($entries2);
						}
						else
						{
							$team=DB::table('team')
								->where('companyId', session()->get('companyId'))
								->update(['members' => intval($team->members)+1]);
						}

						$select2 = DB::table('personalDetails')
								->where('userid', $personalDetail['userid'])
								->first();

						if (empty($select2)) {
							$insert1 = DB::table('personalDetails')
									->insert($personalDetail);
						}

						$progress 	= DB::table('employerProgress')
									->where('companyId', session()->get('companyId'))
									->first();

						if (isset($progress->step2) && $progress->step2!='On')
							$progress 	= DB::table('employerProgress')
										->where('companyId', session()->get('companyId'))
										->update(['step2' => 'on', 'step3' => 'on', 'nextStep' => 'step4']);

						$select1 = DB::table('user')->where('email', $entries['email'])->first();
						$new = DB::table('employeeCtc')->where('userid', $select1->id)->first();
						if (empty($new)){
							$result=DB::table('employeeCtc')->insert($entries1);						
						}
						$fullName = $entries['firstName'] . ' ' . $entries['lastName'];
						
						$email 	= Mail::send(
							'emails.newUser',
							array(
								'email'		=> $entries['email'],
								'fullname'	=> $fullName, 
								'apiKey' 	=> $apiKey, 
								'adminfrom' => 'rajbabuhome@gmail.com'), 
								function($message) {
									$message->from('rajbabuhome@gmail.com', 'Sashtechs');
									$message->to(Input::get('email'))->subject('Welcome to Sashtechs!');
								}
							);

						$result = new StdClass;
						$result->message = "The employee has been created.";
						$result->status = 200;
						Session::put('employeeId', $select1->id);

						if ((isset($entries['employeeSelfOnBoarding']))&&($entries['employeeSelfOnBoarding']=="On")) 
						{
							if (session()->get('complete') != 'on') 
							{
								$url="/salarySetup";
							}
							else
							{
								$url="/manageEmployees";
							}

							$result->url = $url;
						}
						else
						{
							$result->url="/addEmployees_step2";
						}

						$userProgress['userid'] 	= session()->get('employeeId');
						$userProgress['nextStep']	= 'step1';
						$userProgress['complete']	= 'off';

						$check 	= DB::table('userProgress')
								->where('userid', $userProgress['userid'])
								->first();

						if (empty($check)) 
						{
							$insertProgress = DB::table('userProgress')
										->insert($userProgress);
						}
						else
						{
							$updateProgress = DB::table('userProgress')
											->where('userid', $userProgress['userid'])
											->update($userProgress);
						}
					}
					else 
					{
						$result = new StdClass;
						$result->message = "The employee with that email address already exists.";
						$result->status = 208;
					}
					return response()->json($result);
				}
			}
		}
	}
	
	public function getCTC($title)
	{
		$select = DB::table('ctc')
				->where('companyId', session()->get('companyId'))
				->where('jobTitle', $title)
				->get();
				
		return response()->json($select);
	}

	public function insertPersonalData()
	{
		$validator = Validator::make(
				$entries = array(
						'userid' 			=> Session::get('employeeId'),
						'pan' 				=> strtoupper(Input::get('pan')),
						'phone'				=> Input::get('phone'),
						'dateOfBirth'		=> Input::get('dateOfBirth'),
						'permanentAddress' 	=> ucwords(Input::get('permanentAddress')),
						'altEmail' 			=> strtolower(Input::get('employeOthereMail')),	    
						'phone' 			=> strtolower(Input::get('phone')),					    
					),
					array(
						'pan' 				=> 'required|alpha_num|size:10|regex:/([a-zA-Z]){5}([0-9]){4}[a-zdA-Z]/',
						'permanentAddress' 	=> 'required',
						'altEmail' 			=> 'email',
				    ),
				$messages = array(
					   'required' => 'The :attribute field is required.',
					   'email' => 'Enter a valid email address',
					   'regex' => 'Enter a valid pan no..'
					)
				);

		$date = date('Y-m-d');
	    $date1 = date_create($date);

	    $date2 = date_create(Input::get('dateOfBirth'));

	    $diff = date_diff($date1, $date2);
	    $age = $diff->y;

		if ($validator->fails())
        {
        	return response()->json($validator->messages());
		}		
		elseif ($age < 16) {
			return Response::json(['message' => 'The age of the employee can not be less than 16 years.']);
		}	   
		else
		{		
			$select = DB::table('personalDetails')
					->where('userid', Session::get('employeeId'))
					->first();

			if (empty($select))
			{
				$result 	= DB::table('personalDetails')
							->insert($entries);

				$result1 	= DB::table('user')
							->where('id', Session::get('employeeId'))
							->update([
								'gender' => Input::get('gender')
							]);				
			}
			else 
			{
				$result 	= DB::table('personalDetails')
							->where('userid', Session::get('employeeId'))
							->update($entries);

				$result1	= DB::table('user')
							->where('id', Session::get('employeeId'))
							->update([
								'gender' => Input::get('gender')
							]);
					
			}

			if(strtolower(session()->get('plan')) == 'benefits')
			{
				$next 	= 'step2-2';
				$url 	= "/addEmployees_step2-2";
			}
			else
			{
				$next 	= 'step3';
				$url 	= "/addEmployees_step3";
			}

			$progress['userid'] 	= session()->get('employeeId');
			$progress['nextStep']	= $next;
			$progress['complete']	= 'off';
			$progress['updated_at']	= date('Y-m-d h:i:sa');

			$updateProgress = DB::table('userProgress')
							->where('userid', $progress['userid'])
							->update($progress);

	   		return response()->json([
		   				'result' => $result, 
		   				'status' => 200, 
		   				'url' => $url, 
		   				'message'=>'Data inserted successfully'
	   				]);
		}		
	}

	public function getuserinfo($email)
	{
		$select = DB::table('personalDetails')
		 	->where('userid', Session::get('id'))
	     	->get();
		$result = DB::table('personalDetails')
			->where('userid', Session::get('employeeId'))
			->first();
		$result= json_encode($result);
		return $result;
	}

	public function getPersonalData()
	{
		$email=Session::get('email');
		$result= $this->getuserinfo($email);
		return View::make('addEmployees_step2')->with('result' , $result);
	}

	public function insertTaxData()
	{
		$validator = Validator::make(
			$entries = array(
					'userid' 		=> Session::get('employeeId'),
					'taxApplicable' => ucfirst(Input::get('taxApplicable')),
					'PFAcNo' 		=> Input::get('PFAcNo'),
					'esiNumber' 	=> strtoupper(Input::get('esiNumber')),
					'uan' 			=> strtoupper(Input::get('uan')),
				    
				),
				array(
					'PFAcNo' 	=> 'size:23|regex:/([A-Za-z]{5})[0-9]{18}/',
					'esiNumber' => 'digits:10|numeric',
					'uan' 		=> 'digits:12|numeric',
			    ),
			$messages = array(
				   'required' => 'The :attribute field is required.',
				)
			);
		if ($validator->fails())
        {
		   return Response::json($validator->messages());
		}			   
		else
		{		
			$result = DB::table('personalDetails')
					->where('userid', Session::get('employeeId'))
					->update($entries);

			if ($result)
			{
				$progress['userid'] 	= session()->get('employeeId');
				$progress['nextStep']	= 'step3';
				$progress['complete']	= 'off';
				$progress['updated_at']	= date('Y-m-d h:i:sa');

				$updateProgress = DB::table('userProgress')
								->where('userid', $progress['userid'])
								->update($progress);

				$url="/addEmployees_step4";
		   		return response()->json([
		   				'result' 	=> $result, 
		   				'status' 	=> 200, 
		   				'url' 		=> $url, 
		   				'message'	=> 'Data inserted successfully.'
		   				]);
			}
			else 
			{	
	   			return response()->json([
	   					'result' => $result, 
	   					'status' => 400,  
	   					'message'=>'Data was not inserted.'
	   					]);
			}
		}		
	}

	public function getTaxData()
	{
		$email=Session::get('email');
		$result= getuserinfo($email);
		return View::make('addEmployees_step3')->with($result);
	}

	public function getUserTaxInfo($email)
	{
		$select = DB::table('personalDetails')->where('userid', Session::get('employeeId'))->first();
		return $select;
	}

	public function insertBankData()
	{
		if (Input::get('modeOfPayment1')=='cheque') 
		{
			$check 	= DB::table('BankDetails')
					->where('userId', session()->get('employeeId'))
					->first();

			if (!empty($check)) 
			{
				$delete = DB::table('BankDetails')
						->where('userId', session()->get('employeeId'))
						->delete();
			}

			$checkPaytm = DB::table('paytmId')
						->where('userId', Session::get('employeeId'))
						->first();

			if (!empty($checkPaytm)) 
			{
				$delete = DB::table('paytmId')
						->where('userId', session()->get('employeeId'))
						->delete();
			}

			$checkPrev 	= DB::table('BankDetails')
						->where('userId', session()->get('employeeId'))
						->where('modeOfPayment', 'cheque')
						->first();

			$entries['userId'] 			= session()->get('employeeId');
			$entries['modeOfPayment']	= 'cheque';

			if (empty($checkPrev)) 
			{
				$insert = DB::table('BankDetails')
						->insert($entries);
			}
			else
			{
				$entries['updated_at'] = date('Y-m-d h:i:sa');

				$update = DB::table('BankDetails')
						->where('userId', $entries['userId'])
						->update($entries);
			}

			$progress['userid'] 	= session()->get('employeeId');
			$progress['nextStep']	= 'done';
			$progress['complete']	= 'on';
			$progress['updated_at']	= date('Y-m-d h:i:sa');

			$updateProgress = DB::table('userProgress')
							->where('userid', $progress['userid'])
							->update($progress);

			if (session()->get('complete') != 'on') 
			{
				$url="/salarySetup";
			}
			else
			{
				$url="/manageEmployees";
			}

			return response()->json([
	   				'status' 	=> 200, 
	   				'url' 		=> $url,
	   				'url1' 		=> '/addEmployees_step1',
	   				'message'	=> 'Success'
	   				]);	
		}

		if (Input::get('modeOfPayment1')=='directDeposit')
		{
			$validator = Validator::make(
				$entries = array(
						'userid' => Session::get('employeeId'),
						'modeOfPayment' => Input::get('modeOfPayment1'),
						'bankName' => Input::get('bankName1'),
						'branch' => strtoupper(Input::get('branch1')),
						'accountHolder' => strtoupper(Input::get('accountHolder1')),
						'accountNumber' => Input::get('accountNumber1'),
						'accountType' => strtoupper(Input::get('accountType1')),
						'ifsc' => strtoupper(Input::get('ifsc1')),
						'sharePercent' => strtoupper(Input::get('sharePercent1')),
					    
					),
					array(
						'modeOfPayment' => 'required',
						'bankName' => 'required',
						'branch' => 'required',
						'accountHolder' => 'required',
						'accountNumber' => 'required|numeric|regex:/([0-9]){10,}\d/',
						'accountType' => 'required',
						'ifsc' => 'required|alpha_num|size:11|regex:/([A-Za-z]){4}[0][0-9A-Za-z]{6}/',
						'sharePercent' => 'required',
				    	),
				$messages = array(
						   'required' => 'The :attribute field is required.',
						   'numeric' => 'The :attribute field must be a number'
						)

				);
		}
		
		$form2 = Input::get('bankName2');
		if (isset($form2))
		{
			$validator1 = Validator::make(
				$entries1 = array(
						'userid' => Session::get('employeeId'),
						'modeOfPayment' => "Direct Deposit",
						'bankName' => Input::get('bankName2'),
						'branch' => strtoupper(Input::get('branch2')),
						'accountHolder' => strtoupper(Input::get('accountHolder2')),
						'accountNumber' => Input::get('accountNumber2'),
						'accountType' => strtoupper(Input::get('accountType2')),
						'ifsc' => strtoupper(Input::get('ifsc2')),
						'sharePercent' => strtoupper(Input::get('sharePercent2')),
					    
					),
					array(
						'userid' => 'required',
						'bankName' => 'required',
						'branch' => 'required',
						'accountHolder' => 'required',
						'accountNumber' => 'required|numeric|regex:/([0-9]){10,}\d/',
						'accountType' => 'required',
						'ifsc' => 'required|alpha_num|size:11|regex:/([A-Za-z]){4}[0][0-9A-Za-z]{6}/',
						'sharePercent' => 'required',
				    	),
				$messages = array(
					   'required' => 'The :attribute field is required.',
					   'numeric' => 'The :attribute field must be a number'
					)

				);
		}

		$form3 = Input::get('paytmId');
		if (isset($form3))
		{
			$validator2 = Validator::make(
				$entries2 = array(
						'userid' 			=> Session::get('employeeId'),
						'companyId' 		=> Session::get('companyId'),
						'paytmId' 			=> Input::get('paytmId'),
						'paytmSharePercent' => strtoupper(Input::get('paytmSharePercent')),
					    
					),
					array(
						'userid' 			=> 'required',
						'paytmId' 			=> 'required',
						'paytmSharePercent' => 'required',
				    	),
				$messages = array(
						   'required' 	=> 'The :attribute field is required.',
						   'numeric' 	=> 'The :attribute field must be a number'
						)
				);
		}

		if ( (isset($validator)and($validator->fails()))||(isset($validator1)and($validator1->fails()))||(isset($validator2)and($validator2->fails())))
        {
        	$error = array();
        	
        	if (isset($validator))
        	{
        		if ($validator->fails()) 
        		{
	        		$error[0]	=	$validator->messages();
        		}
        		else
        		{
        			$error[0]	=	 new StdClass;
        		} 
        	}
        	if (isset($validator1)) 
        	{
        		if ($validator1->fails()) 
        		{
	        		$error[1]	=	$validator1->messages();
        		}
        		else
        		{
        			$error[1]	=	 new StdClass;
        		} 
        	}
        	if (isset($validator2)) 
        	{
        		if ($validator2->fails()) 
        		{
	        		$error[2]	=	$validator2->messages();
        		}
        		else
        		{
        			$error[2]	=	 new StdClass;
        		} 
        	}

		    return $error;
		}
		else
		{
			$check 	= DB::table('BankDetails')
					->where('userId', Session::get('employeeId'))
					->first();

			if (!empty($check)) 
			{
				$delete = DB::table('BankDetails')
						->where('userId', Session::get('employeeId'))
						->delete();				
			}

			$checkPaytm = DB::table('paytmId')
						->where('userId', Session::get('employeeId'))
						->first();

			if (!empty($checkPaytm)) 
			{
				$deletePaytm 	= DB::table('paytmId')
								->where('userId', Session::get('employeeId'))
								->delete();
			}

			if (isset($entries))
			{
				$select=DB::table('BankDetails')
						->where('userid', Session::get('employeeId'))
						->where('modeOfPayment', "directDeposit")
						->first();

				if (empty($select))
				{
					$result = DB::table('BankDetails')
							->insert($entries);										
				}
				else
				{	
					$entries['updated_at'] = date('Y-m-d h:i:sa');
					$result = DB::table('BankDetails')
							->where('userid', Session::get('employeeId'))
							->where('modeOfPayment', "directDeposit")
							->update($entries);
				}
			}
			if (isset($entries1))
			{
				$select1=DB::table('BankDetails')
						->where('userid', Session::get('employeeId'))
						->where('modeOfPayment', "Direct Deposit")
						->first();
				
				if (empty($select1))
				{
					$result1 = DB::table('BankDetails')
						->insert($entries1);										
				}
				else
				{	
					$entries1['updated_at'] = date('Y-m-d h:i:sa');
					$result1 = DB::table('BankDetails')
							->where('userid', Session::get('employeeId'))
							->where('modeOfPayment', "Direct Deposit")
							->update($entries1);
				}
			}
			else 
				$result1=1;

			if (isset($entries2))
			{
				$select2=DB::table('paytmId')
						->where('userId', Session::get('employeeId'))
						->first();

				if (empty($select2))
				{
					$result2 = DB::table('paytmId')
							->insert($entries2);										
				}
				else
					$result2 = DB::table('paytmId')
							->where('userid', Session::get('employeeId'))
							->where('paytmId', $entries2['paytmId'])
							->update($entries2);
			}
			else 
				$result2=1;

			if ($result || $result1 || $result2)
			{
				$select = DB::table('user')
						->where('id', session()->get('employeeId'))
						->update(['registeration'=> 'bank']);
			}
			if ($result && $result1 && $result2) 
			{
				if (session()->get('complete') != 'on') 
				{
					$url="/salarySetup";
				}
				else
				{
					$url="/manageEmployees";
				}
			}
			else
			{
				$url="/addEmployees_step4";
			}

			$progress['userid'] 	= session()->get('employeeId');
			$progress['nextStep']	= 'done';
			$progress['complete']	= 'on';
			$progress['updated_at']	= date('Y-m-d h:i:sa');

			$updateProgress = DB::table('userProgress')
							->where('userid', $progress['userid'])
							->update($progress);

	   		return response()->json([
	   				'result' => $result, 
	   				'result1' => $result1, 
	   				'result2' => $result2, 
	   				'status' => 200, 
	   				'url' => $url,
	   				'url1' => '/addEmployees_step1',
	   				'message'=>'Success'
	   				]);	
	   	}
	}

	public function getBankData()
	{
		$email=Session::get('email');
		$result= getuserinfo($email);
		return View::make('addEmployees_step4')->with($result);
	}

	public function getUserBankInfo($email)
	{
		$select = DB::table('BankDetails')->where('userid', Session::get('employeeId'))->first();
		return $select;
	}

	public function insertFamilyData()
	{
		$id = Input::get('id');

		$validator = Validator::make(
			$entries = array(
					'userid' 			=> Session::get('employeeId'),
					'name' 				=> ucwords(strtolower(Input::get('name'))),
					'relation'			=> ucwords(strtolower(Input::get('relation'))),
					'dob'				=> Input::get('dob'),
					'address'	 		=> ucwords(Input::get('address')),
					'disabled' 			=> strtolower(Input::get('disabled')),
				),
				array(
					'name' 		=> 'required',
					'relation' 	=> 'required',
					'dob' 		=> 'required|date',
					'address' 	=> 'required',
			    ),
			$messages = array(
				   'required' 	=> 'This field is required.',
				   'date' 		=> 'This is not a valid date.',
				)
			);

		if ($validator->fails())
        {
        	return response()->json($validator->messages());
		}
		else
		{
			if (!empty($id)) 
			{
				$entries['updated_at'] 	= date('Y-m-d h:i:sa');

				$update = DB::table('familyDetails')
						->where('id', $id)
						->where('userid', session()->get('employeeId'))
						->update($entries);

				$select = DB::table('familyDetails')
						->where('id', $id)
						->first();
			}
			else
			{		
				$insert = DB::table('familyDetails')
						->insert($entries);

				$select = DB::table('familyDetails')
						->orderBy('id', 'DESC')
						->first();
			}

			$progress['userid'] 	= session()->get('employeeId');
			$progress['nextStep']	= 'step3';
			$progress['complete']	= 'off';
			$progress['updated_at']	= date('Y-m-d h:i:sa');

			$updateProgress = DB::table('userProgress')
							->where('userid', $progress['userid'])
							->update($progress);

	   		return response()->json([
	   				'status' 	=> 200,
	   				'message'	=> 'Data inserted successfully.', 
	   				'id'		=> $select->id,
	   			]);
		}
	}

	public function getFamilyData($id)
	{
		$select = DB::table('familyDetails')
				->where('id', $id)
				->where('userid', session()->get('employeeId'))
				->first();

		return response()->json($select);
	}

	public function deleteFamilyData($id)
	{
		$delete = DB::table('familyDetails')
				->where('id', $id)
				->where('userid', session()->get('employeeId'))
				->delete();

		$result = new StdClass;
		
		if (!empty($delete)) 
		{
			$result->status 	= 200;
		}
		else
		{
			$result->status 	= 405;
			$result->message 	= "You're not allowed to delete this data.";
		}

		return response()->json($result);
	}

}
