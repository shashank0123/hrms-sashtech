<?php namespace App\Http\Controllers\employer;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB, View, Validator, Input, Response, Session, Hash, Mail, StdClass;
use Illuminate\Http\Request;

class manageEmployeeController extends Controller {

	public function setContractorDetails($id)
	{
		session()->put('contractorId', $id);
		return redirect('/viewContractor');
	}

	public function viewContractorDetails()
	{
		$id 		= Session::get('contractorId');

		$result 	= DB::table('user')
					->where('id', $id)
					->first();

		$result1 	= DB::table('contractor')
					->where('userId', $id)
					->first();

		$result2 	= DB::table('personalDetails')
					->where('userid', $id)
					->first();

		$result3 	= DB::table('BankDetails')
					->where('userId', $id)
					->first();

		return View::make('viewContractor')
				->with('result', $result)
				->with('result1', $result1)
				->with('result2', $result2)
				->with('result3', $result3);
	}

	public function updateContractorDetails()
	{
		// echo "<pre>";
		// var_dump(Input::all());
		// die();
		$area 	= Input::get('area');
		$id 	= Session::get('contractorId');

		if ($area == "personal") 
		{
			$validator = Validator::make(
				$input = array(
					'firstName' => ucwords(strtolower(Input::get('firstName'))),
					'lastName' 	=> ucwords(strtolower(Input::get('lastName'))),
					'status' 	=> ucwords(strtolower(Input::get('status'))),
					'pan' 		=> strtoupper(Input::get('pan')),
					'dob' 		=> Input::get('dateOfBirth'),
				), 
				array(
					'firstName' => 'required',
					'lastName' 	=> 'required',
					'status' 	=> 'required|string',
					'pan' 		=> 'required|alpha_num|size:10|regex:/([a-zA-Z]){5}([0-9]){4}([a-zA-Z])/',
					'dob' 		=> 'required|date',
				), 
				$messages = array(
					'required' 	=> "This field is required.",
					'size'		=> "This field must have a length of 10.",
					'regex'		=> "The PAN is not valid.",
					'date'		=> "The date is not valid or in incorrect format."
				));

			if ($validator->fails()) 
			{
				return Response::json($validator->messages());
			}
			else 
			{
				$updateUser	= DB::table('user')
							->where('id', $id)
							->update([
								'firstName' 	=> $input['firstName'],
								'lastName' 		=> $input['lastName'],
								'updated_at' 	=> date("Y-m-d h:i:sa"),
							]);

				$updateStatus	= DB::table('contractor')
								->where('userId', $id)
								->update([
									'status' 		=> $input['status'],
									'updated_at' 	=> date("Y-m-d h:i:sa"),
								]);

				$updateDetails 	= DB::table('personalDetails')
								->where('userid', $id)
								->update([
									'PAN' 			=> $input['pan'],
									'DateOfBirth' 	=> $input['dob'],
									'updated_at' 	=> date("Y-m-d h:i:sa"),
								]);

				$result = new StdClass;
				if ($updateUser || $updateStatus || $updateDetails) 
				{
					$result->status 	= 200;
					$result->message 	= "Data updated successfully.";
				}
				return Response::json($result);
			}
		}
		elseif ($area == "address") 
		{
			$validator = Validator::make(
				$input = array(
					'localAddress' 		=> ucwords(strtolower(Input::get('localAddress'))),
					'permanentAddress' 	=> ucwords(strtolower(Input::get('permanentAddress'))),
				), 
				array(
					'localAddress' 		=> 'required',
					'permanentAddress' 	=> 'required',
				), 
				$messages = array(
					'required' 	=> "This field is required.",
				));

			if ($validator->fails()) 
			{
				return Response::json($validator->messages());
			}
			else 
			{
				$updateDetails 	= DB::table('personalDetails')
								->where('userid', $id)
								->update([
									'localAddress' 		=> $input['localAddress'],
									'permanentAddress' 	=> $input['permanentAddress'],
									'updated_at' 		=> date("Y-m-d h:i:sa"),
								]);

				$result = new StdClass;
				if ($updateDetails) 
				{
					$result->status 	= 200;
					$result->message 	= "Data updated successfully.";
				}
				return Response::json($result);
			}
		}
		elseif ($area == "contract") 
		{
			$validator = Validator::make(
				$input = array(
					'title' 		=> ucwords(strtolower(Input::get('titleOfContract'))),
					// 'nature' 		=> ucwords(strtolower(Input::get('natureOfContract'))),
					'TDS' 			=> strtoupper(Input::get('tds')),
					// 'serviceTax' 	=> strtoupper(Input::get('serviceTax')),
				), 
				array(
					'title' 		=> 'required',
					// 'nature' 		=> 'required',
					'TDS' 			=> 'required|string',
					// 'serviceTax' 	=> 'required|string',
				), 
				$messages = array(
					'required' 	=> "This field is required.",
					'string'	=> "This field must be a string.",
				));

			if ($validator->fails()) 
			{
				return Response::json($validator->messages());
			}
			else 
			{
				$updateContract	= DB::table('contractor')
								->where('userId', $id)
								->update([
									'titleContract' => $input['title'],
									'updated_at' 	=> date("Y-m-d h:i:sa"),
								]);

				$updateDetails 	= DB::table('personalDetails')
								->where('userId', $id)
								->update([
									'taxApplicable'	=> $input['TDS'],
									'updated_at' 	=> date("Y-m-d h:i:sa"),
								]);

				$result = new StdClass;
				if ($updateContract || $updateDetails) 
				{
					$result->status 	= 200;
					$result->message 	= "Data updated successfully.";
				}
				return Response::json($result);
			}
		}
		// elseif ($area == "bank") 
		// {
		// 	$validator = Validator::make(
		// 		$input = array(
		// 			'nameOfBank' 	=> ucwords(strtolower(Input::get('nameOfBank'))),
		// 			'accountNumber' => ucwords(strtolower(Input::get('accountNumber'))),
		// 			'type' 			=> ucwords(strtolower(Input::get('type'))),
		// 			'ifsc' 			=> strtoupper(Input::get('ifsc')),
		// 		), 
		// 		array(
		// 			'nameOfBank' 	=> 'required|string',
		// 			'accountNumber' => 'required|numeric|regex:/([0-9]){10,}\d/',
		// 			'type' 			=> 'required|string',
		// 			'ifsc' 			=> 'required|alpha_num|size:11|regex:/([A-Za-z]){4}[0][0-9A-Za-z]{6}/',
		// 		), 
		// 		$messages = array(
		// 			'required' 	=> "This field is required.",
		// 			'string'	=> "This field must be a string.",
		// 			'numeric'	=> "This field must be numeric.",
		// 			'size'		=> "The IFSC must be of length 11.",
		// 			'regex'		=> "The IFSC is not valid.",
		// 		));

		// 	if ($validator->fails()) 
		// 	{
		// 		return Response::json($validator->messages());
		// 	}
		// 	else 
		// 	{
		// 		$updateDetails 	= DB::table('BankDetails')
		// 						->where('userId', $id)
		// 						->update([
		// 							'bankName' 		=> $input['nameOfBank'],
		// 							'accountNumber'	=> $input['accountNumber'],
		// 							'ifsc'			=> $input['ifsc'],
		// 							'accountType'	=> $input['type'],
		// 							'updated_at' 	=> date("Y-m-d h:i:sa"),
		// 						]);

		// 		$result = new StdClass;
		// 		if ($updateDetails) 
		// 		{
		// 			$result->status 	= 200;
		// 			$result->message 	= "Data updated successfully.";
		// 		}
		// 		return Response::json($result);
		// 	}
		// }
		elseif ($area == 'payment') 
		{
			if (Input::get('modeOfPayment1')=='cheque') 
			{
				$check 	= DB::table('BankDetails')
						->where('userId', session()->get('contractorId'))
						->first();

				if (!empty($check)) 
				{
					$delete = DB::table('BankDetails')
							->where('userId', session()->get('contractorId'))
							->delete();
				}

				$checkPaytm = DB::table('paytmId')
							->where('userId', Session::get('contractorId'))
							->first();

				if (!empty($checkPaytm)) 
				{
					$delete = DB::table('paytmId')
							->where('userId', session()->get('contractorId'))
							->delete();
				}

				$checkPrev 	= DB::table('BankDetails')
							->where('userId', session()->get('contractorId'))
							->where('modeOfPayment', 'cheque')
							->first();

				$entries['userId'] 			= session()->get('contractorId');
				$entries['modeOfPayment']	= 'cheque';

				if (empty($checkPrev)) 
				{
					$insert = DB::table('BankDetails')
							->insert($entries);
				}
				else
				{
					$entries['updated_at'] = date('Y-m-d h:i:sa');

					$update = DB::table('BankDetails')
							->where('userId', $entries['userId'])
							->update($entries);
				}

				return response()->json([
		   				'status' => 200, 
		   				'message'=>'Success'
		   				]);	
			}

			if (Input::get('modeOfPayment1')=='directDeposit')
			{
				$validator = Validator::make(
					$entries = array(
							'userid' => Session::get('contractorId'),
							'modeOfPayment' => Input::get('modeOfPayment1'),
							'bankName' => Input::get('bankName1'),
							'branch' => strtoupper(Input::get('branch1')),
							'accountHolder' => strtoupper(Input::get('accountHolder1')),
							'accountNumber' => Input::get('accountNumber1'),
							'accountType' => strtoupper(Input::get('accountType1')),
							'ifsc' => strtoupper(Input::get('ifsc1')),
							'sharePercent' => strtoupper(Input::get('sharePercent1')),
						    
						),
						array(
							'modeOfPayment' => 'required',
							'bankName' => 'required',
							'branch' => 'required',
							'accountHolder' => 'required',
							'accountNumber' => 'required|numeric|regex:/([0-9]){10,}\d/',
							'accountType' => 'required',
							'ifsc' => 'required|alpha_num|size:11|regex:/([A-Za-z]){4}[0][0-9A-Za-z]{6}/',
							'sharePercent' => 'required',
					    	),
					$messages = array(
							   'required' => 'The :attribute field is required.',
							   'numeric' => 'The :attribute field must be a number'
							)

					);
			}
			
			$form2 = Input::get('bankName2');
			if (isset($form2))
			{
				$validator1 = Validator::make(
					$entries1 = array(
							'userid' => Session::get('contractorId'),
							'modeOfPayment' => "Direct Deposit",
							'bankName' => Input::get('bankName2'),
							'branch' => strtoupper(Input::get('branch2')),
							'accountHolder' => strtoupper(Input::get('accountHolder2')),
							'accountNumber' => Input::get('accountNumber2'),
							'accountType' => strtoupper(Input::get('accountType2')),
							'ifsc' => strtoupper(Input::get('ifsc2')),
							'sharePercent' => strtoupper(Input::get('sharePercent2')),
						    
						),
						array(
							'userid' => 'required',
							'bankName' => 'required',
							'branch' => 'required',
							'accountHolder' => 'required',
							'accountNumber' => 'required|numeric|regex:/([0-9]){10,}\d/',
							'accountType' => 'required',
							'ifsc' => 'required|alpha_num|size:11|regex:/([A-Za-z]){4}[0][0-9A-Za-z]{6}/',
							'sharePercent' => 'required',
					    	),
					$messages = array(
							   'required' => 'The :attribute field is required.',
							   'numeric' => 'The :attribute field must be a number'
							)

					);
			}

			$form3 = Input::get('paytmId');
			if (isset($form3))
			{
				$validator2 = Validator::make(
					$entries2 = array(
							'userid' 			=> Session::get('contractorId'),
							'companyId' 		=> Session::get('companyId'),
							'paytmId' 			=> Input::get('paytmId'),
							'paytmSharePercent' => strtoupper(Input::get('paytmSharePercent')),
						    
						),
						array(
							'userid' 			=> 'required',
							'paytmId' 			=> 'required',
							'paytmSharePercent' => 'required',
					    	),
					$messages = array(
							   'required' 	=> 'The :attribute field is required.',
							   'numeric' 	=> 'The :attribute field must be a number'
							)
					);
			}

			if ( (isset($validator)and($validator->fails()))||(isset($validator1)and($validator1->fails()))||(isset($validator2)and($validator2->fails())))
	        {
	        	$error = array();
	        	
	        	if (isset($validator))
	        	{
	        		if ($validator->fails()) 
	        		{
		        		$error[0]	=	$validator->messages();
	        		}
	        		else
	        		{
	        			$error[0]	=	 new StdClass;
	        		} 
	        	}
	        	if (isset($validator1)) 
	        	{
	        		if ($validator1->fails()) 
	        		{
		        		$error[1]	=	$validator1->messages();
	        		}
	        		else
	        		{
	        			$error[1]	=	 new StdClass;
	        		} 
	        	}
	        	if (isset($validator2)) 
	        	{
	        		if ($validator2->fails()) 
	        		{
		        		$error[2]	=	$validator2->messages();
	        		}
	        		else
	        		{
	        			$error[2]	=	 new StdClass;
	        		} 
	        	}

			    return $error;
			}
			else
			{
				$check 	= DB::table('BankDetails')
						->where('userId', Session::get('contractorId'))
						->first();

				if (!empty($check)) 
				{
					$delete = DB::table('BankDetails')
							->where('userId', Session::get('contractorId'))
							->delete();				
				}

				$checkPaytm = DB::table('paytmId')
							->where('userId', Session::get('contractorId'))
							->first();

				if (!empty($checkPaytm)) 
				{
					$deletePaytm 	= DB::table('paytmId')
									->where('userId', Session::get('contractorId'))
									->delete();
				}

				if (isset($entries))
				{
					$select=DB::table('BankDetails')
							->where('userid', Session::get('contractorId'))
							->where('modeOfPayment', "directDeposit")
							->first();

					if (empty($select))
					{
						$result = DB::table('BankDetails')
								->insert($entries);										
					}
					else
					{	
						$entries['updated_at'] = date('Y-m-d h:i:sa');
						$result = DB::table('BankDetails')
								->where('userid', Session::get('contractorId'))
								->where('modeOfPayment', "directDeposit")
								->update($entries);
					}
				}
				if (isset($entries1))
				{
					$select1=DB::table('BankDetails')
							->where('userid', Session::get('contractorId'))
							->where('modeOfPayment', "Direct Deposit")
							->first();
					
					if (empty($select1))
					{
						$result1 = DB::table('BankDetails')
							->insert($entries1);										
					}
					else
					{	
						$entries1['updated_at'] = date('Y-m-d h:i:sa');
						$result1 = DB::table('BankDetails')
								->where('userid', Session::get('contractorId'))
								->where('modeOfPayment', "Direct Deposit")
								->update($entries1);
					}
				}
				else 
					$result1=1;

				if (isset($entries2))
				{
					$select2=DB::table('paytmId')
							->where('userId', Session::get('contractorId'))
							->first();

					if (empty($select2))
					{
						$result2 = DB::table('paytmId')
								->insert($entries2);										
					}
					else
						$result2 = DB::table('paytmId')
								->where('userid', Session::get('contractorId'))
								->where('paytmId', $entries2['paytmId'])
								->update($entries2);
				}
				else 
					$result2=1;

				if ($result || $result1 || $result2)
				{
					$select = DB::table('user')
							->where('id', session()->get('contractorId'))
							->update(['registeration'=> 'bank']);
				}
				// if ($result && $result1 && $result2) 
				// {
				// 	$url="/manageEmployees";
				// }
				// else
				// {
				// 	$url="/addEmployees_step4";
				// }

		   		return response()->json([
		   				'result' => $result, 
		   				'result1' => $result1, 
		   				'result2' => $result2, 
		   				'status' => 200, 
		   				// 'url' => $url, 
		   				'message'=>'Success'
		   				]);	
		   	}
		}
		
	}

	public function getEmployeeDetails($companyId)
	{
		$select = DB::table('user')
				->where('companyId', $companyId)
				->where('type', 'Employee')
				->orderBy('created_at', 'DESC')
				->select('firstName', 'lastName', 'id', 'gender', 'status')
				->get();

		foreach ($select as $key => $value) 
		{
			$check 	= DB::table('userProgress')
					->where('userid', $value->id)
					->where('complete', 'on')
					->first();

			if (empty($check)) 
			{
				$newSelect[] = NULL;
				if (empty($value->terminateDate)) 
				{
					if (strtolower($value->status) == 'inactive') 
					{
						$value->action 	= 'Delete';
					}
					// else
					// {
					// 	$value->action 	= 'Terminate';
					// }

					$unfinished[]	= $value;
				}
			}
			else
			{
				$newSelect[] = $value;
			}
		}

		if (!empty($newSelect)) 
		{
			$select 	= array_filter($newSelect);
		}
		else
		{
			$select 	= [];
		}


		$select2 	= DB::table('user')
					->where('companyId', $companyId)
					->where('type', 'Employer')
					->select('firstName', 'lastName', 'id', 'gender')
					->first();

		if (!empty($select2)) 
		{
			array_push($select, $select2);
		}

		$array[0] = $select;
		
		if (!isset($unfinished)) 
		{
			$unfinished = NULL;
		}
		
		$array[1] = $unfinished;

		return $array;
	}

	public function getEmployee()
	{
		$return = $this->getEmployeeDetails(session()->get('companyId'));

		$select 	= $return[0];
		$select2 	= $return[1];

		return View::make('manageEmployees')
				->with('result' , $select)
				->with('result2' , $select2);
	}

	public function delete($id)
	{
		$check 	= DB::table('user')
				->where('id', $id)
				->where('companyId', session()->get('companyId'))
				->first();

		$result = new StdClass;

		if (!empty($check)) 
		{
			if ((strtolower($check->type) == 'employee') or (strtolower($check->type) == 'contractor')) 
			{
				$delete = DB::table('user')
						->where('id', $id)
						->delete();

				$result->status 	= 200;
			}
			else
			{
				$result->status 	= 405;
				$result->message 	= 'You can not delete this type of user!';	
			}
		}
		else
		{
			$result->status 	= 405;
			$result->message 	= 'You can not delete this employee!';
		}

		return response()->json($result);
	}

	public function terminate($id)
	{
		$check 	= DB::table('user')
				->where('id', $id)
				->where('companyId', session()->get('companyId'))
				->first();

		$result = new StdClass;

		if (!empty($check)) 
		{
			if ((strtolower($check->type) == 'employee') or (strtolower($check->type) == 'contractor')) 
			{
				$terminate 	= DB::table('user')
							->where('id', $id)
							->update([
								'terminateDate'=> Input::get('lastDate')
								]);

				$result->status 	= 200;
			}
			else
			{
				$result->status 	= 405;
				$result->message 	= 'You can not terminate this type of user!';	
			}
		}
		else
		{
			$result->status 	= 405;
			$result->message 	= 'You can not terminate this user!';
		}

		// return $result;
	}

	// Contractor functions
	public function getContractorDetails($companyId)
	{
		$select = DB::table('user')
				->select('firstName', 'lastName', 'id')
				->where('companyId', $companyId)
				->where('type', 'Contractor')
				->get();

		return $select;
	}

	public function getContractor()
	{
		$select = $this->getContractorDetails(session()->get('companyId'));
		return View::make('manageContractors')
				->with('result' , $select);
	}

	public function addContractor()
	{
		$validator = Validator::make(
			$input = array(
					'firstName' => ucfirst(strtolower(Input::get('firstName'))),
					'lastName' => ucfirst(strtolower(Input::get('lastName'))),
					'companyId' => Session::get('companyId'),
					'email' => strtolower(Input::get('email')),
					'joinDate' => Input::get('joinDate'),
					'contractorSelfOnBoarding' => ucfirst(strtolower(Input::get('contractorSelfOnBoarding'))),
					'type' => 'Contractor',
					'status' => ucwords(strtolower(Input::get('status'))),
					'team' => ucwords(strtolower(Input::get('team'))),
					'titleContract' => ucwords(strtolower(Input::get('titleContract'))),
					'earning' => ucwords(strtolower(Input::get('earning'))),
					'earningAmount' => Input::get('earningAmount'),
					// 'created_at' => date("d-m-y h:i:sa")
				),
			array(
					'firstName' => 'required',
					'lastName' => 'required',
					'companyId' => 'required',
					'email' => 'required|email',
					'joinDate' => 'required|date',
					'titleContract' => 'required',
					'earning' => 'required',
					'earningAmount' => 'required|numeric',
		    	),
			$messages = array(
				   'required' => 'The :attribute field is required.',
				   'date' => 'The date is not valid.',
				   'email' => 'The email is not valid.'
				)
			);

		if ($validator->fails()) 
		{
			return Response::json($validator->messages());
		}

		// $checkEmail = $input['email'];
		// $checkEmail = explode("@", $checkEmail)[1];
		// $emailDomain = explode(".", $checkEmail)[0];

		// // Not allowed email domains
		// $notAllowedDomains = array('gmail', 'yahoo', 'rediffmail', 'hotmail', 'outlook', 'ibibo', 'live', 'facebook');

		// if (in_array($emailDomain, $notAllowedDomains)) {
		// 	$result = new StdClass;
		// 	$result->email = "That email address is not allowed.";
		// 	$result->status = 406;
		// 	return [$result];
		// }

		if ($validator->fails()) 
		{
			return Response::json($validator->messages());
		}
		else
		{
			$company = DB::table('company')
					->where('id', session()->get('companyId'))
					->select('companyCode')
					->first();
			
			$user 	= DB::table('user')
					->where('companyId', $input['companyId'])
					->orderBy('created_at', 'desc')
					->select('id')
					->first();

			$IdNo = intval(str_replace($company->companyCode, '', $user->id));
			$IdNo++;
			$userid = ($company->companyCode . $IdNo);

			$entries = array(
					'id' => $userid,
					'firstName' => $input['firstName'],
					'lastName' => $input['lastName'],
					'companyId' => Session::get('companyId'),
					'team' => $input['team'],
					'email' => $input['email'],
					'joinDate' => $input['joinDate'],
					'employeeSelfOnBoarding' => $input['contractorSelfOnBoarding'],
					'type' => 'Contractor',
					'earning' => $input['earning'],
					'ctc' => $input['earningAmount'],
					// 'created_at' => date("d-m-y h:i:sa")	
					);
			
			$entries2 = array(
					'status' => $input['status'],
					'titleContract' => $input['titleContract'],
					// 'created_at' => date("d-m-y h:i:sa")
					);

			$select = DB::table('user')->where('email', $entries['email'])->first();

			if (empty($select))
			{
				$entries['apiKey'] = Hash::make($entries['email']);
				$result = DB::table('user')->insert($entries);
				
				$fullName = $entries['firstName'] . ' ' . $entries['lastName'];

				$email = Mail::send('emails.newUser',array('email'=>$entries["email"],'fullname'=>$fullName, 'apiKey' => $entries['apiKey'], 'adminfrom' => 'rajbabuhome@gmail.com'), function($message) {
					$message->from('rajbabuhome@gmail.com', 'Sashtechs');
					$message->to(Input::get('email'))->subject('Welcome to Sashtechs!');
				});

				$select1 = DB::table('user')
						->where('email', $entries['email'])
						->first();

				Session::put('contractorId', $select1->id);

				$entries2['userId'] = $select1->id;

				$select2 = DB::table('contractor')->where('userId', $entries2['userId'])->first();

				if (empty($select2)) 
				{
					$result = DB::table('contractor')->insert($entries2);
				}

				$result = new StdClass;
				$result->message = "The contractor has been created.";
				$result->status = 200;
				if ((isset($input['contractorSelfOnBoarding']))&&($input['contractorSelfOnBoarding']=="On")) 
                {
                    $result->url="/manageContractors";
                }
                else{
                    $result->url="/addContractor_step2";
                }
			}
			else {
				$result = new StdClass;
				$result->message = "The contractor with that email address already exists.";
				$result->status = 208;
			}

		   	return response()->json($result);
		}
	}

	public function insertPersonalData()
	{
		$validator = Validator::make(
			$entries = array(
					'userid' => Session::get('contractorId'),
					'pan' => Input::get('pan'),
					'DateOfBirth' => Input::get('DateOfBirth'),
					'permanentAddress' => ucfirst(strtolower(Input::get('permanentAddress'))),
					// 'created_at' => date("d-m-y h:i:sa")
				),
			array(
					'pan' => 'required|alpha_num|size:10|regex:/([a-zA-Z]){5}([0-9]){4}[a-zdA-Z]/',
					'DateOfBirth' => 'required',
					'permanentAddress' => 'required',
		    	),
			$messages = array(
				   'required' => 'The :attribute field is required.'
				)
			);

		if ($validator->fails()) 
		{
			return Response::json($validator->messages());
		}
		else
		{
			$select = DB::table('personalDetails')->where('userid', Session::get('contractorId'))->first();
			if (empty($select)){
				$result = DB::table('personalDetails')->insert($entries);
			}
			else {
				$result=DB::table('personalDetails')->where('userid', Session::get('contractorId'))->update($entries);
			}
			$url="/addContractor_step3";
	   		return response()->json(['result' => $result, 'status' => 200, 'url' => $url, 'message'=>'Data inserted successfully']);
		}
	}

	public function insertTaxData()
	{
		$validator = Validator::make(
				$entries = array(
						'userid' 		=> Session::get('contractorId'),
						'taxApplicable' => ucfirst(Input::get('taxApplicable')),
						'serviceTaxApplicable'=>Input::get('serviceTaxApplicable'),
						'PFAcNo' 		=> Input::get('PFAcNo'),
						'esiNumber' 	=> strtoupper(Input::get('esiNumber')),
						'uan' 			=> strtoupper(Input::get('uan')),
						'contractorNature' 		=> Input::get('nature'),
					    
					),
					array(
						'PFAcNo' 	=> 'size:23|regex:/([A-Za-z]{5})[0-9]{18}/',
						'esiNumber' => 'digits:10|numeric',
						'uan' 		=> 'digits:12|numeric',
				    ),
				$messages = array(
					   'required' 	=> 'The :attribute field is required.',
					   'regex'		=> 'This is invalid.',
					   'size'		=> 'This must be of length 23.'
					)
				);
			if ($validator->fails())
            {
			   return Response::json($validator->messages());
			}			   
			else
			{		
				$result = DB::table('personalDetails')
						->where('userid', Session::get('contractorId'))
						->update($entries);

				if ($result)
				{
					$url="/addContractor_step4";
			   		return response()->json([
			   				'result' 	=> $result, 
			   				'status' 	=> 200, 
			   				'url' 		=> $url, 
			   				'message'	=> 'Data inserted successfully'
			   				]);
				}
				else 
				{	
		   			return response()->json([
		   					'result' => $result, 
		   					'status' => 400,  
		   					'message'=>'Data can not inserted'
		   					]);
				}
			}		
	}

	public function insertPaymentData1()
	{			
		$validator = Validator::make(
				$entries = array(
						'userid' => Session::get('contractorId'),
						'modeOfPayment' => ucwords(strtolower(Input::get('modeOfPayment'))),
						'bankName' => ucwords(strtolower(Input::get('bankName'))),
						'branch' => ucwords(strtolower(Input::get('branch'))),
						'accountHolder' => ucwords(strtolower(Input::get('accountHolder'))),
						'accountNumber' => Input::get('accountNumber'),
						'accountType' => ucwords(strtolower(Input::get('accountType'))),
						'ifsc' => strtoupper(Input::get('ifsc'))
					    
					),
					array(
						'modeOfPayment' => 'required',
						'bankName' => 'required',
						'branch' => 'required',
						'accountHolder' => 'required',
						'accountNumber' => 'required|numeric|regex:/([0-9]){10,}\d/',
						'accountType' => 'required',
						'ifsc' => 'required|alpha_num|size:11|regex:/([A-Za-z]){4}[0][0-9A-Za-z]{6}/'
				    	),
				$messages = array(
						   'required' => 'The :attribute field is required.',
						   'numeric' => 'The :attribute field must be a number'
						)

				);
		if ($validator->fails())
        {
		   return Response::json($validator->messages());
		}			   
		else
		{
			$select=DB::table('BankDetails')->where('userid', Session::get('contractorId'))->get();
			if ($select){
				$result=DB::table('BankDetails')->where('userid', Session::get('contractorId'))->update($entries);
			}		
			else
				$result=DB::table('BankDetails')->insert($entries);


			$url="/manageContractors";

	   		return response()->json(['result' => $result, 'status' => 200, 'url' => $url, 'message'=>'Success']);	
	   	}
	}

	public function insertPaymentData()
	{
		if (Input::get('modeOfPayment')=='cheque') 
		{
			$check 	= DB::table('BankDetails')
					->where('userId', session()->get('contractorId'))
					->first();

			if (!empty($check)) 
			{
				$delete = DB::table('BankDetails')
						->where('userId', session()->get('contractorId'))
						->delete();
			}

			$checkPaytm = DB::table('paytmId')
						->where('userId', Session::get('contractorId'))
						->first();

			if (!empty($checkPaytm)) 
			{
				$delete = DB::table('paytmId')
						->where('userId', session()->get('contractorId'))
						->delete();
			}

			$checkPrev 	= DB::table('BankDetails')
						->where('userId', session()->get('contractorId'))
						->where('modeOfPayment', 'cheque')
						->first();

			$entries['userId'] 			= session()->get('contractorId');
			$entries['modeOfPayment']	= 'cheque';

			if (empty($checkPrev)) 
			{
				$insert = DB::table('BankDetails')
						->insert($entries);
			}
			else
			{
				$entries['updated_at'] = date('Y-m-d h:i:sa');

				$update = DB::table('BankDetails')
						->where('userId', $entries['userId'])
						->update($entries);
			}

			$progress['userid'] 	= session()->get('contractorId');
			$progress['nextStep']	= 'done';
			$progress['complete']	= 'on';
			$progress['updated_at']	= date('Y-m-d h:i:sa');

			$updateProgress = DB::table('userProgress')
							->where('userid', $progress['userid'])
							->update($progress);

			if (session()->get('complete') != 'on') 
			{
				$url="/salarySetup";
			}
			else
			{
				$url="/manageContractors";
			}

			return response()->json([
	   				'status' 	=> 200, 
	   				'url' 		=> $url,
	   				'url1' 		=> '/addContractor_step1',
	   				'message'	=> 'Success'
	   				]);	
		}

		if (Input::get('modeOfPayment')=='directDeposit')
		{
			$validator = Validator::make(
				$entries = array(
						'userid' => Session::get('contractorId'),
						'modeOfPayment' => Input::get('modeOfPayment'),
						'bankName' => Input::get('bankName'),
						'branch' => strtoupper(Input::get('branch')),
						'accountHolder' => strtoupper(Input::get('accountHolder')),
						'accountNumber' => Input::get('accountNumber'),
						'accountType' => strtoupper(Input::get('accountType')),
						'ifsc' => strtoupper(Input::get('ifsc')),
						'sharePercent' => strtoupper(Input::get('sharePercent')),
					    
					),
					array(
						'modeOfPayment' => 'required',
						'bankName' => 'required',
						'branch' => 'required',
						'accountHolder' => 'required',
						'accountNumber' => 'required|numeric|regex:/([0-9]){10,}\d/',
						'accountType' => 'required',
						'ifsc' => 'required|alpha_num|size:11|regex:/([A-Za-z]){4}[0][0-9A-Za-z]{6}/',
						// 'sharePercent' => 'required',
				    	),
				$messages = array(
						   'required' => 'The :attribute field is required.',
						   'numeric' => 'The :attribute field must be a number'
						)

				);
		}
		
		// $form2 = Input::get('bankName2');
		// if (isset($form2))
		// {
		// 	$validator1 = Validator::make(
		// 		$entries1 = array(
		// 				'userid' => Session::get('contractorId'),
		// 				'modeOfPayment' => "Direct Deposit",
		// 				'bankName' => Input::get('bankName2'),
		// 				'branch' => strtoupper(Input::get('branch2')),
		// 				'accountHolder' => strtoupper(Input::get('accountHolder2')),
		// 				'accountNumber' => Input::get('accountNumber2'),
		// 				'accountType' => strtoupper(Input::get('accountType2')),
		// 				'ifsc' => strtoupper(Input::get('ifsc2')),
		// 				'sharePercent' => strtoupper(Input::get('sharePercent2')),
					    
		// 			),
		// 			array(
		// 				'userid' => 'required',
		// 				'bankName' => 'required',
		// 				'branch' => 'required',
		// 				'accountHolder' => 'required',
		// 				'accountNumber' => 'required|numeric|regex:/([0-9]){10,}\d/',
		// 				'accountType' => 'required',
		// 				'ifsc' => 'required|alpha_num|size:11|regex:/([A-Za-z]){4}[0][0-9A-Za-z]{6}/',
		// 				'sharePercent' => 'required',
		// 		    	),
		// 		$messages = array(
		// 			   'required' => 'The :attribute field is required.',
		// 			   'numeric' => 'The :attribute field must be a number'
		// 			)

		// 		);
		// }

		// $form3 = Input::get('paytmId');
		// if (isset($form3))
		// {
		// 	$validator2 = Validator::make(
		// 		$entries2 = array(
		// 				'userid' 			=> Session::get('contractorId'),
		// 				'companyId' 		=> Session::get('companyId'),
		// 				'paytmId' 			=> Input::get('paytmId'),
		// 				'paytmSharePercent' => strtoupper(Input::get('paytmSharePercent')),
					    
		// 			),
		// 			array(
		// 				'userid' 			=> 'required',
		// 				'paytmId' 			=> 'required',
		// 				'paytmSharePercent' => 'required',
		// 		    	),
		// 		$messages = array(
		// 				   'required' 	=> 'The :attribute field is required.',
		// 				   'numeric' 	=> 'The :attribute field must be a number'
		// 				)
		// 		);
		// }

		if ( (isset($validator)and($validator->fails()))||(isset($validator1)and($validator1->fails()))||(isset($validator2)and($validator2->fails())))
        {
        	$error = array();
        	
        	if (isset($validator))
        	{
        		if ($validator->fails()) 
        		{
	        		$error[0]	=	$validator->messages();
        		}
        		else
        		{
        			$error[0]	=	 new StdClass;
        		} 
        	}
        	if (isset($validator1)) 
        	{
        		if ($validator1->fails()) 
        		{
	        		$error[1]	=	$validator1->messages();
        		}
        		else
        		{
        			$error[1]	=	 new StdClass;
        		} 
        	}
        	if (isset($validator2)) 
        	{
        		if ($validator2->fails()) 
        		{
	        		$error[2]	=	$validator2->messages();
        		}
        		else
        		{
        			$error[2]	=	 new StdClass;
        		} 
        	}

		    return $error;
		}
		else
		{
			$check 	= DB::table('BankDetails')
					->where('userId', Session::get('contractorId'))
					->first();

			if (!empty($check)) 
			{
				$delete = DB::table('BankDetails')
						->where('userId', Session::get('contractorId'))
						->delete();				
			}

			$checkPaytm = DB::table('paytmId')
						->where('userId', Session::get('contractorId'))
						->first();

			if (!empty($checkPaytm)) 
			{
				$deletePaytm 	= DB::table('paytmId')
								->where('userId', Session::get('contractorId'))
								->delete();
			}

			if (isset($entries))
			{
				$select=DB::table('BankDetails')
						->where('userid', Session::get('contractorId'))
						->where('modeOfPayment', "directDeposit")
						->first();

				if (empty($select))
				{
					$result = DB::table('BankDetails')
							->insert($entries);										
				}
				else
				{	
					$entries['updated_at'] = date('Y-m-d h:i:sa');
					$result = DB::table('BankDetails')
							->where('userid', Session::get('contractorId'))
							->where('modeOfPayment', "directDeposit")
							->update($entries);
				}
			}

			if (isset($entries1))
			{
				$select1=DB::table('BankDetails')
						->where('userid', Session::get('contractorId'))
						->where('modeOfPayment', "Direct Deposit")
						->first();
				
				if (empty($select1))
				{
					$result1 = DB::table('BankDetails')
						->insert($entries1);										
				}
				else
				{	
					$entries1['updated_at'] = date('Y-m-d h:i:sa');
					$result1 = DB::table('BankDetails')
							->where('userid', Session::get('contractorId'))
							->where('modeOfPayment', "Direct Deposit")
							->update($entries1);
				}
			}
			else 
				$result1=1;

			if (isset($entries2))
			{
				$select2=DB::table('paytmId')
						->where('userId', Session::get('contractorId'))
						->first();

				if (empty($select2))
				{
					$result2 = DB::table('paytmId')
							->insert($entries2);										
				}
				else
					$result2 = DB::table('paytmId')
							->where('userid', Session::get('contractorId'))
							->where('paytmId', $entries2['paytmId'])
							->update($entries2);
			}
			else 
				$result2=1;

			if ($result || $result1 || $result2)
			{
				$select = DB::table('user')
						->where('id', session()->get('contractorId'))
						->update(['registeration'=> 'bank']);
			}
			if ($result && $result1 && $result2) 
			{
				if (session()->get('complete') != 'on') 
				{
					$url="/salarySetup";
				}
				else
				{
					$url="/manageContractors";
				}
			}
			else
			{
				$url="/addContractor_step4";
			}

			$progress['userid'] 	= session()->get('contractorId');
			$progress['nextStep']	= 'done';
			$progress['complete']	= 'on';
			$progress['updated_at']	= date('Y-m-d h:i:sa');

			$updateProgress = DB::table('userProgress')
							->where('userid', $progress['userid'])
							->update($progress);

	   		return response()->json([
	   				'result' => $result, 
	   				'result1' => $result1, 
	   				'result2' => $result2, 
	   				'status' => 200, 
	   				'url' => $url,
	   				'url1' => '/addContractor_step1',
	   				'message'=>'Success'
	   				]);	
	   	}
	}

	public function getEmployeeDetail()
	{
		$id = session()->get('employeeId');
		session()->put('employeeId', NULL);

		if (empty($id))
		{
			return redirect('manageEmployees');
		}

		$result 	= DB::table('personalDetails')
					->where('userid', $id)
					->first();

		$result1 	= DB::table('user')
					->where('id', $id)
					->first();

		$result2 	= DB::table('employeeCtc')
					->where('userId', $id)
					->first();

		$result3 	= DB::table('BankDetails')
					->where('userId', $id)
					->get();

		$result4 	= DB::table('paytmId')
					->where('userId', $id)
					->first();

		return View::make('viewEmployee')
				->with('result', $result)
				->with('result1', $result1)
				->with('result2', $result2)
				->with('result3', $result3)
				->with('result4', $result4);
	}

	public function viewEmployeeDetails($id)
	{
		session()->put('employeeId', $id);
		return redirect('/viewEmployee');
	}

}
