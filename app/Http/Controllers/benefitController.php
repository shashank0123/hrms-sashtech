<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator,File,DB,Input, View, StdClass;
use Illuminate\Http\Request;

class benefitController extends Controller {
	public function addBenefit()
	{
		$validator = Validator::make(
			$entries = array(
					'companyId' => session()->get('companyId'),
					'planLogo' => Input::file('planLogo'),
					'planCompany' => Input::get('planCompany'),
					'planName' => Input::get('planName'),
					'coverage' => Input::get('coverage'),
					'premium' => Input::get('premium'),
					'sumAssured' => Input::get('sumAssured'),
					'familyFloater' => Input::get('familyFloater'),
					'familyFloaterOption' => Input::get('familyFloaterOption'),
					'waitingPeriod' => Input::get('waitingPeriod'),
					'firstYearExclusion' => Input::get('firstYearExclusion'),
					'twoYearExclusion' => Input::get('twoYearExclusion'),
					'diseaseCover' => Input::get('diseaseCover'),
					'maternityBenefit' => Input::get('maternityBenefit'),
					'maternityLimit' => Input::get('maternityLimit'),
					'extensionNineMonth' => Input::get('extensionNineMonth'),
					'waiverExpense' => Input::get('waiverExpense'),
					'waiverAmount' => Input::get('waiverAmount'),
					'copay' => Input::get('copay'),
					'copayAmount' => Input::get('copayAmount'),
					'childCover' => Input::get('childCover'),
					'corporateBuffer' => Input::get('corporateBuffer'),
					'allDayCare' => Input::get('allDayCare'),
					'tpa' => Input::get('tpa'),
					// 'created_at' => date("d-m-y h:i:sa"),
				),
				array(
					'companyId' => 'required',
					'planLogo' => 'max:500|mimes:jpg,jpeg,png',
					'planName' => 'required',
					'coverage' => 'required',
					'premium' => 'required',
					'sumAssured' => 'required',
			    ),
			$messages = array(
				   'required' => 'The :attribute field is required.',
				   'mimes' => 'The file is not in acceptable format (i.e. jpg,jpeg,png).',
				   'max' => 'The file is bigger than 500KB.'
				)
		);
		if ($validator->fails())
        {
        	return response()->json($validator->messages());
		}			   
		else
		{	
			$entries 	= array_filter($entries);
			$uploadPath = public_path() . '/HTML/Uploads/benefit/';

			//check if the directory exists
			if(!File::exists($uploadPath)) 
			{
				File::makeDirectory($uploadPath , 0775, true);
			}

			// Move file to upload directory
			if (isset($entries['planLogo'])) 
			{
				$planLogo = $uploadPath . 'logo-' . $entries['planLogo']->getClientOriginalName();
				if(File::exists($planLogo)) 
				{
					unlink($planLogo);
				}
				$file = $entries['planLogo']->move($uploadPath, $planLogo);
				
				$planLogo = explode('public/', $planLogo)[1];
				$entries['planLogo'] = $planLogo;
			}

			$select 	= DB::table('benefit')
						->where('planName', Input::get('planName'))
						->first();
			
			if (empty($select))
			{
				$result = DB::table('benefit')
						->insert($entries);
			}
			else 
			{
				$result = DB::table('benefit')
						->where('planName', Input::get('planName'))
						->update($entries);		
			}

			$check 	= DB::table('benefitSearch')
					->where('companyId', session()->get('companyId'))
					->first();

			if (!empty($check)) 
			{
				$delete	= DB::table('benefitSearch')
						->where('companyId', session()->get('companyId'))
						->delete();
			}

			$url="/addBenefit_step2";
		   	
		   	return response()->json([
		   		'result' => $result,'url' => $url,'status' => 200, 'message'=>'Data inserted successfully']);
		}
	}

	public function viewChooseBenefit()
	{
		if (strtolower(session()->get('type')) != 'employer') 
		{
			return redirect('login');
		}

		$id 	= session()->get('companyId');
		$result = $this->getBenefits($id);
		
		return View::make('addBenefit_step2')
				->with('result', $result);
	}

	public function getBenefits($id)
	{
		$select = DB::table('benefit')
				->where('companyId', $id)
				->get();
		
		return $select;
	}

	public function chooseBenefit($id)
	{
		$select = DB::table('companyBenefit')
				->where('companyId', session()->get('companyId'))
				->first();

		if (empty($select))
		{
			$select	= DB::table('companyBenefit')
					->insert([
						'companyId'=>session()->get('companyId'), 
						'benefitId'=> $id
						]);
		}
		else
		{
			$select = DB::table('companyBenefit')
					->where('companyId', session()->get('companyId'))
					->update([
						'benefitId'=>$id
						]);
		}

		$result 		= new StdClass;
		$result->status = 200;
		$result->url 	= "/addBenefit_step3";
		
		return response()->json($result);
	}

	public function addEmployee()
	{
		$id=session()->get('companyId');
		$result= $this->addEmployeeBenefit($id);
		return View::make('addBenefit_step3')->with('result', $result);
	}

	public function addEmployeeBenefit($id)
	{
		$select = DB::table('user')->where('companyId', $id)->select('id', 'firstName', 'lastName')->get();
		return 	$select;	
	}

	public function searchBenefit()
	{
		// echo "<pre>";
		// var_dump(Input::all());
		// die();
		$validator = Validator::make(
			$entries = array(
					'companyId' => session()->get('companyId'),
					'sumAssured' => Input::get('sumAssured'),
					'familyFloater' => Input::get('familyFloater'),
					'familyFloaterOption' => Input::get('familyFloaterOption'),
					'waitingPeriod' => Input::get('waitingPeriod'),
					'firstYearExclusion' => Input::get('firstYearExclusion'),
					'twoYearExclusion' => Input::get('twoYearExclusion'),
					'diseaseCover' => Input::get('diseaseCover'),
					'maternityBenefit' => Input::get('maternityBenefit'),
					'maternityLimit' => Input::get('maternityLimit'),
					'extensionNineMonth' => Input::get('extensionNineMonth'),
					'waiverExpense' => Input::get('waiverExpense'),
					'waiverAmount' => Input::get('waiverAmount'),
					'copay' => Input::get('copay'),
					'copayAmount' => Input::get('copayAmount'),
					'childCover' => Input::get('childCover'),
					'corporateBuffer' => Input::get('corporateBuffer'),
					

					// 'created_at' => date("d-m-y h:i:sa"),
				),
				array(
					'companyId' => 'required',
					'sumAssured' => 'required',
			    ),
			$messages = array(
				   'required' => 'The :attribute field is required.',
				)
		);
		if ($validator->fails())
        {
        	return response()->json($validator->messages());
		}			   
		else
		{	
			$entries 	= array_filter($entries);

			$select 	= DB::table('benefitSearch')
						->where('companyId', session()->get('companyId'))
						->first();

			if (empty($select))
			{
				$result	= DB::table('benefitSearch')
						->insert($entries);
			}
			else 
			{
				$result = DB::table('benefitSearch')
						->where('companyId', session()->get('companyId'))
						->update($entries);
			}

			$check 	= DB::table('benefit')
					->where('companyId', session()->get('companyId'))
					->first();

			if (!empty($check)) 
			{
				$check 	= DB::table('benefit')
						->where('companyId', session()->get('companyId'))
						->delete();
			}

			$steps 	= DB::table('employerProgress')
					->where('companyId', session()->get('companyId'))
					->update([
						'step8'		=> 'on',
						'complete'	=> 'on',
						'nextStep'	=> 'done',
						]);

			// if ($result) 
			// {
			// 	$email = Mail::send('emails.newUser',array('email'=>$entries['email'],'fullname'=>$fullName, 'apiKey' => $apiKey, 'adminfrom' => 'rajbabuhome@gmail.com'), function($message) use ($toemail)
			// 	{
			// 		$message->from('rajbabuhome@gmail.com', 'Sashtechs');
			// 			// var_dump($row['email']);
			// 		$message->to('jaspal@sashtechs.com')->subject('Welcome to Sashtechs!');
			// 	});				
			// }

			$url 	= "/employerDashboard";
		   	
		   	return response()->json([
		   			'result' 	=> $result,
		   			'url' 		=> $url,
		   			'status' 	=> 200,
		   			'message'	=> "We are processing your details and we'll get back to you shortly with competitive quotes."
		   		]);
		}
	}

	public function addBenefitToEmployee()
	{
		// 'employeeContribution' => Input::get('employeeContribution'),
		// 'employerContribution' => Input::get('employerContribution'),
		$validator = Validator::make(
			$entries = array(
					'companyId' => session()->get('companyId'),
					'sumAssured' => Input::get('sumAssured'),
					'familyFloater' => Input::get('familyFloater'),
					'familyFloaterOption' => Input::get('familyFloaterOption'),
					'waitingPeriod' => Input::get('waitingPeriod'),
					'firstYearExclusion' => Input::get('firstYearExclusion'),
					'twoYearExclusion' => Input::get('twoYearExclusion'),
					'diseaseCover' => Input::get('diseaseCover'),
					'maternityExtension' => Input::get('maternityExtension'),
					'maternityLimit' => Input::get('maternityLimit'),
					'childCover' => Input::get('childCover'),
					'corporateBuffer' => Input::get('corporateBuffer'),

					// 'created_at' => date("d-m-y h:i:sa"),
				),
				array(
					'companyId' => 'required',
					'sumAssured' => 'required',
			    ),
			$messages = array(
				   'required' => 'The :attribute field is required.',
				)
		);
		if ($validator->fails())
        {
        	return response()->json($validator->messages());
		}			   
		else
		{	
			$entries = array_filter($entries);
			$select = DB::table('benefit')->where('planName', Input::get('planName'))->first();
			if (empty($select)){
				$result=DB::table('benefit')->insert($entries);
			}
			else {
				$result=DB::table('benefit')->where('planName', Input::get('planName'))->update($entries);		
			}
			$url="addBenefit_step2";
			$steps=DB::table('employerProgress')->where('companyId', session()->get('companyId'))->update(['step8'=>'on']);

		   	return response()->json(['result' => $result,'url' => $url,'status' => 200, 'message'=>'Data inserted successfully']);
		}		
	}	

	public function addemployeeBenefitoptions()
	{
		
		$validator = Validator::make(
			$entries = array(
					'userId' => session()->get('employeeId'),
					'adult' => Input::get('adult'),
					'children' => Input::get('children'),
					'maxAge' => Input::get('maxAge'),
					// 'created_at' => date("d-m-y h:i:sa"),
				),
				array(
					'userId' => 'required',
					'adult' => 'required',
					'children' => 'required',
					// 'maxAge' => 'required',
			    ),
			$messages = array(
				   'required' => 'The :attribute field is required.',
				)
		);
		if ($validator->fails())
        {
        	return response()->json($validator->messages());
		}			   
		else
		{
			$select = DB::table('employeeBenefits')->where('userId', session()->get('employeeId'))->first();
			if (empty($select))
				$select = DB::table('employeeBenefits')->insert($entries);
			else
				$select = DB::table('employeeBenefits')->where('userId', session()->get('employeeId'))->update($entries);
			$result= new StdClass;
			$result->status=200;
			$result->url="viewDetails";
			$result->response=$select;
			return response()->json($result);

		}

	}

	public function viewStep1()
	{
		if (strtolower(session()->get('type')) != 'employer') 
		{
			return redirect('login');
		}

		$checkReq 	= DB::table('benefitSearch')
					->where('companyId', session()->get('companyId'))
					->first();

		if (empty($checkReq)) 
		{
			$select=DB::table('benefit')
					->where('companyId', session()->get('companyId'))
					->first();
			return view('addBenefit_step1')->with('benefit', $select);
		}
		else
		{
			return redirect('addBenefit_step2');
		}
	}
}
