<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use File, DB, Input, StdClass;
use Illuminate\Http\Request;

class calenderController extends Controller 
{
	public function openCalender()
	{
		if (strtolower(session()->get('type')) != 'employer')
		{
			return redirect('login');
		}
		else
		{
			return view('calendar');
		}
	}

	public function getUserCalenderData()
	{
		if (session()->get('employerId'))
		{
			$path     = DB::table('calendar')
					  ->where('userId', session()->get('employerId'))
					  ->first();
			
			if (!empty($path)) 
			{
				$path->jsonObject = public_path() . $path->jsonObject;

				if (File::exists($path->jsonObject)) 
				{
					$file = File::get($path->jsonObject);
				}
				
				if (!isset($file)||empty($file))
				{
					$select= new StdClass;
					$select='[]';
					return $select;
				}
				else 
				{
					return $file;
				}         
			}
			else 
			{
				$select= new StdClass;
				$select='[]';
				return $select;
			}
		}
		elseif (session()->get('employeeId'))
		{
			$path   = DB::table('calendar')
					->where('userId', session()->get('employeeId'))
					->first();

			$cid        = session()->get('companyId');			

			if (!empty($cid)) 
			{
				$path2  = DB::table('calendar')
						->where('userId', $cid)
						->first();

				if (!empty($path2)) 
				{
					$path2->jsonObject = public_path() . $path2->jsonObject;

					if (File::exists($path2->jsonObject)) 
					{
						$file = File::get($path2->jsonObject);
					}
					
					if (!isset($file)||empty($file))
					{
						$array['employer']='{}';
					}
					else 
					{
						$array['employer'] = $file;
					}
				}
				else 
				{
					$array['employer']='{}';
				}
			}

			if (!empty($path)) 
			{
				$path->jsonObject = public_path() . $path->jsonObject;

				if (File::exists($path->jsonObject)) 
				{
					$file2 = File::get($path->jsonObject);
				}

				if (!isset($file2)||empty($file2))
				{
					$array['employee']='[]';
				}
				else 
				{
					$array['employee'] = $file2;
				}         
			}
			else 
			{
				$array['employee']='[]';
			}

			return json_encode($array);
		}
		else 
		{
			$result=new StdClass;
			$result='[]';
			return response()->json($result);
		}   
	}

	public function getCompanyCalenderData()
	{
		if (session()->get('companyId'))
		{
			$path   = DB::table('calendar')
					->where('userId', session()->get('companyId'))
					->first();

			if (!empty($path)) 
			{
				$path->jsonObject = public_path() . $path->jsonObject;

				if (File::exists($path->jsonObject)) 
				{
					$file = File::get($path->jsonObject);
				}
				if (!isset($file)||empty($file))
				{
					$select= new StdClass;
					$select='[]';
					return $select;
				}
				else 
				{
					return $file;
				}         
			}
			else 
			{
				$select= new StdClass;
				$select='[]';
				return $select;
			}
		}
		elseif (session()->get('employeeId'))
		{
			$path   = DB::table('calendar')
					->where('userId', session()->get('companyId'))
					->first();

			if (!empty($path)) 
			{
				$path->jsonObject = public_path() . $path->jsonObject;

				if (File::exists($path->jsonObject)) 
				{
					$file = File::get($path->jsonObject);
				}
				if (!isset($file)||empty($file))
				{
					$select= new StdClass;
					$select='[]';
					return $select;
				}
				else 
				{
					return $file;
				}         
			}
			else 
			{
				$select= new StdClass;
				$select='[]';
				return $select;
			}
		}
		else 
		{
			$result=new StdClass;
			$result='[]';
			return response()->json($result);
		}
	}
		
	public function sendUserCalendarData()
	{
		$content=Input::get('events');

		if (session()->get('employerId'))
		{
			$uploadPath = public_path() . '/HTML/Uploads/' . session()->get('companyId').'/';

			if(!(File::exists($uploadPath))) 
			{
				File::makeDirectory($uploadPath, 0775, true);
			}    
			
			$uploadPath = $uploadPath.'calendarJson';
			$result 	= File::put($uploadPath, $content);

			if ($result)
			{
				$select = DB::table('calendar')
						->where('userid', session()->get('companyId'))
						->first();

				if (empty($select))
				{
					$uploadPath = explode(public_path(), $uploadPath)[1];

					$result 	= DB::table('calendar')
								->insert([
									'userid' => session()->get('companyId'), 
									'jsonObject' => $uploadPath
									]);
				}
				else
				{
					$result = 1;
				}

				return response()->json($result);
			}
		}
		elseif (session()->get('employeeId'))
		{
			$uploadPath = public_path() . '/HTML/Uploads/' . session()->get('employeeId').'/';
		
			if(!File::exists($uploadPath)) 
			{
				File::makeDirectory($uploadPath, 0775, true);
			}    
			
			$uploadPath 	= $uploadPath.'calendarJson';
			$result 		= File::put($uploadPath, $content);

			if ($result)
			{
				$select = DB::table('calendar')
						->where('userid', session()->get('employeeId'))
						->first();

				if (empty($select))
				{
					$uploadPath = explode(public_path(), $uploadPath)[1];

					$result = DB::table('calendar')
							->insert([
								'userid' => session()->get('employeeId'), 
								'jsonObject' => $uploadPath
								]);
				}
				else
				{
					$result = 1;;
				}

				return response()->json($result);
			}
		}
	}

	public function holidayList($id)
	{
		if ($id=="holiday") 
		{
		  	$country = Input::get('country');
		
		  	$path = DB::table('calendar')->where('userId', $country)->first();

			if (!empty($path)) 
			{
			  	$path->jsonObject = public_path() . $path->jsonObject;

				if (File::exists($path->jsonObject)) 
				{
					$file = File::get($path->jsonObject);
				}

				if (!isset($file)||empty($file))
				{
					$select= new StdClass;
					$select='[]';
					return $select;
				}
				else 
				{
					return $file;
				}         
			}
			else 
			{
				$select= new StdClass;
				$select='[]';
				return $select;
			}
		}
	}
}

