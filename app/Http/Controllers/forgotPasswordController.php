<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input, View, Response, Validator, StdClass, DB, Mail;
use Illuminate\Http\Request;

class forgotPasswordController extends Controller {

	public function forgotPassword()
	{
		$validator = Validator::make(
			$input = array(
				'email' => strtolower(Input::get('email')),
			), 
			array(
				'email'	=> 'required|email',
			), 
			$messages = array(
				'required' 	=> "This field is required.",
				'email' 	=> "This field is not a valid.",
			)
		);

		if ($validator->fails()) {
			return View::make('forgotPassword')
						->with('errors', $validator->errors()->all()[0]);
		}
		else
		{
			$check = DB::table('user')
					->where('email', $input['email'])
					->first();

			$result = new StdClass;
			
			if (empty($check)) 
			{
				$result->status 	= 204;
				$result->message 	= "This email address is not registered with us.";

				return View::make('forgotPassword')
						->with('result', $result);
			}
			else
			{
				$random = mt_rand(1000000, 9999999);

				$update = DB::table('user')
						->where('email', $input['email'])
						->update([
							'randomNumber' 	=> $random,
							'updated_at' 	=> date("Y-m-d h:i:sa"),
						]);

				$user 	= DB::table('user')
						->where('email', $input['email'])
						->first();

				$mail 	= Mail::send('emails.forgotPassword', array('email' => $input['email'], 'firstName' => $user->firstName, 'lastName' => $user->lastName, 'apiKey' => $user->apiKey, 'adminfrom' => 'no-reply@sashtechs.com'), function($message) 
					{
	                    $message->from('no-reply@sashtechs.com', 'Sashtechs - System');
	                    $message->to(Input::get('email'))->subject('Reset Password Link.');
	                }
	            );
	            $result->status 	= 200;
	            $result->message 	= "The reset link has been sent to the provided email.";

	            return View::make('forgotPassword')
						->with('result', $result);	
			}
		}
	}
}
