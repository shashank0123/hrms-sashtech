<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator,DB,Input;
use Illuminate\Http\Request;

class taxDetailController extends Controller {

	public function getTaxDetail()
	{
		if (strtolower(session()->get('type')) != 'employer') 
		{
			return redirect('login');
		}

		// if (strtolower(session()->get('complete')) == 'on' ) 
		// {
		// 	return redirect('employerDashboard');
		// }
		
		$companyId 	= session()->get('companyId');

		$details 	= DB::table('companyTaxDetail')
					->where('companyId', $companyId)
					->first();

		return view('taxDetail')
				->with('result', $details);
	}

	public function addTaxDetail()
	{
		$validator = Validator::make(
			$entries = array(
				'companyId' 	=> session()->get('companyId'),
				'pan'			=> Input::get('pan'),
				'tan'			=> Input::get('tan'),
				'epf'			=> Input::get('epf'),
				// 'epfCompany' 	=> Input::get('epfCompany'),
				// 'epfEmployee'	=> Input::get('epfEmployee'),
				'esi' 			=> Input::get('esi'),
				// 'esiCompany' 	=> Input::get('esiCompany'),
				// 'esiEmployee' 	=> Input::get('esiEmployee'),
				'gratuity' 		=> Input::get('gratuity'),
				),
			array(
				'pan' 		=> 'required|alpha_num|size:10|regex:/([a-zA-Z]){5}([0-9]){4}([a-zA-Z])/',
				'tan' => 'required|alpha_num|size:10|regex:/([A-Za-z]){4}([0-9]){5}([A-Za-z])/',
				),
			$messages = array(
				'required' => 'The :attribute field is required.',
				)
			);

		if (empty($entries['epf'])) 
		{
			$entries['epf']	= 'off';
			// $entries['epfCompany']	= NULL;
			// $entries['epfEmployee']	= NULL;
		}

		if (empty($entries['esi'])) 
		{
			$entries['esi']	= 'off';
			// $entries['esiCompany']	= NULL;
			// $entries['esiEmployee']	= NULL;
		}

		if (empty($entries['gratuity'])) 
		{
			$entries['gratuity'] = 'off';
		}

		if ($validator->fails())
		{
			return response()->json($validator->messages());
				   //return Response::json($validator->messages());
		}			   
		else
		{	
			$entries = array_filter($entries);
			
			$select = DB::table('companyTaxDetail')
					->where('companyId', session()->get('companyId'))
					->first();

			if (empty($select))
			{
				$result	= DB::table('companyTaxDetail')
						->insert($entries);

				$select	= DB::table('employerProgress')
						->where('companyId', session()->get('companyId'))
						->update([
							'step5' 	=> 'on', 
							'nextStep' 	=> 'step6', 
							'step6' 	=> 'pending'
						]);
			}
			else 
			{
				$entries['updated_at'] 	= date("Y-m-d h:i:sa");

				$result	= DB::table('companyTaxDetail')
						->where('companyId', session()->get('companyId'))
						->update($entries);

				$select	= DB::table('employerProgress')
						->where('companyId', session()->get('companyId'))
						->update([
							'step5' 	=> 'on', 
							'nextStep' 	=> 'step6', 
							'step6' 	=> 'pending'
						]);
			}

			$url="/setupLeave";

			return response()->json([
								'result' 	=> $result, 
								'status' 	=> 200, 
								'url' 		=> $url, 
								'message'	=> 'Data inserted successfully'
							]);
		}	
	}

}