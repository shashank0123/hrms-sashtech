<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Excel, Input, Validator, StdClass, Hash, DB, View, Response,Session, Mail, ReflectionClass;
use Illuminate\Http\Request;

class bulkFeatureController extends Controller {

	public function usersUpload()
	{
		$validator = Validator::make( 
				$entries = array(
					'file' => Input::file('uploadFile'),
					), 
				array(
					'file' => 'required|max:500|mimes:xls,xlsm,xlsx,xlsb,csv',
					), 
				$messages = array(
					'required' 	=> 'This file is required.',
					'max'		=> 'The file is bigger than 500KB.',
					'mimes'		=> 'The file is not in an acceptable format.',
					)
			);

		if ($validator->fails()) 
		{
			return Response::json($validator->messages());
		}

		$errors=array();

		Excel::load(Input::file('uploadFile'), function ($reader) 
		{
			foreach ($reader->toArray() as $row) 
			{
				if (strtolower($row['firstname'])=="test")
					continue;
				// if ($row['joindate'] == NULL) {
				// 	continue;
				// }

				// var_dump($row['joindate']);
				
				// if (is_object($row['joindate'])) 
				// {
				// 	$row['joindate'] = date('Y-m-d', strtotime($row['joindate']));
				// }
				// else
				// {
				$row['joindate'] = date('Y-m-d', strtotime($row['joindate']));	
				// }

				// var_dump($row['joindate']);
				// continue;
				// die;

				$finalErrors[$row['no']] = '';

				$validator = Validator::make(
					$entries = array(
						'firstName' 				=> ucwords(strtolower($row['firstname'])),
						'lastName' 					=> ucwords(strtolower($row['lastname'])),
						'title' 					=> ucwords(strtolower($row['title'])),
						'localAddress' 				=> ucwords($row['localaddress']),
						'team' 						=> ucwords(strtolower($row['team'])),
						'email' 					=> strtolower($row['email']),
						'companyId'					=> Session::get('companyId'),
						'type' 						=> 'Employee',
						'joinDate' 					=> $row['joindate'],
						'ctc' 						=> $row['ctc'],
						'employeeSelfOnBoarding' 	=> "On",
								// 'created_at' => date("d-m-y h:i:sa")
						),
					array(
						'firstName' 	=> 'required|string',
						'lastName' 		=> 'required|string',
						'title' 		=> 'required',
						'companyId' 	=> 'required',
						'type' 			=> 'required',
						'team' 			=> 'required',
						'localAddress' 	=> 'required',
						'email' 		=> 'required|email',
						'joinDate' 		=> 'required|date',
						'ctc' 			=> 'required'
						),
					$messages = array(
						'required' 	=> 'The :attribute field is required.',
						'date' 		=> 'The date is not valid.',
						'email'		=> 'The email is not valid.'
						)
				);

				if ($validator->fails())
				{
					$errors = $validator->messages();

					$errors1 = new ReflectionClass($errors);
					$errors1 = $errors1->getProperty('messages');
					$errors1->setAccessible(true);
					$errors = $errors1->getValue($errors);
				}			   
				else
				{

					$personalDetail['localaddress'] = $entries['localAddress'];
					$entries['localAddress'] = null;
					$entries = array_filter($entries);

					$validator1 = Validator::make(
						$entries1 = array(
							'companyId' 		=> session()->get('companyId'),
							'jobTitle' 			=> $row['title'],
							'ctc' 				=> $row['ctc'],
							'basic' 			=> $row['basic'],
							'hra' 				=> $row['hra'],
							'conveyance' 		=> $row['conveyance'],
							'medicalAllowance' 	=> $row['medicalallowance'],
							'medicalInsurance' 	=> $row['medicalinsurance'],
							'telephone' 		=> $row['telephone'],
							'leaveTravel' 		=> $row['leavetravel'],
							'uniform' 			=> $row['uniform'],
							'gratuity' 			=> $row['gratuity'],
							'superAnnuation' 	=> $row['superannuation'],
							'annualBonus' 		=> $row['annualbonus'],
							'festivalBonus' 	=> $row['festivalbonus'],
							'incentives' 		=> $row['incentives'],
							'others' 			=> $row['others'],
							'leaveEncashment' 	=> $row['leaveencashment'],
							'pfContribution' 	=> $row['pfcontribution'],
							'esiContribution' 	=> $row['esicontribution'],
									// 'created_at' => date("d-m-y h:i:sa"),
							),
						array(
							'basic' 			=> 'required|numeric',
							'hra' 				=> 'required|numeric',
							'conveyance' 		=> 'required|numeric',
							'medicalAllowance' 	=> 'required|numeric',
							'medicalInsurance' 	=> 'numeric',
							'telephone' 		=> 'numeric',
							'leaveTravel' 		=> 'numeric',
							'uniform' 			=> 'numeric',
							'gratuity' 			=> 'numeric',
							'superAnnuation' 	=> 'numeric',
							'annualBonus' 		=> 'numeric',
							'festivalBonus' 	=> 'numeric',
							'incentives' 		=> 'numeric',
							'others' 			=> 'numeric',
							'leaveEncashment' 	=> 'numeric',
							'pfContribution' 	=> 'numeric',
							'esiContribution' 	=> 'numeric',
							),
						$messages = array(
							'required' 	=> 'The :attribute field is required.',
							'numeric' 	=> 'The :attribute field must be a number.'
							)
					);

					if ($validator1->fails())
					{
						$errors = $validator1->messages();

						$errors1 = new ReflectionClass($errors);
						$errors1 = $errors1->getProperty('messages');
						$errors1->setAccessible(true);
						$errors = $errors1->getValue($errors);
					}			   
					else
					{
						$sum = 0;
						foreach ($entries1 as $key => $value) 
						{
							if (($key == 'companyId') or ($key ==  'ctc') or ($key == 'jobTitle')) 
							{
								continue;
							}
							$sum = $sum + $value;
						}

						if ($sum != $entries1['ctc']) 
						{
							$errors = new StdClass;
							$errors->ctc[] = 'Please cross check the ctc and it\'s components for this.';
							goto assignErrors;
						}

						if ($entries1['conveyance'] > 19200) 
						{
							$errors = new StdClass;
							$errors->ctc[] = 'Please cross check the ctc, the conveyance can not be greater than Rs. 19200.';
							goto assignErrors;
						}

						$toemail = NULL;
						$toemail = $entries['email'];

						$template 	= DB::table('ctc')
									->where('companyId', session()->get('companyId'))
									->where('jobtitle', $row['title'])
									->where('ctc', $entries1['ctc'])
									->first();

						if (empty($template))
						{
							$result	= DB::table('ctc')
									->insert($entries1);					
						}

						$sum = 	$row['medicalinsurance'] + 
								$row['telephone'] + 
								$row['leavetravel'] + 
								$row['uniform'] + 
								$row['gratuity'] + 
								$row['superannuation'] + 
								$row['annualbonus'] + 
								$row['festivalbonus'] + 
								$row['incentives'] + 
								$row['others'] + 
								$row['leaveencashment']; 
								

						$basic 		= $row['basic']+$row['hra']+$row['conveyance']+$row['medicalallowance'];
						$monthlyPay	= ($basic)/12;
						$oneDayPay	= ($basic)/365;

						$entries1['monthlyPay'] = $monthlyPay;
						$entries1['oneDayPay'] 	= $oneDayPay;
						$entries1['sum'] 		= $sum;

						// $validator2 = Validator::make(
						// 	$entries2 = array(
						// 		'team' => ucwords(strtolower($row['team'])),
						// 		'companyId' => Session::get('companyId'),
						// 		'companyCode' => strtoupper(Session::get('companyCode')),
						// 				// 'created_at' => date("d-m-y h:i:sa")
						// 	),
						// 	array(
						// 		'companyCode' => 'required',
						// 		'team' => 'required',
						// 	),
						// 	$messages = array(
						// 		'required' => 'The :attribute field is required.',
						// 	)
						// );
						// if ($validator2->fails())
						// {
						// 	$finalErrors[$row['no']] = $finalErrors[$row['no']].Response::json($validator2->messages());
						// }			   
						// else
						// {	
						
						$select = DB::table('user')
								->where('email', $entries['email'])
								->first();

						$company = DB::table('company')
								->where('id', session()->get('companyId'))
								->select('companyCode')
								->first();

						$user 	= DB::table('user')
								->where('companyId', $entries['companyId'])
								->orderBy('created_at', 'desc')
								->select('id')
								->first();

						$IdNo = intval(str_replace($company->companyCode, '', $user->id))+1;
						$userid = ($company->companyCode . $IdNo);
						$userid;
						$entries['id'] = $userid;
						$entries1['userId'] = $userid;
						$personalDetail['userid'] = $userid;

						if (empty($select))
						{
							$apiKey = Hash::make($entries['email']);
							$entries['apiKey'] = $apiKey;
							
							$result = DB::table('user')
									->insert($entries);

							// $team 	= DB::table('team')
							// 		->where('companyId', session()->get('companyId'))
							// 		->where('team' , ucwords(strtolower($row['team'])))
							// 		->first();

							// if (empty($team))
							// {
							// 	$team 	= DB::table('team')
							// 			->insert($entries2);
							// }
							// else
							// {
							// 	$team=DB::table('team')
							// 	->where('companyId', session()->get('companyId'))
							// 	->update(['members' => intval($team->members)+1]);
							// }

							$select2 = DB::table('personalDetails')
									->where('userid', $personalDetail['userid'])
									->first();

							if (empty($select2)) 
							{
								$insert1 	= DB::table('personalDetails')
											->insert($personalDetail);
							}

							$progress 	= DB::table('employerProgress')
										->where('companyId', session()->get('companyId'))
										->first();

							if ($progress->step2!='On')
							{
								$progress 	= DB::table('employerProgress')
											->where('companyId', session()->get('companyId'))
											->update([
												'step2' => 'on', 
												'step3' => 'on', 
												'nextStep' => 'step4'
												]);
							}

							$select1 = DB::table('user')
									->where('email', $entries['email'])
									->first();

							$new 	= DB::table('employeeCtc')
									->where('userid', $select1->id)
									->first();

							if (empty($new))
							{
								$result = DB::table('employeeCtc')
										->insert($entries1);
							}
							
							$fullName 	= $entries['firstName'] . ' ' . $entries['lastName'];

							$email 	= Mail::send(
										'emails.newUser',
										array(
											'email'		=> $entries['email'],
											'fullname'	=> $fullName, 
											'apiKey' 	=> $apiKey, 
											'adminfrom' => 'rajbabuhome@gmail.com'
										), 
										function($message) use ($toemail)
										{
											$message->from('rajbabuhome@gmail.com', 'Sashtechs');
											$message->to($toemail)->subject('Welcome to Sashtechs!');
										}
									);


							$result = new StdClass;
							$result->message 	= "The employee has been created.";
							$result->status 	= 200;
						}
						else 
						{
							$result 			= new StdClass;
							$result->message 	= "The employee with that email address already exists.";
							$result->status 	= 208;
						}

						$errors = $result;
					}
				}

			assignErrors:	$finalErrors[$row['no']] = $errors;
			}
			$finalErrors = array_filter($finalErrors);
			Session::put('finalErrors', $finalErrors);
		});

		$finalErrors = Session::get('finalErrors');
		Session::put('finalErrors', NULL);

		if ($finalErrors)
		{
			$finalErrors['status'] = 400;
			return response()->json($finalErrors);
		}
		else 
		{
			return response()->json(['status'=>200, 'result'=>'success']);
		}
	}

	public function usersEdit()
	{
		$validator = Validator::make( 
				$entries = array(
					'file' => Input::file('uploadFile'),
					), 
				array(
					'file' => 'required|max:500',
					), 
				$messages = array(
					'required' 	=> 'This file is required.',
					'max'		=> 'The file is bigger than 500KB.',
					'mimes'		=> 'The file is not in an acceptable format.',
					)
			);

		if ($validator->fails()) 
		{
			return Response::json($validator->messages());
		}

		$errors=array();

		Excel::load(Input::file('uploadFile'), function ($reader) 
		{
			foreach ($reader->toArray() as $row) 
			{
				// if ($row['joindate'] == NULL) {
				// 	continue;
				// }

				// var_dump($row['joindate']);
				
				// if (is_object($row['joindate'])) 
				// {
				// 	$row['joindate'] = date('Y-m-d', strtotime($row['joindate']));
				// }
				// else
				// {
				 $row['wef'] = date('Y-m-d', strtotime($row['wef']));	
				// }

				// var_dump($row['joindate']);
				// continue;
				// die;

				$finalErrors[$row['no']] = '';

				$validator = Validator::make(
					$entries = array(
						// 'firstName' 				=> ucwords(strtolower($row['firstname'])),
						// 'lastName' 					=> ucwords(strtolower($row['lastname'])),
						'id' 					=> $row['userid'],
						'title' 					=> ucwords(strtolower($row['title'])),
						'localAddress' 				=> ucwords($row['localaddress']),
						'team' 						=> ucwords(strtolower($row['team'])),
						// 'email' 					=> strtolower($row['email']),
						'companyId'					=> Session::get('companyId'),
						// 'type' 						=> 'Employee',
						// 'joinDate' 					=> $row['joindate'],

						'ctc' 						=> $row['ctc'],
						// 'employeeSelfOnBoarding' 	=> "On",
								// 'created_at' => date("d-m-y h:i:sa")
						),
					array(
						// 'firstName' 	=> 'required|string',
						// 'lastName' 		=> 'required|string',
						'id' 		=> 'required',
						'title' 		=> 'required',
						'companyId' 	=> 'required',
						// 'type' 			=> 'required',
						'team' 			=> 'required',
						'localAddress' 	=> 'required',
						// 'email' 		=> 'required|email',
						// 'joinDate' 		=> 'required|date',
						'ctc' 			=> 'required'
						),
					$messages = array(
						'required' 	=> 'The :attribute field is required.',
						'date' 		=> 'The date is not valid.',
						
						)
				);

				if ($validator->fails())
				{
					$errors = $validator->messages();

					$errors1 = new ReflectionClass($errors);
					$errors1 = $errors1->getProperty('messages');
					$errors1->setAccessible(true);
					$errors = $errors1->getValue($errors);
				}			   
				else
				{

					$personalDetail['localaddress'] = $entries['localAddress'];
					$entries['localAddress'] = null;
					$entries = array_filter($entries);

					$validator1 = Validator::make(
						$entries1 = array(
							'companyId' 		=> session()->get('companyId'),
							'jobTitle' 			=> $row['title'],
							'ctc' 				=> $row['ctc'],
							'basic' 			=> $row['basic'],
							'hra' 				=> $row['hra'],
							'conveyance' 		=> $row['conveyance'],
							'medicalAllowance' 	=> $row['medicalallowance'],
							'medicalInsurance' 	=> $row['medicalinsurance'],
							'telephone' 		=> $row['telephone'],
							'leaveTravel' 		=> $row['leavetravel'],
							'uniform' 			=> $row['uniform'],
							'gratuity' 			=> $row['gratuity'],
							'superAnnuation' 	=> $row['superannuation'],
							'annualBonus' 		=> $row['annualbonus'],
							'festivalBonus' 	=> $row['festivalbonus'],
							'incentives' 		=> $row['incentives'],
							'others' 			=> $row['others'],
							'leaveEncashment' 	=> $row['leaveencashment'],
							
									// 'created_at' => date("d-m-y h:i:sa"),
							),
						array(
							'basic' 			=> 'required|numeric',
							'hra' 				=> 'required|numeric',
							'conveyance' 		=> 'required|numeric',
							'medicalAllowance' 	=> 'required|numeric',
							'medicalInsurance' 	=> 'numeric',
							'telephone' 		=> 'numeric',
							'leaveTravel' 		=> 'numeric',
							'uniform' 			=> 'numeric',
							'gratuity' 			=> 'numeric',
							'superAnnuation' 	=> 'numeric',
							'annualBonus' 		=> 'numeric',
							'festivalBonus' 	=> 'numeric',
							'incentives' 		=> 'numeric',
							'others' 			=> 'numeric',
							'leaveEncashment' 	=> 'numeric',
							'pfContribution' 	=> 'numeric',
							'esiContribution' 	=> 'numeric',
							),
						$messages = array(
							'required' 	=> 'The :attribute field is required.',
							'numeric' 	=> 'The :attribute field must be a number.'
							)
					);

					if ($validator1->fails())
					{
						$errors = $validator1->messages();

						$errors1 = new ReflectionClass($errors);
						$errors1 = $errors1->getProperty('messages');
						$errors1->setAccessible(true);
						$errors = $errors1->getValue($errors);
					}			   
					else
					{
						$sum = 0;
						foreach ($entries1 as $key => $value) 
						{
							if (($key == 'companyId') or ($key ==  'ctc') or ($key == 'jobTitle')) 
							{
								continue;
							}
							$sum = $sum + $value;
						}

						if ($sum != $entries1['ctc']) 
						{
							$errors = new StdClass;
							$errors->ctc[] = 'Please cross check the ctc and it\'s components for this.';
							goto assignErrors;
						}

						if ($entries1['conveyance'] > 19200) 
						{
							$errors = new StdClass;
							$errors->ctc[] = 'Please cross check the ctc, the conveyance can not be greater than Rs. 19200.';
							goto assignErrors;
						}

						// $toemail = NULL;
						// $toemail = $entries['email'];

						// $template 	= DB::table('ctc')
						// 			->where('companyId', session()->get('companyId'))
						// 			->where('jobtitle', $row['title'])
						// 			->where('ctc', $entries1['ctc'])
						// 			->first();

						// if (empty($template))
						// {
						// 	$result	= DB::table('ctc')
						// 			->insert($entries1);					
						// }

						$sum = 	$row['medicalinsurance'] + 
								$row['telephone'] + 
								$row['leavetravel'] + 
								$row['uniform'] + 
								$row['gratuity'] + 
								$row['superannuation'] + 
								$row['annualbonus'] + 
								$row['festivalbonus'] + 
								$row['incentives'] + 
								$row['others'] + 
								$row['leaveencashment']; 
							

						$basic 		= $row['basic']+$row['hra']+$row['conveyance']+$row['medicalallowance'];
						$monthlyPay	= ($basic)/12;
						$oneDayPay	= ($basic)/365;

						$entries1['monthlyPay'] = $monthlyPay;
						$entries1['oneDayPay'] 	= $oneDayPay;
						$entries1['sum'] 		= $sum;

						// $validator2 = Validator::make(
						// 	$entries2 = array(
						// 		'team' => ucwords(strtolower($row['team'])),
						// 		'companyId' => Session::get('companyId'),
						// 		'companyCode' => strtoupper(Session::get('companyCode')),
						// 				// 'created_at' => date("d-m-y h:i:sa")
						// 	),
						// 	array(
						// 		'companyCode' => 'required',
						// 		'team' => 'required',
						// 	),
						// 	$messages = array(
						// 		'required' => 'The :attribute field is required.',
						// 	)
						// );
						// if ($validator2->fails())
						// {
						// 	$finalErrors[$row['no']] = $finalErrors[$row['no']].Response::json($validator2->messages());
						// }			   
						// else
						// {	
						
						$select = DB::table('user')
								->where('id', $entries['id'])
								->first();

						$company = DB::table('company')
								->where('id', session()->get('companyId'))
								->select('companyCode')
								->first();

						$user 	= DB::table('user')
								->where('companyId', $entries['companyId'])
								->orderBy('created_at', 'desc')
								->select('id')
								->first();

						

						
							// $apiKey = Hash::make($entries['email']);
							// $entries['apiKey'] = $apiKey;
							
							 $result = DB::table('user')
									->where('id', $entries['id'])
									->update($entries);

							// $team 	= DB::table('team')
							// 		->where('companyId', session()->get('companyId'))
							// 		->where('team' , ucwords(strtolower($row['team'])))
							// 		->first();

							// if (empty($team))
							// {
							// 	$team 	= DB::table('team')
							// 			->insert($entries2);
							// }
							// else
							// {
							// 	$team=DB::table('team')
							// 	->where('companyId', session()->get('companyId'))
							// 	->update(['members' => intval($team->members)+1]);
							// }

							$select2 = DB::table('personalDetails')
									->where('userid', $entries['id'])
									->first();

							if (empty($select2)) 
							{
								$insert1 	= DB::table('personalDetails')
											->insert($personalDetail);
							}

							
							$select1 = DB::table('user')
									->where('id', $entries['id'])
									->first();

							$new 	= DB::table('employeeCtc')
									->where('userid', $select1->id)
									->first();

							if (empty($new))
							{
								$result = DB::table('employeeCtc')
										->insert($entries1);
							}
							
							// $fullName 	= $entries['firstName'] . ' ' . $entries['lastName'];

							// $email 	= Mail::send(
							// 			'emails.newUser',
							// 			array(
							// 				'email'		=> $entries['email'],
							// 				'fullname'	=> $fullName, 
							// 				'apiKey' 	=> $apiKey, 
							// 				'adminfrom' => 'rajbabuhome@gmail.com'
							// 			), 
							// 			function($message) use ($toemail)
							// 			{
							// 				$message->from('rajbabuhome@gmail.com', 'Sashtechs');
							// 				$message->to($toemail)->subject('Welcome to Sashtechs!');
							// 			}
							// 		);


							$result = new StdClass;
							$result->message 	= "The employee has been created.";
							$result->status 	= 200;
						

						$errors = $result;
					}
				}

			assignErrors:	$finalErrors[$row['no']] = $errors;
			}
			$finalErrors = array_filter($finalErrors);
			Session::put('finalErrors', $finalErrors);
		});

		$finalErrors = Session::get('finalErrors');
		Session::put('finalErrors', NULL);

		if ($finalErrors)
		{
			$finalErrors['status'] = 400;
			return response()->json($finalErrors);
		}
		else 
		{
			return response()->json(['status'=>200, 'result'=>'success']);
		}
	}

}