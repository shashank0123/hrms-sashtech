<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB, Validator, Input, Response, StdClass;
use Illuminate\Http\Request;

class EmployeeDetailApi extends Controller {
	public function getPersonalDetails($Apikey)
	{
		$Select= DB::table('user')->where('Apikey', $Apikey)->first();
		$id=$Select->id;
		$result=DB::table('personalDetails')->where('userid', $id)->first();
		return response()->json([$result]);

	}

	public function editPersonalData($Apikey, $pan)
	{
		
		$Select= DB::table('user')->where('Apikey', $Apikey)->first();
		$id=$Select->id;

		$validator = Validator::make(
					$entries = array(
							'userid' => $id,
							'pan' => strtoupper($pan)
							),
						array(
							'pan' => 'required|alpha_num|size:10|regex:/([a-zA-Z]){5}([0-9]){4}([a-zA-Z])/',
							),
						$messages = array(
 						   'required' => 'The :attribute field is required.',
 						   'regex' => 'Enter a valid pan no..'
							)
					);

			if ($validator->fails())
            {
            	return response()->json([$validator->messages()]);
			}			   
			else
			{		
				$select = DB::table('personalDetails')->where('userid', $id)->first();
				if (empty($select)){
					$result=DB::table('personalDetails')->insert($entries);
				}
				else {
					$result=DB::table('personalDetails')->where('userid', $id)->update($entries);		
				}
			   	return response()->json([$result]);
			}
	}

	// public function updateQuote()
	// {
	// 	$entries['quote'] = trim('
	// 	Always borrow money from a pessimist. He won’t expect it back.
	// 	');
		
	// 	$entries['author'] = trim('
	// 	Oscar Wilde 
	// 	');
		
	// 	$entries['status'] = "not active";

	// 	echo "<pre>";
	// 	var_dump($entries);
	// 	// die;

	// 	$insert = DB::table('quotes')
	// 			->insert($entries);

	// 	var_dump($insert);
	// }

	public function insertPersonalData()
	{
		$apiKey = Input::get('apiKey');
		$select = DB::table('user')
				->where('apiKey', $apiKey)
				->select('id', 'companyId')
				->first();

		$id = $select->id;

			$validator = Validator::make(
					$entries = array(
							'userid' => $id,
							'pan' => strtoupper(Input::get('pan')),
							'dateOfBirth' => Input::get('dateOfBirth'),
							'permanentAddress' => ucwords(strtolower(Input::get('permanentAddress'))),
							'gender' => Input::get('gender'),
							'altEmail' => Input::get('altEmail'),
							'phone' => Input::get('phone'),
						),
						array(
							'userid' => 'required',
							'pan' => 'required|alpha_num|size:10|regex:/([a-zA-Z]){5}([0-9]){4}[a-zdA-Z]/',
							'dateOfBirth' => 'required|date',
							'permanentAddress' => 'required',
							'altEmail' => 'email',
							'gender' => 'required'
					    	),
					$messages = array(
 						   'required' => 'The :attribute field is required.',
 						   'email' => 'Enter a valid email address',
 						   'regex' => 'Enter a valid pan no..'
							)
					);

			$date = date('Y-m-d');
		    $date1 = date_create($date);

		    $date2 = date_create($entries['dateOfBirth']);

		    $diff = date_diff($date1, $date2);
		    $age = $diff->y;

			if ($validator->fails())
            {
			   return Response::json($validator->messages());
			}
			elseif ($age < 16) {
				return Response::json(['dateOfBirth' => ['The age of the employee can not be less than 16 years.']]);
			}
			else
			{
				$gender = $entries['gender'];
				$entries['gender'] = null;
				$entries = array_filter($entries);

				$select = DB::table('personalDetails')->where('userid', $id)->first();

				if (empty($select))
				{
					$result=DB::table('personalDetails')
							->insert($entries);
				}
				else 
				{
					$entries['updated_at'] = date('Y-m-d h:i:sa');

					$result = DB::table('personalDetails')
							->where('userid', $id)
							->update($entries);
				}

				$result1 = DB::table('user')
						->where('id', $entries['userid'])
						->update(array('gender' => $gender));

				if (isset($result) && isset($result1)) 
				{
					$check 	= DB::table('userProgress')
							->where('userid', $entries['userid'])
							->first();

					$progress['userid'] 	= $entries['userid'];
					$progress['nextStep']	= 'step2';
					$progress['complete']	= 'off';

					if (empty($check)) 
					{
						$insertProgress = DB::table('userProgress')
										->insert($progress);
					}
					else
					{
						$progress['updated_at'] = date('Y-m-d h:i:sa');

						$updateProgress = DB::table('userProgress')
										->where('userid', $progress['userid'])
										->update($progress);
					}

					session()->put('employeeProgressNextStep', 'step2');


					$url="/onboardingdetails2";

			   		return response()->json([
			   			'result' => $result, 
			   			'status' => 200, 
			   			'apiKey' => $apiKey, 
			   			'message'=>'Data inserted successfully'
			   			]);
				}
				else
			   		return response()->json(['result' => $result, 'status' => 400, 'message'=>'data not inserted']);
			}
	}

	public function insertTaxData()
	{
		$apiKey = Input::get('apiKey');
		$select = DB::table('user')
				->where('apiKey', $apiKey)
				->select('id', 'companyId')
				->first();

		$id = $select->id;

			$validator = Validator::make(
					$entries = array(
							'userid' => $id,
							'taxApplicable' => Input::get('taxApplicable'),
							'PFAcNo' => Input::get('PFAcNo'),
							'esiNumber' => strtoupper(Input::get('esiNumber')),
    					    
						),
						array(
							'PFAcNo' => 'required',
							'esiNumber' => 'required|digits:10|numeric',
					    	),
					$messages = array(
 						   'required' => 'The field is required.',
							)

					);
			if ($validator->fails())
            {
			   return Response::json($validator->messages());
			}			   
			else
			{
				$entries['updated_at'] = date('Y-m-d h:i:sa');

				$result1 = DB::table('personalDetails')
						->where('userid', $id)
						->update($entries);
						
				$progress['userid'] 	= $entries['userid'];
				$progress['nextStep']	= 'step3';
				$progress['complete']	= 'off';
				$progress['updated_at']	= date('Y-m-d h:i:sa');

				$updateProgress = DB::table('userProgress')
								->where('userid', $progress['userid'])
								->update($progress);

				session()->put('employeeProgressNextStep', 'step3');

				$result= new StdClass;
				$result->response=$result1;
				$result->status=200;
				$result->message="Data inserted successfully";
				$result->apiKey = $apiKey;
			   	return response()->json($result);
			}		
	}

	public function insertBankData()
	{
		$apiKey = Input::get('apiKey');
		$select = DB::table('user')
				->where('apiKey', $apiKey)
				->select('id', 'companyId')
				->first();

		$id = $select->id;
		$cid = $select->companyId;

		if (Input::get('modeOfPayment1')=='cheque') 
		{
			$check 	= DB::table('BankDetails')
					->where('userId', $id)
					->first();

			if (!empty($check)) 
			{
				$delete = DB::table('BankDetails')
						->where('userId', $id)
						->delete();
			}

			$checkPaytm = DB::table('paytmId')
						->where('userId', $id)
						->first();

			if (!empty($checkPaytm)) 
			{
				$delete = DB::table('paytmId')
						->where('userId', $id)
						->delete();
			}

			$checkPrev 	= DB::table('BankDetails')
						->where('userId', $id)
						->where('modeOfPayment', 'cheque')
						->first();

			$entries['userId'] 			= $id;
			$entries['modeOfPayment']	= 'cheque';

			if (empty($checkPrev)) 
			{
				$insert = DB::table('BankDetails')
						->insert($entries);
			}
			else
			{
				$entries['updated_at'] = date('Y-m-d h:i:sa');

				$update = DB::table('BankDetails')
						->where('userId', $entries['userId'])
						->update($entries);
			}

			$progress['userid'] 	= $id;
			$progress['nextStep']	= 'done';
			$progress['complete']	= 'on';
			$progress['updated_at']	= date('Y-m-d h:i:sa');

			$updateProgress = DB::table('userProgress')
							->where('userid', $progress['userid'])
							->update($progress);

			session()->put('employeeProgressNextStep', 'done');
			session()->put('employeeProgressComplete', 'on');

			return response()->json([
	   				'status' => 200, 
	   				'apiKey' => $apiKey,
	   				'message'=>'Success'
	   				]);	
		}

		if (Input::get('modeOfPayment1')=='directDeposit')
		{
			$validator = Validator::make(
				$entries = array(
						'userid' => $id,
						'modeOfPayment' => Input::get('modeOfPayment1'),
						'bankName' => Input::get('bankName1'),
						'branch' => strtoupper(Input::get('branch1')),
						'accountHolder' => strtoupper(Input::get('accountHolder1')),
						'accountNumber' => Input::get('accountNumber1'),
						'accountType' => strtoupper(Input::get('accountType1')),
						'ifsc' => strtoupper(Input::get('ifsc1')),
						'sharePercent' => strtoupper(Input::get('sharePercent1')),
					    
					),
					array(
						'modeOfPayment' => 'required',
						'bankName' => 'required',
						'branch' => 'required',
						'accountHolder' => 'required',
						'accountNumber' => 'required|numeric|regex:/([0-9]){10,}\d/',
						'accountType' => 'required',
						'ifsc' => 'required|alpha_num|size:11|regex:/([A-Za-z]){4}[0][0-9A-Za-z]{6}/',
						'sharePercent' => 'required',
				    	),
				$messages = array(
						   'required' => 'The :attribute field is required.',
						   'numeric' => 'The :attribute field must be a number'
						)

				);
		}
		
		$form2 = Input::get('bankName2');
		if (isset($form2))
		{
			$validator1 = Validator::make(
				$entries1 = array(
						'userid' => $id,
						'modeOfPayment' => "Direct Deposit",
						'bankName' => Input::get('bankName2'),
						'branch' => strtoupper(Input::get('branch2')),
						'accountHolder' => strtoupper(Input::get('accountHolder2')),
						'accountNumber' => Input::get('accountNumber2'),
						'accountType' => strtoupper(Input::get('accountType2')),
						'ifsc' => strtoupper(Input::get('ifsc2')),
						'sharePercent' => strtoupper(Input::get('sharePercent2')),
					    
					),
					array(
						'userid' => 'required',
						'bankName' => 'required',
						'branch' => 'required',
						'accountHolder' => 'required',
						'accountNumber' => 'required|numeric|regex:/([0-9]){10,}\d/',
						'accountType' => 'required',
						'ifsc' => 'required|alpha_num|size:11|regex:/([A-Za-z]){4}[0][0-9A-Za-z]{6}/',
						'sharePercent' => 'required',
				    	),
				$messages = array(
						   'required' => 'The :attribute field is required.',
						   'numeric' => 'The :attribute field must be a number'
						)

				);
		}

		$form3 = Input::get('paytmId');
		if (isset($form3))
		{
			$validator2 = Validator::make(
				$entries2 = array(
						'userid' 			=> $id,
						'companyId' 		=> $cid,
						'paytmId' 			=> Input::get('paytmId'),
						'paytmSharePercent' => strtoupper(Input::get('paytmSharePercent')),
					    
					),
					array(
						'userid' 			=> 'required',
						'paytmId' 			=> 'required',
						'paytmSharePercent' => 'required',
				    	),
				$messages = array(
						   'required' 	=> 'The :attribute field is required.',
						   'numeric' 	=> 'The :attribute field must be a number'
						)

				);
		}

		if ( (isset($validator)and($validator->fails()))||(isset($validator1)and($validator1->fails()))||(isset($validator2)and($validator2->fails())))
        {
        	$error = array();
        	
        	if (isset($validator))
        	{
        		if ($validator->fails()) 
        		{
	        		$error[0]	=	$validator->messages();
        		}
        		else
        		{
        			$error[0]	=	 new StdClass;
        		} 
        	}
        	if (isset($validator1)) 
        	{
        		if ($validator1->fails()) 
        		{
	        		$error[1]	=	$validator1->messages();
        		}
        		else
        		{
        			$error[1]	=	 new StdClass;
        		} 
        	}
        	if (isset($validator2)) 
        	{
        		if ($validator2->fails()) 
        		{
	        		$error[2]	=	$validator2->messages();
        		}
        		else
        		{
        			$error[2]	=	 new StdClass;
        		} 
        	}

		    return $error;
		}
		else
		{
			$check 	= DB::table('BankDetails')
					->where('userId', $id)
					->first();

			if (!empty($check)) 
			{
				$delete = DB::table('BankDetails')
						->where('userId', $id)
						->delete();				
			}

			$checkPaytm = DB::table('paytmId')
						->where('userId', $id)
						->first();

			if (!empty($checkPaytm)) 
			{
				$deletePaytm 	= DB::table('paytmId')
								->where('userId', $id)
								->delete();
			}

			if (isset($entries))
			{
				$select=DB::table('BankDetails')
						->where('userid', $id)
						->where('modeOfPayment', "directDeposit")
						->first();

				if (empty($select))
				{
					$result = DB::table('BankDetails')
							->insert($entries);										
				}
				else
				{	
					$entries['updated_at'] = date('Y-m-d h:i:sa');
					$result = DB::table('BankDetails')
							->where('userid', $id)
							->where('modeOfPayment', "directDeposit")
							->update($entries);
				}
			}
			if (isset($entries1))
			{
				$select1=DB::table('BankDetails')
						->where('userid', $id)
						->where('modeOfPayment', "Direct Deposit")
						->first();
				
				if (empty($select1))
				{
					$result1 = DB::table('BankDetails')
						->insert($entries1);										
				}
				else
				{	
					$entries1['updated_at'] = date('Y-m-d h:i:sa');
					$result1 = DB::table('BankDetails')
							->where('userid', $id)
							->where('modeOfPayment', "Direct Deposit")
							->update($entries1);
				}
			}
			else 
				$result1=1;

			if (isset($entries2))
			{
				$select2=DB::table('paytmId')
						->where('userId', $id)
						->first();

				if (empty($select2))
				{
					$result2 = DB::table('paytmId')
							->insert($entries2);										
				}
				else
					$result2 = DB::table('paytmId')
							->where('userid', $id)
							->where('paytmId', $entries2['paytmId'])
							->update($entries2);
			}
			else 
				$result2=1;

			if ($result || $result1 || $result2)
			{
				$select = DB::table('user')
						->where('id', $id)
						->update(['registeration'=> 'bank']);
			}
			if ($result && $result1 && $result2) 
			{
				$url="/employeeDashboard1";
				
				$progress['userid'] 	= $id;
				$progress['nextStep']	= 'done';
				$progress['complete']	= 'on';
				$progress['updated_at']	= date('Y-m-d h:i:sa');

				$updateProgress = DB::table('userProgress')
								->where('userid', $progress['userid'])
								->update($progress);

				session()->put('employeeProgressNextStep', 'done');
				session()->put('employeeProgressComplete', 'on');
			}
			else
			{
				$url="/onboardingdetails3";
			}


	   		return response()->json([
	   				'result' => $result, 
	   				'result1' => $result1, 
	   				'result2' => $result2, 
	   				'status' => 200, 
	   				'apiKey' => $apiKey,
	   				'url' => $url, 
	   				'message'=>'Success'
	   				]);	
	   	}
		   	
	}
	public function notification()
	{		
		$apiKey = Input::get('apiKey');
		$select = DB::table('user')
				->where('apiKey', $apiKey)
				->select('id', 'companyId')
				->first();

		$user = $select->id;
		$company=$select->companyId;
		$select5 = DB::table('quotes')
				->where('status', 'active')
				->first();
		$d = date_diff(date_create(date('d-m-Y')), date_create(date('d-m-Y', strtotime($select5->updated_at))));
		if ($d->days > 0) 
		{
			$id = intval($select5->id);

			$update = DB::table('quotes')
					->where('id', $id)
					->update(['status'=>'not active']);

			$rows = count(DB::table('quotes')->get());

			//var_dump($rows);
			//var_dump($id);

			if (($id+1) > $rows) 
			{
				$id =1;
			}
			else
			{
				$id += 1;
			}

		$update2=DB::table('quotes')
				->where('id', $id)
				->update([
					'status' => 'active',
					'updated_at' => date('Y-m-d h:i:sa'),
					]);

		$select5= DB::table('quotes')
				->where('status', 'active')
				->first();
	}

		$select6 	= DB::table('companyPolicy')
					->where('companyId', $company)
					->orderBy('id', 'DESC')
					->get();

		$select7 	= DB::table('notification')
					->where('companyId', $company)
					->where('type', "Employee")
					->orderBy('id', 'DESC')
					->get();

		return response()->json(['quotes'=>$select5, 'message'=>$select6, 'notification'=>$select7]);
		
	}
	
	public function employeePayslip()
	{
		$apiKey = Input::get('apiKey');
		$select = DB::table('user')
				->where('apiKey', $apiKey)
				->select('id', 'companyId')
				->first();
		//		var_dump($select);

		$user = $select->id;
		$company=$select->companyId;
		$year = date('Y');
		$year = $year-1 . '-' . substr(($year), 2);

		$result1 = DB::table('payroll')
				->where('userID', $user)
				->where('financialYear', $year)
				->select('month', 'financialYear', 'created_at')
				->get();
		//var_dump($result1);
		if (empty($result1)){
			return response()->json(['message'=>"No payroll Found"]);
		}
		return response()->json(['result'=>$result1]);		
		
	}


	public function getPayslip()
	{
		# code...
	}


	public function addExpenses()
	{
		$apiKey = Input::get('apiKey');
		$select = DB::table('user')
				->where('apiKey', $apiKey)
				->select('id', 'companyId')
				->first();

		$id = $select->id;
		$cid = $select->companyId;

		$validator = Validator::make(
			$entries = array(
					'userId' 		=> $id,
					'companyId'		=> $cid,
					'dateOfExpense' => Input::get('dateOfExpense'),
					'description' 	=> Input::get('description'),
					'docUrl' 		=> Input::file('doc'),
					'amount' 		=> Input::get('amount'),
					// 'preApproved' => ucfirst(strtolower(Input::get('preApproved'))),
					// 'created_at' => date("d-m-y h:i:sa"),
					),
				array(
					'userId' => 'required',
					'dateOfExpense' => 'required',
					'description' => 'required',
					'amount' => 'required|numeric',
					'docUrl' => 'required|max:500|mimes:jpg,jpeg,png,pdf',
					// 'preApproved' => 'required|string',
					),
				$messages = array(
				   'required' => 'This field is required.',
				   'mimes' => 'The file is not in acceptable format.',
				   'max' => 'The file is bigger than allowed file size.',
				   'numeric' => 'This field must be a number'
				   )
			);

		if ($validator->fails())
        {
        	return response()->json($validator->messages());
		}			   
		else
		{
			$entries['month'] = date('M', strtotime($entries['dateOfExpense']));
			$entries = array_filter($entries);

			$uploadPath = public_path() . '/HTML/Uploads/' . $entries['userId'] . '/Reimbursement' . '/';

			//check if the directory exists
			if(!File::exists($uploadPath)) 
			{
				File::makeDirectory($uploadPath, 0775, true);
			}
			
			$entries['status'] = "Waiting";


			$result = new StdClass;

			if (isset($entries['expenseId'])) 
			{
				if ($action == "edit") 
				{
					// To update an already existing expense/reimbursement
					$select = DB::table('reimbursement')
							->where('expenseId', $entries['expenseId'])
							->first();

					// Move file to upload directory
					if (isset($entries['docUrl'])) 
					{
						$docUrl = $uploadPath . $entries['month'] . '-' . $entries['expenseId'] . '-' . $entries['docUrl']->getClientOriginalName();
						
						if (File::exists($select->docUrl)) 
						{
							unlink($select->docUrl);
						}
						$file = $entries['docUrl']->move($uploadPath, $docUrl);

						$docUrl = explode('public/', $docUrl)[1];
						$entries['docUrl'] = $docUrl;
					}

					if (!empty($select)) {
						$entries['updated_at'] = date("Y-m-d h:i:sa");

						$update = DB::table('reimbursement')
								->where('expenseId', $entries['expenseId'])
								->update($entries);

						$result->status 		= '201';
						$result->statusMessage 	= $entries['status'];
						$result->message 		= 'Data updated successfully.';
						$result->expenseId 		= $entries['expenseId'];
	
						return response()->json($result);
					}
				}
			}
			else
			{
				$select = DB::table('reimbursement')
						->where('userId', $entries['userId'])
						->where('month', $entries['month'])
						->orderBy('id', 'desc')
						->select('expenseId', 'docUrl')
						->first();

				// To make expenseId
				if (empty($select)) 
				{
					$entries['expenseId'] = $entries['userId'] . $entries['month'] . '1';
				}
				else 
				{
					$expenseId = intval(explode($entries['userId'] . $entries['month'], $select->expenseId)[1]);
					$expenseId++;
					$entries['expenseId'] = $entries['userId'] . $entries['month'] .  $expenseId;
				}

				// Move file to upload directory
				if (isset($entries['docUrl'])) 
				{
					$docUrl = $uploadPath . $entries['month'] . '-' . $entries['expenseId'] . '-' . $entries['docUrl']->getClientOriginalName();
					if(File::exists($docUrl)) 
					{
						unlink($docUrl);
						if (!empty($select)) 
						{
							if (File::exists($select->docUrl)) 
							{
								unlink($select->docUrl);
							}
						}
					}
					$file = $entries['docUrl']->move($uploadPath, $docUrl);

					$docUrl = explode('public/', $docUrl)[1];
					$entries['docUrl'] = $docUrl;
				}

				$insert = DB::table('reimbursement')
						->insert($entries);

				$result->status 		= '200';
				$result->statusMessage 	= $entries['status'];
				$result->message 		= 'Data inserted successfully.';
				$result->expenseId 		= $entries['expenseId'];

				return response()->json($result);
			}

			$result->status = '304';
			// $result->statusMessage = $entries['status'];
			$result->message = 'No data was modified.';
			// $result->expenseId = $entries['expenseId'];

			return response()->json($result);
		}
	}
	
}
