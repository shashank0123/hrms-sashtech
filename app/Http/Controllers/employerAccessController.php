<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB, View, Input;
use Illuminate\Http\Request;

class employerAccessController extends Controller 
{

	//businessIntelligence
	public function getBusinessIntelligence()
	{
		if (strtolower(session()->get('type')) == 'employer' ) {
			return view('businessIntelligence');		
		}
		
		else {
			return redirect('login');
		}
	}

	public function billingHistory()
	{
		if (strtolower(session()->get('type')) == 'employer' ) {
			return view('billingHistory');		
		}
		
		else {
			return redirect('login');
		}
	}
	
	public function addContractor_step1()
	{
		if (strtolower(session()->get('type')) == 'employer' ) 
		{
			return view('addContractor_step1');		
		}
		else 
		{
			return redirect('login');
		}
	}
	
	public function addContractor_step2()
	{
		if (strtolower(session()->get('type')) == 'employer' ) {
			return view('addContractor_step2');		
		}
		
		else {
			return redirect('login');
		}
	}
	
	public function addContractor_step3()
	{
		if (strtolower(session()->get('type')) == 'employer' ) {
			return view('addContractor_step3');		
		}
		
		else {
			return redirect('login');
		}
	}
	
	public function addContractor_step4()
	{
		if (strtolower(session()->get('type')) == 'employer' ) {
			return view('addContractor_step4');		
		}
		
		else {
			return redirect('login');
		}
	}
	
	public function addEmployees_step1()
	{	

		if (strtolower(session()->get('type')) == 'employer' ) 
		{
			$titles = DB::table('titles')
					->select('titles')
					->where('companyId',session()->get('companyId'))
					->get();

			$teams 	= DB::table('team')
					->select('team')
					->where('companyId',session()->get('companyId'))
					->get();

			return View::make('addEmployees_step1')
					->with('titles', $titles)
					->with('teams', $teams);
		}
		else 
		{
			return redirect('login');
		}
	}
	
	public function addEmployees_step2()
	{
		if (session()->get('employerId') and (strtolower(session()->get('type')) == 'employer')) 
		{
			return view('addEmployees_step2');		
		}
		else 
		{
			return redirect('addEmployees_step1');
		}
	}
	
	public function addEmployees_step22()
	{
		if (strtolower(session()->get('type')) == 'employer') 
		{
			if (empty(session()->get('employeeId'))) 
			{
				return redirect('manageEmployees');
			}

			$select = DB::table('familyDetails')
					->where('userid', session()->get('employeeId'))
					->get();

			return view('addEmployees_step2-2')
					->with('family', $select);
		}
		else 
		{
			return redirect('login');
		}
	}
	
	public function addEmployees_step3()
	{
		if (strtolower(session()->get('type')) == 'employer' ) 
		{
			return view('addEmployees_step3');		
		}
		else 
		{
			return redirect('login');
		}
	}
	
	public function addEmployees_step4()
	{
		if (strtolower(session()->get('type')) == 'employer' ) 
		{
			return view('addEmployees_step4');		
		}
		else 
		{
			return redirect('login');
		}
	}
	
	public function addBranchLocation()
	{
		if (strtolower(session()->get('type')) == 'employer' ) 
		{
			return view('addBranchLocation');		
		}
		else 
		{
			return redirect('login');
		}
	}
	
	public function dashboard()
	{
		if (strtolower(session()->get('type')) == 'employer' ) 
		{
			return view('dashboard');		
		}
		else 
		{
			return redirect('login');
		}
	}

	public function dashboard1()
	{
		if (strtolower(session()->get('type')) == 'employer' ) 
		{
			if (session()->get('complete') == 'on') 
			{
				$select = DB::table('quotes')
						->where('status', 'active')
						->first();

				$d = date_diff(date_create(date('d-m-Y')), date_create(date('d-m-Y', strtotime($select->updated_at))));

				if ($d->days > 0) 
				{
					$id = intval($select->id);

					$update = DB::table('quotes')
					->where('id', $id)
					->update(['status'=>'not active']);

					$rows = count(DB::table('quotes')->get());

					// var_dump($rows);
					// var_dump($id);

					if (($id+1) > $rows) 
					{
						$id =1;
					}
					else
					{
						$id += 1;
					}

					// var_dump($id);

					$update2=DB::table('quotes')
					->where('id', $id)
					->update([
						'status' => 'active',
						'updated_at' => date('Y-m-d h:i:sa'),
						]);

					$select= DB::table('quotes')
					->where('status', 'active')
					->first();
				}

				if (strlen(session()->get('employerId')) < 4) 
				{
					$id = session()->get('companyCode');
				}
				else
				{
					$id = substr(session()->get('employerId'), 0, 4);
				}

				$select2 = DB::table('personalDetails')
						->where('userid',  'like', $id . '%')
						->orderBy('DateOfBirth', 'ASC')
						->select('DateOfBirth', 'userid')
						->get();

				$select3 = DB::table('user')
						->where('companyId',  session()->get('companyId'))
						->get();

				$select4 	= DB::table('notification')
							->where('companyId', session()->get('companyId'))
							->orderBy('created_at', 'DESC')
							->get();

				// $select6 	= DB::table('companyPolicy')
				// 			->where('companyId', session()->get('companyId'))
				// 			->orderBy('id', 'DESC')
				// 			->get();

				$select5 	= DB::table('company')
							->where('id', session()->get('companyId'))
							->select('managerId')
							->first();

				if (!empty($select5->managerId)) 
				{
					$select8 = DB::table('user')
							->where('id', $select5->managerId)
							->first();

					$select7 = DB::table('personalDetails2')
							->where('userid', $select5->managerId)
							->first();

					if (empty($select7)) 
					{
						$select7 	= NULL;
					}
				}
				else
				{
					$select5 	= NULL;
					$select7 	= NULL;
					$select8 	= NULL;
				}

				return view('employerDashboard', [
					'result' => $select, 
					'result2' => $select2,
					'result3' => $select3,
					'result4' => $select4,
					'result5' => $select5,
					// 'result6' => $select6,
					'result7' => $select7,
					'result8' => $select8,
					]);
			}
			else
			{
				return redirect('getStarted');
			}
		}
		else 
		{
			return redirect('login');
		}
	}

	public function tweet()
	{
		$entries['companyId'] 		= session()->get('companyId');
		$entries['type']			= 'notification';
		$entries['messageContent'] 	= htmlspecialchars(Input::get('message'));

		$insert = DB::table('notification')
				->insert($entries);

		return redirect('employerDashboard');
	}

	
	public function bulkFeature()
	{
		if (strtolower(session()->get('type')) == 'employer' ) {
			return view('bulkFeature');		
		}
		
		else {
			return redirect('login');
		}
	}
	
	public function bulkUserUpload()
	{
		if (strtolower(session()->get('type')) == 'employer' ) {
			return view('bulkUserUpload');		
		}
		
		else {
			return redirect('login');
		}
	}
	
	public function bulkUserEdit()
	{
		if (strtolower(session()->get('type')) == 'employer' ) {
			return view('bulkUserEdit');		
		}
		
		else {
			return redirect('login');
		}
	}
	
	public function notification()
	{
		if (strtolower(session()->get('type')) == 'employer' ) {
			return view('notification');		
		}
		
		else {
			return redirect('login');
		}
	}

	public function insuranceRequirement()
	{
		if (strtolower(session()->get('type')) == 'employer' ) 
		{
			$checkReq 	= DB::table('benefitSearch')
						->where('companyId', session()->get('companyId'))
						->first();

			return view('insuranceRequirement')
					->with('requirements', $checkReq);
		}
		else 
		{
			return redirect('login');
		}
	}

	public function suspendCompany_step1()
	{
		if (strtolower(session()->get('type')) == 'employer' ) {
			return view('suspendCompany_step1');		
		}
		
		else {
			return redirect('login');
		}	
	}
	
	public function suspendCompany_step2()
	{
		if (strtolower(session()->get('type')) == 'employer' ) {
			return view('suspendCompany_step2');		
		}
		
		else {
			return redirect('login');
		}	
	}
	
	public function suspendCompany_step3()
	{
		if (strtolower(session()->get('type')) == 'employer' ) {
			return view('suspendCompany_step3');		
		}
		
		else {
			return redirect('login');
		}	
	}

}
