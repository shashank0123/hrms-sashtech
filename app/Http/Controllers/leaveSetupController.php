<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input,DB, Session, Validator, StdClass, View, File;
use Illuminate\Http\Request;

class leaveSetupController extends Controller {

	public function getSetup()
	{
		if (strtolower(session()->get('type')) != 'employer') 
		{
			return redirect('login');
		}

		// if (strtolower(session()->get('complete')) == 'on' ) 
		// {
		// 	return redirect('employerDashboard');
		// }

		$select = DB::table('leaveSetup')->where('companyId', session()->get('companyId'))->first();
		return  View::make('setupLeave')
				->with('result', $select);
	}

	public function addLeave()
	{
		$leaves = Input::get('weekLeave');
	
		if (isset($leaves)) 
		{
			foreach ($leaves as $key => $value) 
			{
				if ($key > 0) 
				{
					$weekLeave .= ', ' . $value;
				}
				else
				{
					$weekLeave = $value;
				}
			}
		}
		else
		{
			$weekLeave = 0;
		}

		if (intval(Input::get('earnedLeave')) < 1) 
		{
			$earnedLeave = 0;
		}
		else
		{
			$earnedLeave = Input::get('earnedLeave');
		}

		$validator = Validator::make(
				$entries = array(
						'companyId' 	=> Session::get('companyId'),
						'setupHoliday' 	=> Input::get('setupHoliday'),
						'weekLeave' 	=> $weekLeave,
						'casualLeave' 	=> Input::get('casualLeave'),
						'earnedLeave' 	=> $earnedLeave,
						'sickLeave' 	=> Input::get('sickLeave'),
						'payDay' 		=> Input::get('payDay'),
						'runPayroll' 	=> Input::get('runPayroll'),
						// 'created_at' => date("d-m-y h:i:sa"),
					),
					array(
						'companyId' 	=> 'required',
						'setupHoliday' 	=> 'required',
						'weekLeave' 	=> 'required',
						'payDay' 		=> 'required',
						'runPayroll' 	=> 'required',
						// 'casualLeave' 	=> 'required',
						// 'earnedLeave' 	=> 'required'
				    	),
				$messages = array(
						   'required' 	=> 'The :attribute field is required.'						   
						)
				);

		if ($validator->fails())
        {
		   return response()->json($validator->messages());
		}			   
		else
		{
			$check = DB::table('leaveSetup')
					->where('companyId', session()->get('companyId'))
					->first();

			if (!empty($check))
			{
				$entries['setupHoliday'] 	= $check->setupHoliday;
			}

			$content=Input::get('events');

	        // if (session()->get('employerId') and (empty($check)))
	        if (session()->get('employerId'))
	        {
	    		$uploadPath = public_path() . '/HTML/Uploads/' . session()->get('companyId').'/';
	            
	            if(!File::exists($uploadPath)) 
	            {
	                File::makeDirectory($uploadPath, 0775, true);
	            }    
	            
	            $uploadPath 	= $uploadPath.'calendarJson';
	            $result 		= File::put($uploadPath, $content);
	            
	            if ($result)
	            {
	            	$uploadPath = explode(public_path(), $uploadPath)[1];

	            	$result = DB::table('calendar')
	            			->where('userid', session()->get('companyId'))
	            			->first();
	            	
	            	if (empty($result))
	            	{
	              		$result	= DB::table('calendar')
	              				->insert([
	              					'userid' => session()->get('companyId'), 
	              					'jsonObject' => $uploadPath
	              				]);
	              	}
	              	else
	              	{
		                $result 	= DB::table('calendar')
		                			->where('userid', session()->get('companyId'))
		                			->update([
		                				'userid' => session()->get('companyId'), 
		                				'jsonObject' => $uploadPath
		                				]);

		                $result 	= !($result);
		            }
	            }
	    	}

			// $date1=date_create_from_format()
			$select = DB::table('leaveSetup')
					->where('companyId', session()->get('companyId'))
					->first();

			// $entries['payDay']		= date('d', strtotime($entries['payDay']));
			// $entries['runPayroll'] = date('d', strtotime($entries['runPayroll']));
			
			if (empty($select))
			{
				$result=DB::table('leaveSetup')->insert($entries);
			}
			else
			{
				$entries['payDay'] 		= $select->payDay;
				$entries['runPayroll']	= $select->runPayroll;

				$result	= DB::table('leaveSetup')
						->where('companyId', session()->get('companyId'))
						->update($entries);
			}
			
			$select = DB::table('employerProgress')
					->where('companyId', session()->get('companyId'))
					->update([
						'step6' 	=> 'on',
						'step7' 	=> 'pending',
						'nextStep' 	=> 'step7',
						]);

			$response			= new StdClass;
			$response->result 	= $result;
			$response->status 	= 200;
			$response->url 		= "/companyPolicy";		

		   	return response()->json($response);
		}
	}

	public function getLeave()
	{
		if (session()->get('employerId')) 
		{
			$select = DB::table('leaveSetup')
					->where('companyId', session()->get('companyId'))
					->orderBy('created_at', 'DESC')
					->first();

			return View::make('setupLeave')
					->with('result', $select);		
		}
		
		else 
		{
			return redirect('login');
		}	
	}
	

}
