<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function()
{
	return View::make('hello');
});
// Route::post('/', 'addEmployerController@fromMain');
Route::get('demo', function()
{
	return View::make('demo');
});
Route::get('contactUs', function()
{
	return View::make('contactUs');
});
Route::get('timeSheet', function()
{
	return View::make('timeSheet');
});
Route::get('pressAndMedia', function()
{
	return View::make('pressAndMedia');
});
Route::get('accountant', function()
{
	return View::make('accountant');
});
Route::get('customer', function()
{
	return View::make('customer');
});
Route::get('pricing', function()
{
	return View::make('pricing');
});
Route::post('pricing','addEmployerController@selectPlan');
Route::get('careers', function()
{
	return View::make('careers');
});
Route::get('security', function()
{
	return View::make('security');
});
Route::get('privacyPolicy', function()
{
	return View::make('privacyPolicy');	
});
Route::get('aboutUs', function()
{
	return View::make('aboutUs');
});
Route::get('payroll', function()
{
	return View::make('payroll');
});
Route::get('health-insurance-benefit', function()
{
	return View::make('health-insurance-benefit');
});
Route::get('inHandPayCalculator', function()
{
	return View::make('inHandPayCalculator');
});
Route::get('expense', function()
{
    return View::make('expense');
});
Route::post('oauth/access_token', function() {
	return Response::json(Authorizer::issueAccessToken());
});
Route::get('getStarted', 'getStartedController@viewDashboard');
Route::get('confirmEmail', 'addEmployerController@confirmEmail');
Route::get('payslip', 'reportController@viewPayslip');
Route::get('payslip/{year}/{month}', 'reportController@getPayslip');
Route::get('taxslip', 'taxslipController@getTaxSlip');
Route::get('monthslip/{month}', 'reportController@employerMonthSlip');
Route::get('tdsReport', 'companyDetailsController@viewTDS');
Route::get('otherReport', 'employeeAccessController@otherReport');
Route::get('businessIntelligence', 'employerAccessController@getBusinessIntelligence');
Route::get('insuranceRequirement', 'employerAccessController@insuranceRequirement');
Route::post('insuranceRequirement','benefitController@searchBenefit');
Route::get('addBenefit_step1', 'benefitController@viewStep1');
Route::post('addBenefit_step1','benefitController@addBenefit');
Route::get('addBenefit_step2', 'benefitController@viewChooseBenefit');
Route::post('addBenefit_step2/{id}','benefitController@chooseBenefit');
Route::get('addBenefit_step3', 'benefitController@addEmployee' );
Route::get('calendar', 'calenderController@openCalender');
Route::get('setupLeave', 'leaveSetupController@getSetup');
Route::post('setupLeave', 'leaveSetupController@addLeave');
Route::get('employeeBenefits_step1', 'employeeAccessController@employeeBenefits_step1');
Route::post('employeeBenefits_step1', 'benefitController@addemployeeBenefitoptions');
// Route::get('employeeToBenefit', function()
// {
// 	return View::make('employeeToBenefit');
// });
Route::get('adminCreateUser', 'adminController@viewUser');
Route::post('adminCreateUser', 'adminController@createUser');
Route::post('adminCreateUser/{id}', 'adminController@updateStatus');
Route::get('adminDashboard', 'adminController@dashboard');
Route::get('adminSelectCompany', 'adminController@selectCompany');
Route::get('adminSelectCompany/{id}', 'adminController@changeCompany');

Route::get('coupon', 'couponController@viewCoupon');
Route::post('coupon', 'couponController@addCoupon');
Route::get('adminAssignCompany', 'adminController@viewAssignCompany');
Route::post('adminAssignCompany', 'adminController@assignCompany');
Route::get('adminBilling', function()
{
	if (strtolower(session()->get('type')) != 'admin') 
	{
		return redirect('login');
	}
	return View::make('admin/adminBilling');
});
Route::get('billingHistory', 'employerAccessController@billingHistory');
Route::get('renewal', function()
{
	return View::make('renewal');
});
Route::get('managerDetail', 'managerDetailsController@getDetails');
Route::get('managerSelectCompany', 'managerDetailsController@viewSelectCompany');
Route::get('managerSelectCompany/{id}', 'managerDetailsController@changeCompany');
Route::get('accountantCreateUser', 'accountantController@viewUser');
Route::post('accountantCreateUser', 'accountantController@createUser');
Route::post('accountantCreateUser/{id}', 'accountantController@updateStatus');
Route::get('accountantDashboard', 'accountantController@dashboard');
Route::get('accountantSelectCompany', 'accountantController@viewSelectCompany');
Route::get('accountantSelectCompany/{id}', 'accountantController@changeCompany');
Route::get('accountantBilling', function()
{
	if (strtolower(session()->get('type')) != 'accountant') 
	{
		return redirect('login');
	}
	return View::make('accountant/accountantBilling');
});
Route::get('accountantAssignCompany', 'accountantController@viewAssignCompany');
Route::post('accountantAssignCompany', 'accountantController@assignCompany');
Route::post('managerDetail', 'managerDetailsController@addDetails');
Route::get('manageContractors', 'employer\manageEmployeeController@getContractor');
Route::get('viewEmployee/{id}', 'employer\manageEmployeeController@viewEmployeeDetails');
Route::get('viewEmployee', 'employer\manageEmployeeController@getEmployeeDetail');
Route::post('viewEmployee', 'employeeDetailsController@editEmployeeDetails');
// Route::get('viewEmployee', function()
// {
// 	return View::make('viewEmployee');
// });
Route::get('viewContractor/{id}', 'employer\manageEmployeeController@setContractorDetails');
Route::get('viewContractor', 'employer\manageEmployeeController@viewContractorDetails');
Route::post('viewContractor', 'employer\manageEmployeeController@updateContractorDetails');
Route::get('suspendCompany_step1', 'employerAccessController@suspendCompany_step1');
Route::post('suspendCompany_step1', 'suspendCompanyController@suspendCompany_step1');
Route::get('suspendCompany_step2', 'employerAccessController@suspendCompany_step2');
Route::post('suspendCompany_step2', 'suspendCompanyController@suspendCompany_step2');
Route::get('suspendCompany_step3', 'employerAccessController@suspendCompany_step3');
Route::post('suspendCompany_step3', 'suspendCompanyController@suspendCompany_step3');
Route::get('addContractor_step1', 'employerAccessController@addContractor_step1');
Route::get('addContractor_step2', 'employerAccessController@addContractor_step2');
Route::get('addContractor_step3', 'employerAccessController@addContractor_step3');
Route::get('addContractor_step4', 'employerAccessController@addContractor_step4');
Route::post('addContractor_step1', 'employer\manageEmployeeController@addContractor');
Route::post('addContractor_step2', 'employer\manageEmployeeController@insertPersonalData');
Route::post('addContractor_step3', 'employer\manageEmployeeController@insertTaxData');
Route::post('addContractor_step4', 'employer\manageEmployeeController@insertPaymentData');
Route::get('addEmployees_step1', 'employerAccessController@addEmployees_step1');
Route::post('addEmployees_step1', 'employer\addEmployeeController@addEmployee');
Route::post('addEmployees_step1/{ctc}', 'employer\addEmployeeController@getCTC');
Route::get('addEmployees_step2', 'employerAccessController@addEmployees_step2');
Route::post('addEmployees_step2', 'employer\addEmployeeController@insertPersonalData');
Route::get('addEmployees_step2-2', 'employerAccessController@addEmployees_step22');
Route::post('addEmployees_step2-2', 'employer\addEmployeeController@insertFamilyData');
Route::get('addEmployees_step2-2/{id}', 'employer\addEmployeeController@getFamilyData');
Route::post('addEmployees_step2-2/{id}', 'employer\addEmployeeController@deleteFamilyData');
Route::get('addEmployees_step3', 'employerAccessController@addEmployees_step3');
Route::post('addEmployees_step3', 'employer\addEmployeeController@insertTaxData');
Route::get('addEmployees_step4', 'employerAccessController@addEmployees_step4');
Route::post('addEmployees_step4', 'employer\addEmployeeController@insertBankData');
Route::get('bankSetup', 'companyDetailsController@getCompanyBankDetails');
Route::post('bankSetup', 'companyDetailsController@addBankAccount');
Route::get('companyDetail', 'companyDetailsController@viewCompanyDetails');
Route::post('companyDetail', 'companyDetailsController@updateCompanyDetails');
Route::get('addCompanyLocation', 'companyDetailsController@getCompanyDetails');
Route::get('addBranchLocation', 'employerAccessController@addBranchLocation');
Route::post('addCompanyLocation', 'companyDetailsController@updateCompanyAddress');
Route::post('addBranchLocation', 'companyDetailsController@updateBranchAddress');
// Route::get('companyDetail_step2', function()
// {
// 	return View::make('companyDetail_step2');
// });
Route::get('companyPolicy', 'companyPolicyController@getPolicy');
Route::post('companyPolicy', 'companyPolicyController@saveCompanyPolicy');
Route::post('companyPolicy/{id}', 'companyPolicyController@deletePolicy');
Route::get('companyPolicy/{id}', 'companyPolicyController@editPolicy');
Route::get('employeeViewCompanyPolicy', 'companyPolicyController@viewEmployeePolicy');
Route::get('taxDetail', 'taxDetailController@getTaxDetail');
Route::post('taxDetail', 'taxDetailController@addTaxDetail');

Route::get('bulkFeature', 'employerAccessController@bulkFeature');
Route::get('bulkUserUpload', 'employerAccessController@bulkUserUpload');
Route::post('bulkUserUpload', 'bulkFeatureController@usersUpload');
Route::get('bulkUserEdit', 'employerAccessController@bulkUserEdit');
Route::post('bulkUserEdit', 'bulkFeatureController@usersEdit');
Route::get('notification', 'employerAccessController@notification');
Route::post('notification', 'notificationController@addNotification');
Route::get('selectPlan', 'companyDetailsController@companyPlan');
Route::post('selectPlan', 'companyDetailsController@selectPlan');
Route::get('accountantSelectPlan', 'companyDetailsController@companyPlan');
Route::post('accountantSelectPlan', 'companyDetailsController@selectPlan');
Route::get('authentication', function()
{
	return View::make('authentication');
});
Route::post('authentication', 'addEmployerController@verifyNumber');
Route::get('viewCompany', function()
{
	return View::make('viewCompany');
});
Route::get('selectCompany', function()
{
	return View::make('selectCompany');
});
Route::get('login', function()
{
	return View::make('login');
});

Route::get('employerExpenses', 'ReimbursementController@approveExpense');
Route::post('employerExpenses', 'ReimbursementController@changeExpenseStatus');
Route::post('login', 'employeeLoginController@login');
Route::get('logout', 'employeeLoginController@logout');
Route::get('signup', function()
{
	return View::make('signup');
});
Route::post('signup', 'addEmployerController@signUpEmployer');
Route::get('forgotPassword', function()
{
	return View::make('forgotPassword');
});
Route::post('forgotPassword', 'forgotPasswordController@forgotPassword');
Route::get('resetPassword', function()
{
	$apiKey  = Input::get('apiKey');
	if (empty($apiKey)) 
	{
		return redirect('forgotPassword');
	}
	return View::make('resetPassword');
});
Route::post('resetPassword', 'employeeLoginController@resetPasswordApiKey');
Route::get('runPayroll', 'payrollController@getPayrolluser');
Route::post('runPayroll', 'payrollController@savePayroll');
Route::get('payContractor', 'payContractorController@getContractor');
Route::get('payContractor1', function()
{
	return View::make('payContractor1');
});
Route::post('payContractor', 'payContractorController@saveContractor');
Route::get('salarySetup', 'companyDetailsController@viewCTC');
Route::post('salarySetup/{id}', 'companyDetailsController@deleteCTC');
Route::get('salarySetup/{id}', 'companyDetailsController@editCTC');
Route::post('salarySetup', 'companyDetailsController@addCTC');
Route::get('oneTimePayment', 'oneTimePaymentController@getOneTimePaymentData');
Route::post('oneTimePayment/{team}', 'oneTimePaymentController@teamDetails');
Route::post('oneTimePayment', 'oneTimePaymentController@enterOTP');
Route::get('employeeDashboard', 'employeeAccessController@dashboard');
Route::get('employeeDashboard1', 'employeeAccessController@dashboard1');
Route::get('employeePayslip', 'employeeAccessController@employeePayslip');
Route::get('employeeExpenses', 'ReimbursementController@getExpenses');
Route::post('employeeExpenses', 'ReimbursementController@updateExpense');
Route::get('employeeDeduction', 'employeeAccessController@employeeDeduction');
Route::get('investmentDeduction', 'deductionController@getInvestment');
Route::post('investmentDeduction', 'employeeDetailsController@investmentDeduction');
Route::get('medicalDeduction', 'deductionController@getMedical');
Route::post('medicalDeduction', 'employeeDetailsController@medicalDeduction');
Route::get('otherDeduction', 'deductionController@getOther');
Route::post('otherDeduction', 'employeeDetailsController@otherDeduction');
Route::get('hraMonthlyDetails', 'deductionController@getHraMonthly');
Route::post('hraMonthlyDetails', 'employeeDetailsController@HRAmonthlyDeduction');
Route::get('interestOnSelfOccupiedProperty', 'deductionController@getSelfOccupiedController');
Route::get('allowanceDeclaration', 'ReimbursementController@viewAllowanceDeclaration');
Route::post('allowanceDeclaration', 'ReimbursementController@addAllowanceDeclaration');
Route::post('interestOnSelfOccupiedProperty', 'employeeDetailsController@selfOccupiedDeduction');
Route::get('incomeFromPreviousEmployer', 'deductionController@getIncomeFromPreEmployer');
Route::post('incomeFromPreviousEmployer', 'employeeDetailsController@previousEmployerDeduction');
Route::get('onboardingdetails1', 'employeeDetailsController@getPersonalData');
Route::post('onboardingdetails1', 'employeeDetailsController@insertPersonalData');
Route::get('onboardingdetails2', 'employeeDetailsController@getTaxData');
Route::post('onboardingdetails2', 'employeeDetailsController@insertTaxData');
Route::get('onboardingdetails1-2', 'employeeDetailsController@getFamilyData');
Route::post('onboardingdetails1-2', 'employeeDetailsController@insertFamilyData');
Route::get('onboardingdetails1-2/{id}', 'employeeDetailsController@editFamilyData');
Route::post('onboardingdetails1-2/{id}', 'employeeDetailsController@deleteFamilyData');

Route::get('onboardingdetails3', 'employeeDetailsController@getBankData');
Route::get('viewDetails', 'employeeDetailsController@getEmployeeDetails');
Route::post('viewDetails', 'employeeDetailsController@updateEmployeeDetails');
Route::post('onboardingdetails3', 'employeeDetailsController@insertBankData');
Route::get('getPersonalData/{apiKey}', 'EmployeeDetailApi@getPersonalDetails');
Route::get('getPersonalData/{apiKey}/{pan}', 'EmployeeDetailApi@editPersonalData');
Route::get('404', function() {
	return View::make('404');
});
Route::get('manageEmployees', 'employer\manageEmployeeController@getEmployee');
Route::post('delete/{id}', 'employer\manageEmployeeController@delete');
// Route::get('terminate/{id}', 'employer\manageEmployeeController@terminate');
Route::post('calendardata', 'calenderController@sendUserCalendarData');
Route::get('calendardata', 'calenderController@getUserCalenderData');
Route::get('addEmployees_step1/{title}', 'addEmployeeController@getCTC');
Route::get('changeView', 'changeViewController@switchView');
Route::get('adminView', 'changeViewController@adminView');
Route::get('accountantView', 'changeViewController@accountantView');
Route::get('employerDashboard', 'employerAccessController@dashboard1');
Route::post('employerDashboard', 'employerAccessController@tweet');
Route::get('accountantDetail', 'accountantController@getDetails');
Route::post('accountantDetail', 'accountantController@accountantDetail');
Route::get('addBenefit_step0', function()
{
	if (strtolower(session()->get('type')) != 'employer') 
	{
		return redirect('login');
	}

	// if (strtolower(session()->get('complete')) == 'on' ) 
	// {
	// 	return redirect('employerDashboard');
	// }

	return View::make('addBenefit_step0');
});
Route::get('runPayroll2', function()
{
	return View::make('runPayroll2');
});
Route::get('payContractor2', function()
{
	return View::make('payContractor2');
});

Route::get('reviewPayroll', 'payrollController@getReview');

Route::get('changePassword', function()
{
	return View::make('changePassword');
});
Route::post('changePassword', 'employeeLoginController@changePassword');
Route::get('terminate', function()
{
	return View::make('terminate');
});
Route::post('terminate', 'employeeDetailsController@terminateEmployee');
Route::get('generateBankData', 'payrollController@payEmployee');


Route::get('faq', function()
{
	return View::make('faq');
});

Route::post('/setupLeave/{id}','calenderController@holidayList');
Route::get('/companyCalendar','calenderController@getCompanyCalenderData');







Route::get('employeeBenefit', function()
{
	return View::make('employee/employeeBenefit');
});

Route::get('contractorDashboard', function()
{
	return View::make('contractor/contractorDashboard');
});
Route::get('contractorDashboard1', function()
{
	return View::make('contractor/contractorDashboard1');
});
Route::get('addContractorOnboarding_step1', function()
{
	return View::make('contractor/addContractor_step1');
});
Route::get('addContractorOnboarding_step2', function()
{
	return View::make('contractor/addContractor_step2');
});
Route::get('addContractorOnboarding_step3', function()
{
	return View::make('contractor/addContractor_step3');
});
Route::get('addContractorOnboarding_step1', 'contractorController@getPersonalData');
Route::post('addContractorOnboarding_step1', 'contractorController@insertPersonalData');
Route::get('addContractorOnboarding_step2', 'contractorController@getTaxData');
Route::post('addContractorOnboarding_step2', 'contractorController@insertTaxData');
Route::get('addContractorOnboarding_step3', 'contractorController@getBankData');
Route::post('addContractorOnboarding_step3', 'contractorController@insertBankData');


Route::get('contractorExpenses', function()
{
	return View::make('contractor/contractorExpenses');
});
Route::get('contractorPayslip', function()
{
	return View::make('contractor/contractorPayslip');
});
Route::get('contractorDetails', function()
{
	return View::make('contractor/viewDetails');
});

Route::get('accountantAddCompany', function()
{
	if (strtolower(session()->get('type')) != 'accountant') 
	{
		return redirect('login');
	}

	return View::make('accountant/accountantAddCompany');
});

Route::get('adminAddCompany', function()
{
	if (strtolower(session()->get('type')) != 'admin') 
	{
		return redirect('login');
	}

	return View::make('admin/adminAddCompany');
});

Route::post('adminAddCompany','adminController@addCompany');
Route::post('accountantAddCompany','accountantController@addCompany');

Route::get('managerHistory', function()
{
	return View::make('manager/managerHistory');
});

Route::get('employeeCalendar', function()
{
	if (strtolower(session()->get('type')) != 'employee' ) 
	{
		return redirect('login');
	}

	return View::make('employee/employeeCalendar');
});

Route::get('managerDashboard', 'managerDetailsController@dashboard');

Route::get('generateRentReceipt', function()
{
	return View::make('generateRentReceipt');
});
Route::get('tools', function()
{
	return View::make('tools');
});



// =================================Android=========================================

Route::get('signin', 'employeeLoginController@signin');
Route::post('onboarding1', 'EmployeeDetailApi@insertPersonalData');
Route::post('onboarding2', 'EmployeeDetailApi@insertTaxData');
Route::post('onboarding3', 'EmployeeDetailApi@insertBankData');
Route::get('employeePayslipAndro', 'EmployeeDetailApi@employeePayslip');

Route::get('notificationandro', 'EmployeeDetailApi@notification');
Route::post('androidExpense', 'EmployeeDetailApi@addExpenses');

