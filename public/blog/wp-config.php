<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'Sashtechs_blog');

/** MySQL database username */
define('DB_USER', 'sashtechsdev');

/** MySQL database password */
define('DB_PASSWORD', 'sashtechsdev');

/** MySQL hostname */
define('DB_HOST', 'sashtechs-dev-env.cvotsjvmphyz.us-east-1.rds.amazonaws.com');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '>O^^+acjE?&Hf#4*JS[<:gv{Q ymb8WbzI0sdlE<k2LJp|/E`bF3Q:+aYLrS~6Bm');
define('SECURE_AUTH_KEY',  '9FPgy-q}>:oJ%o&:-NOS-PF]+O>K87mf~U+F-vFkw]B_n/h);x-~p-{zsE/#ET~>');
define('LOGGED_IN_KEY',    'T_n,$`7NAI1D2PV_dhq92MBi|G=$e./+*>GG+[(n!,@ v,!O=IU%Yk/-z4%R*Z{2');
define('NONCE_KEY',        'z>[%(E(>jIW1b1d,ajI?pv,gMu+huBx~ @U0j1u[sASD. !dBab6&UV*7*|~NS%c');
define('AUTH_SALT',        ';Oh!+`FlGuLjH|I#7ITLrG]rPQ%$R&PM+GB.$~dM)z@+e9EP/.db;K*K|&8e~RRn');
define('SECURE_AUTH_SALT', '|SXF/3vVz]>]S=(0|z)!VhRz`EZQiocZ1HnHAx}c|)efU}$U>+<H=CnCB|B{pX=-');
define('LOGGED_IN_SALT',   '|k[hi`_JdgT.EdP9C_>+ JB0lp3EA.df?h;(T=NPZUFcs;7;-=en;OmT!f1pYY%Q');
define('NONCE_SALT',       '],I{EIu}_&<+(ad&?Tqg!8=_wIw+I6[OADzHe%`$U,(,rMhHyoeCGpm7&o!J2-l}');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
