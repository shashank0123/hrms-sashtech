<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package sparkling
 */
?>
</div><!-- close .row -->
</div><!-- close .container -->
</div><!-- close .site-content -->

<div id="footer-area">
	<div class="container footer-inner">
		<div class="row">
			<?php get_sidebar( 'footer' ); ?>
		</div>
	</div>

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info container">
			<div class="row">
				<?php if( of_get_option('footer_social') ) sparkling_social_icons(); ?>
				<nav role="navigation" class="col-md-12">
					<?php // sparkling_footer_links(); ?>
					<div class="row-fluid visible-desktop container">
						<div class="col-md-3 col-sm-3 col-xs-12 section1" style="color:#fff">
							<h5 style="padding-left:2.2em;"> Resources:</h5>
							<ul>
								<li><a href="<?php echo $base_url?>/blog">Blog</a></li>
								<li><a href="<?php echo $base_url?>/pressAndMedia">Press</a></li>
								<li><a href="<?php echo $base_url?>/help">Help Center</a></li>
								<li><a href="<?php echo $base_url?>/faq">FAQ</a></li>
							</ul>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12 section2">
							<h5 style="padding-left:2.2em;"> Company</h5>
							<ul>
								<li><a href="<?php echo $base_url?>/aboutUs">About Us</a></li>
								<li><a href="<?php echo $base_url?>/customer">Customers</a></li>
								<li><a href="<?php echo $base_url?>/accountant">Partners</a></li>
								<li><a href="<?php echo $base_url?>/pricing">Pricing</a></li>
								<li><a href="<?php echo $base_url?>/security">Security</a></li>
								<li><a href="<?php echo $base_url?>/careers">Careers</a></li>
							</ul>
						</div>
						<div class="col-md-3 col-sm-2 col-xs-6 section3">
							<h5 style="padding-left:2.2em;"> HR PLATFORM</h5>
							<ul>
								<li><a href="<?php echo $base_url?>/payroll">Payroll</a></li>
								<li><a href="<?php echo $base_url?>/expense">Expenses</a></li>
								<li><a href="<?php echo $base_url?>/benefit">Benefits</a></li>
								<li><a href="<?php echo $base_url?>/timeSheet">Time</a></li>
							</ul>
						</div>
						<div class="col-md-3 col-sm-2 col-xs-12 section5">
							<h5 style="padding-left:2.2em;"> GET IN TOUCH</h5>
							<ul>
								<li><a href="<?php echo $base_url?>/contactUs">Contact</a></li>
							</ul>
							<br><br>
							<a href="https://www.facebook.com/Sashtechs" target="blank"><img src="https://www.sashtechs.com/images/fb.png" alt=""></a>
							<a href="https://www.linkedin.com/company/Sashtechs" target="blank"><img src="https://www.sashtechs.com/images/linkdin.png" alt=""></a>
							<a href="https://twitter.com/Sashtechs" target="blank"><img src="https://www.sashtechs.com/images/twitter.png" alt=""></a>
						</div>
					</div>
				</nav>
				<div class="copyright col-md-6">
					<?php echo of_get_option( 'custom_footer_text', 'sparkling' ); ?>
					<?php sparkling_footer_info(); ?>
				</div>
			</div>
		</div><!-- .site-info -->
		<div class="scroll-to-top"><i class="fa fa-angle-up"></i></div><!-- .scroll-to-top -->
	</footer><!-- #colophon -->
</div>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>