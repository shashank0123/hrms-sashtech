<?php

/**
 * Social  Widget
 * Sparkling Theme
 */
class sparkling_social_widget extends WP_Widget
{
	 function sparkling_social_widget(){
            
            $widget_ops = array('classname' => 'sparkling-social','description' => esc_html__( "Sparkling Social Widget" ,'sparkling') );
            parent::__construct('sparkling-social', esc_html__('Sparkling Social Widget','sparkling'), $widget_ops);
            
    }

    function widget($args , $instance) {
    	extract($args);
        $title = isset($instance['title']) ? $instance['title'] : esc_html__('Follow us' , 'sparkling');

        echo $before_widget;
        echo $before_title;
        echo $title;
        echo $after_title;

        /**
         * Widget Content
         */ ?>

        <!-- social icons -->
        <div class="social-icons sticky-sidebar-social">
            <nav id="menu-social" class="social-icons">
             <ul id="menu-social-items" class="social-menu">
               <li id="menu-item-2036" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2036">
                 <a target="_blank" href="https://twitter.com/Sashtechs" onclick="__gaTracker('send', 'event', 'outbound-widget', 'https://twitter.com/Sashtechs', 'Twitter');">
                   <i class="social_icon fa">
                     <span>Twitter</span>
                   </i></a>
                 </li>
                 <li id="menu-item-2037" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2037">
                   <a target="_blank" href="https://www.facebook.com/Sashtechs" onclick="__gaTracker('send', 'event', 'outbound-widget', 'https://www.facebook.com/Sashtechs', 'Follow us on Facebook');">
                     <i class="social_icon fa">
                       <span>Follow us on Facebook</span>
                     </i>
                   </a>
                 </li>
                 <li id="menu-item-2038" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2038">
                   <a target="_blank" href="https://www.linkedin.com/company/Sashtechs" onclick="__gaTracker('send', 'event', 'outbound-widget', 'https://www.linkedin.com/company/Sashtechs', 'Follow us on LinkedIn');">
                     <i class="social_icon fa">
                       <span>Follow us on LinkedIn</span>
                     </i>
                   </a>
                 </li>
                 
                 </ul></nav>

            <?php // sparkling_social_icons(); ?>

        </div><!-- end social icons --><?php

        echo $after_widget;
    }

    function form($instance) {
      if(!isset($instance['title'])) $instance['title'] = esc_html__('Follow us' , 'sparkling'); ?>

      <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php esc_html_e('Title ','sparkling') ?></label>

      <input type="text" value="<?php echo esc_attr($instance['title']); ?>"
                          name="<?php echo $this->get_field_name('title'); ?>"
                          id="<?php $this->get_field_id('title'); ?>"
                          class="widefat" />
      </p><?php
    }

}
?>